---
name: "需求反馈"
about: "向我们反馈你想要需求功能，以便我们改进产品"
title: "【Requirement】"
labels: ["requirement"]
assignees: "openlinksaas"
---

## 概述
<!-- 描述你的需求 -->


## 理想解决方案
<!-- 可以给出你的解决方案 -->