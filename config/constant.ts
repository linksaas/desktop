//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

// 开发端口
export const PORT = 3000;
// 基本路径
export const VITE_BASE_PATH = '/';
// 应用名称
export const VITE_APP_TITLE = 'Vite React App';


// 去除 console
// export const VITE_DROP_CONSOLE = true;
// 开启兼容
export const VITE_APP_LEGACY = true;