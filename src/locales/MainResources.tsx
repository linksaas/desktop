//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { EnHeader, ZhHeader } from "./header"
import { EnLeftMenu, ZhLeftMenu } from "./leftMenu"
import { EnProject, ZhProject } from "./project"
import { EnText, ZhText } from "./text"
import { EnUser, ZhUser } from "./user"
import { EnWorkBench, ZhWorkBench } from "./workbench"

const EnResource = {
    ...EnLeftMenu,
    ...EnHeader,
    ...EnWorkBench,
    ...EnText,
    ...EnUser,
    ...EnProject,
}

const ZhResource = {
    ...ZhLeftMenu,
    ...ZhHeader,
    ...ZhWorkBench,
    ...ZhText,
    ...ZhUser,
    ...ZhProject,
}

export const MainResources = {
    "en": {
        translation: EnResource,
    },
    "zh": {
        translation: ZhResource,
    }
}