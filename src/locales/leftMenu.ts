//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

export const EnLeftMenu = {
    "leftMenu.workbench": "WorkBench",
    "leftMenu.dataview": "DataView",
    "leftMenu.project": "Projects",
    "leftMenu.org": "Teams",
    "leftMenu.pubres": "Public Resource",
    "leftMenu.growcenter": "Grow Center",
}

export const ZhLeftMenu = {
    "leftMenu.workbench": "工作台",
    "leftMenu.dataview": "数据视图",
    "leftMenu.project": "项目",
    "leftMenu.org": "团队",
    "leftMenu.pubres": "公共资源",
    "leftMenu.growcenter": "成长中心",
}
