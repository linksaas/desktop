//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

export const EnUser = {
    "user.authorizedLogin": "Authorized login",
    "user.registerAccount": "Register Account",
    "user.login": "Login",
    "user.authorize.atomGit": "Authorize By AtomGit",
    "user.authorize.gitCode": "Authorize By GitCode",
    "user.authorize.gitee": "Authorize By Gitee",
    "user.logout": "Logout",
    "user.logoutMsg": "Do you want to logout?",
}

export const ZhUser = {
    "user.authorizedLogin": "授权登录",
    "user.registerAccount": "注册账号",
    "user.login": "登录",
    "user.authorize.atomGit": "AtomGit授权登录",
    "user.authorize.gitCode": "GitCode授权登录",
    "user.authorize.gitee": "Gitee授权登录",
    "user.logout": "退出登录",
    "user.logoutMsg": "是否确认退出?",
}