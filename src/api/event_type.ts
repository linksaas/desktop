//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import type { PluginEvent } from './events';
import type { LinkInfo } from '@/stores/linkAux';
import { LinkNoneInfo } from '@/stores/linkAux';
import * as pi from './project_issue';

import { type AllProjectEvent, get_project_simple_content } from './events/project';
import { type AllIssueEvent, get_issue_simple_content } from './events/issue';
import { type AllCodeEvent, get_code_simple_content } from './events/code';
import { type AllRequirementEvent, get_requirement_simple_content } from './events/requirement';
import { type AllIdeaEvent, get_idea_simple_content } from './events/idea';
import { type AllEntryEvent, get_entry_simple_content } from './events/entry';
import { type AllTestCaseEvent, get_testcase_simple_content } from './events/testcase';

export function get_issue_type_str(issue_type: number): string {
  if (issue_type == pi.ISSUE_TYPE_BUG) {
    return '缺陷';
  } else if (issue_type == pi.ISSUE_TYPE_TASK) {
    return '任务';
  } else {
    return '';
  }
}

export class AllEvent {
  ProjectEvent?: AllProjectEvent;
  IssueEvent?: AllIssueEvent;
  RequirementEvent?: AllRequirementEvent;
  CodeEvent?: AllCodeEvent;
  IdeaEvent?: AllIdeaEvent;
  EntryEvent?: AllEntryEvent;
  TestCaseEvent?: AllTestCaseEvent;
}

export function get_simple_content(ev: PluginEvent, skip_prj_name: boolean): LinkInfo[] {
  if (ev.event_data.ProjectEvent !== undefined) {
    return get_project_simple_content(ev, skip_prj_name, ev.event_data.ProjectEvent);
  } else if (ev.event_data.IssueEvent !== undefined) {
    return get_issue_simple_content(ev, skip_prj_name, ev.event_data.IssueEvent);
  } else if (ev.event_data.RequirementEvent !== undefined) {
    return get_requirement_simple_content(ev, skip_prj_name, ev.event_data.RequirementEvent);
  } else if (ev.event_data.CodeEvent !== undefined) {
    return get_code_simple_content(ev, skip_prj_name, ev.event_data.CodeEvent);
  } else if (ev.event_data.IdeaEvent !== undefined) {
    return get_idea_simple_content(ev, skip_prj_name, ev.event_data.IdeaEvent);
  } else if (ev.event_data.EntryEvent !== undefined) {
    return get_entry_simple_content(ev, skip_prj_name, ev.event_data.EntryEvent);
  } else if (ev.event_data.TestCaseEvent !== undefined) {
    return get_testcase_simple_content(ev, skip_prj_name, ev.event_data.TestCaseEvent);
  } 
  return [new LinkNoneInfo('未知事件')];
}
