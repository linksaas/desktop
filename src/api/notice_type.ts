//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import type { LinkInfo } from "@/stores/linkAux";
import type { COMMENT_TARGET_TYPE } from "./project_comment";
import type { WATCH_TARGET_TYPE } from "./project_watch";

/* eslint-disable @typescript-eslint/no-namespace */

export namespace user {
  export type UserMailNotice = {
    user_id: string;
  };

  export type AllNotice = {
    UserMailNotice?: UserMailNotice;
  }
}

export namespace project {
  export type UpdateProjectNotice = {
    project_id: string;
  };
  export type RemoveProjectNotice = {
    project_id: string;
  };
  export type AddMemberNotice = {
    project_id: string;
    member_user_id: string;
  };
  export type UpdateMemberNotice = {
    project_id: string;
    member_user_id: string;
  };
  export type RemoveMemberNotice = {
    project_id: string;
    member_user_id: string;
  };

  export type NewEventNotice = {
    project_id: string;
    member_user_id: string;
    event_id: string;
  };
  export type SetMemberRoleNotice = {
    project_id: string;
    member_user_id: string;
    role_id: string;
  }

  export type UpdateShortNoteNotice = {
    project_id: string;
    member_user_id: string;
  };

  export type UpdateAlarmStatNotice = {
    project_id: string;
  };

  export type CreateBulletinNotice = {
    project_id: string;
    bulletin_id: string;
    create_user_id: string;
  }

  export type UpdateBulletinNotice = {
    project_id: string;
    bulletin_id: string;
  }

  export type RemoveBulletinNotice = {
    project_id: string;
    bulletin_id: string;
  }

  export type AddTagNotice = {
    project_id: string;
    tag_id: string;
  };

  export type UpdateTagNotice = {
    project_id: string;
    tag_id: string;
  };

  export type RemoveTagNotice = {
    project_id: string;
    tag_id: string;
  };

  export type UpdateSpritNotice = {
    project_id: string;
    sprit_id: string;
  };


  export type CreateReviewNotice = {
    project_id: string;
    review_id: string;
    create_user_id: string;
  };

  export type UpdateReviewNotice = {
    project_id: string;
    review_id: string;
  };

  export type RemoveReviewNotice = {
    project_id: string;
    review_id: string;
  };

  export type AllNotice = {
    UpdateProjectNotice?: UpdateProjectNotice;
    RemoveProjectNotice?: RemoveProjectNotice;
    AddMemberNotice?: AddMemberNotice;
    UpdateMemberNotice?: UpdateMemberNotice;
    RemoveMemberNotice?: RemoveMemberNotice;
    NewEventNotice?: NewEventNotice;
    SetMemberRoleNotice?: SetMemberRoleNotice;
    UpdateShortNoteNotice?: UpdateShortNoteNotice;
    UpdateAlarmStatNotice?: UpdateAlarmStatNotice;
    CreateBulletinNotice?: CreateBulletinNotice;
    UpdateBulletinNotice?: UpdateBulletinNotice;
    RemoveBulletinNotice?: RemoveBulletinNotice;
    AddTagNotice?: AddTagNotice;
    UpdateTagNotice?: UpdateTagNotice;
    RemoveTagNotice?: RemoveTagNotice;
    UpdateSpritNotice?: UpdateSpritNotice;
    CreateReviewNotice?: CreateReviewNotice;
    UpdateReviewNotice?: UpdateReviewNotice;
    RemoveReviewNotice?: RemoveReviewNotice;
  };
}

export namespace project_doc {
  export type LinkSpritNotice = {
    project_id: string;
    doc_id: string;
    sprit_id: string;
  };

  export type CancelLinkSpritNotice = {
    project_id: string;
    doc_id: string;
    sprit_id: string;
  };
}

export namespace issue {
  export type NewIssueNotice = {
    project_id: string;
    issue_id: string;
    create_user_id: string;
  };

  export type UpdateIssueNotice = {
    project_id: string;
    issue_id: string;
  };

  export type RemoveIssueNotice = {
    project_id: string;
    issue_id: string;
  };

  export type SetSpritNotice = {
    project_id: string;
    issue_id: string;
    old_sprit_id: string;
    new_sprit_id: string;
  };

  export type UpdateIssueDepNotice = {
    project_id: string;
    issue_id: string;
    dep_issue_id: string;
  };

  export type UpdateSubIssueNotice = {
    project_id: string;
    issue_id: string;
  };

  export type AllNotice = {
    NewIssueNotice?: NewIssueNotice;
    RemoveIssueNotice?: RemoveIssueNotice;
    UpdateIssueNotice?: UpdateIssueNotice;
    SetSpritNotice?: SetSpritNotice;
    UpdateIssueDepNotice?: UpdateIssueDepNotice;
    UpdateSubIssueNotice?: UpdateSubIssueNotice;
  };
}

export namespace entry {

  export type NewEntryNotice = {
    project_id: string;
    entry_id: string;
    create_user_id: string;
  };

  export type UpdateEntryNotice = {
    project_id: string;
    entry_id: string;
  };

  export type RemoveEntryNotice = {
    project_id: string;
    entry_id: string;
  };

  export type AllNotice = {
    NewEntryNotice?: NewEntryNotice;
    UpdateEntryNotice?: UpdateEntryNotice;
    RemoveEntryNotice?: RemoveEntryNotice;
  }
}

export namespace requirement {
  export type NewRequirementNotice = {
    project_id: string;
    requirement_id: string;
    create_user_id: string;
  };

  export type UpdateRequirementNotice = {
    project_id: string;
    requirement_id: string;
  };

  export type RemoveRequirementNotice = {
    project_id: string;
    requirement_id: string;
  };

  export type LinkIssueNotice = {
    project_id: string;
    requirement_id: string;
    issue_id: string;
  };

  export type UnlinkIssueNotice = {
    project_id: string;
    requirement_id: string;
    issue_id: string;
  };

  export type AllNotice = {
    NewRequirementNotice?: NewRequirementNotice;
    UpdateRequirementNotice?: UpdateRequirementNotice;
    RemoveRequirementNotice?: RemoveRequirementNotice;
    LinkIssueNotice?: LinkIssueNotice;
    UnlinkIssueNotice?: UnlinkIssueNotice;
  };
}

export namespace testcase {
  export type NewFolderNotice = {
    project_id: string;
    folder_id: string;
    create_user_id: string;
  };

  export type UpdateFolderNotice = {
    project_id: string;
    folder_id: string;
  };

  export type RemoveFolderNotice = {
    project_id: string;
    folder_id: string;
  };

  export type NewCaseNotice = {
    project_id: string;
    case_id: string;
    create_user_id: string;
  };

  export type UpdateCaseNotice = {
    project_id: string;
    case_id: string;
  };

  export type RemoveCaseNotice = {
    project_id: string;
    case_id: string;
  };

  export type LinkSpritNotice = {
    project_id: string;
    case_id: string;
    sprit_id: string;
  };

  export type UnlinkSpritNotice = {
    project_id: string;
    case_id: string;
    sprit_id: string;
  };

  export type AllNotice = {
    NewFolderNotice?: NewFolderNotice;
    UpdateFolderNotice?: UpdateFolderNotice;
    RemoveFolderNotice?: RemoveFolderNotice;
    NewCaseNotice?: NewCaseNotice;
    UpdateCaseNotice?: UpdateCaseNotice;
    RemoveCaseNotice?: RemoveCaseNotice;
    LinkSpritNotice?: LinkSpritNotice;
    UnlinkSpritNotice?: UnlinkSpritNotice;
  };
}

export namespace org {
  export type JoinOrgNotice = {
    org_id: string;
    member_user_id: string;
  };

  export type LeaveOrgNotice = {
    org_id: string;
    member_user_id: string;
  };

  export type UpdateOrgNotice = {
    org_id: string;
  };

  export type CreateDepartMentNotice = {
    org_id: string;
    depart_ment_id: string;
  };

  export type RemoveDepartMentNotice = {
    org_id: string;
    depart_ment_id: string;
  };

  export type UpdateDepartMentNotice = {
    org_id: string;
    depart_ment_id: string;
  };

  export type UpdateMemberNotice = {
    org_id: string;
    member_user_id: string;
  };

  export type AllNotice = {
    JoinOrgNotice?: JoinOrgNotice;
    LeaveOrgNotice?: LeaveOrgNotice;
    UpdateOrgNotice?: UpdateOrgNotice;
    CreateDepartMentNotice?: CreateDepartMentNotice;
    RemoveDepartMentNotice?: RemoveDepartMentNotice;
    UpdateDepartMentNotice?: UpdateDepartMentNotice;
    UpdateMemberNotice?: UpdateMemberNotice;
  };
}

export namespace triger {
  export type CreateTrigerNotice = {
    project_id: string;
    triger_id: string;
  };

  export type UpdateTrigerNotice = {
    project_id: string;
    triger_id: string;
  };

  export type RemoveTrigerNotice = {
    project_id: string;
    triger_id: string;
  };

  export type AllNotice = {
    CreateTrigerNotice?: CreateTrigerNotice;
    UpdateTrigerNotice?: UpdateTrigerNotice;
    RemoveTrigerNotice?: RemoveTrigerNotice;
  };
}

export namespace client {
  export type WrongSessionNotice = {
    name: string;
  };

  export type StartMinAppNotice = {
    min_app_id: string;
  };

  export type OpenLinkInfoNotice = {
    link_info: LinkInfo;
  }

  //本地API打开内容的通知
  export type OpenEntryNotice = {
    project_id: string;
    entry_id: string;
  }

  export type NewExtraTokenNotice = {
    extra_token: string;
  };

  export type AtomGitLoginNotice = {
    code: string;
  };

  export type GitCodeLoginNotice = {
    code: string;
  };

  export type GiteeLoginNotice = {
    code: string;
  };

  export type LocalRepoChangeNotice = {
    repoId: string;
  }

  export type WatchChangeNotice = {
    targetType: WATCH_TARGET_TYPE;
    targetId: string;
  };

  export type UpdateUserMemoNotice = {
    memoId: string;
  };

  export type UserTodoChangeNotice = {};

  export type TodoConfigChangeNotice = {
    userFlag: boolean;
    issueFlag: boolean;
  };

  export type GitTodoCountNotice = {
    count: number;
  };

  export type CheckUpdateNotice = {};

  export type UpdateMailStateNotice = {
    mailId: string;
    hasRead: boolean;
  };

  export type RemoveMailNotice = {
    mailId: string;
  };

  export type AccessRoadmapNotice = {
    roadmapId: string;
    doneTopicCount: number;
    totalTopicCount: number;
    doneSubTopicCount: number;
    totalSubTopicCount: number;
  };

  export type AllNotice = {
    WrongSessionNotice?: WrongSessionNotice;
    StartMinAppNotice?: StartMinAppNotice;
    OpenLinkInfoNotice?: OpenLinkInfoNotice;
    OpenEntryNotice?: OpenEntryNotice;

    NewExtraTokenNotice?: NewExtraTokenNotice;
    AtomGitLoginNotice?: AtomGitLoginNotice;
    GitCodeLoginNotice?: GitCodeLoginNotice;
    GiteeLoginNotice?: GiteeLoginNotice;

    LocalRepoChangeNotice?: LocalRepoChangeNotice;
    WatchChangeNotice?: WatchChangeNotice;
    UpdateUserMemoNotice?: UpdateUserMemoNotice;

    UserTodoChangeNotice?: UserTodoChangeNotice;
    TodoConfigChangeNotice?: TodoConfigChangeNotice;
    GitTodoCountNotice?: GitTodoCountNotice;

    CheckUpdateNotice?: CheckUpdateNotice;

    UpdateMailStateNotice?: UpdateMailStateNotice;
    RemoveMailNotice?: RemoveMailNotice;

    AccessRoadmapNotice?: AccessRoadmapNotice;
  };
}

export namespace idea {
  export type CreateGroupNotice = {
    project_id: string;
  };

  export type UpdateGroupNotice = {
    project_id: string;
    idea_group_id: string;
  }

  export type RemoveGroupNotice = {
    project_id: string;
    idea_group_id: string;
  };

  export type CreateIdeaNotice = {
    project_id: string;
    idea_group_id: string;
  };

  export type UpdateIdeaNotice = {
    project_id: string;
    idea_id: string;
  };

  export type RemoveIdeaNotice = {
    project_id: string;
    idea_id: string;
  };

  export type MoveIdeaNotice = {
    project_id: string;
    idea_id: string;
    from_idea_group_id: string;
    to_idea_group_id: string;
  };

  export type ClearGroupNotice = {
    project_id: string;
    idea_group_id: string;
  }

  export type AllNotice = {
    CreateGroupNotice?: CreateGroupNotice;
    UpdateGroupNotice?: UpdateGroupNotice;
    RemoveGroupNotice?: RemoveGroupNotice;
    CreateIdeaNotice?: CreateIdeaNotice;
    UpdateIdeaNotice?: UpdateIdeaNotice;
    RemoveIdeaNotice?: RemoveIdeaNotice;
    MoveIdeaNotice?: MoveIdeaNotice;
    ClearGroupNotice?: ClearGroupNotice;
  };
}

export namespace comment {
  export type AddCommentNotice = {
    project_id: string;
    comment_id: string;
    target_type: COMMENT_TARGET_TYPE;
    target_id: string;

  };

  export type UpdateCommentNotice = {
    project_id: string;
    comment_id: string;
    target_type: COMMENT_TARGET_TYPE;
    target_id: string;
  };

  export type RemoveCommentNotice = {
    project_id: string;
    comment_id: string;
    target_type: COMMENT_TARGET_TYPE;
    target_id: string;
  };

  export type RemoveUnReadNotice = {
    project_id: string;
  };

  export type AllNotice = {
    AddCommentNotice?: AddCommentNotice;
    UpdateCommentNotice?: UpdateCommentNotice;
    RemoveCommentNotice?: RemoveCommentNotice;
    RemoveUnReadNotice?: RemoveUnReadNotice;
  };
}

export namespace dataview {
  export type CreateViewNotice = {
    project_id: string;
    view_id: string;
  };

  export type UpdateViewNotice = {
    project_id: string;
    view_id: string;
  };

  export type RemoveViewNotice = {
    project_id: string;
    view_id: string;
  };

  export type CreateNodeNotice = {
    project_id: string;
    view_id: string;
    node_id: string;
  };

  export type UpdateNodeNotice = {
    project_id: string;
    view_id: string;
    node_id: string;
  };

  export type RemoveNodeNotice = {
    project_id: string;
    view_id: string;
    node_id: string;
  };

  export type EdgeKey = {
    from_node_id: string;
    from_handle_id: string;
    to_node_id: string;
    to_handle_id: string;
  };

  export type CreateEdgeNotice = {
    project_id: string;
    view_id: string;
    edge_key: EdgeKey;
  };

  export type UpdateEdgeNotice = {
    project_id: string;
    view_id: string;
    edge_key: EdgeKey;
  };

  export type RemoveEdgeNotice = {
    project_id: string;
    view_id: string;
    edge_key: EdgeKey;
  };

  export type NewChatMsgNotice = {
    project_id: string;
    view_id: string;
    msg_id: string;
    send_user_id: string;
  };

  export type UpdateChatMsgNotice = {
    project_id: string;
    view_id: string;
    msg_id: string;
  };

  export type AllNotice = {
    CreateViewNotice?: CreateViewNotice;
    UpdateViewNotice?: UpdateViewNotice;
    RemoveViewNotice?: RemoveViewNotice;
    CreateNodeNotice?: CreateNodeNotice;
    UpdateNodeNotice?: UpdateNodeNotice;
    RemoveNodeNotice?: RemoveNodeNotice;
    CreateEdgeNotice?: CreateEdgeNotice;
    UpdateEdgeNotice?: UpdateEdgeNotice;
    RemoveEdgeNotice?: RemoveEdgeNotice;
    NewChatMsgNotice?: NewChatMsgNotice;
    UpdateChatMsgNotice?: UpdateChatMsgNotice;
  };
}


export type AllNotice = {
  UserNotice?: user.AllNotice;
  ProjectNotice?: project.AllNotice;
  IssueNotice?: issue.AllNotice;
  ClientNotice?: client.AllNotice;
  IdeaNotice?: idea.AllNotice;
  CommentNotice?: comment.AllNotice;
  EntryNotice?: entry.AllNotice;
  RequirementNotice?: requirement.AllNotice;
  TestcaseNotice?: testcase.AllNotice;
  OrgNotice?: org.AllNotice;
  TrigerNotice?: triger.AllNotice;
  DataviewNotice?: dataview.AllNotice;
};
