//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export type SOURCE_TYPE = number;

export const SOURCE_TYPE_ATOMGIT: SOURCE_TYPE = 0;
export const SOURCE_TYPE_GITCODE: SOURCE_TYPE = 1;
export const SOURCE_TYPE_GITEE: SOURCE_TYPE = 2;
export const SOURCE_TYPE_GITLAB: SOURCE_TYPE = 3;

export const SOURCE_TYPE_TEN_COLOUD = 100; //腾讯云

export type UserDataSourceInfo = {
    id: string;
    name: string;
    source_type: SOURCE_TYPE;
    source_info: string;
};

export type UserDataViewInfo = {
    id: string,
    name: string,
    desc: string,
    bg_color: string,
};

export type GitSourceInfo = {
    accessToken: string;
    baseUrl?: string; //SOURE_TYPE_GITLAB时会使用
}

export type TenCloudSourceInfo = {
    secretId: string;
    secretKey: string;

    regionList: string[];

    enableLightHouse: boolean;
    enableDns: boolean;
    enableCdn: boolean;
}

export type SourceInfoType = GitSourceInfo | TenCloudSourceInfo;


export function get_type_name(sourceType: SOURCE_TYPE): string {
    if (sourceType == SOURCE_TYPE_ATOMGIT) {
        return "AtomGit";
    } else if (sourceType == SOURCE_TYPE_GITCODE) {
        return "GitCode";
    } else if (sourceType == SOURCE_TYPE_GITEE) {
        return "Gitee";
    } else if (sourceType == SOURCE_TYPE_GITLAB) {
        return "GitLab";
    } else if (sourceType == SOURCE_TYPE_TEN_COLOUD) {
        return "腾讯云";
    }
    return "";
}

//增加数据源
export async function add_source(source: UserDataSourceInfo): Promise<void> {
    return invoke<void>("plugin:user_dataview_api|add_source", {
        source,
    });
}

//更新数据源
export async function update_source(source: UserDataSourceInfo): Promise<void> {
    return invoke<void>("plugin:user_dataview_api|update_source", {
        source,
    });
}

//删除数据源
export async function remove_source(id: string): Promise<void> {
    return invoke<void>("plugin:user_dataview_api|remove_source", {
        id,
    });
}

//列出数据源
export async function list_source(): Promise<UserDataSourceInfo[]> {
    return invoke<UserDataSourceInfo[]>("plugin:user_dataview_api|list_source", {});
}

//获取数据源
export async function get_source(id: string): Promise<UserDataSourceInfo | undefined> {
    const tmpList = await list_source();
    return tmpList.find(item => item.id == id);
}

//加载节点数据
export async function load_node_data(id: string): Promise<string> {
    return invoke<string>("plugin:user_dataview_api|load_node_data", { id });
}

//保存节点数据
export async function save_node_data(data: string, id: string): Promise<void> {
    return invoke<void>("plugin:user_dataview_api|save_node_data", { data, id });
}

//增加视图
export async function add_view(view: UserDataViewInfo): Promise<void> {
    return invoke<void>("plugin:user_dataview_api|add_view", {
        view,
    });
}

//更新视图
export async function update_view(view: UserDataViewInfo): Promise<void> {
    return invoke<void>("plugin:user_dataview_api|update_view", {
        view,
    });
}

//删除视图
export async function remove_view(id: string): Promise<void> {
    return invoke<void>("plugin:user_dataview_api|remove_view", {
        id,
    });
}

//列出视图
export async function list_view(): Promise<UserDataViewInfo[]> {
    return invoke<UserDataViewInfo[]>("plugin:user_dataview_api|list_view", {});
}