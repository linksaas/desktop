//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';


export type ProjectLinkInfo = {
    yaml_content: string;
};
//读取git hooks
export async function get_link_info(git_path: string): Promise<ProjectLinkInfo> {
    const request = {
        gitPath: git_path,
    }
    const cmd = 'plugin:project_tool_api|get_link_info';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ProjectLinkInfo>(cmd, request);
}

//设置git hooks
export async function set_link_info(
    git_path: string,
    project_id: string,
): Promise<void> {
    const request = {
        gitPath: git_path,
        projectId: project_id,
    };
    const cmd = 'plugin:project_tool_api|set_link_info';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<void>(cmd, request);
}