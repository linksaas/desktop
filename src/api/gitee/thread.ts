//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { fetch } from '@tauri-apps/api/http';

// {
//     "id": 811554105,
//     "content": "朱梓 更改了缺陷 #IAQ8ZK 使用tauri时复杂页面会无响应 的状态为 已确认",
//     "type": "issue_state_change",
//     "unread": true,
//     "mute": false,
//     "updated_at": "2024-10-14T12:08:12+08:00",
//     "url": "https://gitee.com/api/v5/notification/threads/811554105",
//     "html_url": "https://gitee.com/openkylin/webkit2gtk/issues/IAQ8ZK",
//     "actor": {
//         "id": 11841721,
//         "login": "grass-eating-in-the-world",
//         "name": "朱梓",
//         "avatar_url": "https://foruda.gitee.com/avatar/1701936024720309859/11841721_grass-eating-in-the-world_1701936024.png",
//         "url": "https://gitee.com/api/v5/users/grass-eating-in-the-world",
//         "html_url": "https://gitee.com/grass-eating-in-the-world",
//         "remark": "",
//         "followers_url": "https://gitee.com/api/v5/users/grass-eating-in-the-world/followers",
//         "following_url": "https://gitee.com/api/v5/users/grass-eating-in-the-world/following_url{/other_user}",
//         "gists_url": "https://gitee.com/api/v5/users/grass-eating-in-the-world/gists{/gist_id}",
//         "starred_url": "https://gitee.com/api/v5/users/grass-eating-in-the-world/starred{/owner}{/repo}",
//         "subscriptions_url": "https://gitee.com/api/v5/users/grass-eating-in-the-world/subscriptions",
//         "organizations_url": "https://gitee.com/api/v5/users/grass-eating-in-the-world/orgs",
//         "repos_url": "https://gitee.com/api/v5/users/grass-eating-in-the-world/repos",
//         "events_url": "https://gitee.com/api/v5/users/grass-eating-in-the-world/events{/privacy}",
//         "received_events_url": "https://gitee.com/api/v5/users/grass-eating-in-the-world/received_events",
//         "type": "User"
//     },
//     "repository": {
//         "id": 22428845,
//         "full_name": "openkylin/webkit2gtk",
//         "human_name": "openKylin/webkit2gtk",
//         "url": "https://gitee.com/api/v5/repos/openkylin/webkit2gtk",
//         "namespace": {
//             "id": 8380244,
//             "type": "group",
//             "name": "openKylin",
//             "path": "openkylin",
//             "html_url": "https://gitee.com/openkylin"
//         },
//         "path": "webkit2gtk",
//         "name": "webkit2gtk",
//         "owner": {
//             "id": 9637903,
//             "login": "jiangwei124",
//             "name": "姜威",
//             "avatar_url": "https://foruda.gitee.com/avatar/1695607353279231791/9637903_jiangwei124_1695607353.png",
//             "url": "https://gitee.com/api/v5/users/jiangwei124",
//             "html_url": "https://gitee.com/jiangwei124",
//             "remark": "",
//             "followers_url": "https://gitee.com/api/v5/users/jiangwei124/followers",
//             "following_url": "https://gitee.com/api/v5/users/jiangwei124/following_url{/other_user}",
//             "gists_url": "https://gitee.com/api/v5/users/jiangwei124/gists{/gist_id}",
//             "starred_url": "https://gitee.com/api/v5/users/jiangwei124/starred{/owner}{/repo}",
//             "subscriptions_url": "https://gitee.com/api/v5/users/jiangwei124/subscriptions",
//             "organizations_url": "https://gitee.com/api/v5/users/jiangwei124/orgs",
//             "repos_url": "https://gitee.com/api/v5/users/jiangwei124/repos",
//             "events_url": "https://gitee.com/api/v5/users/jiangwei124/events{/privacy}",
//             "received_events_url": "https://gitee.com/api/v5/users/jiangwei124/received_events",
//             "type": "User"
//         },
//         "assigner": {
//             "id": 10071196,
//             "login": "openkylin-cibot",
//             "name": "openKylin CI Bot",
//             "avatar_url": "https://foruda.gitee.com/avatar/1677197034551167067/10071196_openkylin-cibot_1656900805.png",
//             "url": "https://gitee.com/api/v5/users/openkylin-cibot",
//             "html_url": "https://gitee.com/openkylin-cibot",
//             "remark": "",
//             "followers_url": "https://gitee.com/api/v5/users/openkylin-cibot/followers",
//             "following_url": "https://gitee.com/api/v5/users/openkylin-cibot/following_url{/other_user}",
//             "gists_url": "https://gitee.com/api/v5/users/openkylin-cibot/gists{/gist_id}",
//             "starred_url": "https://gitee.com/api/v5/users/openkylin-cibot/starred{/owner}{/repo}",
//             "subscriptions_url": "https://gitee.com/api/v5/users/openkylin-cibot/subscriptions",
//             "organizations_url": "https://gitee.com/api/v5/users/openkylin-cibot/orgs",
//             "repos_url": "https://gitee.com/api/v5/users/openkylin-cibot/repos",
//             "events_url": "https://gitee.com/api/v5/users/openkylin-cibot/events{/privacy}",
//             "received_events_url": "https://gitee.com/api/v5/users/openkylin-cibot/received_events",
//             "type": "User"
//         },
//         "description": "",
//         "private": false,
//         "public": true,
//         "internal": false,
//         "fork": false,
//         "html_url": "https://gitee.com/openkylin/webkit2gtk.git",
//         "ssh_url": "git@gitee.com:openkylin/webkit2gtk.git"
//     },
//     "subject": {
//         "title": "使用tauri时复杂页面会无响应",
//         "url": "https://gitee.com/api/v5/repos/openkylin/webkit2gtk/issues/IAQ8ZK",
//         "latest_comment_url": "https://gitee.com/api/v5/repos/openkylin/webkit2gtk/issues/comments/31895872",
//         "type": "Issue"
//     },
//     "namespaces": [
//         {
//             "name": "openKylin/webkit2gtk",
//             "html_url": "https://gitee.com/openkylin/webkit2gtk",
//             "type": "project"
//         }
//     ]
// }

export interface GiteeThreadItem {
    id: number;
    content: string;
    type: string;
    unread: boolean;
    html_url: string;
    updated_at: string;
}

export interface GiteeThread {
    total_count: number;
    list: GiteeThreadItem[] | null | undefined;
}

export async function list_thread(accessToken: string):Promise<GiteeThread> {
    const url = `https://gitee.com/api/v5/notifications/threads?access_token=${accessToken}&unread=true&per_page=100`;
    const res = await fetch<GiteeThread>(url, {
        method: "GET",
        timeout: 10,
    });
    if (res.ok && res.status == 200) {
        return res.data;
    } else {
        console.log(res);
        throw "error list my threads";
    }
}