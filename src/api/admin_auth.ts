//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';


export type Signature = {
    format: string;
    blob: number[];
    rest: number[];
};

export type UserPerm = {
    read: boolean;
    create: boolean;
    set_state: boolean;
    set_test_account: boolean;
    reset_password: boolean;
};

export type ProjectPerm = {
    read: boolean;
    update: boolean;
    access_event: boolean;
};

export type ProjectMemberPerm = {
    read: boolean;
};

export type MenuPerm = {
    read: boolean;
    add: boolean;
    remove: boolean;
    update: boolean;
};

export type AppStorePerm = {
    read: boolean;
    add_cate: boolean;
    update_cate: boolean;
    remove_cate: boolean;
    add_app: boolean;
    update_app: boolean;
    remove_app: boolean;
};

export type IdeaStorePerm = {
    read: boolean;
    create_store_cate: boolean;
    update_store_cate: boolean;
    remove_store_cate: boolean;
    create_store: boolean;
    update_store: boolean;
    move_store: boolean;
    remove_store: boolean;
    create_idea: boolean;
    update_idea: boolean;
    move_idea: boolean;
    remove_idea: boolean;
};

export type WidgetStorePerm = {
    read: boolean;
    add_widget: boolean;
    update_widget: boolean;
    remove_widget: boolean;
};

export type SwStorePerm = {
    read: boolean;
    add_cate: boolean;
    update_cate: boolean;
    remove_cate: boolean;
    add_soft_ware: boolean;
    update_soft_ware: boolean;
    remove_soft_ware: boolean;
};

export type OrgPerm = {
    read: boolean;
    update: boolean;
};

export type OrgMemberPerm = {
    read: boolean;
}

export type KeywordPerm = {
    read: boolean;
    add: boolean;
    remove: boolean;
};

export type FeedBackPerm = {
    read: boolean;
    remove: boolean;
};

export type RoadmapPerm = {
    read: boolean;
    create: boolean;
    update_content: boolean;
    update_pub_state: boolean;
    remove: boolean;
    change_owner: boolean;
    add_white_user: boolean;
    update_white_user: boolean;
    remove_white_user: boolean;
    add_tag: boolean;
    modify_tag: boolean;
    remove_tag: boolean;
    update_tag: boolean;
};

export type AdminPermInfo = {
    user_perm: UserPerm;
    project_perm: ProjectPerm;
    project_member_perm: ProjectMemberPerm;
    menu_perm: MenuPerm;
    app_store_perm: AppStorePerm;
    idea_store_perm: IdeaStorePerm;
    widget_store_perm: WidgetStorePerm;
    sw_store_perm: SwStorePerm;
    org_perm: OrgPerm;
    org_member_perm: OrgMemberPerm;
    keyword_perm: KeywordPerm;
    feed_back_perm: FeedBackPerm;
    roadmap_perm: RoadmapPerm;
    super_admin_user: boolean;
};

export type PreAuthRequest = {
    user_name: string;
};

export type PreAuthResponse = {
    code: number;
    err_msg: string;
    admin_session_id: string;
    to_sign_str: string;
};


export type AuthRequest = {
    admin_session_id: string;
    sign: Signature;
};

export type AuthResponse = {
    code: number;
    err_msg: string;
    admin_perm_info: AdminPermInfo;
    global_server: boolean;
};

export async function pre_auth(request: PreAuthRequest): Promise<PreAuthResponse> {
    const cmd = 'plugin:admin_auth_api|pre_auth';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<PreAuthResponse>(cmd, {
        request,
    });
}

export async function auth(request: AuthRequest): Promise<AuthResponse> {
    const cmd = 'plugin:admin_auth_api|auth';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    const ret = await invoke<AuthResponse>(cmd, {
        request,
    });
    return ret;
}

//获取当前管理会话ID
export async function get_admin_session(): Promise<string> {
    const cmd = 'plugin:admin_auth_api|get_admin_session';
    return invoke<string>(cmd, {});
}

//获取当前管理会话权限
export async function get_admin_perm(): Promise<AdminPermInfo | null> {
    const cmd = 'plugin:admin_auth_api|get_admin_perm';
    const perm = await invoke<AdminPermInfo>(cmd, {});
    return perm;
}

//检测是否是全局服务器
export async function is_global_server(): Promise<boolean> {
    const cmd = 'plugin:admin_auth_api|is_global_server';
    const globalServer = await invoke<boolean>(cmd, {});
    return globalServer ?? false;
}

//用私钥对内容签名
export async function sign(privateKeyFile: string, toSignStr: string): Promise<Signature> {
    const cmd = 'plugin:admin_auth_api|sign';
    const request = {
        privateKeyFile,
        toSignStr,
    };
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<Signature>(cmd, request);
}