//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export type OS_SCOPE = number;
export const OS_SCOPE_WINDOWS: OS_SCOPE = 0;
export const OS_SCOPE_MAC: OS_SCOPE = 1;
export const OS_SCOPE_LINUX: OS_SCOPE = 2;

export type SORT_KEY = number;
export const SORT_KEY_UPDATE_TIME: SORT_KEY = 0;
export const SORT_KEY_INSTALL_COUNT: SORT_KEY = 1;
export const SORT_KEY_AGREE_COUNT: SORT_KEY = 2;

export type APP_TYPE = number;
export const APP_TYPE_UNKWOWN: APP_TYPE = 0;
export const APP_TYPE_SSH: APP_TYPE = 1;
export const APP_TYPE_MYSQL: APP_TYPE = 2;
export const APP_TYPE_POSTGRES: APP_TYPE = 3;
export const APP_TYPE_MONGO: APP_TYPE = 4;
export const APP_TYPE_REDIS: APP_TYPE = 5;
export const APP_TYPE_GRPC: APP_TYPE = 6;
export const APP_TYPE_VNC: APP_TYPE = 7;

export const APP_TYPE_ALL: APP_TYPE = 99;

export type MajorCate = {
    cate_id: string;
    cate_name: string;
    minor_cate_count: number;
    app_count: number;
};

export type MinorCate = {
    cate_id: string;
    cate_name: string;
    sub_minor_cate_count: number;
    app_count: number;
};

export type SubMinorCate = {
    cate_id: string;
    cate_name: string;
    app_count: number;
};

export type BaseAppInfo = {
    app_name: string;
    app_desc: string;
    icon_file_id: string;
    src_url: string;
    app_type: APP_TYPE;
};

export type AppNetPerm = {
    cross_domain_http: boolean;
    proxy_redis: boolean;
    proxy_mysql: boolean;
    proxy_post_gres: boolean;
    proxy_mongo: boolean;
    proxy_ssh: boolean;
    net_util: boolean;
    proxy_grpc: boolean;
};

export type AppFsPerm = {
    read_file: boolean;
    write_file: boolean;
};

export type AppExtraPerm = {
    cross_origin_isolated: boolean;
    open_browser: boolean;
};

export type AppPerm = {
    net_perm: AppNetPerm;
    fs_perm: AppFsPerm;
    extra_perm: AppExtraPerm;
};

export type AppInfo = {
    app_id: string;
    base_info: BaseAppInfo;
    major_cate: MajorCate;
    minor_cate: MinorCate;
    sub_minor_cate: SubMinorCate;
    app_perm: AppPerm;
    file_id: string;
    os_windows: boolean;
    os_mac: boolean;
    os_linux: boolean;
    user_app: boolean;
    create_time: number;
    update_time: number;
    install_count: number;
    agree_count: number;
    my_agree: boolean;
};

export type ListAppParam = {
    filter_by_major_cate_id: boolean;
    major_cate_id: string;
    filter_by_minor_cate_id: boolean;
    minor_cate_id: string;
    filter_by_sub_minor_cate_id: boolean;
    sub_minor_cate_id: string;
    filter_by_os_scope: boolean;
    os_scope: OS_SCOPE;
    filter_by_keyword: boolean;
    keyword: string;

};

export type CatePath = {
    major_cate_id: string;
    minor_cate_id: string;
    sub_minor_cate_id: string;
};

// eslint-disable-next-line @typescript-eslint/ban-types
export type ListMajorCateRequest = {};

export type ListMajorCateResponse = {
    code: number;
    err_msg: string;
    cate_info_list: MajorCate[];
};


export type ListMinorCateRequest = {
    major_cate_id: string;
};

export type ListMinorCateResponse = {
    code: number;
    err_msg: string;
    cate_info_list: MinorCate[];
}


export type ListSubMinorCateRequest = {
    minor_cate_id: string;
};

export type ListSubMinorCateResponse = {
    code: number;
    err_msg: string;
    cate_info_list: SubMinorCate[];
};

export type GetCatePathRequest = {
    cate_id: string;
};

export type GetCatePathResponse = {
    code: number;
    err_msg: string;
    cate_path: CatePath;
};

export type ListAppRequest = {
    list_param: ListAppParam;
    session_id: string;
    offset: number;
    limit: number;
    sort_key: SORT_KEY;
};

export type ListAppResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    app_info_list: AppInfo[];
};

export type ListAppByIdRequest = {
    app_id_list: string[];
    session_id: string;
};

export type ListAppByIdResponse = {
    code: number;
    err_msg: string;
    app_info_list: AppInfo[];
}

export type ListAppByTypeRequest = {
    app_type: APP_TYPE;
    session_id: string;
};

export type ListAppByTypeResponse = {
    code: number;
    err_msg: string;
    app_info_list: AppInfo[];
};

export type GetAppRequest = {
    app_id: string;
    session_id: string;
};

export type GetAppResponse = {
    code: number;
    err_msg: string;
    app_info: AppInfo;
};

export type InstallAppRequest = {
    app_id: string;
};

export type InstallAppResponse = {
    code: number;
    err_msg: string;
};

export type QueryPermRequest = {
    app_id: string;
};
export type QueryPermResponse = {
    code: number;
    err_msg: string;
    app_perm: AppPerm;
};

export type AgreeAppRequest = {
    session_id: string;
    app_id: string;
};

export type AgreeAppResponse = {
    code: number;
    err_msg: string;
};

export type CancelAgreeAppRequest = {
    session_id: string;
    app_id: string;
};

export type CancelAgreeAppResponse = {
    code: number;
    err_msg: string;
};

//列出一级分类
export async function list_major_cate(addr: string, request: ListMajorCateRequest): Promise<ListMajorCateResponse> {
    const cmd = 'plugin:appstore_api|list_major_cate';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListMajorCateResponse>(cmd, {
        addr,
        request,
    });
}


//列出二级分类
export async function list_minor_cate(addr: string, request: ListMinorCateRequest): Promise<ListMinorCateResponse> {
    const cmd = 'plugin:appstore_api|list_minor_cate';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListMinorCateResponse>(cmd, {
        addr,
        request,
    });
}


//列出三级分类
export async function list_sub_minor_cate(addr: string, request: ListSubMinorCateRequest): Promise<ListSubMinorCateResponse> {
    const cmd = 'plugin:appstore_api|list_sub_minor_cate';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListSubMinorCateResponse>(cmd, {
        addr,
        request,
    });
}

//列出分类路径
export async function get_cate_path(addr: string, request: GetCatePathRequest): Promise<GetCatePathResponse> {
    const cmd = 'plugin:appstore_api|get_cate_path';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetCatePathResponse>(cmd, {
        addr,
        request,
    });
}


//列出应用
export async function list_app(addr: string, request: ListAppRequest): Promise<ListAppResponse> {
    const cmd = 'plugin:appstore_api|list_app';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListAppResponse>(cmd, {
        addr,
        request,
    });
}

//按Id列出应用
export async function list_app_by_id(addr: string, request: ListAppByIdRequest): Promise<ListAppByIdResponse> {
    const cmd = 'plugin:appstore_api|list_app_by_id';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListAppByIdResponse>(cmd, {
        addr,
        request,
    });
}

//根据类型列出应用
export async function list_app_by_type(addr: string, request: ListAppByTypeRequest): Promise<ListAppByTypeResponse> {
    const cmd = 'plugin:appstore_api|list_app_by_type';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListAppByTypeResponse>(cmd, {
        addr,
        request,
    });
}

//获取单个应用
export async function get_app(addr: string, request: GetAppRequest): Promise<GetAppResponse> {
    const cmd = 'plugin:appstore_api|get_app';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetAppResponse>(cmd, {
        addr,
        request,
    });
}

//安装应用(增加安装计数值)
export async function install_app(addr: string, request: InstallAppRequest): Promise<InstallAppResponse> {
    const cmd = 'plugin:appstore_api|install_app';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<InstallAppResponse>(cmd, {
        addr,
        request,
    });
}

//获取权限
export async function query_perm(addr: string, request: QueryPermRequest): Promise<QueryPermResponse> {
    const cmd = 'plugin:appstore_api|query_perm';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<QueryPermResponse>(cmd, {
        addr,
        request,
    });
}

//赞同应用
export async function agree_app(addr: string, request: AgreeAppRequest): Promise<AgreeAppResponse> {
    const cmd = 'plugin:appstore_api|agree_app';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AgreeAppResponse>(cmd, {
        addr,
        request,
    });
}

//取消赞同应用
export async function cancel_agree_app(addr: string, request: CancelAgreeAppRequest): Promise<CancelAgreeAppResponse> {
    const cmd = 'plugin:appstore_api|cancel_agree_app';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<CancelAgreeAppResponse>(cmd, {
        addr,
        request,
    });
}