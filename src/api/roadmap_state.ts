//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export type STATE_OP_TYPE = number;

export const STATE_OP_SET: STATE_OP_TYPE = 0;
export const STATE_OP_REMOVE: STATE_OP_TYPE = 1;

export type TOPIC_STATE_TYPE = number;

export const TOPIC_STATE_DOING: TOPIC_STATE_TYPE = 0;  //学习/执行中
export const TOPIC_STATE_DONE: TOPIC_STATE_TYPE = 1;   //已学会/掌握
export const TOPIC_STATE_SKIP: TOPIC_STATE_TYPE = 2;   //忽略

export type NodeTopicState = {
    state: TOPIC_STATE_TYPE
    title: string;
};

export type NodeTodoStateItem = {
    todo_id: string;
    title: string;
};

export type NodeTodoListState = {
    done_list: NodeTodoStateItem[];
};

export type UserNoteState = {
    title: string;
};

export type StateData = {
    TopicState?: NodeTopicState;
    TodoListState?: NodeTodoListState;
    UserNoteState?: UserNoteState;
};

export type BasicStateInfo = {
    state_data: StateData;
};

export type StateInfo = {
    roadmap_id: string;
    node_id: string;
    time_stamp: number;
    basic_info: BasicStateInfo;
};

export type StateOpItem = {
    op_id: string;
    roadmap_id: string;
    node_id: string;
    op_type: STATE_OP_TYPE;
    time_stamp: number;
    roadmap_title: string;
    user_id: string;
    user_display_name: string;
    user_logo_uri: string;
    roadmap_exist: boolean;
    can_update_roadmap: boolean;
    state_data: StateData;
};

export type SetStateRequest = {
    session_id: string;
    roadmap_id: string;
    node_id: string;
    basic_info: BasicStateInfo;
};

export type SetStateResponse = {
    code: number;
    err_msg: string;
};

export type ListStateRequest = {
    session_id: string;
    roadmap_id: string;
    filterby_node_id: boolean;
    node_id: string;
};

export type ListStateResponse = {
    code: number;
    err_msg: string;
    state_list: StateInfo[];
};

export type RemoveStateRequest = {
    session_id: string;
    roadmap_id: string;
    node_id: string;
};

export type RemoveStateResponse = {
    code: number;
    err_msg: string;
};

export type ListMyLogRequest = {
    session_id: string;
    offset: number;
    limit: number;
};

export type ListMyLogResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    op_list: StateOpItem[];
};

//设置状态
export async function set_state(request: SetStateRequest): Promise<SetStateResponse> {
    const cmd = 'plugin:roadmap_state_api|set_state';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<SetStateResponse>(cmd, {
        request,
    });
}

//列出状态
export async function list_state(request: ListStateRequest): Promise<ListStateResponse> {
    const cmd = 'plugin:roadmap_state_api|list_state';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListStateResponse>(cmd, {
        request,
    });
}

//删除状态
export async function remove_state(request: RemoveStateRequest): Promise<RemoveStateResponse> {
    const cmd = 'plugin:roadmap_state_api|remove_state';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveStateResponse>(cmd, {
        request,
    });
}

//列出日志(个人维度)
export async function list_my_log(request: ListMyLogRequest): Promise<ListMyLogResponse> {
    const cmd = 'plugin:roadmap_state_api|list_my_log';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListMyLogResponse>(cmd, {
        request,
    });
}