//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export type EVALUATE_STATE = number;
export const EVALUATE_STATE_DRAFT: EVALUATE_STATE = 0;     //草案阶段
export const EVALUATE_STATE_EVALUATE: EVALUATE_STATE = 1;  //评估阶段
export const EVALUATE_STATE_FINISH: EVALUATE_STATE = 2;    //评估结束

export type EvaluateTargetInfo = {
    target_id: string;
    target_name: string;
    evaluate_count: number;
};

export type EvaluateTargetScore = {
    target_id: string;
    target_name: string;
    score: number;
};

export type EvaluateMemberInfo = {
    member_user_id: string;
    member_display_name: string;
    member_logo_uri: string;
    score_list: EvaluateTargetScore[];
    has_evaluate: boolean;
};

export type MyEvaluateResult = {
    member_user_id: string;
    member_display_name: string;
    member_logo_uri: string;
    score_list: EvaluateTargetScore[];
};

export type MyEvaluateScore = {
    target_id: string;
    score: number;
};

export type MyEvaluateItem = {
    member_user_id: string;
    score_list: MyEvaluateScore[];
};

export type UserPerm = {
    can_update: boolean;
    can_remove: boolean;
    can_view: boolean;
};

export type EvaluateInfo = {
    evaluate_id: string;
    evaluate_title: string;
    evaluate_desc: string;
    member_list: EvaluateMemberInfo[];
    evaluate_state: EVALUATE_STATE;
    depart_ment_id: string;
    create_user_id: string;
    create_time: number;
    create_display_name: string;
    create_logo_uri: string;
    update_user_id: string;
    update_time: number;
    update_display_name: string;
    update_logo_uri: string;
    user_perm: UserPerm;
};

export type AddTargetRequest = {
    session_id: string;
    org_id: string;
    target_name: string;
};

export type AddTargetResponse = {
    code: number;
    err_msg: string;
    target_id: string;
};

export type ListTargetRequest = {
    session_id: string;
    org_id: string;
};

export type ListTargetResponse = {
    code: number;
    err_msg: string;
    target_list: EvaluateTargetInfo[];
};

export type UpdateTargetRequest = {
    session_id: string;
    org_id: string;
    target_id: string;
    target_name: string;
};

export type UpdateTargetResponse = {
    code: number;
    err_msg: string;
};

export type RemoveTargetRequest = {
    session_id: string;
    org_id: string;
    target_id: string;
};

export type RemoveTargetResponse = {
    code: number;
    err_msg: string;
};

export type CreateEvaluateRequest = {
    session_id: string;
    org_id: string;
    depart_ment_id: string;
    evaluate_title: string;
    evaluate_desc: string;
    member_user_id_list: string[];
    target_id_list: string[];
};

export type CreateEvaluateResponse = {
    code: number;
    err_msg: string;
    evaluate_id: string;
};

export type UpdateBaseForEvaluateRequest = {
    session_id: string;
    org_id: string;
    evaluate_id: string;
    evaluate_title: string;
    evaluate_desc: string;
    member_user_id_list: string[];
    target_id_list: string[];
};

export type UpdateBaseForEvaluateResponse = {
    code: number;
    err_msg: string;
};

export type UpdateStateForEvaluateRequest = {
    session_id: string;
    org_id: string;
    evaluate_id: string;
    state: EVALUATE_STATE;
};

export type UpdateStateForEvaluateResponse = {
    code: number;
    err_msg: string;
};

export type ListEvaluateRequest = {
    session_id: string;
    org_id: string;
    depart_ment_id: string;
    filter_member_user_id: boolean;
    member_user_id: string;
    offset: number;
    limit: number;
};

export type ListEvaluateResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    evaluate_list: EvaluateInfo[];
};

export type GetEvaluateRequest = {
    session_id: string;
    org_id: string;
    evaluate_id: string;
};

export type GetEvaluateResponse = {
    code: number;
    err_msg: string;
    evaluate: EvaluateInfo;
};

export type RemoveEvaluateRequest = {
    session_id: string;
    org_id: string;
    evaluate_id: string;
};

export type RemoveEvaluateResponse = {
    code: number;
    err_msg: string;
};

export type GetMyEvaluateRequest = {
    session_id: string;
    org_id: string;
    evaluate_id: string;
};

export type GetMyEvaluateResponse = {
    code: number;
    err_msg: string;
    result_list: MyEvaluateResult[];
};

export type UpdateMyEvaluateRequest = {
    session_id: string;
    org_id: string;
    evaluate_id: string;
    item_list: MyEvaluateItem[];
};

export type UpdateMyEvaluateResponse = {
    code: number;
    err_msg: string;
};


//增加评估目标
export async function add_target(request: AddTargetRequest): Promise<AddTargetResponse> {
    const cmd = 'plugin:org_evaluate_api|add_target';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AddTargetResponse>(cmd, {
        request,
    });
}

//列出评估目标
export async function list_target(request: ListTargetRequest): Promise<ListTargetResponse> {
    const cmd = 'plugin:org_evaluate_api|list_target';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListTargetResponse>(cmd, {
        request,
    });
}

//更新评估目标
export async function update_target(request: UpdateTargetRequest): Promise<UpdateTargetResponse> {
    const cmd = 'plugin:org_evaluate_api|update_target';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateTargetResponse>(cmd, {
        request,
    });
}

//删除评估目标
export async function remove_target(request: RemoveTargetRequest): Promise<RemoveTargetResponse> {
    const cmd = 'plugin:org_evaluate_api|remove_target';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveTargetResponse>(cmd, {
        request,
    });
}

//创建互评
export async function create_evaluate(request: CreateEvaluateRequest): Promise<CreateEvaluateResponse> {
    const cmd = 'plugin:org_evaluate_api|create_evaluate';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<CreateEvaluateResponse>(cmd, {
        request,
    });
}

//更新评估基本信息
export async function update_base_for_evaluate(request: UpdateBaseForEvaluateRequest): Promise<UpdateBaseForEvaluateResponse> {
    const cmd = 'plugin:org_evaluate_api|update_base_for_evaluate';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateBaseForEvaluateResponse>(cmd, {
        request,
    });
}

//更新评估状态
export async function update_state_for_evaluate(request: UpdateStateForEvaluateRequest): Promise<UpdateStateForEvaluateResponse> {
    const cmd = 'plugin:org_evaluate_api|update_state_for_evaluate';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateStateForEvaluateResponse>(cmd, {
        request,
    });
}

//列出评估
export async function list_evaluate(request: ListEvaluateRequest): Promise<ListEvaluateResponse> {
    const cmd = 'plugin:org_evaluate_api|list_evaluate';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListEvaluateResponse>(cmd, {
        request,
    });
}

//获得单个评估
export async function get_evaluate(request: GetEvaluateRequest): Promise<GetEvaluateResponse> {
    const cmd = 'plugin:org_evaluate_api|get_evaluate';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetEvaluateResponse>(cmd, {
        request,
    });
}

//删除评估
export async function remove_evaluate(request: RemoveEvaluateRequest): Promise<RemoveEvaluateResponse> {
    const cmd = 'plugin:org_evaluate_api|remove_evaluate';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveEvaluateResponse>(cmd, {
        request,
    });
}

//获取我的评估
export async function get_my_evaluate(request: GetMyEvaluateRequest): Promise<GetMyEvaluateResponse> {
    const cmd = 'plugin:org_evaluate_api|get_my_evaluate';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetMyEvaluateResponse>(cmd, {
        request,
    });
}

//更新我的评估
export async function update_my_evaluate(request: UpdateMyEvaluateRequest): Promise<UpdateMyEvaluateResponse> {
    const cmd = 'plugin:org_evaluate_api|update_my_evaluate';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateMyEvaluateResponse>(cmd, {
        request,
    });
}

