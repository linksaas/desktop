//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export type MemberPerm = {
    change_auth_check: boolean;
    read_auth_secret: boolean;
    manage_auth_secret: boolean;
};

export type MemberInfo = {
    member_user_id: string;
    member_display_name: string;
    member_logo_uri: string;
    perm: MemberPerm;
};

export type ListRequest = {
    access_token: string;
    remote_id: string;
};

export type ListResponse = {
    code: number;
    err_msg: string;
    member_list: MemberInfo[];
};

export type RemoveRequest = {
    access_token: string;
    remote_id: string;
    member_user_id: string;
};

export type RemoveResponse = {
    code: number;
    err_msg: string;
};

export type UpdatePermRequest = {
    access_token: string;
    remote_id: string;
    member_user_id: string;
    perm: MemberPerm;
};

export type UpdatePermResponse = {
    code: number;
    err_msg: string;
};

export type GetMyPermRequest = {
    access_token: string;
    remote_id: string;
};

export type GetMyPermResponse = {
    code: number;
    err_msg: string;
    perm: MemberPerm;
};

//列出成员
export async function list(addr: string, request: ListRequest): Promise<ListResponse> {
    const cmd = 'plugin:dragonfly_member_api|list';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListResponse>(cmd, {
        addr,
        request,
    });
}

//删除成员
export async function remove(addr: string, request: RemoveRequest): Promise<RemoveResponse> {
    const cmd = 'plugin:dragonfly_member_api|remove';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveResponse>(cmd, {
        addr,
        request,
    });
}

//更新成员权限
export async function update_perm(addr: string, request: UpdatePermRequest): Promise<UpdatePermResponse> {
    const cmd = 'plugin:dragonfly_member_api|update_perm';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdatePermResponse>(cmd, {
        addr,
        request,
    });
}

//获得我的权限
export async function get_my_perm(addr: string, request: GetMyPermRequest): Promise<GetMyPermResponse> {
    const cmd = 'plugin:dragonfly_member_api|get_my_perm';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetMyPermResponse>(cmd, {
        addr,
        request,
    });
}