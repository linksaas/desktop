//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export type SORT_BY = number;

export const SORT_BY_START_TIME: SORT_BY = 0;
export const SORT_BY_CONSUME_TIME: SORT_BY = 1;

export type SPAN_KIND = number;
export const SPAN_KIND_UNSPECIFIED: SPAN_KIND = 0;
export const SPAN_KIND_INTERNAL: SPAN_KIND = 1;
export const SPAN_KIND_SERVER: SPAN_KIND = 2;
export const SPAN_KIND_CLIENT: SPAN_KIND = 3;
export const SPAN_KIND_PRODUCER: SPAN_KIND = 4;
export const SPAN_KIND_CONSUMER: SPAN_KIND = 5;

export type AttrInfo = {
    key: string;
    value: string;
};

export type EventInfo = {
    name: string;
    time_stamp: number;
    attr_list: AttrInfo[];
};

export type SpanInfo = {
    span_id: string;
    parent_span_id: string;
    span_name: string;
    start_time_stamp: number;
    end_time_stamp: number;
    span_kind: SPAN_KIND;
    attr_list: AttrInfo[];
    event_list: EventInfo[];
};

export type ServiceInfo = {
    service_name: string;
    service_version: string;
};

export type TraceInfo = {
    trace_id: string;
    service: ServiceInfo;
    root_span: SpanInfo;
};

export type ListServiceRequest = {
    access_token: string;
    remote_id: string;
    from_time: number;
    to_time: number;
};

export type ListServiceResponse = {
    code: number;
    err_msg: string;
    service_list: ServiceInfo[];
};

export type ListRootSpanNameRequest = {
    access_token: string;
    remote_id: string;
    service: ServiceInfo;
    from_time: number;
    to_time: number;
};

export type ListRootSpanNameResponse = {
    code: number;
    err_msg: string;
    name_list: string[];
};

export type ListTraceRequest = {
    access_token: string;
    remote_id: string;
    service: ServiceInfo;
    filter_by_root_span_name: boolean;
    root_span_name: string;
    from_time: number;
    to_time: number;
    limit: number;
    sort_by: SORT_BY;
};

export type ListTraceResponse = {
    code: number;
    err_msg: string;
    trace_list: TraceInfo[];
};

export type GetTraceRequest = {
    access_token: string;
    remote_id: string;
    service: ServiceInfo;
    trace_id: string;
};

export type GetTraceResponse = {
    code: number;
    err_msg: string;
    trace: TraceInfo;
};

export type ListSpanRequest = {
    access_token: string;
    remote_id: string;
    service: ServiceInfo;
    trace_id: string;
};

export type ListSpanResponse = {
    code: number;
    err_msg: string;
    span_list: SpanInfo[];
};

//列出服务
export async function list_service(addr: string, request: ListServiceRequest): Promise<ListServiceResponse> {
    const cmd = 'plugin:dragonfly_trace_api|list_service';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListServiceResponse>(cmd, {
        addr,
        request,
    });
}

//列出入口名称
export async function list_root_span_name(addr: string, request: ListRootSpanNameRequest): Promise<ListRootSpanNameResponse> {
    const cmd = 'plugin:dragonfly_trace_api|list_root_span_name';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListRootSpanNameResponse>(cmd, {
        addr,
        request,
    });
}

//列出Trace
export async function list_trace(addr: string, request: ListTraceRequest): Promise<ListTraceResponse> {
    const cmd = 'plugin:dragonfly_trace_api|list_trace';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListTraceResponse>(cmd, {
        addr,
        request,
    });
}

//获得单个Trace
export async function get_trace(addr: string, request: GetTraceRequest): Promise<GetTraceResponse> {
    const cmd = 'plugin:dragonfly_trace_api|get_trace';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetTraceResponse>(cmd, {
        addr,
        request,
    });
}

//列出Span
export async function list_span(addr: string, request: ListSpanRequest): Promise<ListSpanResponse> {
    const cmd = 'plugin:dragonfly_trace_api|list_span';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListSpanResponse>(cmd, {
        addr,
        request,
    });
}