//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export type PortInfo = {
    has_zipkin_port: boolean;
    zipkin_port: number;
    has_jaeger_grpc_port: boolean;
    jaeger_grpc_port: number;
    has_jaeger_http_port: boolean;
    jaeger_http_port: number;
    has_skywalking_grpc_port: boolean;
    skywalking_grpc_port: number;
    has_skywalking_http_port: boolean;
    skywalking_http_port: number;
    has_otlp_grpc_port: boolean;
    otlp_grpc_port: number;
    has_otlp_http_port: boolean;
    otlp_http_port: number;
};

export type AuthSecretInfo = {
    username: string;
    password: string;
};

export type DataTtlInfo = {
    keep_trace_day: number;
};

export type GetDataTtlRequest = {
    access_token: string;
    remote_id: string;
};

export type GetDataTtlResponse = {
    code: number;
    err_msg: string;
    data_ttl: DataTtlInfo;
};

export type GetPortInfoRequest = {
    access_token: string;
    remote_id: string;
};

export type GetPortInfoResponse = {
    code: number;
    err_msg: string;
    port_info: PortInfo;
};

export type GetAuthCheckStateRequest = {
    access_token: string;
    remote_id: string;
};

export type GetAuthCheckStateResponse = {
    code: number;
    err_msg: string;
    enable: boolean;
};

export type EnableAuthCheckRequest = {
    access_token: string;
    remote_id: string;
};

export type EnableAuthCheckResponse = {
    code: number;
    err_msg: string;
};

export type DisableAuthCheckRequest = {
    access_token: string;
    remote_id: string;
};

export type DisableAuthCheckResponse = {
    code: number;
    err_msg: string;
};

export type AddAuthSecretRequest = {
    access_token: string;
    remote_id: string;
    auth_secret: AuthSecretInfo;
};

export type AddAuthSecretResponse = {
    code: number;
    err_msg: string;
};

export type RemoveAuthSecretRequest = {
    access_token: string;
    remote_id: string;
    username: string;
};

export type RemoveAuthSecretResponse = {
    code: number;
    err_msg: string;
};

export type UpdateAuthSecretRequest = {
    access_token: string;
    remote_id: string;
    auth_secret: AuthSecretInfo;
};

export type UpdateAuthSecretResponse = {
    code: number;
    err_msg: string;
};

export type GetAuthSecretRequest = {
    access_token: string;
    remote_id: string;
    username: string;
};

export type GetAuthSecretResponse = {
    code: number;
    err_msg: string;
    auth_secret: AuthSecretInfo;
};

export type ListAuthSecretRequest = {
    access_token: string;
    remote_id: string;
};

export type ListAuthSecretResponse = {
    code: number;
    err_msg: string;
    auth_secret_list: AuthSecretInfo[];
};

//获取数据存储时长
export async function get_data_ttl(addr: string, request: GetDataTtlRequest): Promise<GetDataTtlResponse> {
    const cmd = 'plugin:dragonfly_config_api|get_data_ttl';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetDataTtlResponse>(cmd, {
        addr,
        request,
    });
}

//获得端口信息
export async function get_port_info(addr: string, request: GetPortInfoRequest): Promise<GetPortInfoResponse> {
    const cmd = 'plugin:dragonfly_config_api|get_port_info';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetPortInfoResponse>(cmd, {
        addr,
        request,
    });
}

//获得上报密钥检查设置
export async function get_auth_check_state(addr: string, request: GetAuthCheckStateRequest): Promise<GetAuthCheckStateResponse> {
    const cmd = 'plugin:dragonfly_config_api|get_auth_check_state';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetAuthCheckStateResponse>(cmd, {
        addr,
        request,
    });
}

//打开上报密钥检查
export async function enable_auth_check(addr: string, request: EnableAuthCheckRequest): Promise<EnableAuthCheckResponse> {
    const cmd = 'plugin:dragonfly_config_api|enable_auth_check';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<EnableAuthCheckResponse>(cmd, {
        addr,
        request,
    });
}

//关闭上报密钥检查
export async function disable_auth_check(addr: string, request: DisableAuthCheckRequest): Promise<DisableAuthCheckResponse> {
    const cmd = 'plugin:dragonfly_config_api|disable_auth_check';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<DisableAuthCheckResponse>(cmd, {
        addr,
        request,
    });
}

//增加上报密钥
export async function add_auth_secret(addr: string, request: AddAuthSecretRequest): Promise<AddAuthSecretResponse> {
    const cmd = 'plugin:dragonfly_config_api|add_auth_secret';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AddAuthSecretResponse>(cmd, {
        addr,
        request,
    });
}

//删除上报密钥
export async function remove_auth_secret(addr: string, request: RemoveAuthSecretRequest): Promise<RemoveAuthSecretResponse> {
    const cmd = 'plugin:dragonfly_config_api|remove_auth_secret';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveAuthSecretResponse>(cmd, {
        addr,
        request,
    });
}

//更新上报密钥
export async function update_auth_secret(addr: string, request: UpdateAuthSecretRequest): Promise<UpdateAuthSecretResponse> {
    const cmd = 'plugin:dragonfly_config_api|update_auth_secret';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateAuthSecretResponse>(cmd, {
        addr,
        request,
    });
}

//获得单个上报密钥
export async function get_auth_secret(addr: string, request: GetAuthSecretRequest): Promise<GetAuthSecretResponse> {
    const cmd = 'plugin:dragonfly_config_api|get_auth_secret';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetAuthSecretResponse>(cmd, {
        addr,
        request,
    });
}

//列出上报密钥
export async function list_auth_secret(addr: string, request: ListAuthSecretRequest): Promise<ListAuthSecretResponse> {
    const cmd = 'plugin:dragonfly_config_api|list_auth_secret';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListAuthSecretResponse>(cmd, {
        addr,
        request,
    });
}