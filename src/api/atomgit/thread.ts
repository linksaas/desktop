//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { fetch } from '@tauri-apps/api/http';

// {
//     "id": "5c88e6163e6f45f49802b471a1079b24",
//     "content": "kyle 更改了任务 #33 希望增加更多的使用小工具   的状态为：已关闭",
//     "unread": true,
//     "updated_at": "2024-11-04T16:19:58+08:00",
//     "url": "https://api.atomgit.com/notifications/5c88e6163e6f45f49802b471a1079b24",
//     "html_url": "https://atomgit.com/openlinksaas/desktop/issues/33",
//     "actor": {
//         "login": "kyle",
//         "id": "657092d6e8906cd3d23cc2d2",
//         "url": "https://api.atomgit.comusers/kyle",
//         "type": "user",
//         "node_id": null,
//         "avatar_url": "https://file.atomgit.com/uploads/user/1706770460938_4087.jpeg",
//         "gravatar_id": null,
//         "html_url": "https://atomgit.com/kyle",
//         "followers_url": null,
//         "following_url": null,
//         "gists_url": null,
//         "starred_url": null,
//         "subscriptions_url": null,
//         "organizations_url": "https://api.atomgit.comusers/kyle/starred",
//         "repos_url": "https://api.atomgit.comusers/kyle/projects",
//         "events_url": null,
//         "received_events_url": null,
//         "site_admin": false
//     },
//     "type": "issue_update_state",
//     "namespace": {
//         "id": "96897",
//         "name": "openlinksaas / desktop",
//         "path": "openlinksaas/desktop",
//         "type": "project"
//     }
// }

export interface AtomGitThreadItem {
    id: string;
    unread: boolean;
    content: string;
    html_url: string;
    type: string;
    updated_at: string;
}

export interface AtomGitThread {
    total: number;
    list: AtomGitThreadItem[] | null | undefined,
}

export async function list_thread(accessToken: string): Promise<AtomGitThread> {
    const url = `https://api.atomgit.com/notifications/threads?unread=true&per_page=100`;
    const res = await fetch<AtomGitThread>(url, {
        method: "GET",
        timeout: 10,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
        },
    });
    if (res.ok && res.status == 200) {
        return res.data;
    } else {
        console.log(res);
        throw "error list my threads";
    }
}