//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { fetch } from '@tauri-apps/api/http';
import type { AtomGitUser } from './common';

export async function get_self_info(accessToken: string): Promise<AtomGitUser> {
    const url = `https://api.atomgit.com/user/info`;
    const res = await fetch<AtomGitUser>(url, {
        method: "GET",
        timeout: 10,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
        },
    });
    if (res.ok && res.status == 200) {
        return res.data;
    } else {
        console.log(res);
        throw "error get user info";
    }
}