//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export type REVIEW_TYPE = number;

export const REVIEW_TYPE_CODE: REVIEW_TYPE = 0;     //代码复盘
export const REVIEW_TYPE_FLOW: REVIEW_TYPE = 1;     //研发流程复盘
export const REVIEW_TYPE_PRODUCT: REVIEW_TYPE = 2;  //产品复盘
export const REVIEW_TYPE_BUG: REVIEW_TYPE = 3;      //缺陷复盘
export const REVIEW_TYPE_TEST: REVIEW_TYPE = 4;     //测试复盘
export const REVIEW_TYPE_MONTH: REVIEW_TYPE = 5;    //月度复盘
export const REVIEW_TYPE_QUARTER: REVIEW_TYPE = 6;  //季度复盘


export type LINK_TYPE = number;

export const LINK_TYPE_CONTENT: LINK_TYPE = 0;
export const LINK_TYPE_REQUIREMENT: LINK_TYPE = 1;
export const LINK_TYPE_BUG: LINK_TYPE = 2;
export const LINK_TYPE_TASK: LINK_TYPE = 3;
export const LINK_TYPE_TEST_CASE: LINK_TYPE = 4;
export const LINK_TYPE_SPRIT: LINK_TYPE = 100;
export const LINK_TYPE_DOC: LINK_TYPE = 101;
export const LINK_TYPE_PAGES: LINK_TYPE = 102;
export const LINK_TYPE_API_COLL: LINK_TYPE = 103;
export const LINK_TYPE_DRAW: LINK_TYPE = 104;

export type ReviewMemberItem = {
    member_user_id: string;
    member_display_name: string;
    member_logo_uri: string;
};

export type ReviewResultItem = {
    result_id: string;
    content: string;
    time_stamp: number;
};

export type ReviewLinkItem = {
    link_id: string;
    link_title: string;
    link_type: LINK_TYPE;
    link_target_id: string;
    time_stamp: number;
};

export type ReviewInfo = {
    review_id: string;
    title: string;
    review_type: REVIEW_TYPE;
    desc: string;
    member_list: ReviewMemberItem[];
    result_list: ReviewResultItem[];
    link_list: ReviewLinkItem[];
    create_time: number;
    update_time: number;
};

export type SimpleReviewInfo = {
    review_id: string;
    title: string;
    review_type: REVIEW_TYPE;
    desc: string;
    member_list: ReviewMemberItem[];
    create_time: number;
    update_time: number;
};

export type CreateRequest = {
    session_id: string;
    project_id: string;
    title: string;
    review_type: REVIEW_TYPE;
    desc: string;
    member_user_id_list: string[];
};

export type CreateResponse = {
    code: number;
    err_msg: string;
    review_id: string;
};

export type ListSimpleRequest = {
    session_id: string;
    project_id: string;
    filter_by_review_type: boolean;
    review_type: REVIEW_TYPE;
    filter_by_title_keyword: boolean;
    title_keyword: string;
    offset: number;
    limit: number;
};

export type ListSimpleResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    review_list: SimpleReviewInfo[];
};

export type ListRequest = {
    session_id: string;
    project_id: string;
    offset: number;
    limit: number;
};

export type ListResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    review_list: ReviewInfo[];
};

export type GetRequest = {
    session_id: string;
    project_id: string;
    review_id: string;
};

export type GetResponse = {
    code: number;
    err_msg: string;
    review: ReviewInfo;
};

export type RemoveRequest = {
    session_id: string;
    project_id: string;
    review_id: string;
};

export type RemoveResponse = {
    code: number;
    err_msg: string;
};

export type UpdateBasicRequest = {
    session_id: string;
    project_id: string;
    review_id: string;
    title: string;
    review_type: REVIEW_TYPE;
    desc: string;
    member_user_id_list: string[];
};

export type UpdateBasicResponse = {
    code: number;
    err_msg: string;
};

export type AddResultRequest = {
    session_id: string;
    project_id: string;
    review_id: string;
    content: string;
};

export type AddResultResponse = {
    code: number;
    err_msg: string;
    result_id: string;
};

export type RemoveResultRequest = {
    session_id: string;
    project_id: string;
    review_id: string;
    result_id: string;
};

export type RemoveResultResponse = {
    code: number;
    err_msg: string;
};

export type UpdateResultRequest = {
    session_id: string;
    project_id: string;
    review_id: string;
    result_id: string;
    content: string;
};

export type UpdateResultResponse = {
    code: number;
    err_msg: string;
};

export type AddLinkRequest = {
    session_id: string;
    project_id: string;
    review_id: string;
    link_type: LINK_TYPE;
    link_target_id: string;
};

export type AddLinkResponse = {
    code: number;
    err_msg: string;
    link_id: string;
};

export type RemoveLinkRequest = {
    session_id: string;
    project_id: string;
    review_id: string;
    link_id: string;
};

export type RemoveLinkResponse = {
    code: number;
    err_msg: string;
};


//创建复盘
export async function create(request: CreateRequest): Promise<CreateResponse> {
    const cmd = 'plugin:project_review_api|create';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<CreateResponse>(cmd, {
        request,
    });
}

//列出复盘(精简信息)
export async function list_simple(request: ListSimpleRequest): Promise<ListSimpleResponse> {
    const cmd = 'plugin:project_review_api|list_simple';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListSimpleResponse>(cmd, {
        request,
    });
}

//列出复盘
export async function list(request: ListRequest): Promise<ListResponse> {
    const cmd = 'plugin:project_review_api|list';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListResponse>(cmd, {
        request,
    });
}

//获取单个复盘信息
export async function get(request: GetRequest): Promise<GetResponse> {
    const cmd = 'plugin:project_review_api|get';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetResponse>(cmd, {
        request,
    });
}

//删除复盘
export async function remove(request: RemoveRequest): Promise<RemoveResponse> {
    const cmd = 'plugin:project_review_api|remove';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveResponse>(cmd, {
        request,
    });
}

//更新基本信息
export async function update_basic(request: UpdateBasicRequest): Promise<UpdateBasicResponse> {
    const cmd = 'plugin:project_review_api|update_basic';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateBasicResponse>(cmd, {
        request,
    });
}

//增加复盘结果
export async function add_result(request: AddResultRequest): Promise<AddResultResponse> {
    const cmd = 'plugin:project_review_api|add_result';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AddResultResponse>(cmd, {
        request,
    });
}

//删除复盘结果
export async function remove_result(request: RemoveResultRequest): Promise<RemoveResultResponse> {
    const cmd = 'plugin:project_review_api|remove_result';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveResultResponse>(cmd, {
        request,
    });
}

//更新复盘结果
export async function update_result(request: UpdateResultRequest): Promise<UpdateResultResponse> {
    const cmd = 'plugin:project_review_api|update_result';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateResultResponse>(cmd, {
        request,
    });
}

//增加链接
export async function add_link(request: AddLinkRequest): Promise<AddLinkResponse> {
    const cmd = 'plugin:project_review_api|add_link';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AddLinkResponse>(cmd, {
        request,
    });
}

//删除链接
export async function remove_link(request: RemoveLinkRequest): Promise<RemoveLinkResponse> {
    const cmd = 'plugin:project_review_api|remove_link';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveLinkResponse>(cmd, {
        request,
    });
}