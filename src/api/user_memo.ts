//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';
import type * as NoticeType from '@/api/notice_type';
import {emit as emit_all} from "@tauri-apps/api/event";

export type BasicMemoInfo = {
    content: string;
    tag_list: string[];
    bg_color: string;
};

export type MemoInfo = {
    memo_id: string;
    basic_info: BasicMemoInfo;
    create_time: number;
    update_time: number;
};

export type ListTagRequest = {
    session_id: string;
};

export type ListTagResponse = {
    code: number;
    err_msg: string;
    tag_list: string[];
};

export type CreateMemoRequest = {
    session_id: string;
    basic_info: BasicMemoInfo;
};

export type CreateMemoResponse = {
    code: number;
    err_msg: string;
    memo_id: string;
};

export type UpdateMemoRequest = {
    session_id: string;
    memo_id: string;
    basic_info: BasicMemoInfo;
};

export type UpdateMemoResponse = {
    code: number;
    err_msg: string;
};

export type ListMemoRequest = {
    session_id: string;
    filter_by_tag: boolean;
    tag_list: string[];
    offset: number;
    limit: number;
};

export type ListMemoResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    memo_list: MemoInfo[];
};

export type GetMemoRequest = {
    session_id: string;
    memo_id: string;
};

export type GetMemoResponse = {
    code: number;
    err_msg: string;
    memo: MemoInfo;
};

export type RemoveMemoRequest = {
    session_id: string;
    memo_id: string;
};

export type RemoveMemoResponse = {
    code: number;
    err_msg: string;
};

//列出标签
export async function list_tag(request: ListTagRequest): Promise<ListTagResponse> {
    const cmd = 'plugin:user_memo_api|list_tag';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListTagResponse>(cmd, {
        request,
    });
}

//创建备忘录
export async function create_memo(request: CreateMemoRequest): Promise<CreateMemoResponse> {
    const cmd = 'plugin:user_memo_api|create_memo';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<CreateMemoResponse>(cmd, {
        request,
    });
}

//更新备忘录
export async function update_memo(request: UpdateMemoRequest): Promise<UpdateMemoResponse> {
    const cmd = 'plugin:user_memo_api|update_memo';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    const res = await invoke<UpdateMemoResponse>(cmd, {
        request,
    });
    const notice: NoticeType.AllNotice = {
        ClientNotice: {
            UpdateUserMemoNotice: {
                memoId: request.memo_id,
            },
        },
    };
    await emit_all("notice", notice);
    return res;
}

//列出备忘录
export async function list_memo(request: ListMemoRequest): Promise<ListMemoResponse> {
    const cmd = 'plugin:user_memo_api|list_memo';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListMemoResponse>(cmd, {
        request,
    });
}

//获取单条备忘录
export async function get_memo(request: GetMemoRequest): Promise<GetMemoResponse> {
    const cmd = 'plugin:user_memo_api|get_memo';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetMemoResponse>(cmd, {
        request,
    });
}

//删除备忘录
export async function remove_memo(request: RemoveMemoRequest): Promise<RemoveMemoResponse> {
    const cmd = 'plugin:user_memo_api|remove_memo';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveMemoResponse>(cmd, {
        request,
    });
}