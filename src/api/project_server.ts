//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export type SERVER_TYPE = number;

export const SERVER_TYPE_SSH: SERVER_TYPE = 0;
export const SERVER_TYPE_MYSQL: SERVER_TYPE = 1;
export const SERVER_TYPE_POSTGRES: SERVER_TYPE = 2;
export const SERVER_TYPE_MONGO: SERVER_TYPE = 3;
export const SERVER_TYPE_REDIS: SERVER_TYPE = 4;
export const SERVER_TYPE_GRPC: SERVER_TYPE = 5;
export const SERVER_TYPE_EXTERN: SERVER_TYPE = 100;
export const SERVER_TYPE_OPEN_SEA_JELLY: SERVER_TYPE = 101;  //OpenLinkSaas服务器管理
export const SERVER_TYPE_OPEN_DRAGON_FLY: SERVER_TYPE = 102; //OpenLinkSaas可视化服务 
export const SERVER_TYPE_OPEN_SEA_OTTER: SERVER_TYPE = 103;  //OpenLinkSaas镜像仓库

export type EXTERN_SERVER_TYPE = number;

export const EXTERN_SERVER_TYPE_NULL: EXTERN_SERVER_TYPE = 0;
export const EXTERN_SERVER_TYPE_PACKAGE_REPO: EXTERN_SERVER_TYPE = 1;  //软件库仓库
export const EXTERN_SERVER_TYPE_IMAGE_REPO: EXTERN_SERVER_TYPE = 2;    //镜像仓库
export const EXTERN_SERVER_TYPE_ISSUE: EXTERN_SERVER_TYPE = 3;         //任务/缺陷管理
export const EXTERN_SERVER_TYPE_DOC: EXTERN_SERVER_TYPE = 4;           //文档管理
export const EXTERN_SERVER_TYPE_MONITOR: EXTERN_SERVER_TYPE = 5;       //监控
export const EXTERN_SERVER_TYPE_TRACE: EXTERN_SERVER_TYPE = 6;         //调用追踪系统
export const EXTERN_SERVER_TYPE_INSTRUMENT: EXTERN_SERVER_TYPE = 7;    //仪表盘
export const EXTERN_SERVER_TYPE_CICD: EXTERN_SERVER_TYPE = 8;          //CICD服务器


export type BasicServerInfo = {
    server_name: string;
    server_type: SERVER_TYPE;
    addr_list: string[];
    extern_server_type: EXTERN_SERVER_TYPE;
};

export type ServerInfo = {
    server_id: string;
    basic_info: BasicServerInfo;
    create_time: number;
    update_time: number;
};

export type AddServerRequest = {
    session_id: string;
    project_id: string;
    basic_info: BasicServerInfo;
};

export type AddServerResponse = {
    code: number;
    err_msg: string;
    server_id: string;
};

export type UpdateServerRequest = {
    session_id: string;
    project_id: string;
    server_id: string;
    basic_info: BasicServerInfo;
};

export type UpdateServerResponse = {
    code: number;
    err_msg: string;
};

export type ListServerRequest = {
    session_id: string;
    project_id: string;
};

export type ListServerResponse = {
    code: number;
    err_msg: string;
    server_info_list: ServerInfo[];
};

export type GetServerRequest = {
    session_id: string;
    project_id: string;
    server_id: string;
};

export type GetServerResponse = {
    code: number;
    err_msg: string;
    server_info: ServerInfo;
};

export type RemoveServerRequest = {
    session_id: string;
    project_id: string;
    server_id: string;
};

export type RemoveServerResponse = {
    code: number;
    err_msg: string;
};

export type GenJoinTokenRequest = {
    session_id: string;
    project_id: string;
    server_type: SERVER_TYPE;
};

export type GenJoinTokenResponse = {
    code: number;
    err_msg: string;
    token: string;
};

export type GenAccessTokenRequest = {
    session_id: string;
    project_id: string;
    server_id: string;
};

export type GenAccessTokenResponse = {
    code: number;
    err_msg: string;
    token: string;
};

//增加服务器
export async function add_server(request: AddServerRequest): Promise<AddServerResponse> {
    const cmd = 'plugin:project_server_api|add_server';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AddServerResponse>(cmd, {
        request,
    });
}

//更新服务器
export async function update_server(request: UpdateServerRequest): Promise<UpdateServerResponse> {
    const cmd = 'plugin:project_server_api|update_server';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateServerResponse>(cmd, {
        request,
    });
}

//列出服务器
export async function list_server(request: ListServerRequest): Promise<ListServerResponse> {
    const cmd = 'plugin:project_server_api|list_server';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListServerResponse>(cmd, {
        request,
    });
}

//获取单个服务器
export async function get_server(request: GetServerRequest): Promise<GetServerResponse> {
    const cmd = 'plugin:project_server_api|get_server';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetServerResponse>(cmd, {
        request,
    });
}

//删除服务器
export async function remove_server(request: RemoveServerRequest): Promise<RemoveServerResponse> {
    const cmd = 'plugin:project_server_api|remove_server';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveServerResponse>(cmd, {
        request,
    });
}

//生成加入令牌
export async function gen_join_token(request: GenJoinTokenRequest): Promise<GenJoinTokenResponse> {
    const cmd = 'plugin:project_server_api|gen_join_token';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GenJoinTokenResponse>(cmd, {
        request,
    });
}

//生成访问令牌
export async function gen_access_token(request: GenAccessTokenRequest): Promise<GenAccessTokenResponse> {
    const cmd = 'plugin:project_server_api|gen_access_token';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GenAccessTokenResponse>(cmd, {
        request,
    });
}