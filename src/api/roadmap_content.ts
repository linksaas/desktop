//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export type NODE_TYPE = number;
export const NODE_LABEL: NODE_TYPE = 0;
export const NODE_RICH_TEXT: NODE_TYPE = 1;
export const NODE_TOPIC: NODE_TYPE = 2;
export const NODE_SUB_TOPIC: NODE_TYPE = 3;
export const NODE_TODO_LIST: NODE_TYPE = 4;
export const NODE_ROADMAP_REF: NODE_TYPE = 5;
export const NODE_WEB_LINK: NODE_TYPE = 6;
export const NODE_CODE: NODE_TYPE = 7;
export const NODE_GROUP: NODE_TYPE = 999;

export type EDGE_TYPE = number;
export const EDGE_BEZIER: EDGE_TYPE = 0;       //贝塞尔曲线
export const EDGE_STRAIGHT: EDGE_TYPE = 1;     //直线
export const EDGE_STEP: EDGE_TYPE = 2;         //折线
export const EDGE_SMOOTH_STEP: EDGE_TYPE = 3;  //平滑折线

export type LEARN_POLICY_TYPE = number;
export const LEARN_POLICY_NONE: LEARN_POLICY_TYPE = 0;       //未设置策略
export const LEARN_POLICY_RECOMMEND: LEARN_POLICY_TYPE = 1;  //推荐学习
export const LEARN_POLICY_OPTION: LEARN_POLICY_TYPE = 2;     //可选学习
export const LEARN_POLICY_EXTRA: LEARN_POLICY_TYPE = 3;      //进阶学习

export type EXTRA_LINK_TYPE = number;
export const EXTRA_LINK_ARTICLE: EXTRA_LINK_TYPE = 0;  //指向文章
export const EXTRA_LINK_VIDEO: EXTRA_LINK_TYPE = 1;    //指向视频
export const EXTRA_LINK_TEST: EXTRA_LINK_TYPE = 2;     //指向能力测试
export const EXTRA_LINK_COURSE: EXTRA_LINK_TYPE = 3;   //指向教学课程
export const EXTRA_LINK_WEBSITE: EXTRA_LINK_TYPE = 4;  //指向网站


/// 对应NODE_LABEL
export type NodeDataText = {
    font_size: number;
    font_weight: number;
    font_color: string;
    content: string;
    bg_color: string;
};

//对应NODE_RICH_TEXT
export type NodeDataRichText = {
    content: string;
    bg_color: string;
};

export type ExtraLinkItem = {
    link_id: string;
    link_type: EXTRA_LINK_TYPE;
    link_title: string;
    link_url: string;
};

/// 对应NODE_TOPIC和NODE_SUB_TOPIC
export type NodeDataTopic = {
    font_size: number;
    font_weight: number;
    font_color: string;
    title: string;
    bg_color: string;
    content: string;
    learn_policy: LEARN_POLICY_TYPE;
    link_list: ExtraLinkItem[];
};
export type NodeTodoItem = {
    todo_id: string;
    title: string;
};

/// 对应NODE_TODO_LIST
export type NodeDataTodoList = {
    item_list: NodeTodoItem[];
    bg_color: string;
};

/// 对应NODE_GROUP
export type NodeDataGroup = {
    bg_color: string;
    border_color: string;
    border_radius: number;
};

//对应NODE_ROADMAP_REF
export type NodeDataRoadmapRef = {
    target_title: string;
    target_id: string;
};

/// 对应NODE_WEB_LINK
export type NodeDataWebLink = {
    link_name: string;
    link_url: string;
};

/// 对应NODE_CODE
export type NodeDataCode = {
    lang: string;
    code: string;
};

export type NodeSize = {
    w: number;
    h: number;
};

export type NodePosition = {
    x: number;
    y: number;
};

export type NodeData = {
    TextData?: NodeDataText;
    TopicData?: NodeDataTopic;
    TodoListData?: NodeDataTodoList;
    GroupData?: NodeDataGroup;
    RichTextData?: NodeDataRichText;
    RoadmapRefData?: NodeDataRoadmapRef;
    WebLinkData?: NodeDataWebLink;
    CodeData?: NodeDataCode;
};

export type BasicNodeInfo = {
    node_type: NODE_TYPE;
    node_size: NodeSize;
    node_position: NodePosition;
    node_data: NodeData;
};

export type NodeInfo = {
    node_id: string;
    basic_info: BasicNodeInfo;
    roadmap_id: string;
};

export type BasicEdgeInfo = {
    edge_type: EDGE_TYPE;
    edge_color: string;
    has_begin_arrow: boolean;
    has_end_arrow: boolean;
    edge_width: number;
};

export type EdgeKey = {
    from_node_id: string;
    from_handle_id: string;
    to_node_id: string;
    to_handle_id: string;
    roadmap_id: string;
};

export type EdgeInfo = {
    edge_key: EdgeKey;
    basic_info: BasicEdgeInfo;
};

export type AddNodeRequest = {
    session_id: string;
    roadmap_id: string;
    basic_info: BasicNodeInfo;
};

export type AddNodeResponse = {
    code: number;
    err_msg: string;
    node_id: string;
};

export type ListNodeRequest = {
    session_id: string;
    roadmap_id: string;
};

export type ListNodeResponse = {
    code: number;
    err_msg: string;
    node_list: NodeInfo[];
};

export type GetNodeRequest = {
    session_id: string;
    roadmap_id: string;
    node_id: string;
};

export type GetNodeResponse = {
    code: number;
    err_msg: string;
    node: NodeInfo;
};

export type UpdateNodeSizeRequest = {
    session_id: string;
    roadmap_id: string;
    node_id: string;
    node_size: NodeSize;
};

export type UpdateNodeSizeResponse = {
    code: number;
    err_msg: string;
};

export type UpdateNodePositionRequest = {
    session_id: string;
    roadmap_id: string;
    node_id: string;
    node_position: NodePosition;
};

export type UpdateNodePositionResponse = {
    code: number;
    err_msg: string;
};

export type UpdateNodeDataRequest = {
    session_id: string;
    roadmap_id: string;
    node_id: string;
    node_data: NodeData;
};

export type UpdateNodeDataResponse = {
    code: number;
    err_msg: string;
};

export type RemoveNodeRequest = {
    session_id: string;
    roadmap_id: string;
    node_id: string;
};

export type RemoveNodeResponse = {
    code: number;
    err_msg: string;
};

export type AddEdgeRequest = {
    session_id: string;
    edge_key: EdgeKey;
    basic_info: BasicEdgeInfo;
};

export type AddEdgeResponse = {
    code: number;
    err_msg: string;
};

export type ListEdgeRequest = {
    session_id: string;
    roadmap_id: string;
};

export type ListEdgeResponse = {
    code: number;
    err_msg: string;
    edge_list: EdgeInfo[];
};

export type GetEdgeRequest = {
    session_id: string;
    edge_key: EdgeKey;
};

export type GetEdgeResponse = {
    code: number;
    err_msg: string;
    edge: EdgeInfo;
};

export type UpdateEdgeRequest = {
    session_id: string;
    edge_key: EdgeKey;
    basic_info: BasicEdgeInfo;
};

export type UpdateEdgeResponse = {
    code: number;
    err_msg: string;
};

export type RemoveEdgeRequest = {
    session_id: string;
    edge_key: EdgeKey;
};

export type RemoveEdgeResponse = {
    code: number;
    err_msg: string;
};


//创建节点
export async function add_node(request: AddNodeRequest): Promise<AddNodeResponse> {
    const cmd = 'plugin:roadmap_content_api|add_node';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AddNodeResponse>(cmd, {
        request,
    });
}

//列出节点
export async function list_node(request: ListNodeRequest): Promise<ListNodeResponse> {
    const cmd = 'plugin:roadmap_content_api|list_node';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListNodeResponse>(cmd, {
        request,
    });
}

//获得单个节点
export async function get_node(request: GetNodeRequest): Promise<GetNodeResponse> {
    const cmd = 'plugin:roadmap_content_api|get_node';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetNodeResponse>(cmd, {
        request,
    });
}

//更新节点大小
export async function update_node_size(request: UpdateNodeSizeRequest): Promise<UpdateNodeSizeResponse> {
    const cmd = 'plugin:roadmap_content_api|update_node_size';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateNodeSizeResponse>(cmd, {
        request,
    });
}

//更新节点位置
export async function update_node_position(request: UpdateNodePositionRequest): Promise<UpdateNodePositionResponse> {
    const cmd = 'plugin:roadmap_content_api|update_node_position';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateNodePositionResponse>(cmd, {
        request,
    });
}

//更新节点数据
export async function update_node_data(request: UpdateNodeDataRequest): Promise<UpdateNodeDataResponse> {
    const cmd = 'plugin:roadmap_content_api|update_node_data';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateNodeDataResponse>(cmd, {
        request,
    });
}

//删除节点
export async function remove_node(request: RemoveNodeRequest): Promise<RemoveNodeResponse> {
    const cmd = 'plugin:roadmap_content_api|remove_node';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveNodeResponse>(cmd, {
        request,
    });
}

//创建连接
export async function add_edge(request: AddEdgeRequest): Promise<AddEdgeResponse> {
    const cmd = 'plugin:roadmap_content_api|add_edge';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AddEdgeResponse>(cmd, {
        request,
    });
}

//列出连接
export async function list_edge(request: ListEdgeRequest): Promise<ListEdgeResponse> {
    const cmd = 'plugin:roadmap_content_api|list_edge';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListEdgeResponse>(cmd, {
        request,
    });
}

//获得单个连接
export async function get_edge(request: GetEdgeRequest): Promise<GetEdgeResponse> {
    const cmd = 'plugin:roadmap_content_api|get_edge';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetEdgeResponse>(cmd, {
        request,
    });
}

//更新连接
export async function update_edge(request: UpdateEdgeRequest): Promise<UpdateEdgeResponse> {
    const cmd = 'plugin:roadmap_content_api|update_edge';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateEdgeResponse>(cmd, {
        request,
    });
}

//删除连接
export async function remove_edge(request: RemoveEdgeRequest): Promise<RemoveEdgeResponse> {
    const cmd = 'plugin:roadmap_content_api|remove_edge';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveEdgeResponse>(cmd, {
        request,
    });
}