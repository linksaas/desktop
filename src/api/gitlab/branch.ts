//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { fetch } from '@tauri-apps/api/http';

export interface GitLabBranch {
    name: string;
    protected: boolean;
}

export async function list_branch(baseUrl: string, accessToken: string, projectId: string): Promise<GitLabBranch[]> {
    const url = `${baseUrl}/api/v4/projects/${projectId}/repository/branches?access_token=${accessToken}`;
    const res = await fetch<GitLabBranch[]>(url, {
        method: "GET",
        timeout: 10,
    });
    if (res.ok && res.status == 200) {
        return res.data;
    } else {
        console.log(res);
        throw "error list branch";
    }
}