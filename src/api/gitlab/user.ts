//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { fetch } from '@tauri-apps/api/http';
import { GitLabUser } from "./common";

export async function get_self_info(baseUrl: string, accessToken: string): Promise<GitLabUser> {
    const url = `${baseUrl}/api/v4/user?access_token=${accessToken}`;
    const res = await fetch<GitLabUser>(url, {
        method: "GET",
        timeout: 10,
    });
    if (res.ok && res.status == 200) {
        return res.data;
    } else {
        console.log(res);
        throw "error get self info";
    }
}