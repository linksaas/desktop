//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { fetch } from '@tauri-apps/api/http';

export interface GitLabCommit {
    id: string;
    short_id: string;
    created_at: string;
    title: string;
    committer_name: string;
    committer_email: string;
    committed_date: string;
    web_url: string;
}

// {
//     "id": "0774550b582633cfcb818a989a582f1144405779",
//     "short_id": "0774550b",
//     "created_at": "2024-09-13T03:15:06.000+00:00",
//     "parent_ids": [],
//     "title": "Initial commit",
//     "message": "Initial commit",
//     "author_name": "Administrator",
//     "author_email": "admin@example.com",
//     "authored_date": "2024-09-13T03:15:06.000+00:00",
//     "committer_name": "Administrator",
//     "committer_email": "admin@example.com",
//     "committed_date": "2024-09-13T03:15:06.000+00:00",
//     "trailers": {},
//     "web_url": "http://localhost:8000/group1/group2/test/-/commit/0774550b582633cfcb818a989a582f1144405779"
// }

export async function list_commit(baseUrl: string, accessToken: string, projectId: string, refName: string): Promise<GitLabCommit[]> {
    const url = `${baseUrl}/api/v4/projects/${projectId}/repository/commits?access_token=${accessToken}&ref_name=${encodeURIComponent(refName)}`;
    const res = await fetch<GitLabCommit[]>(url, {
        method: "GET",
        timeout: 10,
    });
    if (res.ok && res.status == 200) {
        return res.data;
    } else {
        console.log(res);
        throw "error list commit";
    }
}