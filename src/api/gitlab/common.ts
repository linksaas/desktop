//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

export interface GitLabUser {
    id: number;
    username: string;
    name: string;
    avatar_url: string;
    web_url: string;
}

// {
//     "id": 1,
//     "username": "root",
//     "name": "Administrator",
//     "state": "active",
//     "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
//     "web_url": "http://localhost:8000/root",
//     "created_at": "2024-09-13T03:06:58.064Z",
//     "bio": "",
//     "location": "",
//     "public_email": null,
//     "skype": "",
//     "linkedin": "",
//     "twitter": "",
//     "discord": "",
//     "website_url": "",
//     "organization": "",
//     "job_title": "",
//     "pronouns": null,
//     "bot": false,
//     "work_information": null,
//     "local_time": null,
//     "last_sign_in_at": "2024-09-13T03:10:21.555Z",
//     "confirmed_at": "2024-09-13T03:06:57.894Z",
//     "last_activity_on": "2024-09-13",
//     "email": "admin@example.com",
//     "theme_id": 1,
//     "color_scheme_id": 1,
//     "projects_limit": 100000,
//     "current_sign_in_at": "2024-09-13T03:10:21.555Z",
//     "identities": [],
//     "can_create_group": true,
//     "can_create_project": true,
//     "two_factor_enabled": false,
//     "external": false,
//     "private_profile": false,
//     "commit_email": "admin@example.com",
//     "is_admin": true,
//     "note": null,
//     "namespace_id": 1,
//     "created_by": null
// }