//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { fetch } from '@tauri-apps/api/http';
import type { GitLabIssue } from './issue';

export interface GitLabTodo {
    id: number;
    target_type: string;
    target: GitLabIssue;
    created_at: string;
    updated_at: string;
}

// {
//     "id": 1,
//     "project": {
//         "id": 1,
//         "description": null,
//         "name": "test",
//         "name_with_namespace": "group1 / group2 / test",
//         "path": "test",
//         "path_with_namespace": "group1/group2/test",
//         "created_at": "2024-09-13T03:15:05.556Z"
//     },
//     "author": {
//         "id": 1,
//         "username": "root",
//         "name": "Administrator",
//         "state": "active",
//         "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
//         "web_url": "http://localhost:8000/root"
//     },
//     "action_name": "assigned",
//     "target_type": "Issue",
//     "target": {
//         "id": 1,
//         "iid": 1,
//         "project_id": 1,
//         "title": "xxxxxxxx",
//         "description": "fasdfsdf",
//         "state": "opened",
//         "created_at": "2024-09-13T06:47:00.109Z",
//         "updated_at": "2024-09-13T06:47:00.109Z",
//         "closed_at": null,
//         "closed_by": null,
//         "labels": [],
//         "milestone": null,
//         "assignees": [
//             {
//                 "id": 1,
//                 "username": "root",
//                 "name": "Administrator",
//                 "state": "active",
//                 "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
//                 "web_url": "http://localhost:8000/root"
//             }
//         ],
//         "author": {
//             "id": 1,
//             "username": "root",
//             "name": "Administrator",
//             "state": "active",
//             "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
//             "web_url": "http://localhost:8000/root"
//         },
//         "type": "ISSUE",
//         "assignee": {
//             "id": 1,
//             "username": "root",
//             "name": "Administrator",
//             "state": "active",
//             "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
//             "web_url": "http://localhost:8000/root"
//         },
//         "user_notes_count": 0,
//         "merge_requests_count": 0,
//         "upvotes": 0,
//         "downvotes": 0,
//         "due_date": null,
//         "confidential": false,
//         "discussion_locked": null,
//         "issue_type": "issue",
//         "web_url": "http://localhost:8000/group1/group2/test/-/issues/1",
//         "time_stats": {
//             "time_estimate": 0,
//             "total_time_spent": 0,
//             "human_time_estimate": null,
//             "human_total_time_spent": null
//         },
//         "task_completion_status": {
//             "count": 0,
//             "completed_count": 0
//         },
//         "has_tasks": true,
//         "task_status": "0 of 0 checklist items completed",
//         "_links": {
//             "self": "http://localhost:8000/api/v4/projects/1/issues/1",
//             "notes": "http://localhost:8000/api/v4/projects/1/issues/1/notes",
//             "award_emoji": "http://localhost:8000/api/v4/projects/1/issues/1/award_emoji",
//             "project": "http://localhost:8000/api/v4/projects/1",
//             "closed_as_duplicate_of": null
//         },
//         "references": {
//             "short": "#1",
//             "relative": "#1",
//             "full": "group1/group2/test#1"
//         },
//         "severity": "UNKNOWN",
//         "moved_to_id": null,
//         "service_desk_reply_to": null
//     },
//     "target_url": "http://localhost:8000/group1/group2/test/-/issues/1",
//     "body": "xxxxxxxx",
//     "state": "pending",
//     "created_at": "2024-09-13T06:47:00.629Z",
//     "updated_at": "2024-09-13T06:47:00.629Z"
// }

export async function list_todo(baseUrl: string, accessToken: string): Promise<GitLabTodo[]> {
    const url = `${baseUrl}/api/v4/todos?access_token=${accessToken}`;
    const res = await fetch<GitLabTodo[]>(url, {
        method: "GET",
        timeout: 10,
    });
    if (res.ok && res.status == 200) {
        return res.data;
    } else {
        console.log(res);
        throw "error list todo";
    }
}