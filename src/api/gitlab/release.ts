//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { fetch } from '@tauri-apps/api/http';
import type { GitLabUser } from './common';
import type { GitLabCommit } from './commit';

export interface GitLabSource {
    format: string;
    url: string;
}

export interface GitLabAssets {
    count: number;
    sources: GitLabSource[];
}

export interface GitLabReleaseLink {
    self: string;
}

export interface GitLabRelease {
    name: string;
    tag_name: string;
    description: string;
    created_at: string;
    released_at: string;
    author: GitLabUser,
    commit: GitLabCommit;
    assets: GitLabAssets;
    _links: GitLabReleaseLink;
}

// {
//     "name": "测试代码1.0版本发布",
//     "tag_name": "1.0",
//     "description": "噶士大夫敢死队风格",
//     "created_at": "2024-09-13T08:23:09.330Z",
//     "released_at": "2024-09-13T08:23:09.330Z",
//     "upcoming_release": false,
//     "author": {
//         "id": 1,
//         "username": "root",
//         "name": "Administrator",
//         "state": "active",
//         "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
//         "web_url": "http://localhost:8000/root"
//     },
//     "commit": {
//         "id": "0774550b582633cfcb818a989a582f1144405779",
//         "short_id": "0774550b",
//         "created_at": "2024-09-13T03:15:06.000+00:00",
//         "parent_ids": [],
//         "title": "Initial commit",
//         "message": "Initial commit",
//         "author_name": "Administrator",
//         "author_email": "admin@example.com",
//         "authored_date": "2024-09-13T03:15:06.000+00:00",
//         "committer_name": "Administrator",
//         "committer_email": "admin@example.com",
//         "committed_date": "2024-09-13T03:15:06.000+00:00",
//         "trailers": {},
//         "web_url": "http://localhost:8000/group1/group2/test/-/commit/0774550b582633cfcb818a989a582f1144405779"
//     },
//     "commit_path": "/group1/group2/test/-/commit/0774550b582633cfcb818a989a582f1144405779",
//     "tag_path": "/group1/group2/test/-/tags/1.0",
//     "assets": {
//         "count": 4,
//         "sources": [
//             {
//                 "format": "zip",
//                 "url": "http://localhost:8000/group1/group2/test/-/archive/1.0/test-1.0.zip"
//             },
//             {
//                 "format": "tar.gz",
//                 "url": "http://localhost:8000/group1/group2/test/-/archive/1.0/test-1.0.tar.gz"
//             },
//             {
//                 "format": "tar.bz2",
//                 "url": "http://localhost:8000/group1/group2/test/-/archive/1.0/test-1.0.tar.bz2"
//             },
//             {
//                 "format": "tar",
//                 "url": "http://localhost:8000/group1/group2/test/-/archive/1.0/test-1.0.tar"
//             }
//         ],
//         "links": []
//     },
//     "evidences": [
//         {
//             "sha": "1403439f628bf1f021a8f7360bee6d3295a25c05012d",
//             "filepath": "http://localhost:8000/group1/group2/test/-/releases/1.0/evidences/1.json",
//             "collected_at": "2024-09-13T08:23:09.442Z"
//         }
//     ],
//     "_links": {
//         "closed_issues_url": "http://localhost:8000/group1/group2/test/-/issues?release_tag=1.0&scope=all&state=closed",
//         "closed_merge_requests_url": "http://localhost:8000/group1/group2/test/-/merge_requests?release_tag=1.0&scope=all&state=closed",
//         "edit_url": "http://localhost:8000/group1/group2/test/-/releases/1.0/edit",
//         "merged_merge_requests_url": "http://localhost:8000/group1/group2/test/-/merge_requests?release_tag=1.0&scope=all&state=merged",
//         "opened_issues_url": "http://localhost:8000/group1/group2/test/-/issues?release_tag=1.0&scope=all&state=opened",
//         "opened_merge_requests_url": "http://localhost:8000/group1/group2/test/-/merge_requests?release_tag=1.0&scope=all&state=opened",
//         "self": "http://localhost:8000/group1/group2/test/-/releases/1.0"
//     }
// }

export async function list_release(baseUrl: string,accessToken: string, projectId: string): Promise<GitLabRelease[]> {
    const url = `${baseUrl}/api/v4/projects/${projectId}/releases?access_token=${accessToken}`;
    console.log(url);
    const res = await fetch<GitLabRelease[]>(url, {
        method: "GET",
        timeout: 10,
    });
    console.log(res.data);
    if (res.ok && res.status == 200) {
        return res.data;
    } else {
        console.log(res);
        throw "error list release";
    }
}