//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export type BasicTodoInfo = {
    title: string;
    title_color: string;
    done: boolean;
};

export type TodoInfo = {
    todo_id: string;
    basic_info: BasicTodoInfo;
    create_time: number;
    update_time: number;
};

export type CreateTodoRequest = {
    session_id: string;
    basic_info: BasicTodoInfo;
};

export type CreateTodoResponse = {
    code: number;
    err_msg: string;
    todo_id: string;
};

export type UpdateTodoRequest = {
    session_id: string;
    todo_id: string;
    basic_info: BasicTodoInfo;
};

export type UpdateTodoResponse = {
    code: number;
    err_msg: string;
};

export type CountTodoRequest = {
    session_id: string;
};

export type CountTodoResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    done_count: number;
};

export type ListTodoRequest = {
    session_id: string;
};

export type ListTodoResponse = {
    code: number;
    err_msg: string;
    todo_list: TodoInfo[];
};

export type GetTodoRequest = {
    session_id: string;
    todo_id: string;
};

export type GetTodoResponse = {
    code: number;
    err_msg: string;
    todo: TodoInfo;
};

export type RemoveTodoRequest = {
    session_id: string;
    todo_id: string;
};

export type RemoveTodoResponse = {
    code: number;
    err_msg: string;
};

export type ClearTodoRequest = {
    session_id: string;
};

export type ClearTodoResponse = {
    code: number;
    err_msg: string;
};

//创建待办
export async function create_todo(request: CreateTodoRequest): Promise<CreateTodoResponse> {
    const cmd = 'plugin:user_todo_api|create_todo';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<CreateTodoResponse>(cmd, {
        request,
    });
}

//更新待办
export async function update_todo(request: UpdateTodoRequest): Promise<UpdateTodoResponse> {
    const cmd = 'plugin:user_todo_api|update_todo';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateTodoResponse>(cmd, {
        request,
    });
}

//获取待办数量
export async function count_todo(request: CountTodoRequest): Promise<CountTodoResponse> {
    const cmd = 'plugin:user_todo_api|count_todo';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<CountTodoResponse>(cmd, {
        request,
    });
}

//列出待办
export async function list_todo(request: ListTodoRequest): Promise<ListTodoResponse> {
    const cmd = 'plugin:user_todo_api|list_todo';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    const retRes = await invoke<ListTodoResponse>(cmd, {
        request,
    });
    for (const todoItem of (retRes.todo_list ?? [])) {
        if (todoItem.basic_info.title_color == "") {
            todoItem.basic_info.title_color = "black";
        }
    }
    return retRes;
}

//获取单个待办
export async function get_todo(request: GetTodoRequest): Promise<GetTodoResponse> {
    const cmd = 'plugin:user_todo_api|get_todo';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    const retRes = await invoke<GetTodoResponse>(cmd, {
        request,
    });
    if(retRes.todo.basic_info.title_color == "") {
        retRes.todo.basic_info.title_color = "black"; 
    }
    return retRes;
}

//删除待办
export async function remove_todo(request: RemoveTodoRequest): Promise<RemoveTodoResponse> {
    const cmd = 'plugin:user_todo_api|remove_todo';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveTodoResponse>(cmd, {
        request,
    });
}

//清空待办
export async function clear_todo(request: ClearTodoRequest): Promise<ClearTodoResponse> {
    const cmd = 'plugin:user_todo_api|clear_todo';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ClearTodoResponse>(cmd, {
        request,
    });
}