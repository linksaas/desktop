//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export type AttrDesc = {
    attr_key: string;
    desc: string;
};

export type AttrDescGroup = {
    group_name: string;
    attr_desc_list: AttrDesc[];
};

export type TrigerTypeInfo = {
    type_id: string;
    type_desc: string;
    has_secret: boolean;
    attr_desc_group_list: AttrDescGroup[];
};


export type TrigerInfo = {
    triger_id: string;
    triger_name: string;
    type_id: string;
    secret: string;
    create_time: number;
    create_user_id: string;
    create_display_name: string;
    create_logo_uri: string;
    update_time: number;
    update_user_id: string;
    update_display_name: string;
    update_logo_uri: string;
    msg_count: number;
    has_secret: boolean;
};

export type TrigerMsgInfo = {
    msg_id: string;
    triger_id: string;
    time_stamp: number;
    attr_list: string[];
    content: number[];
};

export type ListTypeRequest = {};

export type ListTypeResponse = {
    code: number;
    err_msg: string;
    triger_type_list: TrigerTypeInfo[];
};

export type GetTypeRequest = {
    type_id: string;
};

export type GetTypeResponse = {
    code: number;
    err_msg: string;
    triger_type: TrigerTypeInfo;
};

export type CreateRequest = {
    session_id: string;
    project_id: string;
    type_id: string;
    triger_name: string;
};

export type CreateResponse = {
    code: number;
    err_msg: string;
    triger_id: string;
};

export type UpdateNameRequest = {
    session_id: string;
    project_id: string;
    triger_id: string;
    triger_name: string;
};

export type UpdateNameResponse = {
    code: number;
    err_msg: string;
};


export type ResetSecretRequest = {
    session_id: string;
    project_id: string;
    triger_id: string;
};

export type ResetSecretResponse = {
    code: number;
    err_msg: string;
};

export type RemoveRequest = {
    session_id: string;
    project_id: string;
    triger_id: string;
};

export type RemoveResponse = {
    code: number;
    err_msg: string;
};

export type ListRequest = {
    session_id: string;
    project_id: string;
    offset: number;
    limit: number;
};

export type ListResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    triger_list: TrigerInfo[];
};

export type GetRequest = {
    session_id: string;
    project_id: string;
    triger_id: string;
};

export type GetResponse = {
    code: number;
    err_msg: string;
    triger: TrigerInfo;
};

export type ListMsgRequest = {
    session_id: string;
    project_id: string;
    triger_id: string;
    filter_by_attr: boolean;
    attr_list: string[];
    offset: number;
    limit: number;
};

export type ListMsgResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    msg_list: TrigerMsgInfo[];
};


// 列出触发器类型
export async function list_type(request: ListTypeRequest): Promise<ListTypeResponse> {
    const cmd = 'plugin:project_triger_api|list_type';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListTypeResponse>(cmd, {
        request,
    });
}

// 获取单个触发器类型
export async function get_type(request: GetTypeRequest): Promise<GetTypeResponse> {
    const cmd = 'plugin:project_triger_api|get_type';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetTypeResponse>(cmd, {
        request,
    });
}

// 创建触发器
export async function create(request: CreateRequest): Promise<CreateResponse> {
    const cmd = 'plugin:project_triger_api|create';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<CreateResponse>(cmd, {
        request,
    });
}

// 更新触发器名称
export async function update_name(request: UpdateNameRequest): Promise<UpdateNameResponse> {
    const cmd = 'plugin:project_triger_api|update_name';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateNameResponse>(cmd, {
        request,
    });
}

// 重置触发器密钥
export async function reset_secret(request: ResetSecretRequest): Promise<ResetSecretResponse> {
    const cmd = 'plugin:project_triger_api|reset_secret';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ResetSecretResponse>(cmd, {
        request,
    });
}

// 删除触发器
export async function remove(request: RemoveRequest): Promise<RemoveResponse> {
    const cmd = 'plugin:project_triger_api|remove';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveResponse>(cmd, {
        request,
    });
}

// 列出触发器
export async function list(request: ListRequest): Promise<ListResponse> {
    const cmd = 'plugin:project_triger_api|list';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListResponse>(cmd, {
        request,
    });
}

// 获取单个触发器
export async function get(request: GetRequest): Promise<GetResponse> {
    const cmd = 'plugin:project_triger_api|get';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetResponse>(cmd, {
        request,
    });
}

// 列出接收消息
export async function list_msg(request: ListMsgRequest): Promise<ListMsgResponse> {
    const cmd = 'plugin:project_triger_api|list_msg';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListMsgResponse>(cmd, {
        request,
    });
}