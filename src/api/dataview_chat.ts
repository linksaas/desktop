//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';


export type LIST_TYPE = number;
export const LIST_TYPE_BEFORE: LIST_TYPE = 0;
export const LIST_TYPE_AFTER: LIST_TYPE = 1;

export type ChatMsgInfo = {
    msg_id: string;
    content: string;
    send_user_id: string;
    send_display_name: string;
    send_logo_uri: string;
    send_time: number;
    has_changed: boolean;
};

export type SendMsgRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
    content: string;
};

export type SendMsgResponse = {
    code: number;
    err_msg: string;
    msg_id: string;
};

export type ListMsgRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
    list_type: LIST_TYPE;
    list_ref_time: number;
    limit: number;
};

export type ListMsgResponse = {
    code: number;
    err_msg: string;
    msg_list: ChatMsgInfo[];
    last_msg_id: string;
};

export type UpdateMsgRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
    msg_id: string;
    content: string;
};

export type UpdateMsgResponse = {
    code: number;
    err_msg: string;
};

export type GetMsgRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
    msg_id: string;
};

export type GetMsgResponse = {
    code: number;
    err_msg: string;
    msg: ChatMsgInfo;
};

//发送消息
export async function send_msg(request: SendMsgRequest): Promise<SendMsgResponse> {
    const cmd = 'plugin:dataview_chat_api|send_msg';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<SendMsgResponse>(cmd, {
        request,
    });
}

//列出消息
export async function list_msg(request: ListMsgRequest): Promise<ListMsgResponse> {
    const cmd = 'plugin:dataview_chat_api|list_msg';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListMsgResponse>(cmd, {
        request,
    });
}

//修改消息
export async function update_msg(request: UpdateMsgRequest): Promise<UpdateMsgResponse> {
    const cmd = 'plugin:dataview_chat_api|update_msg';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateMsgResponse>(cmd, {
        request,
    });
}

//获得单条消息
export async function get_msg(request: GetMsgRequest): Promise<GetMsgResponse> {
    const cmd = 'plugin:dataview_chat_api|get_msg';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetMsgResponse>(cmd, {
        request,
    });
}