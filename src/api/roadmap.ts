//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export const DND_ITEM_TYPE = "node";

export type ROADMAP_OWNER_TYPE = number;
export const ROADMAP_OWNER_ADMIN: ROADMAP_OWNER_TYPE = 0;
export const ROADMAP_OWNER_USER: ROADMAP_OWNER_TYPE = 1;

export type BasicRoadmapInfo = {
    title: string;
    desc: string;
};

export type UserPerm = {
    can_update_content: boolean;
    can_update_pub_state: boolean;
    can_remove: boolean;
};

export type RoadmapInfo = {
    roadmap_id: string;
    basic_info: BasicRoadmapInfo;
    owner_type: ROADMAP_OWNER_TYPE;
    pub_state: boolean;
    owner_user_id: string;
    owner_display_name: string;
    owner_logo_uri: string;
    create_time: number;
    update_time: number;
    user_perm: UserPerm;
    hot_value: number;
    tag_list: string[];
};

export type CreateRequest = {
    session_id: string;
    basic_info: BasicRoadmapInfo;
};

export type CreateResponse = {
    code: number;
    err_msg: string;
    roadmap_id: string;
};

export type ListPubRequest = {
    session_id: string;
    filter_by_keyword: boolean;
    keyword: string;
    filter_by_tag: boolean;
    tag: string;
    offset: number;
    limit: number;
};

export type ListPubResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    roadmap_list: RoadmapInfo[];
};

export type ListMyRequest = {
    session_id: string;
    filter_by_keyword: boolean;
    keyword: string;
    filter_by_tag: boolean;
    tag: string;
    offset: number;
    limit: number;
};

export type ListMyResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    roadmap_list: RoadmapInfo[];
};

export type GetRequest = {
    session_id: string;
    roadmap_id: string;
};

export type GetResponse = {
    code: number;
    err_msg: string;
    roadmap: RoadmapInfo;
};

export type UpdateRequest = {
    session_id: string;
    roadmap_id: string;
    basic_info: BasicRoadmapInfo;
};

export type UpdateResponse = {
    code: number;
    err_msg: string;
};

export type UpdatePubStateRequest = {
    session_id: string;
    roadmap_id: string;
    pub_state: boolean;
};

export type UpdatePubStateResponse = {
    code: number;
    err_msg: string;
};

export type RemoveRequest = {
    session_id: string;
    roadmap_id: string;
};

export type RemoveResponse = {
    code: number;
    err_msg: string;
};

//创建路线图
export async function create(request: CreateRequest): Promise<CreateResponse> {
    const cmd = 'plugin:roadmap_api|create';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<CreateResponse>(cmd, {
        request,
    });
}

//列出路线图(发布状态)
export async function list_pub(request: ListPubRequest): Promise<ListPubResponse> {
    const cmd = 'plugin:roadmap_api|list_pub';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListPubResponse>(cmd, {
        request,
    });
}

//列出我的路线图
export async function list_my(request: ListMyRequest): Promise<ListMyResponse> {
    const cmd = 'plugin:roadmap_api|list_my';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListMyResponse>(cmd, {
        request,
    });
}

//获得单个路线图
export async function get(request: GetRequest): Promise<GetResponse> {
    const cmd = 'plugin:roadmap_api|get';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetResponse>(cmd, {
        request,
    });
}

//更新路线图
export async function update(request: UpdateRequest): Promise<UpdateResponse> {
    const cmd = 'plugin:roadmap_api|update';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateResponse>(cmd, {
        request,
    });
}

//更新发布状态
export async function update_pub_state(request: UpdatePubStateRequest): Promise<UpdatePubStateResponse> {
    const cmd = 'plugin:roadmap_api|update_pub_state';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdatePubStateResponse>(cmd, {
        request,
    });
}

//删除路线图
export async function remove(request: RemoveRequest): Promise<RemoveResponse> {
    const cmd = 'plugin:roadmap_api|remove';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveResponse>(cmd, {
        request,
    });
}