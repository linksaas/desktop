//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export type ENTRY_TYPE = number;
export const ENTRY_TYPE_SPRIT: ENTRY_TYPE = 0;
export const ENTRY_TYPE_DOC: ENTRY_TYPE = 1;
export const ENTRY_TYPE_PAGES: ENTRY_TYPE = 2;
// export const ENTRY_TYPE_BOARD: ENTRY_TYPE = 3;
// export const ENTRY_TYPE_FILE: ENTRY_TYPE = 4;
export const ENTRY_TYPE_API_COLL: ENTRY_TYPE = 5;
// export const ENTRY_TYPE_DATA_ANNO: ENTRY_TYPE = 6;
export const ENTRY_TYPE_DRAW: ENTRY_TYPE = 7;

//特殊内容入口
export const ENTRY_TYPE_KNOWLEDGE: ENTRY_TYPE = 902; //知识沉淀，包含ENTRY_TYPE_DOC，ENTRY_TYPE_PAGES，ENTRY_TYPE_API_COLL，ENTRY_TYPE_DRAW
export const ENTRY_TYPE_DATAVIEW: ENTRY_TYPE = 903;


export type ISSUE_LIST_TYPE = number;
export const ISSUE_LIST_ALL: ISSUE_LIST_TYPE = 0;
export const ISSUE_LIST_LIST: ISSUE_LIST_TYPE = 1;
export const ISSUE_LIST_KANBAN: ISSUE_LIST_TYPE = 2;

export type API_COLL_TYPE = number;
export const API_COLL_GRPC: API_COLL_TYPE = 0;
export const API_COLL_OPENAPI: API_COLL_TYPE = 1;
export const API_COLL_CUSTOM: API_COLL_TYPE = 2;

export type EntryTag = {
    tag_id: string;
    tag_name: string;
    bg_color: string;
};

export type EntryPerm = {
    update_for_all: boolean;
    extra_update_user_id_list: string[];
};

export type ExtraSpritInfo = {
    start_time: number;
    end_time: number;
    non_work_day_list: number[];
    issue_list_type: ISSUE_LIST_TYPE;
    hide_gantt_panel: boolean;
    hide_burndown_panel: boolean;
    hide_stat_panel: boolean;
    hide_summary_panel: boolean;
    hide_test_plan_panel: boolean;
};

export type ExtraPagesInfo = {
    file_id: string;
};


export type ExtraApiCollInfo = {
    api_coll_type: API_COLL_TYPE;
    default_addr: string;
};

export type ExtraInfo = {
    ExtraSpritInfo?: ExtraSpritInfo;
    ExtraPagesInfo?: ExtraPagesInfo;
    ExtraApiCollInfo?: ExtraApiCollInfo;
};

export type WatchUser = {
    member_user_id: string;
    display_name: string;
    logo_uri: string;
};

export type EntryInfo = {
    entry_id: string;
    entry_type: ENTRY_TYPE;
    entry_title: string;
    my_watch: boolean;
    watch_user_list: WatchUser[];
    tag_list: EntryTag[];
    entry_perm: EntryPerm;
    create_user_id: string;
    create_display_name: string;
    create_logo_uri: string;
    create_time: number;
    update_user_id: string;
    update_display_name: string;
    update_logo_uri: string;
    update_time: number;
    can_update: boolean;
    can_remove: boolean;
    extra_info: ExtraInfo;
};

export type ListParam = {
    filter_by_watch: boolean;
    filter_by_tag_id: boolean;
    tag_id_list: string[];
    filter_by_keyword: boolean;
    keyword: string;
    filter_by_entry_type: boolean;
    entry_type_list: ENTRY_TYPE[];
};

export type CreateRequest = {
    session_id: string;
    project_id: string;
    entry_id: string;
    entry_type: ENTRY_TYPE;
    entry_title: string;
    tag_id_list: string[];
    entry_perm: EntryPerm;
    extra_info?: ExtraInfo;
};

export type CreateResponse = {
    code: number;
    err_msg: string;
};

export type ListRequest = {
    session_id: string;
    project_id: string;
    list_param: ListParam;
    offset: number;
    limit: number;
};

export type ListResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    entry_list: EntryInfo[];
};


export type GetRequest = {
    session_id: string;
    project_id: string;
    entry_id: string;
};

export type GetResponse = {
    code: number;
    err_msg: string;
    entry: EntryInfo;
};

export type RemoveRequest = {
    session_id: string;
    project_id: string;
    entry_id: string;
};

export type RemoveResponse = {
    code: number;
    err_msg: string;
};

export type UpdateTagRequest = {
    session_id: string;
    project_id: string;
    entry_id: string;
    tag_id_list: string[];
};

export type UpdateTagResponse = {
    code: number;
    err_msg: string;
};

export type UpdateTitleRequest = {
    session_id: string;
    project_id: string;
    entry_id: string;
    title: string;
};

export type UpdateTitleResponse = {
    code: number;
    err_msg: string;
};

export type UpdatePermRequest = {
    session_id: string;
    project_id: string;
    entry_id: string;
    entry_perm: EntryPerm;
};

export type UpdatePermResponse = {
    code: number;
    err_msg: string;
};

export type UpdateExtraInfoRequest = {
    session_id: string;
    project_id: string;
    entry_id: string;
    extra_info: ExtraInfo;
};

export type UpdateExtraInfoResponse = {
    code: number;
    err_msg: string;
};

//创建入口
export async function create(request: CreateRequest): Promise<CreateResponse> {
    const cmd = 'plugin:project_entry_api|create';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<CreateResponse>(cmd, {
        request,
    });
}

//列出入口
export async function list(request: ListRequest): Promise<ListResponse> {
    const cmd = 'plugin:project_entry_api|list';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListResponse>(cmd, {
        request,
    });
}

//获取入口
export async function get(request: GetRequest): Promise<GetResponse> {
    const cmd = 'plugin:project_entry_api|get';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetResponse>(cmd, {
        request,
    });
}

//删除入口
export async function remove(request: RemoveRequest): Promise<RemoveResponse> {
    const cmd = 'plugin:project_entry_api|remove';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveResponse>(cmd, {
        request,
    });
}

//更新标签
export async function update_tag(request: UpdateTagRequest): Promise<UpdateTagResponse> {
    const cmd = 'plugin:project_entry_api|update_tag';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateTagResponse>(cmd, {
        request,
    });
}

//更新标题
export async function update_title(request: UpdateTitleRequest): Promise<UpdateTitleResponse> {
    const cmd = 'plugin:project_entry_api|update_title';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateTitleResponse>(cmd, {
        request,
    });
}

//更新权限
export async function update_perm(request: UpdatePermRequest): Promise<UpdatePermResponse> {
    const cmd = 'plugin:project_entry_api|update_perm';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdatePermResponse>(cmd, {
        request,
    });
}

//更新额外信息
export async function update_extra_info(request: UpdateExtraInfoRequest): Promise<UpdateExtraInfoResponse> {
    const cmd = 'plugin:project_entry_api|update_extra_info';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateExtraInfoResponse>(cmd, {
        request,
    });
}