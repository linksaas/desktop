//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';
import type { FeedBackInfo } from './feedback';

export type AdminListRequest = {
    admin_session_id: string;
    offset: number;
    limit: number;
};

export type AdminListResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    info_list: FeedBackInfo[];
}

export type AdminRemoveRequest = {    
    admin_session_id: string;
    feed_back_id: string;
};

export type AdminRemoveResponse = {
    code: number;
    err_msg: string;
}

//列出反馈
export async function list(request: AdminListRequest): Promise<AdminListResponse> {
    const cmd = 'plugin:feedback_admin_api|list';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AdminListResponse>(cmd, {
        request,
    });
}

//删除反馈
export async function remove(request: AdminRemoveRequest): Promise<AdminRemoveResponse> {
    const cmd = 'plugin:feedback_admin_api|remove';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AdminRemoveResponse>(cmd, {
        request,
    });
}