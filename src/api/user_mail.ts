//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export type MailAccount = {
    account: string;
    user_id: string;
    remark: string;    
    time_stamp: number;
};

export type MailInfo = {
    mail_id: string;
    user_id: string;
    from_account: string;
    to_account: string;
    title: string;
    time_stamp: number;    
    has_read: boolean;
};

export type CreateAccountRequest ={    
    session_id: string;
};

export type CreateAccountResponse ={
    code: number;
    err_msg: string;    
    account: string;
};

export type UpdateAccountRequest ={
    session_id: string;
    account: string;    
    remark: string;
};

export type UpdateAccountResponse ={
    code: number;    
    err_msg: string;
};

export type ListAccountRequest ={    
    session_id: string;
};

export type ListAccountResponse = {
    code: number;
    err_msg: string;    
    account_list: MailAccount[];
};

export type RemoveAccountRequest = {
    session_id: string;    
    account: string;
};

export type RemoveAccountResponse = {
    code: number;    
    err_msg: string;
};

export type GetMailStatusRequest = {    
    session_id: string;
};

export type GetMailStatusResponse = {
    code: number;
    err_msg: string;
    all_count: number;    
    has_read_count: number;
};

export type ListMailRequest = {
    session_id: string;
    filter_by_has_read: boolean;
    has_read: boolean;
    offset: number;    
    limit: number;
};

export type ListMailResponse = {
    code: number;
    err_msg: string;
    total_count: number;    
    info_list: MailInfo[];
};

export type GetMailDetailRequest = {
    session_id: string;    
    mail_id: string;
};

export type GetMailDetailResponse = {
    code: number;
    err_msg: string;
    info: MailInfo;
    text_body: string;    
    html_body: string;
};

export type RemoveMailRequest = {
    session_id: string;    
    mail_id: string;
};

export type RemoveMailResponse = {
    code: number;    
    err_msg: string;
};

export type SetMailStateRequest = {
    session_id: string;
    mail_id: string;    
    has_read: boolean;
};

export type SetMailStateResponse = {
    code: number;  
    err_msg: string;
};

//创建账号
export async function create_account(request: CreateAccountRequest): Promise<CreateAccountResponse> {
    const cmd = 'plugin:user_mail_api|create_account';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<CreateAccountResponse>(cmd, {
        request,
    });
}

//更新账号
export async function update_account(request: UpdateAccountRequest): Promise<UpdateAccountResponse> {
    const cmd = 'plugin:user_mail_api|update_account';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateAccountResponse>(cmd, {
        request,
    });
}

//列出账号
export async function list_account(request: ListAccountRequest): Promise<ListAccountResponse> {
    const cmd = 'plugin:user_mail_api|list_account';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListAccountResponse>(cmd, {
        request,
    });
}

//删除账号
export async function remove_account(request: RemoveAccountRequest): Promise<RemoveAccountResponse> {
    const cmd = 'plugin:user_mail_api|remove_account';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveAccountResponse>(cmd, {
        request,
    });
}

//获取邮件数量
export async function get_mail_status(request: GetMailStatusRequest): Promise<GetMailStatusResponse> {
    const cmd = 'plugin:user_mail_api|get_mail_status';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetMailStatusResponse>(cmd, {
        request,
    });
}

//列出邮件
export async function list_mail(request: ListMailRequest): Promise<ListMailResponse> {
    const cmd = 'plugin:user_mail_api|list_mail';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListMailResponse>(cmd, {
        request,
    });
}

//获取邮件详情
export async function get_mail_detail(request: GetMailDetailRequest): Promise<GetMailDetailResponse> {
    const cmd = 'plugin:user_mail_api|get_mail_detail';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetMailDetailResponse>(cmd, {
        request,
    });
}

//删除邮件
export async function remove_mail(request: RemoveMailRequest): Promise<RemoveMailResponse> {
    const cmd = 'plugin:user_mail_api|remove_mail';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveMailResponse>(cmd, {
        request,
    });
}

//设置邮件状态
export async function set_mail_state(request: SetMailStateRequest): Promise<SetMailStateResponse> {
    const cmd = 'plugin:user_mail_api|set_mail_state';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<SetMailStateResponse>(cmd, {
        request,
    });
}