//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export type BaseDoc = {
    content: string;
};

export type Doc = {
    doc_id: string;
    base_info: BaseDoc;
};

export type DocHistory = {
    history_id: string;
    time_stamp: number;
    update_user_id: string;
    update_display_name: string;
    update_logo_uri: string;
};

export type CreateDocRequest = {
    session_id: string;
    project_id: string;
    base_info: BaseDoc;
};

export type CreateDocResponse = {
    code: number;
    err_msg: string;
    doc_id: string;
};

export type StartUpdateDocRequest = {
    session_id: string;
    project_id: string;
    doc_id: string;
};

export type StartUpdateDocResponse = {
    code: number;
    err_msg: string;
};

export type KeepUpdateDocRequest = {
    session_id: string;
    doc_id: string;
};

export type KeepUpdateDocResponse = {
    code: number;
    err_msg: string;
    keep_update: boolean;
};

export type EndUpdateDocRequest = {
    session_id: string;
    doc_id: string;
};

export type EndUpdateDocResponse = {
    code: number;
    err_msg: string;
};

export type UpdateDocContentRequest = {
    session_id: string;
    project_id: string;
    doc_id: string;
    content: string;
};

export type UpdateDocContentResponse = {
    code: number;
    err_msg: string;
};

export type GetDocRequest = {
    session_id: string;
    project_id: string;
    doc_id: string;
};

export type GetDocResponse = {
    code: number;
    err_msg: string;
    doc: Doc;
    idea_keyword_list: string[];
};

//创建文档
export async function create_doc(request: CreateDocRequest): Promise<CreateDocResponse> {
    const cmd = 'plugin:project_doc_api|create_doc';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<CreateDocResponse>(cmd, {
        request,
    });
}

//开始更新文档内容
export async function start_update_doc(request: StartUpdateDocRequest): Promise<StartUpdateDocResponse> {
    const cmd = 'plugin:project_doc_api|start_update_doc';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<StartUpdateDocResponse>(cmd, {
        request,
    });
}

//持续保持更新文档状态(心跳)
export async function keep_update_doc(request: KeepUpdateDocRequest): Promise<KeepUpdateDocResponse> {
    const cmd = 'plugin:project_doc_api|keep_update_doc';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<KeepUpdateDocResponse>(cmd, {
        request,
    });
}

//结束更新文档
export async function end_update_doc(request: EndUpdateDocRequest): Promise<EndUpdateDocResponse> {
    const cmd = 'plugin:project_doc_api|end_update_doc';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<EndUpdateDocResponse>(cmd, {
        request,
    });
}

//更新文档内容
export async function update_doc_content(request: UpdateDocContentRequest): Promise<UpdateDocContentResponse> {
    const cmd = 'plugin:project_doc_api|update_doc_content';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateDocContentResponse>(cmd, {
        request,
    });
}

//获取文档
export async function get_doc(request: GetDocRequest): Promise<GetDocResponse> {
    const cmd = 'plugin:project_doc_api|get_doc';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetDocResponse>(cmd, {
        request,
    });
}