//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { Command } from '@tauri-apps/api/shell';

export interface AccountSummaryData {
    Policies: number;
    Roles: number;
    Idps: number;
    User: number;
    Group: number;
    IdentityProviders: number;
    RequestId: string;
}

export interface AccountSummaryResponse {
    Response:AccountSummaryData,
}

// {
//     "Response": {
//         "Policies": 0,
//         "Roles": 13,
//         "Idps": 0,
//         "User": 4,
//         "Group": 0,
//         "Member": 0,
//         "IdentityProviders": 0,
//         "RequestId": "07d81874-6239-4cdd-825d-6533d0aeac15"
//     }
// }

export async function getAccountSummary(secretId: string, secretKey: string): Promise<AccountSummaryResponse> {
    const command = Command.sidecar('bin/linksaas_tencloud', ["--secretId", secretId, "--secretKey", secretKey, "cam", "getAccountSummary"]);
    const result = await command.execute();
    if (result.code != 0) {
        throw new Error(result.stdout);
    }
    return JSON.parse(result.stdout) as AccountSummaryResponse;
}