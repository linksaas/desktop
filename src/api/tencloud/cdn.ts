//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { Command } from '@tauri-apps/api/shell';
import { Moment } from 'moment';

export interface CdnDomainInfo {
    ResourceId: string;
    AppId: number;
    Domain: string;
    Cname: string;
    Status: string;
    ServiceType: string;
    CreateTime: string;
    UpdateTime: string;
    Origin: {
        Origins: string[];
        OriginType: string;
        ServerName: string;
    };
}

export interface ListDomainData {
    Domains: CdnDomainInfo[];
}

export interface ListDomainResponse {
    Response: ListDomainData
}

// {
//     "Response": {
//         "Domains": [
//             {
//                 "ResourceId": "cdn-bpue3hh4",
//                 "AppId": 1304919705,
//                 "Domain": "file.linksaas.pro",
//                 "Cname": "file.linksaas.pro.cdn.dnsv1.com",
//                 "Status": "online",
//                 "ProjectId": 0,
//                 "ServiceType": "download",
//                 "CreateTime": "2024-03-08 16:58:34",
//                 "UpdateTime": "2024-03-08 17:06:50",
//                 "Origin": {
//                     "Origins": [
//                         "www.linksaas.pro:443"
//                     ],
//                     "OriginType": "domain",
//                     "ServerName": "www.linksaas.pro",
//                     "CosPrivateAccess": "off",
//                     "OriginPullProtocol": "https",
//                     "BackupOrigins": [],
//                     "PathRules": [],
//                     "PathBasedOrigin": [],
//                     "Sni": {
//                         "Switch": "off"
//                     },
//                     "AdvanceHttps": {
//                         "CustomTlsStatus": "off",
//                         "TlsVersion": [
//                             "TLSv1",
//                             "TLSv1.1",
//                             "TLSv1.2"
//                         ],
//                         "Cipher": "DEFAULT",
//                         "VerifyOriginType": "off"
//                     }
//                 },
//                 "Disable": "normal",
//                 "Area": "mainland",
//                 "Readonly": "normal",
//                 "Product": "cdn",
//                 "ParentHost": ""
//             }
//         ],
//         "TotalNumber": 1,
//         "RequestId": "4be04f78-703c-42c0-b05a-7a7e70a6efe5"
//     }
// }

export interface CdnDataItem {
    Metric: string;
    SummarizedData: {
        Value: number;
    };
}

export interface QueryDataItem {
    Resource: string;
    CdnData: CdnDataItem[];
}

export interface QueryDataData {
    Data: QueryDataItem[],
}

export interface QueryDataResponse {
    Response: QueryDataData;
}

// {
//     "Response": {
//         "Interval": "day",
//         "Data": [
//             {
//                 "Resource": "multiDomains",
//                 "CdnData": [
//                     {
//                         "Metric": "flux",
//                         "DetailData": [
//                             {
//                                 "Time": "2024-09-16 00:00:00",
//                                 "Value": 72254
//                             }
//                         ],
//                         "SummarizedData": {
//                             "Name": "sum",
//                             "Value": 72254
//                         }
//                     }
//                 ]
//             }
//         ],
//         "RequestId": "1745b34c-dc0c-4ba8-a26d-6101bd611690"
//     }
// }

export async function listDomain(secretId: string, secretKey: string, domainList: string[] = []): Promise<ListDomainResponse> {
    const args = ["--secretId", secretId, "--secretKey", secretKey, "cdn", "listDomain"]
    for (const domain of domainList) {
        args.push("--domain");
        args.push(domain);
    }
    const command = Command.sidecar('bin/linksaas_tencloud', args);
    const result = await command.execute();
    if (result.code != 0) {
        throw new Error(result.stdout);
    }
    return JSON.parse(result.stdout) as ListDomainResponse;
}

export async function queryData(secretId: string, secretKey: string, domain: string, metric: "flux" | "request", startTime: Moment, endTime: Moment): Promise<QueryDataResponse> {
    const startTimeStr = startTime.format("YYYY-MM-DD");
    const endTimeStr = endTime.format("YYYY-MM-DD");
    const command = Command.sidecar('bin/linksaas_tencloud', ["--secretId", secretId, "--secretKey", secretKey, "cdn", "queryData", domain, metric, "day", startTimeStr, endTimeStr]);
    const result = await command.execute();
    if (result.code != 0) {
        throw new Error(result.stdout);
    }
    return JSON.parse(result.stdout) as QueryDataResponse;
}