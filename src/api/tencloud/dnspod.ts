//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { Command } from '@tauri-apps/api/shell';

export interface DnsDomainInfo {
    DomainId: number;
    Name: string;
    Status: string;
    TTL: number;
    CreatedOn: string;
    UpdatedOn: string;
}

export interface ListDomainData {
    DomainList: DnsDomainInfo[];
}

export interface ListDomainResponse {
    Response: ListDomainData
}

// {
//     "Response": {
//         "DomainCountInfo": {
//             "DomainTotal": 1,
//             "AllTotal": 1,
//             "MineTotal": 1,
//             "ShareTotal": 0,
//             "VipTotal": 1,
//             "PauseTotal": 0,
//             "ErrorTotal": 0,
//             "LockTotal": 0,
//             "SpamTotal": 0,
//             "VipExpire": 0,
//             "ShareOutTotal": 0,
//             "GroupTotal": 0
//         },
//         "DomainList": [
//             {
//                 "DomainId": 92366144,
//                 "Name": "linksaas.pro",
//                 "Status": "ENABLE",
//                 "TTL": 600,
//                 "CNAMESpeedup": "DISABLE",
//                 "DNSStatus": "",
//                 "Grade": "DP_PLUS",
//                 "GroupId": 1,
//                 "SearchEnginePush": "NO",
//                 "Remark": "",
//                 "Punycode": "linksaas.pro",
//                 "EffectiveDNS": [
//                     "ns3.dnsv2.com",
//                     "ns4.dnsv2.com"
//                 ],
//                 "GradeLevel": 3,
//                 "GradeTitle": "专业版",
//                 "IsVip": "YES",
//                 "VipStartAt": "2022-09-29 10:06:48",
//                 "VipEndAt": "2025-09-29 10:06:48",
//                 "VipAutoRenew": "DEFAULT",
//                 "RecordCount": 18,
//                 "CreatedOn": "2022-08-28 09:15:09",
//                 "UpdatedOn": "2024-09-09 15:22:33",
//                 "Owner": "qcloud_uin_100006441360@qcloud.com",
//                 "TagList": []
//             }
//         ],
//         "RequestId": "5bd668ed-937b-4c99-9c7e-e0ef4343d36d"
//     }
// }

export interface DnsRecord {
    RecordId: number;
    Name: string;
    Value: string;
    Status: string;
    UpdatedOn: string;
    Line: string;
    LineId: string;
    Type: string;
    MonitorStatus: string;
    TTL: number;
    MX: number;
    DefaultNS: boolean;
}

export interface ListRecordData {
    RecordList: DnsRecord[];
}

export interface ListRecordResponse {
    Response: ListRecordData;
}

// {
//     "Response": {
//         "RecordCountInfo": {
//             "SubdomainCount": 20,
//             "ListCount": 20,
//             "TotalCount": 20
//         },
//         "RecordList": [
//             {
//                 "RecordId": 1188621176,
//                 "Value": "ns3.dnsv2.com.",
//                 "Status": "ENABLE",
//                 "UpdatedOn": "2022-08-28 09:15:09",
//                 "Name": "@",
//                 "Line": "默认",
//                 "LineId": "0",
//                 "Type": "NS",
//                 "MonitorStatus": "",
//                 "Remark": "",
//                 "TTL": 86400,
//                 "MX": 0,
//                 "DefaultNS": true
//             },
//             {
//                 "RecordId": 1188621179,
//                 "Value": "ns4.dnsv2.com.",
//                 "Status": "ENABLE",
//                 "UpdatedOn": "2022-08-28 09:15:10",
//                 "Name": "@",
//                 "Line": "默认",
//                 "LineId": "0",
//                 "Type": "NS",
//                 "MonitorStatus": "",
//                 "Remark": "",
//                 "TTL": 86400,
//                 "MX": 0,
//                 "DefaultNS": true
//             },
//             {
//                 "RecordId": 1188733290,
//                 "Value": "mxbiz1.qq.com.",
//                 "Status": "ENABLE",
//                 "UpdatedOn": "2022-08-28 13:24:16",
//                 "Name": "@",
//                 "Line": "默认",
//                 "LineId": "0",
//                 "Type": "MX",
//                 "MonitorStatus": "",
//                 "Remark": "",
//                 "TTL": 600,
//                 "MX": 5,
//                 "DefaultNS": false
//             },
//             {
//                 "RecordId": 1188733629,
//                 "Value": "mxbiz2.qq.com.",
//                 "Status": "ENABLE",
//                 "UpdatedOn": "2022-08-28 13:24:35",
//                 "Name": "@",
//                 "Line": "默认",
//                 "LineId": "0",
//                 "Type": "MX",
//                 "MonitorStatus": "",
//                 "Remark": "",
//                 "TTL": 600,
//                 "MX": 10,
//                 "DefaultNS": false
//             },
//             {
//                 "RecordId": 1188735033,
//                 "Value": "8E325B8C2502F23C3979AD375B8AB587.A96B30B25FACF03EF1A24266372D72FA.TTDt6btamq.trust-provider.com.",
//                 "Status": "ENABLE",
//                 "UpdatedOn": "2022-08-28 13:27:03",
//                 "Name": "_c031a9c713cb946a218eb36cb487bd9b",
//                 "Line": "默认",
//                 "LineId": "0",
//                 "Type": "CNAME",
//                 "MonitorStatus": "",
//                 "Remark": "",
//                 "TTL": 600,
//                 "MX": 0,
//                 "DefaultNS": false
//             },
//             {
//                 "RecordId": 1188798824,
//                 "Value": "v=spf1 include:spf.mail.qq.com ~all",
//                 "Status": "ENABLE",
//                 "UpdatedOn": "2022-08-28 15:36:49",
//                 "Name": "@",
//                 "Line": "默认",
//                 "LineId": "0",
//                 "Type": "TXT",
//                 "MonitorStatus": "",
//                 "Remark": "",
//                 "TTL": 600,
//                 "MX": 0,
//                 "DefaultNS": false
//             },
//             {
//                 "RecordId": 1193381259,
//                 "Value": "mxbiz1.qq.com.",
//                 "Status": "ENABLE",
//                 "UpdatedOn": "2022-09-05 13:03:12",
//                 "Name": "mail",
//                 "Line": "默认",
//                 "LineId": "0",
//                 "Type": "MX",
//                 "MonitorStatus": "",
//                 "Remark": "",
//                 "TTL": 600,
//                 "MX": 5,
//                 "DefaultNS": false
//             },
//             {
//                 "RecordId": 1193381770,
//                 "Value": "v=spf1 include:qcloudmail.com ~all",
//                 "Status": "ENABLE",
//                 "UpdatedOn": "2022-09-05 13:03:42",
//                 "Name": "mail",
//                 "Line": "默认",
//                 "LineId": "0",
//                 "Type": "TXT",
//                 "MonitorStatus": "",
//                 "Remark": "",
//                 "TTL": 600,
//                 "MX": 0,
//                 "DefaultNS": false
//             },
//             {
//                 "RecordId": 1193382059,
//                 "Value": "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDEruH2V2sjP1aDy1QjF91ANJ2RDWZyag9dltDXOuTVf8ftsJ9wwgsXwUWsj4zNb/dhjVXWTMcIijCWt54/Xfavv0H2TkT/Muwo5OR1bvlBAop7vmSbXbHYEEsNNhXV/nak33mvnJLX+z6LTFGAAhoVNPCRIvqKD+74r/ku2qMTPwIDAQAB",
//                 "Status": "ENABLE",
//                 "UpdatedOn": "2022-09-05 13:04:07",
//                 "Name": "qcloud._domainkey.mail",
//                 "Line": "默认",
//                 "LineId": "0",
//                 "Type": "TXT",
//                 "MonitorStatus": "",
//                 "Remark": "",
//                 "TTL": 600,
//                 "MX": 0,
//                 "DefaultNS": false
//             },
//             {
//                 "RecordId": 1193382279,
//                 "Value": "v=DMARC1; p=none",
//                 "Status": "ENABLE",
//                 "UpdatedOn": "2022-09-05 13:04:30",
//                 "Name": "_dmarc.mail",
//                 "Line": "默认",
//                 "LineId": "0",
//                 "Type": "TXT",
//                 "MonitorStatus": "",
//                 "Remark": "",
//                 "TTL": 600,
//                 "MX": 0,
//                 "DefaultNS": false
//             },
//             {
//                 "RecordId": 1202862588,
//                 "Value": "43.142.193.138",
//                 "Status": "ENABLE",
//                 "UpdatedOn": "2022-09-20 10:27:18",
//                 "Name": "@",
//                 "Line": "默认",
//                 "LineId": "0",
//                 "Type": "A",
//                 "MonitorStatus": "",
//                 "Remark": "",
//                 "TTL": 600,
//                 "MX": 0,
//                 "DefaultNS": false
//             },
//             {
//                 "RecordId": 1202862790,
//                 "Value": "43.142.193.138",
//                 "Status": "ENABLE",
//                 "UpdatedOn": "2022-09-20 10:27:32",
//                 "Name": "www",
//                 "Line": "默认",
//                 "LineId": "0",
//                 "Type": "A",
//                 "MonitorStatus": "",
//                 "Remark": "",
//                 "TTL": 600,
//                 "MX": 0,
//                 "DefaultNS": false
//             },
//             {
//                 "RecordId": 1208543008,
//                 "Value": "101.35.146.148",
//                 "Status": "ENABLE",
//                 "UpdatedOn": "2022-09-29 10:13:08",
//                 "Name": "serv",
//                 "Line": "默认",
//                 "LineId": "0",
//                 "Type": "A",
//                 "MonitorStatus": "",
//                 "Remark": "",
//                 "TTL": 600,
//                 "MX": 0,
//                 "DefaultNS": false
//             },
//             {
//                 "RecordId": 1208543568,
//                 "Value": "106.54.181.14",
//                 "Status": "ENABLE",
//                 "UpdatedOn": "2022-09-29 10:14:07",
//                 "Name": "serv",
//                 "Line": "默认",
//                 "LineId": "0",
//                 "Type": "A",
//                 "MonitorStatus": "",
//                 "Remark": "",
//                 "TTL": 600,
//                 "MX": 0,
//                 "DefaultNS": false
//             },
//             {
//                 "RecordId": 1555240048,
//                 "Value": "E95376C59961A86D8F93F683D507CA0B.FD4405F85A72BA8181B7848E4FB47C61.cmcdtcik1wzn6v.trust-provider.com.",
//                 "Status": "ENABLE",
//                 "UpdatedOn": "2023-07-31 10:24:24",
//                 "Name": "_e8f0ef4bf5dc9d281cacdf9c67c75e07",
//                 "Line": "默认",
//                 "LineId": "0",
//                 "Type": "CNAME",
//                 "MonitorStatus": "",
//                 "Remark": "",
//                 "TTL": 600,
//                 "MX": 0,
//                 "DefaultNS": false
//             },
//             {
//                 "RecordId": 1742829318,
//                 "Value": "6bfcb5e9446acf9bb252bb4b4628a611.d91eba084d5ef747d37fccea8400c199.cmcdtcpt4ra9b8.trust-provider.com.",
//                 "Status": "ENABLE",
//                 "UpdatedOn": "2024-03-08 17:00:56",
//                 "Name": "_21ae9e667770e4a61f0a555be04b499c.file",
//                 "Line": "默认",
//                 "LineId": "0",
//                 "Type": "CNAME",
//                 "MonitorStatus": "",
//                 "Remark": "",
//                 "TTL": 600,
//                 "MX": 0,
//                 "DefaultNS": false
//             },
//             {
//                 "RecordId": 1742833841,
//                 "Value": "file.linksaas.pro.cdn.dnsv1.com.",
//                 "Status": "ENABLE",
//                 "UpdatedOn": "2024-03-08 17:08:17",
//                 "Name": "file",
//                 "Line": "默认",
//                 "LineId": "0",
//                 "Type": "CNAME",
//                 "Weight": 1,
//                 "MonitorStatus": "",
//                 "Remark": "",
//                 "TTL": 600,
//                 "MX": 0,
//                 "DefaultNS": false
//             },
//             {
//                 "RecordId": 1809285887,
//                 "Value": "27d4c640e24bd851f4d73aa4fa17dfab.f80700428fd1e2413d0887660abcf8be.cmcdt3vfhhqkov.trust-provider.com.",
//                 "Status": "ENABLE",
//                 "UpdatedOn": "2024-07-02 11:32:06",
//                 "Name": "_49c337c27852abbd366ae9108075c962",
//                 "Line": "默认",
//                 "LineId": "0",
//                 "Type": "CNAME",
//                 "MonitorStatus": "",
//                 "Remark": "",
//                 "TTL": 600,
//                 "MX": 0,
//                 "DefaultNS": false
//             },
//             {
//                 "RecordId": 1850096447,
//                 "Value": "43.142.193.138",
//                 "Status": "ENABLE",
//                 "UpdatedOn": "2024-09-09 15:22:17",
//                 "Name": "share",
//                 "Line": "默认",
//                 "LineId": "0",
//                 "Type": "A",
//                 "MonitorStatus": "",
//                 "Remark": "",
//                 "TTL": 600,
//                 "MX": 0,
//                 "DefaultNS": false
//             },
//             {
//                 "RecordId": 1850096494,
//                 "Value": "283d5a122ce09ad4ed8a8153b374226a.5ec5168ea191d1c2247a8bae74940084.cmcdt3gclrviu8.trust-provider.com.",
//                 "Status": "ENABLE",
//                 "UpdatedOn": "2024-09-09 15:22:33",
//                 "Name": "_382272f45941f5b3eeabe6bab2f35cb8.share",
//                 "Line": "默认",
//                 "LineId": "0",
//                 "Type": "CNAME",
//                 "MonitorStatus": "",
//                 "Remark": "",
//                 "TTL": 600,
//                 "MX": 0,
//                 "DefaultNS": false
//             }
//         ],
//         "RequestId": "1a84e66c-587d-4539-8923-efb667ceff24"
//     }
// }

export async function listDomain(secretId: string, secretKey: string): Promise<ListDomainResponse> {
    const command = Command.sidecar('bin/linksaas_tencloud', ["--secretId", secretId, "--secretKey", secretKey, "dnspod", "listDomain"]);
    const result = await command.execute();
    if (result.code != 0) {
        throw new Error(result.stdout);
    }
    return JSON.parse(result.stdout) as ListDomainResponse;
}

export async function listRecord(secretId: string, secretKey: string, domain: string): Promise<ListRecordResponse> {
    const command = Command.sidecar('bin/linksaas_tencloud', ["--secretId", secretId, "--secretKey", secretKey, "dnspod", "listRecord", domain]);
    const result = await command.execute();
    if (result.code != 0) {
        throw new Error(result.stdout);
    }
    return JSON.parse(result.stdout) as ListRecordResponse;
}