//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { Command } from '@tauri-apps/api/shell';

export interface LighthouseInstance {
    InstanceId: string;
    BundleId: string;
    BlueprintId: string;
    CPU: number;
    Memory: number;
    InstanceName: string;
    SystemDisk: {
        DiskType: string;
        DiskSize: number;
        DiskId: string;
    };
    PrivateAddresses: string[];
    PublicAddresses: string[];
    InstanceState: string;
    CreatedTime: string;
    ExpiredTime: string;
    PlatformType: string;
    Platform: string;
    OsName: string;
    Zone: string;
}

export interface ListInstanceData {
    TotalCount: 1,
    InstanceSet: LighthouseInstance[],
}

export interface ListInstanceResponse {
    Response: ListInstanceData;
}

// {
//     "Response": {
//         "TotalCount": 1,
//         "InstanceSet": [
//             {
//                 "InstanceId": "lhins-cxuckbj7",
//                 "BundleId": "bundle_gen_04",
//                 "BlueprintId": "lhbp-flkras26",
//                 "CPU": 2,
//                 "Memory": 4,
//                 "InstanceName": "Ubuntu-YCho",
//                 "InstanceChargeType": "PREPAID",
//                 "SystemDisk": {
//                     "DiskType": "CLOUD_SSD",
//                     "DiskSize": 80,
//                     "DiskId": "lhdisk-fbs6h6y7"
//                 },
//                 "PrivateAddresses": [
//                     "10.0.20.14"
//                 ],
//                 "PublicAddresses": [
//                     "175.178.105.150"
//                 ],
//                 "InternetAccessible": {
//                     "InternetChargeType": "TRAFFIC_POSTPAID_BY_HOUR",
//                     "InternetMaxBandwidthOut": 8,
//                     "PublicIpAssigned": true
//                 },
//                 "RenewFlag": "NOTIFY_AND_MANUAL_RENEW",
//                 "LoginSettings": {
//                     "KeyIds": []
//                 },
//                 "InstanceState": "RUNNING",
//                 "Uuid": "9a3bec3c-02ab-4a72-9513-9f9cff9504f8",
//                 "LatestOperation": "RenewInstances",
//                 "LatestOperationState": "SUCCESS",
//                 "LatestOperationRequestId": "cb5dd495-27cc-4b60-b6cc-4ad92f006dc2",
//                 "CreatedTime": "2022-02-08T14:15:41Z",
//                 "ExpiredTime": "2025-02-08T14:15:41Z",
//                 "PlatformType": "LINUX_UNIX",
//                 "Platform": "UBUNTU",
//                 "OsName": "Ubuntu Server 20.04 LTS 64bit",
//                 "Zone": "ap-guangzhou-6",
//                 "Tags": [],
//                 "InstanceRestrictState": "NORMAL",
//                 "InitInvocationId": ""
//             }
//         ],
//         "RequestId": "6de1bded-8824-44ae-a01a-e0bce475c991"
//     }
// }

export async function listInstance(secretId: string, secretKey: string, region: string, instanceIdList: string[] = []): Promise<ListInstanceResponse> {
    const args = ["--secretId", secretId, "--secretKey", secretKey, "--region", region, "lighthouse", "listInstance"];
    for (const instanceId of instanceIdList) {
        args.push("--instance");
        args.push(instanceId);
    }
    const command = Command.sidecar('bin/linksaas_tencloud', args);
    const result = await command.execute();
    if (result.code != 0) {
        throw new Error(result.stdout);
    }
    return JSON.parse(result.stdout) as ListInstanceResponse;
}