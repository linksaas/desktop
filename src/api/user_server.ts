//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';
import type { APP_TYPE } from './appstore';

export type UserServerInfo = {
    id: string;
    name: string;
    server_type: APP_TYPE;
    server_info: string;
    project_id?: string;
};

export type SshServerInfo = {
    addr: string;
    username: string;
    usePrivKey: boolean;
    password: string;
    privKeyPath: string;
    privKeyPassword: string;
};

//支持mysql和postgres
export type SqlServerInfo = {
    driver: "mysql" | "postgres";
    addr: string;
    username: string;
    password: string;
};

export type MongoServerInfo = {
    addrs: string[];
    username: string;
    password: string;
    authSource: string;
    authMechanism: string;
    replicaSet: string;
    defaultDb: string;
};

export type RedisServerInfo = {
    addrs: string[];
    dbId: number;
    username: string;
    password: string;
};

export type GrpcServerInfo = {
    addr: string;
    rootPath: string;
    importPathList: string[];
    secure: boolean;
}

//增加服务器
export async function add_server(server: UserServerInfo): Promise<void> {
    return invoke<void>("plugin:user_server_api|add_server", {
        server,
    });
}

//更新服务器
export async function update_server(server: UserServerInfo): Promise<void> {
    return invoke<void>("plugin:user_server_api|update_server", {
        server,
    });
}

//删除服务器
export async function remove_server(id: string): Promise<void> {
    return invoke<void>("plugin:user_server_api|remove_server", {
        id,
    });
}

//列出服务器
export async function list_server(): Promise<UserServerInfo[]> {
    return invoke<UserServerInfo[]>("plugin:user_server_api|list_server", {});
}