//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export type DrawInfo = {
    draw_id: string;    
    content: string;
};

export type CreateDrawRequest = {
    session_id: string;    
    project_id: string;
};

export type CreateDrawResponse = {
    code: number;
    err_msg: string;
    draw_id: string;
};

export type StartUpdateDrawRequest = {
    session_id: string;
    project_id: string;    
    draw_id: string;
};

export type StartUpdateDrawResponse = {
    code: number;    
    err_msg: string;
};

export type KeepUpdateDrawRequest = {
    session_id: string;    
    draw_id: string;
};

export type KeepUpdateDrawResponse = {
    code: number;
    err_msg: string;    
    keep_update: boolean;
};

export type EndUpdateDrawRequest = {
    session_id: string;    
    draw_id: string;
};

export type EndUpdateDrawResponse = {
    code: number;    
    err_msg: string;
};

export type UpdateDrawRequest = {
    session_id: string;
    project_id: string;
    draw_id: string;    
    content: string;
};

export type UpdateDrawResponse = {
    code: number;    
    err_msg: string;
};

export type GetDrawRequest = {
    session_id: string;
    project_id: string;    
    draw_id: string;
};

export type GetDrawResponse = {
    code: number;
    err_msg: string;
    info: DrawInfo;
};

//创建绘图
export async function create_draw(request: CreateDrawRequest): Promise<CreateDrawResponse> {
    const cmd = 'plugin:project_draw_api|create_draw';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<CreateDrawResponse>(cmd, {
        request,
    });
}

//开始更新内容
export async function start_update_draw(request: StartUpdateDrawRequest): Promise<StartUpdateDrawResponse> {
    const cmd = 'plugin:project_draw_api|start_update_draw';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<StartUpdateDrawResponse>(cmd, {
        request,
    });
}

//持续更新内容
export async function keep_update_draw(request: KeepUpdateDrawRequest): Promise<KeepUpdateDrawResponse> {
    const cmd = 'plugin:project_draw_api|keep_update_draw';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<KeepUpdateDrawResponse>(cmd, {
        request,
    });
}

//结束更新内容
export async function end_update_draw(request: EndUpdateDrawRequest): Promise<EndUpdateDrawResponse> {
    const cmd = 'plugin:project_draw_api|end_update_draw';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<EndUpdateDrawResponse>(cmd, {
        request,
    });
}

//更新内容
export async function update_draw(request: UpdateDrawRequest): Promise<UpdateDrawResponse> {
    const cmd = 'plugin:project_draw_api|update_draw';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateDrawResponse>(cmd, {
        request,
    });
}

//获取内容
export async function get_draw(request: GetDrawRequest): Promise<GetDrawResponse> {
    const cmd = 'plugin:project_draw_api|get_draw';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetDrawResponse>(cmd, {
        request,
    });
}