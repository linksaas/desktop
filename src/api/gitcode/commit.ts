//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { fetch } from '@tauri-apps/api/http';

export interface GitCodeCommit {
    sha: string;
    html_url: string;
    commit: {
        committer: {
            name: string;
            date: string;
            email: string;
        };
        message: string;
    };
    //TODO
}

// [
// {
//     "url": "https://api.gitcode.com/api/v5/repos/linksaas/desktop/commits/91d81d8371110381577ee4d0eb7ff7930f799c00",
//     "sha": "91d81d8371110381577ee4d0eb7ff7930f799c00",
//     "html_url": "https://gitcode.com/linksaas/desktop/commits/detail/91d81d8371110381577ee4d0eb7ff7930f799c00",
//     "comments_url": "https://api.gitcode.com/api/v5/repos/linksaas/desktop/commits/91d81d8371110381577ee4d0eb7ff7930f799c00/comments",
//     "commit": {
//         "author": {
//             "name": "linksaas",
//             "date": "2024-12-05T09:42:24+08:00",
//             "email": "panleiming@linksaas.pro",
//             "login": "linksaas"
//         },
//         "committer": {
//             "name": "linksaas",
//             "date": "2024-12-05T09:42:24+08:00",
//             "email": "panleiming@linksaas.pro",
//             "login": "linksaas"
//         },
//         "tree": {
//             "sha": "182a76004f4cf609097cb509c02c3402a97f7037",
//             "url": "https://api.gitcode.com/api/v5/repos/linksaas/desktop/git/trees/182a76004f4cf609097cb509c02c3402a97f7037"
//         },
//         "message": "Merge branch 'release/0.9.61'"
//     },
//     "author": {
//         "name": "linksaas",
//         "email": "panleiming@linksaas.pro",
//         "id": 571598,
//         "login": "linksaas",
//         "type": "User"
//     },
//     "committer": {
//         "name": "linksaas",
//         "date": "2024-12-05T09:42:24+08:00",
//         "email": "panleiming@linksaas.pro",
//         "id": 571598,
//         "login": "linksaas",
//         "type": "User"
//     },
//     "parents": [
//         {
//             "sha": "a4aa1e29755942c7d2f125c696b0dbed37df6ce9",
//             "url": "https://api.gitcode.com/api/v5/repos/linksaas/desktop/commits/a4aa1e29755942c7d2f125c696b0dbed37df6ce9"
//         },
//         {
//             "sha": "2c720152b4b42c0204a4ca4b2314931001465fc3",
//             "url": "https://api.gitcode.com/api/v5/repos/linksaas/desktop/commits/2c720152b4b42c0204a4ca4b2314931001465fc3"
//         }
//     ]
// }
// ]

export async function list_commit(accessToken: string, orgPath: string, repoPath: string, sha: string): Promise<GitCodeCommit[]> {
    const reqUrl = `https://api.gitcode.com/api/v5/repos/${encodeURIComponent(orgPath)}/${encodeURIComponent(repoPath)}/commits?access_token=${encodeURIComponent(accessToken)}&sha=${encodeURIComponent(sha)}`;

    const res = await fetch<GitCodeCommit[]>(reqUrl, {
        method: "GET",
        timeout: 10,
    });

    if (res.ok && res.status == 200) {
        return res.data;
    } else {
        console.log(res);
        throw "error list commit";
    }
}