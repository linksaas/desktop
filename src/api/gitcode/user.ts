//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { fetch } from '@tauri-apps/api/http';
import type { GitCodeUser } from './common';

export async function get_self_info(accessToken: string): Promise<GitCodeUser> {
    const reqUrl = `https://api.gitcode.com/api/v5/user?access_token=${encodeURIComponent(accessToken)}`;
    const res = await fetch<GitCodeUser>(reqUrl, {
        method: "GET",
        timeout: 10,
    });

    if (res.ok && res.status < 300) {
        return res.data;
    } else {
        console.log(res);
        throw "error get user info";
    }
}