//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

export function get_username(username: string): string {
    if (username.startsWith("gitcode:")) {
        return username.substring("gitcode:".length);
    }
    return username;
}

export interface GitCodeCommiter {
    name: string;
    date: string;
    email: string;
}

export interface GitCodeUser {
    avatar_url: string;
    html_url: string;
    id: string;
    login: string;
    name: string;
}