//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { fetch } from '@tauri-apps/api/http';

export interface GitCodeTag {
    name: string;
    commit: {
        sha: string;
    };
}

export async function list_tag(accessToken: string, orgPath: string, repoPath: string): Promise<GitCodeTag[]> {
    const reqUrl = `https://api.gitcode.com/api/v5/repos/${encodeURIComponent(orgPath)}/${encodeURIComponent(repoPath)}/tags?access_token=${encodeURIComponent(accessToken)}&per_page=100`;
    const res = await fetch<GitCodeTag[]>(reqUrl, {
        method: "GET",
        timeout: 10,
    });

    if (res.ok && res.status < 300) {
        return res.data;
    } else {
        console.log(res);
        throw "error list tag";
    }
}