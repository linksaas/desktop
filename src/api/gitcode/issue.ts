//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { fetch } from '@tauri-apps/api/http';
import type { GitCodeUser } from './common';

export interface GitCodeIssue {
    id: number;
    html_url: string;
    number: number;
    state: string;
    title: string;
    user: GitCodeUser;
    assignee?: GitCodeUser;
    created_at: string;
    updated_at: string;
    finished_at: string;
}


export async function list_issue(accessToken: string, orgPath: string, repoPath: string): Promise<GitCodeIssue[]> {
    const reqUrl = `https://api.gitcode.com/api/v5/repos/${encodeURIComponent(orgPath)}/${encodeURIComponent(repoPath)}/issues?access_token=${encodeURIComponent(accessToken)}`;

    const res = await fetch<GitCodeIssue[]>(reqUrl, {
        method: "GET",
        timeout: 10,
    });

    if (res.ok && res.status == 200) {
        return res.data;
    } else {
        console.log(res);
        throw "error list issue";
    }
}

export async function list_my_issue(accessToken: string): Promise<GitCodeIssue[]> {
    const reqUrl = `https://api.gitcode.com/api/v5/user/issues?access_token=${encodeURIComponent(accessToken)}`;
    const res = await fetch<GitCodeIssue[]>(reqUrl, {
        method: "GET",
        timeout: 10,
    });

    if (res.ok && res.status == 200) {
        return res.data;
    } else {
        console.log(res);
        throw "error list my issue";
    }
}