//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { fetch } from '@tauri-apps/api/http';

export interface GitCodeOrg {
    id: number;
    login: string;
    path: string;
    name: string;
    url: string;
    description: string;
}

export async function list_org(accessToken: string): Promise<GitCodeOrg[]> {
    const res = await fetch<GitCodeOrg[]>(`https://api.gitcode.com/api/v5/users/orgs?access_token=${encodeURIComponent(accessToken)}&per_page=100`,
        {
            method: "GET",
            timeout: 10,
        });

    if (res.ok && res.status == 200) {
        return res.data;
    } else {
        console.log(res);
        throw "error list org";
    }
}