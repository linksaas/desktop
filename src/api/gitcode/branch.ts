//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { fetch } from '@tauri-apps/api/http';

export interface GitCodeBranch {
    name: string;
    protected: boolean;
    commit: {
        sha: string;
    };
}

export async function list_branch(accessToken: string, orgPath: string, repoPath: string):Promise<GitCodeBranch[]> {
    const reqUrl = `https://api.gitcode.com/api/v5/repos/${encodeURIComponent(orgPath)}/${encodeURIComponent(repoPath)}/branches?access_token=${encodeURIComponent(accessToken)}`;
    const res = await fetch<GitCodeBranch[]>(reqUrl, {
        method: "GET",
        timeout: 10,
    });

    if (res.ok && res.status == 200) {
        return res.data;
    } else {
        console.log(res);
        throw "error list branch";
    }
}