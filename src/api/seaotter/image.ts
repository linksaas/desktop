//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

interface StringMap<T> {
    [index: string]: T;
}

export type Config = {
    user: string;
    exposed_ports: StringMap<string>,
    env: string[];
    entrypoint: string[];
    cmd: string[];
    volumes: StringMap<string>,
    working_dir: string;
    labels: StringMap<string>,
};

export type ImageConfig = {
    created: number;
    author: string;
    architecture: string;
    os: string;
    config: Config;
};

/// 对应如下信息结构的公用信息
/// application/vnd.docker.distribution.manifest.v2+json
/// application/vnd.oci.image.manifest.v1+json

export type Layer = {
    media_type: string;
    digest: string;
    size: number;
};

export type ImageManifest = {
    config: ImageConfig;
    layers: Layer[];
};

/// 对应如下信息结构的公用信息
/// application/vnd.docker.distribution.manifest.list.v2+json
/// application/vnd.oci.image.index.v1+json

export type ManifestItem = {
    manifest: ImageManifest;
    architecture: string;
    os: string;
};

export type ImageManifestList = {
    manifests: ManifestItem[];
};

export type ImageReference = {
    group_name: string;
    image_name: string;
    reference: string;
    manifest_info: ImageManifestList;
    time_stamp: number;
    digest: string;
};

export type ImageInfo = {
    group_name: string;
    image_name: string;
    reference_count: number;
    create_time: number;
    update_time: number;
};

export type ListImageRequest = {
    access_token: string;
    remote_id: string;
    group_name: string;
    offset: number;
    limit: number;
};

export type ListImageResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    image_list: ImageInfo[];
};

export type ListAllImageRequest = {
    access_token: string;
    remote_id: string;
};

export type ListAllImageResponse = {
    code: number;
    err_msg: string;
    image_list: ImageInfo[];
};

export type GetImageRequest = {
    access_token: string;
    remote_id: string;
    group_name: string;
    image_name: string;
};

export type GetImageResponse = {
    code: number;
    err_msg: string;
    image: ImageInfo;
};

export type RemoveImageRequest = {
    access_token: string;
    remote_id: string;
    group_name: string;
    image_name: string;
    force: boolean;
};

export type RemoveImageResponse = {
    code: number;
    err_msg: string;
};

export type ListReferenceRequest = {
    access_token: string;
    remote_id: string;
    group_name: string;
    image_name: string;
    load_manifest: boolean;
    offset: number;
    limit: number;
}

export type ListReferenceResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    reference_list: ImageReference[];
};

export type GetReferenceRequest = {
    access_token: string;
    remote_id: string;
    group_name: string;
    image_name: string;
    reference: string;
};

export type GetReferenceResponse = {
    code: number;
    err_msg: string;
    reference: ImageReference;
}

export type RemoveReferenceRequest = {
    access_token: string;
    remote_id: string;
    group_name: string;
    image_name: string;
    reference: string;
};

export type RemoveReferenceResponse = {
    code: number;
    err_msg: string;
};


//列出镜像信息
export async function list_image(addr: string, request: ListImageRequest): Promise<ListImageResponse> {
    const cmd = 'plugin:seaotter_image_api|list_image';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListImageResponse>(cmd, {
        addr,
        request,
    });
}

//列出所有镜像信息
export async function list_all_image(addr: string, request: ListAllImageRequest): Promise<ListAllImageResponse> {
    const cmd = 'plugin:seaotter_image_api|list_all_image';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListAllImageResponse>(cmd, {
        addr,
        request,
    });
}

//获得单个镜像信息
export async function get_image(addr: string, request: GetImageRequest): Promise<GetImageResponse> {
    const cmd = 'plugin:seaotter_image_api|get_image';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetImageResponse>(cmd, {
        addr,
        request,
    });
}

//删除镜像信息
export async function remove_image(addr: string, request: RemoveImageRequest): Promise<RemoveImageResponse> {
    const cmd = 'plugin:seaotter_image_api|remove_image';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveImageResponse>(cmd, {
        addr,
        request,
    });
}

//列出镜像Reference
export async function list_reference(addr: string, request: ListReferenceRequest): Promise<ListReferenceResponse> {
    const cmd = 'plugin:seaotter_image_api|list_reference';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListReferenceResponse>(cmd, {
        addr,
        request,
    });
}

//获得单个镜像Reference
export async function get_reference(addr: string, request: GetReferenceRequest): Promise<GetReferenceResponse> {
    const cmd = 'plugin:seaotter_image_api|get_reference';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetReferenceResponse>(cmd, {
        addr,
        request,
    });
}

//删除镜像Reference
export async function remove_reference(addr: string, request: RemoveReferenceRequest): Promise<RemoveReferenceResponse> {
    const cmd = 'plugin:seaotter_image_api|remove_reference';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveReferenceResponse>(cmd, {
        addr,
        request,
    });
}