//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export type DomainInfo = {
    domain: string;
    http_port: number;
    enable_https: boolean;
    https_port: number;
};

export type GetDomainRequest = {
    access_token: string;
    remote_id: string;
};

export type GetDomainResponse = {
    code: number;
    err_msg: string;
    info: DomainInfo;
};

//获取域名信息
export async function get_domain(addr: string, request: GetDomainRequest): Promise<GetDomainResponse> {
    const cmd = 'plugin:seaotter_config_api|get_domain';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetDomainResponse>(cmd, {
        addr,
        request,
    });
}