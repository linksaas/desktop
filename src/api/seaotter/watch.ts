//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';


export type WatchInfo = {
    watch_id: string;
    secret: string;
    full_image_name_list: string[];
    create_time: number;
    update_time: number;
    enable: boolean;
    desc: string;
    change_count: number;
};

export type ChangeInfo = {
    change_id: string;
    watch_id: string;
    time_stamp: number;
    change_from_url: string;
    change_to_url: string;
    change_from_digest: string;
    change_to_digest: string;
    un_change_url_list: string[];
};

export type CreateRequest = {
    access_token: string;
    remote_id: string;
    full_image_name_list: string[];
    desc: string;
};

export type CreateResponse = {
    code: number;
    err_msg: string;
    watch_id: string;
};

export type ListRequest = {
    access_token: string;
    remote_id: string;
    offset: number;
    limit: number;
};

export type ListResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    info_list: WatchInfo[];
};

export type GetRequest = {
    access_token: string;
    remote_id: string;
    watch_id: string;
};

export type GetResponse = {
    code: number;
    err_msg: string;
    info: WatchInfo;
};

export type RemoveRequest = {
    access_token: string;
    remote_id: string;
    watch_id: string;
};

export type RemoveResponse = {
    code: number;
    err_msg: string;
};

export type ResetSecretRequest = {
    access_token: string;
    remote_id: string;
    watch_id: string;
};

export type ResetSecretResponse = {
    code: number;
    err_msg: string;
    secret: string;
};

export type UpdateRequest = {
    access_token: string;
    remote_id: string;
    watch_id: string;
    full_image_name_list: string[];
    desc: string;
};

export type UpdateResponse = {
    code: number;
    err_msg: string;
};

export type EnableRequest = {
    access_token: string;
    remote_id: string;
    watch_id: string;
};

export type EnableResponse = {
    code: number;
    err_msg: string;
};

export type DisableRequest = {
    access_token: string;
    remote_id: string;
    watch_id: string;
};

export type DisableResponse = {
    code: number;
    err_msg: string;
};

export type ListChangeRequest = {
    access_token: string;
    remote_id: string;
    watch_id: string;
    offset: number;
    limit: number;
};

export type ListChangeResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    change_list: ChangeInfo[];
};


export type TrigerChangeRequest = {
    access_token: string;
    remote_id: string;
    watch_id: string;
    change_id: string;
};

export type TrigerChangeResponse = {
    code: number;
    err_msg: string;
};


// 创建变更订阅
export async function create(addr: string, request: CreateRequest): Promise<CreateResponse> {
    const cmd = 'plugin:seaotter_watch_api|create';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<CreateResponse>(cmd, {
        addr,
        request,
    });
}

// 列出变更订阅
export async function list(addr: string, request: ListRequest): Promise<ListResponse> {
    const cmd = 'plugin:seaotter_watch_api|list';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListResponse>(cmd, {
        addr,
        request,
    });
}

// 获得单个变更订阅
export async function get(addr: string, request: GetRequest): Promise<GetResponse> {
    const cmd = 'plugin:seaotter_watch_api|get';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetResponse>(cmd, {
        addr,
        request,
    });
}

// 删除变更订阅
export async function remove(addr: string, request: RemoveRequest): Promise<RemoveResponse> {
    const cmd = 'plugin:seaotter_watch_api|remove';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveResponse>(cmd, {
        addr,
        request,
    });
}

// 重新生成Token
export async function reset_secret(addr: string, request: ResetSecretRequest): Promise<ResetSecretResponse> {
    const cmd = 'plugin:seaotter_watch_api|reset_secret';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ResetSecretResponse>(cmd, {
        addr,
        request,
    });
}

// 更新基本信息
export async function update(addr: string, request: UpdateRequest): Promise<UpdateResponse> {
    const cmd = 'plugin:seaotter_watch_api|update';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateResponse>(cmd, {
        addr,
        request,
    });
}

// 打开订阅
export async function enable(addr: string, request: EnableRequest): Promise<EnableResponse> {
    const cmd = 'plugin:seaotter_watch_api|enable';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<EnableResponse>(cmd, {
        addr,
        request,
    });
}

// 关闭订阅
export async function disable(addr: string, request: DisableRequest): Promise<DisableResponse> {
    const cmd = 'plugin:seaotter_watch_api|disable';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<DisableResponse>(cmd, {
        addr,
        request,
    });
}

// 列出变更
export async function list_change(addr: string, request: ListChangeRequest): Promise<ListChangeResponse> {
    const cmd = 'plugin:seaotter_watch_api|list_change';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListChangeResponse>(cmd, {
        addr,
        request,
    });
}

// 重新发送变更
export async function triger_change(addr: string, request: TrigerChangeRequest): Promise<TrigerChangeResponse> {
    const cmd = 'plugin:seaotter_watch_api|triger_change';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<TrigerChangeResponse>(cmd, {
        addr,
        request,
    });
}