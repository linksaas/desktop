//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

interface StringMap<T> {
    [index: string]: T;
}

export type ImageGroupMemberList = {
    member_user_id_for_view: string[];
    member_user_id_for_remove: string[];
}

export type ImageGroupPerm = {
    public_group: boolean;
    member_perm: StringMap<ImageGroupMemberList>;
    pull_secret_name_list: string[];
    push_secret_name_list: string[];
};

export type ImageGroupInfo = {
    group_name: string;
    perm_info: ImageGroupPerm;
    image_info_count: number;
    create_time: number;
    update_time: number;
};

export type CreateRequest = {
    access_token: string;
    remote_id: string;
    group_name: string;
    perm_info: ImageGroupPerm;
};

export type CreateResponse = {
    code: number;
    err_msg: string;
};

export type UpdatePermRequest = {
    access_token: string;
    remote_id: string;
    group_name: string;
    perm_info: ImageGroupPerm;
};

export type UpdatePermResponse = {
    code: number;
    err_msg: string;
};

export type RemoveRequest = {
    access_token: string;
    remote_id: string;
    group_name: string;
};

export type RemoveResponse = {
    code: number;
    err_msg: string;
};

export type ListRequest = {
    access_token: string;
    remote_id: string;
    offset: number;
    limit: number;
};

export type ListResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    group_list: ImageGroupInfo[];
};

export type GetRequest = {
    access_token: string;
    remote_id: string;
    group_name: string;
};

export type GetResponse = {
    code: number;
    err_msg: string;
    group: ImageGroupInfo;
};

//创建分组
export async function create(addr: string, request: CreateRequest): Promise<CreateResponse> {
    const cmd = 'plugin:seaotter_image_group_api|create';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<CreateResponse>(cmd, {
        addr,
        request,
    });
}

//更新分组权限
export async function update_perm(addr: string, request: UpdatePermRequest): Promise<UpdatePermResponse> {
    const cmd = 'plugin:seaotter_image_group_api|update_perm';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdatePermResponse>(cmd, {
        addr,
        request,
    });
}

//删除分组
export async function remove(addr: string, request: RemoveRequest): Promise<RemoveResponse> {
    const cmd = 'plugin:seaotter_image_group_api|remove';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveResponse>(cmd, {
        addr,
        request,
    });
}

//列出分组
export async function list(addr: string, request: ListRequest): Promise<ListResponse> {
    const cmd = 'plugin:seaotter_image_group_api|list';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListResponse>(cmd, {
        addr,
        request,
    });
}

//获得单个分组
export async function get(addr: string, request: GetRequest): Promise<GetResponse> {
    const cmd = 'plugin:seaotter_image_group_api|get';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetResponse>(cmd, {
        addr,
        request,
    });
}