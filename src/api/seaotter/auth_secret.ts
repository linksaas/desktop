//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export type AuthSecretInfo = {
    username: string;
    password: string;
};

export type AddRequest = {
    access_token: string;
    remote_id: string;
    auth_secret: AuthSecretInfo;
};

export type AddResponse = {
    code: number;
    err_msg: string;
}

export type RemoveRequest = {
    access_token: string;
    remote_id: string;
    username: string;
};

export type RemoveResponse = {
    code: number;
    err_msg: string;
};

export type UpdateRequest = {
    access_token: string;
    remote_id: string;
    auth_secret: AuthSecretInfo;
};

export type UpdateResponse = {
    code: number;
    err_msg: string;
};

export type GetRequest = {
    access_token: string;
    remote_id: string;
    username: string;
};

export type GetResponse = {
    code: number;
    err_msg: string;
    auth_secret: AuthSecretInfo;
};

export type ListRequest = {
    access_token: string;
    remote_id: string;
};

export type ListResponse = {
    code: number;
    err_msg: string;
    auth_secret_list: AuthSecretInfo[];
};

export type ListNameRequest = {
    access_token: string;
    remote_id: string;
};

export type ListNameResponse = {
    code: number;
    err_msg: string;
    name_list: string[];
};

//增加密钥
export async function add(addr: string, request: AddRequest): Promise<AddResponse> {
    const cmd = 'plugin:seaotter_auth_secret_api|add';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AddResponse>(cmd, {
        addr,
        request,
    });
}

//删除密钥
export async function remove(addr: string, request: RemoveRequest): Promise<RemoveResponse> {
    const cmd = 'plugin:seaotter_auth_secret_api|remove';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveResponse>(cmd, {
        addr,
        request,
    });
}

//更新密钥
export async function update(addr: string, request: UpdateRequest): Promise<UpdateResponse> {
    const cmd = 'plugin:seaotter_auth_secret_api|update';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateResponse>(cmd, {
        addr,
        request,
    });
}

//获得单个密钥
export async function get(addr: string, request: GetRequest): Promise<GetResponse> {
    const cmd = 'plugin:seaotter_auth_secret_api|get';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetResponse>(cmd, {
        addr,
        request,
    });
}

//列出密钥
export async function list(addr: string, request: ListRequest): Promise<ListResponse> {
    const cmd = 'plugin:seaotter_auth_secret_api|list';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListResponse>(cmd, {
        addr,
        request,
    });
}

//列出密钥名称
export async function list_name(addr: string, request: ListNameRequest): Promise<ListNameResponse> {
    const cmd = 'plugin:seaotter_auth_secret_api|list_name';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListNameResponse>(cmd, {
        addr,
        request,
    });
}