//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export type FeedBackInfo = {
    feed_back_id: string;
    content: string;
    user_id: string;
    user_display_name: string;
    user_logo_uri: string;
    time_stamp: number;
};

export type AddRequest = {
    content: string;    
    session_id: string;
};

export type AddResponse = {
    code: number;
    err_msg: string;
    feed_back_id: string;
};

export type ListRequest = {
    session_id: string;
    offset: number;
    limit: number;
};

export type ListResponse = {
    code: number;
    err_msg: string;
    total_count: number;  
    info_list: FeedBackInfo[];
};

export type RemoveRequest = {
    session_id: string;
    feed_back_id: string;
};

export type RemoveResponse = {
    code: number;
    err_msg: string;
};

//增加反馈
export async function add(request: AddRequest): Promise<AddResponse> {
    const cmd = 'plugin:feedback_api|add';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AddResponse>(cmd, {
        request,
    });
}

//列出反馈
export async function list(request: ListRequest): Promise<ListResponse> {
    const cmd = 'plugin:feedback_api|list';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListResponse>(cmd, {
        request,
    });
}

//删除反馈
export async function remove(request: RemoveRequest): Promise<RemoveResponse> {
    const cmd = 'plugin:feedback_api|remove';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveResponse>(cmd, {
        request,
    });
}