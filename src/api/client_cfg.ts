//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';
import moment from 'moment';

export type ExtraMenuItem = {
  name: string;
  url: string;
  menu_id: string;
  weight: number;
  main_menu: boolean;
  open_in_browser: boolean;
};


export type GetCfgResponse = {
  item_list: ExtraMenuItem[];
  enable_admin: boolean;
  atom_git_client_id: string;
  gitee_client_id: string;
  git_code_client_id: string;
  server_time: number;
  client_time: number; //本地属性，非服务端返回
  enable_user_mail: boolean;
  max_mail_count_per_user: number;
  max_mail_account_per_user: number;
  user_mail_domain: string;
  serv_http_addr: string;
};

export type ReportErrorRequest = {
  err_data: string;
};

export type ReportErrorResponse = {};

export type ServerInfo = {
  name: string,
  system: boolean;
  addr: string;
  default_server: boolean;
};

export type ListServerResult = {
  server_list: ServerInfo[];
};

export type VendorAbility = {
  enable_work_bench: boolean;
  enable_dataview: boolean;
  enable_project: boolean;
  enable_org: boolean;
  enable_pubres: boolean;
  enable_user_mail: boolean;
  enable_grow_center: boolean;
};

export type VendorWorkBench = {
  enable_minapp: boolean;
  enable_user_memo: boolean;
  enable_server_list: boolean;
  enable_user_resume: boolean;
  enable_user_todo: boolean;
}

export type VendorDataView = {
  enable_atomgit: boolean;
  enable_gitcode: boolean;
  enable_gitee: boolean;
  enable_gitlab: boolean;
  enable_tencloud: boolean;
  enable_alicloud: boolean;
  enable_hwcloud: boolean;
};

export type VendorProject = {
  show_requirement_list_entry: boolean;
  show_task_list_entry: boolean;
  show_bug_list_entry: boolean;
  show_testcase_list_entry: boolean;
};

export type VendorOrg = {
  enable_forum: boolean;
};

export type VendorGrowCenter = {
  force_show: boolean; //强制显示成长中心入口
  filter_tag: string; //显示路线图列表时按tag过滤
}

export type VendorAccount = {
  inner_account: boolean;
  external_account: boolean;
  external_atomgit: boolean;
  external_gitcode: boolean;
  external_gitee: boolean;
};

export type VendorMenuItem = {
  id: string;
  name: string;
  url: string;
};

export type VendorLayout = {
  app_name: string;
  hide_sys_bar: boolean;
  show_feedback_modal: boolean;
  feedback_url: string;
  show_server_switch: boolean;
  show_quick_access: boolean;
  show_admin_in_quick_access: boolean;
  show_local_api_in_quick_access: boolean;
  enable_normal_layout: boolean;
  show_layout_switch: boolean;
  enable_server_menu: boolean;
  menu_list: VendorMenuItem[];
  welcome_page_url: string;
  show_manual_and_version: boolean;
  vendor_logo_url: string;
  vendor_logo_height: number;
  vendor_link_url: string;
  vendor_intro: string;
};

export type VendorConfig = {
  vendor_name: string;
  default_server_name: string;
  default_server_addr: string;
  global_server_addr: string;
  ability: VendorAbility;
  work_bench: VendorWorkBench;
  dataview: VendorDataView;
  project: VendorProject;
  org: VendorOrg;
  grow_center: VendorGrowCenter;
  account: VendorAccount;
  layout: VendorLayout;
};

/*
 * 获取客户端的配置，包含如下内容：
 * * 在左侧显示的额外功能板块
 */
export async function get_cfg(): Promise<GetCfgResponse> {
  const res = await invoke<GetCfgResponse>('plugin:client_cfg_api|get_cfg', {
    request: {},
  });
  res.client_time = moment().valueOf();
  return res;
}

// 汇报客户端错误
export async function report_error(request: ReportErrorRequest): Promise<ReportErrorResponse> {
  const res = await invoke<ReportErrorResponse>('plugin:client_cfg_api|report_error', {
    request: request,
  });
  return res;
}

export async function set_default_server(addr: string): Promise<void> {
  return invoke<void>('plugin:client_cfg_api|set_default_server', {
    addr,
  });
}

export async function list_server(skip_system: boolean): Promise<ListServerResult> {
  return invoke<ListServerResult>('plugin:client_cfg_api|list_server', {
    skipSystem: skip_system,
  });
}

export async function save_server_list(serverList: ServerInfo[]): Promise<void> {
  return invoke<void>('plugin:client_cfg_api|save_server_list', {
    serverList: serverList,
  });
}

export async function get_global_server_addr(): Promise<string> {
  return invoke<string>('plugin:client_cfg_api|get_global_server_addr', {});
}

export async function set_global_server_addr(addr: string): Promise<void> {
  return invoke<void>('plugin:client_cfg_api|set_global_server_addr', { addr: addr });
}

export async function get_vendor_config(): Promise<VendorConfig> {
  return invoke<VendorConfig>("plugin:client_cfg_api|get_vendor_config", {});
}