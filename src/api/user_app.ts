//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export type AddAppRequest = {
    session_id: string;
    app_id: string;
};

export type AddAppResponse = {
    code: number;
    err_msg: string;
};

export type ListAppRequest = {
    session_id: string;
};

export type ListAppResponse = {
    code: number;
    err_msg: string;
    app_id_list: string[];
};

export type SetTopRequest = {
    session_id: string;  
    app_id: string;
};

export type SetTopResponse = {
    code: number;
    err_msg: string;
};

export type RemoveAppRequest = {    
    session_id: string;
    app_id: string;
};

export type RemoveAppResponse = {  
    code: number;
    err_msg: string;
}

//增加App
export async function add_app(request: AddAppRequest): Promise<AddAppResponse> {
    const cmd = 'plugin:user_app_api|add_app';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AddAppResponse>(cmd, {
        request,
    });
}

//列出App
export async function list_app(request: ListAppRequest): Promise<ListAppResponse> {
    const cmd = 'plugin:user_app_api|list_app';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListAppResponse>(cmd, {
        request,
    });
}

//置顶App
export async function set_top(request: SetTopRequest): Promise<SetTopResponse> {
    const cmd = 'plugin:user_app_api|set_top';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<SetTopResponse>(cmd, {
        request,
    });
}

//删除App
export async function remove_app(request: RemoveAppRequest): Promise<RemoveAppResponse> {
    const cmd = 'plugin:user_app_api|remove_app';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveAppResponse>(cmd, {
        request,
    });
}