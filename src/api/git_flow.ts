//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { list_config, remove_config, set_config } from "./local_repo";
import type { LocalRepoBranchInfo } from "./local_repo";

export type GitFlowInfo = {
    masterBranch?: string;
    developBranch?: string;
    featurePrefix?: string;
    bugfixPrefix?: string;
    releasePrefix?: string;
    hotfixPrefix?: string;
    supportPrefix?: string;
    versionTagPrefix?: string;
    hooksPath?: string;
};

export async function getGitFlowInfo(path: string): Promise<GitFlowInfo> {
    const retInfo: GitFlowInfo = {};
    const cfgItemList = await list_config(path, true);
    for (const cfgItem of cfgItemList) {
        if (cfgItem.name == "gitflow.branch.master") {
            retInfo.masterBranch = cfgItem.value;
        } else if (cfgItem.name == "gitflow.branch.develop") {
            retInfo.developBranch = cfgItem.value;
        } else if (cfgItem.name == "gitflow.prefix.feature") {
            retInfo.featurePrefix = cfgItem.value;
        } else if (cfgItem.name == "gitflow.prefix.bugfix") {
            retInfo.bugfixPrefix = cfgItem.value;
        } else if (cfgItem.name == "gitflow.prefix.release") {
            retInfo.releasePrefix = cfgItem.value;
        } else if (cfgItem.name == "gitflow.prefix.hotfix") {
            retInfo.hotfixPrefix = cfgItem.value;
        } else if (cfgItem.name == "gitflow.prefix.support") {
            retInfo.supportPrefix = cfgItem.value;
        } else if (cfgItem.name == "gitflow.prefix.versiontag") {
            retInfo.versionTagPrefix = cfgItem.value;
        } else if (cfgItem.name == "gitflow.path.hooks") {
            retInfo.hooksPath = cfgItem.value;
        }
    }
    return retInfo;
}

export async function setGitFlowInfo(path: string, info: GitFlowInfo): Promise<void> {
    if (info.masterBranch !== undefined) {
        await set_config("gitflow.branch.master", info.masterBranch, path, true);
    }
    if (info.developBranch !== undefined) {
        await set_config("gitflow.branch.develop", info.developBranch, path, true);
    }
    if (info.featurePrefix !== undefined) {
        await set_config("gitflow.prefix.feature", info.featurePrefix, path, true);
    }
    if (info.bugfixPrefix !== undefined) {
        await set_config("gitflow.prefix.bugfix", info.bugfixPrefix, path, true);
    }
    if (info.releasePrefix !== undefined) {
        await set_config("gitflow.prefix.release", info.releasePrefix, path, true);
    }
    if (info.hotfixPrefix !== undefined) {
        await set_config("gitflow.prefix.hotfix", info.hotfixPrefix, path, true);
    }
    if (info.supportPrefix !== undefined) {
        await set_config("gitflow.prefix.support", info.supportPrefix, path, true);
    }
    if (info.versionTagPrefix !== undefined) {
        await set_config("gitflow.prefix.versiontag", info.versionTagPrefix, path, true);
    }
    if (info.hooksPath !== undefined) {
        await set_config("gitflow.path.hooks", info.hooksPath, path, true);
    }
}

export async function removeGitFlowInfo(path: string): Promise<void> {
    await remove_config("gitflow.branch.master", path, true);
    await remove_config("gitflow.branch.develop", path, true);
    await remove_config("gitflow.prefix.feature", path, true);
    await remove_config("gitflow.prefix.bugfix", path, true);
    await remove_config("gitflow.prefix.release", path, true);
    await remove_config("gitflow.prefix.hotfix", path, true);
    await remove_config("gitflow.prefix.support", path, true);
    await remove_config("gitflow.prefix.versiontag", path, true);
    await remove_config("gitflow.path.hooks", path, true);
}

export function isGitFlowInit(branchList: LocalRepoBranchInfo[], info: GitFlowInfo): boolean {
    if (info.masterBranch == undefined) {
        return false;
    }
    if (info.developBranch == undefined) {
        return false;
    }
    if (info.featurePrefix == undefined) {
        return false;
    }
    if (info.bugfixPrefix == undefined) {
        return false;
    }
    if (info.releasePrefix == undefined) {
        return false;
    }
    if (info.hotfixPrefix == undefined) {
        return false;
    }
    if (info.supportPrefix == undefined) {
        return false;
    }
    if (info.versionTagPrefix == undefined) {
        return false;
    }
    if (info.hooksPath == undefined) {
        return false;
    }
    const nameList = branchList.map(item => item.name);
    return nameList.includes(info.masterBranch) && nameList.includes(info.developBranch);
}