//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';


export type AccessRecord = {
    roadmap_id: string;
    user_id: string;
    time_stamp: number;
    done_topic_count: number;
    total_topic_count: number;
    done_sub_topic_count: number;
    total_sub_topic_count: number;
    roadmap_title: string;
    roadmap_desc: string;
    can_update_roadmap: boolean;
};

export type LearnState = {
    access_record_count: number;
    done_topic_count: number;
    total_topic_count: number;
    done_sub_topic_count: number;
    total_sub_topic_count: number;
};

export type NoteInfo = {
    roadmap_id: string;
    node_id: string;
    content: string;
    roadmap_title: string;
    node_title: string;
    time_stamp: number;
    roadmap_exist: boolean;
    can_update_roadmap: boolean;
};

export type SimpleRoadmapInfo = {
    roadmap_id: string;
    roadmap_title: string;
};

export type SetAccessRequest = {
    session_id: string;
    roadmap_id: string;
    done_topic_count: number;
    total_topic_count: number;
    done_sub_topic_count: number;
    total_sub_topic_count: number;
};

export type SetAccessResponse = {
    code: number;
    err_msg: string;
};

export type ListAccessRequest = {
    session_id: string;
    offset: number;
    limit: number;
};

export type ListAccessResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    record_list: AccessRecord[];
};

export type RemoveAccessRequest = {
    session_id: string;
    roadmap_id: string;
};

export type RemoveAccessResponse = {
    code: number;
    err_msg: string;
};

export type GetLearnStateRequest = {
    session_id: string;
};

export type GetLearnStateResponse = {
    code: number;
    err_msg: string;
    learn_state: LearnState;
};

export type SetNoteRequest = {
    session_id: string;
    roadmap_id: string;
    node_id: string;
    content: string;
};

export type SetNoteResponse = {
    code: number;
    err_msg: string;
};

export type ListNoteRequest = {
    session_id: string;
    roadmap_id: string;
    filter_by_node_id: boolean;
    node_id: string;
};

export type ListNoteResponse = {
    code: number;
    err_msg: string;
    note_list: NoteInfo[];
};

export type ListMyNoteRequest = {
    session_id: string;
    filter_by_roadmap_id: boolean;
    roadmap_id: string;
    offset: number;
    limit: number;
};

export type ListMyNoteResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    note_list: NoteInfo[];
};

export type RemoveNoteRequest = {
    session_id: string;
    roadmap_id: string;
    node_id: string;
};

export type RemoveNoteResponse = {
    code: number;
    err_msg: string;
};

export type ListNoteRoadmapRequest = {
    session_id: string;
};

export type ListNoteRoadmapResponse = {
    code: number;
    err_msg: string;
    info_list: SimpleRoadmapInfo[];
};

//设置访问记录
export async function set_access(request: SetAccessRequest): Promise<SetAccessResponse> {
    const cmd = 'plugin:roadmap_user_api|set_access';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<SetAccessResponse>(cmd, {
        request,
    });
}

//列出访问记录
export async function list_access(request: ListAccessRequest): Promise<ListAccessResponse> {
    const cmd = 'plugin:roadmap_user_api|list_access';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListAccessResponse>(cmd, {
        request,
    });
}

//删除访问记录
export async function remove_access(request: RemoveAccessRequest): Promise<RemoveAccessResponse> {
    const cmd = 'plugin:roadmap_user_api|remove_access';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveAccessResponse>(cmd, {
        request,
    });
}

//获取学习状态
export async function get_learn_state(request: GetLearnStateRequest): Promise<GetLearnStateResponse> {
    const cmd = 'plugin:roadmap_user_api|get_learn_state';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetLearnStateResponse>(cmd, {
        request,
    });
}

// 设置学习笔记
export async function set_note(request: SetNoteRequest): Promise<SetNoteResponse> {
    const cmd = 'plugin:roadmap_user_api|set_note';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<SetNoteResponse>(cmd, {
        request,
    });
}

// 列出路线图学习笔记
export async function list_note(request: ListNoteRequest): Promise<ListNoteResponse> {
    const cmd = 'plugin:roadmap_user_api|list_note';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListNoteResponse>(cmd, {
        request,
    });
}

// 列出我的学习笔记
export async function list_my_note(request: ListMyNoteRequest): Promise<ListMyNoteResponse> {
    const cmd = 'plugin:roadmap_user_api|list_my_note';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListMyNoteResponse>(cmd, {
        request,
    });
}

// 删除学习笔记
export async function remove_note(request: RemoveNoteRequest): Promise<RemoveNoteResponse> {
    const cmd = 'plugin:roadmap_user_api|remove_note';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveNoteResponse>(cmd, {
        request,
    });
}

//列出学习笔记相关路线图
export async function list_note_roadmap(request: ListNoteRoadmapRequest): Promise<ListNoteRoadmapResponse> {
    const cmd = 'plugin:roadmap_user_api|list_note_roadmap';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListNoteRoadmapResponse>(cmd, {
        request,
    });
}