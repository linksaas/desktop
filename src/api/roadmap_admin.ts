//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';
import type { BasicRoadmapInfo, ROADMAP_OWNER_TYPE, RoadmapInfo } from './roadmap';

export type ListParam = {
    filter_by_owner: boolean;
    owner_type: ROADMAP_OWNER_TYPE;
    filter_by_owner_user_id: boolean;
    owner_user_id: string;
    filter_by_keyword: boolean;
    keyword: string;
    filter_by_tag: boolean;
    tag: string;
};

export type WhiteUserInfo = {
    user_id: string;
    user_name: string;
    user_display_name: string;
    user_logo_uri: string;
    default_tag: string;
    user_desc: string;
    create_time: number;
    update_time: number;
};


export type TagInfo = {
    tag: string;
    name: string;
};

export type AdminCreateRequest = {
    session_id: string;
    basic_info: BasicRoadmapInfo;
};

export type AdminCreateResponse = {
    code: number;
    err_msg: string;
    roadmap_id: string;
};

export type AdminListRequest = {
    session_id: string;
    list_param: ListParam;
    filter_by_pub_state: boolean;
    pub_state: boolean;
    offset: number;
    limit: number;
};

export type AdminListResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    roadmap_list: RoadmapInfo[];
};

export type AdminGetRequest = {
    session_id: string;
    roadmap_id: string;
};

export type AdminGetResponse = {
    code: number;
    err_msg: string;
    roadmap: RoadmapInfo;
};

export type AdminUpdateRequest = {
    session_id: string;
    roadmap_id: string;
    basic_info: BasicRoadmapInfo;
};

export type AdminUpdateResponse = {
    code: number;
    err_msg: string;
};

export type AdminUpdateTagRequest = {
    session_id: string;
    roadmap_id: string;
    tag_list: string[];
};

export type AdminUpdateTagResponse = {
    code: number;
    err_msg: string;
};

export type AdminUpdatePubStateRequest = {
    session_id: string;
    roadmap_id: string;
    pub_state: boolean;
};

export type AdminUpdatePubStateResponse = {
    code: number;
    err_msg: string;
};

export type AdminRemoveRequest = {
    session_id: string;
    roadmap_id: string;
};

export type AdminRemoveResponse = {
    code: number;
    err_msg: string;
};

export type AdminChangeOwnerUserRequest = {
    session_id: string;
    roadmap_id: string;
    target_user_id: string;
};

export type AdminChangeOwnerUserResponse = {
    code: number;
    err_msg: string;
};


export type AdminAddWhiteUserRequest = {
    session_id: string;
    target_user_name: string;
    default_tag: string;
    user_desc: string;
};

export type AdminAddWhiteUserResponse = {
    code: number;
    err_msg: string;
};

export type AdminUpdateWhiteUserRequest = {
    session_id: string;
    target_user_name: string;
    default_tag: string;
    user_desc: string;
};

export type AdminUpdateWhiteUserResponse = {
    code: number;
    err_msg: string;
};

export type AdminRemoveWhiteUserRequest = {
    session_id: string;
    target_user_id: string;
};

export type AdminRemoveWhiteUserResponse = {
    code: number;
    err_msg: string;
};

export type AdminListWhiteUserRequest = {
    session_id: string;
    offset: number;
    limit: number;
};

export type AdminListWhiteUserResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    white_user_list: WhiteUserInfo[];
};


export type AdminAddTagRequest = {
    session_id: string;
    tag: string;
    name: string;
};

export type AdminAddTagResponse = {
    code: number;
    err_msg: string;
};

export type AdminModifyTagRequest = {
    session_id: string;
    tag: string;
    name: string;
};

export type AdminModifyTagResponse = {
    code: number;
    err_msg: string;
};

export type AdminRemoveTagRequest = {
    session_id: string;
    tag: string;
};

export type AdminRemoveTagResponse = {
    code: number;
    err_msg: string;
};

export type AdminListTagRequest = {
    session_id: string;
};

export type AdminListTagResponse = {
    code: number;
    err_msg: string;
    tag_list: TagInfo[];
};

//创建路线图
export async function create(request: AdminCreateRequest): Promise<AdminCreateResponse> {
    const cmd = 'plugin:roadmap_admin_api|create';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AdminCreateResponse>(cmd, {
        request,
    });
}

//列出路线图
export async function list(request: AdminListRequest): Promise<AdminListResponse> {
    const cmd = 'plugin:roadmap_admin_api|list';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AdminListResponse>(cmd, {
        request,
    });
}

//获得单个路线图
export async function get(request: AdminGetRequest): Promise<AdminGetResponse> {
    const cmd = 'plugin:roadmap_admin_api|get';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AdminGetResponse>(cmd, {
        request,
    });
}

//更新路线图
export async function update(request: AdminUpdateRequest): Promise<AdminUpdateResponse> {
    const cmd = 'plugin:roadmap_admin_api|update';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AdminUpdateResponse>(cmd, {
        request,
    });
}

//更新标签
export async function update_tag(request: AdminUpdateTagRequest): Promise<AdminUpdateTagResponse> {
    const cmd = 'plugin:roadmap_admin_api|update_tag';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AdminUpdateTagResponse>(cmd, {
        request,
    });
}

//更新发布状态
export async function update_pub_state(request: AdminUpdatePubStateRequest): Promise<AdminUpdatePubStateResponse> {
    const cmd = 'plugin:roadmap_admin_api|update_pub_state';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AdminUpdatePubStateResponse>(cmd, {
        request,
    });
}

//删除路线图
export async function remove(request: AdminRemoveRequest): Promise<AdminRemoveResponse> {
    const cmd = 'plugin:roadmap_admin_api|remove';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AdminRemoveResponse>(cmd, {
        request,
    });
}

//更换owner user
export async function change_owner_user(request: AdminChangeOwnerUserRequest): Promise<AdminChangeOwnerUserResponse> {
    const cmd = 'plugin:roadmap_admin_api|change_owner_user';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AdminChangeOwnerUserResponse>(cmd, {
        request,
    });
}

//增加白名单
export async function add_white_user(request: AdminAddWhiteUserRequest): Promise<AdminAddWhiteUserResponse> {
    const cmd = 'plugin:roadmap_admin_api|add_white_user';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AdminAddWhiteUserResponse>(cmd, {
        request,
    });
}

//更新白名单
export async function update_white_user(request: AdminUpdateWhiteUserRequest): Promise<AdminUpdateWhiteUserResponse> {
    const cmd = 'plugin:roadmap_admin_api|update_white_user';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AdminUpdateWhiteUserResponse>(cmd, {
        request,
    });
}

//删除白名单
export async function remove_white_user(request: AdminRemoveWhiteUserRequest): Promise<AdminRemoveWhiteUserResponse> {
    const cmd = 'plugin:roadmap_admin_api|remove_white_user';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AdminRemoveWhiteUserResponse>(cmd, {
        request,
    });
}

//列出白名单
export async function list_white_user(request: AdminListWhiteUserRequest): Promise<AdminListWhiteUserResponse> {
    const cmd = 'plugin:roadmap_admin_api|list_white_user';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AdminListWhiteUserResponse>(cmd, {
        request,
    });
}


//增加标签
export async function add_tag(request: AdminAddTagRequest): Promise<AdminAddTagResponse> {
    const cmd = 'plugin:roadmap_admin_api|add_tag';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AdminAddTagResponse>(cmd, {
        request,
    });
}

//修改标签
export async function modify_tag(request: AdminModifyTagRequest): Promise<AdminModifyTagResponse> {
    const cmd = 'plugin:roadmap_admin_api|modify_tag';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AdminModifyTagResponse>(cmd, {
        request,
    });
}


//删除标签
export async function remove_tag(request: AdminRemoveTagRequest): Promise<AdminRemoveTagResponse> {
    const cmd = 'plugin:roadmap_admin_api|remove_tag';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AdminRemoveTagResponse>(cmd, {
        request,
    });
}

//列出标签
export async function list_tag(request: AdminListTagRequest): Promise<AdminListTagResponse> {
    const cmd = 'plugin:roadmap_admin_api|list_tag';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AdminListTagResponse>(cmd, {
        request,
    });
}