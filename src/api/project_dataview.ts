//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export const DND_ITEM_TYPE = "node";

export type NODE_TYPE = number;

export const NODE_TYPE_REF_IDEA: NODE_TYPE = 0;
export const NODE_TYPE_REF_REQUIREMENT: NODE_TYPE = 1;
export const NODE_TYPE_REF_TASK: NODE_TYPE = 2;
export const NODE_TYPE_REF_BUG: NODE_TYPE = 3;
export const NODE_TYPE_REF_TESTCASE: NODE_TYPE = 4;
export const NODE_TYPE_REF_SERVER: NODE_TYPE = 5;
export const NODE_TYPE_REF_ENTRY: NODE_TYPE = 6;

export const NODE_TYPE_BASE_EMPTY_REF: NODE_TYPE = 199;
export const NODE_TYPE_BASE_GROUP: NODE_TYPE = 200;
export const NODE_TYPE_BASE_TEXT: NODE_TYPE = 201;
export const NODE_TYPE_BASE_IFRAME: NODE_TYPE = 202

export type UserPerm = {
    can_update: boolean;
    can_remove: boolean;
    can_update_perm: boolean;
};

export type ViewInfo = {
    view_id: string;
    title: string;
    bg_color: string;
    desc: string;
    owner_user_id: string;
    owner_display_name: string;
    owner_logo_uri: string;
    create_time: number;
    project_id: string;
    project_name: string;
    user_perm: UserPerm;
    extra_update_user_id_list: string[];
};

export type NodeInfo = {
    node_id: string;
    node_type: NODE_TYPE;
    x: number;
    y: number;
    w: number;
    h: number;
    bg_color: string;
    node_data: NodeData;
};

export type EdgeInfoKey = {
    from_node_id: string;
    from_handle_id: string;
    to_node_id: string;
    to_handle_id: string;
};

export type EdgeInfo = {
    edge_key: EdgeInfoKey;
    label: string;
};

export type CreateViewRequest = {
    session_id: string;
    project_id: string;
    title: string;
    bg_color: string;
    desc: string;
};

export type CreateViewResponse = {
    code: number;
    err_msg: string;
    view_id: string;
};

export type ListViewRequest = {
    session_id: string;
    filter_by_project_id: boolean;
    project_id: string;
    offset: number;
    limit: number;
};

export type ListViewResponse = {
    code: number;
    err_msg: string;
    total_count: number;
    view_list: ViewInfo[];
};

export type GetViewRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
};

export type GetViewResponse = {
    code: number;
    err_msg: string;
    view: ViewInfo;
};

export type UpdateViewRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
    title: string;
    bg_color: string;
    desc: string;
};

export type UpdateViewResponse = {
    code: number;
    err_msg: string;
};

export type RemoveViewRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
};

export type RemoveViewResponse = {
    code: number;
    err_msg: string;
};

export type UpdateViewPermRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
    extra_update_user_id_list: string[];
};

export type UpdateViewPermResponse = {
    code: number;
    err_msg: string;
};

export type CreateNodeRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
    node_type: NODE_TYPE;
    x: number;
    y: number;
    w: number;
    h: number;
    bg_color: string;
    node_data: NodeData;
};

export type NodeData = {
    RefTargetId?: string;
    BaseGroupName?: string;
    BaseText?: string;
    BaseIframeUrl?: string;
}

export type CreateNodeResponse = {
    code: number;
    err_msg: string;
    node_id: string;
};

export type UpdateNodePositionRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
    node_id: string;
    x: number;
    y: number;
};

export type UpdateNodePositionResponse = {
    code: number;
    err_msg: string;
};

export type UpdateNodeSizeRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
    node_id: string;
    w: number;
    h: number;
};

export type UpdateNodeSizeResponse = {
    code: number;
    err_msg: string;
};

export type UpdateNodeBgColorRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
    node_id: string;
    bg_color: string;
};

export type UpdateNodeBgColorResponse = {
    code: number;
    err_msg: string;
};

export type RemoveNodeRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
    node_id: string;
};

export type RemoveNodeResponse = {
    code: number;
    err_msg: string;
};

export type ListNodeRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
};

export type ListNodeResponse = {
    code: number;
    err_msg: string;
    node_list: NodeInfo[];
};

export type GetNodeRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
    node_id: string;
};

export type GetNodeResponse = {
    code: number;
    err_msg: string;
    node: NodeInfo;
};

export type UpdateNodeDataRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
    node_id: string;
    node_data: NodeData;
};

export type UpdateNodeDataResponse = {
    code: number;
    err_msg: string;
};

export type ConvertToRefNodeRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
    node_id: string;
    target_node_type: NODE_TYPE;
    ref_target_id: string;
};

export type ConvertToRefNodeResponse = {
    code: number;
    err_msg: string;
};

export type CreateEdgeRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
    edge: EdgeInfo;
};

export type CreateEdgeResponse = {
    code: number;
    err_msg: string;
};

export type UpdateEdgeRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
    edge: EdgeInfo;
};

export type UpdateEdgeResponse = {
    code: number;
    err_msg: string;
};

export type RemoveEdgeRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
    edge_key: EdgeInfoKey;
};

export type RemoveEdgeResponse = {
    code: number;
    err_msg: string;
};

export type ListEdgeRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
};

export type ListEdgeResponse = {
    code: number;
    err_msg: string;
    edge_list: EdgeInfo[];
};

export type GetEdgeRequest = {
    session_id: string;
    project_id: string;
    view_id: string;
    edge_key: EdgeInfoKey;
};

export type GetEdgeResponse = {
    code: number;
    err_msg: string;
    edge: EdgeInfo;
};


//创建视图
export async function create_view(request: CreateViewRequest): Promise<CreateViewResponse> {
    const cmd = 'plugin:project_dataview_api|create_view';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<CreateViewResponse>(cmd, {
        request,
    });
}

//列出视图
export async function list_view(request: ListViewRequest): Promise<ListViewResponse> {
    const cmd = 'plugin:project_dataview_api|list_view';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListViewResponse>(cmd, {
        request,
    });
}

//获取单个视图
export async function get_view(request: GetViewRequest): Promise<GetViewResponse> {
    const cmd = 'plugin:project_dataview_api|get_view';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetViewResponse>(cmd, {
        request,
    });
}

//修改视图
export async function update_view(request: UpdateViewRequest): Promise<UpdateViewResponse> {
    const cmd = 'plugin:project_dataview_api|update_view';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateViewResponse>(cmd, {
        request,
    });
}

//删除视图
export async function remove_view(request: RemoveViewRequest): Promise<RemoveViewResponse> {
    const cmd = 'plugin:project_dataview_api|remove_view';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveViewResponse>(cmd, {
        request,
    });
}

//更新视图权限
export async function update_view_perm(request: UpdateViewPermRequest): Promise<UpdateViewPermResponse> {
    const cmd = 'plugin:project_dataview_api|update_view_perm';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateViewPermResponse>(cmd, {
        request,
    });
}

//创建节点
export async function create_node(request: CreateNodeRequest): Promise<CreateNodeResponse> {
    const cmd = 'plugin:project_dataview_api|create_node';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<CreateNodeResponse>(cmd, {
        request,
    });
}

//更新节点位置
export async function update_node_position(request: UpdateNodePositionRequest): Promise<UpdateNodePositionResponse> {
    const cmd = 'plugin:project_dataview_api|update_node_position';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateNodePositionResponse>(cmd, {
        request,
    });
}

//更新节点大小
export async function update_node_size(request: UpdateNodeSizeRequest): Promise<UpdateNodeSizeResponse> {
    const cmd = 'plugin:project_dataview_api|update_node_size';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateNodeSizeResponse>(cmd, {
        request,
    });
}

//更新节点背景颜色
export async function update_node_bg_color(request: UpdateNodeBgColorRequest): Promise<UpdateNodeBgColorResponse> {
    const cmd = 'plugin:project_dataview_api|update_node_bg_color';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateNodeBgColorResponse>(cmd, {
        request,
    });
}

//删除节点
export async function remove_node(request: RemoveNodeRequest): Promise<RemoveNodeResponse> {
    const cmd = 'plugin:project_dataview_api|remove_node';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveNodeResponse>(cmd, {
        request,
    });
}

//列出节点
export async function list_node(request: ListNodeRequest): Promise<ListNodeResponse> {
    const cmd = 'plugin:project_dataview_api|list_node';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListNodeResponse>(cmd, {
        request,
    });
}

//获取单个节点
export async function get_node(request: GetNodeRequest): Promise<GetNodeResponse> {
    const cmd = 'plugin:project_dataview_api|get_node';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetNodeResponse>(cmd, {
        request,
    });
}

//更新节点数据
export async function update_node_data(request: UpdateNodeDataRequest): Promise<UpdateNodeDataResponse> {
    const cmd = 'plugin:project_dataview_api|update_node_data';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateNodeDataResponse>(cmd, {
        request,
    });
}

//转换成引用节点
export async function convert_to_ref_node(request: ConvertToRefNodeRequest): Promise<ConvertToRefNodeResponse> {
    const cmd = 'plugin:project_dataview_api|convert_to_ref_node';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ConvertToRefNodeResponse>(cmd, {
        request,
    });
}

//创建连接
export async function create_edge(request: CreateEdgeRequest): Promise<CreateEdgeResponse> {
    const cmd = 'plugin:project_dataview_api|create_edge';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<CreateEdgeResponse>(cmd, {
        request,
    });
}

//更新连接
export async function update_edge(request: UpdateEdgeRequest): Promise<UpdateEdgeResponse> {
    const cmd = 'plugin:project_dataview_api|update_edge';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateEdgeResponse>(cmd, {
        request,
    });
}

//删除连接
export async function remove_edge(request: RemoveEdgeRequest): Promise<RemoveEdgeResponse> {
    const cmd = 'plugin:project_dataview_api|remove_edge';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveEdgeResponse>(cmd, {
        request,
    });
}

//列出连接
export async function list_edge(request: ListEdgeRequest): Promise<ListEdgeResponse> {
    const cmd = 'plugin:project_dataview_api|list_edge';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListEdgeResponse>(cmd, {
        request,
    });
}

//获取单个连接
export async function get_edge(request: GetEdgeRequest): Promise<GetEdgeResponse> {
    const cmd = 'plugin:project_dataview_api|get_edge';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetEdgeResponse>(cmd, {
        request,
    });
}