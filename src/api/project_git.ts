//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { invoke } from '@tauri-apps/api/tauri';

export type BasicGitRepoInfo ={
    name: string;  
    git_url: string;
};

export type GitRepoInfo = {
    git_repo_id: string;
    basic_info: BasicGitRepoInfo;
    create_time: number;
    update_time: number;
};

export type AddGitRepoRequest = {
    session_id: string;
    project_id: string;
    basic_info: BasicGitRepoInfo;
};

export type AddGitRepoResponse = {
    code: number;
    err_msg: string;  
    git_repo_id: string;
};

export type UpdateGitRepoRequest = {
    session_id: string;
    project_id: string;
    git_repo_id: string;  
    basic_info: BasicGitRepoInfo;
};

export type UpdateGitRepoResponse = {
    code: number;   
    err_msg: string;
};

export type RemoveGitRepoRequest = {
    session_id: string;
    project_id: string;  
    git_repo_id: string;
};

export type RemoveGitRepoResponse = {
    code: number;  
    err_msg: string;
};

export type ListGitRepoRequest = {
    session_id: string;
    project_id: string;
};

export type ListGitRepoResponse = {
    code: number;
    err_msg: string;
    repo_info_list: GitRepoInfo[];
};

export type GetGitRepoRequest = {
    session_id: string;
    project_id: string;  
    git_repo_id: string;
};

export type GetGitRepoResponse = {
    code: number;
    err_msg: string;  
    repo_info: GitRepoInfo;
};

//增加仓库
export async function add_git_repo(request: AddGitRepoRequest): Promise<AddGitRepoResponse> {
    const cmd = 'plugin:project_git_api|add_git_repo';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<AddGitRepoResponse>(cmd, {
        request,
    });
}

//更新仓库
export async function update_git_repo(request: UpdateGitRepoRequest): Promise<UpdateGitRepoResponse> {
    const cmd = 'plugin:project_git_api|update_git_repo';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<UpdateGitRepoResponse>(cmd, {
        request,
    });
}

//删除仓库
export async function remove_git_repo(request: RemoveGitRepoRequest): Promise<RemoveGitRepoResponse> {
    const cmd = 'plugin:project_git_api|remove_git_repo';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<RemoveGitRepoResponse>(cmd, {
        request,
    });
}

//列出仓库
export async function list_git_repo(request: ListGitRepoRequest): Promise<ListGitRepoResponse> {
    const cmd = 'plugin:project_git_api|list_git_repo';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<ListGitRepoResponse>(cmd, {
        request,
    });
}

//获取仓库
export async function get_git_repo(request: GetGitRepoRequest): Promise<GetGitRepoResponse> {
    const cmd = 'plugin:project_git_api|get_git_repo';
    console.log(`%c${cmd}`, 'color:#0f0;', request);
    return invoke<GetGitRepoResponse>(cmd, {
        request,
    });
}