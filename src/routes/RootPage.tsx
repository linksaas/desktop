//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import { useStores } from '@/hooks';
import { WORKBENCH_PATH, GROW_CENTER_PATH } from "@/utils/constant";
import { Redirect } from 'react-router-dom';

const RootPage = () => {
    const appStore = useStores("appStore");
    const [path, setPath] = useState<string | null>(null);

    useEffect(()=>{
        if(appStore.vendorCfg !== undefined){
            if(appStore.vendorCfg.ability.enable_work_bench){
                setPath(WORKBENCH_PATH);
            }else if(appStore.vendorCfg.ability.enable_grow_center && appStore.vendorCfg.grow_center.force_show){
                setPath(GROW_CENTER_PATH);
            }
        }
    },[appStore.vendorCfg]);

    return (
        <>
        {path != null && (
            <Redirect to={path}/>
        )}
        </>
    );
};

export default observer(RootPage);