//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from "react";
import { observer } from 'mobx-react';
import type { MemberInfo } from "@/api/dragonfly/member";
import { update_perm } from "@/api/dragonfly/member";
import { Checkbox, message, Modal, Space } from "antd";
import { useDragonFlyStores } from "../store";
import { get_session, get_user_id } from "@/api/user";
import { request } from "@/utils/request";
import { gen_access_token } from "@/api/project_server";

export interface UpdatePermModalProps {
    member: MemberInfo;
    onCancel: () => void;
    onOk: () => void;
}

const UpdatePermModal = (props: UpdatePermModalProps) => {
    const appStore = useDragonFlyStores();

    const [permInfo, setPermInfo] = useState(props.member.perm);
    const [hasChange, setHasChange] = useState(false);

    const updatePerm = async () => {
        if (appStore.remoteServer == null || appStore.remoteServer.basic_info.addr_list.length == 0) {
            return;
        }
        const sessionId = await get_session();
        const accessRes = await request(gen_access_token({
            session_id: sessionId,
            project_id: appStore.projectId,
            server_id: appStore.remoteServer.server_id,
        }));
        await request(update_perm(appStore.remoteServer.basic_info.addr_list[0],{
                access_token: accessRes.token,
                remote_id: appStore.remoteServer.server_id,
                member_user_id: props.member.member_user_id,
                perm: permInfo,
        }));
        const myUserId = await get_user_id();
        if(myUserId == props.member.member_user_id){
            await appStore.loadMyPerm();
        }
        message.info("设置成功");
        props.onOk();
    };

    return (
        <Modal open title="设置权限" mask={false}
            okText="设置" okButtonProps={{ disabled: !hasChange }}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onCancel();
            }}
            onOk={e => {
                e.stopPropagation();
                e.preventDefault();
                updatePerm();
            }}>
            <Space direction="vertical">
                <Checkbox checked={permInfo.change_auth_check} onChange={e => {
                    e.stopPropagation();
                    setPermInfo({
                        ...permInfo,
                        change_auth_check: e.target.checked,
                    });
                    setHasChange(true);
                }}>密钥验证开关</Checkbox>
                <Checkbox checked={permInfo.read_auth_secret} onChange={e => {
                    e.stopPropagation();
                    setPermInfo({
                        ...permInfo,
                        read_auth_secret: e.target.checked,
                    });
                    setHasChange(true);
                }}>读取验证密钥</Checkbox>
                <Checkbox checked={permInfo.manage_auth_secret} onChange={e => {
                    e.stopPropagation();
                    setPermInfo({
                        ...permInfo,
                        manage_auth_secret: e.target.checked,
                    });
                    setHasChange(true);
                }}>管理验证密钥</Checkbox>
            </Space>
        </Modal>
    );
};

export default observer(UpdatePermModal);