//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { observer } from 'mobx-react';
import { Descriptions, message, Space } from "antd";
import { useDragonFlyStores } from "./store";
import type { PortInfo } from "@/api/dragonfly/config";
import { get_port_info } from "@/api/dragonfly/config";
import { get_session } from "@/api/user";
import { request } from "@/utils/request";
import { gen_access_token } from "@/api/project_server";
import { writeText } from "@tauri-apps/api/clipboard";


const PortInfoPanel = () => {
    const appStore = useDragonFlyStores();

    const [portInfo, setPortInfo] = useState<PortInfo | null>(null);

    const loadPortInfo = async () => {
        if (appStore.remoteServer == null || appStore.remoteServer.basic_info.addr_list.length == 0) {
            return;
        }
        const sessionId = await get_session();
        const accessRes = await request(gen_access_token({
            session_id: sessionId,
            project_id: appStore.projectId,
            server_id: appStore.remoteServer.server_id,
        }));
        const res = await request(get_port_info(appStore.remoteServer.basic_info.addr_list[0], {
            access_token: accessRes.token,
            remote_id: appStore.remoteServer.server_id,
        }));
        setPortInfo(res.port_info);
    };

    const genAddrByPort = (port: number) => {
        if (appStore.remoteServer == null || appStore.remoteServer.basic_info.addr_list.length == 0) {
            return "";
        }
        const addr = appStore.remoteServer.basic_info.addr_list[0].split(":")[0];
        return `${addr}:${port}`;
    }

    useEffect(() => {
        loadPortInfo();
    }, [appStore.remoteServer]);

    return (
        <>
            {portInfo != null && appStore.remoteServer != null && appStore.remoteServer.basic_info.addr_list.length > 0 && (
                <Descriptions column={1} bordered labelStyle={{ width: "100px" }} style={{ padding: "0px 10px" }}>
                    {portInfo.has_zipkin_port && (
                        <Descriptions.Item label="zipkin">
                            {genAddrByPort(portInfo.zipkin_port)}
                            &nbsp;<a onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                writeText(genAddrByPort(portInfo.zipkin_port));
                                message.info("复制成功")
                            }}>复制</a></Descriptions.Item>
                    )}
                    {(portInfo.has_jaeger_grpc_port || portInfo.has_jaeger_http_port) && (
                        <Descriptions.Item label="jaeger">
                            <Space>
                                <span>
                                    {portInfo.has_jaeger_grpc_port && `${genAddrByPort(portInfo.jaeger_grpc_port)}(grpc)`}
                                    &nbsp;<a onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        writeText(genAddrByPort(portInfo.jaeger_grpc_port));
                                        message.info("复制成功")
                                    }}>复制</a>
                                </span>
                                <span>
                                    {portInfo.has_jaeger_http_port && `${genAddrByPort(portInfo.jaeger_http_port)}(http)`}
                                    &nbsp;<a onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        writeText(genAddrByPort(portInfo.jaeger_http_port));
                                        message.info("复制成功")
                                    }}>复制</a>
                                </span>
                            </Space>
                        </Descriptions.Item>
                    )}
                    {(portInfo.has_skywalking_grpc_port || portInfo.has_skywalking_http_port) && (
                        <Descriptions.Item label="skywalking">
                            <Space>
                                <span>
                                    {portInfo.has_skywalking_grpc_port && `${genAddrByPort(portInfo.skywalking_grpc_port)}(grpc)`}
                                    &nbsp;<a onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        writeText(genAddrByPort(portInfo.skywalking_grpc_port));
                                        message.info("复制成功")
                                    }}>复制</a>
                                </span>
                                <span>
                                    {portInfo.has_skywalking_http_port && `${genAddrByPort(portInfo.skywalking_http_port)}(http)`}
                                    &nbsp;<a onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        writeText(genAddrByPort(portInfo.skywalking_http_port));
                                        message.info("复制成功")
                                    }}>复制</a>
                                </span>
                            </Space>
                        </Descriptions.Item>
                    )}
                    {(portInfo.has_otlp_grpc_port || portInfo.has_otlp_http_port) && (
                        <Descriptions.Item label="otlp">
                            <Space>
                                <span>
                                    {portInfo.has_otlp_grpc_port && `${genAddrByPort(portInfo.otlp_grpc_port)}(grpc)`}
                                    &nbsp;<a onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        writeText(genAddrByPort(portInfo.otlp_grpc_port));
                                        message.info("复制成功")
                                    }}>复制</a>
                                </span>
                                <span>
                                    {portInfo.has_otlp_http_port && `${genAddrByPort(portInfo.otlp_http_port)}(http)`}
                                    &nbsp;<a onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        writeText(genAddrByPort(portInfo.otlp_http_port));
                                        message.info("复制成功")
                                    }}>复制</a>
                                </span>
                            </Space>
                        </Descriptions.Item>
                    )}
                </Descriptions>
            )}
        </>
    );
};

export default observer(PortInfoPanel);