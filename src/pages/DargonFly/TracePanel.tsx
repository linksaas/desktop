//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { observer } from 'mobx-react';
import { Empty, Layout } from "antd";
import { useDragonFlyStores } from "./store";
import SimpleTraceList from "./components/SimpleTraceList";
import TraceDetail from "./components/TraceDetail";




const TracePanel = () => {
    const appStore = useDragonFlyStores();

    const [curTraceId, setCurTraceId] = useState("");


    useEffect(() => {
        setCurTraceId("");
        if (appStore.traceFilter != null && appStore.traceFilter.filterByTraceId && appStore.traceFilter.traceId != undefined) {
            setCurTraceId(appStore.traceFilter.traceId);
        }
    }, [appStore.traceFilter]);

    return (
        <Layout style={{ backgroundColor: "white" }}>
            {appStore.traceFilter != null && appStore.traceFilter.filterByTraceId == false && (
                <Layout.Sider style={{ height: "calc(100vh - 50px)", overflowY: "scroll", borderRight: "1px solid #e4e4e8" }} width={300} theme="light">
                    <SimpleTraceList curTraceId={curTraceId} onChange={newTraceId => setCurTraceId(newTraceId)} />
                </Layout.Sider>
            )}
            <Layout.Content style={{height: "calc(100vh - 50px)", overflowY: "scroll"}}>
                {curTraceId == "" && (
                    <Empty image={Empty.PRESENTED_IMAGE_SIMPLE}/>
                )}
                {curTraceId != "" &&appStore.traceFilter != null && (
                    <TraceDetail serviceInfo={appStore.traceFilter.serviceInfo} traceId={curTraceId} />
                )}
            </Layout.Content>
        </Layout>
    );
};

export default observer(TracePanel);