//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from 'react';
import { useDrag } from 'react-dnd';

import { DND_ITEM_TYPE, NodeData } from './types';
import { BlockOutlined } from '@ant-design/icons';

interface LocalDragTreeNodeProps {
    title: string;
    nodeData: NodeData;
}

const LocalDragTreeNode = (props: LocalDragTreeNodeProps) => {
    const [hover, setHover] = useState(false);

    const [_, drag] = useDrag(() => ({
        type: DND_ITEM_TYPE,
        item: props.nodeData,
        canDrag: true,
    }));

    return (
        <div ref={drag} style={{ cursor: "move" }}
            onMouseEnter={e => {
                e.stopPropagation();
                e.preventDefault();
                setHover(true);
            }}
            onMouseLeave={e => {
                e.stopPropagation();
                e.preventDefault();
                setHover(false);
            }}>
            <div style={{ backgroundColor: hover ? "#aaa" : "#eee", paddingRight: "10px", paddingLeft: "10px" }}>
                <BlockOutlined />
                &nbsp;&nbsp;{props.title}
                {hover && <br />}
                {hover && <span style={{ color: "green" }}>可拖动到主面板</span>}
            </div>
        </div>
    );
};

export default LocalDragTreeNode;