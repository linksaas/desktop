//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import type {  NodeProps } from "reactflow";
import { NodeData } from '../types';
import { observer } from 'mobx-react';
import LocalNodeWrap from '../LocalNodeWrap';
import { Button, Input, Space } from 'antd';
import { CheckOutlined, CloseOutlined, EditOutlined } from '@ant-design/icons';
import { useDataViewStores } from '@/pages/DataView/store';

const GroupNode = (props: NodeProps<NodeData>) => {
    const dataViewStore = useDataViewStores();
    
    const [inEdit, setInEdit] = useState(false);

    const [text, setText] = useState("");

    useEffect(() => {
        setText(props.data.extraInfo as string);
    }, [props.data.extraInfo]);
    
    return (
        <LocalNodeWrap title={
            <div>
                {inEdit == false && (
                    <Space>
                        <span style={{ fontSize: "16px" }}>{props.data.extraInfo as string}</span>
                        <Button type="link" icon={<EditOutlined />} onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            setInEdit(true);
                        }} />
                    </Space>
                )}
                {inEdit == true && (
                    <Input value={text} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setText(e.target.value);
                    }} addonAfter={
                        <Space>
                            <Button type="text" style={{ padding: "0px 0px", height: "20px", minWidth: "0px" }} icon={<CloseOutlined style={{ color: "red" }} />}
                                onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    setInEdit(false);
                                    setText(props.data.extraInfo as string);
                                }} />
                            <Button type="text" style={{ padding: "0px 0px", height: "20px", minWidth: "0px" }} icon={<CheckOutlined style={{ color: "green" }} />}
                                onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    setInEdit(false);
                                    const tmpList = dataViewStore.localStore.nodeList.slice();
                                    const index = tmpList.findIndex(item => item.id == props.id);
                                    if (index != -1) {
                                        tmpList[index].data.extraInfo = text;
                                        dataViewStore.localStore.nodeList = tmpList;
                                        dataViewStore.localStore.saveNodeList();
                                    }
                                }} />
                        </Space>
                    } />
                )}
            </div>
        } nodeData={props} canChangeBgColor/>
    );
};

export default observer(GroupNode);