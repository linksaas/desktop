//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import type { NodeProps } from "reactflow";
import { NodeData } from '../types';
import { observer } from 'mobx-react';
import LocalNodeWrap from '../LocalNodeWrap';
import { Button, Input } from 'antd';
import { CheckOutlined, CloseOutlined, EditOutlined } from '@ant-design/icons';
import { useDataViewStores } from '@/pages/DataView/store';

const TextNode = (props: NodeProps<NodeData>) => {
    const dataViewStore = useDataViewStores();

    const [inEdit, setInEdit] = useState(false);

    const [text, setText] = useState("");

    useEffect(() => {
        setText(props.data.extraInfo as string);
    }, [props.data.extraInfo]);

    return (
        <LocalNodeWrap title="文本" nodeData={props} canChangeBgColor extraButtonList={inEdit ? [
            <Button type="text" style={{ padding: "0px 0px", minWidth: "0px" }} icon={<CloseOutlined style={{ color: "red" }} />}
                onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    setInEdit(false);
                    setText(props.data.extraInfo as string);
                }} />,
            <Button type="text" style={{ padding: "0px 0px", minWidth: "0px" }} icon={<CheckOutlined style={{ color: "green" }} />}
                onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    setInEdit(false);
                    const tmpList = dataViewStore.localStore.nodeList.slice();
                    const index = tmpList.findIndex(item => item.id == props.id);
                    if (index != -1) {
                        tmpList[index].data.extraInfo = text;
                        dataViewStore.localStore.nodeList = tmpList;
                        dataViewStore.localStore.saveNodeList();
                    }
                }} disabled={text == ""} />
        ] : [
            <Button type="link" icon={<EditOutlined />} onClick={e => {
                e.stopPropagation();
                e.preventDefault();
                setInEdit(true);
            }} />
        ]}>
            <div>
                {inEdit == false && (
                    <pre style={{ fontSize: "16px", whiteSpace: "pre-wrap", wordWrap: "break-word", height: props.data.height - 60, overflowY: "scroll" }} className='nodrag nopan nowheel'>
                        {props.data.extraInfo as string}
                    </pre>
                )}
                {inEdit == true && (
                    <Input.TextArea value={text} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setText(e.target.value);
                    }} style={{ marginTop: "10px" }} autoSize={{ maxRows: 5 }} />
                )}
            </div>
        </LocalNodeWrap>);
};

export default observer(TextNode);