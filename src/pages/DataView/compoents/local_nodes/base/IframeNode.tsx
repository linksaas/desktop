//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import { NodeData } from '../types';
import LocalNodeWrap from '../LocalNodeWrap';
import type { NodeProps } from "reactflow";
import { Button, Form, Input, Modal } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import { useDataViewStores } from '@/pages/DataView/store';


const IframeNode = (props: NodeProps<NodeData>) => {
    const dataViewStore = useDataViewStores();

    const [url, setUrl] = useState(props.data.extraInfo as string);
    const [showEditModal, setShowEditModal] = useState(false);

    useEffect(()=>{
        setUrl(props.data.extraInfo as string);
    },[props.data.extraInfo]);

    return (
        <LocalNodeWrap title={`网址:${props.data.extraInfo as string}`} canChangeBgColor={false} nodeData={props}
            extraButtonList={[
                <Button type="text" icon={<EditOutlined />} onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    setShowEditModal(true);
                }} />
            ]}>
            {props.data.extraInfo != "" && (
                <iframe src={props.data.extraInfo as string} style={{ width: '100%', height: props.data.height - 40 }} />
            )}
            {showEditModal && (
                <Modal open title="设置网址" mask={false}
                    okText="设置" okButtonProps={{ disabled: !url.startsWith("https://") }}
                    onCancel={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setShowEditModal(false);
                        setUrl(props.data.extraInfo as string);
                    }}
                    onOk={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        const tmpList = dataViewStore.localStore.nodeList.slice();
                        const index = tmpList.findIndex(item => item.id == props.id);
                        if (index != -1) {
                            tmpList[index].data.extraInfo = url;
                            dataViewStore.localStore.nodeList = tmpList;
                            dataViewStore.localStore.saveNodeList();
                        }
                        setShowEditModal(false);
                    }}>
                    <Form>
                        <Form.Item label="网址" help="必须以https://开始">
                            <Input value={url} onChange={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setUrl(e.target.value.trim());
                            }} status={url.startsWith("https://") ? undefined : "error"} />
                        </Form.Item>
                    </Form>
                </Modal>
            )}
        </LocalNodeWrap>
    );
};

export default observer(IframeNode);