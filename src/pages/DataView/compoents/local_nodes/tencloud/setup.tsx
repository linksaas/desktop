//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from 'react';
import { type TenCloudSourceInfo, type UserDataSourceInfo } from "@/api/user_dataview";
import type { TreeDataNode } from 'antd';
import { listInstance } from "@/api/tencloud/lighthouse";
import { listDomain as listDnsDomain } from "@/api/tencloud/dnspod";
import { listDomain as listCdnDomain } from "@/api/tencloud/cdn";
import LocalDragTreeNode from '../LocalDragTreeNode';
import { NODE_TYPE_TENCLOUD_CDN_STATUS, NODE_TYPE_TENCLOUD_DNS_RECORD_LIST, NODE_TYPE_TENCLOUD_LIGHTHOUSE_INSTANCE } from '../types';
import { getRegionName } from './utils';


export const setupTenCloudTreeData = async (dataSource: UserDataSourceInfo): Promise<TreeDataNode[]> => {
    const tmpTreeData: TreeDataNode[] = [];
    const sourceInfo = JSON.parse(dataSource.source_info) as TenCloudSourceInfo;
    if (sourceInfo.enableLightHouse) {
        const tmpInstanceList: TreeDataNode[] = [];
        for (const region of sourceInfo.regionList) {
            const res = await listInstance(sourceInfo.secretId, sourceInfo.secretKey, region);
            for (const instance of res.Response.InstanceSet) {
                tmpInstanceList.push({
                    title: <LocalDragTreeNode title={`${instance.InstanceName}(${getRegionName(region)})`} nodeData={{
                        nodeType: NODE_TYPE_TENCLOUD_LIGHTHOUSE_INSTANCE,
                        dataSourceId: dataSource.id,
                        extraInfo: {
                            region: region,
                            instanceId: instance.InstanceId,
                        },
                        height: 680,
                        width: 250,
                    }} />,
                    key: `${dataSource.id}/lighthouse/${instance.InstanceId}`,
                });
            }
        }
        tmpTreeData.push({
            title: "轻量应用服务器",
            key: `${dataSource.id}/lighthouse`,
            children: tmpInstanceList,
        });
    }
    if (sourceInfo.enableDns) {
        const tmpDomainList: TreeDataNode[] = [];
        const res = await listDnsDomain(sourceInfo.secretId, sourceInfo.secretKey);
        for (const domain of res.Response.DomainList) {
            tmpDomainList.push(
                {
                    title: <LocalDragTreeNode title={domain.Name} nodeData={{
                        nodeType: NODE_TYPE_TENCLOUD_DNS_RECORD_LIST,
                        dataSourceId: dataSource.id,
                        extraInfo: domain.Name,
                        height: 500,
                        width: 500,
                    }} />,
                    key: `${dataSource.id}/dnspod/${domain.DomainId}`,
                }
            );
        }
        tmpTreeData.push({
            title: "云解析",
            key: `${dataSource.id}/dnspod`,
            children: tmpDomainList,
        });
    }
    if (sourceInfo.enableCdn) {
        const tmpDomainList: TreeDataNode[] = [];
        const res = await listCdnDomain(sourceInfo.secretId, sourceInfo.secretKey);
        for (const domain of res.Response.Domains) {
            tmpDomainList.push(
                {
                    title: <LocalDragTreeNode title={domain.Domain} nodeData={{
                        nodeType: NODE_TYPE_TENCLOUD_CDN_STATUS,
                        dataSourceId: dataSource.id,
                        extraInfo: domain.Domain,
                        height: 300,
                        width: 250,
                    }} />,
                    key: `${dataSource.id}/dnspod/${domain.ResourceId}`,
                }
            );
        }
        tmpTreeData.push({
            title: "内容分发网络",
            key: `${dataSource.id}/cdn`,
            children: tmpDomainList,
        });
    }
    return tmpTreeData;
}