//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import type { NodeProps } from "reactflow";
import LocalNodeWrap from '../LocalNodeWrap';
import type { NodeData, TenCloudLighthouseInstanceExtraInfo } from '../types';
import { observer } from 'mobx-react';
import type { LighthouseInstance } from '@/api/tencloud/lighthouse';
import { listInstance } from '@/api/tencloud/lighthouse';
import { getRegionName } from './utils';
import { get_source, type TenCloudSourceInfo, type UserDataSourceInfo } from '@/api/user_dataview';
import { Button, Descriptions } from 'antd';
import moment from 'moment';
import { ReloadOutlined } from '@ant-design/icons';

const TenCloudLighthouseInstanceNode = (props: NodeProps<NodeData>) => {

    const [instance, setInstance] = useState<LighthouseInstance | null>(null);
    const [regionName, setRegionName] = useState("");
    const [dataSource, setDataSource] = useState<UserDataSourceInfo | undefined>(undefined);

    const loadDataSource = async () => {
        const res = await get_source(props.data.dataSourceId ?? "");
        setDataSource(res);
    };

    const loadRegionName = () => {
        const extraInfo = props.data.extraInfo as TenCloudLighthouseInstanceExtraInfo;
        const name = getRegionName(extraInfo.region);
        setRegionName(name);
    };

    const loadInstance = async () => {
        if (dataSource == undefined) {
            return;
        }
        const sourceInfo = JSON.parse(dataSource.source_info) as TenCloudSourceInfo;
        const extraInfo = props.data.extraInfo as TenCloudLighthouseInstanceExtraInfo;
        const res = await listInstance(sourceInfo.secretId, sourceInfo.secretKey, extraInfo.region, [extraInfo.instanceId]);
        if (res.Response.InstanceSet.length == 1) {
            setInstance(res.Response.InstanceSet[0]);
        }
    };

    useEffect(() => {
        loadDataSource();
    }, []);
    
    useEffect(()=>{
        if(dataSource != undefined){
            loadRegionName();
            loadInstance();
        }
    },[dataSource]);
    return (
        <LocalNodeWrap title={`实例:${instance?.InstanceName ?? ""}(${regionName})`} nodeData={props} canChangeBgColor
            extraButtonList={[
                (
                    <Button type="text" icon={<ReloadOutlined />} onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        loadInstance();
                    }} title='刷新' />
                ),
            ]}>
            {instance != null && (
                <Descriptions column={1} bordered style={{ backgroundColor: props.data.bgColor ?? "white", overflowY: "scroll", height: props.data.height - 90 }}
                    labelStyle={{ backgroundColor: props.data.bgColor ?? "white", width: "100px" }}>
                    <Descriptions.Item label="CPU数量">{instance.CPU}</Descriptions.Item>
                    <Descriptions.Item label="内存">{instance.Memory}G</Descriptions.Item>
                    <Descriptions.Item label="系统盘类型">{instance.SystemDisk.DiskType}</Descriptions.Item>
                    <Descriptions.Item label="系统盘大小">{instance.SystemDisk.DiskSize}G</Descriptions.Item>
                    {instance.PrivateAddresses.length > 0 && (
                        <Descriptions.Item label="内网地址">{instance.PrivateAddresses.join(",")}</Descriptions.Item>
                    )}
                    {instance.PublicAddresses.length > 0 && (
                        <Descriptions.Item label="外网地址">{instance.PublicAddresses.join(",")}</Descriptions.Item>
                    )}
                    <Descriptions.Item label="实例状态">{instance.InstanceState}</Descriptions.Item>
                    <Descriptions.Item label="系统大类">{instance.PlatformType}</Descriptions.Item>
                    <Descriptions.Item label="系统类型">{instance.Platform}</Descriptions.Item>
                    <Descriptions.Item label="系统名称">{instance.OsName}</Descriptions.Item>
                    <Descriptions.Item label="创建时间">{moment(instance.CreatedTime).format("YYYY-MM-DD")}</Descriptions.Item>
                    <Descriptions.Item label="过期时间">{moment(instance.ExpiredTime).format("YYYY-MM-DD")}</Descriptions.Item>
                </Descriptions>
            )}

        </LocalNodeWrap>
    );
}

export default observer(TenCloudLighthouseInstanceNode);