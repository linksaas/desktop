//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

export function getRegionName(region: string): string {
    if (region == "ap-bangkok") {
        return "亚太东南（曼谷）";
    }
    if (region == "ap-beijing") {
        return "华北地区（北京）";
    }
    if (region == "ap-chengdu") {
        return "西南地区（成都）";
    }
    if (region == "ap-guangzhou") {
        return "华南地区（广州）";
    }
    if (region == "ap-hongkong") {
        return "港澳台地区（中国香港）";
    }
    if (region == "ap-jakarta") {
        return "亚太东南（雅加达）";
    }
    if (region == "ap-mumbai") {
        return "亚太南部（孟买）";
    }
    if (region == "ap-nanjing") {
        return "华东地区（南京）"
    }
    if (region == "ap-seoul") {
        return "亚太东北（首尔）";
    }
    if (region == "ap-shanghai") {
        return "华东地区（上海）";
    }
    if (region == "ap-singapore") {
        return "亚太东南（新加坡）";
    }
    if (region == "ap-tokyo") {
        return "亚太东北（东京）";
    }
    if (region == "eu-frankfurt") {
        return "欧洲地区（法兰克福）";
    }
    if (region == "na-ashburn") {
        return "美国东部（弗吉尼亚）";
    }
    if (region == "na-siliconvalley") {
        return "美国西部（硅谷）";
    }
    if (region == "sa-saopaulo") {
        return "南美地区（圣保罗）";
    }
    return "";
}