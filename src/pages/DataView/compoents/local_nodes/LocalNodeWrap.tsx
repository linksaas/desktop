//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from 'react';
import { NodeResizeControl, type NodeProps } from "reactflow";
import { NodeData } from './types';
import { observer } from 'mobx-react';
import { Button, Card, Popover, Space } from 'antd';
import { Compact as CompactPicker } from '@uiw/react-color';
import { FormatPainterFilled, MoreOutlined } from '@ant-design/icons';
import { useDataViewStores } from '../../store';


function ResizeIcon() {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="20"
            height="20"
            viewBox="0 0 24 24"
            strokeWidth="2"
            stroke="#ff0071"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
            style={{ position: 'absolute', right: 5, bottom: 5, zIndex: 9000 }}
        >
            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
            <polyline points="16 20 20 20 20 16" />
            <line x1="14" y1="14" x2="20" y2="20" />
            <polyline points="8 4 4 4 4 8" />
            <line x1="4" y1="4" x2="10" y2="10" />
        </svg>
    );
}

export interface LocalNodeWrapProps {
    title: React.ReactNode;
    nodeData: NodeProps<NodeData>;
    canChangeBgColor: boolean;
    children?: React.ReactNode;
    extraButtonList?: React.ReactNode[];
}

const LocalNodeWrap = (props: LocalNodeWrapProps) => {
    const dataViewStore = useDataViewStores();

    const [hover, setHover] = useState(false);

    return (
        <Card title={props.title} style={{
            width: props.nodeData.data.width,
            height: props.nodeData.data.height,
            backgroundColor: props.nodeData.data.bgColor ?? "white",
            borderRadius: "10px",
            boxShadow: "10px 10px 5px 0px #888"
        }}
            headStyle={{ border: "none", height: "30px" }}
            onMouseEnter={() => setHover(true)}
            onMouseLeave={() => setHover(false)}
            extra={
                <>
                    {hover == true && (
                        <Space className="nodrag nopan nowheel">
                            {props.canChangeBgColor && (
                                <Popover trigger="click" placement="top" content={
                                    <CompactPicker onChange={color => {
                                        dataViewStore.localStore.updateBgColor(props.nodeData.id, color.hex);
                                        dataViewStore.localStore.saveNodeList();
                                    }} />
                                }>
                                    <FormatPainterFilled />
                                </Popover>
                            )}

                            {(props.extraButtonList ?? []).map((btn, btnIndex) => (
                                <span key={btnIndex}>
                                    {btn}
                                </span>
                            ))}
                            <Popover trigger="click" placement="bottom" content={
                                <Button type="link" danger onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    dataViewStore.localStore.removeNode(props.nodeData.id);
                                    dataViewStore.localStore.saveNodeList();
                                }}>删除</Button>
                            }>
                                <MoreOutlined />
                            </Popover>
                        </Space>
                    )}
                </>

            }>
            <NodeResizeControl minWidth={100} minHeight={40} style={{ zIndex: 4000 }}
                shouldResize={() => true} >
                <ResizeIcon />
            </NodeResizeControl>
            {props.children != undefined && props.children}
        </Card>
    );
};

export default observer(LocalNodeWrap);