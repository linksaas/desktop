//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from 'react';
import type { GitSourceInfo, UserDataSourceInfo } from "@/api/user_dataview";
import { Space, type TreeDataNode } from 'antd';
import { get_self_info } from "@/api/gitcode/user";
import { list_org } from "@/api/gitcode/org";
import { list_org_repo } from "@/api/gitcode/repo";
import LocalDragTreeNode from '../LocalDragTreeNode';
import { NODE_TYPE_GITCODE_BASIC_INFO, NODE_TYPE_GITCODE_COMMIT_LIST, NODE_TYPE_GITCODE_ISSUE_LIST } from '../types';
import { ProjectOutlined } from '@ant-design/icons';



export const setupGitCodeTreeData = async (dataSource: UserDataSourceInfo ):Promise<TreeDataNode[]> => {
    const tmpTreeData: TreeDataNode[] = [];
    const sourceInfo = JSON.parse(dataSource.source_info) as GitSourceInfo;
    //获取当前用户信息
    const userInfo = await get_self_info(sourceInfo.accessToken);
    //列出所有组织
    const orgList = await list_org(sourceInfo.accessToken);
    for (const org of [{
        id: userInfo.id,
        login: userInfo.login,
        path: userInfo.login,
        name: userInfo.name,
        url: userInfo.html_url,
        description: "",
    }, ...orgList]) {
        const orgRepoNodeList: TreeDataNode[] = [];
        //列出组织仓库
        const repoList = await list_org_repo(sourceInfo.accessToken, org.login);
        for (const repo of repoList) {
            orgRepoNodeList.push({
                title: (<Space>
                    <ProjectOutlined />
                    {repo.name}
                </Space>),
                key: `${dataSource.id}/${org.login}/${repo.id}`,
                children: [
                    {
                        title: <LocalDragTreeNode title="基本信息" nodeData={{
                            nodeType: NODE_TYPE_GITCODE_BASIC_INFO,
                            dataSourceId: dataSource.id,
                            extraInfo: {
                                ownerName: org.login,
                                repoName: repo.path,
                            },
                            height: 80,
                            width: 300,
                        }} />,
                        key: `${dataSource.id}/${org.login}/${repo.id}/basicInfo`,
                    },
                    {
                        title: <LocalDragTreeNode title="工单列表" nodeData={{
                            nodeType: NODE_TYPE_GITCODE_ISSUE_LIST,
                            dataSourceId: dataSource.id,
                            extraInfo: {
                                ownerName: org.login,
                                repoName: repo.path,
                            },
                            height: 500,
                            width: 500,
                        }} />,
                        key: `${dataSource.id}/${org.login}/${repo.id}/issueList`,
                    },
                    {
                        title: <LocalDragTreeNode title="提交列表" nodeData={{
                            nodeType: NODE_TYPE_GITCODE_COMMIT_LIST,
                            dataSourceId: dataSource.id,
                            extraInfo: {
                                ownerName: org.login,
                                repoName: repo.path,
                            },
                            height: 500,
                            width: 500,
                        }} />,
                        key: `${dataSource.id}/${org.login}/${repo.id}/commitList`,
                    },
                ],
            });
        }
        tmpTreeData.push({
            title: org.login,
            key: `${dataSource.id}/${org.login}`,
            children: orgRepoNodeList,
        });
    }
    return tmpTreeData;
};