//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import TextNode from './base/TextNode';
import GroupNode from './base/GroupNode';
import AtomGitIssueListNode from './atomgit/AtomGitIssueListNode';
import GitCodeIssueListNode from './gitcode/GitCodeIssueListNode';
import GiteeIssueListNode from './gitee/GiteeIssueListNode';
import AtomGitBasicInfoNode from './atomgit/AtomGitBasicInfoNode';
import GitCodeBasicInfoNode from './gitcode/GitCodeBasicInfoNode';
import GiteeBasicInfoNode from './gitee/GiteeBasicInfoNode';
import GitLabBasicInfoNode from './gitlab/GitLabBasicInfoNode';
import GitLabTodoListNode from './gitlab/GitLabTodoListNode';
import GitLabIssueListNode from './gitlab/GitLabIssueListNode';
import GitLabCommitListNode from './gitlab/GitLabCommitListNode';
import GitLabReleaseListNode from './gitlab/GitLabReleaseListNode';
import TenCloudLighthouseInstanceNode from './tencloud/TenCloudLighthouseInstanceNode';
import TenCloudCdnStatusNode from './tencloud/TenCloudCdnStatusNode';
import TenCloudDnsRecordListNode from './tencloud/TenCloudDnsRecordListNode';
import { NODE_TYPE_ATOMGIT_BASIC_INFO, NODE_TYPE_ATOMGIT_ISSUE_LIST, NODE_TYPE_BASE_GROUP, NODE_TYPE_BASE_IFRAME, NODE_TYPE_BASE_TEXT, NODE_TYPE_GITCODE_BASIC_INFO, NODE_TYPE_GITCODE_COMMIT_LIST, NODE_TYPE_GITCODE_ISSUE_LIST, NODE_TYPE_GITEE_BASIC_INFO, NODE_TYPE_GITEE_ISSUE_LIST, NODE_TYPE_GITLAB_BASIC_INFO, NODE_TYPE_GITLAB_COMMIT_LIST, NODE_TYPE_GITLAB_ISSUE_LIST, NODE_TYPE_GITLAB_RELEASE_LIST, NODE_TYPE_GITLAB_TODO_LIST, NODE_TYPE_TENCLOUD_CDN_STATUS, NODE_TYPE_TENCLOUD_DNS_RECORD_LIST, NODE_TYPE_TENCLOUD_LIGHTHOUSE_INSTANCE } from './types';
import type { NODE_TYPE } from './types';
import IframeNode from './base/IframeNode';
import GitCodeCommitListNode from './gitcode/GitCodeCommitListNode';

export const allNodeTyps = {
    TextNode: TextNode,
    GroupNode: GroupNode,
    IframeNode: IframeNode,
    AtomGitBasicInfoNode: AtomGitBasicInfoNode,
    AtomGitIssueListNode: AtomGitIssueListNode,
    GitCodeIssueListNode: GitCodeIssueListNode,
    GitCodeBasicInfoNode: GitCodeBasicInfoNode,
    GitCodeCommitListNode: GitCodeCommitListNode,
    GiteeIssueListNode: GiteeIssueListNode,
    GiteeBasicInfoNode: GiteeBasicInfoNode,
    GitLabBasicInfoNode: GitLabBasicInfoNode,
    GitLabTodoListNode: GitLabTodoListNode,
    GitLabIssueListNode: GitLabIssueListNode,
    GitLabCommitListNode: GitLabCommitListNode,
    GitLabReleaseListNode: GitLabReleaseListNode,
    TenCloudLighthouseInstanceNode: TenCloudLighthouseInstanceNode,
    TenCloudDnsRecordListNode: TenCloudDnsRecordListNode,
    TenCloudCdnStatusNode: TenCloudCdnStatusNode,
};


export function getNodeTypeStr(nodeType: NODE_TYPE): string {
    if (nodeType == NODE_TYPE_BASE_TEXT) {
        return "TextNode";
    } else if (nodeType == NODE_TYPE_BASE_GROUP) {
        return "GroupNode";
    } else if (nodeType == NODE_TYPE_BASE_IFRAME) {
        return "IframeNode";
    } else if (nodeType == NODE_TYPE_ATOMGIT_ISSUE_LIST) {
        return "AtomGitIssueListNode";
    } else if (nodeType == NODE_TYPE_GITCODE_ISSUE_LIST) {
        return "GitCodeIssueListNode";
    } else if (nodeType == NODE_TYPE_GITEE_ISSUE_LIST) {
        return "GiteeIssueListNode";
    } else if (nodeType == NODE_TYPE_ATOMGIT_BASIC_INFO) {
        return "AtomGitBasicInfoNode";
    } else if (nodeType == NODE_TYPE_GITCODE_BASIC_INFO) {
        return "GitCodeBasicInfoNode";
    } else if (nodeType == NODE_TYPE_GITCODE_COMMIT_LIST) {
        return "GitCodeCommitListNode";
    } else if (nodeType == NODE_TYPE_GITEE_BASIC_INFO) {
        return "GiteeBasicInfoNode";
    } else if (nodeType == NODE_TYPE_GITLAB_BASIC_INFO) {
        return "GitLabBasicInfoNode";
    } else if (nodeType == NODE_TYPE_GITLAB_TODO_LIST) {
        return "GitLabTodoListNode";
    } else if (nodeType == NODE_TYPE_GITLAB_ISSUE_LIST) {
        return "GitLabIssueListNode";
    } else if (nodeType == NODE_TYPE_GITLAB_COMMIT_LIST) {
        return "GitLabCommitListNode";
    } else if (nodeType == NODE_TYPE_GITLAB_RELEASE_LIST) {
        return "GitLabReleaseListNode";
    } else if (nodeType == NODE_TYPE_TENCLOUD_LIGHTHOUSE_INSTANCE) {
        return "TenCloudLighthouseInstanceNode";
    } else if (nodeType == NODE_TYPE_TENCLOUD_DNS_RECORD_LIST) {
        return "TenCloudDnsRecordListNode";
    } else if (nodeType == NODE_TYPE_TENCLOUD_CDN_STATUS) {
        return "TenCloudCdnStatusNode";
    }
    return "";
}