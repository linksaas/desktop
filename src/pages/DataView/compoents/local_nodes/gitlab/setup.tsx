//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from 'react';
import { type GitSourceInfo, type UserDataSourceInfo } from "@/api/user_dataview";
import { Space, type TreeDataNode } from 'antd';
import { get_self_info } from "@/api/gitlab/user";
import { list_user_repo } from "@/api/gitlab/repo";
import LocalDragTreeNode from '../LocalDragTreeNode';
import { NODE_TYPE_GITLAB_BASIC_INFO, NODE_TYPE_GITLAB_COMMIT_LIST, NODE_TYPE_GITLAB_ISSUE_LIST, NODE_TYPE_GITLAB_RELEASE_LIST, NODE_TYPE_GITLAB_TODO_LIST } from '../types';
import { ProjectOutlined, UserOutlined } from '@ant-design/icons';



export const setupGitLabTreeData = async (dataSource: UserDataSourceInfo): Promise<TreeDataNode[]> => {
    const tmpTreeData: TreeDataNode[] = [];
    const sourceInfo = JSON.parse(dataSource.source_info) as GitSourceInfo;
    const url = new URL(sourceInfo.baseUrl ?? "");
    const baseUrl = `${url.protocol}//${url.host}`;
    //获取当前用户信息
    const userInfo = await get_self_info(baseUrl, sourceInfo.accessToken);
    //列出用户仓库
    const repoList = await list_user_repo(baseUrl, sourceInfo.accessToken);
    const userRepoNodeList: TreeDataNode[] = [];
    for (const repo of repoList) {
        userRepoNodeList.push({
            title: (<Space>
                <ProjectOutlined />
                {repo.path_with_namespace}
            </Space>),
            key: `${dataSource.id}/${userInfo.id}/${repo.id}`,
            children: [
                {
                    title: <LocalDragTreeNode title="基本信息" nodeData={{
                        nodeType: NODE_TYPE_GITLAB_BASIC_INFO,
                        dataSourceId: dataSource.id,
                        extraInfo: {
                            ownerName: userInfo.username,
                            repoName: `${repo.id}`,
                        },
                        height: 80,
                        width: 300,
                    }} />,
                    key: `${dataSource.id}/${userInfo.id}/${repo.id}/basicInfo`,
                },
                {
                    title: <LocalDragTreeNode title="提交列表" nodeData={{
                        nodeType: NODE_TYPE_GITLAB_COMMIT_LIST,
                        dataSourceId: dataSource.id,
                        extraInfo: {
                            ownerName: userInfo.username,
                            repoName: `${repo.id}`,
                        },
                        height: 500,
                        width: 500,
                    }} />,
                    key: `${dataSource.id}/${userInfo.id}/${repo.id}/commitList`,
                },
                {
                    title: <LocalDragTreeNode title="工单列表" nodeData={{
                        nodeType: NODE_TYPE_GITLAB_ISSUE_LIST,
                        dataSourceId: dataSource.id,
                        extraInfo: {
                            ownerName: userInfo.username,
                            repoName: `${repo.id}`,
                        },
                        height: 500,
                        width: 500,
                    }} />,
                    key: `${dataSource.id}/${userInfo.id}/${repo.id}/issueList`,
                },
                {
                    title: <LocalDragTreeNode title="发行版列表" nodeData={{
                        nodeType: NODE_TYPE_GITLAB_RELEASE_LIST,
                        dataSourceId: dataSource.id,
                        extraInfo: {
                            ownerName: userInfo.username,
                            repoName: `${repo.id}`,
                        },
                        height: 500,
                        width: 500,
                    }} />,
                    key: `${dataSource.id}/${userInfo.id}/${repo.id}/releaseList`,
                },
            ],
        });
    }
    tmpTreeData.push({
        title: (<Space>
            <UserOutlined />
            {userInfo.username}
        </Space>),
        key: `${dataSource.id}/${userInfo.id}`,
        children: userRepoNodeList,
    });

    return [
        {
            title: <LocalDragTreeNode title="待办事项" nodeData={{
                nodeType: NODE_TYPE_GITLAB_TODO_LIST,
                dataSourceId: dataSource.id,
                extraInfo: {
                    ownerName: userInfo.username,
                    repoName: "",
                },
                height: 500,
                width: 500,
            }} />,
            key: `${dataSource.id}/${userInfo.id}/todoList`,
        },
        ...tmpTreeData
    ];
}