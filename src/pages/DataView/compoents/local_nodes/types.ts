//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only


export const DND_ITEM_TYPE = "node";

export type NODE_TYPE = number;

export const NODE_TYPE_BASE_TEXT: NODE_TYPE = 1;
export const NODE_TYPE_BASE_GROUP: NODE_TYPE = 2;
export const NODE_TYPE_BASE_IFRAME: NODE_TYPE = 3;

export const NODE_TYPE_ATOMGIT_BASIC_INFO: NODE_TYPE = 11;
export const NODE_TYPE_ATOMGIT_ISSUE_LIST: NODE_TYPE = 12;

export const NODE_TYPE_GITCODE_BASIC_INFO: NODE_TYPE = 21;
export const NODE_TYPE_GITCODE_ISSUE_LIST: NODE_TYPE = 22;
export const NODE_TYPE_GITCODE_COMMIT_LIST: NODE_TYPE = 23;


export const NODE_TYPE_GITEE_BASIC_INFO: NODE_TYPE = 31;
export const NODE_TYPE_GITEE_ISSUE_LIST: NODE_TYPE = 32;

export const NODE_TYPE_GITLAB_BASIC_INFO: NODE_TYPE = 41;
export const NODE_TYPE_GITLAB_TODO_LIST: NODE_TYPE = 42;
export const NODE_TYPE_GITLAB_COMMIT_LIST: NODE_TYPE = 43;
export const NODE_TYPE_GITLAB_ISSUE_LIST: NODE_TYPE = 44;
export const NODE_TYPE_GITLAB_RELEASE_LIST: NODE_TYPE = 45;


export const NODE_TYPE_TENCLOUD_LIGHTHOUSE_INSTANCE: NODE_TYPE = 100;
export const NODE_TYPE_TENCLOUD_DNS_RECORD_LIST: NODE_TYPE = 101;
export const NODE_TYPE_TENCLOUD_CDN_STATUS: NODE_TYPE = 102;


export interface GitExtraInfo {
    ownerName: string;
    repoName: string;
}

export interface TenCloudLighthouseInstanceExtraInfo {
    region: string;
    instanceId: string;
}

export interface NodeData {
    nodeType: NODE_TYPE;
    dataSourceId?: string;
    extraInfo: string | GitExtraInfo | TenCloudLighthouseInstanceExtraInfo;
    width: number;
    height: number;
    bgColor?: string;
}