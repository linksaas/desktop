//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from 'react';
import { type GitSourceInfo, type UserDataSourceInfo } from "@/api/user_dataview";
import { Space, type TreeDataNode } from 'antd';
import { get_self_info } from "@/api/gitee/user";
import { list_user_repo } from "@/api/gitee/repo";
import LocalDragTreeNode from '../LocalDragTreeNode';
import { NODE_TYPE_GITEE_BASIC_INFO, NODE_TYPE_GITEE_ISSUE_LIST } from '../types';
import { ProjectOutlined, UserOutlined } from '@ant-design/icons';



export const setupGiteeTreeData = async (dataSource: UserDataSourceInfo): Promise<TreeDataNode[]> => {
    const tmpTreeData: TreeDataNode[] = [];
    const sourceInfo = JSON.parse(dataSource.source_info) as GitSourceInfo;
    //获取当前用户信息
    const userInfo = await get_self_info(sourceInfo.accessToken);
    //列出用户仓库
    const repoList = await list_user_repo(sourceInfo.accessToken, 99, 1);
    const userRepoNodeList: TreeDataNode[] = [];
    for (const repo of repoList) {
        userRepoNodeList.push({
            title: (<Space>
                <ProjectOutlined />
                {repo.name}
            </Space>),
            key: `${dataSource.id}/${userInfo.login}/${repo.id}`,
            children: [
                {
                    title: <LocalDragTreeNode title="基本信息" nodeData={{
                        nodeType: NODE_TYPE_GITEE_BASIC_INFO,
                        dataSourceId: dataSource.id,
                        extraInfo: {
                            ownerName: userInfo.login,
                            repoName: repo.path,
                        },
                        height: 80,
                        width: 300,
                    }} />,
                    key: `${dataSource.id}/${userInfo.login}/${repo.id}/basicInfo`,
                },
                {
                    title: <LocalDragTreeNode title="工单列表" nodeData={{
                        nodeType: NODE_TYPE_GITEE_ISSUE_LIST,
                        dataSourceId: dataSource.id,
                        extraInfo: {
                            ownerName: userInfo.login,
                            repoName: repo.path,
                        },
                        height: 500,
                        width: 500,
                    }} />,
                    key: `${dataSource.id}/${userInfo.login}/${repo.id}/issueList`,
                },
            ],
        });
    }
    tmpTreeData.push({
        title: (<Space>
            <UserOutlined />
            {userInfo.login}
        </Space>),
        key: `${dataSource.id}/${userInfo.login}`,
        children: userRepoNodeList,
    });

    return tmpTreeData;
};