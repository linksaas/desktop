//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import type { NodeProps } from "reactflow";
import { GitExtraInfo, NodeData } from '../types';
import LocalNodeWrap from '../LocalNodeWrap';
import { Button, Space } from 'antd';
import { BugOutlined, EyeOutlined, ForkOutlined, ReloadOutlined, StarOutlined } from '@ant-design/icons';
import type { GiteeRepo } from "@/api/gitee/repo";
import { get_repo } from "@/api/gitee/repo";
import { get_source, get_type_name, type UserDataSourceInfo, type GitSourceInfo } from '@/api/user_dataview';
import { open as shell_open } from '@tauri-apps/api/shell';


const GiteeBasicInfoNode = (props: NodeProps<NodeData>) => {
    const [repoInfo, setRepoInfo] = useState<GiteeRepo | null>(null);
    const [dataSource, setDataSource] = useState<UserDataSourceInfo | undefined>(undefined);

    const loadDataSource = async () => {
        const res = await get_source(props.data.dataSourceId ?? "");
        setDataSource(res);
    };

    const loadRepoInfo = async () => {
        if (dataSource == undefined) {
            return;
        }
        const sourceInfo = JSON.parse(dataSource.source_info) as GitSourceInfo;
        const extraInfo = props.data.extraInfo as GitExtraInfo;
        const res = await get_repo(sourceInfo.accessToken, extraInfo.ownerName, extraInfo.repoName);
        setRepoInfo(res);
    };


    const getSourceTypeName = () => {
        if (dataSource == undefined) {
            return "";
        }
        return get_type_name(dataSource.source_type);
    };


    useEffect(() => {
        loadDataSource();
    }, []);

    useEffect(()=>{
        if(dataSource != undefined){
            loadRepoInfo();
        }
    },[dataSource]);

    return (
        <LocalNodeWrap title={
            <Space>
                <span>{getSourceTypeName()}</span>
                <span>基本信息</span>
                {repoInfo != null && (
                    <a onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        shell_open(repoInfo.html_url);
                    }}>{repoInfo.full_name}</a>
                )}
            </Space>
        } nodeData={props} extraButtonList={[
            <Button type="text" icon={<ReloadOutlined />} onClick={e => {
                e.stopPropagation();
                e.preventDefault();
                loadRepoInfo();
            }} title='刷新' />
        ]} canChangeBgColor>
            <Space size="large">
                <Space style={{ fontSize: "16px" }}>
                    <EyeOutlined />
                    {repoInfo?.watchers_count ?? 0}
                </Space>
                <Space style={{ fontSize: "16px" }}>
                    <StarOutlined />
                    {repoInfo?.stargazers_count ?? 0}
                </Space>
                <Space style={{ fontSize: "16px" }}>
                    <ForkOutlined />
                    {repoInfo?.forks_count ?? 0}
                </Space>
                <Space style={{ fontSize: "16px" }}>
                    <BugOutlined />
                    {repoInfo?.open_issues_count ?? 0}
                </Space>
            </Space>
        </LocalNodeWrap>
    );
};

export default GiteeBasicInfoNode;