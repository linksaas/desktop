//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import { SOURCE_TYPE_ATOMGIT, SOURCE_TYPE_GITCODE, SOURCE_TYPE_GITEE, SOURCE_TYPE_GITLAB, SOURCE_TYPE_TEN_COLOUD, get_type_name, list_source } from "@/api/user_dataview";
import type { UserDataSourceInfo } from "@/api/user_dataview";
import { Button, Card, Select, Space, Spin, Tree } from 'antd';
import { CaretDownFilled, CaretUpFilled, LoadingOutlined } from '@ant-design/icons';
import { NODE_TYPE_BASE_GROUP, NODE_TYPE_BASE_IFRAME, NODE_TYPE_BASE_TEXT } from "./types";
import type { TreeDataNode } from 'antd';
import LocalDragTreeNode from './LocalDragTreeNode';
import { setupAtomGitTreeData } from './atomgit/setup';
import { setupGitCodeTreeData } from './gitcode/setup';
import { setupGiteeTreeData } from './gitee/setup';
import { setupGitLabTreeData } from './gitlab/setup';
import { setupTenCloudTreeData } from './tencloud/setup';


const LocalNodeTreePanel = () => {
    const [expand, setExpand] = useState(false);
    const [treeData, setTreeData] = useState<TreeDataNode[]>([]);
    const [curDataSourceId, setCurDataSourceId] = useState("");
    const [dataSourceList, setDataSourceList] = useState<UserDataSourceInfo[]>([]);
    const [inLoad, setInLoad] = useState(false);

    const loaDataSourceList = async () => {
        const res = await list_source();
        setDataSourceList(res);
    };

    const setupTreeData = async () => {
        if (curDataSourceId == "") {
            setTreeData([
                {
                    title: <LocalDragTreeNode title="分组" nodeData={{
                        nodeType: NODE_TYPE_BASE_GROUP,
                        extraInfo: "",
                        height: 500,
                        width: 500,
                    }} />,
                    key: "base/group",
                },
                {
                    title: <LocalDragTreeNode title="文本" nodeData={{
                        nodeType: NODE_TYPE_BASE_TEXT,
                        extraInfo: "",
                        height: 80,
                        width: 300,
                    }} />,
                    key: "base/text",
                },
                {
                    title: <LocalDragTreeNode title='内嵌网页' nodeData={{
                        nodeType: NODE_TYPE_BASE_IFRAME,
                        extraInfo: "",
                        height: 500,
                        width: 500,
                    }} />,
                    key: "base/iframe"
                },
            ]);
            return;
        }
        setTreeData([]);
        try {
            setInLoad(true);
            for (const dataSource of dataSourceList) {
                if (dataSource.id != curDataSourceId) {
                    continue;
                }
                if (dataSource.source_type == SOURCE_TYPE_ATOMGIT) {
                    const tmpTreeData = await setupAtomGitTreeData(dataSource);
                    setTreeData(tmpTreeData);
                } else if (dataSource.source_type == SOURCE_TYPE_GITCODE) {
                    const tmpTreeData = await setupGitCodeTreeData(dataSource);
                    setTreeData(tmpTreeData);
                } else if (dataSource.source_type == SOURCE_TYPE_GITEE) {
                    const tmpTreeData = await setupGiteeTreeData(dataSource);
                    setTreeData(tmpTreeData);
                } else if (dataSource.source_type == SOURCE_TYPE_GITLAB) {
                    const tmpTreeData = await setupGitLabTreeData(dataSource);
                    setTreeData(tmpTreeData);
                } else if (dataSource.source_type == SOURCE_TYPE_TEN_COLOUD) {
                    const tmpTreeData = await setupTenCloudTreeData(dataSource);
                    setTreeData(tmpTreeData);
                }
            }
        } finally {
            setInLoad(false);
        }
    };

    useEffect(() => {
        loaDataSourceList();
    }, []);

    useEffect(() => {
        setupTreeData();
    }, [curDataSourceId]);

    return (
        <div>
            {expand == false && (
                <Space style={{ cursor: "pointer", backgroundColor: "white", padding: "2px 10px" }} onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    setExpand(true);
                }}>
                    <span style={{ fontSize: "16px" }}>节点库</span>
                    <CaretDownFilled style={{ fontSize: "24px" }} />
                </Space>
            )}
            {expand == true && (
                <Card title="节点库" style={{ width: "250px" }} bodyStyle={{ height: "50vh", overflowY: "scroll" }}
                    extra={
                        <Space>
                            <Select value={curDataSourceId} onChange={value => setCurDataSourceId(value)} style={{ width: "130px" }}>
                                <Select.Option value="">基础节点</Select.Option>
                                {dataSourceList.map(item => (
                                    <Select.Option key={item.id} value={item.id}>{`${item.name}(${get_type_name(item.source_type)})`}</Select.Option>
                                ))}
                            </Select>
                            <Button type="text" icon={<CaretUpFilled style={{ fontSize: "24px" }} />} onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setExpand(false);
                            }} title='收起' />
                        </Space>
                    }>
                    {inLoad && (
                        <div style={{ display: "flex", justifyContent: "space-around" }}>
                            <Spin indicator={<LoadingOutlined style={{ fontSize: 24 }} spin />} tip="加载中..." />
                        </div>
                    )}
                    <Tree treeData={treeData} />
                </Card>
            )}
        </div>
    );
};

export default LocalNodeTreePanel;