//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from 'react';
import type { GitSourceInfo, UserDataSourceInfo } from "@/api/user_dataview";
import { Space, type TreeDataNode } from 'antd';
import { get_self_info } from "@/api/atomgit/user";
import { list_user_org } from "@/api/atomgit/org";
import { list_user_repo, list_org_repo } from "@/api/atomgit/repo";
import { NODE_TYPE_ATOMGIT_BASIC_INFO, NODE_TYPE_ATOMGIT_ISSUE_LIST } from '../types';
import { ProjectOutlined, UserOutlined } from '@ant-design/icons';
import LocalDragTreeNode from '../LocalDragTreeNode';

const getRepoPath = (fullName: string) => {
    const parts = fullName.split("/");
    if (parts.length > 1) {
        return parts[1];
    }
    return "";
};

export const setupAtomGitTreeData = async (dataSource: UserDataSourceInfo): Promise<TreeDataNode[]> => {
    const tmpTreeData: TreeDataNode[] = [];
    const sourceInfo = JSON.parse(dataSource.source_info) as GitSourceInfo;
    //获取当前用户信息
    const userInfo = await get_self_info(sourceInfo.accessToken);
    //列出用户仓库
    const repoList = await list_user_repo(sourceInfo.accessToken, userInfo.login, 99, 1);
    const userRepoNodeList: TreeDataNode[] = [];
    for (const repo of repoList) {
        userRepoNodeList.push({
            title: (<Space>
                <ProjectOutlined />
                {repo.name}
            </Space>),
            key: `${dataSource.id}/${userInfo.login}/${repo.id}`,
            children: [
                {
                    title: <LocalDragTreeNode title="基本信息" nodeData={{
                        nodeType: NODE_TYPE_ATOMGIT_BASIC_INFO,
                        dataSourceId: dataSource.id,
                        extraInfo: {
                            ownerName: userInfo.login,
                            repoName: getRepoPath(repo.full_name),
                        },
                        height: 80,
                        width: 300,
                    }} />,
                    key: `${dataSource.id}/${userInfo.login}/${repo.id}/basicInfo`,
                },
                {
                    title: <LocalDragTreeNode title="工单列表" nodeData={{
                        nodeType: NODE_TYPE_ATOMGIT_ISSUE_LIST,
                        dataSourceId: dataSource.id,
                        extraInfo: {
                            ownerName: userInfo.login,
                            repoName: getRepoPath(repo.full_name),
                        },
                        height: 500,
                        width: 500,
                    }} />,
                    key: `${dataSource.id}/${userInfo.login}/${repo.id}/issueList`,
                },
            ],
        });
    }
    tmpTreeData.push({
        title: (<Space>
            <UserOutlined />
            {userInfo.login}
        </Space>),
        key: `${dataSource.id}/${userInfo.login}`,
        children: userRepoNodeList,
    });

    //获取组织信息
    const orgList = await list_user_org(sourceInfo.accessToken);
    for (const org of orgList) {
        const orgRepoNodeList: TreeDataNode[] = [];
        //列出组织仓库
        const repoList = await list_org_repo(sourceInfo.accessToken, org.login, 99, 1);
        for (const repo of repoList) {
            orgRepoNodeList.push({
                title: (<Space>
                    <ProjectOutlined />
                    {repo.name}
                </Space>),
                key: `${dataSource.id}/${org.login}/${repo.id}`,
                children: [
                    {
                        title: <LocalDragTreeNode title="基本信息" nodeData={{
                            nodeType: NODE_TYPE_ATOMGIT_BASIC_INFO,
                            dataSourceId: dataSource.id,
                            extraInfo: {
                                ownerName: org.login,
                                repoName: getRepoPath(repo.full_name),
                            },
                            height: 80,
                            width: 300,
                        }} />,
                        key: `${dataSource.id}/${org.login}/${repo.id}/basicInfo`,
                    },
                    {
                        title: <LocalDragTreeNode title="工单列表" nodeData={{
                            nodeType: NODE_TYPE_ATOMGIT_ISSUE_LIST,
                            dataSourceId: dataSource.id,
                            extraInfo: {
                                ownerName: org.login,
                                repoName: getRepoPath(repo.full_name),
                            },
                            height: 500,
                            width: 500,
                        }} />,
                        key: `${dataSource.id}/${org.login}/${repo.id}/issueList`,
                    }
                ],
            });
        }
        tmpTreeData.push({
            title: org.login,
            key: `${dataSource.id}/${org.login}`,
            children: orgRepoNodeList,
        });
    }

    return tmpTreeData;
};