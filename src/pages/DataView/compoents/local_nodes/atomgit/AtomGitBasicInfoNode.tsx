//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import type { NodeProps } from "reactflow";
import { GitExtraInfo, NodeData } from '../types';
import LocalNodeWrap from '../LocalNodeWrap';
import { Button, Space } from 'antd';
import { ForkOutlined, ReloadOutlined } from '@ant-design/icons';
import type { AtomGitRepo } from "@/api/atomgit/repo";
import { get_repo } from "@/api/atomgit/repo";
import { get_type_name, get_source, type GitSourceInfo, type UserDataSourceInfo } from '@/api/user_dataview';
import { open as shell_open } from '@tauri-apps/api/shell';


const AtomGitBasicInfoNode = (props: NodeProps<NodeData>) => {
    const [repoInfo, setRepoInfo] = useState<AtomGitRepo | null>(null);

    const [dataSource, setDataSource] = useState<UserDataSourceInfo | undefined>(undefined);

    const loadDataSource = async () => {
        const res = await get_source(props.data.dataSourceId ?? "");
        setDataSource(res);
    };

    const loadRepoInfo = async () => {
        if (dataSource == undefined) {
            return;
        }
        const sourceInfo = JSON.parse(dataSource.source_info) as GitSourceInfo;
        const extraInfo = props.data.extraInfo as GitExtraInfo;
        const res = await get_repo(sourceInfo.accessToken, extraInfo.ownerName, extraInfo.repoName);
        setRepoInfo(res);
        console.log(res);
    };



    const getSourceTypeName = () => {
        if (dataSource == undefined) {
            return "";
        }
        return get_type_name(dataSource.source_type);
    };



    useEffect(() => {
        loadDataSource();
    }, []);

    useEffect(() => {
        if (dataSource != undefined) {
            loadRepoInfo();
        }
    }, [dataSource]);

    return (
        <LocalNodeWrap title={
            <Space>
                <span>{getSourceTypeName()}</span>
                <span>基本信息</span>
                {repoInfo != null && (
                    <a onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        shell_open(repoInfo.html_url);
                    }}>{repoInfo.full_name}</a>
                )}
            </Space>
        } nodeData={props} extraButtonList={[
            <Button type="text" icon={<ReloadOutlined />} onClick={e => {
                e.stopPropagation();
                e.preventDefault();
                loadRepoInfo();
            }} title='刷新' />
        ]} canChangeBgColor>
            <Space style={{ fontSize: "16px" }}>
                <ForkOutlined />
                {repoInfo?.forks ?? 0}
            </Space>
        </LocalNodeWrap>
    );
};

export default AtomGitBasicInfoNode;