//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import type { NodeProps } from "reactflow";
import type { NodeInfo } from '@/api/project_dataview';
import ProjectNodeWrap from '../ProjectNodeWrap';
import { Button, Descriptions } from 'antd';
import { ReloadOutlined } from '@ant-design/icons';
import { get_server, SERVER_TYPE_GRPC, SERVER_TYPE_MONGO, SERVER_TYPE_MYSQL, SERVER_TYPE_POSTGRES, SERVER_TYPE_REDIS, SERVER_TYPE_SSH, type GetServerResponse } from "@/api/project_server";
import { useDataViewStores } from '@/pages/DataView/store';
import { get_session } from '@/api/user';


const RefServerNode = (props: NodeProps<NodeInfo>) => {
    const dataViewStore = useDataViewStores();

    const [dataResponse, setDataResponse] = useState<GetServerResponse | null>(null);

    const loadDataResponse = async () => {
        const sessionId = await get_session();
        const res = await get_server({
            session_id: sessionId,
            project_id: dataViewStore.projectStore.projectId,
            server_id: props.data.node_data.RefTargetId ?? "",
        });
        setDataResponse(res);
    };

    useEffect(() => {
        loadDataResponse();
    }, []);

    return (
        <ProjectNodeWrap title="服务器" nodeData={props} canChangeBgColor
            extraButtonList={[
                <Button type="text" icon={<ReloadOutlined />} onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    loadDataResponse();
                }} />
            ]}>
            {dataResponse != null && (
                <>
                    {dataResponse.code != 0 && <span style={{ color: "red" }}>{dataResponse.err_msg}</span>}
                    {dataResponse.code == 0 && (
                        <div>
                            <Descriptions column={1}>
                                <Descriptions.Item label="服务名称">
                                    {dataResponse.server_info.basic_info.server_name}
                                </Descriptions.Item>
                                <Descriptions.Item label="服务地址">
                                    {dataResponse.server_info.basic_info.addr_list.join(",")}
                                </Descriptions.Item>
                                <Descriptions.Item label="服务类型">
                                    {dataResponse.server_info.basic_info.server_type == SERVER_TYPE_SSH && "ssh"}
                                    {dataResponse.server_info.basic_info.server_type == SERVER_TYPE_MYSQL && "mysql"}
                                    {dataResponse.server_info.basic_info.server_type == SERVER_TYPE_POSTGRES && "postgres"}
                                    {dataResponse.server_info.basic_info.server_type == SERVER_TYPE_MONGO && "mongo"}
                                    {dataResponse.server_info.basic_info.server_type == SERVER_TYPE_REDIS && "redis"}
                                    {dataResponse.server_info.basic_info.server_type == SERVER_TYPE_GRPC && "grpc"}
                                </Descriptions.Item>
                            </Descriptions>
                        </div>
                    )}
                </>
            )}
        </ProjectNodeWrap>
    )
};

export default RefServerNode;
