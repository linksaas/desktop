//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import type { NodeProps } from "reactflow";
import type { NodeInfo } from '@/api/project_dataview';
import ProjectNodeWrap from '../ProjectNodeWrap';
import { Button, Descriptions, Modal, Space, Tag } from 'antd';
import { DislikeFilled, LikeFilled, ReloadOutlined } from '@ant-design/icons';
import { get_idea, type GetIdeaResponse } from '@/api/project_idea';
import { observer } from 'mobx-react';
import { useDataViewStores } from '@/pages/DataView/store';
import { get_session } from '@/api/user';
import type * as NoticeType from '@/api/notice_type';
import { listen } from '@tauri-apps/api/event';
import UserPhoto from '@/components/Portrait/UserPhoto';
import { ReadOnlyEditor } from '@/components/Editor';

const RefIdeaNode = (props: NodeProps<NodeInfo>) => {
    const dataViewStore = useDataViewStores();

    const [dataResponse, setDataResponse] = useState<GetIdeaResponse | null>(null);
    const [showDetailModal, setShowDetailModal] = useState(false);

    const loadDataResponse = async () => {
        const sessionId = await get_session();
        const res = await get_idea({
            session_id: sessionId,
            project_id: dataViewStore.projectStore.projectId,
            idea_id: props.data.node_data.RefTargetId ?? "",
        });
        setDataResponse(res);
    };

    useEffect(() => {
        loadDataResponse();
    }, []);

    useEffect(() => {
        const unListenFn = listen<NoticeType.AllNotice>("notice", ev => {
            const notice = ev.payload;
            if (notice.IdeaNotice?.UpdateIdeaNotice != undefined && notice.IdeaNotice.UpdateIdeaNotice.idea_id == props.data.node_data.RefTargetId) {
                loadDataResponse();
            }
        });
        return () => {
            unListenFn.then((unListen) => unListen());
        };
    }, []);

    return (
        <ProjectNodeWrap title="知识点" nodeData={props} canChangeBgColor
            extraButtonList={[
                <Button type="text" icon={<ReloadOutlined />} onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    loadDataResponse();
                }} />
            ]}>
            {dataResponse != null && (
                <>
                    {dataResponse.code != 0 && <span style={{ color: "red" }}>{dataResponse.err_msg}</span>}
                    {dataResponse.code == 0 && (
                        <div>
                            <a style={{ fontSize: "20px", fontWeight: 700 }} onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setShowDetailModal(true);
                            }}>{dataResponse.idea.basic_info.title}</a>
                            <Descriptions column={1}>
                                <Descriptions.Item label="投票">
                                    <Space>
                                        <div>
                                            <LikeFilled style={{ marginRight: "4px" }} />
                                            {dataResponse.idea.agree_count}
                                        </div>
                                        <div>
                                            <DislikeFilled style={{ marginRight: "4px" }} />
                                            {dataResponse.idea.disagree_count}
                                        </div>
                                    </Space>
                                </Descriptions.Item>
                                <Descriptions.Item label="关键词">
                                    <Space style={{ flexWrap: "wrap" }}>
                                        {dataResponse.idea.basic_info.keyword_list.map((kw, index) => (
                                            <Tag key={index}>{kw}</Tag>
                                        ))}
                                    </Space>
                                </Descriptions.Item>
                                <Descriptions.Item label="创建人">
                                    <Space>
                                        <UserPhoto logoUri={dataResponse.idea.create_logo_uri} style={{ width: "16px", borderRadius: "10px" }} />
                                        {dataResponse.idea.create_display_name}
                                    </Space>
                                </Descriptions.Item>
                            </Descriptions>
                        </div>
                    )}
                </>
            )}
            {dataResponse != null && showDetailModal == true && (
                <Modal open title={dataResponse.idea.basic_info.title} mask={false}
                    footer={null}
                    onCancel={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setShowDetailModal(false);
                    }}>

                    <div style={{ maxHeight: "calc(100vh - 400px)", overflowY: "scroll" }}>
                        <ReadOnlyEditor content={dataResponse.idea.basic_info.content} />
                    </div>
                </Modal>
            )}
        </ProjectNodeWrap>
    )
};

export default observer(RefIdeaNode);
