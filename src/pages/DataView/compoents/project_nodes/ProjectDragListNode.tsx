//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from 'react';
import { useDrag } from 'react-dnd';
import { BlockOutlined } from '@ant-design/icons';
import { DND_ITEM_TYPE } from '@/api/project_dataview';
import type { NODE_TYPE, NodeData } from '@/api/project_dataview';

export interface DragNodeInfo {
    nodeType: NODE_TYPE;
    nodeData: NodeData;
    defaultWidth: number;
    defaultHeight: number;
}

export interface LocalDragTreeNodeProps {
    title: string;
    dragNodeInfo: DragNodeInfo;
}

const ProjectDragTreeNode = (props: LocalDragTreeNodeProps) => {
    const [hover, setHover] = useState(false);

    const [_, drag] = useDrag(() => ({
        type: DND_ITEM_TYPE,
        item: props.dragNodeInfo,
        canDrag: true,
    }));

    return (
        <div ref={drag} style={{ cursor: "move",backgroundColor: hover ? "#aaa" : "#eee",width:"100%" }}
            onMouseEnter={e => {
                e.stopPropagation();
                e.preventDefault();
                setHover(true);
            }}
            onMouseLeave={e => {
                e.stopPropagation();
                e.preventDefault();
                setHover(false);
            }}>
            <div style={{  paddingRight: "10px", paddingLeft: "10px" }}>
                <BlockOutlined />
                &nbsp;&nbsp;{props.title}
                {hover && <br />}
                {hover && <span style={{ color: "green" }}>可拖动到主面板</span>}
            </div>
        </div>
    );
};

export default ProjectDragTreeNode;