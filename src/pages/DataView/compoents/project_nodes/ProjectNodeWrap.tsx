//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from 'react';
import { Handle, NodeResizeControl, Position, type NodeProps } from "reactflow";
import type { NodeInfo } from "@/api/project_dataview";
import { remove_node, update_node_bg_color } from "@/api/project_dataview";
import { observer } from 'mobx-react';
import { Compact as CompactPicker } from '@uiw/react-color';
import { FormatPainterFilled, MoreOutlined } from '@ant-design/icons';
import { useDataViewStores } from '../../store';
import { Button, Card, message, Popover, Space } from 'antd';
import { request } from '@/utils/request';
import { get_session } from '@/api/user';

function ResizeIcon() {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="20"
            height="20"
            viewBox="0 0 24 24"
            strokeWidth="2"
            stroke="#ff0071"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
            style={{ position: 'absolute', right: 5, bottom: 5, zIndex: 9000 }}
        >
            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
            <polyline points="16 20 20 20 20 16" />
            <line x1="14" y1="14" x2="20" y2="20" />
            <polyline points="8 4 4 4 4 8" />
            <line x1="4" y1="4" x2="10" y2="10" />
        </svg>
    );
}

export interface ProjectNodeWrapProps {
    title: React.ReactNode;
    nodeData: NodeProps<NodeInfo>;
    canChangeBgColor: boolean;
    children?: React.ReactNode;
    extraButtonList?: React.ReactNode[];
}


const ProjectNodeWrap = (props: ProjectNodeWrapProps) => {
    const dataViewStore = useDataViewStores();

    const [hover, setHover] = useState(false);

    const removeNode = async () => {
        const sessionId = await get_session();
        await request(remove_node({
            session_id: sessionId,
            project_id: dataViewStore.projectStore.projectId,
            view_id: dataViewStore.projectStore.dataViewId,
            node_id: props.nodeData.data.node_id,
        }));
        message.info("删除成功");
    };

    const updateBgColor = async (bgColor: string) => {
        const sessionId = await get_session();
        await request(update_node_bg_color({
            session_id: sessionId,
            project_id: dataViewStore.projectStore.projectId,
            view_id: dataViewStore.projectStore.dataViewId,
            node_id: props.nodeData.data.node_id,
            bg_color: bgColor,
        }));
    };

    return (
        <Card title={props.title} style={{
            backgroundColor: props.nodeData.data.bg_color ?? "white",
            borderRadius: "10px",
            boxShadow: "10px 10px 5px 0px #888",
        }}
            headStyle={{ border: "none", height: "30px" }}
            bodyStyle={{
                width: props.nodeData.data.w,
                height: props.nodeData.data.h,
                overflow: "hidden",
                paddingTop: "0px",
            }}
            onMouseEnter={() => setHover(true)}
            onMouseLeave={() => setHover(false)}
            extra={
                <>
                    {hover == true && (
                        <Space className="nodrag nopan">
                            {dataViewStore.projectStore.viewInfo?.user_perm.can_update == true && props.canChangeBgColor && (
                                <Popover trigger="click" placement="top" content={
                                    <CompactPicker onChange={color => {
                                        updateBgColor(color.hex);
                                    }} />
                                }>
                                    <FormatPainterFilled />
                                </Popover>
                            )}

                            {(props.extraButtonList ?? []).map((btn, btnIndex) => (
                                <span key={btnIndex}>
                                    {btn}
                                </span>
                            ))}
                            <Popover trigger="click" placement="bottom" content={
                                <Button type="link" danger onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    removeNode();
                                }} disabled={dataViewStore.projectStore.viewInfo?.user_perm.can_remove == false}>删除</Button>
                            }>
                                <MoreOutlined />
                            </Popover>
                        </Space>
                    )}
                </>

            }>
            <NodeResizeControl minWidth={100} minHeight={40} style={{ zIndex: 4000 }}
                shouldResize={() => true} >
                <ResizeIcon />
            </NodeResizeControl>
            <Handle type="source" position={Position.Left} id="left" style={{ width: "6px", height: "6px", borderRadius: "3px", backgroundColor: "grey" }} />
            <Handle type="source" position={Position.Top} id="top" style={{ width: "6px", height: "6px", borderRadius: "3px", backgroundColor: "grey" }} />
            <Handle type="source" position={Position.Right} id="right" style={{ width: "6px", height: "6px", borderRadius: "3px", backgroundColor: "grey" }} />
            <Handle type="source" position={Position.Bottom} id="bottom" style={{ width: "6px", height: "6px", borderRadius: "3px", backgroundColor: "grey" }} />
            {props.children != undefined && props.children}
        </Card>
    );
};

export default observer(ProjectNodeWrap);