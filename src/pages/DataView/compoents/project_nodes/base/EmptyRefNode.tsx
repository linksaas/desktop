//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from 'react';
import type { NodeProps } from "reactflow";
import { convert_to_ref_node, NODE_TYPE_REF_BUG, NODE_TYPE_REF_REQUIREMENT, NODE_TYPE_REF_TASK, type NodeInfo } from '@/api/project_dataview';
import ProjectNodeWrap from '../ProjectNodeWrap';
import { useDataViewStores } from '@/pages/DataView/store';
import { observer } from 'mobx-react';
import { Button, Form, Input, Segmented } from 'antd';
import { request } from '@/utils/request';
import { create_requirement } from '@/api/project_requirement';
import { get_session } from '@/api/user';
import { BUG_LEVEL_MAJOR, BUG_PRIORITY_NORMAL, create as create_issue, ISSUE_TYPE_BUG, ISSUE_TYPE_TASK, TASK_PRIORITY_MIDDLE } from "@/api/project_issue";


const EmptyRefNode = (props: NodeProps<NodeInfo>) => {
    const dataViewStore = useDataViewStores();

    const [refType, setRefType] = useState<"req" | "task" | "bug">("task");
    const [title, setTitle] = useState("");

    const createRequireMent = async () => {
        const sessionId = await get_session();
        const res = await request(create_requirement({
            session_id: sessionId,
            project_id: dataViewStore.projectStore.projectId,
            base_info: {
                title: title,
                content: JSON.stringify({ type: "doc" }),
                tag_id_list: [],
            },
        }));
        await request(convert_to_ref_node({
            session_id: sessionId,
            project_id: dataViewStore.projectStore.projectId,
            view_id: dataViewStore.projectStore.dataViewId,
            node_id: props.data.node_id,
            target_node_type: NODE_TYPE_REF_REQUIREMENT,
            ref_target_id: res.requirement_id,
        }));
    };

    const createTask = async () => {
        const sessionId = await get_session();
        const res = await request(create_issue({
            session_id: sessionId,
            project_id: dataViewStore.projectStore.projectId,
            issue_type: ISSUE_TYPE_TASK,
            basic_info: {
                title: title,
                content: JSON.stringify({ type: "doc" }),
                tag_id_list: [],
            },
            extra_info: {
                ExtraTaskInfo: {
                    priority: TASK_PRIORITY_MIDDLE,
                },
            },
        }));
        await request(convert_to_ref_node({
            session_id: sessionId,
            project_id: dataViewStore.projectStore.projectId,
            view_id: dataViewStore.projectStore.dataViewId,
            node_id: props.data.node_id,
            target_node_type: NODE_TYPE_REF_TASK,
            ref_target_id: res.issue_id,
        }));
    };

    const createBug = async () => {
        const sessionId = await get_session();
        const res = await request(create_issue({
            session_id: sessionId,
            project_id: dataViewStore.projectStore.projectId,
            issue_type: ISSUE_TYPE_BUG,
            basic_info: {
                title: title,
                content: JSON.stringify({ type: "doc" }),
                tag_id_list: [],
            },
            extra_info: {
                ExtraBugInfo: {
                    software_version: "",
                    level: BUG_LEVEL_MAJOR,
                    priority: BUG_PRIORITY_NORMAL,
                },
            },
        }));
        await request(convert_to_ref_node({
            session_id: sessionId,
            project_id: dataViewStore.projectStore.projectId,
            view_id: dataViewStore.projectStore.dataViewId,
            node_id: props.data.node_id,
            target_node_type: NODE_TYPE_REF_BUG,
            ref_target_id: res.issue_id,
        }));
    };

    return (
        <ProjectNodeWrap title="空白引用" nodeData={props} canChangeBgColor={false} >
            <Form>
                <Form.Item label="类型">
                    <Segmented value={refType} onChange={value => setRefType(value as "req" | "task" | "bug")}
                        style={{ backgroundColor: "#ccc" }}
                        options={[
                            {
                                label: "项目需求",
                                value: "req",
                            },
                            {
                                label: "项目任务",
                                value: "task",
                            },
                            {
                                label: "项目缺陷",
                                value: "bug",
                            },
                        ]} />
                </Form.Item>
                <Form.Item label="标题">
                    <Input value={title} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setTitle(e.target.value.trim());
                    }} />
                </Form.Item>
                <Form.Item style={{ display: "flex", flexDirection: "row-reverse" }}>
                    <Button type="primary" disabled={title == ""} onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        if (refType == "req") {
                            createRequireMent();
                        } else if (refType == "task") {
                            createTask();
                        } else if (refType == "bug") {
                            createBug();
                        }
                    }}>创建</Button>
                </Form.Item>
            </Form>
        </ProjectNodeWrap>
    )
};

export default observer(EmptyRefNode);
