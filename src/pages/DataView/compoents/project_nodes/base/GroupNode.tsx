//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import type { NodeProps } from "reactflow";
import { update_node_data, type NodeInfo } from '@/api/project_dataview';
import ProjectNodeWrap from '../ProjectNodeWrap';
import { useDataViewStores } from '@/pages/DataView/store';
import { observer } from 'mobx-react';
import { Button, Input, Space } from 'antd';
import { CheckOutlined, CloseOutlined, EditOutlined } from '@ant-design/icons';
import { request } from '@/utils/request';
import { get_session } from '@/api/user';


const GroupNode = (props: NodeProps<NodeInfo>) => {
    const dataViewStore = useDataViewStores();

    const [inEdit, setInEdit] = useState(false);

    const [text, setText] = useState("");

    const updateNodeData = async () => {
        const sessionId = await get_session();
        await request(update_node_data({
            session_id: sessionId,
            project_id: dataViewStore.projectStore.projectId,
            view_id: dataViewStore.projectStore.dataViewId,
            node_id: props.data.node_id,
            node_data: {
                BaseGroupName: text,
            },
        }));
        setInEdit(false);
    };

    useEffect(() => {
        setText(props.data.node_data.BaseGroupName ?? "");
    }, [props.data.node_data.BaseGroupName]);

    return (
        <ProjectNodeWrap title={
            <div>
                {inEdit == false && (
                    <Space>
                        <span style={{ fontSize: "16px" }}>{props.data.node_data.BaseGroupName ?? ""}</span>
                        <Button type="link" icon={<EditOutlined />} onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            setInEdit(true);
                        }} />
                    </Space>
                )}
                {inEdit == true && (
                    <Input value={text} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setText(e.target.value);
                    }} addonAfter={
                        <Space>
                            <Button type="text" style={{ padding: "0px 0px", height: "20px", minWidth: "0px" }} icon={<CloseOutlined style={{ color: "red" }} />}
                                onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    setInEdit(false);
                                    setText(props.data.node_data.BaseGroupName ?? "");
                                }} />
                            <Button type="text" style={{ padding: "0px 0px", height: "20px", minWidth: "0px" }} icon={<CheckOutlined style={{ color: "green" }} />}
                                onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    updateNodeData();
                                }} disabled={text == ""} />
                        </Space>
                    } />
                )}
            </div>
        } nodeData={props} canChangeBgColor />
    )
};

export default observer(GroupNode);
