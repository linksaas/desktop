//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import type { NodeProps } from "reactflow";
import { Button, Form, Input, Modal } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import { useDataViewStores } from '@/pages/DataView/store';
import { update_node_data, type NodeInfo } from '@/api/project_dataview';
import ProjectNodeWrap from '../ProjectNodeWrap';
import { get_session } from '@/api/user';
import { request } from '@/utils/request';


const IframeNode = (props: NodeProps<NodeInfo>) => {
    const dataViewStore = useDataViewStores();

    const [url, setUrl] = useState(props.data.node_data.BaseIframeUrl ?? "");
    const [showEditModal, setShowEditModal] = useState(false);

    const updateNodeData = async () => {
        const sessionId = await get_session();
        await request(update_node_data({
            session_id: sessionId,
            project_id: dataViewStore.projectStore.projectId,
            view_id: dataViewStore.projectStore.dataViewId,
            node_id: props.data.node_id,
            node_data: {
                BaseIframeUrl: url,
            },
        }));
        setShowEditModal(false);
    };

    useEffect(() => {
        setUrl(props.data.node_data.BaseIframeUrl ?? "");
    }, [props.data.node_data.BaseIframeUrl]);

    return (
        <ProjectNodeWrap title={`网址:${props.data.node_data.BaseIframeUrl ?? ""}`} canChangeBgColor={false} nodeData={props}
            extraButtonList={[
                <Button type="text" icon={<EditOutlined />} onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    setShowEditModal(true);
                }} />
            ]}>
            {(props.data.node_data.BaseIframeUrl ?? "") != "" && (
                <iframe src={props.data.node_data.BaseIframeUrl ?? ""} style={{ width: '100%', height: props.data.h - 40 }} />
            )}
            {showEditModal && (
                <Modal open title="设置网址" mask={false}
                    okText="设置" okButtonProps={{ disabled: !url.startsWith("https://") }}
                    onCancel={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setShowEditModal(false);
                        setUrl(props.data.node_data.BaseIframeUrl ?? "");
                    }}
                    onOk={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        updateNodeData();
                    }}>
                    <Form>
                        <Form.Item label="网址" help="必须以https://开始">
                            <Input value={url} onChange={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setUrl(e.target.value.trim());
                            }} status={url.startsWith("https://") ? undefined : "error"} />
                        </Form.Item>
                    </Form>
                </Modal>
            )}
        </ProjectNodeWrap>
    );
};

export default observer(IframeNode);