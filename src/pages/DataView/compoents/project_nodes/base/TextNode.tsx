//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import type { NodeProps } from "reactflow";
import { observer } from 'mobx-react';
import { Button, Input } from 'antd';
import { CheckOutlined, CloseOutlined, EditOutlined } from '@ant-design/icons';
import { useDataViewStores } from '@/pages/DataView/store';
import ProjectNodeWrap from '../ProjectNodeWrap';
import { update_node_data, type NodeInfo } from '@/api/project_dataview';
import { get_session } from '@/api/user';
import { request } from '@/utils/request';

const TextNode = (props: NodeProps<NodeInfo>) => {
    const dataViewStore = useDataViewStores();

    const [inEdit, setInEdit] = useState(false);

    const [text, setText] = useState("");

    const updateNodeData = async () => {
        const sessionId = await get_session();
        await request(update_node_data({
            session_id: sessionId,
            project_id: dataViewStore.projectStore.projectId,
            view_id: dataViewStore.projectStore.dataViewId,
            node_id: props.data.node_id,
            node_data: {
                BaseText: text,
            },
        }));
        setInEdit(false);
    };

    useEffect(() => {
        setText(props.data.node_data.BaseText ?? "");
    }, [props.data.node_data.BaseText]);

    return (
        <ProjectNodeWrap title="文本" nodeData={props} canChangeBgColor extraButtonList={inEdit ? [
            <Button type="text" style={{ padding: "0px 0px", minWidth: "0px" }} icon={<CloseOutlined style={{ color: "red" }} />}
                onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    setInEdit(false);
                    setText(props.data.node_data.BaseText ?? "");
                }} />,
            <Button type="text" style={{ padding: "0px 0px", minWidth: "0px" }} icon={<CheckOutlined style={{ color: "green" }} />}
                onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    updateNodeData();
                }} disabled={text == ""} />
        ] : [
            <Button type="link" icon={<EditOutlined />} onClick={e => {
                e.stopPropagation();
                e.preventDefault();
                setInEdit(true);
            }} />
        ]}>
            <div>
                {inEdit == false && (
                    <pre style={{ fontSize: "16px", whiteSpace: "pre-wrap", wordWrap: "break-word", height: props.data.h - 20, overflowY: "scroll" }} className='nodrag nopan nowheel'>
                        {props.data.node_data.BaseText ?? ""}
                    </pre>
                )}
                {inEdit == true && (
                    <Input.TextArea value={text} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setText(e.target.value);
                    }} style={{ marginTop: "10px" }} autoSize={{ maxRows: 5 }} />
                )}
            </div>
        </ProjectNodeWrap>
    );
};

export default observer(TextNode);