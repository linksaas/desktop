//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from "react";
import { BaseEdge, EdgeLabelRenderer, getBezierPath } from 'reactflow';
import type { EdgeProps } from 'reactflow';
import { Button, Input, Modal, Popover, Space } from "antd";
import { observer } from 'mobx-react';
import { EditOutlined, MoreOutlined } from "@ant-design/icons";
import { request } from "@/utils/request";
import { EdgeInfo, EdgeInfoKey, remove_edge, update_edge } from "@/api/project_dataview";
import { useDataViewStores } from "../../store";
import { get_session } from "@/api/user";

interface EditLabelModalProps {
    label: string;
    projectId: string;
    viewId: string;
    edgeKey: EdgeInfoKey;
    onClose: () => void;
}

const EditLabelModal = (props: EditLabelModalProps) => {
    const [label, setLabel] = useState(props.label);

    const updateLabel = async () => {
        const sessionId = await get_session();
        await request(update_edge({
            session_id: sessionId,
            project_id: props.projectId,
            view_id: props.viewId,
            edge: {
                edge_key: props.edgeKey,
                label: label,
            },
        }));
        props.onClose();
    };

    return (
        <Modal open title="修改连接标签" okText="修改" okButtonProps={{ disabled: label == props.label }} mask={false}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onClose();
            }}
            onOk={e => {
                e.stopPropagation();
                e.preventDefault();
                updateLabel();
            }}>
            <Input value={label} onChange={e => {
                e.stopPropagation();
                e.preventDefault();
                setLabel(e.target.value.trim());
            }} />
        </Modal>
    );
};

const LabelEdge = (props: EdgeProps<EdgeInfo>) => {
    const dataViewStore = useDataViewStores();

    const [showEditModal, setShowEditModal] = useState(false);

    const [edgePath, labelX, labelY] = getBezierPath({
        sourceX: props.sourceX,
        sourceY: props.sourceY,
        sourcePosition: props.sourcePosition,
        targetX: props.targetX,
        targetY: props.targetY,
        targetPosition: props.targetPosition,
    });

    const removeEdge = async () => {
        if (props.data == undefined) {
            return;
        }
        const sessionId = await get_session();
        await request(remove_edge({
            session_id: sessionId,
            project_id: dataViewStore.projectStore.projectId,
            view_id: dataViewStore.projectStore.dataViewId,
            edge_key: props.data.edge_key,
        }));
    };

    return (
        <>
            <BaseEdge path={edgePath} markerEnd={props.markerEnd} style={{ strokeWidth: 2, stroke: "orange" }} />
            <EdgeLabelRenderer>
                <div
                    style={{
                        position: 'absolute',
                        transform: `translate(-50%, -50%) translate(${labelX}px,${labelY}px)`,
                        fontSize: 14,
                        pointerEvents: 'all',
                        zIndex: 3000,
                    }}
                >
                    <Space>
                        <span>{props.data?.label ?? ""}</span>
                        {(dataViewStore.projectStore.viewInfo?.user_perm.can_update ?? false) == true && (
                            <a onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setShowEditModal(true);
                            }}><EditOutlined /></a>
                        )}
                        {(dataViewStore.projectStore.viewInfo?.user_perm.can_remove ?? false) == true && (
                            <Popover placement="bottom" trigger="click" content={
                                <Button type="link" danger onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    removeEdge();
                                }}>删除</Button>
                            }>
                                <MoreOutlined />
                            </Popover>
                        )}
                    </Space>
                </div>
            </EdgeLabelRenderer>
            {showEditModal == true && props.data !== undefined && (
                <EditLabelModal label={props.data.label} projectId={dataViewStore.projectStore.projectId} viewId={dataViewStore.projectStore.dataViewId}
                    edgeKey={props.data.edge_key} onClose={() => setShowEditModal(false)} />
            )}
        </>
    );
};

export default observer(LabelEdge);