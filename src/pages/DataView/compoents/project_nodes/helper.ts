//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { NODE_TYPE, NODE_TYPE_BASE_EMPTY_REF, NODE_TYPE_BASE_GROUP, NODE_TYPE_BASE_IFRAME, NODE_TYPE_BASE_TEXT, NODE_TYPE_REF_BUG, NODE_TYPE_REF_ENTRY, NODE_TYPE_REF_IDEA, NODE_TYPE_REF_REQUIREMENT, NODE_TYPE_REF_SERVER, NODE_TYPE_REF_TASK, NODE_TYPE_REF_TESTCASE } from "@/api/project_dataview";
import RefBugNode from "./refnode/RefBugNode";
import RefEntryNode from "./refnode/RefEntryNode";
import RefIdeaNode from "./refnode/RefIdeaNode";
import RefServerNode from "./refnode/RefServerNode";
import RefTaskNode from "./refnode/RefTaskNode";
import RefTestCaseNode from "./refnode/RefTestCaseNode";
import RefRequireMentNode from "./refnode/RefRequireMentNode";
import GroupNode from "./base/GroupNode";
import TextNode from "./base/TextNode";
import IframeNode from "./base/IframeNode";
import EmptyRefNode from "./base/EmptyRefNode";



export const allNodeTyps = {
    RefRequireMentNode: RefRequireMentNode,
    RefBugNode: RefBugNode,
    RefIdeaNode: RefIdeaNode,
    RefTaskNode: RefTaskNode,
    RefEntryNode: RefEntryNode,
    RefServerNode: RefServerNode,
    RefTestCaseNode: RefTestCaseNode,
    GroupNode: GroupNode,
    TextNode: TextNode,
    IframeNode: IframeNode,
    EmptyRefNode: EmptyRefNode,
};

export function getNodeTypeStr(nodeType: NODE_TYPE): string {
    if (nodeType == NODE_TYPE_REF_IDEA) {
        return "RefIdeaNode";
    } else if (nodeType == NODE_TYPE_REF_REQUIREMENT) {
        return "RefRequireMentNode";
    } else if (nodeType == NODE_TYPE_REF_TASK) {
        return "RefTaskNode";
    } else if (nodeType == NODE_TYPE_REF_BUG) {
        return "RefBugNode";
    } else if (nodeType == NODE_TYPE_REF_TESTCASE) {
        return "RefTestCaseNode";
    } else if (nodeType == NODE_TYPE_REF_SERVER) {
        return "RefServerNode";
    } else if (nodeType == NODE_TYPE_REF_ENTRY) {
        return "RefEntryNode";
    } else if (nodeType == NODE_TYPE_BASE_GROUP) {
        return "GroupNode";
    } else if (nodeType == NODE_TYPE_BASE_TEXT) {
        return "TextNode";
    } else if (nodeType == NODE_TYPE_BASE_IFRAME) {
        return "IframeNode";
    } else if (nodeType == NODE_TYPE_BASE_EMPTY_REF) {
        return "EmptyRefNode";
    }
    return "";
}