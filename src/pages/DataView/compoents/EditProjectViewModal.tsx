//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from "react";
import type { ViewInfo } from "@/api/project_dataview";
import { create_view, update_view } from "@/api/project_dataview";
import { observer } from 'mobx-react';
import { Form, Input, message, Modal, Popover } from "antd";
import { useStores } from "@/hooks";
import { request } from "@/utils/request";
import { Compact as CompactPicker } from '@uiw/react-color';

export interface EditProjectViewModalProps {
    viewInfo?: ViewInfo;
    onCancel: () => void;
    onOk: () => void;
}

const EditProjectViewModal = (props: EditProjectViewModalProps) => {
    const userStore = useStores('userStore');
    const projectStore = useStores('projectStore');

    const [title, setTitle] = useState(props.viewInfo?.title ?? "");
    const [bgColor, setBgColor] = useState(props.viewInfo?.bg_color ?? "#8bc34a");
    const [desc, setDesc] = useState(props.viewInfo?.desc ?? "");

    const createView = async () => {
        await request(create_view({
            session_id: userStore.sessionId,
            project_id: projectStore.curProjectId,
            title: title,
            bg_color: bgColor,
            desc: desc,
        }));
        message.info("增加成功");
        props.onOk();
    };

    const updateView = async () => {
        if (props.viewInfo == undefined) {
            return;
        }
        await request(update_view({
            session_id: userStore.sessionId,
            project_id: props.viewInfo.project_id,
            view_id: props.viewInfo.view_id,
            title: title,
            bg_color: bgColor,
            desc: desc,
        }));
        message.info("修改成功");
        props.onOk();
    };

    return (
        <Modal open title={`${props.viewInfo == undefined ? "增加" : "修改"}项目视图`} mask={false}
            okText={props.viewInfo == undefined ? "增加" : "修改"} okButtonProps={{ disabled: title == "" }}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onCancel();
            }}
            onOk={e => {
                e.stopPropagation();
                e.preventDefault();
                if (props.viewInfo == undefined) {
                    createView();
                } else {
                    updateView();
                }
            }}>
            <Form labelCol={{ span: 3 }}>
                <Form.Item label="名称">
                    <Input value={title} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setTitle(e.target.value.trim());
                    }} />
                </Form.Item>
                <Form.Item label="背景色">
                    <Popover trigger="click" placement="bottom" content={
                        <CompactPicker color={bgColor}
                            onChange={value => {
                                setBgColor(value.hex);
                            }} />
                    }>
                        <div style={{ backgroundColor: bgColor, width: "20px", height: "20px", cursor: "pointer" }} />
                    </Popover>
                </Form.Item>
                <Form.Item label="备注信息">
                    <Input.TextArea value={desc} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setDesc(e.target.value);
                    }} autoSize={{ minRows: 3, maxRows: 3 }} />
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default observer(EditProjectViewModal);