//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from "react";
import type { UserDataViewInfo } from "@/api/user_dataview";
import { add_view, update_view } from "@/api/user_dataview";
import { Form, Input, message, Modal, Popover } from "antd";
import { uniqId } from "@/utils/utils";
import { Compact as CompactPicker } from '@uiw/react-color';

export interface EditLocalViewModalProps {
    viewInfo?: UserDataViewInfo;
    onCancel: () => void;
    onOk: () => void;
}

const EditLocalViewModal = (props: EditLocalViewModalProps) => {

    const [name, setName] = useState(props.viewInfo?.name ?? "");
    const [desc, setDesc] = useState(props.viewInfo?.desc ?? "");
    const [bgColor, setBgColor] = useState(props.viewInfo?.bg_color ?? "#8bc34a");

    const addView = async () => {
        await add_view({
            id: uniqId(),
            name: name,
            desc: desc,
            bg_color: bgColor,
        });
        message.info("增加成功");
        props.onOk();
    };

    const updateView = async () => {
        await update_view({
            id: props.viewInfo?.id ?? "",
            name: name,
            desc: desc,
            bg_color: bgColor,
        });
        message.info("修改成功");
        props.onOk();
    };

    return (
        <Modal open title={`${props.viewInfo == undefined ? "增加" : "修改"}本地视图`} mask={false}
            okText={props.viewInfo == undefined ? "增加" : "修改"} okButtonProps={{ disabled: name == "" }}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onCancel();
            }}
            onOk={e => {
                e.stopPropagation();
                e.preventDefault();
                if (props.viewInfo == undefined) {
                    addView();
                } else {
                    updateView();
                }
            }}>
            <Form labelCol={{ span: 3 }}>
                <Form.Item label="名称">
                    <Input value={name} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setName(e.target.value.trim());
                    }} />
                </Form.Item>
                <Form.Item label="背景色">
                    <Popover trigger="click" placement="bottom" content={
                         <CompactPicker color={bgColor} onChange={value => {
                            setBgColor(value.hex);
                        }} />
                    }>
                        <div style={{ backgroundColor: bgColor, width: "20px", height: "20px", cursor: "pointer" }} />
                    </Popover>
                </Form.Item>
                <Form.Item label="备注">
                    <Input.TextArea autoSize={{ minRows: 3, maxRows: 3 }} value={desc} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setDesc(e.target.value);
                    }} />
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default EditLocalViewModal;