//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import type { UserDataSourceInfo } from "@/api/user_dataview";
import { list_source } from "@/api/user_dataview";
import { get_type_name, remove_source } from "@/api/user_dataview";
import { Button, message, Modal, Popover, Space, Table } from "antd";
import type { ColumnsType } from 'antd/lib/table';
import { MoreOutlined } from "@ant-design/icons";
import EditLocalSourceModal from "./compoents/EditLocalSourceModal";

export interface LocalDataSourceListProps {
    dataVersion: number;
}

const LocalDataSourceList = (props: LocalDataSourceListProps) => {
    const [updateInfo, setUpdateInfo] = useState<UserDataSourceInfo | null>(null);
    const [removeInfo, setRemoveInfo] = useState<UserDataSourceInfo | null>(null);
    const [dataSourceList, setDataSourceList] = useState<UserDataSourceInfo[]>([]);

    const loadDataSourceList = async () => {
        const res = await list_source();
        setDataSourceList(res);
    };

    const removeSource = async () => {
        if (removeInfo == null) {
            return;
        }
        await remove_source(removeInfo.id);
        message.info("删除成功");
        setRemoveInfo(null);
    };

    const columns: ColumnsType<UserDataSourceInfo> = [
        {
            title: "名称",
            dataIndex: "name",
        },
        {
            title: "类型",
            render: (_, row: UserDataSourceInfo) => get_type_name(row.source_type),
        },
        {
            title: "操作",
            width: 100,
            render: (_, row: UserDataSourceInfo) => (
                <Space>
                    <Button type="link" style={{ minWidth: 0, padding: "0px 0px" }} onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setUpdateInfo(row);
                    }}>修改</Button>
                    <Popover trigger="click" placement="bottom" content={
                        <Space direction="vertical">
                            <Button type="link" danger onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setRemoveInfo(row);
                            }}>删除</Button>
                        </Space>
                    }>
                        <MoreOutlined />
                    </Popover>
                </Space>
            ),
        }
    ];

    useEffect(() => {
        loadDataSourceList();
    }, [props.dataVersion]);

    return (
        <>
            <Table rowKey="id" dataSource={dataSourceList} columns={columns} pagination={false} />
            {updateInfo != null && (
                <EditLocalSourceModal sourceInfo={updateInfo} onCancel={() => setUpdateInfo(null)} onOk={() => {
                    setUpdateInfo(null);
                    loadDataSourceList();
                }} />
            )}
            {removeInfo != null && (
                <Modal open title="删除数据源" mask={false}
                    okText="删除" okButtonProps={{ danger: true }}
                    onCancel={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setRemoveInfo(null);
                    }}
                    onOk={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        removeSource();
                    }}>
                    是否删除&nbsp;{removeInfo.name}({get_type_name(removeInfo.source_type)})&nbsp;?
                </Modal>
            )}
        </>
    );
};

export default LocalDataSourceList;