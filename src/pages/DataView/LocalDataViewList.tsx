//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import type { UserDataViewInfo } from "@/api/user_dataview";
import { list_view, remove_view } from "@/api/user_dataview";
import { Button, Card, List, message, Modal, Popover, Space } from "antd";
import { EditOutlined, MoreOutlined } from "@ant-design/icons";
import EditLocalViewModal from "./compoents/EditLocalViewModal";
import { openDataView } from "@/utils/dataview";

export interface LocalDataViewListProps {
    dataVersion: number;
}

const LocalDataViewList = (props: LocalDataViewListProps) => {
    const [updateInfo, setUpdateInfo] = useState<UserDataViewInfo | null>(null);
    const [removeInfo, setRemoveInfo] = useState<UserDataViewInfo | null>(null);
    const [dataViewList, setDataViewList] = useState<UserDataViewInfo[]>([]);
    const [hoverViewId, setHoverViewId] = useState("");

    const loadDataViewList = async () => {
        const res = await list_view();
        setDataViewList(res);
    };

    const removeView = async () => {
        if (removeInfo == null) {
            return;
        }
        await remove_view(removeInfo.id);
        message.info("删除成功");
        setRemoveInfo(null);
        await loadDataViewList();
    };


    useEffect(() => {
        loadDataViewList();
    }, [props.dataVersion]);

    return (
        <>
            <List rowKey="id" dataSource={dataViewList} pagination={false} grid={{ gutter: 16 }}
                renderItem={item => (
                    <List.Item>
                        <Card title={
                            <a
                                onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    openDataView(item.id, item.name, "");
                                }}>{item.name}</a>
                        }
                            style={{ backgroundColor: item.bg_color, width: "200px", boxShadow: "6px 6px 5px 0px #777", borderRadius: "10px" }} headStyle={{ border: "none", fontSize: "20px", fontWeight: 600, textOverflow: "ellipsis", overflow: "hidden", whiteSpace: "nowrap" }}
                            bodyStyle={{ height: "60px", padding: "0px 10px" }}
                            onMouseEnter={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setHoverViewId(item.id);
                            }}
                            onMouseLeave={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setHoverViewId("");
                            }}
                            extra={
                                <Space>
                                    {hoverViewId == item.id && (
                                        <Button type="text" icon={<EditOutlined />} onClick={e => {
                                            e.stopPropagation();
                                            e.preventDefault();
                                            setUpdateInfo(item);
                                        }} />
                                    )}
                                    <Popover placement="bottom" trigger="click" content={
                                        <div>
                                            <Button type="link" danger onClick={e => {
                                                e.stopPropagation();
                                                e.preventDefault();
                                                setRemoveInfo(item);
                                            }}>删除</Button>
                                        </div>
                                    }>
                                        <MoreOutlined />
                                    </Popover>
                                </Space>
                            }>
                            <div onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                openDataView(item.id, item.name, "");
                            }} style={{ cursor: "pointer" }}>
                                <pre style={{ whiteSpace: "pre-wrap", wordWrap: "break-word", height: "50px", overflowY: "scroll" }}>{item.desc}</pre>
                            </div>
                        </Card>
                    </List.Item>
                )} />
            {updateInfo != null && (
                <EditLocalViewModal viewInfo={updateInfo} onCancel={() => setUpdateInfo(null)} onOk={() => {
                    setUpdateInfo(null);
                    loadDataViewList();
                }} />
            )}
            {removeInfo != null && (
                <Modal open title="删除本地视图" mask={false}
                    okText="删除" okButtonProps={{ danger: true }}
                    onCancel={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setRemoveInfo(null);
                    }}
                    onOk={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        removeView();
                    }}>
                    是否删除本地视图&nbsp;{removeInfo.name}&nbsp;?
                </Modal>
            )}
        </>
    );
};

export default LocalDataViewList;