//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect } from "react";
import { ConfigProvider, message } from 'antd';
import zhCN from 'antd/lib/locale/zh_CN';
import { createRoot } from 'react-dom/client';
import 'moment/dist/locale/zh-cn';
import 'remirror/styles/all.css';
import 'reactflow/dist/base.css';
import '@/components/Editor/editor.less';
import '@/styles/global.less';
import { BrowserRouter, useLocation } from "react-router-dom";
import { Provider } from 'mobx-react';
import stores, { useDataViewStores } from './store/index';
import LocalDataViewBoard from "./LocalDataViewBoard";
import ProjectDataViewBoard from "./ProjectDataViewBoard";
import { get_user_id } from "@/api/user";
import { appWindow } from "@tauri-apps/api/window";
import { report_error } from "@/api/client_cfg";

const DataViewBoard = () => {
    const dataViewStore = useDataViewStores();

    const location = useLocation();
    const urlParams = new URLSearchParams(location.search);
    const id = urlParams.get("id") ?? "";
    const projectId = urlParams.get("projectId") ?? "";

    const initProjectStore = async () => {
        const myUserId = await get_user_id();
        dataViewStore.projectStore.dataViewId = id;
        dataViewStore.projectStore.projectId = projectId;
        dataViewStore.projectStore.myUserId = myUserId;
        await dataViewStore.projectStore.loadProject();
        await dataViewStore.projectStore.loadMemberList();
        await dataViewStore.projectStore.loadTagList();
        await dataViewStore.projectStore.loadViewInfo();
        await dataViewStore.projectStore.loadNodeList();
        await dataViewStore.projectStore.loadEdgeList();
    };

    useEffect(() => {
        if (projectId == "") {
            dataViewStore.localStore.dataViewId = id;
            dataViewStore.localStore.loadNodeList();
        } else {
            initProjectStore();
        }
        appWindow.setAlwaysOnTop(true);
        setTimeout(() => {
            appWindow.setAlwaysOnTop(false);
        }, 500);
    }, [id, projectId]);

    return (
        <>
            {projectId == "" && (<LocalDataViewBoard />)}
            {projectId != "" && (<ProjectDataViewBoard />)}
        </>
    );
};

const App = () => {
    return (
        <Provider {...stores}>
            <ConfigProvider locale={zhCN}>
                <BrowserRouter>
                    <DataViewBoard />
                </BrowserRouter>
            </ConfigProvider>
        </Provider>
    );
}

const root = createRoot(document.getElementById('root')!);
root.render(<App />);

window.addEventListener('unhandledrejection', function (event) {
    // 防止默认处理（例如将错误输出到控制台）
    event.preventDefault();
    if (`${event.reason}`.includes("error trying to connect")) {
      return;
    }
    message.error(event?.reason);
    // console.log(event);
    try {
      report_error({
        err_data: `${event?.reason}`,
      });
    } catch (e) {
      console.log(e);
    }
  });