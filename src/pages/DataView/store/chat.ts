//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { makeAutoObservable, runInAction } from 'mobx';
import type { ChatMsgInfo } from '@/api/dataview_chat';

export class ChatStore {
    constructor() {
        makeAutoObservable(this);
    }

    private _expand = false;
    private _msgList: ChatMsgInfo[] = [];
    private _lastMsgId = "";
    private _unReadCount = 0;
    private _initLoad = false;

    get expand() {
        return this._expand;
    }

    set expand(val: boolean) {
        runInAction(() => {
            this._expand = val;
        });
    }

    get msgList() {
        return this._msgList;
    }

    set msgList(val: ChatMsgInfo[]) {
        runInAction(() => {
            this._msgList = val;
        });
    }

    get lastMsgId() {
        return this._lastMsgId;
    }

    set lastMsgId(val: string) {
        runInAction(() => {
            this._lastMsgId = val;
        });
    }

    get unReadCount() {
        return this._unReadCount;
    }

    set unReadCount(val: number) {
        runInAction(() => {
            this._unReadCount = val;
        });
    }

    get initLoad() {
        return this._initLoad;
    }

    set initLoad(val: boolean) {
        runInAction(() => {
            this._initLoad = val;
        });
    }
}