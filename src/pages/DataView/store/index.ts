//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from "react";
import { LocalDataViewStore } from "./local_dataview";
import { ProjectDataViewStore } from "./project_dataview";
import { ChatStore } from "./chat";

const stores = React.createContext({
    localStore: new LocalDataViewStore(),
    projectStore: new ProjectDataViewStore(),
    chatStore: new ChatStore(),
});

export const useDataViewStores = () => React.useContext(stores);

export default stores;