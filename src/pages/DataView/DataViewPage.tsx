//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from "react";
import { Button, Space, Tabs } from "antd";
import s from "./DataViewPage.module.less";
import LocalDataSourceList from "./LocalDataSourceList";
import LocalDataViewList from "./LocalDataViewList";
import EditLocalViewModal from "./compoents/EditLocalViewModal";
import EditLocalSourceModal from "./compoents/EditLocalSourceModal";
import { PlusOutlined } from "@ant-design/icons";

const DataViewPage = () => {

    const [activeKey, setActiveKey] = useState<"localView" | "localSource">("localView");
    const [showAddLocalViewModal, setShowAddLocalViewModal] = useState(false);
    const [showAddLocalSourceModal, setShowAddLocalSourceModal] = useState(false);
    const [localViewVersion, setLocalViewVersion] = useState(0);
    const [localSourceVersion, setLocalSourceVersion] = useState(0);

    return (
        <div className={s.tabs_wrap}>
            <Tabs activeKey={activeKey}
                type='card'
                onChange={key => {
                    setActiveKey(key as "localView" | "localSource");
                }}
                tabBarExtraContent={
                    <Space style={{ marginRight: "20px" }}>
                        {activeKey == "localView" && (
                            <Button type="primary" icon={<PlusOutlined />} onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setShowAddLocalViewModal(true);
                            }}>创建本地视图</Button>
                        )}
                        {activeKey == "localSource" && (
                            <Button type="primary" icon={<PlusOutlined />} onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setShowAddLocalSourceModal(true);
                            }}>创建本地数据源</Button>
                        )}
                    </Space>
                }>
                <Tabs.TabPane tab={<h2>本地视图</h2>} key="localView">
                    {activeKey == "localView" && (
                        <div className={s.content_wrap}>
                            <LocalDataViewList dataVersion={localViewVersion} />
                        </div>
                    )}
                </Tabs.TabPane>
                <Tabs.TabPane tab={<h2>本地数据源</h2>} key="localSource">
                    {activeKey == "localSource" && (
                        <div className={s.content_wrap}>
                            <LocalDataSourceList dataVersion={localSourceVersion} />
                        </div>
                    )}
                </Tabs.TabPane>
            </Tabs>
            {showAddLocalViewModal && (
                <EditLocalViewModal onCancel={() => setShowAddLocalViewModal(false)} onOk={() => {
                    setShowAddLocalViewModal(false);
                    setLocalViewVersion(localViewVersion + 1);
                }} />
            )}
            {showAddLocalSourceModal && (
                <EditLocalSourceModal onCancel={() => setShowAddLocalSourceModal(false)} onOk={() => {
                    setShowAddLocalSourceModal(false);
                    setLocalSourceVersion(localSourceVersion + 1);
                }} />
            )}
        </div>
    );
};

export default DataViewPage;