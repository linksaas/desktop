//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import type { TagInfo, WhiteUserInfo } from "@/api/roadmap_admin";
import { add_white_user, update_white_user, list_tag } from "@/api/roadmap_admin";
import { Form, Input, message, Modal, Select } from "antd";
import { get_admin_session } from "@/api/admin_auth";
import { request } from "@/utils/request";
import { calcCompUserName, getUserType, getSimpleUerName, USER_TYPE_INTERNAL, USER_TYPE_ATOM_GIT, USER_TYPE_GIT_CODE, USER_TYPE_GITEE } from "@/api/user";

export interface EditWhiteUserModalProps {
    whiteUser?: WhiteUserInfo;
    onCancel: () => void;
    onOk: () => void;
}

const EditWhiteUserModal = (props: EditWhiteUserModalProps) => {
    const [tagInfoList, setTagInfoList] = useState<TagInfo[]>([]);
    const [userType, setUserType] = useState(getUserType(props.whiteUser?.user_name ?? ""));
    const [simpleUserName, setSimpleUserName] = useState(getSimpleUerName(props.whiteUser?.user_name ?? ""));
    const [defaultTag, setDefaultTag] = useState(props.whiteUser?.default_tag ?? "");
    const [userDesc, setUserDesc] = useState(props.whiteUser?.user_desc ?? "");

    const loadTagInfoList = async () => {
        const sessionId = await get_admin_session();
        const res = await request(list_tag({
            session_id: sessionId,
        }));
        setTagInfoList(res.tag_list);
    };

    const addWhiteUser = async () => {
        try {
            const sessionId = await get_admin_session();
            await request(add_white_user({
                session_id: sessionId,
                target_user_name: calcCompUserName(userType, simpleUserName),
                default_tag: defaultTag,
                user_desc: userDesc,
            }));
            message.info("增加成功");
            props.onOk()
        } catch (e) {
            console.log(e);
            message.error("白名单用户已存在或缺乏权限");
        }
    };

    const updateWhiteUser = async () => {
        const sessionId = await get_admin_session();
        await request(update_white_user({
            session_id: sessionId,
            target_user_name: calcCompUserName(userType, simpleUserName),
            default_tag: defaultTag,
            user_desc: userDesc,
        }));
        message.info("修改成功");
        props.onOk();
    };

    useEffect(() => {
        loadTagInfoList();
    }, []);


    return (
        <Modal open title={props.whiteUser == undefined ? "增加白名单用户" : "修改白名单用户"} mask={false}
            okText={props.whiteUser == undefined ? "增加" : "修改"} okButtonProps={{ disabled: simpleUserName == "" || defaultTag == "" }}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onCancel();
            }}
            onOk={e => {
                e.stopPropagation();
                e.preventDefault();
                if (props.whiteUser == undefined) {
                    addWhiteUser();
                } else {
                    updateWhiteUser();
                }
            }}>
            <Form labelCol={{ span: 3 }}>
                {props.whiteUser == undefined && (
                    <>
                        <Form.Item label="用户类型">
                            <Select value={userType} onChange={value => setUserType(value)}>
                                <Select.Option value={USER_TYPE_INTERNAL}>内部用户</Select.Option>
                                <Select.Option value={USER_TYPE_ATOM_GIT}>AtomGit用户</Select.Option>
                                <Select.Option value={USER_TYPE_GIT_CODE}>GitCode用户</Select.Option>
                                <Select.Option value={USER_TYPE_GITEE}>Gitee用户</Select.Option>
                            </Select>
                        </Form.Item>
                        <Form.Item label="用户账号">
                            <Input value={simpleUserName} onChange={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setSimpleUserName(e.target.value.trim());
                            }} />
                        </Form.Item>
                    </>
                )}
                <Form.Item label="默认渠道">
                    <Select value={defaultTag} onChange={value => setDefaultTag(value)}>
                        {tagInfoList.map(tagInfo => (
                            <Select.Option key={tagInfo.tag} value={tagInfo.tag}>{tagInfo.name}</Select.Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item label="备注">
                    <Input value={userDesc} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setUserDesc(e.target.value);
                    }} />
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default EditWhiteUserModal;