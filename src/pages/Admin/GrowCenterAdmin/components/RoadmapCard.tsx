//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from "react";
import { Button, Card, Descriptions, Form, message, Modal, Popover, Select, Space, Switch, Tag } from "antd";
import { get_admin_session, type AdminPermInfo } from '@/api/admin_auth';
import type { RoadmapInfo } from "@/api/roadmap";
import { ROADMAP_OWNER_ADMIN } from "@/api/roadmap";
import type { TagInfo } from "@/api/roadmap_admin";
import { update_pub_state, remove as remove_roadmap, update_tag } from "@/api/roadmap_admin";
import { request } from "@/utils/request";
import { EditOutlined, MoreOutlined } from "@ant-design/icons";
import moment from "moment";
import EditRoadmapModal from "./EditRoadmapModal";
import { openRoadmapView } from "@/utils/roadmap";

interface UpdateTagModalProps {
    roadmap: RoadmapInfo;
    tagInfoList: TagInfo[];
    onCancel: () => void;
    onOk: () => void;
}

const UpdateTagModal = (props: UpdateTagModalProps) => {
    const [newTagList, setNewTagList] = useState(props.roadmap.tag_list);

    const updateRoadmapTag = async () => {
        const sessionId = await get_admin_session();
        await request(update_tag({
            session_id: sessionId,
            roadmap_id: props.roadmap.roadmap_id,
            tag_list: newTagList,
        }));
        message.info("设置成功");
        props.onOk();
    };

    return (
        <Modal open title="设置渠道" mask={false}
            okText="设置"
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onCancel();
            }}
            onOk={e => {
                e.stopPropagation();
                e.stopPropagation();
                updateRoadmapTag();
            }}>
            <Form>
                <Form.Item label="渠道">
                    <Select mode="multiple" value={newTagList}
                        onChange={value => setNewTagList(value)}>
                        {props.tagInfoList.map(tagInfo => (
                            <Select.Option key={tagInfo.tag} value={tagInfo.tag}>{tagInfo.name}</Select.Option>
                        ))}
                    </Select>
                </Form.Item>
            </Form>
        </Modal>
    );
}

export interface RoadmapCardProps {
    roadmap: RoadmapInfo;
    tagInfoList: TagInfo[];
    permInfo: AdminPermInfo;
    onChange: () => void;
    onRemove: () => void;
}

const RoadmapCard = (props: RoadmapCardProps) => {
    const [showEditModal, setShowEditModal] = useState(false);
    const [showRemoveModal, setShowRemoveModal] = useState(false);
    const [showUpdateTagModal, setShowUpdateTagModal] = useState(false);

    const updatePubState = async (pubState: boolean) => {
        const sessionId = await get_admin_session();
        await request(update_pub_state({
            session_id: sessionId,
            roadmap_id: props.roadmap.roadmap_id,
            pub_state: pubState,
        }));
        message.info("设置成功");
        props.onChange();
    };

    const removeRoadmap = async () => {
        const sessionId = await get_admin_session();
        await request(remove_roadmap({
            session_id: sessionId,
            roadmap_id: props.roadmap.roadmap_id,
        }));
        message.info("删除成功");
        setShowRemoveModal(false);
        props.onRemove();
    };

    const openEditor = async () => {
        const sessionId = await get_admin_session();
        await openRoadmapView(props.roadmap.roadmap_id, props.roadmap.basic_info.title, sessionId, true, props.roadmap.user_perm.can_update_content);
    };

    return (
        <Card title={<a onClick={e => {
            e.stopPropagation();
            e.preventDefault();
            openEditor();
        }}>
            {props.roadmap.basic_info.title}
        </a>}
            style={{ width: "300px" }} headStyle={{ backgroundColor: "#eee" }}
            bodyStyle={{ padding: "0px 0px" }}
            extra={
                <Space>
                    <Form layout="inline">
                        <Form.Item label="发布状态">
                            <Switch checked={props.roadmap.pub_state} size="small" disabled={props.permInfo.roadmap_perm.update_pub_state == false}
                                onChange={checked => updatePubState(checked)} />
                        </Form.Item>
                    </Form>
                    <Popover placement="bottom" trigger="click" content={
                        <Space direction="vertical">
                            <Button type="link" disabled={props.permInfo.roadmap_perm.update_content == false}
                                onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    setShowEditModal(true);
                                }}>修改</Button>
                            <Button type="link" danger disabled={props.permInfo.roadmap_perm.remove == false}
                                onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    setShowRemoveModal(true)
                                }}>删除</Button>
                        </Space>
                    }>
                        <MoreOutlined />
                    </Popover>
                </Space>
            }>
            <Descriptions bordered column={1} labelStyle={{ width: "90px" }}>
                <Descriptions.Item label="类型">
                    {props.roadmap.owner_type == ROADMAP_OWNER_ADMIN ? "官方" : "用户"}
                </Descriptions.Item>
                <Descriptions.Item label="热度值">
                    {props.roadmap.hot_value}
                </Descriptions.Item>
                <Descriptions.Item label="创建人">
                    {props.roadmap.owner_display_name}
                </Descriptions.Item>
                <Descriptions.Item label="创建时间">
                    {moment(props.roadmap.create_time).format("YYYY-MM-DD HH:mm")}
                </Descriptions.Item>
                <Descriptions.Item label="更新时间">
                    {moment(props.roadmap.update_time).format("YYYY-MM-DD HH:mm")}
                </Descriptions.Item>
                <Descriptions.Item label="渠道" style={{ height: "100px" }}>
                    <Space style={{ flexWrap: "wrap" }}>
                        {props.tagInfoList.filter(tagInfo => props.roadmap.tag_list.includes(tagInfo.tag)).map(tagInfo => (
                            <Tag key={tagInfo.tag}>{tagInfo.name}</Tag>
                        ))}
                        {props.permInfo.roadmap_perm.update_tag && (
                            <Button type="link" icon={<EditOutlined />} onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setShowUpdateTagModal(true);
                            }} />
                        )}
                    </Space>
                </Descriptions.Item>
                <Descriptions.Item label="备注">
                    <pre style={{ whiteSpace: "pre-wrap", wordWrap: "break-word", height: "100px", overflowY: "scroll" }}>
                        {props.roadmap.basic_info.desc}
                    </pre>
                </Descriptions.Item>
            </Descriptions>
            {showEditModal == true && (
                <EditRoadmapModal roadmap={props.roadmap} onCancel={() => setShowEditModal(false)} onOk={() => {
                    props.onChange();
                    setShowEditModal(false);
                }} />
            )}
            {showRemoveModal == true && (
                <Modal open title="删除路线图"
                    okText="删除" okButtonProps={{ danger: true }}
                    onCancel={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setShowRemoveModal(false);
                    }}
                    onOk={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        removeRoadmap();
                    }}>
                    是否删除路线图&nbsp;{props.roadmap.basic_info.title}&nbsp;?
                </Modal>
            )}
            {showUpdateTagModal == true && (
                <UpdateTagModal roadmap={props.roadmap} tagInfoList={props.tagInfoList} onCancel={() => setShowUpdateTagModal(false)}
                    onOk={() => {
                        setShowUpdateTagModal(false);
                        props.onChange();
                    }} />
            )}
        </Card>
    );
};

export default RoadmapCard;