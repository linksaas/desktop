//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { get_admin_session, get_admin_perm } from '@/api/admin_auth';
import type { AdminPermInfo } from '@/api/admin_auth';
import type { TagInfo } from "@/api/roadmap_admin";
import { list_tag, remove_tag } from "@/api/roadmap_admin";
import { request } from "@/utils/request";
import { Button, Card, message, Modal, Popover, Space, Table } from "antd";
import EditTagModal from "./components/EditTagModal";
import type { ColumnsType } from 'antd/lib/table';
import { MoreOutlined } from "@ant-design/icons";


const RoadmapTagAdminList = () => {
    const [permInfo, setPermInfo] = useState<AdminPermInfo | null>(null);

    const [tagList, setTagList] = useState<TagInfo[]>([]);
    const [showAddModal, setShowAddModal] = useState(false);
    const [modifyTagInfo, setModifyTagInfo] = useState<TagInfo | null>(null);
    const [removeTagInfo, setRemoveTagInfo] = useState<TagInfo | null>(null);

    const loadTagList = async () => {
        const sessionId = await get_admin_session();
        const res = await request(list_tag({
            session_id: sessionId,
        }));

        setTagList(res.tag_list);
    };

    const removeTag = async () => {
        if(removeTagInfo == null){
            return;
        }
        const sessionId = await get_admin_session();
        await request(remove_tag({
            session_id:sessionId,
            tag:removeTagInfo.tag,
        }));
        await loadTagList();
        setRemoveTagInfo(null);
        message.info("删除成功");
    };


    const columns: ColumnsType<TagInfo> = [
        {
            title: "渠道ID",
            dataIndex: "tag",
        },
        {
            title: "渠道名称",
            dataIndex: "name",
        },
        {
            title: "操作",
            width: 100,
            render: (_, row: TagInfo) => (
                <Space>
                    <Button type="link" style={{ minWidth: 0, padding: "0px 0px" }} disabled={!(permInfo?.roadmap_perm.modify_tag ?? false)}
                        onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            setModifyTagInfo(row);
                        }}>修改</Button>
                    <Popover placement="bottom" trigger="click" content={
                        <Space direction="vertical">
                            <Button type="link" danger disabled={!(permInfo?.roadmap_perm.remove_tag ?? false)}
                                onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    setRemoveTagInfo(row);
                                }}>删除</Button>
                        </Space>
                    }>
                        <MoreOutlined />
                    </Popover>
                </Space>
            ),
        }
    ];

    useEffect(() => {
        loadTagList();
        get_admin_perm().then(res => setPermInfo(res));
    }, []);


    return (
        <Card title="渠道管理" bodyStyle={{ height: "calc(100vh - 90px)", overflowY: "scroll" }}
            extra={
                <Button type="primary" disabled={!(permInfo?.roadmap_perm.add_tag ?? false)}
                    onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setShowAddModal(true);
                    }}>增加</Button>
            }>
            <Table rowKey="tag" dataSource={tagList} columns={columns} pagination={false} />
            {showAddModal == true && (
                <EditTagModal onCancel={() => setShowAddModal(false)} onOk={() => {
                    setShowAddModal(false);
                    loadTagList();
                }} />
            )}
            {modifyTagInfo != null && (
                <EditTagModal tagInfo={modifyTagInfo} onCancel={() => setModifyTagInfo(null)}
                    onOk={() => {
                        setModifyTagInfo(null);
                        loadTagList();
                    }} />
            )}
            {removeTagInfo != null && (
                <Modal open title="删除渠道" mask={false}
                okText="删除" okButtonProps={{danger:true}}
                onCancel={e=>{
                    e.stopPropagation();
                    e.preventDefault();
                    setRemoveTagInfo(null);
                }}
                onOk={e=>{
                    e.stopPropagation();
                    e.preventDefault();
                    removeTag();
                }}>
                    是否删除渠道&nbsp;{removeTagInfo.tag}:{removeTagInfo.name}&nbsp;?
                </Modal>
            )}
        </Card>
    );
};

export default RoadmapTagAdminList;