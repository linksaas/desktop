//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { Button, Card, List, message, Modal, Popover, Space } from "antd";
import React, { useEffect, useState } from "react";
import type { FeedBackInfo } from "@/api/feedback";
import type { AdminPermInfo } from '@/api/admin_auth';
import { get_admin_session, get_admin_perm } from '@/api/admin_auth';
import { request } from "@/utils/request";
import { list as list_feedback, remove as remove_feedback } from "@/api/feedback_admin";
import UserPhoto from "@/components/Portrait/UserPhoto";
import moment from "moment";
import { MoreOutlined } from "@ant-design/icons";

const PAGE_SIZE = 10;

const FeedBackList = () => {

    const [permInfo, setPermInfo] = useState<AdminPermInfo | null>(null);

    const [feedBackList, setFeedBackList] = useState<FeedBackInfo[]>([]);
    const [totalCount, setTotalCount] = useState(0);
    const [curPage, setCurPage] = useState(0);

    const [removeFeedBackInfo, setRemoveFeedBackInfo] = useState<FeedBackInfo | null>(null);

    const loadFeedBackList = async () => {
        const sessionId = await get_admin_session();
        const res = await request(list_feedback({
            admin_session_id: sessionId,
            offset: PAGE_SIZE * curPage,
            limit: PAGE_SIZE,
        }));
        setFeedBackList(res.info_list);
        setTotalCount(res.total_count);
    };

    const removeFeedBack = async () => {
        if (removeFeedBackInfo == null) {
            return;
        }
        const sessionId = await get_admin_session();
        await request(remove_feedback({
            admin_session_id: sessionId,
            feed_back_id: removeFeedBackInfo.feed_back_id,
        }));
        message.info("删除成功");
        setRemoveFeedBackInfo(null);
        await loadFeedBackList();
    };

    useEffect(() => {
        get_admin_perm().then(res => setPermInfo(res));
    }, []);

    useEffect(() => {
        loadFeedBackList();
    }, [curPage]);

    return (
        <Card title="反馈信息列表" bodyStyle={{ height: "calc(100vh - 85px)", overflowY: "scroll" }}>
            <List rowKey="feed_back_id" dataSource={feedBackList}
                pagination={{ pageSize: PAGE_SIZE, current: curPage + 1, total: totalCount, onChange: page => setCurPage(page - 1), hideOnSinglePage: true, showSizeChanger: false }}
                renderItem={item => (
                    <List.Item>
                        <Card title={
                            <Space>
                                {item.user_id != "" && (
                                    <>
                                        <UserPhoto logoUri={item.user_logo_uri} style={{ width: "17px", borderRadius: "10px" }} />
                                        <span>{item.user_display_name}</span>
                                    </>
                                )}
                                {moment(item.time_stamp).format("YYYY-MM-DD HH:mm")}
                            </Space>
                        } style={{ width: "100%" }} headStyle={{ backgroundColor: "#eee" }}
                            extra={
                                <Space>
                                    <Popover placement="bottom" trigger="click" content={
                                        <Space direction="vertical">
                                            <Button type="link" danger disabled={!(permInfo?.feed_back_perm.remove ?? false)} onClick={e => {
                                                e.stopPropagation();
                                                e.preventDefault();
                                                setRemoveFeedBackInfo(item);
                                            }}>删除</Button>
                                        </Space>
                                    }>
                                        <MoreOutlined />
                                    </Popover>
                                </Space>
                            }>
                            <pre style={{ whiteSpace: "pre-wrap", wordWrap: "break-word" }}>{item.content}</pre>
                        </Card>
                    </List.Item>
                )} />
            {removeFeedBackInfo != null && (
                <Modal open title="删除反馈意见"
                    okText="删除" okButtonProps={{ danger: true }}
                    onCancel={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setRemoveFeedBackInfo(null);
                    }}
                    onOk={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        removeFeedBack();
                    }}>
                    是否删除当前反馈意见?
                </Modal>
            )}
        </Card>
    );
};

export default FeedBackList;