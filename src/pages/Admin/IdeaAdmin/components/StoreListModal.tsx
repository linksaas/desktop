//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { Button, Card, List, Modal } from "antd";
import type { IdeaStoreCate, IdeaStore } from "@/api/idea_store";
import { list_store_cate, list_store } from "@/api/idea_store";
import { request } from "@/utils/request";
import { get_conn_server_addr } from "@/api/main";


export interface StoreListModalProps {
    disableStoreId: string;
    onCancel: () => void;
    onOk: (storeId: string) => void;
}

const StoreListModal = (props: StoreListModalProps) => {
    const [storeCateList, setStoreCateList] = useState([] as IdeaStoreCate[]);
    const [storeList, setStoreList] = useState([] as IdeaStore[]);

    const loadStoreCateList = async () => {
        const addr = await get_conn_server_addr();
        const res = await request(list_store_cate(addr, {}));
        setStoreCateList(res.cate_list);
    };

    const loadStoreList = async () => {
        const addr = await get_conn_server_addr();
        const res = await request(list_store(addr, {
            filter_by_store_cate_id: false,
            store_cate_id: "",
        }));
        setStoreList(res.store_list);
    };


    useEffect(() => {
        loadStoreCateList();
        loadStoreList();
    }, []);

    return (
        <Modal open title="选择知识库"
            bodyStyle={{ maxHeight: "calc(100vh - 300px)", overflowY: "scroll" }}
            footer={null}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onCancel();
            }}>
            {storeCateList.map(cateItem => (
                <Card key={cateItem.store_cate_id} title={cateItem.name} bordered={false}>
                    <List rowKey="idea_store_id" dataSource={storeList.filter(storeItem => storeItem.store_cate_id == cateItem.store_cate_id)}
                        grid={{ gutter: 16 }}
                        renderItem={storeItem => (
                            <List.Item>
                                <Button type="link" disabled={storeItem.idea_store_id == props.disableStoreId}
                                    onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        props.onOk(storeItem.idea_store_id);
                                    }}>{storeItem.name}</Button>
                            </List.Item>
                        )} />
                </Card>
            ))}
        </Modal>
    );
};

export default StoreListModal;