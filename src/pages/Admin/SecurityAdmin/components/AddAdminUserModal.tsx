//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { Checkbox, Form, Input, Modal } from "antd";
import React, { useState } from "react";
import { add_user } from "@/api/admin_auth_admin";
import {
    appStorePermOptionList, calcAppStorePerm, calcFeedBackPerm, calcIdeaStorePerm,
    calcKeywordPerm, calcMenuPerm, calcOrgMemberPerm, calcOrgPerm, calcProjectMemberPerm, calcProjectPerm,
    calcRoadmapPerm,
    calcSwStorePerm, calcUserPerm, calcWidgetStorePerm,
    feedBackPermOptionList,
    ideaStorePermOptionList, keywordPermOptionList, menuPermOptionList, orgMemberPermOptionList, orgPermOptionList, projectMemberPermOptionList,
    projectPermOptionList, roadmapPermOptionList, swStorePermOptionList, userPermOptionList, widgetStorePermOptionList
} from "./permUtil";
import { request } from "@/utils/request";
import { get_admin_session } from "@/api/admin_auth";

export interface AddAdminUserModalProps {
    onCancel: () => void;
    onOk: () => void;
}

const AddAdminUserModal = (props: AddAdminUserModalProps) => {
    const [userName, setUserName] = useState("");
    const [pubKey, setPubKey] = useState("");
    const [userDesc, setUserDesc] = useState("");

    const [userPermValues, setUserPermValues] = useState([] as string[]);
    const [projectPermValues, setProjectPermValues] = useState([] as string[]);
    const [projectMemberPermValues, setProjectMemberPermValues] = useState([] as string[]);
    const [menuPermValues, setMenuPermValues] = useState([] as string[]);
    const [appStorePermValues, setAppStorePermValues] = useState([] as string[]);
    const [ideaStorePermValues, setIdeaStorePermValues] = useState([] as string[]);
    const [widgetStorePermValues, setWidgetStorePermValues] = useState([] as string[]);
    const [swStorePermValues, setSwStorePermValues] = useState([] as string[]);
    const [orgPermValues, setOrgPermValues] = useState([] as string[]);
    const [orgMemberPermValues, setOrgMemberPermValues] = useState([] as string[]);
    const [keywordPermValues, setKeywordPermValues] = useState([] as string[]);
    const [feedBackPermValues, setFeedBackPermValues] = useState([] as string[]);
    const [roadmapPermValues, setRoadmapPermValues] = useState([] as string[]);

    const createAdminUser = async () => {
        const sessionId = await get_admin_session();
        await request(add_user({
            admin_session_id: sessionId,
            user_name: userName,
            user_desc: userDesc,
            pub_key: pubKey,
            perm_info: {
                user_perm: calcUserPerm(userPermValues),
                project_perm: calcProjectPerm(projectPermValues),
                project_member_perm: calcProjectMemberPerm(projectMemberPermValues),
                menu_perm: calcMenuPerm(menuPermValues),
                app_store_perm: calcAppStorePerm(appStorePermValues),
                idea_store_perm: calcIdeaStorePerm(ideaStorePermValues),
                widget_store_perm: calcWidgetStorePerm(widgetStorePermValues),
                sw_store_perm: calcSwStorePerm(swStorePermValues),
                org_perm: calcOrgPerm(orgPermValues),
                org_member_perm: calcOrgMemberPerm(orgMemberPermValues),
                keyword_perm: calcKeywordPerm(keywordPermValues),
                feed_back_perm: calcFeedBackPerm(feedBackPermValues),
                roadmap_perm: calcRoadmapPerm(roadmapPermValues),
                super_admin_user: false,
            },
        }));
        props.onOk();
    };

    return (
        <Modal open title="增加管理员" mask={false}
            bodyStyle={{ maxHeight: "calc(100vh - 300px)", overflowY: "scroll" }}
            okText="增加" okButtonProps={{ disabled: userName == "" || pubKey == "" }}
            width={800}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onCancel();
            }}
            onOk={e => {
                e.stopPropagation();
                e.preventDefault();
                createAdminUser();
            }}>
            <Form labelCol={{ span: 4 }}>
                <Form.Item label="用户名">
                    <Input value={userName} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setUserName(e.target.value.trim());
                    }} />
                </Form.Item>
                <Form.Item label="公钥">
                    <Input.TextArea autoSize={{ minRows: 5, maxRows: 5 }} value={pubKey} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setPubKey(e.target.value);
                    }} />
                </Form.Item>
                <Form.Item label="备注">
                    <Input.TextArea autoSize={{ minRows: 5, maxRows: 5 }} value={userDesc} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setUserDesc(e.target.value);
                    }} />
                </Form.Item>
                <Form.Item label="用户管理权限">
                    <Checkbox.Group options={userPermOptionList} value={userPermValues} onChange={values => setUserPermValues(values as string[])} />
                </Form.Item>
                <Form.Item label="项目管理权限">
                    <Checkbox.Group options={projectPermOptionList} value={projectPermValues} onChange={values => setProjectPermValues(values as string[])} />
                </Form.Item>
                <Form.Item label="项目成员权限">
                    <Checkbox.Group options={projectMemberPermOptionList} value={projectMemberPermValues} onChange={values => setProjectMemberPermValues(values as string[])} />
                </Form.Item>
                <Form.Item label="额外菜单权限">
                    <Checkbox.Group options={menuPermOptionList} value={menuPermValues} onChange={values => setMenuPermValues(values as string[])} />
                </Form.Item>
                <Form.Item label="应用管理权限">
                    <Checkbox.Group options={appStorePermOptionList} value={appStorePermValues} onChange={values => setAppStorePermValues(values as string[])} />
                </Form.Item>
                <Form.Item label="知识库管理权限">
                    <Checkbox.Group options={ideaStorePermOptionList} value={ideaStorePermValues} onChange={values => setIdeaStorePermValues(values as string[])} />
                </Form.Item>
                <Form.Item label="Git内容插件管理权限">
                    <Checkbox.Group options={widgetStorePermOptionList} value={widgetStorePermValues} onChange={values => setWidgetStorePermValues(values as string[])} />
                </Form.Item>
                <Form.Item label="软件管理权限">
                    <Checkbox.Group options={swStorePermOptionList} value={swStorePermValues} onChange={values => setSwStorePermValues(values as string[])} />
                </Form.Item>
                <Form.Item label="团队管理权限">
                    <Checkbox.Group options={orgPermOptionList} value={orgPermValues} onChange={values => setOrgPermValues(values as string[])} />
                </Form.Item>
                <Form.Item label="团队成员权限">
                    <Checkbox.Group options={orgMemberPermOptionList} value={orgMemberPermValues} onChange={values => setOrgMemberPermValues(values as string[])} />
                </Form.Item>
                <Form.Item label="关键词管理权限">
                    <Checkbox.Group options={keywordPermOptionList} value={keywordPermValues} onChange={values => setKeywordPermValues(values as string[])} />
                </Form.Item>
                <Form.Item label="路线图管理权限">
                    <Checkbox.Group options={roadmapPermOptionList} value={roadmapPermValues} onChange={values => setRoadmapPermValues(values as string[])} />
                </Form.Item>
                <Form.Item label="反馈管理权限">
                    <Checkbox.Group options={feedBackPermOptionList} value={feedBackPermValues} onChange={values => setFeedBackPermValues(values as string[])} />
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default AddAdminUserModal;