//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { Card, Checkbox, Form, Popover, Space } from "antd";
import React, { useState } from "react";
import type { AppPerm } from "@/api/appstore";
import type { CheckboxOptionType } from 'antd';
import { InfoCircleOutlined } from "@ant-design/icons";
import { useTranslation } from "react-i18next";



interface AppPermPanelProps {
    disable: boolean;
    showTitle: boolean;
    perm?: AppPerm;
    onChange: (perm: AppPerm) => void;
}

const AppPermPanel: React.FC<AppPermPanelProps> = (props) => {
    const { t } = useTranslation();

    const netOptionList: CheckboxOptionType[] = [
        {
            label: (
                <Space size="small">
                    <span>{t("workbench.minapp.perm.crossDomainHttp")}</span>
                    <Popover content={
                        <div style={{ padding: "10px 10px" }}>
                            {t("workbench.minapp.crossDomainDesc")}
                            <a href="https://tauri.app/v1/api/js/http" target="_blank" rel="noreferrer">{t("workbench.minapp.apiDetail")}</a>。
                        </div>
                    }>
                        <InfoCircleOutlined style={{ color: "blue" }} />
                    </Popover>
                </Space>
            ),
            value: "cross_domain_http",
        },
        {
            label: t("workbench.minapp.perm.proxyRedis"),
            value: "proxy_redis",
        },
        {
            label: t("workbench.minapp.perm.proxyMysql"),
            value: "proxy_mysql",
        },
        {
            label: t("workbench.minapp.perm.proxyPg"),
            value: "proxy_post_gres",
        },
        {
            label: t("workbench.minapp.perm.proxyMongo"),
            value: "proxy_mongo",
        },
        {
            label: t("workbench.minapp.perm.proxySsh"),
            value: "proxy_ssh"
        },
        {
            label: t("workbench.minapp.perm.netutil"),
            value: "net_util",
        },
        {
            label: t("workbench.minapp.perm.proxyGrpc"),
            value: "proxy_grpc",
        }
    ];

    const fsOptionList: CheckboxOptionType[] = [
        {
            label: t("workbench.minapp.perm.readFile"),
            value: "read_file",
        },
        {
            label: t("workbench.minapp.perm.writeFile"),
            value: "write_file",
        },
    ];

    const extraOptionList: CheckboxOptionType[] = [
        {
            label: (
                <Space size="small">
                    <span>crossOriginIsolated</span>
                    <Popover content={
                        <div style={{ padding: "10px 10px" }}>
                            {t("workbench.minapp.crossOriginDesc")}
                            <a href="https://developer.mozilla.org/en-US/docs/Web/API/crossOriginIsolated" target="_blank" rel="noreferrer">{t("workbench.minapp.detailInfo")}</a>。
                        </div>
                    }>
                        <InfoCircleOutlined style={{ color: "blue" }} />
                    </Popover>
                </Space>
            ),
            value: "cross_origin_isolated",
        },
        {
            label: t("workbench.minapp.perm.OpenBrowser"),
            value: "open_browser",
        },
    ];

    const tmpNetValues: string[] = [];
    const tmpFsValues: string[] = [];
    const tmpExtraValues: string[] = [];

    if (props.perm != undefined) {

        if (props.perm.net_perm.cross_domain_http) {
            tmpNetValues.push("cross_domain_http");
        }
        if (props.perm.net_perm.proxy_redis) {
            tmpNetValues.push("proxy_redis");
        }
        if (props.perm.net_perm.proxy_mysql) {
            tmpNetValues.push("proxy_mysql");
        }
        if (props.perm.net_perm.proxy_post_gres) {
            tmpNetValues.push("proxy_post_gres");
        }
        if (props.perm.net_perm.proxy_mongo) {
            tmpNetValues.push("proxy_mongo");
        }
        if (props.perm.net_perm.proxy_ssh) {
            tmpNetValues.push("proxy_ssh");
        }
        if (props.perm.net_perm.proxy_grpc) {
            tmpNetValues.push("proxy_grpc");
        }
        if (props.perm.net_perm.net_util) {
            tmpNetValues.push("net_util");
        } if (props.perm.fs_perm.read_file) {
            tmpFsValues.push("read_file");
        }
        if (props.perm.fs_perm.write_file) {
            tmpFsValues.push("write_file");
        }
        if (props.perm.extra_perm.cross_origin_isolated) {
            tmpExtraValues.push("cross_origin_isolated");
        }
        if (props.perm.extra_perm.open_browser) {
            tmpExtraValues.push("open_browser");
        }
    }

    const [netValues, setNetValues] = useState<string[]>(tmpNetValues);
    const [fsValues, setFsValues] = useState<string[]>(tmpFsValues);
    const [extraValues, setExtraValues] = useState<string[]>(tmpExtraValues);

    const calcPerm = (netPermList: string[], fsPermList: string[], extraPermList: string[]) => {
        const tempPerm: AppPerm = {
            net_perm: {
                cross_domain_http: false,
                proxy_redis: false,
                proxy_mysql: false,
                proxy_post_gres: false,
                proxy_mongo: false,
                proxy_ssh: false,
                net_util: false,
                proxy_grpc: false,
            },
            fs_perm: {
                read_file: false,
                write_file: false,
            },
            extra_perm: {
                cross_origin_isolated: false,
                open_browser: false,
            }
        };
        netPermList.forEach(permStr => {
            if (permStr == "cross_domain_http") {
                tempPerm.net_perm.cross_domain_http = true;
            } else if (permStr == "proxy_redis") {
                tempPerm.net_perm.proxy_redis = true;
            } else if (permStr == "proxy_mysql") {
                tempPerm.net_perm.proxy_mysql = true;
            } else if (permStr == "proxy_post_gres") {
                tempPerm.net_perm.proxy_post_gres = true;
            } else if (permStr == "proxy_mongo") {
                tempPerm.net_perm.proxy_mongo = true;
            } else if (permStr == "proxy_ssh") {
                tempPerm.net_perm.proxy_ssh = true;
            } else if (permStr == "net_util") {
                tempPerm.net_perm.net_util = true;
            } else if (permStr == "proxy_grpc") {
                tempPerm.net_perm.proxy_grpc = true;
            }
        });
        fsPermList.forEach(permStr => {
            if (permStr == "read_file") {
                tempPerm.fs_perm.read_file = true;
            } else if (permStr == "write_file") {
                tempPerm.fs_perm.write_file = true;
            }
        });
        extraPermList.forEach(permStr => {
            if (permStr == "cross_origin_isolated") {
                tempPerm.extra_perm.cross_origin_isolated = true;
            } else if (permStr == "open_browser") {
                tempPerm.extra_perm.open_browser = true;
            }
        });
        props.onChange(tempPerm);
    };

    return (
        <Card title={props.showTitle ? t("workbench.minapp.perm") : null} bordered={false}>
            <Form labelCol={{ span: 5 }}>
                <Form.Item label={t("workbench.minapp.netPerm")}>
                    <Checkbox.Group disabled={props.disable} options={netOptionList} value={netValues}
                        onChange={values => {
                            setNetValues(values as string[]);
                            calcPerm(values as string[], fsValues, extraValues);
                        }} />
                </Form.Item>

                <Form.Item label={t("workbench.minapp.fsPerm")}>
                    <Checkbox.Group disabled={props.disable} options={fsOptionList} value={fsValues}
                        onChange={values => {
                            setFsValues(values as string[]);
                            calcPerm(netValues, values as string[], extraValues);
                        }} />
                </Form.Item>

                <Form.Item label={t("workbench.minapp.otherPerm")}>
                    <Checkbox.Group disabled={props.disable} options={extraOptionList} value={extraValues}
                        onChange={values => {
                            setExtraValues(values as string[]);
                            calcPerm(netValues, fsValues, values as string[]);
                        }} />
                </Form.Item>
            </Form>
        </Card>
    );
}

export default AppPermPanel;