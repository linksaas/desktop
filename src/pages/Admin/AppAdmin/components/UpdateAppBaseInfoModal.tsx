//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { Modal, Form, Input, Card, message, Select } from "antd";
import React, { useEffect, useState } from "react";
import { APP_TYPE_GRPC, APP_TYPE_MONGO, APP_TYPE_MYSQL, APP_TYPE_POSTGRES, APP_TYPE_REDIS, APP_TYPE_SSH, APP_TYPE_UNKWOWN, type APP_TYPE, type BaseAppInfo } from "@/api/appstore";
import s from "./AddAppModal.module.less";
import defaultIcon from '@/assets/allIcon/app-default-icon.png';
import { open as open_dialog } from '@tauri-apps/api/dialog';
import { get_admin_session } from '@/api/admin_auth';
import { write_file, set_file_owner, FILE_OWNER_TYPE_APP_STORE, GLOBAL_APPSTORE_FS_ID } from "@/api/fs";
import { request } from "@/utils/request";
import { useCommonEditor } from "@/components/Editor";
import { update_app } from "@/api/appstore_admin";
import AsyncImage from "@/components/AsyncImage";

interface UpdateAppBaseInfoModalProps {
    isOsWindows: boolean;
    appId: string;
    baseInfo: BaseAppInfo;
    onCancel: () => void;
    onOk: () => void;
}

const UpdateAppBaseInfoModal: React.FC<UpdateAppBaseInfoModalProps> = (props) => {

    const [iconFileId, setIconFileId] = useState(props.baseInfo.icon_file_id);
    const [iconUrl, setIconUrl] = useState("");
    const [appName, setAppName] = useState(props.baseInfo.app_name);
    const [srcUrl, setSrcUrl] = useState(props.baseInfo.src_url);
    const [appType, SetAppType] = useState<APP_TYPE>(APP_TYPE_UNKWOWN);

    const { editor, editorRef } = useCommonEditor({
        content: props.baseInfo.app_desc,
        fsId: "",
        ownerType: 0,
        ownerId: "",
        historyInToolbar: false,
        clipboardInToolbar: false,
        commonInToolbar: true,
        pubResInToolbar: false,
    });

    const changeIcon = async () => {
        const selectd = await open_dialog({
            title: "更好应用图标",
            filters: [{
                name: "图标",
                extensions: ["ico", "png", "jpg", "jpeg"],
            }],
        });
        if (selectd == null || Array.isArray(selectd)) {
            return;
        } else {
            const sessionId = await get_admin_session();
            const res = await request(write_file(sessionId, GLOBAL_APPSTORE_FS_ID, selectd, ""));
            setIconFileId(res.file_id);
            if (props.isOsWindows) {
                setIconUrl(`https://fs.localhost/${GLOBAL_APPSTORE_FS_ID}/${res.file_id}/icon.png`);
            } else {
                setIconUrl(`fs://localhost/${GLOBAL_APPSTORE_FS_ID}/${res.file_id}/icon.png`);
            }
        }
    };

    const updateBaseInfo = async () => {
        const appNameValue = appName.trim();
        if (appNameValue.length == 0) {
            message.error("请输入应用名称");
            return;
        }
        const sessionId = await get_admin_session();
        if (iconFileId != "" && iconFileId != props.baseInfo.icon_file_id) {
            await request(set_file_owner({
                session_id: sessionId,
                fs_id: GLOBAL_APPSTORE_FS_ID,
                file_id: iconFileId,
                owner_type: FILE_OWNER_TYPE_APP_STORE,
                owner_id: props.appId,
            }));
        }
        const content = editorRef.current?.getContent() ?? { type: 'doc' };
        await request(update_app({
            admin_session_id: sessionId,
            app_id: props.appId,
            base_info: {
                app_name: appNameValue,
                app_desc: JSON.stringify(content),
                icon_file_id: iconFileId,
                src_url: srcUrl.startsWith("https://") ? srcUrl : props.baseInfo.src_url,
                app_type: appType,
            },
        }));
        props.onOk();
    };

    useEffect(() => {
        if (props.baseInfo.icon_file_id != "") {
            if (props.isOsWindows) {
                setIconUrl(`https://fs.localhost/${GLOBAL_APPSTORE_FS_ID}/${props.baseInfo.icon_file_id}/icon.png`);
            } else {
                setIconUrl(`fs://localhost/${GLOBAL_APPSTORE_FS_ID}/${props.baseInfo.icon_file_id}/icon.png`);
            }
        }
    }, []);

    return (
        <Modal open title="修改应用信息" mask={false}
            okText="修改"
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onCancel();
            }}
            onOk={e => {
                e.stopPropagation();
                e.preventDefault();
                updateBaseInfo();
            }}>
            <div className={s.head}>
                <div className={s.left}>
                    <AsyncImage style={{ width: "80px", cursor: "pointer" }}
                        src={iconUrl}
                        preview={false}
                        fallback={defaultIcon}
                        onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            changeIcon();
                        }}
                        useRawImg={false}
                    />
                </div>
                <div className={s.right}>
                    <Form labelCol={{ span: 8 }}>
                        <Form.Item label="应用名称">
                            <Input value={appName} onChange={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setAppName(e.target.value);
                            }} />
                        </Form.Item>
                        <Form.Item label="源代码地址">
                            <Input value={srcUrl} onChange={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setSrcUrl(e.target.value);
                            }} />
                        </Form.Item>
                        <Form.Item label="关联类型">
                            <Select value={appType} onChange={value => SetAppType(value)}>
                                <Select.Option value={APP_TYPE_UNKWOWN}>不关联</Select.Option>
                                <Select.Option value={APP_TYPE_SSH}>SSH</Select.Option>
                                <Select.Option value={APP_TYPE_MYSQL}>MYSQL</Select.Option>
                                <Select.Option value={APP_TYPE_POSTGRES}>POSTGRES</Select.Option>
                                <Select.Option value={APP_TYPE_MONGO}>MONGO</Select.Option>
                                <Select.Option value={APP_TYPE_REDIS}>REDIS</Select.Option>
                                <Select.Option value={APP_TYPE_GRPC}>GRPC</Select.Option>
                            </Select>
                        </Form.Item>
                    </Form>
                </div>
            </div>
            <Card title={<h2 style={{ fontSize: "14px", fontWeight: 800 }}>应用描述</h2>} bordered={false}>
                <div className="_projectEditContext">
                    {editor}
                </div>
            </Card>
        </Modal>
    );
};

export default UpdateAppBaseInfoModal;