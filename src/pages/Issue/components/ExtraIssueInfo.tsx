//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from "react";
import { Tabs } from 'antd';
import { SubIssuePanel } from "./SubIssuePanel";
import { MyDependPanel } from "./MyDependPanel";
import { DependMePanel } from "./DependMePanel";
import type { ProjectInfo } from "@/api/project";
import type { ISSUE_TYPE } from "@/api/project_issue";



interface ExtraIssueInfoProps {
    projectInfo: ProjectInfo;
    myUserId: string;
    issueId: string;
    canOptSubIssue: boolean;
    canOptDependence: boolean;
    onClickIssue: (projectId: string, issueId: string, issueType: ISSUE_TYPE) => void;
}

export const ExtraIssueInfo: React.FC<ExtraIssueInfoProps> = (props) => {
    return (
        <div style={{ backgroundColor: "white" }}>
            <Tabs defaultActiveKey="sub" type="card" tabPosition="left" size="large">
                <Tabs.TabPane tab="子任务" key="sub">
                    <SubIssuePanel projectInfo={props.projectInfo} issueId={props.issueId} canOptSubIssue={props.canOptSubIssue} inModal={false} />
                </Tabs.TabPane>
                <Tabs.TabPane tab="我的依赖" key="myDepend">
                    <MyDependPanel projectInfo={props.projectInfo} issueId={props.issueId} canOptDependence={props.canOptDependence} inModal={false}
                        onClickIssue={(projectId, issueId, issueType) => props.onClickIssue(projectId, issueId, issueType)} myUserId={props.myUserId}/>
                </Tabs.TabPane>
                <Tabs.TabPane tab="依赖我的" key="dependMe">
                    <DependMePanel projectInfo={props.projectInfo} issueId={props.issueId} inModal={false}
                        onClickIssue={(projectId, issueId, issueType) => props.onClickIssue(projectId, issueId, issueType)} />
                </Tabs.TabPane>
            </Tabs>
        </div>
    );
};