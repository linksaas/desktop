//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState, useEffect } from "react";
import { Card, Table, message } from 'antd';
import { request } from '@/utils/request';
import type { ISSUE_TYPE, IssueInfo } from '@/api/project_issue';
import { add_dependence, list_my_depend, remove_dependence } from '@/api/project_issue';
import type { ColumnsType } from 'antd/lib/table';
import { get_issue_type_str } from '@/api/event_type';
import { renderState, renderTitle } from "./dependComon";
import Button from "@/components/Button";
import { listen } from '@tauri-apps/api/event';
import type * as NoticeType from '@/api/notice_type';
import type { ProjectInfo } from "@/api/project";
import { get_session } from "@/api/user";
import AddTaskOrBug from "./AddTaskOrBug";
import { LINK_TARGET_TYPE, LinkBugInfo, LinkTaskInfo, type LinkInfo } from "@/stores/linkAux";


interface MyDependPanelProps {
    projectInfo: ProjectInfo;
    myUserId: string;
    issueId: string;
    canOptDependence: boolean;
    inModal: boolean;
    onClickIssue: (projectId: string, issueId: string, issueType: ISSUE_TYPE) => void;
}

export const MyDependPanel: React.FC<MyDependPanelProps> = (props) => {

    const [issueList, setIssueList] = useState<IssueInfo[]>([]);
    const [showSelectLink, setShowSelectLink] = useState(false);


    const loadIssue = async () => {
        const sessionId = await get_session();
        const res = await request(list_my_depend({
            session_id: sessionId,
            project_id: props.projectInfo.project_id,
            issue_id: props.issueId,
        }));
        if (res) {
            setIssueList(res.issue_list);
        }
    };

    const addDependIssue = async (dependIssueId: string) => {
        const sessionId = await get_session();
        const index = issueList.findIndex(item => item.issue_id == dependIssueId);
        if (index != -1) {
            setShowSelectLink(false);
            return;
        }
        const res = await request(add_dependence({
            session_id: sessionId,
            project_id: props.projectInfo.project_id,
            issue_id: props.issueId,
            depend_issue_id: dependIssueId,
        }));
        if (res) {
            message.info("设置依赖工单成功");
            setShowSelectLink(false);
        }
    };

    const removeDependIssue = async (dependIssueId: string) => {
        const sessionId = await get_session();
        const res = await request(remove_dependence({
            session_id: sessionId,
            project_id: props.projectInfo.project_id,
            issue_id: props.issueId,
            depend_issue_id: dependIssueId,
        }));
        if (res) {
            message.info("取消依赖工单成功");
            const itemList = issueList.filter(item => item.issue_id != dependIssueId);
            setIssueList(itemList);
        }
    };

    const issueColums: ColumnsType<IssueInfo> = [
        {
            title: 'ID',
            dataIndex: 'issue_index',
            ellipsis: true,
            width: 60,
        },
        {
            title: '类别',
            dataIndex: 'issue_type',
            width: 60,
            render: (v: number) => {
                return get_issue_type_str(v);
            },
        },
        {
            title: '名称',
            ellipsis: true,
            dataIndex: ['basic_info', 'title'],
            width: 150,
            render: (_, row: IssueInfo) => renderTitle(row, props.inModal, () => {
                props.onClickIssue(row.project_id, row.issue_id, row.issue_type);
            }),
        },
        {
            title: '阶段',
            dataIndex: 'state',
            width: 100,
            align: 'center',
            render: (val: number) => renderState(val),
        },
        {
            title: '操作',
            width: 80,
            render: (_, record: IssueInfo) => {
                return (
                    <Button
                        type="link"
                        disabled={props.projectInfo.closed || !props.canOptDependence}
                        style={{ minWidth: 0, padding: "0px 0px" }}
                        onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            removeDependIssue(record.issue_id);
                        }}>取消依赖</Button>
                );
            }
        }
    ];

    useEffect(() => {
        loadIssue();
    }, [props.issueId])

    useEffect(() => {
        const unListenFn = listen<NoticeType.AllNotice>("notice", ev => {
            const notice = ev.payload;
            if (notice.IssueNotice?.UpdateIssueDepNotice != undefined && notice.IssueNotice.UpdateIssueDepNotice.issue_id == props.issueId) {
                loadIssue();
            }
        });
        return () => {
            unListenFn.then((unListen) => unListen());
        };
    }, [props.issueId]);

    return (
        <Card bordered={false}
            bodyStyle={{ maxHeight: "calc(100vh - 370px)", overflowY: "scroll" }}
            extra={
                <Button
                    type={props.inModal ? "primary" : "link"}
                    disabled={props.projectInfo.closed || !props.canOptDependence}
                    onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setShowSelectLink(true);
                    }}>新增依赖工单</Button>
            }>
            <Table rowKey={'issue_id'} dataSource={issueList} columns={issueColums} pagination={false} />
            {showSelectLink == true && (
                <AddTaskOrBug projectInfo={props.projectInfo} myUserId={props.myUserId}
                    open title="依赖任务/缺陷"
                    type='all' issueIdList={[props.issueId, ...issueList.map(item => item.issue_id)]}
                    onOK={links => {
                        let tmpLinks: LinkInfo[] = [];
                        if (Array.isArray(links)) {
                            tmpLinks = links.slice();
                        } else {
                            tmpLinks.push(links);
                        }
                        for (const link of tmpLinks) {
                            let dependIssueId = "";
                            if (link.linkTargeType == LINK_TARGET_TYPE.LINK_TARGET_TASK) {
                                const taskLink = link as LinkTaskInfo;
                                dependIssueId = taskLink.issueId;
                            } else if (link.linkTargeType == LINK_TARGET_TYPE.LINK_TARGET_BUG) {
                                const bugLink = link as LinkBugInfo;
                                dependIssueId = bugLink.issueId;
                            }
                            if (dependIssueId != props.issueId) {
                                addDependIssue(dependIssueId);
                            }
                        }
                    }}
                    onCancel={() => setShowSelectLink(false)} />
            )}
        </Card>
    );
};