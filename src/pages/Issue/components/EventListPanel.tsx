//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import type { PluginEvent } from '@/api/events';
import { EVENT_REF_TYPE_BUG, EVENT_REF_TYPE_TASK, EVENT_TYPE_BUG, EVENT_TYPE_TASK } from '@/api/events';
import { request } from "@/utils/request";
import { list_event_by_ref } from '@/api/events';
import { Space, Timeline } from "antd";
import EventCom from "@/components/EventCom";
import moment from "moment";
import UserPhoto from "@/components/Portrait/UserPhoto";
import { type ISSUE_TYPE, ISSUE_TYPE_TASK } from "@/api/project_issue";
import type { ProjectInfo } from "@/api/project";
import { get_session } from "@/api/user";

export interface EventListPanelProps {
    projectInfo: ProjectInfo;
    issueType: ISSUE_TYPE;
    issueId: string;
}

const EventListPanel = (props: EventListPanelProps) => {

    const [timeLine, setTimeLine] = useState<PluginEvent[]>();

    const loadEvent = async () => {
        const sessionId = await get_session();
        const res = await request(list_event_by_ref({
            session_id: sessionId,
            project_id: props.projectInfo.project_id,
            event_type: props.issueType == ISSUE_TYPE_TASK ? EVENT_TYPE_TASK : EVENT_TYPE_BUG,
            ref_type: props.issueType == ISSUE_TYPE_TASK ? EVENT_REF_TYPE_TASK : EVENT_REF_TYPE_BUG,
            ref_id: props.issueId,
        }));
        setTimeLine(res.event_list);
    }

    useEffect(() => {
        loadEvent();
    }, [props.issueId]);

    return (
        <Timeline reverse={true} style={{ paddingTop: "10px" }}>
            {timeLine?.map((item) => (
                <Timeline.Item color="gray" key={item.event_id}>
                    <Space>
                        <UserPhoto logoUri={item.cur_logo_uri} style={{ width: "16px", borderRadius: "10px" }} />
                        <span>
                            {item.cur_user_display_name}({moment(item.event_time).format("YYYY-MM-DD HH:mm:ss")})
                        </span>
                    </Space>
                    <EventCom key={item.event_id} item={item} skipProjectName={true} skipLink={true} showMoreLink={false} />
                </Timeline.Item>
            ))}
        </Timeline>
    );
};

export default EventListPanel;