//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import type { FC } from 'react';
import React from 'react';
import s from './Tabs.module.less';
import filterIcon from '@/assets/image/filterIcon.png';
import noFilterIcon from '@/assets/image/noFilterIcon.png';
import type { ISSUE_TAB_LIST_TYPE } from '@/stores/linkAux';
import { Input } from 'antd';

type TabsProps = {
  list: { name: string; value: ISSUE_TAB_LIST_TYPE }[];
  activeVal: ISSUE_TAB_LIST_TYPE;
  onChang: (val: ISSUE_TAB_LIST_TYPE) => void;
  isFilter: boolean;
  setIsFilter: (val: boolean) => void;
  keyword: string;
  setKeyword: (val: string) => void;
};

const Tabs: FC<TabsProps> = (props) => {
  const { list, activeVal, onChang, isFilter, setIsFilter } = props;

  return (
    <div className={s.tabs_wrap}>
      {list.map((item) => (
        <div
          key={item.value}
          className={item.value == activeVal ? s.active : ''}
          onClick={() => onChang(item.value)}
        >
          {item.name}
        </div>
      ))}
      <div style={{ opacity: 0.2 }}>|</div>
      <div>
        <Input size='small' style={{ width: "200px" }} placeholder='过滤标题' allowClear value={props.keyword}
          onChange={e => {
            e.stopPropagation();
            e.preventDefault();
            props.setKeyword(e.target.value.trim());
          }} />
      </div>
      <div style={{ opacity: 0.2 }}>|</div>
      <div onClick={() => setIsFilter(!isFilter)} className={isFilter ? s.filter : ''}>
        <img
          src={isFilter ? filterIcon : noFilterIcon}
          alt=""
          style={{ verticalAlign: 'middle' }}
        />
        更多筛选
      </div>
    </div>
  );
};

export default Tabs;
