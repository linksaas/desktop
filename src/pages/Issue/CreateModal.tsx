//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";

import { DatePicker, Descriptions, Divider, Form, Input, Modal, Select, message } from "antd";
import { BUG_LEVEL_MINOR, BUG_PRIORITY_LOW, ISSUE_TYPE, ISSUE_TYPE_BUG, ISSUE_TYPE_TASK, TASK_PRIORITY_LOW, assign_check_user, assign_exec_user, create as create_issue, set_dead_line_time } from "@/api/project_issue";
import { change_file_fs, change_file_owner, useCommonEditor } from "@/components/Editor";
import { FILE_OWNER_TYPE_ISSUE, FILE_OWNER_TYPE_PROJECT } from "@/api/fs";
import { Moment } from "moment";
import { EditSelectItem } from "@/components/EditCell/EditSelect";
import { bugLvSelectItems, bugPrioritySelectItems, taskPrioritySelectItems } from "./components/constant";
import { getMemberSelectItems } from "./components/utils";
import { request } from "@/utils/request";
import type { ProjectInfo } from "@/api/project";
import { type MemberInfo, list_member } from "@/api/project_member";
import { get_session } from "@/api/user";

interface RenderSelectProps {
    itemList: EditSelectItem[];
    value: string | number;
    allowClear: boolean;
    onChange: (value: string | number | undefined) => boolean;
}

const RenderSelect: React.FC<RenderSelectProps> = (props) => {
    const [selValue, setSelValue] = useState(props.value);

    return (
        <Select
            allowClear={props.allowClear}
            value={selValue}
            showArrow={true}
            style={{ width: "120px" }}
            onChange={(value) => {
                if (props.onChange(value)) {
                    setSelValue(value);
                }
            }}>
            {props.itemList.map(item => (
                <Select.Option key={item.value} value={item.value}>
                    <span style={{ color: item.color, display: "inline-block", width: "80px", textAlign: "center" }}>{item.label}</span>
                </Select.Option>
            ))}
        </Select>
    );
};

export interface CreateModalProps {
    projectInfo: ProjectInfo;
    createIssueType: ISSUE_TYPE;
    createIssueLinkSpritId: string;
    onCancel: () => void;
    onOk: (issueId: string) => void;
}

const CreateModal = (props: CreateModalProps) => {

    const { editor, editorRef } = useCommonEditor({
        content: "",
        fsId: props.projectInfo.issue_fs_id,
        ownerType: FILE_OWNER_TYPE_PROJECT,
        ownerId: props.projectInfo.project_id,
        historyInToolbar: false,
        clipboardInToolbar: false,
        commonInToolbar: true,
        pubResInToolbar: false,
    });

    const [title, setTitle] = useState("");
    const [bugLv, setBugLv] = useState(BUG_LEVEL_MINOR);
    const [bugPriority, setBugPriority] = useState(BUG_PRIORITY_LOW);
    const [softVersion, setSoftVersion] = useState("");
    const [taskPriority, setTaskPriority] = useState(TASK_PRIORITY_LOW);
    const [execUserId, setExecUserId] = useState("");
    const [checkUserId, setCheckUserId] = useState("");
    const [deadLineTime, setDeadLineTime] = useState<Moment | null>(null);
    const [memberList, setMemberList] = useState<MemberInfo[]>([]);

    const loadMemberList = async () => {
        const sessionId = await get_session();
        const res = await request(list_member(sessionId, props.projectInfo.project_id, false, []));
        setMemberList(res.member_list);
    };

    const createIssue = async () => {
        if (title == "") {
            message.error("标题不能为空");
            return;
        }
        if (editorRef.current == null) {
            return;
        }
        const sessionId = await get_session();
        const content = editorRef.current.getContent();;
        //更新文件存储
        await change_file_fs(
            content,
            props.projectInfo.issue_fs_id,
            sessionId,
            FILE_OWNER_TYPE_PROJECT,
            props.projectInfo.project_id,
        );
        const createRes = await request(create_issue({
            session_id: sessionId,
            project_id: props.projectInfo.project_id,
            issue_type: props.createIssueType,
            basic_info: {
                title: title,
                content: JSON.stringify(content),
                tag_id_list: [],
            },
            extra_info: {
                ExtraTaskInfo: props.createIssueType == ISSUE_TYPE_TASK ? {
                    priority: taskPriority,
                } : undefined,
                ExtraBugInfo: props.createIssueType == ISSUE_TYPE_BUG ? {
                    software_version: softVersion,
                    level: bugLv,
                    priority: bugPriority,
                } : undefined,
            }
        }));
        if (!createRes) {
            return;
        }
        if (deadLineTime != null) {
            await request(set_dead_line_time({
                session_id: sessionId,
                project_id: props.projectInfo.project_id,
                issue_id: createRes.issue_id,
                dead_line_time: deadLineTime.startOf("day").valueOf(),
            }));
        }
        //变更文件Owner
        await change_file_owner(content, sessionId, FILE_OWNER_TYPE_ISSUE, createRes.issue_id);
        if (execUserId != "") {
            await request(assign_exec_user(sessionId, props.projectInfo.project_id, createRes.issue_id, execUserId));
        }
        if (checkUserId != "") {
            await request(assign_check_user(sessionId, props.projectInfo.project_id, createRes.issue_id, checkUserId));
        }
        message.info(`创建${props.createIssueType == ISSUE_TYPE_TASK ? "任务" : "缺陷"}成功`);

        props.onOk(createRes.issue_id);
    }

    useEffect(() => {
        loadMemberList();
    }, [props.projectInfo.project_id]);

    return (
        <Modal open title={props.createIssueType == ISSUE_TYPE_TASK ? "创建任务" : "创建缺陷"}
            okText="创建" okButtonProps={{ disabled: title.trim() == "" }} width={800} mask={false}
            bodyStyle={{ maxHeight: "calc(100vh - 300px)", overflowY: "scroll" }}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onCancel();
            }}
            onOk={e => {
                e.stopPropagation();
                e.preventDefault();
                createIssue();
            }}
        >
            <Form labelCol={{ span: 2 }}>
                <Form.Item label="标题">
                    <Input
                        allowClear
                        bordered={false}
                        placeholder={`请输入标题`}
                        style={{ marginBottom: '12px', borderBottom: "1px solid #e4e4e8" }}
                        onChange={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            setTitle(e.target.value);
                        }}
                    />
                </Form.Item>
                <Form.Item label="内容">
                    <div className="_chatContext">
                        {editor}
                    </div>
                </Form.Item>
            </Form>
            <Divider orientation="left">其他配置</Divider>
            <Descriptions bordered>
                {props.createIssueType == ISSUE_TYPE_BUG && (
                    <>
                        <Descriptions.Item label="级别">
                            <RenderSelect itemList={bugLvSelectItems} value={BUG_LEVEL_MINOR}
                                allowClear={false}
                                onChange={
                                    (value) => {
                                        setBugLv(value as number);
                                        return true;
                                    }} />
                        </Descriptions.Item>
                        <Descriptions.Item label="优先级">
                            <RenderSelect itemList={bugPrioritySelectItems} value={BUG_PRIORITY_LOW}
                                allowClear={false}
                                onChange={
                                    (value) => {
                                        setBugPriority(value as number);
                                        return true;
                                    }} />
                        </Descriptions.Item>
                        <Descriptions.Item label="软件版本">
                            <Input style={{ width: "120px" }} onChange={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setSoftVersion(e.target.value);
                            }} />
                        </Descriptions.Item>
                    </>
                )}
                {props.createIssueType == ISSUE_TYPE_TASK && (
                    <Descriptions.Item label="优先级">
                        <RenderSelect itemList={taskPrioritySelectItems} value={TASK_PRIORITY_LOW}
                            allowClear={false}
                            onChange={(value) => {
                                setTaskPriority(value as number);
                                return true;
                            }} />
                    </Descriptions.Item>
                )}
                <Descriptions.Item label="处理人">
                    <RenderSelect itemList={getMemberSelectItems(memberList)} value=""
                        allowClear
                        onChange={(value) => {
                            let v = value;
                            if (v === undefined) {
                                v = "";
                            }
                            if (checkUserId != "" && checkUserId == v) {
                                message.error("处理人和验收人不能是同一人");
                                return false;
                            }
                            setExecUserId(v as string);
                            return true;
                        }} />
                </Descriptions.Item>
                <Descriptions.Item label="验收人">
                    <RenderSelect itemList={getMemberSelectItems(memberList)} value=""
                        allowClear
                        onChange={(value) => {
                            let v = value;
                            if (v === undefined) {
                                v = "";
                            }
                            if (execUserId != "" && execUserId == v) {
                                message.error("验收人和处理人不能是同一人");
                                return false;
                            }
                            setCheckUserId(v as string);
                            return true;
                        }} />
                </Descriptions.Item>
                <Descriptions.Item label="截止时间">
                    <DatePicker style={{ width: "120px" }} allowClear onChange={value => setDeadLineTime(value)} popupStyle={{ zIndex: 10000 }} />
                </Descriptions.Item>
            </Descriptions>
        </Modal>
    );
};

export default CreateModal;