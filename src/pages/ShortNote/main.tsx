//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import { createRoot } from 'react-dom/client';
import '@/styles/global.less';
import randomColor from 'randomcolor';
import { CloseSquareOutlined, FormatPainterOutlined, PushpinFilled, PushpinOutlined } from '@ant-design/icons/lib/icons';
import { appWindow, WebviewWindow } from '@tauri-apps/api/window';
import type { ShortNoteEvent } from '@/utils/short_note';
import { useLocation } from 'react-router-dom';
import { BrowserRouter } from 'react-router-dom';
import type { SHORT_NOTE_TYPE } from '@/api/short_note';
import { SHORT_NOTE_TASK, SHORT_NOTE_BUG, SHORT_NOTE_MODE_DETAIL } from '@/api/short_note';
import '@/styles/global.less';
import { message } from 'antd';
import { report_error } from '@/api/client_cfg';

const Content = () => {
    const [bgColor, setBgColor] = useState("");
    const [onTop, setOnTop] = useState(true);

    const location = useLocation();
    const urlParams = new URLSearchParams(location.search);
    const shortNoteTypeStr = urlParams.get("type");
    const projectId = urlParams.get("projectId");
    const projectName = urlParams.get("projectName");
    const id = urlParams.get("id");
    const title = urlParams.get("title");

    let shortNoteType: SHORT_NOTE_TYPE = SHORT_NOTE_TASK;
    if (shortNoteTypeStr == "task") {
        shortNoteType = SHORT_NOTE_TASK;
    } else if (shortNoteTypeStr == "bug") {
        shortNoteType = SHORT_NOTE_BUG;
    }

    const randomBgColor = () => {
        const color = randomColor({ luminosity: "light", format: "rgba", alpha: 0.8 });
        setBgColor(color);
    };

    const getShortNoteType = () => {
        if (shortNoteType == SHORT_NOTE_TASK) {
            return "任务";
        } else if (shortNoteType == SHORT_NOTE_BUG) {
            return "缺陷";
        }
        return "";
    };

    const showDetail = () => {
        const ev: ShortNoteEvent = {
            projectId: projectId ?? "",
            shortNoteModeType: SHORT_NOTE_MODE_DETAIL,
            shortNoteType: shortNoteType,
            targetId: id ?? "",
            extraTargetValue: "",
        };
        const mainWindow = WebviewWindow.getByLabel("main");
        mainWindow?.emit("shortNote", ev);
    };

    useEffect(() => {
        randomBgColor();
    }, []);

    return (
        <div style={{
            backgroundColor: bgColor,
            width: "100wh",
            height: "100vh",
            overflowY: "scroll",
            cursor: "move"
        }} data-tauri-drag-region>
            <div style={{ position: "relative", height: "30px", paddingTop: "10px" }} data-tauri-drag-region>
                <div style={{ position: "absolute", right: "0px" }}>
                    <a onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        if (onTop) {
                            appWindow.setAlwaysOnTop(false).then(() => setOnTop(false));
                        } else {
                            appWindow.setAlwaysOnTop(true).then(() => setOnTop(true));
                        }
                    }}>
                        {onTop == true && (
                            <PushpinFilled style={{ fontSize: "24px", color: "black" }} title='取消置顶' />
                        )}
                        {onTop == false && (
                            <PushpinOutlined style={{ fontSize: "24px", color: "black" }} title='置顶' />
                        )}
                    </a>
                    <a style={{ marginLeft: "10px" }} onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        randomBgColor();
                    }}><FormatPainterOutlined style={{ fontSize: "24px", color: "black" }} title='更新背景色' /></a>
                    <a style={{ marginLeft: "10px" }} onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        appWindow.close();
                    }}><CloseSquareOutlined style={{ fontSize: "24px", color: "black" }} title='关闭' /></a>
                </div>
            </div>
            <div style={{ paddingLeft: "20px" }} data-tauri-drag-region>
                {projectName != "" && (
                    <div data-tauri-drag-region>项目:{projectName}</div>
                )}
                <div data-tauri-drag-region>
                    {getShortNoteType()}:{title}&nbsp;&nbsp;
                    <a onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        showDetail();
                    }}>查看详情</a>
                </div>
            </div>
        </div>
    );
}


const App = () => {
    return (
        <BrowserRouter><Content /></BrowserRouter>
    );
}

const root = createRoot(document.getElementById('root')!);
root.render(<App />);

window.addEventListener('unhandledrejection', function (event) {
    // 防止默认处理（例如将错误输出到控制台）
    event.preventDefault();
    if (`${event.reason}`.includes("error trying to connect")) {
      return;
    }
    message.error(event?.reason);
    // console.log(event);
    try {
      report_error({
        err_data: `${event?.reason}`,
      });
    } catch (e) {
      console.log(e);
    }
  });