//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect } from "react";
import { observer } from "mobx-react";
import { Button, Card, List, message, Popover, Space, Tag } from "antd";
import { useGitStores } from "../store";
import moment from "moment";
import type { LocalRepoStashInfo } from "@/api/local_repo";
import { appWindow, WebviewWindow } from "@tauri-apps/api/window";
import { stash_drop, stash_pop } from "@/api/git_wrap";
import { MoreOutlined } from "@ant-design/icons";

const StashList = () => {
    const store = useGitStores();

    const openCommitDiff = async (commit: LocalRepoStashInfo, fileName: string) => {
        const pos = await appWindow.innerPosition();
        new WebviewWindow(`commit:${commit.commit_id}`, {
            url: `git_diff.html?path=${encodeURIComponent(store.repoInfo?.path ?? "")}&commitId=${commit.commit_id}&summary=${encodeURIComponent(commit.commit_summary)}&commiter=&fileName=${fileName}`,
            title: `${store.repoInfo?.name ?? ""}(commit:${commit.commit_id.substring(0, 8)})`,
            x: pos.x + Math.floor(Math.random() * 200),
            y: pos.y + Math.floor(Math.random() * 200),
        })
    };

    useEffect(() => {
        store.loadStashList();
    }, []);

    return (
        <List rowKey="commit_id" dataSource={store.stashList} pagination={false}
            style={{ height: "100vh", overflowY: "scroll", backgroundColor: "white" }} renderItem={(info, infoIndex) => (
                <List.Item>
                    <Card style={{ width: "100%" }} title={`${moment(info.time_stamp).format("YYYY-MM-DD HH:mm:ss")} ${info.commit_summary}`}
                        headStyle={{ backgroundColor: "#eee" }} bordered={false} extra={
                            <Space>
                                <Button type="primary" onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    stash_pop(store.repoInfo?.path ?? "", infoIndex).then(() => {
                                        store.loadStashList();
                                        message.info("恢复成功");
                                    });
                                }}>恢复</Button>
                                <Popover placement="bottom" trigger="click" content={
                                    <Space direction="vertical">
                                        <Button type="link" danger onClick={e => {
                                            e.stopPropagation();
                                            e.preventDefault();
                                            stash_drop(store.repoInfo?.path ?? "", infoIndex).then(() => {
                                                store.loadStashList();
                                                message.info("删除成功");
                                            });
                                        }}>删除</Button>
                                    </Space>
                                }>
                                    <MoreOutlined />
                                </Popover>
                            </Space>
                        }>
                        <Space style={{ flexWrap: "wrap" }}>
                            {info.file_list.map(fileName => (
                                <Tag key={fileName}>
                                    <a onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        openCommitDiff(info, fileName);
                                    }}>{fileName}</a>
                                </Tag>
                            ))}
                        </Space>
                    </Card>
                </List.Item>
            )} split={false} />
    );
};

export default observer(StashList);
