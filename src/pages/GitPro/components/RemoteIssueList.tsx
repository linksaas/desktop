//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { observer } from "mobx-react";
import { useGitStores } from "../store";
import { USER_TYPE_ATOM_GIT, USER_TYPE_GIT_CODE, USER_TYPE_GITEE, USER_TYPE_INTERNAL } from "@/api/user";
import { AtomGitIssueList } from "@/pages/Workbench/components/AtomGitList";
import { GitCodeIssueList } from "@/pages/Workbench/components/GitCodeList";
import { GiteeIssueList } from "@/pages/Workbench/components/GiteeList";

const RemoteIssueList = () => {
    const store = useGitStores();

    const [remoteSite, setRemoteSite] = useState<"" | "atomgit" | "gitcode" | "gitee">("");
    const [ownerName, setOwnerName] = useState("");
    const [repoName, setRepoName] = useState("");

    const calcOwnerAndReop = (url: string) => {
        let ownerAndRepoName = "";
        if (url.includes("@") && url.includes(":")) {
            const index = url.indexOf(":");
            ownerAndRepoName = url.substring(index + 1);
        } else if (url.startsWith("https://")) {
            const remain = url.substring("https://".length);
            const index = url.indexOf("/");
            if (index == -1) {
                return false;
            }
            ownerAndRepoName = remain.substring(index + 1);
        }
        const parts = ownerAndRepoName.split("/");
        if (parts.length == 2) {
            setOwnerName(parts[0]);
            if (parts[1].endsWith(".git")) {
                setRepoName(parts[1].substring(0, parts[1].length - 4));
            } else {
                setRepoName(parts[1]);
            }
            return true;
        }
        return false;
    };

    const checkRemoteSite = () => {
        if (store.userToken == "") {
            return;
        }
        if (store.userType == USER_TYPE_INTERNAL) {
            return;
        }
        const remoteInfo = store.remoteList.find(item => item.name == "origin");
        if (remoteInfo == undefined) {
            return;
        }

        if (calcOwnerAndReop(remoteInfo.url) == false) {
            return;
        }


        if (store.userType == USER_TYPE_ATOM_GIT) {
            if (remoteInfo.url.startsWith("https://atomgit.com/")) {
                setRemoteSite("atomgit");
                return;
            }
            if (remoteInfo.url.includes("@atomgit.com:")) {
                setRemoteSite("atomgit");
                return;
            }
        } else if (store.userType == USER_TYPE_GITEE) {
            if (remoteInfo.url.startsWith("https://gitee.com/")) {
                setRemoteSite("gitee");
                return;
            }
            if (remoteInfo.url.includes("@gitee.com:")) {
                setRemoteSite("gitee");
                return;
            }
        } else if (store.userType == USER_TYPE_GIT_CODE) {
            if (remoteInfo.url.startsWith("https://gitcode.com/")) {
                setRemoteSite("gitcode");
                return;
            }
            if (remoteInfo.url.includes("@gitcode.com:")) {
                setRemoteSite("gitcode");
                return;
            }
        }
    };

    useEffect(() => {
        checkRemoteSite();
    }, [store.userType, store.userToken, store.remoteList]);

    return (
        <>
            {remoteSite == "atomgit" && (
                <div style={{ height: "100vh", overflowY: "scroll", backgroundColor: "white", padding: "0px 10px" }}>
                    <AtomGitIssueList userToken={store.userToken} ownerName={ownerName} repoName={repoName} />
                </div>
            )}
            {remoteSite == "gitcode" && (
                <div style={{ height: "100vh", overflowY: "scroll", backgroundColor: "white", padding: "0px 10px" }}>
                    <GitCodeIssueList userToken={store.userToken} ownerName={ownerName} repoName={repoName} />
                </div>
            )}
            {remoteSite == "gitee" && (
                <div style={{ height: "100vh", overflowY: "scroll", backgroundColor: "white", padding: "0px 10px" }}>
                    <GiteeIssueList userToken={store.userToken} ownerName={ownerName} repoName={repoName} />
                </div>
            )}
        </>
    );
};


export default observer(RemoteIssueList);
