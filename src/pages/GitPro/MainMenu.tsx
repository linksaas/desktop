//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { observer } from "mobx-react";
import React, { useEffect, useState } from "react";
import { useGitStores } from "./store";
import { Badge, Button, Divider, Input, Menu, message, Space } from "antd";
import { ItemType } from "antd/lib/menu/hooks/useItems";
import { BranchesOutlined, CloudOutlined, ReloadOutlined, SearchOutlined, TagOutlined } from "@ant-design/icons";
import { USER_TYPE_ATOM_GIT, USER_TYPE_GIT_CODE, USER_TYPE_GITEE, USER_TYPE_INTERNAL } from "@/api/user";

export interface MainMenuProps {
    curMenuValue: string;
    onMenuChange: (value: string) => void;
}

const MainMenu = (props: MainMenuProps) => {
    const store = useGitStores();

    const [refItemList, setRefItemList] = useState<ItemType[]>([]);
    const [keyword, setKeyword] = useState("");

    const calcRefItemList = async () => {
        const localBranchList = store.localBranchList.map(branch => ({
            label: `${branch.name}${(store.headInfo?.detached == false && store.headInfo.branch_name == branch.name) ? "(HEAD)" : ""}`,
            title: branch.name,
            key: branch.commit_id,
            style: { fontWeight: 400 },
        }));
        const tagList = store.tagList.map(tag => ({
            label: `${tag.name}${(store.headInfo?.detached == true && store.headInfo.tag_name == tag.name) ? "(HEAD)" : ""}`,
            key: tag.commit_id,
            style: { fontWeight: 400 },
        }));
        const remoteList = store.remoteList.map(remote => {
            const branchList = store.remoteBranchList.filter(branch => branch.name.startsWith(`${remote.name}/`)).map(branch => ({
                label: branch.name.substring(`${remote.name}/`.length),
                title: branch.name.substring(`${remote.name}/`.length),
                key: branch.commit_id,
                style: { fontWeight: 400 },
            }))
            return {
                label: remote.name,
                key: `remote/${remote.name}`,
                children: branchList,
                style: { fontWeight: 400 },
            };
        })
        setRefItemList([
            {
                label: "分支",
                key: "localBranchList",
                icon: <BranchesOutlined style={{ fontSize: "16px" }} />,
                children: localBranchList,
                style: { fontSize: "16px", fontWeight: 700 },
            },
            {
                label: "标签",
                key: "tagList",
                icon: <TagOutlined style={{ fontSize: "16px" }} />,
                children: tagList,
                style: { fontSize: "16px", fontWeight: 700 },
            },
            {
                label: "远程",
                key: "remoteList",
                icon: <CloudOutlined style={{ fontSize: "16px" }} />,
                children: remoteList,
                style: { fontSize: "16px", fontWeight: 700 },
            }
        ]);
    };

    const hasRemoteIssue = () => {
        if (store.userToken == "") {
            return false;
        }
        if (store.userType == USER_TYPE_INTERNAL) {
            return false;
        }
        const remoteInfo = store.remoteList.find(item => item.name == "origin");
        if (remoteInfo == undefined) {
            return false;
        }
        if (store.userType == USER_TYPE_ATOM_GIT) {
            if (remoteInfo.url.startsWith("https://atomgit.com/")) {
                return true;
            }
            if (remoteInfo.url.includes("@atomgit.com:")) {
                return true;
            }
        } else if (store.userType == USER_TYPE_GITEE) {
            if (remoteInfo.url.startsWith("https://gitee.com/")) {
                return true;
            }
            if (remoteInfo.url.includes("@gitee.com:")) {
                return true;
            }
        } else if (store.userType == USER_TYPE_GIT_CODE) {
            if (remoteInfo.url.startsWith("https://gitcode.com/")) {
                return true;
            }
            if (remoteInfo.url.includes("@gitcode.com:")) {
                return true;
            }
        }
        return false;
    };

    useEffect(() => {
        calcRefItemList();
    }, [store.localBranchList, store.remoteBranchList, store.headInfo, store.tagList, store.remoteList, keyword]);

    return (
        <div>
            <Menu mode="inline" selectedKeys={[props.curMenuValue]}
                items={[
                    {
                        label: "工作目录",
                        key: "workDir",
                        style: { fontSize: "16px", fontWeight: 700 },
                    },
                    {
                        label: (
                            <Badge count={store.statusList.length} overflowCount={99} offset={[20, 8]} size="small" style={{ padding: "0px 6px" }}>
                                <span style={{ fontSize: "16px", fontWeight: 700, color: store.statusList.length == 0 ? "grey" : "black" }}>未提交文件</span>
                            </Badge>
                        ),
                        key: "fileStatus",
                        style: { fontSize: "16px", fontWeight: 700 },
                    },
                    {
                        label: (
                            <Badge count={store.stashList.length} overflowCount={99} offset={[20, 8]} size="small" style={{ padding: "0px 6px" }}>
                                <span style={{ fontSize: "16px", fontWeight: 700 }}>贮藏列表</span>
                            </Badge>
                        ),
                        key: "stashList",
                        style: { fontSize: "16px", fontWeight: 700 },
                    },
                    hasRemoteIssue() ? {
                        label: "远程工单列表",
                        key: "issueList",
                        style: { fontSize: "16px", fontWeight: 700 },
                    } : null,
                ]}
                onSelect={info => {
                    if (info.selectedKeys.length > 0) {
                        props.onMenuChange(info.selectedKeys[0]);
                    }
                }} />
            <Divider style={{ margin: "4px 0px" }} />
            <Space>
                <Input bordered placeholder="搜索" suffix={<SearchOutlined />} style={{ marginBottom: "4px", width: "200px", marginLeft: "8px" }}
                    value={keyword} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setKeyword(e.target.value.trim());
                    }} />
                <Button type="link" icon={<ReloadOutlined style={{ fontSize: "20px" }} title="刷新" onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    const f = async () => {
                        await store.loadHeadInfo();
                        await store.loadBranchList();
                        await store.loadTagList();
                        await store.loadRemoteList();
                        await store.loadStashList();
                        await store.loadStatusList();
                        message.info("已刷新分支/标签/远程信息");
                    };
                    f();
                }} />} />
            </Space>
            <Menu mode="inline" selectedKeys={[props.curMenuValue]} items={refItemList} defaultOpenKeys={["localBranchList"]}
                style={{ height: "calc(100vh - 180px)", overflowY: "scroll", overflowX: "hidden" }}
                onSelect={info => {
                    if (info.selectedKeys.length > 0) {
                        props.onMenuChange(info.selectedKeys[0]);
                    }
                }} />
        </div>
    )
};

export default observer(MainMenu);