//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { useGitStores } from "./store";
import { Layout } from "antd";
import MainMenu from "./MainMenu";
import WorkDir from "./components/WorkDir";
import CommitGraph from "./components/CommitGraph";
import FileStatus from "./components/FileStatus";
import StashList from "./components/StashList";
import { USER_TYPE } from "@/api/user";
import RemoteIssueList from "./components/RemoteIssueList";

const GitProApp = () => {
    const store = useGitStores();

    const location = useLocation();
    const urlParams = new URLSearchParams(location.search);
    const repoId = urlParams.get("id") ?? "";
    const userName = urlParams.get("username") ?? "";
    const userType = parseInt(urlParams.get("usertype") ?? "0") as USER_TYPE;
    const userToken = urlParams.get("token") ?? "";

    const [curMenuValue, setCurMenuValue] = useState("workDir");


    useEffect(() => {
        store.initRepo(repoId, userName, userType, userToken);
    }, []);

    return (
        <Layout>
            <Layout.Sider style={{ height: "100vh" }} theme="light" width={250}>
                <MainMenu curMenuValue={curMenuValue} onMenuChange={value => setCurMenuValue(value)} />
            </Layout.Sider>
            <Layout.Content>
                {curMenuValue == "workDir" && (
                    <WorkDir />
                )}
                {curMenuValue == "fileStatus" && (
                    <FileStatus />
                )}
                {curMenuValue == "stashList" && (
                    <StashList />
                )}
                {curMenuValue == "issueList" && (
                    <RemoteIssueList />
                )}
                {["workDir", "fileStatus", "stashList"].includes(curMenuValue) == false && (
                    <CommitGraph refCommitId={curMenuValue} />
                )}
            </Layout.Content>
        </Layout>
    );
};

export default GitProApp;