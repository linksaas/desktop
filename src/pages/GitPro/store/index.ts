//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from "react";
import { GitStore } from "./git";

const stores = React.createContext(new GitStore());

export const useGitStores = () => React.useContext(stores);

export default stores;