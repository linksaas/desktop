//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import { NodeEditPanelProps } from './common';
import { useRoadmapStores } from '../../store';
import { Compact as CompactPicker } from '@uiw/react-color';
import { update_node_data } from "@/api/roadmap_content";
import { request } from '@/utils/request';
import { Button, Form, message, Popover, Space } from 'antd';
import { useCommonEditor } from '@/components/Editor';
import { FILE_OWNER_TYPE_ROADMAP, GLOBAL_GROW_CENTER_FS_ID } from '@/api/fs';
import { DeleteOutlined } from '@ant-design/icons';

const RichTextNodeEditPanel = (props: NodeEditPanelProps) => {
    const roadmapStore = useRoadmapStores();

    const [bgColor, setBgColor] = useState("white");

    const { editor, editorRef } = useCommonEditor({
        content: "",
        fsId: GLOBAL_GROW_CENTER_FS_ID,
        ownerType: FILE_OWNER_TYPE_ROADMAP,
        ownerId: roadmapStore.roadmapId,
        historyInToolbar: false,
        clipboardInToolbar: false,
        commonInToolbar: true,
        pubResInToolbar: true,
    });

    const reset = () => {
        const curNode = roadmapStore.getNode(props.nodeId);
        if (curNode == undefined) {
            return;
        }
        editorRef.current?.setContent(curNode.basic_info.node_data.RichTextData?.content ?? "");
        setBgColor(curNode.basic_info.node_data.RichTextData?.bg_color ?? "");
    };

    const updateNode = async (close: boolean) => {
        const content = editorRef.current?.getContent() ?? { type: "doc" };
        await request(update_node_data({
            session_id: roadmapStore.sessionId,
            roadmap_id: roadmapStore.roadmapId,
            node_id: props.nodeId,
            node_data: {
                RichTextData: {
                    content: JSON.stringify(content),
                    bg_color: bgColor,
                }
            },
        }));
        await roadmapStore.onUpdateNode(props.nodeId);
        await reset();
        message.info("更新成功");
        if (close) {
            roadmapStore.selectNodeId = "";
        }
    };

    useEffect(() => {
        if (editorRef.current != undefined) {
            reset();
        }
    }, [editorRef.current]);

    return (
        <div>
            <Button type='primary' danger
                style={{ position: "fixed", top: "10px", right: "20px" }} icon={<DeleteOutlined />}
                onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    roadmapStore.removeNode(props.nodeId);
                }}>删除</Button>
            <div className='_roadmapContext' style={{ backgroundColor: bgColor }}>
                {editor}
            </div>
            <Form labelCol={{ span: 4 }} style={{ marginTop: "10px" }}>
                <Form.Item label="背景颜色">
                    <Popover trigger="click" placement="bottom" content={
                        <CompactPicker color={bgColor} onChange={value => {
                            setBgColor(value.hex);
                        }} />
                    }>
                        <div style={{ backgroundColor: bgColor, width: "20px", height: "20px", cursor: "pointer", border: "1px solid grey" }} />
                    </Popover>
                </Form.Item>
                <Form.Item style={{ display: "flex", justifyContent: "flex-end", marginRight: "20px" }}>
                    <Space>
                        <Button type="default" onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            reset();
                        }}>重置</Button>
                        <Button type='primary' onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            updateNode(false);
                        }}>更新</Button>
                        <Button type='primary' onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            updateNode(true);
                        }}>更新并关闭</Button>
                    </Space>
                </Form.Item>
            </Form>
        </div>
    );
};

export default observer(RichTextNodeEditPanel);