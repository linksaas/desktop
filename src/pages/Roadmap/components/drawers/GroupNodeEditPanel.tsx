//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import { NodeEditPanelProps } from './common';
import { Button, Form, InputNumber, message, Popover, Space } from 'antd';
import { useRoadmapStores } from '../../store';
import { Compact as CompactPicker } from '@uiw/react-color';
import { update_node_data } from "@/api/roadmap_content";
import { request } from '@/utils/request';
import { DeleteOutlined } from '@ant-design/icons';

const GroupNodeEditPanel = (props: NodeEditPanelProps) => {
    const roadmapStore = useRoadmapStores();

    const [bgColor, setBgColor] = useState("white");
    const [borderColor, setBorderColor] = useState("black");
    const [borderRadius, setBorderRadius] = useState(0);
    const [hasChange, setHasChange] = useState(false);

    const reset = () => {
        const curNode = roadmapStore.getNode(props.nodeId);
        if (curNode == undefined) {
            return;
        }

        setBgColor(curNode.basic_info.node_data.GroupData?.bg_color ?? "white");
        setBorderColor(curNode.basic_info.node_data.GroupData?.border_color ?? "black");
        setBorderRadius(curNode.basic_info.node_data.GroupData?.border_radius ?? 0);
        setHasChange(false);
    };

    const updateNode = async (close:boolean) => {
        await request(update_node_data({
            session_id: roadmapStore.sessionId,
            roadmap_id: roadmapStore.roadmapId,
            node_id: props.nodeId,
            node_data: {
                GroupData: {
                    bg_color: bgColor,
                    border_color: borderColor,
                    border_radius: borderRadius,
                },
            },
        }));
        await roadmapStore.onUpdateNode(props.nodeId);
        await reset();
        message.info("更新成功");
        if(close){
            roadmapStore.selectNodeId = "";
        }
    };

    useEffect(() => {
        reset();
    }, []);

    return (
        <div>
            <Button type='primary' danger
                style={{ position: "fixed", top: "10px", right: "20px" }} icon={<DeleteOutlined />}
                onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    roadmapStore.removeNode(props.nodeId);
                }}>删除</Button>
            <Form labelCol={{ span: 4 }}>
                <Form.Item label="背景颜色">
                    <Popover trigger="click" placement="bottom" content={
                        <CompactPicker color={bgColor} onChange={value => {
                            setBgColor(value.hex);
                            setHasChange(true);
                        }} />
                    }>
                        <div style={{ backgroundColor: bgColor, width: "20px", height: "20px", cursor: "pointer", border: "1px solid grey" }} />
                    </Popover>
                </Form.Item>
                <Form.Item label="边框颜色">
                    <Popover trigger="click" placement="bottom" content={
                        <CompactPicker color={borderColor} onChange={value => {
                            setBorderColor(value.hex);
                            setHasChange(true);
                        }} />
                    }>
                        <div style={{ backgroundColor: borderColor, width: "20px", height: "20px", cursor: "pointer", border: "1px solid grey" }} />
                    </Popover>
                </Form.Item>
                <Form.Item label="边框圆角">
                    <InputNumber value={borderRadius} min={0} max={100} controls={false} precision={0}
                        onChange={value => {
                            if (value != null) {
                                setBorderRadius(Math.round(value));
                                setHasChange(true);
                            }
                        }} />
                </Form.Item>
                <Form.Item style={{ display: "flex", justifyContent: "flex-end", marginRight: "20px" }}>
                    <Space>
                        <Button type="default" disabled={!hasChange} onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            reset();
                        }}>重置</Button>
                        <Button type='primary' disabled={!hasChange} onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            updateNode(false);
                        }}>更新</Button>
                        <Button type='primary' disabled={!hasChange} onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            updateNode(true);
                        }}>更新并关闭</Button>
                    </Space>
                </Form.Item>
            </Form>
        </div>
    );
};

export default observer(GroupNodeEditPanel);