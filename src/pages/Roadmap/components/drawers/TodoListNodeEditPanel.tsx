//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import { NodeEditPanelProps } from './common';
import { useRoadmapStores } from '../../store';
import { request } from '@/utils/request';
import type { NodeTodoItem } from '@/api/roadmap_content';
import { update_node_data } from '@/api/roadmap_content';
import { Button, Form, Input, List, message, Popover, Space } from 'antd';
import { Compact as CompactPicker } from '@uiw/react-color';
import { DeleteOutlined, MinusCircleOutlined, PlusCircleOutlined } from '@ant-design/icons';
import { uniqId } from '@/utils/utils';

const TodoListNodeEditPanel = (props: NodeEditPanelProps) => {
    const roadmapStore = useRoadmapStores();

    const [itemList, setItemList] = useState<NodeTodoItem[]>([]);
    const [bgColor, setBgColor] = useState("white");
    const [hasChange, setHasChange] = useState(false);

    const reset = () => {
        const curNode = roadmapStore.getNode(props.nodeId);
        if (curNode == undefined) {
            return;
        }
        setItemList((curNode.basic_info.node_data.TodoListData?.item_list ?? []).slice());
        setBgColor(curNode.basic_info.node_data.TodoListData?.bg_color ?? "white");
        setHasChange(false);
    };

    const updateNode = async (close: boolean) => {
        await request(update_node_data({
            session_id: roadmapStore.sessionId,
            roadmap_id: roadmapStore.roadmapId,
            node_id: props.nodeId,
            node_data: {
                TodoListData: {
                    item_list: itemList,
                    bg_color: bgColor,
                },
            },
        }));
        await roadmapStore.onUpdateNode(props.nodeId);
        await reset();
        message.info("更新成功");
        if(close){
            roadmapStore.selectNodeId = "";
        }
    };

    useEffect(() => {
        reset();
    }, []);


    return (
        <div>
            <Button type='primary' danger
                style={{ position: "fixed", top: "10px", right: "20px" }} icon={<DeleteOutlined />}
                onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    roadmapStore.removeNode(props.nodeId);
                }}>删除</Button>
            <List rowKey="todo_id" dataSource={itemList} pagination={false} style={{ maxHeight: "50vh" }}
                renderItem={item => (
                    <List.Item extra={
                        <Space size="small">
                            <Button type="link" icon={<PlusCircleOutlined />} style={{ minWidth: 0, padding: "0px 0px" }}
                                onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    const tmpList = itemList.slice();
                                    const index = itemList.findIndex(tmpItem => tmpItem.todo_id == item.todo_id);
                                    if (index != -1) {
                                        tmpList.splice(index + 1, 0, {
                                            todo_id: uniqId(),
                                            title: "",
                                        });
                                        setItemList(tmpList);
                                    }
                                    setHasChange(true);
                                }} />
                            <Button type="link" icon={<MinusCircleOutlined />} style={{ minWidth: 0, padding: "0px 0px" }} disabled={itemList.length <= 1}
                                onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    const tmpList = itemList.filter(tmpItem => tmpItem.todo_id != item.todo_id);
                                    setItemList(tmpList);
                                    setHasChange(true);
                                }} />
                        </Space>
                    }>
                        <Input value={item.title} onChange={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            const tmpList = itemList.slice();
                            const index = tmpList.findIndex(tmpItem => tmpItem.todo_id == item.todo_id);
                            if (index != -1) {
                                tmpList[index].title = e.target.value.trim();
                                setItemList(tmpList);
                            }
                            setHasChange(true);
                        }} />
                    </List.Item>
                )} />
            <Form labelCol={{ span: 4 }}>
                <Form.Item label="背景颜色">
                    <Popover trigger="click" placement="bottom" content={
                        <CompactPicker color={bgColor} onChange={value => {
                            setBgColor(value.hex);
                            setHasChange(true);
                        }} />
                    }>
                        <div style={{ backgroundColor: bgColor, width: "20px", height: "20px", cursor: "pointer", border: "1px solid grey" }} />
                    </Popover>
                </Form.Item>
                <Form.Item style={{ display: "flex", justifyContent: "flex-end", marginRight: "20px" }}>
                    <Space>
                        <Button type="default" disabled={!hasChange} onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            reset();
                        }}>重置</Button>
                        <Button type='primary' disabled={!hasChange} onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            updateNode(false);
                        }}>更新</Button>
                        <Button type='primary' disabled={!hasChange} onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            updateNode(true);
                        }}>更新并关闭</Button>
                    </Space>
                </Form.Item>
            </Form>

        </div>
    );
}

export default observer(TodoListNodeEditPanel);