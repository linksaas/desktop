//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import { NodeEditPanelProps } from './common';
import { Button, Form, Input, message, Popover, Select, Space } from 'antd';
import { useRoadmapStores } from '../../store';
import { Compact as CompactPicker } from '@uiw/react-color';
import { update_node_data } from "@/api/roadmap_content";
import { request } from '@/utils/request';
import { DeleteOutlined } from '@ant-design/icons';


const LabelNodeEditPanel = (props: NodeEditPanelProps) => {
    const roadmapStore = useRoadmapStores();

    const [content, setContent] = useState("");
    const [fontSize, setFontSize] = useState(14);
    const [fontWeight, setFontWeight] = useState(500);
    const [fontColor, setFontColor] = useState("black");
    const [bgColor, setBgColor] = useState("white");
    const [hasChange, setHasChange] = useState(false);

    const reset = () => {
        const curNode = roadmapStore.getNode(props.nodeId);
        if (curNode == undefined) {
            return;
        }
        setContent(curNode.basic_info.node_data.TextData?.content ?? "");
        setFontSize(curNode.basic_info.node_data.TextData?.font_size ?? 14);
        setFontWeight(curNode.basic_info.node_data.TextData?.font_weight ?? 500);
        setFontColor(curNode.basic_info.node_data.TextData?.font_color ?? "black");
        setBgColor(curNode.basic_info.node_data.TextData?.bg_color ?? "white");
        setHasChange(false);
    };

    const updateNode = async (close: boolean) => {
        await request(update_node_data({
            session_id: roadmapStore.sessionId,
            roadmap_id: roadmapStore.roadmapId,
            node_id: props.nodeId,
            node_data: {
                TextData: {
                    font_size: fontSize,
                    font_weight: fontWeight,
                    font_color: fontColor,
                    content: content,
                    bg_color: bgColor,
                },
            },
        }));
        await roadmapStore.onUpdateNode(props.nodeId);
        await reset();
        message.info("更新成功");
        if(close){
            roadmapStore.selectNodeId = "";
        }
    };

    useEffect(() => {
        reset();
    }, []);

    return (
        <div>
            <Button type='primary' danger
                style={{ position: "fixed", top: "10px", right: "20px" }} icon={<DeleteOutlined />}
                onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    roadmapStore.removeNode(props.nodeId);
                }}>删除</Button>
            <Form labelCol={{ span: 4 }}>
                <Form.Item label="内容">
                    <Input value={content} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setContent(e.target.value.trim());
                        setHasChange(true);
                    }} />
                </Form.Item>
                <Form.Item label="字体大小">
                    <Select value={fontSize} onChange={value => {
                        setFontSize(value);
                        setHasChange(true);
                    }}>
                        {[10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50].map(item => (
                            <Select.Option key={item} value={item}>{item}</Select.Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item label="字体颜色">
                    <Popover trigger="click" placement="bottom" content={
                        <CompactPicker color={fontColor} onChange={value => {
                            setFontColor(value.hex);
                            setHasChange(true);
                        }} />
                    }>
                        <div style={{ backgroundColor: fontColor, width: "20px", height: "20px", cursor: "pointer", border: "1px solid grey" }} />
                    </Popover>
                </Form.Item>
                <Form.Item label="字体权重">
                    <Select value={fontWeight} onChange={value => {
                        setFontWeight(value);
                        setHasChange(true);
                    }}>
                        {[100, 200, 300, 400, 500, 600, 700, 800, 900].map(item => (
                            <Select.Option key={item} value={item}>{item}</Select.Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item label="背景颜色">
                    <Popover trigger="click" placement="bottom" content={
                        <CompactPicker color={bgColor} onChange={value => {
                            setBgColor(value.hex);
                            setHasChange(true);
                        }} />
                    }>
                        <div style={{ backgroundColor: bgColor, width: "20px", height: "20px", cursor: "pointer", border: "1px solid grey" }} />
                    </Popover>
                </Form.Item>
                <Form.Item style={{ display: "flex", justifyContent: "flex-end", marginRight: "20px" }}>
                    <Space>
                        <Button type="default" disabled={!hasChange} onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            reset();
                        }}>重置</Button>
                        <Button type='primary' disabled={!hasChange} onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            updateNode(false);
                        }}>更新</Button>
                        <Button type='primary' disabled={!hasChange} onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            updateNode(true);
                        }}>更新并关闭</Button>
                    </Space>
                </Form.Item>
            </Form>
        </div>
    );
};

export default observer(LabelNodeEditPanel); 