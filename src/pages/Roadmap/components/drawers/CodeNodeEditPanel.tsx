//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import { NodeEditPanelProps } from './common';
import { Button, Form, message, Select, Space } from 'antd';
import { useRoadmapStores } from '../../store';
import { DeleteOutlined } from '@ant-design/icons';
import { update_node_data } from '@/api/roadmap_content';
import { request } from '@/utils/request';
import { LANG_LIST } from '@/utils/constant';
import CodeEditor from '@uiw/react-textarea-code-editor';

const CodeNodeEditPanel = (props: NodeEditPanelProps) => {
    const roadmapStore = useRoadmapStores();

    const [lang, setLang] = useState("");
    const [code, setCode] = useState("");
    const [hasChange, setHasChange] = useState(false);

    const reset = () => {
        const curNode = roadmapStore.getNode(props.nodeId);
        if (curNode == undefined) {
            return;
        }
        setLang(curNode.basic_info.node_data.CodeData?.lang ?? "");
        setCode(curNode.basic_info.node_data.CodeData?.code ?? "");
        setHasChange(false);
    };

    const updateNode = async (close: boolean) => {
        await request(update_node_data({
            session_id: roadmapStore.sessionId,
            roadmap_id: roadmapStore.roadmapId,
            node_id: props.nodeId,
            node_data: {
                CodeData: {
                    lang,
                    code,
                }
            },
        }));
        await roadmapStore.onUpdateNode(props.nodeId);
        await reset();
        message.info("更新成功");
        if (close) {
            roadmapStore.selectNodeId = "";
        }
    };

    useEffect(() => {
        reset();
    }, []);

    return (
        <div>
            <Button type='primary' danger
                style={{ position: "fixed", top: "10px", right: "20px" }} icon={<DeleteOutlined />}
                onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    roadmapStore.removeNode(props.nodeId);
                }}>删除</Button>
            <Form labelCol={{ span: 4 }}>
                <Form.Item label="编程语言">
                    <Select value={lang} onChange={value => setLang(value)} showSearch>
                        {LANG_LIST.map(langName => (
                            <Select.Option key={langName} value={langName}>{langName}</Select.Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item>
                    <div style={{ height: "50vh", overflowY: "scroll" }}>
                        <CodeEditor
                            value={code}
                            language={lang}
                            placeholder='请输入代码'
                            style={{
                                fontSize: 14,
                                backgroundColor: '#f5f5f5',
                                minHeight: "50vh",
                            }}
                            onChange={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setHasChange(true);
                                setCode(e.target.value);
                            }}
                        />
                    </div>
                </Form.Item>
                <Form.Item style={{ display: "flex", justifyContent: "flex-end", marginRight: "20px" }}>
                    <Space>
                        <Button type="default" onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            reset();
                        }} disabled={!hasChange}>重置</Button>
                        <Button type='primary' onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            updateNode(false);
                        }} disabled={!hasChange}>更新</Button>
                        <Button type='primary' onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            updateNode(true);
                        }} disabled={!hasChange}>更新并关闭</Button>
                    </Space>
                </Form.Item>
            </Form>
        </div>
    );
};

export default observer(CodeNodeEditPanel);