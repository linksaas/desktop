//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import type { EdgeKey } from '@/api/roadmap_content';
import { EDGE_BEZIER, EDGE_SMOOTH_STEP, EDGE_STEP, EDGE_STRAIGHT, update_edge } from '@/api/roadmap_content';
import { useRoadmapStores } from '../../store';
import { request } from '@/utils/request';
import { Button, Form, InputNumber, message, Popover, Select, Space, Switch } from 'antd';
import { Compact as CompactPicker } from '@uiw/react-color';
import { DeleteOutlined } from '@ant-design/icons';

export interface EdgeEditPanelProps {
    edgeKey: EdgeKey;
}

const EdgeEditPanel = (props: EdgeEditPanelProps) => {
    const roadmapStore = useRoadmapStores();

    const [edgeType, setEdgeType] = useState(EDGE_BEZIER);
    const [edgeColor, setEdgeColor] = useState("black");
    const [hasBeginArrow, setHasBeginArrow] = useState(false);
    const [hasEndArrow, setHasEndArrow] = useState(false);
    const [edgeWidth, setEdgeWidth] = useState(1);
    const [hasChange, setHasChange] = useState(false);

    const reset = () => {
        const curEdge = roadmapStore.getEdge(props.edgeKey);
        if (curEdge == undefined) {
            return;
        }
        setEdgeType(curEdge.basic_info.edge_type);
        setEdgeColor(curEdge.basic_info.edge_color);
        setHasBeginArrow(curEdge.basic_info.has_begin_arrow);
        setHasEndArrow(curEdge.basic_info.has_end_arrow);
        setEdgeWidth(curEdge.basic_info.edge_width);
        setHasChange(false);
    };

    const updateEdge = async (close: boolean) => {
        await request(update_edge({
            session_id: roadmapStore.sessionId,
            edge_key: props.edgeKey,
            basic_info: {
                edge_type: edgeType,
                edge_color: edgeColor,
                has_begin_arrow: hasBeginArrow,
                has_end_arrow: hasEndArrow,
                edge_width: edgeWidth,
            },
        }));
        await roadmapStore.onUpdateEdge(props.edgeKey);
        await reset();
        message.info("更新成功");
        if (close) {
            roadmapStore.selectEdgeKey = null;
        }
    };

    useEffect(() => {
        reset();
    }, []);

    return (
        <div>
            <Button type='primary' danger
                style={{ position: "fixed", top: "10px", right: "20px" }} icon={<DeleteOutlined />}
                onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    roadmapStore.removeEdge(props.edgeKey);
                }}>删除</Button>
            <Form labelCol={{ span: 4 }}>
                <Form.Item label="连线类型">
                    <Select value={edgeType} onChange={value => {
                        setEdgeType(value);
                        setHasChange(true);
                    }}>
                        <Select.Option value={EDGE_BEZIER}>贝塞尔曲线</Select.Option>
                        <Select.Option value={EDGE_STRAIGHT}>直线</Select.Option>
                        <Select.Option value={EDGE_STEP}>折线</Select.Option>
                        <Select.Option value={EDGE_SMOOTH_STEP}>平滑折线</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item label="连线颜色">
                    <Popover trigger="click" placement="bottom" content={
                        <CompactPicker color={edgeColor} onChange={value => {
                            setEdgeColor(value.hex);
                            setHasChange(true);
                        }} />
                    }>
                        <div style={{ backgroundColor: edgeColor, width: "20px", height: "20px", cursor: "pointer", border: "1px solid grey" }} />
                    </Popover>
                </Form.Item>
                <Form.Item label="起始箭头">
                    <Switch checked={hasBeginArrow} onChange={checked => {
                        setHasBeginArrow(checked);
                        setHasChange(true);
                    }} size='small' />
                </Form.Item>
                <Form.Item label="结束箭头">
                    <Switch checked={hasEndArrow} onChange={checked => {
                        setHasEndArrow(checked);
                        setHasChange(true);
                    }} size='small' />
                </Form.Item>
                <Form.Item label="连线宽度">
                    <InputNumber value={edgeWidth} min={1} max={20} controls={false} precision={0} onChange={value => {
                        if (value != null) {
                            setEdgeWidth(Math.round(value));
                            setHasChange(true);
                        }
                    }} />
                </Form.Item>
                <Form.Item style={{ display: "flex", justifyContent: "flex-end", marginRight: "20px" }}>
                    <Space>
                        <Button type="default" disabled={!hasChange} onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            reset();
                        }}>重置</Button>
                        <Button type='primary' disabled={!hasChange} onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            updateEdge(false);
                        }}>更新</Button>
                        <Button type='primary' disabled={!hasChange} onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            updateEdge(true);
                        }}>更新并关闭</Button>
                    </Space>
                </Form.Item>
            </Form>
        </div>
    );
};

export default observer(EdgeEditPanel);