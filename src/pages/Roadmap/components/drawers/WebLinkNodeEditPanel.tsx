//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import { NodeEditPanelProps } from './common';
import { Button, Form, Input, message, Space } from 'antd';
import { useRoadmapStores } from '../../store';
import { DeleteOutlined } from '@ant-design/icons';
import { update_node_data } from '@/api/roadmap_content';
import { request } from '@/utils/request';

const WebLinkNodeEditPanel = (props: NodeEditPanelProps) => {
    const roadmapStore = useRoadmapStores();

    const [linkName, setLinkName] = useState("");
    const [linkUrl, setLinkUrl] = useState("");
    const [hasChange, setHasChange] = useState(false);

    const reset = () => {
        const curNode = roadmapStore.getNode(props.nodeId);
        if (curNode == undefined) {
            return;
        }
        setLinkName(curNode.basic_info.node_data.WebLinkData?.link_name ?? "");
        setLinkUrl(curNode.basic_info.node_data.WebLinkData?.link_url ?? "");
        setHasChange(false);
    };

    const updateNode = async (close: boolean) => {
        await request(update_node_data({
            session_id: roadmapStore.sessionId,
            roadmap_id: roadmapStore.roadmapId,
            node_id: props.nodeId,
            node_data: {
                WebLinkData: {
                    link_name: linkName,
                    link_url: linkUrl,
                }
            },
        }));
        await roadmapStore.onUpdateNode(props.nodeId);
        await reset();
        message.info("更新成功");
        if (close) {
            roadmapStore.selectNodeId = "";
        }
    };

    useEffect(() => {
        reset();
    }, []);

    return (
        <div>
            <Button type='primary' danger
                style={{ position: "fixed", top: "10px", right: "20px" }} icon={<DeleteOutlined />}
                onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    roadmapStore.removeNode(props.nodeId);
                }}>删除</Button>
            <Form labelCol={{ span: 4 }}>
                <Form.Item label="网页名称">
                    <Input value={linkName} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setHasChange(true);
                        setLinkName(e.target.value.trim());
                    }} />
                </Form.Item>
                <Form.Item label="网页地址">
                    <Input value={linkUrl} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setHasChange(true);
                        setLinkUrl(e.target.value.trim());
                    }} />
                </Form.Item>
                <Form.Item style={{ display: "flex", justifyContent: "flex-end", marginRight: "20px" }}>
                    <Space>
                        <Button type="default" onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            reset();
                        }} disabled={!hasChange}>重置</Button>
                        <Button type='primary' onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            updateNode(false);
                        }} disabled={!hasChange}>更新</Button>
                        <Button type='primary' onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            updateNode(true);
                        }} disabled={!hasChange}>更新并关闭</Button>
                    </Space>
                </Form.Item>
            </Form>
        </div>

    );
};

export default observer(WebLinkNodeEditPanel);