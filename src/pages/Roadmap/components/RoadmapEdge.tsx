//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { observer } from 'mobx-react';
import { EDGE_BEZIER, EDGE_SMOOTH_STEP, EDGE_STEP, EDGE_STRAIGHT, NODE_SUB_TOPIC, type EdgeInfo } from "@/api/roadmap_content";
import { BaseEdge, getBezierPath, getStraightPath, getSmoothStepPath, type EdgeProps, EdgeLabelRenderer } from 'reactflow';
import { useRoadmapStores } from "../store";
import { Button } from "antd";
import { EditOutlined } from "@ant-design/icons";



const RoadmapEdge = (props: EdgeProps<EdgeInfo>) => {
    const roadmapStore = useRoadmapStores();

    const [edgePath, setEdgePath] = useState("");
    const [labelX, setLabelX] = useState(0);
    const [labelY, setLabelY] = useState(0);

    const calcEdgePath = () => {
        if (props.data?.basic_info.edge_type == EDGE_BEZIER) {
            const result = getBezierPath({
                sourceX: props.sourceX,
                sourceY: props.sourceY,
                sourcePosition: props.sourcePosition,
                targetX: props.targetX,
                targetY: props.targetY,
                targetPosition: props.targetPosition,
            });
            setEdgePath(result[0]);
            setLabelX(result[1]);
            setLabelY(result[2]);
        } else if (props.data?.basic_info.edge_type == EDGE_STRAIGHT) {
            const result = getStraightPath({
                sourceX: props.sourceX,
                sourceY: props.sourceY,
                targetX: props.targetX,
                targetY: props.targetY,
            });
            setEdgePath(result[0]);
            setLabelX(result[1]);
            setLabelY(result[2]);
        } else if (props.data?.basic_info.edge_type == EDGE_STEP) {
            const result = getSmoothStepPath({
                sourceX: props.sourceX,
                sourceY: props.sourceY,
                sourcePosition: props.sourcePosition,
                targetX: props.targetX,
                targetY: props.targetY,
                targetPosition: props.targetPosition,
                borderRadius: 0,
                offset: 0,
            });
            setEdgePath(result[0]);
            setLabelX(result[1]);
            setLabelY(result[2]);
        } else if (props.data?.basic_info.edge_type == EDGE_SMOOTH_STEP) {
            const result = getSmoothStepPath({
                sourceX: props.sourceX,
                sourceY: props.sourceY,
                sourcePosition: props.sourcePosition,
                targetX: props.targetX,
                targetY: props.targetY,
                targetPosition: props.targetPosition,
            });
            setEdgePath(result[0]);
            setLabelX(result[1]);
            setLabelY(result[2]);
        }
    };

    const getStrokeDasharray = () => {
        const fromNode = roadmapStore.getNode(props.data?.edge_key.from_node_id ?? "");
        const toNode = roadmapStore.getNode(props.data?.edge_key.to_node_id ?? "");
        if (fromNode?.basic_info.node_type == NODE_SUB_TOPIC || toNode?.basic_info.node_type == NODE_SUB_TOPIC) {
            return "3";
        } else {
            return "0";
        }
    }

    useEffect(() => {
        calcEdgePath();
    }, [props]);

    return (
        <>
            <BaseEdge path={edgePath} markerStart={props.markerStart} markerEnd={props.markerEnd}
                style={{ strokeWidth: props.data?.basic_info.edge_width ?? 2, stroke: props.data?.basic_info.edge_color ?? "black", strokeDasharray: getStrokeDasharray() }}
            />
            {roadmapStore.inEdit == true && (
                <EdgeLabelRenderer>
                    <div
                        style={{
                            position: 'absolute',
                            transform: `translate(-50%, -50%) translate(${labelX}px,${labelY}px)`,
                            fontSize: 14,
                            pointerEvents: 'all',
                            zIndex: 3000,
                        }}
                    >
                        <Button type="link" icon={<EditOutlined />} onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            if (props.data != undefined) {
                                roadmapStore.selectEdgeKey = props.data.edge_key;
                            }
                        }} style={{ minWidth: "0px", padding: "0px 0px", width: "20px" }} />
                    </div>
                </EdgeLabelRenderer>
            )}
        </>
    );
};

export default observer(RoadmapEdge);