//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from 'react';
import { observer } from 'mobx-react';
import { Button, Divider, message, Modal, Space } from 'antd';
import DragNode from './nodes/DragNode';
import { type NodeInfo, add_node } from '@/api/roadmap_content';
import { AlignLeftOutlined, BookOutlined, BorderOutlined, BulbOutlined, CodeOutlined, DeploymentUnitOutlined, GroupOutlined, ImportOutlined, LinkOutlined, OrderedListOutlined, TagOutlined } from '@ant-design/icons';
import CodeEditor from '@uiw/react-textarea-code-editor';
import { request } from '@/utils/request';
import { useRoadmapStores } from '../store';
import BatchAddNodeModal from './BatchAddNodeModal';
import { useHotkeys } from 'react-hotkeys-hook';
import { CodeDargNodeInfo, GroupDargNodeInfo, LabelDargNodeInfo, RichTextDargNodeInfo, RoadmapRefDargNodeInfo, SubTopicDargNodeInfo, TodoListDargNodeInfo, TopicDargNodeInfo, WebLinkDargNodeInfo } from './nodes/contants';
import { addCodeNode, addGroupNode, addLabelNode, addRichTextNode, addRoadmapRefNode, addSubTopicNode, addTodoListNode, addTopicNode, addWebLinkNode } from './nodes/utils';

interface ImportModalProps {
    onClose: () => void;
}

const ImportModal = observer((props: ImportModalProps) => {
    const roadmapStore = useRoadmapStores();

    const [data, setData] = useState("[]");

    const runImport = async () => {
        const newNodeIdList: string[] = [];
        try {
            const tmpNodeList = JSON.parse(data) as NodeInfo[];
            for (const tmpNode of tmpNodeList) {
                const res = await request(add_node({
                    session_id: roadmapStore.sessionId,
                    roadmap_id: roadmapStore.roadmapId,
                    basic_info: tmpNode.basic_info,
                }));
                await roadmapStore.onUpdateNode(res.node_id);
                newNodeIdList.push(res.node_id);
            }
            message.info("导入成功");
            props.onClose();
        } catch (e) {
            message.error(`$e`);
            return;
        }
        for (const nodeId of newNodeIdList) {
            roadmapStore.addBatchNodeId(nodeId);
        }
    };

    return (
        <Modal open title="导入节点" mask={false}
            okText="导入" okButtonProps={{ disabled: data.length <= 2 }}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onClose();
            }}
            onOk={e => {
                e.stopPropagation();
                e.preventDefault();
                runImport();
            }}>
            <CodeEditor
                value={data}
                language="json"
                minHeight={200}
                placeholder="请输入代码"
                onChange={(e) => {
                    e.stopPropagation();
                    e.preventDefault();
                    setData(e.target.value);
                }}
                style={{
                    fontSize: 14,
                    backgroundColor: '#f5f5f5',
                    maxHeight: "50vh",
                    overflow: "scroll",
                }}
            />
        </Modal>
    );
});

const NodeListPanel = () => {
    const roadmapStore = useRoadmapStores();

    const [showImportModal, setShowImportModal] = useState(false);
    const [showBatchAddModal, setShowBatchAddModal] = useState(false);

    useHotkeys("ctrl+1", () => addTopicNode(roadmapStore));
    useHotkeys("ctrl+2", () => addSubTopicNode(roadmapStore));
    useHotkeys("ctrl+3", () => addRoadmapRefNode(roadmapStore));
    useHotkeys("ctrl+4", () => addLabelNode(roadmapStore));
    useHotkeys("ctrl+5", () => addRichTextNode(roadmapStore));
    useHotkeys("ctrl+6", () => addTodoListNode(roadmapStore));
    useHotkeys("ctrl+7", () => addWebLinkNode(roadmapStore));
    useHotkeys("ctrl+8", () => addCodeNode(roadmapStore));
    useHotkeys("ctrl+9", () => addGroupNode(roadmapStore));

    return (
        <div style={{ width: "180px", backgroundColor: "white", padding: "10px 10px" }}>
            <h1>组件(拖动到白板上)</h1>
            <Space direction='vertical'>
                <DragNode title="学习阶段(ctrl+1)" icon={<BookOutlined />} dragNodeInfo={TopicDargNodeInfo} />
                <DragNode title="知识点(ctrl+2)" icon={<BulbOutlined />} dragNodeInfo={SubTopicDargNodeInfo} />
                <DragNode title="引用路线图(ctrl+3)" icon={<DeploymentUnitOutlined />} dragNodeInfo={RoadmapRefDargNodeInfo} />
                <Divider style={{ margin: "4px 0px" }} />
                <DragNode title="标签(ctrl+4)" icon={<TagOutlined />} dragNodeInfo={LabelDargNodeInfo} />
                <DragNode title="文本段落(ctrl+5)" icon={<AlignLeftOutlined />} dragNodeInfo={RichTextDargNodeInfo} />
                <DragNode title="待办列表(ctrl+6)" icon={<OrderedListOutlined />} dragNodeInfo={TodoListDargNodeInfo} />
                <DragNode title="网页链接(ctrl+7)" icon={<LinkOutlined />} dragNodeInfo={WebLinkDargNodeInfo} />
                <DragNode title="代码(ctrl+8)" icon={<CodeOutlined />} dragNodeInfo={CodeDargNodeInfo} />
                <Divider style={{ margin: "4px 0px" }} />
                <DragNode title="边框(ctrl+9)" icon={<BorderOutlined />} dragNodeInfo={GroupDargNodeInfo} />
            </Space>
            <Divider />
            {roadmapStore.adminUser == true && (
                <Button type='primary' icon={<ImportOutlined />} style={{ width: "160px" }}
                    onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setShowImportModal(true);
                    }}>导入节点</Button>
            )}
            <Button type="primary" icon={<GroupOutlined />} style={{ width: "160px" }}
                onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    setShowBatchAddModal(true);
                }}>批量添加</Button>

            {showImportModal == true && (
                <ImportModal onClose={() => setShowImportModal(false)} />
            )}
            {showBatchAddModal == true && (
                <BatchAddNodeModal
                    onClose={() => setShowBatchAddModal(false)} />
            )}
        </div>);
};

export default observer(NodeListPanel);