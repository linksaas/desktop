//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from 'react';
import type { NodeProps } from "reactflow";
import { NodeInfo } from '@/api/roadmap_content';
import NodeWrap from './NodeWrap';
import { observer } from 'mobx-react';
import { useRoadmapStores } from '../../store';

const RoadmapRefNode = (props: NodeProps<NodeInfo>) => {
    const roadmapStore = useRoadmapStores();

    return (
        <NodeWrap hasHandle nodeData={props} style={{
            backgroundColor: "#4136D6",
            borderRadius: "10px",
        }}>
            <div className='nowheel' style={{
                padding: "4px 4px", overflow: "hidden",
                height: props.data.basic_info.node_size.h, color: "white", fontSize: "16px", fontWeight: 700,
                textOverflow: "clip", width: props.data.basic_info.node_size.w, whiteSpace: "nowrap",
                textAlign: "center",
            }} title={roadmapStore.inEdit ? "编辑引用路线图" : "打开引用路线图"}>
                {props.data.basic_info.node_data.RoadmapRefData?.target_title ?? ""}
            </div>
        </NodeWrap>
    );
};

export default observer(RoadmapRefNode);