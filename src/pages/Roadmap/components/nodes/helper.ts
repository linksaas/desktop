//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { NODE_GROUP, NODE_LABEL, NODE_SUB_TOPIC, NODE_RICH_TEXT, NODE_TODO_LIST, NODE_TOPIC, NODE_TYPE, NODE_ROADMAP_REF, NODE_WEB_LINK, NODE_CODE } from "@/api/roadmap_content";
import LabelNode from "./LabelNode";
import RichTextNode from "./RichTextNode";
import RoadmapEdge from "../RoadmapEdge";
import TopicNode from "./TopicNode";
import SubTopicNode from "./SubTopicNode";
import GroupNode from "./GroupNode";
import TodoListNode from "./TodoListNode";
import RoadmapRefNode from "./RoadmapRefNode";
import WebLinkNode from "./WebLinkNode";
import CodeNode from "./CodeNode";

export const allNodeTypes = {
    LabelNode: LabelNode,
    RichTextNode: RichTextNode,
    TopicNode: TopicNode,
    SubTopicNode: SubTopicNode,
    TodoListNode: TodoListNode,
    GroupNode: GroupNode,
    RoadmapRefNode: RoadmapRefNode,
    WebLinkNode: WebLinkNode,
    CodeNode: CodeNode,
};

export const allEdgeTypes = {
    RoadmapEdge: RoadmapEdge,
}

export function getNodeTypeStr(nodeType: NODE_TYPE): string {
    if (nodeType == NODE_LABEL) {
        return "LabelNode";
    } else if (nodeType == NODE_RICH_TEXT) {
        return "RichTextNode";
    } else if (nodeType == NODE_TOPIC) {
        return "TopicNode";
    } else if (nodeType == NODE_SUB_TOPIC) {
        return "SubTopicNode";
    } else if (nodeType == NODE_TODO_LIST) {
        return "TodoListNode";
    } else if (nodeType == NODE_GROUP) {
        return "GroupNode";
    } else if (nodeType == NODE_ROADMAP_REF) {
        return "RoadmapRefNode";
    } else if (nodeType == NODE_WEB_LINK) {
        return "WebLinkNode";
    } else if (nodeType == NODE_CODE) {
        return "CodeNode";
    }
    return "";
}