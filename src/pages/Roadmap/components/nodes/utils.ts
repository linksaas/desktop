//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import type { XYPosition } from "reactflow";
import type { RoadmapStore } from "../../store/roadmap";
import type { DragNodeInfo } from "./DragNode";
import { add_node, NODE_TODO_LIST } from "@/api/roadmap_content";
import { uniqId } from "@/utils/utils";
import { request } from "@/utils/request";
import { CodeDargNodeInfo, GroupDargNodeInfo, LabelDargNodeInfo, RichTextDargNodeInfo, RoadmapRefDargNodeInfo, SubTopicDargNodeInfo, TodoListDargNodeInfo, TopicDargNodeInfo, WebLinkDargNodeInfo } from "./contants";

export async function createNode(roadmapStore: RoadmapStore, itemData: DragNodeInfo, position?: XYPosition) {
    if (roadmapStore.inEdit == false) {
        return;
    }
    let newPosition = position;
    if (newPosition == undefined) {
        if (roadmapStore.flowInstance == null || roadmapStore.mousePosition == null) {
            return;
        }
        const domRef = document.querySelector('.react-flow') as HTMLElement;
        if (domRef == null) {
            return;
        }
        const rect = domRef.getBoundingClientRect();
        newPosition = roadmapStore.flowInstance.project({
            x: roadmapStore.mousePosition.x - rect.left,
            y: roadmapStore.mousePosition.y - rect.top,
        });
    }
    const nodeData = {
        ...itemData.nodeData,
    };
    if (itemData.nodeType == NODE_TODO_LIST) {
        const tmpList = (nodeData.TodoListData?.item_list ?? []).map(item => ({
            todo_id: uniqId(),
            title: item.title,
        }));
        nodeData.TodoListData = {
            item_list: tmpList,
            bg_color: itemData.nodeData.TodoListData?.bg_color ?? "white",
        };
    }
    const res = await request(add_node({
        session_id: roadmapStore.sessionId,
        roadmap_id: roadmapStore.roadmapId,
        basic_info: {
            node_type: itemData.nodeType,
            node_size: {
                w: itemData.defaultWidth,
                h: itemData.defaultHeight,
            },
            node_position: {
                x: newPosition.x,
                y: newPosition.y,
            },
            node_data: nodeData,
        },
    }));
    roadmapStore.onUpdateNode(res.node_id);
};


export async function addTopicNode(roadmapStore: RoadmapStore) {
    if (roadmapStore.flowInstance != null && roadmapStore.mousePosition != null) {
        await createNode(roadmapStore, TopicDargNodeInfo);
    }
};

export async function addSubTopicNode(roadmapStore: RoadmapStore) {
    if (roadmapStore.flowInstance != null && roadmapStore.mousePosition != null) {
        await createNode(roadmapStore, SubTopicDargNodeInfo);
    }
};

export async function addRoadmapRefNode(roadmapStore: RoadmapStore) {
    if (roadmapStore.flowInstance != null && roadmapStore.mousePosition != null) {
        await createNode(roadmapStore, RoadmapRefDargNodeInfo);
    }
};

export async function addLabelNode(roadmapStore: RoadmapStore) {
    if (roadmapStore.flowInstance != null && roadmapStore.mousePosition != null) {
        await createNode(roadmapStore, LabelDargNodeInfo);
    }
};

export async function addRichTextNode(roadmapStore: RoadmapStore) {
    if (roadmapStore.flowInstance != null && roadmapStore.mousePosition != null) {
        await createNode(roadmapStore, RichTextDargNodeInfo);
    }
};

export async function addTodoListNode(roadmapStore: RoadmapStore) {
    if (roadmapStore.flowInstance != null && roadmapStore.mousePosition != null) {
        await createNode(roadmapStore, TodoListDargNodeInfo);
    }
};

export async function addWebLinkNode(roadmapStore: RoadmapStore) {
    if (roadmapStore.flowInstance != null && roadmapStore.mousePosition != null) {
        await createNode(roadmapStore, WebLinkDargNodeInfo);
    }
};

export async function addCodeNode(roadmapStore: RoadmapStore) {
    if (roadmapStore.flowInstance != null && roadmapStore.mousePosition != null) {
        await createNode(roadmapStore, CodeDargNodeInfo);
    }
};

export async function addGroupNode(roadmapStore: RoadmapStore) {
    if (roadmapStore.flowInstance != null && roadmapStore.mousePosition != null) {
        await createNode(roadmapStore, GroupDargNodeInfo);
    }
};