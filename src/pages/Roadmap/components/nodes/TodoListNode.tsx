//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import type { NodeProps } from "reactflow";
import { NodeInfo } from '@/api/roadmap_content';
import NodeWrap from './NodeWrap';
import { Checkbox, List, Space } from 'antd';
import { observer } from 'mobx-react';
import { useRoadmapStores } from '../../store';
import { set_state } from "@/api/roadmap_state";
import { request } from '@/utils/request';



const TodoListNode = (props: NodeProps<NodeInfo>) => {
    const roadmapStore = useRoadmapStores();

    const [selTodoIdList, setSelTodoIdList] = useState<string[]>([]);

    const addTodoId = async (todoId: string) => {
        const newTodoIdList = [...selTodoIdList, todoId];
        const doneList = (props.data.basic_info.node_data.TodoListData?.item_list ?? []).filter(item => newTodoIdList.includes(item.todo_id)).map(item => ({
            todo_id: item.todo_id,
            title: item.title,
        }))
        await request(set_state({
            session_id: roadmapStore.sessionId,
            roadmap_id: roadmapStore.roadmapId,
            node_id: props.data.node_id,
            basic_info: {
                state_data: {
                    TodoListState: {
                        done_list: doneList,
                    },
                },
            },
        }));
        roadmapStore.onUpdateNodeState(props.data.node_id);
        roadmapStore.loadStateList();
    };

    const removeTodId = async (todoId: string) => {
        const newTodoIdList = selTodoIdList.filter(item => item != todoId);
        const doneList = (props.data.basic_info.node_data.TodoListData?.item_list ?? []).filter(item => newTodoIdList.includes(item.todo_id)).map(item => ({
            todo_id: item.todo_id,
            title: item.title,
        }))
        await request(set_state({
            session_id: roadmapStore.sessionId,
            roadmap_id: roadmapStore.roadmapId,
            node_id: props.data.node_id,
            basic_info: {
                state_data: {
                    TodoListState: {
                        done_list: doneList,
                    },
                },
            },
        }));
        roadmapStore.onUpdateNodeState(props.data.node_id);
        roadmapStore.loadStateList();
    };

    useEffect(() => {
        if (roadmapStore.inEdit || roadmapStore.adminUser) {
            return;
        }
        const state = roadmapStore.stateList.find(item => item.node_id = props.data.node_id);
        if (state == undefined) {
            setSelTodoIdList([]);
        } else {
            const tmpList = (state.basic_info.state_data.TodoListState?.done_list ?? []).map(item => item.todo_id);
            setSelTodoIdList(tmpList);
        }
    }, [roadmapStore.stateList, props.data.node_id, roadmapStore.adminUser, roadmapStore.inEdit]);

    return (
        <NodeWrap hasHandle={false} nodeData={props} style={{
            backgroundColor: props.data.basic_info.node_data.TodoListData?.bg_color ?? "white",
        }} >
            <List rowKey="todo_id" dataSource={props.data.basic_info.node_data.TodoListData?.item_list ?? []} pagination={false}
                style={{ height: `${props.data.basic_info.node_size.h}px`, overflowY: "scroll", padding: "10px 10px" }}
                renderItem={item => (
                    <List.Item>
                        <Space>
                            {roadmapStore.inEdit && (
                                <Checkbox disabled />
                            )}
                            {roadmapStore.inEdit == false && (
                                <Checkbox disabled={roadmapStore.adminUser} checked={selTodoIdList.includes(item.todo_id)}
                                    onChange={e => {
                                        e.stopPropagation();
                                        if (e.target.checked) {
                                            addTodoId(item.todo_id);
                                        } else {
                                            removeTodId(item.todo_id);
                                        }
                                    }} />
                            )}
                            <span style={{ textDecoration: selTodoIdList.includes(item.todo_id) ? "line-through" : undefined }}>{item.title}</span>
                        </Space>
                    </List.Item>
                )} />
        </NodeWrap>
    );
};

export default observer(TodoListNode);