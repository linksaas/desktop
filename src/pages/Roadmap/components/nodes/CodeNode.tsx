//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from 'react';
import type { NodeProps } from "reactflow";
import { NodeInfo } from '@/api/roadmap_content';
import NodeWrap from './NodeWrap';
import { observer } from 'mobx-react';
import CodeEditor from '@uiw/react-textarea-code-editor';


const CodeNode = (props: NodeProps<NodeInfo>) => {
    return (
        <NodeWrap hasHandle nodeData={props} style={{
            backgroundColor: "white",
            borderRadius: "10px",
            padding: "4px 4px",
        }}>
            <CodeEditor
                className='nowheel'
                value={props.data.basic_info.node_data.CodeData?.code ?? ""}
                language={props.data.basic_info.node_data.CodeData?.lang ?? ""}
                readOnly
                style={{
                    fontSize: 14,
                    backgroundColor: '#f5f5f5',
                    height: props.data.basic_info.node_size.h - 8,
                    overflowY: "scroll",
                }}
            />
        </NodeWrap>
    );
};

export default observer(CodeNode);