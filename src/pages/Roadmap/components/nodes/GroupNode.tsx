//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from 'react';
import type { NodeProps } from "reactflow";
import { NodeInfo } from '@/api/roadmap_content';
import NodeWrap from './NodeWrap';
import { observer } from 'mobx-react';



const GroupNode = (props: NodeProps<NodeInfo>) => {
    return (
        <NodeWrap hasHandle={false} nodeData={props} style={{
            backgroundColor: props.data.basic_info.node_data.GroupData?.bg_color ?? "",
            border: `2px solid ${props.data.basic_info.node_data.GroupData?.border_color ?? "black"}`,
            borderRadius: `${props.data.basic_info.node_data.GroupData?.border_radius ?? 0}px`
        }} />
    );
};

export default observer(GroupNode);