//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from 'react';
import type { NodeProps } from "reactflow";
import { NodeInfo } from '@/api/roadmap_content';
import NodeWrap from './NodeWrap';
import { observer } from 'mobx-react';
import { useRoadmapStores } from '../../store';
import { LinkOutlined } from '@ant-design/icons';

const WebLinkNode = (props: NodeProps<NodeInfo>) => {
    const roadmapStore = useRoadmapStores();

    return (
        <NodeWrap hasHandle nodeData={props} style={{
            backgroundColor: "white",
            borderRadius: "10px",
        }}>
            <div className='nowheel' style={{
                padding: "4px 4px", overflow: "hidden",
                height: props.data.basic_info.node_size.h, color: "blue", fontSize: "16px", fontWeight: 700,
                textOverflow: "clip", width: props.data.basic_info.node_size.w, whiteSpace: "nowrap",
                textAlign: "center",
            }} title={roadmapStore.inEdit ? "编辑网页链接" : "打开网页链接"}>
                <LinkOutlined />{props.data.basic_info.node_data.WebLinkData?.link_name??""}
            </div>
        </NodeWrap>
    );
};

export default observer(WebLinkNode);