//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from 'react';
import { useDrag } from 'react-dnd';
import { DND_ITEM_TYPE } from '@/api/roadmap';
import type { NODE_TYPE, NodeData } from '@/api/roadmap_content';

export interface DragNodeInfo {
    nodeType: NODE_TYPE;
    nodeData: NodeData;
    defaultWidth: number;
    defaultHeight: number;
}

export interface DragNodeProps {
    title: string;
    icon: React.ReactNode;
    dragNodeInfo: DragNodeInfo;
}

const DragNode = (props: DragNodeProps) => {
    const [hover, setHover] = useState(false);

    const [_, drag] = useDrag(() => ({
        type: DND_ITEM_TYPE,
        item: props.dragNodeInfo,
        canDrag: true,
    }));

    return (
        <div ref={drag} style={{ cursor: "move", backgroundColor: hover ? "#aaa" : "#eee", width: "160px", border: "1px solid #e4e4e8", padding: "2px 10px" }}
            onMouseEnter={e => {
                e.stopPropagation();
                e.preventDefault();
                setHover(true);
            }}
            onMouseLeave={e => {
                e.stopPropagation();
                e.preventDefault();
                setHover(false);
            }}>
            {props.icon}
            &nbsp;&nbsp;
            {props.title}
        </div>
    );
};

export default DragNode;