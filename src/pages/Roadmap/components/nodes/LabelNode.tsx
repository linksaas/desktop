//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from 'react';
import type { NodeProps } from "reactflow";
import { NodeInfo, update_node_data } from '@/api/roadmap_content';
import NodeWrap from './NodeWrap';
import { observer } from 'mobx-react';
import { useRoadmapStores } from '../../store';
import { Button, Input, message } from 'antd';
import { CheckOutlined, CloseOutlined } from '@ant-design/icons';
import { request } from '@/utils/request';



const LabelNode = (props: NodeProps<NodeInfo>) => {
    const roadmapStore = useRoadmapStores();

    const [quickEdit, setQuickEdit] = useState(false);
    const [title, setTitle] = useState("");

    const updateTitle = async () => {
        if (props.data.basic_info.node_data.TextData == undefined) {
            return;
        }
        await request(update_node_data({
            session_id: roadmapStore.sessionId,
            roadmap_id: roadmapStore.roadmapId,
            node_id: props.data.node_id,
            node_data: {
                TextData: {
                    ...props.data.basic_info.node_data.TextData,
                    content: title,
                },
            },
        }));
        roadmapStore.onUpdateNode(props.data.node_id);
        setQuickEdit(false);
        message.info("修改成功");
    };

    return (
        <NodeWrap hasHandle nodeData={props} style={{
            backgroundColor: props.data.basic_info.node_data.TextData?.bg_color ?? "white",
        }}>

            <div title={props.data.basic_info.node_data.TextData?.content ?? ""}
                style={{
                    textOverflow: "ellipsis", overflow: quickEdit ? "visible" : "hidden", whiteSpace: "nowrap",
                    padding: "4px 4px",
                    textAlign: "center",
                    color: props.data.basic_info.node_data.TextData?.font_color ?? "black",
                    fontWeight: props.data.basic_info.node_data.TextData?.font_weight ?? 500,
                    fontSize: props.data.basic_info.node_data.TextData?.font_size ?? 14,
                }}
                onDoubleClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    if (roadmapStore.inEdit && quickEdit == false) {
                        setTitle(props.data.basic_info.node_data.TextData?.content ?? "");
                        setQuickEdit(true);
                    }
                }}>
                {quickEdit == false &&
                    <>
                        {props.data.basic_info.node_data.TextData?.content ?? ""}
                    </>
                }
                {quickEdit == true && (
                    <Input value={title} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setTitle(e.target.value);
                    }} prefix={<div>
                        <Button type="text" icon={<CloseOutlined />} style={{ minWidth: 0, padding: "0px 0px", color: "#777", zIndex: 9000  }} title='取消'
                            onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setQuickEdit(false);
                            }} size='small'/>
                        <Button type="text" icon={<CheckOutlined />} style={{ minWidth: 0, padding: "0px 0px", color: "green", zIndex: 9000  }} title='保存'
                            onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                updateTitle();
                            }} disabled={title.trim() == ""} size='small'/>
                    </div>} style={{ minWidth: 200, zIndex: 9000 }}
                        onKeyDown={e => {
                            if (e.key == "Enter") {
                                e.stopPropagation();
                                e.preventDefault();
                                updateTitle();
                            } else if (e.key == "Escape") {
                                e.stopPropagation();
                                e.preventDefault();
                                setQuickEdit(false);
                            }
                        }} autoFocus onBlur={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            setQuickEdit(false);
                        }} />
                )}
            </div>
        </NodeWrap>
    );
};

export default observer(LabelNode);