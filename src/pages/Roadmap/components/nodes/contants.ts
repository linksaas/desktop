//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { LEARN_POLICY_NONE, NODE_CODE, NODE_GROUP, NODE_LABEL, NODE_RICH_TEXT, NODE_ROADMAP_REF, NODE_SUB_TOPIC, NODE_TODO_LIST, NODE_TOPIC, NODE_WEB_LINK } from "@/api/roadmap_content";
import type { DragNodeInfo } from "./DragNode";


export const TopicDargNodeInfo: DragNodeInfo = {
    nodeType: NODE_TOPIC,
    nodeData: {
        TopicData: {
            font_size: 16,
            font_weight: 700,
            font_color: "black",
            title: "学习阶段",
            bg_color: "#fdff00",
            content: "",
            learn_policy: LEARN_POLICY_NONE,
            link_list: [],
        },
    },
    defaultWidth: 100,
    defaultHeight: 40,
};

export const SubTopicDargNodeInfo: DragNodeInfo = {
    nodeType: NODE_SUB_TOPIC,
    nodeData: {
        TopicData: {
            font_size: 16,
            font_weight: 700,
            font_color: "black",
            title: "知识点",
            bg_color: "#ffe599",
            content: "",
            learn_policy: LEARN_POLICY_NONE,
            link_list: [],
        },
    },
    defaultWidth: 100,
    defaultHeight: 40,
};

export const RoadmapRefDargNodeInfo: DragNodeInfo = {
    nodeType: NODE_ROADMAP_REF,
    nodeData: {
        RoadmapRefData: {
            target_id: "",
            target_title: "",
        },
    },
    defaultWidth: 120,
    defaultHeight: 40,
};

export const LabelDargNodeInfo: DragNodeInfo = {
    nodeType: NODE_LABEL,
    nodeData: {
        TextData: {
            font_size: 14,
            font_weight: 500,
            font_color: "black",
            content: "标签",
            bg_color: "white",
        },
    },
    defaultWidth: 100,
    defaultHeight: 40,
};

export const RichTextDargNodeInfo: DragNodeInfo = {
    nodeType: NODE_RICH_TEXT,
    nodeData: {
        RichTextData: {
            content: "",
            bg_color: "#73d8ff",
        },
    },
    defaultWidth: 200,
    defaultHeight: 300,
};

export const TodoListDargNodeInfo: DragNodeInfo = {
    nodeType: NODE_TODO_LIST,
    nodeData: {
        TodoListData: {
            item_list: [
                {
                    todo_id: "todo1",
                    title: "待办事项1",
                },
                {
                    todo_id: "todo2",
                    title: "待办事项2",
                },
                {
                    todo_id: "todo3",
                    title: "待办事项3",
                },
            ],
            bg_color: "white",
        },
    },
    defaultWidth: 150,
    defaultHeight: 200,
};

export const WebLinkDargNodeInfo: DragNodeInfo = {
    nodeType: NODE_WEB_LINK,
    nodeData: {
        WebLinkData: {
            link_name: "",
            link_url: "",
        },
    },
    defaultWidth: 100,
    defaultHeight: 40,
};

export const CodeDargNodeInfo: DragNodeInfo = {
    nodeType: NODE_CODE,
    nodeData: {
        CodeData: {
            lang: "",
            code: "",
        },
    },
    defaultWidth: 200,
    defaultHeight: 300,
};

export const GroupDargNodeInfo: DragNodeInfo = {
    nodeType: NODE_GROUP,
    nodeData: {
        GroupData: {
            bg_color: "white",
            border_color: "black",
            border_radius: 0,
        },
    },
    defaultWidth: 300,
    defaultHeight: 300,
};