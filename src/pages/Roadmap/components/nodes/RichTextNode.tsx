//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from 'react';
import type { NodeProps } from "reactflow";
import { NodeInfo } from '@/api/roadmap_content';
import NodeWrap from './NodeWrap';
import { observer } from 'mobx-react';
import { ReadOnlyEditor } from '@/components/Editor';

const RichTextNode = (props: NodeProps<NodeInfo>) => {
    return (
        <NodeWrap hasHandle nodeData={props} style={{ backgroundColor: props.data.basic_info.node_data.RichTextData?.bg_color ?? "white" }}>
            <div className='nowheel' style={{ padding: "10px 10px", overflow: "hidden", height: props.data.basic_info.node_size.h }}>
                <div className='_noScrollContext'>
                    <ReadOnlyEditor content={props.data.basic_info.node_data.RichTextData?.content ?? ""} />
                </div>
            </div>
        </NodeWrap>
    );
};

export default observer(RichTextNode);