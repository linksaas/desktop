//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import { Drawer } from 'antd';
import { useRoadmapStores } from '../store';
import { type NodeInfo, type EdgeInfo, NODE_LABEL, NODE_RICH_TEXT, NODE_TOPIC, NODE_SUB_TOPIC, NODE_TODO_LIST, NODE_GROUP, NODE_ROADMAP_REF, NODE_WEB_LINK, NODE_CODE } from "@/api/roadmap_content";
import LabelNodeEditPanel from './drawers/LabelNodeEditPanel';
import RichTextNodeEditPanel from './drawers/RichTextNodeEditPanel';
import TopicNodeEditPanel from './drawers/TopicNodeEditPanel';
import GroupNodeEditPanel from './drawers/GroupNodeEditPanel';
import TodoListNodeEditPanel from './drawers/TodoListNodeEditPanel';
import EdgeEditPanel from './drawers/EdgeEditPanel';
import TopicNodeViewPanel from './drawers/TopicNodeViewPanel';
import RoadmapRefNodeEditPanel from './drawers/RoadmapRefNodeEditPanel';
import WebLinkNodeEditPanel from './drawers/WebLinkNodeEditPanel';
import CodeNodeEditPanel from './drawers/CodeNodeEditPanel';

const DataEditDrawer = () => {
    const roadmapStore = useRoadmapStores();

    const [curNode, setCurNode] = useState<NodeInfo | null>(null);
    const [curEdge, setCurEdge] = useState<EdgeInfo | null>(null);

    const calcTitle = () => {
        if (curEdge != null) {
            return "连接";
        } else if (curNode != null) {
            if (curNode.basic_info.node_type == NODE_LABEL) {
                return "标签";
            } else if (curNode.basic_info.node_type == NODE_RICH_TEXT) {
                return "文本段落";
            } else if (curNode.basic_info.node_type == NODE_TOPIC) {
                return "学习阶段";
            } else if (curNode.basic_info.node_type == NODE_SUB_TOPIC) {
                return "知识点";
            } else if (curNode.basic_info.node_type == NODE_TODO_LIST) {
                return "待办列表";
            } else if (curNode.basic_info.node_type == NODE_ROADMAP_REF) {
                return "引用路线图";
            } else if (curNode.basic_info.node_type == NODE_GROUP) {
                return "边框";
            }
        }
        return "";
    };

    useEffect(() => {
        if (roadmapStore.selectNodeId == "") {
            setCurNode(null);
        } else {
            const tmpNode = roadmapStore.getNode(roadmapStore.selectNodeId);
            setCurNode(tmpNode ?? null);
        }
        if (roadmapStore.selectEdgeKey == null) {
            setCurEdge(null);
        } else {
            const tmpEdge = roadmapStore.getEdge(roadmapStore.selectEdgeKey);
            setCurEdge(tmpEdge ?? null);
        }
    }, [roadmapStore.selectNodeId, roadmapStore.selectEdgeKey]);


    return (
        <Drawer
            title={calcTitle()}
            placement="right"
            width={600}
            onClose={e => {
                e.stopPropagation();
                e.preventDefault();
                roadmapStore.selectNodeId = "";
                roadmapStore.selectEdgeKey = null;
            }}
            open
        >
            {roadmapStore.inEdit && curNode != null && (
                <>
                    {curNode.basic_info.node_type == NODE_LABEL && <LabelNodeEditPanel nodeId={curNode.node_id} />}
                    {curNode.basic_info.node_type == NODE_RICH_TEXT && <RichTextNodeEditPanel nodeId={curNode.node_id} />}
                    {(curNode.basic_info.node_type == NODE_TOPIC || curNode.basic_info.node_type == NODE_SUB_TOPIC) && <TopicNodeEditPanel nodeId={curNode.node_id} />}
                    {curNode.basic_info.node_type == NODE_TODO_LIST && <TodoListNodeEditPanel nodeId={curNode.node_id} />}
                    {curNode.basic_info.node_type == NODE_ROADMAP_REF && <RoadmapRefNodeEditPanel nodeId={curNode.node_id} /> }
                    {curNode.basic_info.node_type == NODE_WEB_LINK && <WebLinkNodeEditPanel nodeId={curNode.node_id} /> }
                    {curNode.basic_info.node_type == NODE_CODE && <CodeNodeEditPanel nodeId={curNode.node_id}/>}
                    {curNode.basic_info.node_type == NODE_GROUP && <GroupNodeEditPanel nodeId={curNode.node_id} />}
                </>
            )}
            {roadmapStore.inEdit && curEdge != null && (
                <EdgeEditPanel edgeKey={curEdge.edge_key} />
            )}
            {roadmapStore.inEdit == false && roadmapStore.adminUser == false && curNode != null && (
                <>
                    {(curNode.basic_info.node_type == NODE_TOPIC || curNode.basic_info.node_type == NODE_SUB_TOPIC) && <TopicNodeViewPanel nodeId={curNode.node_id} />}
                </>
            )}
        </Drawer>
    );
};

export default observer(DataEditDrawer);