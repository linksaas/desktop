//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { makeAutoObservable, runInAction } from 'mobx';
import { type Doc, get_doc } from "@/api/project_doc";
import { type EntryInfo, get as get_entry } from "@/api/project_entry";
import { request } from '@/utils/request';
import { get_session, get_user_id } from '@/api/user';
import { type ProjectInfo, get_project } from "@/api/project";
import { type GetCfgResponse, VendorConfig, get_cfg, get_vendor_config } from "@/api/client_cfg";

export class DocStore {
    constructor() {
        makeAutoObservable(this);
    }

    private _projectId = "";
    private _myUserId = "";
    private _docId = "";
    private _inEdit = false;

    private _entry: EntryInfo | null = null;
    private _doc: Doc | null = null;
    private _keywordList: string[] = [];
    private _project: ProjectInfo | null = null;
    private _clientCfg: GetCfgResponse | null = null;
    private _vendorCfg: VendorConfig | null = null;

    get projectId() {
        return this._projectId;
    }

    set projectId(val: string) {
        runInAction(() => {
            this._projectId = val;
        });
    }

    get myUserId() {
        return this._myUserId;
    }

    get docId() {
        return this._docId;
    }

    set docId(val: string) {
        runInAction(() => {
            this._docId = val;
        })
    }

    get inEdit() {
        return this._inEdit;
    }

    set inEdit(val: boolean) {
        runInAction(() => {
            this._inEdit = val;
        });
    }

    get entry() {
        return this._entry;
    }

    get doc() {
        return this._doc;
    }

    get keywordList() {
        return this._keywordList;
    }

    get project() {
        return this._project;
    }

    get clientCfg() {
        return this._clientCfg;
    }

    get vendorCfg() {
        return this._vendorCfg;
    }

    async loadProjectAndUser() {
        const cfgRes = await get_cfg();
        const vendorRes = await get_vendor_config();

        const sessionId = await get_session();
        const prjRes = await request(get_project(sessionId, this._projectId));
        const userId = await get_user_id();
        runInAction(() => {
            this._clientCfg = cfgRes;
            this._vendorCfg = vendorRes;
            this._project = prjRes.info;
            this._myUserId = userId;
        });
    }

    async loadDoc() {
        const sessionId = await get_session();
        const entryRes = await request(get_entry({
            session_id: sessionId,
            project_id: this._projectId,
            entry_id: this._docId,
        }));
        const docRes = await request(get_doc({
            session_id: sessionId,
            project_id: this._projectId,
            doc_id: this._docId,
        }));
        runInAction(() => {
            this._entry = entryRes.entry;
            this._doc = docRes.doc;
            this._keywordList = docRes.idea_keyword_list;
        });
    }
}