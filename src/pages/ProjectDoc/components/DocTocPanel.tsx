//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from 'react';
import s from './EditDoc.module.less';
import { TocInfo } from '@/components/Editor/extensions/TocExtension';


export interface DocTocPanelProps {
    tocList: TocInfo[];
}

const DocTocPanel = (props:DocTocPanelProps) => {
    return (
        <div className={s.toc}>
            <h1>标题列表</h1>
            {props.tocList.map((toc, index) => (
                <div key={index} title={toc.title} style={{ paddingLeft: 20 * toc.level, overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap" }}>
                    <a onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        toc.scrollView();
                    }}>{toc.title}</a>
                </div>
            ))}
        </div>
    );
};

export default DocTocPanel;