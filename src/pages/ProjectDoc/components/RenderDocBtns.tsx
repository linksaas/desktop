//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from 'react';
import { observer } from 'mobx-react';
import { Popover, Space } from 'antd';
import Button from '@/components/Button';
import { BulbFilled } from '@ant-design/icons';
import CommentEntry from '@/components/CommentEntry';
import { COMMENT_TARGET_ENTRY } from '@/api/project_comment';
import { useDocStores } from '../store';
import IdeaTipModal from '@/pages/Idea/IdeaTipModal';
import type * as NoticeType from '@/api/notice_type';
import { WebviewWindow } from '@tauri-apps/api/window';
import { LinkIdeaPageInfo } from '@/stores/linkAux';

export interface RenderDocBtnsProps {
  keyWordList?: string[];
}

const RenderDocBtns = (props: RenderDocBtnsProps) => {
  const store = useDocStores();

  const [ideaKeyword, setIdeaKeyword] = useState("");

  return (
    <Space size="small">
      <CommentEntry projectId={store.projectId} targetType={COMMENT_TARGET_ENTRY}
        targetId={store.docId} myUserId={store.myUserId} myAdmin={store.project?.user_project_perm.can_admin ?? false}
        enableAdd={!(store.project?.closed ?? false)} />
      {(props.keyWordList ?? []).length > 0 && (
        <Popover placement='bottom'
          title="相关知识点"
          overlayStyle={{ width: 150 }}
          content={
            <div style={{ maxHeight: "calc(100vh - 300px)", padding: "10px 10px" }}>
              {(props.keyWordList ?? []).map(keyword => (
                <Button key={keyword} type="link" style={{ minWidth: 0 }} onClick={e => {
                  e.stopPropagation();
                  e.preventDefault();
                  setIdeaKeyword(keyword);
                }}>{keyword}</Button>
              ))}
            </div>
          }>
          <Button type="text" style={{ padding: "0px 0px", minWidth: 0 }}>
            <BulbFilled style={{ color: "orange", fontSize: "28px" }} />
          </Button>
        </Popover>
      )}

      <Button
        type="primary"
        style={{ marginLeft: "10px", padding: "0px 0px" }}
        disabled={store.project?.closed || !(store.entry?.can_update ?? false)}
        onClick={(e) => {
          e.stopPropagation();
          e.preventDefault();
          store.inEdit = true;
        }}
      >
        编辑
      </Button>

      {ideaKeyword != "" && store.project != null && (
        <IdeaTipModal projectInfo={store.project} ideaKeyword={ideaKeyword}
          onCancel={() => setIdeaKeyword("")}
          onOk={() => {
            const notice: NoticeType.AllNotice = {
              ClientNotice: {
                OpenLinkInfoNotice: {
                  link_info: new LinkIdeaPageInfo("", store.projectId, "", [ideaKeyword], "", false),
                },
              },
            };
            const mainWindow = WebviewWindow.getByLabel("main");
            mainWindow?.emit("notice", notice);
          }} />
      )}
    </Space>
  );
};

export default observer(RenderDocBtns);
