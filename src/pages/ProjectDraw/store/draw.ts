//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { makeAutoObservable, runInAction } from 'mobx';
import { type DrawInfo,get_draw } from '@/api/project_draw';
import { type EntryInfo, get as get_entry } from "@/api/project_entry";
import { request } from '@/utils/request';
import { get_session, get_user_id } from '@/api/user';
import { type ProjectInfo, get_project } from "@/api/project";
import { type GetCfgResponse, VendorConfig, get_cfg, get_vendor_config } from "@/api/client_cfg";

export class DrawStore {
    constructor() {
        makeAutoObservable(this);
    }

    private _projectId = "";
    private _myUserId = "";
    private _drawId = "";
    private _inEdit = false;

    private _entry: EntryInfo | null = null;
    private _draw: DrawInfo | null = null;
    private _project: ProjectInfo | null = null;
    private _clientCfg: GetCfgResponse | null = null;
    private _vendorCfg: VendorConfig | null = null;

    get projectId() {
        return this._projectId;
    }

    set projectId(val: string) {
        runInAction(() => {
            this._projectId = val;
        });
    }

    get myUserId() {
        return this._myUserId;
    }

    get drawId() {
        return this._drawId;
    }

    set drawId(val: string) {
        runInAction(() => {
            this._drawId = val;
        })
    }

    get inEdit() {
        return this._inEdit;
    }

    set inEdit(val: boolean) {
        runInAction(() => {
            this._inEdit = val;
        });
    }

    get entry() {
        return this._entry;
    }

    get draw() {
        return this._draw;
    }

    get project() {
        return this._project;
    }

    get clientCfg() {
        return this._clientCfg;
    }

    get vendorCfg() {
        return this._vendorCfg;
    }

    async loadProjectAndUser() {
        const cfgRes = await get_cfg();
        const verdorRes = await get_vendor_config();

        const sessionId = await get_session();
        const prjRes = await request(get_project(sessionId, this._projectId));
        const userId = await get_user_id();
        runInAction(() => {
            this._clientCfg = cfgRes;
            this._vendorCfg = verdorRes;
            this._project = prjRes.info;
            this._myUserId = userId;
        });
    }

    async loadDraw() {
        const sessionId = await get_session();
        const entryRes = await request(get_entry({
            session_id: sessionId,
            project_id: this._projectId,
            entry_id: this._drawId,
        }));
        const drawRes = await request(get_draw({
            session_id: sessionId,
            project_id: this._projectId,
            draw_id: this._drawId,
        }));
        runInAction(() => {
            this._entry = entryRes.entry;
            this._draw = drawRes.info;
        });
    }
}