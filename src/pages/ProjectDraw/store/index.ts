//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from "react";
import { DrawStore } from "./draw";

const stores = React.createContext(new DrawStore());

export const useDrawStores = () => React.useContext(stores);

export default stores;