//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect } from "react";
import { observer } from 'mobx-react';
import { runInAction } from "mobx";
import { useLocalObservable } from "mobx-react";
import { Button, Progress, Select, Space } from "antd";
import { PauseCircleFilled, PauseOutlined, PlayCircleOutlined, StopOutlined } from "@ant-design/icons";


const Pomodoro = () => {

    const localStore = useLocalObservable(() => ({
        totalSeconds: 25 * 60,
        setTotalSeconds(val: number) {
            runInAction(() => {
                this.totalSeconds = val;
            });
        },
        passSeconds: 0,
        setPassSeconds(val: number) {
            runInAction(() => {
                this.passSeconds = val;
            });
        },
        running: false,
        setRunning(val: boolean) {
            runInAction(() => {
                this.running = val;
            });
        },
        pause: false,
        setPause(val: boolean) {
            runInAction(() => {
                this.pause = val;
            });
        }
    }));

    useEffect(() => {
        if (localStore.running == false || localStore.pause) {
            return;
        }
        const t = setInterval(() => {
            if (localStore.passSeconds < localStore.totalSeconds) {
                localStore.setPassSeconds(localStore.passSeconds + 1);
            }
        }, 1000);
        return () => {
            clearInterval(t);
        };
    }, [localStore.running, localStore.pause]);

    return (
        <div style={{ display: "flex", marginTop: "20px", height: "30px", paddingBottom: "4px", borderBottom: "1px solid #e4e4e8" }}>
            <div style={{ flex: 1, marginLeft: "10px" }}>
                {localStore.running == false && "未开始计时"}
                {localStore.running && (
                    <Progress type="line" percent={localStore.passSeconds * 100 / localStore.totalSeconds} trailColor="grey"
                        format={() => `${(localStore.passSeconds / 60).toFixed(0)}分${localStore.passSeconds % 60}秒/${(localStore.totalSeconds / 60).toFixed(0)}分钟`}
                        size="small" style={{ width: "240px" }} />
                )}
            </div>
            <Space style={{ width: localStore.running ? "60px" : "100px", marginRight: "10px", marginLeft: "10px" }}>
                {localStore.running == false && (
                    <>
                        <Select value={localStore.totalSeconds} onChange={value => {
                            localStore.setTotalSeconds(value);
                            localStore.setPassSeconds(0);
                            localStore.setPause(false);
                        }} style={{ width: "70px" }}>
                            <Select.Option value={5 * 60}>5分钟</Select.Option>
                            <Select.Option value={10 * 60}>10分钟</Select.Option>
                            <Select.Option value={15 * 60}>15分钟</Select.Option>
                            <Select.Option value={20 * 60}>20分钟</Select.Option>
                            <Select.Option value={25 * 60}>25分钟</Select.Option>
                            <Select.Option value={30 * 60}>30分钟</Select.Option>
                            <Select.Option value={35 * 60}>35分钟</Select.Option>
                            <Select.Option value={40 * 60}>40分钟</Select.Option>
                            <Select.Option value={45 * 60}>45分钟</Select.Option>
                            <Select.Option value={50 * 60}>50分钟</Select.Option>
                            <Select.Option value={55 * 60}>55分钟</Select.Option>
                            <Select.Option value={60 * 60}>60分钟</Select.Option>
                        </Select>
                        <Button type="text" icon={<PlayCircleOutlined style={{ fontSize: "24px" }} />} onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            localStore.setRunning(true);
                        }} />

                    </>
                )}
                {localStore.running == true && (
                    <>
                        <Button type="text" icon={localStore.pause ? <PauseCircleFilled style={{ fontSize: "24px" }} title="取消暂停" /> : <PauseOutlined style={{ fontSize: "24px" }} title="暂停" />}
                            onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                localStore.setPause(!localStore.pause);
                            }} />
                        <Button type="text" icon={<StopOutlined style={{ fontSize: "24px" }} />} onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            localStore.setRunning(false);
                            localStore.setPassSeconds(0);
                            localStore.setPause(false);
                        }} />

                    </>
                )}
            </Space>
        </div>
    );
};

export default observer(Pomodoro);