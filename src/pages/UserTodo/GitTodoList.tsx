//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { USER_TYPE_ATOM_GIT, USER_TYPE_GIT_CODE, USER_TYPE_GITEE, type USER_TYPE } from "@/api/user";
import { List } from "antd";
import { list_my_issue as list_my_atomgit_issue } from "@/api/atomgit/issue";
import { list_my_issue as list_my_gitcode_issue } from "@/api/gitcode/issue";
import { list_my_issue as list_my_gitee_issue } from "@/api/gitee/issue";
import type * as NoticeType from '@/api/notice_type';
import { emit } from "@tauri-apps/api/event";
import { open as shell_open } from '@tauri-apps/api/shell';
import { ExportOutlined } from "@ant-design/icons";

export interface GitTodoListProps {
    userType: USER_TYPE;
    userToken: string;
    showClock: boolean;
}

interface GitMsg {
    id: string;
    url: string;
    title: string;
}

const GitTodoList = (props: GitTodoListProps) => {
    const [msgList, setMsgList] = useState<GitMsg[]>([]);

    const loadIssueList = async () => {
        let count = 0;
        if (props.userType == USER_TYPE_ATOM_GIT) {
            const tmpList = await list_my_atomgit_issue(props.userToken, {
                sort: "updated",
                direction: "desc",
                page: 1,
                per_page: 100,
            });
            const resultList = tmpList.map(item => ({
                id: item.id,
                url: item.html_url,
                title: item.title,
            }));
            setMsgList(resultList);
            count = resultList.length;
        } else if (props.userType == USER_TYPE_GIT_CODE) {
            const tmpList = await list_my_gitcode_issue(props.userToken);
            setMsgList(tmpList.map(item => ({
                id: `${item.id}`,
                url: item.html_url,
                title: item.title,
            })));
            count = tmpList.length;
        } else if (props.userType == USER_TYPE_GITEE) {
            const tmpList = await list_my_gitee_issue(props.userToken);
            const resultList = tmpList.map(item => ({
                id: `${item.id}`,
                url: item.html_url,
                title: item.title,
            }));
            setMsgList(resultList);
            count = resultList.length;
        }
        const notice: NoticeType.AllNotice = {
            ClientNotice: {
                GitTodoCountNotice: {
                    count: count,
                },
            }
        };
        emit("notice", notice);
    };

    useEffect(() => {
        loadIssueList();
    }, []);

    return (
        <List rowKey="id" dataSource={msgList} pagination={false} style={{ height: props.showClock ? "calc(100vh - 130px)" : "calc(100vh - 90px)", overflowY: "scroll", padding: "0px 10px" }}
            renderItem={item => (
                <List.Item>
                    <a style={{ fontSize: "14px", fontWeight: 600 }} onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        shell_open(item.url);
                    }}>{item.title}&nbsp;&nbsp;<ExportOutlined /></a>
                </List.Item>
            )} />
    );
};

export default GitTodoList;