//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import type { SimpleIssueInfo } from "@/api/project_issue";
import { ISSUE_TYPE_TASK, list_my_simple_todo, SORT_KEY_UPDATE_TIME, SORT_TYPE_DSC } from "@/api/project_issue";
import { listen } from '@tauri-apps/api/event';
import type * as NoticeType from '@/api/notice_type';
import { get_session } from "@/api/user";
import { request } from "@/utils/request";
import { Button, List } from "antd";
import { showShortNote, type ShortNoteEvent } from "@/utils/short_note";
import { SHORT_NOTE_BUG, SHORT_NOTE_MODE_DETAIL, SHORT_NOTE_TASK } from "@/api/short_note";
import { WebviewWindow } from "@tauri-apps/api/window";
import { ExportOutlined } from "@ant-design/icons";

export interface IssueTodoListProps {
    showClock: boolean;
}

const IssueTodoList = (props: IssueTodoListProps) => {
    const [issueList, setIssueList] = useState<SimpleIssueInfo[]>([]);

    const loadIssueList = async () => {
        const sessionId = await get_session();
        const res = await request(list_my_simple_todo({
            session_id: sessionId,
            sort_type: SORT_TYPE_DSC,
            sort_key: SORT_KEY_UPDATE_TIME,
        }));
        setIssueList(res.info_list);
    };

    useEffect(() => {
        loadIssueList();
        const unListenFn = listen<NoticeType.AllNotice>("notice", ev => {
            const notice = ev.payload;
            if ((notice.IssueNotice?.NewIssueNotice != undefined) || (notice.IssueNotice?.UpdateIssueNotice != undefined) || (notice.IssueNotice?.RemoveIssueNotice != undefined)) {
                loadIssueList();
            }
        });
        return () => {
            unListenFn.then((unListen) => unListen());
        };
    }, []);

    return (
        <List rowKey="issue_id" dataSource={issueList} pagination={false} style={{ height: props.showClock ? "calc(100vh - 130px)" : "calc(100vh - 90px)", overflowY: "scroll", padding: "0px 10px" }}
            renderItem={issue => (
                <List.Item style={{ borderBottom: "1px solid #e4e4e8" }} extra={
                    <Button type="link" icon={<ExportOutlined />} title="桌面便签"
                        onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            get_session().then(sessionId => {
                                showShortNote(sessionId, {
                                    shortNoteType: issue.issue_type == ISSUE_TYPE_TASK ? SHORT_NOTE_TASK : SHORT_NOTE_BUG,
                                    data: issue,
                                }, issue.project_name);
                            });
                        }} />
                }>
                    <a style={{ display: "inline-block", width: "330px", whiteSpace: "wrap", wordWrap: "break-word", fontSize: "14px" }}
                        onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            const ev: ShortNoteEvent = {
                                projectId: issue.project_id,
                                shortNoteModeType: SHORT_NOTE_MODE_DETAIL,
                                shortNoteType: issue.issue_type == ISSUE_TYPE_TASK ? SHORT_NOTE_TASK : SHORT_NOTE_BUG,
                                targetId: issue.issue_id,
                                extraTargetValue: "",
                            };
                            const mainWindow = WebviewWindow.getByLabel("main");
                            mainWindow?.emit("shortNote", ev);
                        }}>
                        {issue.issue_type == ISSUE_TYPE_TASK ? "[任务]" : "[缺陷]"}
                        &nbsp;&nbsp;
                        {issue.basic_info.title}
                    </a>
                </List.Item>
            )} />
    );
};

export default IssueTodoList;