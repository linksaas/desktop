//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { observer } from 'mobx-react';
import s from "./index.module.less";
import { useHistory, useLocation } from "react-router-dom";
import { useStores } from "@/hooks";
import { GROW_CENTER_PATH } from "@/utils/constant";
import { Button, Input, Select, Space, Tabs } from "antd";
import { BookOutlined, EyeOutlined, PlusOutlined, RiseOutlined, UnorderedListOutlined } from "@ant-design/icons";
import RoadmapListPanel from "./RoadmapListPanel";
import UserRoadmapEvListPanel from "./UserRoadmapEvListPanel";
import EditRoadmapModal from "./components/EditRoadmapModal";
import AccessListPanel from "./AccessListPanel";
import NoteListPanel from "./NoteListPanel";
import type { SimpleRoadmapInfo } from "@/api/roadmap_user";
import { list_note_roadmap } from "@/api/roadmap_user";
import { request } from "@/utils/request";

const GrowCenter = () => {
    const location = useLocation();
    const history = useHistory();

    const userStore = useStores('userStore');
    
    const urlParams = new URLSearchParams(location.search);
    let tab = urlParams.get('tab') ?? "";
    if (tab == "") {
        tab = "pubRoadmap";
    }

    const [activeKey, setActiveKey] = useState(tab);

    const [dataVersion, setDataVersion] = useState(0);
    const [showCreateModal, setShowCreateModal] = useState(false);

    const [simpleRoadmapList, setSimpleRoadmapList] = useState<SimpleRoadmapInfo[]>([]);
    const [curNoteRoadmapId, setCurNoteRoadmapId] = useState("");

    const [roadmapKeyword, setRoadmapKeyword] = useState("");

    const loadSimpleRoadmapList = async () => {
        const res = await request(list_note_roadmap({
            session_id: userStore.sessionId,
        }));
        setSimpleRoadmapList(res.info_list);
    };

    useEffect(() => {
        if (userStore.sessionId == "" && ["myRoadmap", "access", "record"].includes(activeKey)) {
            history.push(`${GROW_CENTER_PATH}?tab=pubRoadmap`);
            setActiveKey("pubRoadmap");
        }
    }, [userStore.sessionId]);

    useEffect(() => {
        if (tab == "note") {
            loadSimpleRoadmapList();
        }
    }, [tab]);

    return (
        <div className={s.tabs_wrap}>
            <Tabs activeKey={activeKey}
                type='card'
                onChange={key => {
                    setActiveKey(key);
                    history.push(`${GROW_CENTER_PATH}?tab=${key}`);
                }}
                tabBarExtraContent={
                    <Space style={{ marginRight: "20px" }}>
                        {["pubRoadmap", "myRoadmap"].includes(activeKey) && (
                            <Input placeholder="请输入关键词" allowClear value={roadmapKeyword} onChange={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setRoadmapKeyword(e.target.value.trim());
                            }} />
                        )}
                        {activeKey == "myRoadmap" && (
                            <Button type="primary" icon={<PlusOutlined />} onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setShowCreateModal(true);
                            }}>创建路线图</Button>
                        )}
                        {activeKey == "note" && (
                            <Select value={curNoteRoadmapId} onChange={value => setCurNoteRoadmapId(value)} style={{ width: "150px" }}>
                                <Select.Option value="">全部</Select.Option>
                                {simpleRoadmapList.map(infoItem => (
                                    <Select.Option key={infoItem.roadmap_id} value={infoItem.roadmap_id}>{infoItem.roadmap_title}</Select.Option>
                                ))}
                            </Select>
                        )}
                    </Space>
                }>
                <Tabs.TabPane tab={<h2><RiseOutlined />&nbsp;公开路线</h2>} key="pubRoadmap">
                    {activeKey == "pubRoadmap" && (
                        <div className={s.content_wrap}>
                            <RoadmapListPanel sourceType="pub" dataVersion={0} keyword={roadmapKeyword} />
                        </div>
                    )}
                </Tabs.TabPane>

                {userStore.sessionId != "" && (
                    <>
                        <Tabs.TabPane tab={<h2><RiseOutlined />&nbsp;我的路线</h2>} key="myRoadmap">
                            {activeKey == "myRoadmap" && (
                                <div className={s.content_wrap}>
                                    <RoadmapListPanel sourceType="my" dataVersion={dataVersion} keyword={roadmapKeyword} />
                                </div>
                            )}
                        </Tabs.TabPane>
                        <Tabs.TabPane tab={<h2><EyeOutlined />&nbsp;最近访问</h2>} key="access">
                            {activeKey == "access" && (
                                <div className={s.content_wrap}>
                                    <AccessListPanel />
                                </div>
                            )}
                        </Tabs.TabPane>
                        <Tabs.TabPane tab={<h2><BookOutlined />&nbsp;学习笔记</h2>} key="note">
                            {activeKey == "note" && (
                                <div className={s.content_wrap}>
                                    <NoteListPanel roadmapId={curNoteRoadmapId} />
                                </div>
                            )}
                        </Tabs.TabPane>
                        <Tabs.TabPane tab={<h2><UnorderedListOutlined />&nbsp;成长记录</h2>} key="record">
                            {activeKey == "record" && (
                                <div className={s.content_wrap}>
                                    <UserRoadmapEvListPanel />
                                </div>
                            )}
                        </Tabs.TabPane>
                    </>
                )}
            </Tabs>
            {showCreateModal == true && (
                <EditRoadmapModal onCancel={() => setShowCreateModal(false)} onOk={() => {
                    setShowCreateModal(false);
                    setRoadmapKeyword("");
                    setDataVersion(dataVersion + 1);
                }} />
            )}
        </div>
    );
};

export default observer(GrowCenter);