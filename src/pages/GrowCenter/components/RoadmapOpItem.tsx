//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from "react";
import type { TOPIC_STATE_TYPE, StateOpItem } from "@/api/roadmap_state";
import { STATE_OP_REMOVE, STATE_OP_SET, TOPIC_STATE_DOING, TOPIC_STATE_DONE, TOPIC_STATE_SKIP } from "@/api/roadmap_state";
import { Button, Space } from "antd";
import UserPhoto from "@/components/Portrait/UserPhoto";
import moment from "moment";
import { openRoadmapView } from "@/utils/roadmap";

export interface RoadmapOpItemProps {
    opItem: StateOpItem;
    showUser: boolean;
    sessionId: string;
}

const RoadmapOpItem = (props: RoadmapOpItemProps) => {
    const getOpTypeStr = () => {
        if (props.opItem.state_data.UserNoteState == undefined) {
            if (props.opItem.op_type == STATE_OP_SET) {
                return "设置状态";
            } else if (props.opItem.op_type == STATE_OP_REMOVE) {
                return "移除状态";
            }
        } else {
            if (props.opItem.op_type == STATE_OP_SET) {
                return "更新学习笔记";
            } else if (props.opItem.op_type == STATE_OP_REMOVE) {
                return "删除学习笔记";
            }
        }
        return "";
    };

    const getTopicStateStr = (state: TOPIC_STATE_TYPE) => {
        if (state == TOPIC_STATE_DOING) {
            return "学习中";
        } else if (state == TOPIC_STATE_DONE) {
            return "已掌握";
        } else if (state == TOPIC_STATE_SKIP) {
            return "忽略";
        }
        return "";
    };

    const openRoadmapWindow = async (nodeId: string) => {
        await openRoadmapView(props.opItem.roadmap_id, props.opItem.roadmap_title, props.sessionId, false, props.opItem.can_update_roadmap, nodeId, props.opItem.state_data.UserNoteState != undefined);
    };

    return (
        <Space style={{ flexWrap: "wrap" }} size="small">
            {moment(props.opItem.time_stamp).format("YYYY-MM-DD HH:mm:ss")}
            {props.showUser && (
                <Space>
                    <UserPhoto logoUri={props.opItem.user_logo_uri} width="20px" height="20px" style={{ borderRadius: "10px" }} />
                    {props.opItem.user_display_name}
                </Space>
            )}
            对成长路线<Button type="link" disabled={props.opItem.roadmap_exist == false || props.sessionId == ""} style={{ minWidth: "0px", padding: "0px 0px" }}
                onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    openRoadmapWindow("");
                }}>{props.opItem.roadmap_title}</Button>
            {props.opItem.state_data.TopicState != undefined && (
                <>
                    节点<Button type="link" disabled={props.opItem.roadmap_exist == false || props.sessionId == ""} style={{ minWidth: "0px", padding: "0px 0px" }}
                        onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            openRoadmapWindow(props.opItem.node_id);
                        }}>{props.opItem.state_data.TopicState.title}</Button>
                </>
            )}
            {props.opItem.state_data.UserNoteState != undefined && (
                <>
                    节点<Button type="link" disabled={props.opItem.roadmap_exist == false || props.sessionId == ""} style={{ minWidth: "0px", padding: "0px 0px" }}
                        onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            openRoadmapWindow(props.opItem.node_id);
                        }}>{props.opItem.state_data.UserNoteState.title}</Button>
                </>
            )}
            {getOpTypeStr()}
            {props.opItem.state_data.TopicState !== undefined && getTopicStateStr(props.opItem.state_data.TopicState.state)}
            {props.opItem.state_data.TodoListState !== undefined && `完成${(props.opItem.state_data.TodoListState.done_list.map(item => item.title)).join(" ")}`}
        </Space>
    );
}

export default RoadmapOpItem;