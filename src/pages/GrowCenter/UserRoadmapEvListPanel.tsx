//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { observer } from 'mobx-react';
import { List } from "antd";
import { useStores } from "@/hooks";
import type { StateOpItem } from "@/api/roadmap_state";
import { list_my_log } from "@/api/roadmap_state";
import { request } from "@/utils/request";
import RoadmapOpItem from "./components/RoadmapOpItem";

const PAGE_SIZE = 20;

const UserRoadmapEvListPanel = () => {
    const userStore = useStores("userStore");

    const [opItemList, setOpItemList] = useState<StateOpItem[]>([]);
    const [totalCount, setTotalCount] = useState(0);
    const [curPage, setCurPage] = useState(0);

    const loadOpItemList = async () => {
        const res = await request(list_my_log({
            session_id: userStore.sessionId,
            offset: curPage * PAGE_SIZE,
            limit: PAGE_SIZE,
        }));
        setOpItemList(res.op_list);
        setTotalCount(res.total_count);
    };

    useEffect(() => {
        loadOpItemList();
    }, [curPage]);

    return (
        <List rowKey="op_id" dataSource={opItemList}
            style={{ height: "calc(100vh - 130px)", overflowY: "scroll" }}
            pagination={{ current: curPage + 1, total: totalCount, pageSize: PAGE_SIZE, onChange: page => setCurPage(page - 1), showSizeChanger: false }}
            split={false}
            renderItem={opItem => (
                <List.Item>
                    <RoadmapOpItem opItem={opItem} showUser={false} sessionId={userStore.sessionId} />
                </List.Item>
            )} />
    );
};

export default observer(UserRoadmapEvListPanel);