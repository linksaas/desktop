//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { observer } from 'mobx-react';
import { useStores } from "@/hooks";
import type { NoteInfo } from "@/api/roadmap_user";
import { list_my_note, remove_note } from "@/api/roadmap_user";
import { request } from "@/utils/request";
import { Button, Card, List, message, Modal, Popover, Space } from "antd";
import { ReadOnlyEditor } from "@/components/Editor";
import { openRoadmapView } from "@/utils/roadmap";
import moment from "moment";
import { DeleteOutlined, MoreOutlined } from "@ant-design/icons";

const PAGE_SIZE = 10;

export interface NoteListPanelProps {
    roadmapId: string;
}

const NoteListPanel = (props: NoteListPanelProps) => {
    const userStore = useStores('userStore');

    const [noteList, setNoteList] = useState<NoteInfo[]>([]);
    const [totalCount, setTotalCount] = useState(0);
    const [curPage, setCurPage] = useState(0);
    const [removeNoteInfo, setRemoveNoteInfo] = useState<NoteInfo | null>(null);

    const loadNoteList = async () => {
        const res = await request(list_my_note({
            session_id: userStore.sessionId,
            filter_by_roadmap_id: props.roadmapId != "",
            roadmap_id: props.roadmapId,
            offset: PAGE_SIZE * curPage,
            limit: PAGE_SIZE,
        }));
        setNoteList(res.note_list);
        setTotalCount(res.total_count);
    };

    const removeNote = async () => {
        if (removeNoteInfo == null) {
            return;
        }
        await request(remove_note({
            session_id: userStore.sessionId,
            roadmap_id: removeNoteInfo.roadmap_id,
            node_id: removeNoteInfo.node_id,
        }));
        await loadNoteList();
        message.info("删除成功");
        setRemoveNoteInfo(null);
    };

    const openRoadmapWindow = async (noteItem: NoteInfo, showNote: boolean) => {
        await openRoadmapView(noteItem.roadmap_id, noteItem.roadmap_title, userStore.sessionId, false, noteItem.can_update_roadmap, showNote ? noteItem.node_id : "", showNote);
    };

    useEffect(() => {
        loadNoteList();
    }, [userStore.sessionId, curPage]);

    useEffect(() => {
        if (curPage != 0) {
            setCurPage(0);
        } else {
            loadNoteList();
        }
    }, [props.roadmapId]);

    return (
        <>
            <List rowKey="node_id" dataSource={noteList}
                style={{ height: "calc(100vh - 130px)", overflowY: "scroll", padding: "10px 10px", overflowX: "hidden" }}
                pagination={{ current: curPage + 1, total: totalCount, pageSize: PAGE_SIZE, onChange: page => setCurPage(page - 1), showSizeChanger: false }}
                renderItem={noteItem => (
                    <Card title={
                        <Space>
                            <Button type="link" disabled={!noteItem.roadmap_exist} onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                openRoadmapWindow(noteItem, false);
                            }}>{noteItem.roadmap_title}</Button>
                            /
                            <Button type="link" disabled={!noteItem.roadmap_exist} onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                openRoadmapWindow(noteItem, true);
                            }}>{noteItem.node_title}</Button>
                        </Space>
                    } style={{ marginBottom: "10px" }} bordered={false} headStyle={{ backgroundColor: "#eee" }}
                        extra={
                            <Space>
                                {moment(noteItem.time_stamp).format("YYYY-MM-DD HH:mm:ss")}
                                <Popover placement="bottom" trigger="click" content={
                                    <Space direction="vertical">
                                        <Button type="link" danger icon={<DeleteOutlined />} onClick={e => {
                                            e.stopPropagation();
                                            e.preventDefault();
                                            setRemoveNoteInfo(noteItem);
                                        }}>删除</Button>
                                    </Space>
                                }>
                                    <MoreOutlined />
                                </Popover>
                            </Space>
                        }>
                        <ReadOnlyEditor content={noteItem.content} />
                    </Card>
                )} />
            {removeNoteInfo != null && (
                <Modal open title="删除学习笔记"
                    okText="删除" okButtonProps={{ danger: true }}
                    onCancel={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setRemoveNoteInfo(null);
                    }}
                    onOk={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        removeNote();
                    }}>
                    是否删除学习笔记&nbsp;{removeNoteInfo.roadmap_title}/{removeNoteInfo.node_title}&nbsp;?
                </Modal>
            )}
        </>
    );
};

export default observer(NoteListPanel);