//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useRef, useState } from "react";
import { Button, Space, Table } from "antd";
import type { LocalRepoInfo, LocalRepoCommitInfo, LocalRepoBranchInfo, CommitGraphInfo, LocalRepoTagInfo, HeadInfo } from "@/api/local_repo";
import { list_commit_graph, list_repo_commit, list_repo_branch, list_repo_tag } from "@/api/local_repo";
import type { ColumnsType } from 'antd/lib/table';
import moment from "moment";
import { WebviewWindow, appWindow } from '@tauri-apps/api/window';
import { createGitgraph, type CommitOptions, MergeStyle } from "@gitgraph/js";
import CreateTagModal from "./CreateTagModal";
import CreateBranchModal from "./CreateBranchModal";
import "./CommitList.css";

export interface CommitListProps {
    repo: LocalRepoInfo;
    headInfo: HeadInfo;
    showType: "list" | "graph";
}

const CommitList = (props: CommitListProps) => {
    const graphRef = useRef<HTMLDivElement>(null);

    const [commitList, setCommitList] = useState<LocalRepoCommitInfo[]>([]);

    const [branchList, setBranchList] = useState<LocalRepoBranchInfo[]>([]);
    const [tagList, setTagList] = useState<LocalRepoTagInfo[]>([]);

    const [newTagCommitId, setNewTagCommitId] = useState("");
    const [newBranchCommitId, setNewBranchCommitId] = useState("");

    const loadCommitList = async () => {
        const refName = props.headInfo.detached ? `refs/tags/${props.headInfo.tag_name}` : `refs/heads/${props.headInfo.branch_name}`;
        console.log(refName);
        const commitRes = await list_repo_commit(props.repo.path, refName);
        setCommitList(commitRes);
        const tmpList: string[] = [];
        for (const commit of commitRes) {
            if (!tmpList.includes(commit.commiter)) {
                tmpList.push(commit.commiter);
            }
        }
    };

    const loadBranchList = async () => {
        const tmpList = await list_repo_branch(props.repo.path);
        setBranchList(tmpList);
    };

    const loadTagList = async () => {
        const tmpList = await list_repo_tag(props.repo.path);
        setTagList(tmpList);
    };

    const openCommitDiff = async (row: LocalRepoCommitInfo) => {
        const pos = await appWindow.innerPosition();
        new WebviewWindow(`commit:${row.id}`, {
            url: `git_diff.html?path=${encodeURIComponent(props.repo.path)}&commitId=${row.id}&summary=${encodeURIComponent(row.summary)}&commiter=${encodeURIComponent(row.commiter)}`,
            title: `${props.repo.name}(commit:${row.id.substring(0, 8)})`,
            x: pos.x + Math.floor(Math.random() * 200),
            y: pos.y + Math.floor(Math.random() * 200),
        })
    };

    const openCommitDiff2 = async (row: CommitGraphInfo) => {
        const pos = await appWindow.innerPosition();
        new WebviewWindow(`commit:${row.hash}`, {
            url: `git_diff.html?path=${encodeURIComponent(props.repo.path)}&commitId=${row.hash}&summary=${encodeURIComponent(row.subject)}&commiter=${encodeURIComponent(row.committer.name)}`,
            title: `${props.repo.name}(commit:${row.hash.substring(0, 8)})`,
            x: pos.x + Math.floor(Math.random() * 200),
            y: pos.y + Math.floor(Math.random() * 200),
        })
    };

    const initGraph = async () => {
        if (graphRef == null || graphRef.current == null) {
            return;
        }
        graphRef.current.innerText = "";
        const gitgraph = createGitgraph(graphRef.current, {
            responsive: true,
            template: {
                colors: ["#6963FF", "#47E8D4", "#6BDB52", "#E84BA5", "#FFA657"],
                branch: {
                    lineWidth: 2,
                    spacing: 20,
                    mergeStyle: MergeStyle.Straight,
                    label: {
                        display: true,
                        bgColor: "white",
                        borderRadius: 10,
                    },
                },
                commit: {
                    spacing: 40,
                    hasTooltipInCompactMode: true,
                    dot: {
                        size: 4,
                        font: "normal 14px monospace",
                    },
                    message: {
                        color: "black",
                        display: true,
                        displayAuthor: false,
                        displayHash: true,
                        font: "normal 14px monospace",
                    },
                },
                arrow: {
                    color: null,
                    size: 8,
                    offset: -1.5,
                },
                tag: {},
            },
        });
        const commitList = await list_commit_graph(props.repo.path, props.headInfo.commit_id);
        for (const commit of commitList) {
            const options = commit as any as CommitOptions;
            options.onClick = () => {
                openCommitDiff2(commit);
            };
            options.onMessageClick = () => {
                openCommitDiff2(commit);
            };
        }
        gitgraph.import(commitList);
    };

    const columns: ColumnsType<LocalRepoCommitInfo> = [
        {
            title: "",
            width: 60,
            render: (_, row: LocalRepoCommitInfo) => (
                <span title={row.id}>{row.id.substring(0, 8)}</span>
            ),
        },
        {
            title: "提交备注",
            width: 200,
            render: (_, row: LocalRepoCommitInfo) => (
                <a onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    openCommitDiff(row);
                }}>{row.summary}</a>
            ),
        },
        {
            title: "分支",
            width: 100,
            render: (_, row: LocalRepoCommitInfo) => (
                <>
                    {branchList.filter(item => item.commit_id == row.id).map(item => (
                        <div>{item.name}</div>
                    ))}
                </>
            ),
        },
        {
            title: "标记",
            width: 100,
            render: (_, row: LocalRepoCommitInfo) => tagList.find(item => item.commit_id == row.id)?.name ?? "",
        },
        {
            title: "提交时间",
            width: 100,
            render: (_, row: LocalRepoCommitInfo) => `${moment(row.time_stamp).format("YYYY-MM-DD HH:mm")}`,
        },
        {
            title: "提交者",
            width: 100,
            render: (_, row: LocalRepoCommitInfo) => `${row.commiter}(${row.email})`,
        },
        {
            title: "操作",
            width: 150,
            render: (_, row: LocalRepoCommitInfo) => (
                <Space>
                    <Button type="link" style={{ minWidth: 0, padding: "0px 0px" }} onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setNewBranchCommitId(row.id);
                    }}>创建分支</Button>
                    {tagList.findIndex(item => item.commit_id == row.id) == -1 && (
                        <Button type="link" style={{ minWidth: 0, padding: "0px 0px" }} onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            setNewTagCommitId(row.id);
                        }}>创建标记</Button>
                    )}
                </Space>
            ),
        }
    ];


    useEffect(() => {
        if (props.headInfo.branch_name != "" || props.headInfo.tag_name != "") {
            if (props.showType == "list") {
                loadCommitList();
            } else if (props.showType == "graph" && graphRef != null && graphRef.current != null) {
                initGraph();
            }
        }
    }, [props.headInfo.branch_name, props.headInfo.tag_name, props.showType, graphRef]);

    useEffect(() => {
        loadBranchList();
        loadTagList();
    }, []);

    return (
        <div style={{ height: "calc(100vh - 50px)", overflow: "scroll" }}>
            {props.showType == "list" && (
                <Table rowKey="id" dataSource={commitList} columns={columns} pagination={{ pageSize: 20, showSizeChanger: false }} />
            )}
            {props.showType == "graph" && (<div ref={graphRef} />)}
            {newTagCommitId != "" && (
                <CreateTagModal repo={props.repo} commitId={newTagCommitId} onCancel={() => setNewTagCommitId("")}
                    onOk={() => {
                        setNewTagCommitId("");
                        loadTagList().then(() => loadCommitList());
                    }} />
            )}
            {newBranchCommitId != "" && (
                <CreateBranchModal repo={props.repo} commitId={newBranchCommitId} onCancel={() => setNewBranchCommitId("")}
                    onOk={() => {
                        setNewBranchCommitId("");
                        loadBranchList().then(() => loadCommitList());
                    }} />
            )}
        </div>
    );
};

export default CommitList;