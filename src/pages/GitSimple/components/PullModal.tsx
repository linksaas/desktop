//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { Modal, Form, Select, Radio, Input, message, Progress } from "antd";
import { homeDir, resolve } from '@tauri-apps/api/path';
import type { RemoteInfo } from "@/api/local_repo";
import { list_remote, list_ssh_key_name } from "@/api/local_repo";
import type { AUTH_TYPE, GitProgressItem } from "@/api/git_wrap";
import { pull as run_pull, run_status, test_ssh } from "@/api/git_wrap";
import { useTranslation } from "react-i18next";

interface PullModalProps {
    userName: string;
    headBranch: string;
    repoPath: string;
    onClose: () => void;
}

const PullModal = (props: PullModalProps) => {
    const { t } = useTranslation();

    const [curRemote, setCurRemote] = useState<RemoteInfo | null>(null);
    const [remoteList, setRemoteList] = useState([] as RemoteInfo[]);

    const [authType, setAuthType] = useState<AUTH_TYPE>("none");
    const [username, setUsername] = useState(props.userName);
    const [password, setPassword] = useState("");

    const [sshKeyNameList, setSshKeyNameList] = useState([] as string[]);
    const [curSshKey, setCurSshKey] = useState("");

    const [inPull, setInPull] = useState(false);

    const [pullProgress, setPullProgress] = useState<GitProgressItem | null>(null);

    const loadRemoteList = async () => {
        const res = await list_remote(props.repoPath);
        setRemoteList(res);
        const index = res.findIndex(item => item.name == "origin");
        if (index != -1) {
            setCurRemote(res[index]);
        } else {
            if (res.length > 0) {
                setCurRemote(res[0]);
            }
        }
    };

    const runPull = async () => {
        //检查是否有文件变更
        const statusList = await run_status(props.repoPath);
        if (statusList.length > 0) {
            message.warn("本地有文件修改未提交，请先提交文件");
            return;
        }

        const home = await homeDir();
        const privKeyPath = await resolve(home, ".ssh", curSshKey);
        if (authType == "sshKey") {
            await test_ssh(curRemote?.url ?? "", privKeyPath);
        }
        setInPull(true);
        setPullProgress({
            stage: "拉取中",
            doneCount: 0,
            totalCount: 1,
        })
        try {
            await run_pull(props.repoPath, curRemote?.name ?? "", props.headBranch, authType, username, password, privKeyPath, info => {
                setPullProgress(info);
                if (info == null) {
                    setInPull(false);
                    message.info("拉取成功");
                    props.onClose();
                }
            });
        } catch (e) {
            message.error(`${e}`);
            setInPull(false);
            setPullProgress(null);
        }
    };

    useEffect(() => {
        loadRemoteList();
    }, [props.repoPath]);

    useEffect(() => {
        if (curRemote == null) {
            return;
        }
        const remoteUrl = curRemote.url;
        if (remoteUrl.startsWith("git@")) {
            setAuthType("sshKey");
        } else if (remoteUrl.startsWith("http")) {
            if (authType == "sshKey") {
                setAuthType("none");
            }
        }
    }, [curRemote?.url]);

    useEffect(() => {
        list_ssh_key_name().then(res => {
            setSshKeyNameList(res);
            if (res.length > 0) {
                setCurSshKey(res[0]);
            }
        });
    }, []);

    return (
        <Modal open title="拉取" width={800} mask={false}
            okText="拉取" okButtonProps={{ disabled: (curRemote == null) || inPull }}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onClose();
            }}
            onOk={e => {
                e.stopPropagation();
                e.preventDefault();
                runPull();
            }}>
            <Form labelCol={{ span: 4 }}>
                <Form.Item label="远程仓库">
                    <Select value={curRemote?.name ?? ""} onChange={value => {
                        const tmpItem = remoteList.find(item => item.name == value);
                        setCurRemote(tmpItem ?? null);
                    }}>
                        {remoteList.map(item => (
                            <Select.Option key={item.name} value={item.name}>{item.name}</Select.Option>
                        ))}
                    </Select>
                </Form.Item>
                {curRemote != null && (
                    <Form.Item label="远程地址">
                        {curRemote.url}
                    </Form.Item>
                )}
                {(curRemote != null) && (curRemote.url.startsWith("git@") || curRemote.url.startsWith("http")) && (
                    <Form.Item label="验证方式">
                        <Radio.Group value={authType} onChange={e => {
                            e.stopPropagation();
                            setAuthType(e.target.value);
                        }}>
                            <Radio value="none" disabled={curRemote.url.startsWith("git@")}>无需验证</Radio>
                            <Radio value="password" disabled={curRemote.url.startsWith("git@")}>账号密码</Radio>
                            <Radio value="sshKey" disabled={curRemote.url.startsWith("http")}>SSH公钥</Radio>
                        </Radio.Group>
                    </Form.Item>
                )}
                {authType == "password" && (
                    <>
                        <Form.Item label="账号">
                            <Input value={username} onChange={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setUsername(e.target.value.trim());
                            }} placeholder="请输入远程Git仓库账号" />
                        </Form.Item>
                        <Form.Item label="密码">
                            <Input.Password value={password} onChange={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setPassword(e.target.value.trim());
                            }} placeholder="请输入账号密码或双重认证密码" />
                        </Form.Item>
                    </>
                )}
                {authType == "sshKey" && (
                    <Form.Item label={t("text.sshKeys")}>
                        <Select value={curSshKey} onChange={key => setCurSshKey(key)}>
                            {sshKeyNameList.map(sshName => (
                                <Select.Option key={sshName} value={sshName}>{sshName}</Select.Option>
                            ))}
                        </Select>
                    </Form.Item>
                )}
                {pullProgress != null && (
                    <Form.Item label={pullProgress.stage}>
                        <Progress percent={pullProgress.totalCount == 0 ? 0 : (pullProgress.doneCount * 100 / pullProgress.totalCount)} showInfo={false} />
                    </Form.Item>
                )}
            </Form>
        </Modal>
    );
};

export default PullModal;
