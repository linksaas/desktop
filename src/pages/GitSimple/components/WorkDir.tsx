//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { Button, Card, List, Space, Breadcrumb } from "antd";
import { FileOutlined, FolderOutlined } from "@ant-design/icons";
import { readDir, type FileEntry } from '@tauri-apps/api/fs';
import { resolve } from '@tauri-apps/api/path';
import { type WidgetInfo } from "@/api/widget";
import GitFile from "./GitFile";

interface WorkDirProps {
    basePath: string;
    headCommitId: string;
    widgetList: WidgetInfo[];
}

const WorkDir = (props: WorkDirProps) => {
    const [curDirList, setCurDirList] = useState([] as string[]);
    const [fileEntryList, setFileEntryList] = useState([] as FileEntry[]);

    const loadFileEntryList = async () => {
        const path = await resolve(props.basePath, ...curDirList);
        const tmpList = await readDir(path);
        setFileEntryList(tmpList.filter(item => item.name != null && item.name != ".git"));
    };

    useEffect(() => {
        loadFileEntryList();
    }, [curDirList]);

    useEffect(() => {
        if (curDirList.length != 0) {
            setCurDirList([]);
        } else {
            loadFileEntryList();
        }
    }, [props.headCommitId]);

    return (
        <Card bordered={false} bodyStyle={{ height: "calc(100vh - 340px)", overflow: "scroll", paddingTop: "2px" }}
            headStyle={{ backgroundColor: "#eee", height: "40px" }}
            title={
                <Space>
                    <span>当前路径:</span>
                    <Breadcrumb>
                        <Breadcrumb.Item>
                            <Button type="link" disabled={curDirList.length == 0} onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setCurDirList([]);
                            }}>
                                根目录
                            </Button>
                        </Breadcrumb.Item>
                        {curDirList.map((name, index) => (
                            <Breadcrumb.Item key={index}>
                                <Button type="link" disabled={(index + 1) == curDirList.length}
                                    onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        setCurDirList(curDirList.slice(0, index + 1));
                                    }}>
                                    {name}
                                </Button>
                            </Breadcrumb.Item>
                        ))}
                    </Breadcrumb>
                </Space>
            }>
            <List rowKey="name" dataSource={fileEntryList} pagination={false}
                grid={{ gutter: 16 }}
                renderItem={entry => (
                    <List.Item style={{ width: "250px" }}>
                        <Space style={{ fontSize: "16px" }}>
                            {entry.children != null && <FolderOutlined />}
                            {entry.children == null && <FileOutlined />}
                            {entry.children != null && (
                                <a onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    setCurDirList([...curDirList, entry.name ?? ""]);
                                }} title={entry.name ?? ""} style={{ display: "inline-block", width: "200px", overflow: "hidden", textAlign: "left", textOverflow: "ellipsis", whiteSpace: "nowrap" }}>{entry.name}</a>
                            )}
                            {entry.children == null && (
                                <GitFile basePath={props.basePath} curDirList={curDirList} curFileName={entry.name ?? ""} widgetList={props.widgetList} />
                            )}
                        </Space>
                    </List.Item>
                )} />

        </Card>
    );
};

export default WorkDir;