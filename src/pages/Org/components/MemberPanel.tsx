//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { MemberInfo } from "@/api/org_mebmer";
import { Button, Tabs } from "antd";
import { observer } from 'mobx-react';
import OkrList, { EditModal as EditOkrModal } from "./OkrList";
import DayReportList, { EditModal as EditDayReportModal } from "./DayReportList";
import WeekReportList, { EditModal as EditWeekReportModal } from "./WeekReportList";
import { PlusOutlined } from "@ant-design/icons";
import { useStores } from "@/hooks";
import UserContentList from "./UserContentList";
import MemberResume from "./MemberResume";
import EvalList from "./evaluate/EvalList";

export interface MemberPanelProps {
    curMember: MemberInfo;
    onGotoForum: (newForumId: string, newThreadId: string) => void;
}

const MemberPanel = (props: MemberPanelProps) => {
    const userStore = useStores('userStore');
    const orgStore = useStores("orgStore");
    const appStore = useStores("appStore");

    const [showCreateModal, setShowCreateModal] = useState(false);
    const [activeKey, setActiveKey] = useState<"dayReport" | "weekReport" | "okr" | "contentList" | "resume" | "eval" | "">("");

    const [dayReportDataVersion, setDayReportDataVersion] = useState(0);
    const [weekReportDataVersion, setWeekReportDataVersion] = useState(0);
    const [okrDataVersion, setOkrDataVersion] = useState(0);


    useEffect(() => {
        if (orgStore.curOrg?.setting.enable_day_report == true) {
            setActiveKey("dayReport");
        } else if (orgStore.curOrg?.setting.enble_week_report == true) {
            setActiveKey("weekReport");
        } else if (orgStore.curOrg?.setting.enable_okr == true) {
            setActiveKey("okr");
        } else if (orgStore.curOrg?.setting.enable_evaluate == true && props.curMember.parent_depart_ment_id != orgStore.curOrg.new_member_depart_ment_id) {
            setActiveKey("eval");
        } else if (appStore.vendorCfg?.org.enable_forum == true) {
            setActiveKey("contentList");
        } else if (appStore.vendorCfg?.work_bench.enable_user_resume && props.curMember.has_resume) {
            setActiveKey("resume");
        }
    }, []);

    return (
        <>
            <Tabs activeKey={activeKey} onChange={key => setActiveKey(key as "dayReport" | "weekReport" | "okr" | "eval" | "contentList")}
                type="card"
                tabBarStyle={{ fontSize: "16px", fontWeight: 600, paddingLeft: "10px", height: "40px" }}
                tabBarExtraContent={
                    <div style={{ paddingRight: "10px" }}>
                        {activeKey == "dayReport" && props.curMember.member_user_id == userStore.userInfo.userId && (
                            <Button type="primary" icon={<PlusOutlined />} onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setShowCreateModal(true);
                            }}>创建日报</Button>
                        )}
                        {activeKey == "weekReport" && props.curMember.member_user_id == userStore.userInfo.userId && (
                            <Button type="primary" icon={<PlusOutlined />} onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setShowCreateModal(true);
                            }}>创建周报</Button>
                        )}
                        {activeKey == "okr" && props.curMember.member_user_id == userStore.userInfo.userId && (
                            <Button type="primary" icon={<PlusOutlined />} onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setShowCreateModal(true);
                            }}>创建个人目标</Button>
                        )}
                    </div>
                } >
                {orgStore.curOrg?.setting.enable_day_report == true && (
                    <Tabs.TabPane key="dayReport" tab="日报">
                        <div style={{ height: "calc(100vh - 110px)", overflowY: "scroll", padding: "10px 10px" }}>
                            {activeKey == "dayReport" && (
                                <DayReportList memberUserId={props.curMember.member_user_id} dataVersion={dayReportDataVersion} />
                            )}
                        </div>
                    </Tabs.TabPane>
                )}
                {orgStore.curOrg?.setting.enble_week_report == true && (
                    <Tabs.TabPane key="weekReport" tab="周报">
                        <div style={{ height: "calc(100vh - 110px)", overflowY: "scroll", padding: "10px 10px" }}>
                            {activeKey == "weekReport" && (
                                <WeekReportList memberUserId={props.curMember.member_user_id} dataVersion={weekReportDataVersion} />
                            )}
                        </div>
                    </Tabs.TabPane>
                )}
                {orgStore.curOrg?.setting.enable_okr == true && (
                    <Tabs.TabPane key="okr" tab="个人目标">
                        <div style={{ height: "calc(100vh - 110px)", overflowY: "scroll", padding: "10px 10px" }}>
                            {activeKey == "okr" && (
                                <OkrList memberUserId={props.curMember.member_user_id} dataVersion={okrDataVersion} />
                            )}
                        </div>
                    </Tabs.TabPane>
                )}
                {orgStore.curOrg?.setting.enable_evaluate == true && props.curMember.parent_depart_ment_id != orgStore.curOrg.new_member_depart_ment_id && (
                    <Tabs.TabPane key="eval" tab="成员互评">
                        <div style={{ height: "calc(100vh - 110px)", overflowY: "scroll", padding: "10px 10px" }}>
                            {activeKey == "eval" && (
                                <EvalList departMentId={props.curMember.parent_depart_ment_id} memberUserId={props.curMember.member_user_id} dataVersion={0} />
                            )}
                        </div>
                    </Tabs.TabPane>
                )}
                {appStore.vendorCfg?.org.enable_forum == true && (
                    <Tabs.TabPane key="contentList" tab="讨论记录">
                        <div style={{ height: "calc(100vh - 110px)", overflowY: "scroll", padding: "10px 10px" }}>
                            {activeKey == "contentList" && (
                                <UserContentList memberUserId={props.curMember.member_user_id}
                                    onClick={(newForumId, newThreadId) => {
                                        props.onGotoForum(newForumId, newThreadId);
                                    }} />
                            )}
                        </div>
                    </Tabs.TabPane>
                )}
                {appStore.vendorCfg?.work_bench.enable_user_resume && props.curMember.has_resume && (
                    <Tabs.TabPane key="resume" tab="个人信息">
                        <div style={{ height: "calc(100vh - 110px)", overflowY: "scroll", padding: "10px 10px" }}>
                            {activeKey == "resume" && (
                                <MemberResume memberUserId={props.curMember.member_user_id} />
                            )}
                        </div>
                    </Tabs.TabPane>
                )}
            </Tabs>
            {showCreateModal == true && activeKey == "dayReport" && (
                <EditDayReportModal onCancel={() => setShowCreateModal(false)} onOk={() => {
                    setDayReportDataVersion(oldValue => oldValue + 1);
                    setShowCreateModal(false);
                }} />
            )}
            {showCreateModal == true && activeKey == "weekReport" && (
                <EditWeekReportModal onCancel={() => setShowCreateModal(false)} onOk={() => {
                    setWeekReportDataVersion(oldValue => oldValue + 1);
                    setShowCreateModal(false);
                }} />
            )}
            {showCreateModal == true && activeKey == "okr" && (
                <EditOkrModal onCancel={() => setShowCreateModal(false)} onOk={() => {
                    setOkrDataVersion(oldValue => oldValue + 1);
                    setShowCreateModal(false);
                }} />
            )}
        </>
    );
};

export default observer(MemberPanel);