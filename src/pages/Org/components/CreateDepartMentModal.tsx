//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from "react";
import { create_depart_ment,  } from "@/api/org";
import { Form, Input, message, Modal } from "antd";
import { useStores } from "@/hooks";
import { request } from "@/utils/request";


export interface CreateDepartMentModalProps {
    parentDepartMentId: string;
    onClose: () => void;
}

const CreateDepartMentModal = (props: CreateDepartMentModalProps) => {
    const userStore = useStores('userStore');
    const orgStore = useStores('orgStore');

    const [name, setName] = useState("");

    const createDepartMent = async () => {
        const res = await request(create_depart_ment({
            session_id: userStore.sessionId,
            org_id: orgStore.curOrgId,
            parent_depart_ment_id: props.parentDepartMentId,
            depart_ment_name: name,
        }));
        await orgStore.onUpdateDepartMent(orgStore.curOrgId, res.depart_ment_id);
        if (props.parentDepartMentId != "") {
            await orgStore.onUpdateDepartMent(orgStore.curOrgId, props.parentDepartMentId);
        }
        message.info("创建成功");
        props.onClose();
    };

    return (
        <Modal open title="创建部门" mask={false}
            okText="创建" okButtonProps={{ disabled: name == "" }}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onClose();
            }}
            onOk={e => {
                e.stopPropagation();
                e.preventDefault();
                createDepartMent();
            }}>
            <Form>
                <Form.Item label="部门名称">
                    <Input value={name} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setName(e.target.value.trim());
                    }} />
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default CreateDepartMentModal;