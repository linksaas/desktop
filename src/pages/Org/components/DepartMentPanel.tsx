//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from "react";
import { observer } from 'mobx-react';
import type { DepartMentInfo, DepartMentOrMember } from "@/api/org";
import { Button, Tabs } from "antd";
import { useStores } from "@/hooks";
import DepartMentList from "./DepartMentList";
import MemberList from "./MemberList";
import { PlusOutlined } from "@ant-design/icons";
import EditEvalModal from "./evaluate/EditEvalModal";
import EvalList from "./evaluate/EvalList";
import CreateDepartMentModal from "./CreateDepartMentModal";


export interface DepartMentPanelProps {
    curDepartMent?: DepartMentInfo;
    onSelect: (newItem: DepartMentOrMember) => void;
}

const DepartMentPanel = (props: DepartMentPanelProps) => {
    const userStore = useStores("userStore");
    const orgStore = useStores('orgStore');

    const [activeKey, setActiveKey] = useState<"member" | "department" | "eval" | "">("member");

    const [showAddDepartMentModal, setShowAddDepartMentModal] = useState(false);

    const [showAddEvalModal, setShowAddEvalModal] = useState(false);
    const [evalDataVersion, setEvalDataVersion] = useState(0);

    return (
        <>
            <Tabs activeKey={activeKey} onChange={key => setActiveKey(key as "member" | "department"| "eval" | "")}
                type="card"
                tabBarStyle={{ fontSize: "16px", fontWeight: 600, paddingLeft: "10px", height: "40px" }}
                tabBarExtraContent={
                    <div style={{ paddingRight: "10px" }}>
                        {activeKey == "department" && orgStore.curOrg?.owner_user_id == userStore.userInfo.userId && props.curDepartMent?.depart_ment_id != orgStore.curOrg.new_member_depart_ment_id && (
                            <Button type="primary" icon={<PlusOutlined />} onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setShowAddDepartMentModal(true);
                            }}>创建部门</Button>
                        )}
                        {activeKey == "eval" && (orgStore.curOrg?.owner_user_id == userStore.userInfo.userId ||
                            (orgStore.selfMember?.member_perm_info.manage_depart_ment_evaluate == true && orgStore.selfMember.parent_depart_ment_id == props.curDepartMent?.depart_ment_id)) && (
                                <Button type="primary" icon={<PlusOutlined />} onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    setShowAddEvalModal(true);
                                }}>增加评估</Button>
                            )}
                    </div>
                }>
                <Tabs.TabPane key="member" tab="成员列表">
                    <div style={{ height: "calc(100vh - 110px)", overflowY: "scroll", padding: "10px 10px" }}>
                        {activeKey == "member" && (
                            <MemberList parentDepartMentId={props.curDepartMent?.depart_ment_id ?? ""} onSelect={newItem => props.onSelect(newItem)} />
                        )}
                    </div>
                </Tabs.TabPane>
                <Tabs.TabPane key="department" tab="下级部门列表">
                    <div style={{ height: "calc(100vh - 110px)", overflowY: "scroll", padding: "10px 10px" }}>
                        {activeKey == "department" && (
                            <DepartMentList parentDepartMentId={props.curDepartMent?.depart_ment_id ?? ""} onSelect={newItem => props.onSelect(newItem)} />
                        )}
                    </div>
                </Tabs.TabPane>
                {orgStore.curOrg?.setting.enable_evaluate == true && props.curDepartMent?.depart_ment_id != orgStore.curOrg.new_member_depart_ment_id &&
                    (orgStore.curOrg.owner_user_id == userStore.userInfo.userId || orgStore.selfMember?.member_perm_info.view_all_evaluate ||
                        (orgStore.selfMember?.parent_depart_ment_id == props.curDepartMent?.depart_ment_id)) && (
                        <Tabs.TabPane key="eval" tab="成员互评">
                            <div style={{ height: "calc(100vh - 110px)", overflowY: "scroll", padding: "10px 10px" }}>
                                {activeKey == "eval" && (
                                    <EvalList departMentId={props.curDepartMent?.depart_ment_id ?? ""} dataVersion={evalDataVersion} />
                                )}
                            </div>
                        </Tabs.TabPane>
                    )}
            </Tabs>
            {showAddDepartMentModal == true && (
                <CreateDepartMentModal parentDepartMentId={props.curDepartMent?.depart_ment_id ?? ""} onClose={() => setShowAddDepartMentModal(false)} />
            )}
            {showAddEvalModal == true && (
                <EditEvalModal departMentId={props.curDepartMent?.depart_ment_id ?? ""} onCancel={() => setShowAddEvalModal(false)}
                    onOk={() => {
                        setShowAddEvalModal(false);
                        setEvalDataVersion(evalDataVersion + 1);
                    }} />
            )}
        </>
    );
};

export default observer(DepartMentPanel);