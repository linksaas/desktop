//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from "react";
import { Checkbox, message, Modal, Space } from "antd";
import { useStores } from "@/hooks";
import type { MemberInfo, MemberPermInfo } from "@/api/org_mebmer";
import { update_member_perm } from "@/api/org_mebmer";
import { request } from "@/utils/request";


export interface ChangeMemberPermModalProps {
    memberInfo: MemberInfo;
    onClose: () => void;
}

const ChangeMemberPermModal = (props: ChangeMemberPermModalProps) => {
    const userStore = useStores("userStore");
    const orgStore = useStores("orgStore");

    const [permInfo, setPermInfo] = useState<MemberPermInfo>({
        manage_depart_ment_perm: props.memberInfo.member_perm_info.manage_depart_ment_perm,
        manage_evaluate_target: props.memberInfo.member_perm_info.manage_evaluate_target,
        view_all_evaluate: props.memberInfo.member_perm_info.view_all_evaluate,
        manage_depart_ment_evaluate: props.memberInfo.member_perm_info.manage_depart_ment_evaluate,
    });
    const [hasChange, setHasChange] = useState(false);

    const updatePerm = async () => {
        await request(update_member_perm({
            session_id: userStore.sessionId,
            org_id: orgStore.curOrgId,
            member_user_id: props.memberInfo.member_user_id,
            member_perm_info: permInfo,
        }));
        await orgStore.onUpdateMember(orgStore.curOrgId, props.memberInfo.member_user_id);
        message.info("设置成功");
        props.onClose();
    };

    return (
        <Modal open title="设置权限" mask={false}
            okText="设置" okButtonProps={{ disabled: !hasChange }}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onClose();
            }}
            onOk={e => {
                e.stopPropagation();
                e.preventDefault();
                updatePerm();
            }}>
            <Space direction="vertical">
                <Checkbox checked={permInfo.manage_depart_ment_perm}
                    onChange={e => {
                        e.stopPropagation();
                        setPermInfo({ ...permInfo, manage_depart_ment_perm: e.target.checked });
                        setHasChange(true);
                    }}>部门管理</Checkbox>
                <Checkbox disabled={userStore.userInfo.userId != orgStore.curOrg?.owner_user_id}
                    checked={permInfo.manage_evaluate_target}
                    onChange={e => {
                        e.stopPropagation();
                        setPermInfo({ ...permInfo, manage_evaluate_target: e.target.checked });
                        setHasChange(true);
                    }}>评估指标管理</Checkbox>
                <Checkbox disabled={userStore.userInfo.userId != orgStore.curOrg?.owner_user_id}
                    checked={permInfo.view_all_evaluate}
                    onChange={e => {
                        e.stopPropagation();
                        setPermInfo({ ...permInfo, view_all_evaluate: e.target.checked });
                        setHasChange(true);
                    }}>查看当前及下属部门团队互评</Checkbox>
                <Checkbox checked={permInfo.manage_depart_ment_evaluate}
                    onChange={e => {
                        e.stopPropagation();
                        setPermInfo({ ...permInfo, manage_depart_ment_evaluate: e.target.checked });
                        setHasChange(true);
                    }}>当前部门成员互评管理</Checkbox>
            </Space>
        </Modal>
    );
};

export default ChangeMemberPermModal;