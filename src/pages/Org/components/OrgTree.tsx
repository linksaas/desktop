//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import type { DepartMentOrMember } from "@/api/org";
import { observer } from 'mobx-react';
import { Button, Card, Popover, Space, Tree } from "antd";
import type { DataNode } from "antd/lib/tree";
import { useStores } from "@/hooks";
import { InfoCircleOutlined, MoreOutlined, RadarChartOutlined, SettingOutlined, TeamOutlined, UserAddOutlined } from "@ant-design/icons";
import UserPhoto from "@/components/Portrait/UserPhoto";
import type { MemberInfo } from "@/api/org_mebmer";
import s from "./OrgTree.module.less";
import UpdateOrgModal from "./UpdateOrgModal";
import { ReadOnlyEditor } from "@/components/Editor";
import EvalTargetModal from "./evaluate/EvalTargetModal";

export interface OrgTreeProps {
    curItem: DepartMentOrMember,
    onChange: (newItem: DepartMentOrMember) => void;
}

const OrgTree = (props: OrgTreeProps) => {
    const userStore = useStores('userStore');
    const orgStore = useStores('orgStore');
    const appStore = useStores('appStore');

    const [treeNodeList, setTreeNodeList] = useState([] as DataNode[]);

    const checkSelectMember = (myMember: MemberInfo | undefined, targetMember: MemberInfo): boolean => {
        if (myMember == undefined) {
            return false
        }
        if (myMember.member_user_id == orgStore.curOrg?.owner_user_id) {
            return true;
        }
        if (myMember.depart_ment_id_path.length > targetMember.depart_ment_id_path.length) {
            return false;
        }
        for (const item of myMember.depart_ment_id_path.entries()) {
            if (item[1] != targetMember.depart_ment_id_path[item[0]]) {
                return false;
            }
        }
        return true;
    }

    const checkSelectDepartMent = (myMember: MemberInfo | undefined, departMentId: string) => {
        if (myMember == undefined) {
            return false
        }
        if (myMember.member_user_id == orgStore.curOrg?.owner_user_id) {
            return true;
        }
        const pathIdList = myMember.depart_ment_id_path;
        if (pathIdList.length == 0) {
            return true;
        }
        if (pathIdList.includes(departMentId) && pathIdList[pathIdList.length - 1] != departMentId) {
            return false;
        }
        return true;
    }

    const setupTreeNode = (nodeList: DataNode[], parentDepartMentId: string) => {
        //处理下属成员
        for (const member of orgStore.memberList) {
            if (member.parent_depart_ment_id != parentDepartMentId) {
                continue;
            }
            const subNode: DataNode = {
                key: member.member_user_id,
                title: (
                    <Space>
                        <UserPhoto logoUri={member.logo_uri} style={{ width: "16px", borderRadius: "10px", backgroundColor: "white" }} />
                        {member.display_name}
                    </Space>
                ),
                disabled: !checkSelectMember(orgStore.selfMember, member),
            };
            nodeList.push(subNode);
        }
        //处理下属部门
        for (const departMent of orgStore.departMentList) {
            if (departMent.parent_depart_ment_id != parentDepartMentId) {
                continue;
            }
            //只有管理员可见未分配成员
            if (departMent.depart_ment_id == orgStore.curOrg?.new_member_depart_ment_id && orgStore.curOrg.owner_user_id != userStore.userInfo.userId) {
                continue;
            }
            const subNode: DataNode = {
                key: departMent.depart_ment_id,
                title: departMent.depart_ment_name,
                children: [],
                switcherIcon: () => "",
                icon: <TeamOutlined />,
                selectable: true,
                disabled: !checkSelectDepartMent(orgStore.selfMember, departMent.depart_ment_id),
            }
            nodeList.push(subNode);
            setupTreeNode(subNode.children!, departMent.depart_ment_id);
        }

    }

    const initTree = async () => {
        const tmpNodeList = [] as DataNode[];
        setupTreeNode(tmpNodeList, "");
        setTreeNodeList([{
            key: "",
            title: "总部门",
            children: tmpNodeList,
            checkable: false,
            selectable: true,
            switcherIcon: () => "",
            icon: <TeamOutlined />,
            disabled: !(orgStore.curOrg?.owner_user_id == userStore.userInfo.userId || orgStore.selfMember?.parent_depart_ment_id == ""),
        }]);
    };

    useEffect(() => {
        initTree();
    }, [orgStore.departMentList, orgStore.memberList]);

    return (
        <Card bordered={false} title={
            <>
                团队:{orgStore.curOrg?.basic_info.org_name}&nbsp;
                <Popover trigger="click" placement="bottom" content={
                    <div className="_commentContext" style={{ width: "300px" }}>
                        <h1 style={{ fontSize: "20px", fontWeight: 700 }}>团队简介</h1>
                        <ReadOnlyEditor content={orgStore.curOrg?.basic_info.org_desc ?? ""} />
                    </div>
                } destroyTooltipOnHide>
                    <InfoCircleOutlined style={{ color: "blue", fontSize: "14px" }} />
                </Popover>
            </>
        }
            headStyle={{ fontWeight: 600, backgroundColor: "#eee" }}
            className={s.treeWrap}
            extra={
                <Space size="middle">
                    {userStore.userInfo.userId == orgStore.curOrg?.owner_user_id && (
                        <Button type="primary" icon={<UserAddOutlined />} onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            orgStore.showInviteMember = true;
                        }}>邀请</Button>
                    )}
                    {userStore.userInfo.userId == orgStore.curOrg?.owner_user_id && (
                            <Popover placement="bottom" trigger="click" content={
                                <Space direction="vertical">
                                    {userStore.userInfo.userId == orgStore.curOrg?.owner_user_id && (
                                        <Button type="link" icon={<SettingOutlined />} onClick={e => {
                                            e.stopPropagation();
                                            e.preventDefault();
                                            orgStore.showUpdateOrgModal = true;
                                        }} >设置</Button>
                                    )}
                                    {orgStore.curOrg.setting.enable_evaluate == true && (userStore.userInfo.userId == orgStore.curOrg?.owner_user_id ||
                                        orgStore.selfMember?.member_perm_info.manage_evaluate_target) && (
                                            <Button type="link" icon={<RadarChartOutlined />} onClick={e => {
                                                e.stopPropagation();
                                                e.preventDefault();
                                                orgStore.showEvalTargetModal = true;
                                            }}>评估指标</Button>
                                        )}
                                </Space>
                            }>
                                <MoreOutlined />
                            </Popover>
                        )}
                </Space>
            }
            bodyStyle={{ overflowY: "scroll", height: appStore.vendorCfg?.org.enable_forum ? "calc(100vh - 310px)" : "calc(100vh - 110px)" }}>
            <Tree expandedKeys={["", ...orgStore.departMentList.map(item => item.depart_ment_id)]}
                selectedKeys={[props.curItem.id]} treeData={treeNodeList} showIcon
                onSelect={keys => {
                    const tmpList = keys.filter(key => key != props.curItem.id);
                    if (tmpList.length > 0) {
                        const key = tmpList[0];
                        if (key == "") {
                            props.onChange({
                                type: "departMent",
                                id: "",
                                value: {
                                    depart_ment_id: "",
                                    depart_ment_name: "总部门",
                                    parent_depart_ment_id: "",
                                    create_time: 0,
                                    create_user_id: "",
                                    create_display_name: "",
                                    create_logo_uri: "",
                                    update_time: 0,
                                    sub_depart_ment_count: 0,
                                    sub_member_count: 0,
                                    evaluate_count: 0,
                                },
                            });
                            return;
                        }
                        const departMent = orgStore.departMentList.find(item => item.depart_ment_id == key);
                        if (departMent !== undefined) {
                            props.onChange({
                                type: "departMent",
                                id: departMent.depart_ment_id,
                                value: departMent,
                            });
                            return;
                        }
                        const member = orgStore.memberList.find(item => item.member_user_id == key);
                        if (member !== undefined) {
                            props.onChange({
                                type: "member",
                                id: member.member_user_id,
                                value: member,
                            });
                        }
                    }
                }} />
            {orgStore.curOrg != undefined && orgStore.showUpdateOrgModal == true && (
                <UpdateOrgModal orgInfo={orgStore.curOrg} onClose={() => orgStore.showUpdateOrgModal = false} />
            )}
            {orgStore.showEvalTargetModal == true && (
                <EvalTargetModal onClose={() => orgStore.showEvalTargetModal = false} />
            )}
        </Card>
    );
};

export default observer(OrgTree);