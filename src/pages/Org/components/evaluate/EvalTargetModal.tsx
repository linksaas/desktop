//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { observer } from 'mobx-react';
import { useStores } from "@/hooks";
import { Button, Form, Input, message, Modal, Table } from "antd";
import type { EvaluateTargetInfo } from "@/api/org_evaluate";
import { list_target, update_target, remove_target,add_target } from "@/api/org_evaluate";
import { request } from "@/utils/request";
import type { ColumnsType } from 'antd/lib/table';
import { EditText } from "@/components/EditCell/EditText";

export interface EvalTargetModalProps {
    onClose: () => void;
}

const EvalTargetModal = (props: EvalTargetModalProps) => {
    const userStore = useStores("userStore");
    const orgStore = useStores("orgStore");

    const [targetInfoList, setTargetInfoList] = useState<EvaluateTargetInfo[]>([]);
    const [newTargetName, setNewTargetName] = useState("");

    const loadTargetInfoList = async () => {
        if (orgStore.curOrgId == "") {
            return;
        }
        const res = await request(list_target({
            session_id: userStore.sessionId,
            org_id: orgStore.curOrgId,
        }));
        setTargetInfoList(res.target_list);
    };

    const addTarget = async () => {
        if(newTargetName == "" ||orgStore.curOrgId == ""){
            return;
        }
        await request(add_target({
            session_id: userStore.sessionId,
            org_id: orgStore.curOrgId,
            target_name: newTargetName,
        }));
        message.info("增加成功");
        setNewTargetName("");
        await loadTargetInfoList();
    };

    const removeTarget = async (targetId: string) => {
        await request(remove_target({
            session_id: userStore.sessionId,
            org_id: orgStore.curOrgId,
            target_id: targetId,
        }));
        message.info("删除成功");
        await loadTargetInfoList();
    };

    const columns: ColumnsType<EvaluateTargetInfo> = [
        {
            title: "名称",
            render: (_, row: EvaluateTargetInfo) => (
                <EditText editable content={row.target_name} showEditIcon
                    onChange={async content => {
                        if (content.trim() == "") {
                            return false;
                        }
                        try {
                            await request(update_target({
                                session_id: userStore.sessionId,
                                org_id: orgStore.curOrgId,
                                target_id: row.target_id,
                                target_name: content.trim(),
                            }));
                            await loadTargetInfoList();
                            return true;
                        } catch (e) {
                            console.log(e);
                            return false;
                        }
                    }} />

            ),
        },
        {
            title: "相关评估数量",
            width: 100,
            dataIndex: "evaluate_count",
        },
        {
            title: "操作",
            width: 100,
            render: (_, row: EvaluateTargetInfo) => (
                <Button type="link" danger style={{ minWidth: 0, padding: "0px 0px" }} disabled={row.evaluate_count > 0}
                    onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        removeTarget(row.target_id);
                    }}>删除</Button>
            ),
        }
    ];

    useEffect(() => {
        loadTargetInfoList();
    }, []);

    return (
        <Modal open title="管理评估指标" footer={null} mask={false}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onClose();
            }}>
            <Table rowKey="target_id" dataSource={targetInfoList} columns={columns} pagination={false} bordered={false}
                scroll={{ y: "calc(100vh - 300px)" }} />
            <Form layout="inline" style={{ marginTop: "10px" }}>
                <Form.Item>
                    <Input style={{ width: "380px" }} value={newTargetName} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setNewTargetName(e.target.value.trim());
                    }} />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" disabled={newTargetName == ""} onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        addTarget();
                    }}>增加指标</Button>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default observer(EvalTargetModal);