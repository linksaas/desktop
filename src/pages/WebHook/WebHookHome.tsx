//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from "react";
import { observer } from 'mobx-react';
import { Button, message, Popover, Space } from "antd";
import { useStores } from "@/hooks";
import TrigerList from "./TrigerList";
import TrigerDetail from "./TrigerDetail";
import CardWrap from "@/components/CardWrap";
import CreateTrigerModal from "./components/CreateTrigerModal";
import { open as shell_open } from '@tauri-apps/api/shell';
import { writeText } from "@tauri-apps/api/clipboard";
import { DoubleLeftOutlined, MoreOutlined } from "@ant-design/icons";


const WebHookHome = () => {
    const projectStore = useStores('projectStore');
    const appStore = useStores('appStore');

    const [showCreateModal, setShowCreateModal] = useState(false);

    return (
        <CardWrap title={
            <>
                {projectStore.projectHome.curTriger == null && "第三方事件"}
                {projectStore.projectHome.curTriger != null && (
                    <span>
                        <a onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            projectStore.projectHome.curTriger = null;
                        }}><DoubleLeftOutlined />&nbsp;第三方事件</a>/{projectStore.projectHome.curTriger.triger_name}(
                        <a onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            if (projectStore.projectHome.curTriger?.triger_id == "atomgit") {
                                shell_open("https://atomgit.com/");
                            } else if (projectStore.projectHome.curTriger?.triger_id == "gitcode") {
                                shell_open("https://gitcode.com/");
                            } else if (projectStore.projectHome.curTriger?.triger_id == "gitee") {
                                shell_open("https://gitee.com/");
                            }
                        }}>{projectStore.projectHome.curTriger?.type_id}</a>
                        )
                    </span>
                )}
            </>

        } extra={
            <Space>
                {projectStore.isAdmin && !projectStore.isClosed && projectStore.projectHome.curTriger == null && (
                    <Button type="primary" onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setShowCreateModal(true);
                    }}>创建接收器</Button>
                )}
                {projectStore.projectHome.curTriger != null && (
                    <Space>
                        <span>
                            {`${appStore.clientCfg?.serv_http_addr ?? ""}/webhook/${projectStore.projectHome.curTriger?.type_id}/${projectStore.projectHome.curTriger?.triger_id}`}
                        </span>
                        <Popover placement="bottom" trigger="click" content={
                            <Space direction="vertical">
                                <Button type="link" onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    writeText(`${appStore.clientCfg?.serv_http_addr ?? ""}/webhook/${projectStore.projectHome.curTriger?.type_id}/${projectStore.projectHome.curTriger?.triger_id}`);
                                    message.info("复制成功");
                                }}>复制地址</Button>
                                {projectStore.isAdmin && projectStore.projectHome.curTriger?.has_secret == true && (
                                    <Button type="link" onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        writeText(projectStore.projectHome.curTriger?.secret ?? "");
                                        message.info("复制成功");
                                    }}>复制密码</Button>
                                )}
                            </Space>
                        }>
                            <MoreOutlined />
                        </Popover>
                    </Space>
                )}
            </Space>
        }>

            <div style={{ height: "calc(100vh - 150px)", overflowY: "scroll" }}>
                {projectStore.projectHome.curTriger == null && (<TrigerList />)}
                {projectStore.projectHome.curTriger != null && (<TrigerDetail />)}
            </div>
            {showCreateModal == true && (
                <CreateTrigerModal onClose={() => setShowCreateModal(false)} />
            )}
        </CardWrap>
    );
};

export default observer(WebHookHome);