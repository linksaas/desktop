//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { useStores } from "@/hooks";
import { Form, Input, message, Modal, Select } from "antd";
import type { TrigerTypeInfo } from "@/api/project_triger";
import { list_type as list_triger_type, create as create_triger } from "@/api/project_triger";
import { request } from "@/utils/request";

export interface CreateTrigerModalProps {
    onClose: () => void;
}

const CreateTrigerModal = (props: CreateTrigerModalProps) => {
    const userStore = useStores("userStore");
    const projectStore = useStores("projectStore");

    const [trigerTypeList, setTrigerTypeList] = useState<TrigerTypeInfo[]>([]);
    const [curTrigerType, setCurTrigerType] = useState<TrigerTypeInfo | null>(null);
    const [name, setName] = useState("");

    const loadTrigerTypeList = async () => {
        if (projectStore.curProjectId == "") {
            return;
        }
        const res = await request(list_triger_type({}));
        setTrigerTypeList(res.triger_type_list);
        if (res.triger_type_list.length == 0) {
            setCurTrigerType(null);
        } else {
            if (curTrigerType == null) {
                setCurTrigerType(res.triger_type_list[0]);
            }
        }
    };

    const createTriger = async () => {
        if (curTrigerType == null) {
            return;
        }
        await request(create_triger({
            session_id: userStore.sessionId,
            project_id: projectStore.curProjectId,
            type_id: curTrigerType.type_id,
            triger_name: name,
        }));
        message.info("创建成功");
        props.onClose();
    };

    useEffect(() => {
        if (projectStore.curProjectId != "") {
            loadTrigerTypeList();
        }
    }, [projectStore.curProjectId]);

    return (
        <Modal open title="创建接收器" mask={false}
            okText="创建" okButtonProps={{ disabled: name == "" || curTrigerType == null }}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onClose();
            }}
            onOk={e => {
                e.stopPropagation();
                e.preventDefault();
                createTriger();
            }}>
            <Form labelCol={{ span: 3 }}>
                {trigerTypeList.length > 0 && curTrigerType != null && (
                    <Form.Item label="类型" help={curTrigerType.type_desc}>
                        <Select value={curTrigerType.type_id} onChange={value => {
                            const tmpList = trigerTypeList.slice();
                            const trigerType = tmpList.find(item => item.type_id == value);
                            setCurTrigerType(trigerType ?? null);
                        }}>
                            {trigerTypeList.map(trigerType => (
                                <Select.Option key={trigerType.type_id} value={trigerType.type_id}>{trigerType.type_id}</Select.Option>
                            ))}
                        </Select>
                    </Form.Item>
                )}
                {curTrigerType != null && (
                    <Form.Item label="名称">
                        <Input value={name} onChange={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            setName(e.target.value.trim());
                        }} />
                    </Form.Item>
                )}
            </Form>
        </Modal>
    );
};

export default CreateTrigerModal;