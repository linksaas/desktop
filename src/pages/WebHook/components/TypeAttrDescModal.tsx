//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { Descriptions, Modal } from "antd";
import React, { useEffect, useState } from "react";
import type { TrigerTypeInfo } from "@/api/project_triger";
import { get_type as get_triger_type } from "@/api/project_triger";
import { request } from "@/utils/request";

export interface TypeAttrDescModalProps {
    typeId: string;
    onClose: () => void;
}

const TypeAttrDescModal = (props: TypeAttrDescModalProps) => {
    const [typeInfo, setTypeInfo] = useState<TrigerTypeInfo | null>(null);

    const loadTypeInfo = async () => {
        const res = await request(get_triger_type({
            type_id: props.typeId,
        }));
        setTypeInfo(res.triger_type);
    };

    useEffect(() => {
        loadTypeInfo();
    }, []);

    return (
        <Modal open title="查看属性定义" mask={false} footer={null}
            bodyStyle={{ height: "calc(100vh - 300px)", overflowY: "scroll" }}
            onCancel={e=>{
                e.stopPropagation();
                e.preventDefault();
                props.onClose();
            }}>
            {typeInfo != null && (
                <>
                    {typeInfo.attr_desc_group_list.map(group => (
                        <div key={group.group_name}>
                            <h1 style={{ fontSize: "20px", fontWeight: 600 }}>{group.group_name}</h1>
                            <Descriptions bordered column={1} labelStyle={{width:"150px"}}>
                                {group.attr_desc_list.map(attr => (
                                    <Descriptions.Item key={attr.attr_key} label={attr.attr_key}>
                                        {attr.desc}
                                    </Descriptions.Item>
                                ))}
                            </Descriptions>
                        </div>
                    ))}
                </>
            )}
        </Modal>
    );
};

export default TypeAttrDescModal;