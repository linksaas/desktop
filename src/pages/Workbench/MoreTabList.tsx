//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from 'react';
import { observer } from 'mobx-react';
import { Tabs } from 'antd';
import { useHistory } from 'react-router-dom';
import { WORKBENCH_PATH } from '@/utils/constant';
import { useStores } from '@/hooks';
import { CloudServerOutlined, CreditCardOutlined } from '@ant-design/icons';
import MemoList from './components/usermemo/MemoList';
import UserServerList from './components/userserv/UserServerList';
import { useTranslation } from "react-i18next";


export interface MoreTabListProps {
    subTab: string;
}

const MoreTabList = (props: MoreTabListProps) => {
    const history = useHistory();

    const { t } = useTranslation();

    const userStore = useStores("userStore");
    const appStore = useStores("appStore");

    return (
        <Tabs activeKey={props.subTab} onChange={value => history.push(`${WORKBENCH_PATH}?tab=more&subTab=${value}`)} tabPosition='left'
            style={{ height: "calc(100vh - 210px)", overflowY: "hidden" }}
            tabBarStyle={{ width: "120px" }}>

            {appStore.vendorCfg?.work_bench.enable_user_memo == true && (
                <Tabs.TabPane tab={<h2 style={{ color: userStore.sessionId == "" ? "#aaa" : undefined }} title={userStore.sessionId == "" ? "需登录状态" : ""}><CreditCardOutlined />{t("workbench.main.memo")}</h2>}
                    key="userMemo" disabled={userStore.sessionId == ""} style={{ paddingLeft: "0px" }}>
                    {props.subTab == "userMemo" && (
                        <MemoList />
                    )}
                </Tabs.TabPane>
            )}
            {appStore.vendorCfg?.work_bench.enable_server_list == true && (
                <Tabs.TabPane tab={<h2 style={{ color: userStore.sessionId == "" ? "#aaa" : undefined }} title={userStore.sessionId == "" ? "需登录状态" : ""}><CloudServerOutlined />{t("workbench.main.servList")}</h2>}
                    key="userServer" disabled={userStore.sessionId == ""} style={{ paddingLeft: "0px" }}>
                    {props.subTab == "userServer" && (
                        <div style={{ height: "calc(100vh - 210px)", overflowY: "hidden", overflowX: "hidden" }}>
                            <UserServerList />
                        </div>
                    )}
                </Tabs.TabPane>
            )}
        </Tabs>
    );
};

export default observer(MoreTabList);