//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { observer } from "mobx-react";
import type { GitCodeOrg } from "@/api/gitcode/org";
import { list_org } from "@/api/gitcode/org";
import { get_username } from "@/api/gitcode/common";
import { useStores } from "@/hooks";
import type { GitCodeRepo } from "@/api/gitcode/repo";
import { list_org_repo } from "@/api/gitcode/repo";
import { Button, Card, Dropdown, Empty, Input, Layout, Menu, Select, Space, Tabs } from "antd";
import { DownOutlined, ExportOutlined, FilterFilled, GlobalOutlined, ProjectOutlined } from "@ant-design/icons";
import type { LocalRepoInfo } from "@/api/local_repo";
import { openGitAssistant } from "@/utils/git_assistant";
import { GitCodeBranchList, GitCodeIssueList, GitCodeTagList } from "./components/GitCodeList";
import AddRepoModal from "./components/localrepo/AddRepoModal";

interface GitRepoRepoPanelProps {
    repoInfo?: GitCodeRepo;
    curOrgPath: string;
}

const GitRepoRepoPanel = (props: GitRepoRepoPanelProps) => {
    const userStore = useStores("userStore");
    const localRepoStore = useStores("localRepoStore");

    const [localRepo, setLocalRepo] = useState<LocalRepoInfo | null>(null);
    const [cloneUrl, setCloneUrl] = useState("");
    const [activeKey, setActiveKey] = useState("issue");

    const findLocalRepo = async () => {
        setLocalRepo(null);
        if (props.repoInfo == undefined) {
            return;
        }
        for (const tmpRepo of localRepoStore.repoExtList) {
            for (const remoteInfo of tmpRepo.remoteList) {
                if (remoteInfo.url == props.repoInfo.http_url_to_repo || remoteInfo.url == props.repoInfo.ssh_url_to_repo) {
                    setLocalRepo(tmpRepo.repoInfo);
                    return;
                }
            }
        }
    }

    useEffect(() => {
        findLocalRepo();
    }, [props.repoInfo?.id]);

    return (
        <div>
            {props.repoInfo == undefined && (
                <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
            )}
            {props.repoInfo != undefined && (
                <>
                    <Card title={<a href={props.repoInfo.http_url_to_repo} target="_blank" rel="noreferrer" style={{ fontSize: "16px", fontWeight: 600 }}>{props.repoInfo.name}&nbsp;<ExportOutlined /></a>}
                        bordered={false} bodyStyle={{ minHeight: "50px" }} headStyle={{ backgroundColor: "#eee", height: "46px" }}
                        extra={
                            <Space>
                                {localRepo != null && (
                                    <>
                                        <Button style={{ color: "orange", fontWeight: 500 }} onClick={e => {
                                            e.stopPropagation();
                                            e.preventDefault();
                                            openGitAssistant(localRepo, userStore.userInfo.userName, userStore.userInfo.userType, userStore.userInfo.extraToken);
                                        }}>查看本地仓库</Button>
                                    </>
                                )}
                                {localRepo == null && (
                                    <Dropdown menu={{
                                        items: [
                                            {
                                                key: "ssh",
                                                label: <span title={(localRepoStore.checkResult?.hasGit == false) ? "未安装Git工具" : ""}>SSH</span>,
                                                onClick: () => setCloneUrl(props.repoInfo?.ssh_url_to_repo ?? ""),
                                                disabled: localRepoStore.checkResult?.hasGit == false,
                                            },
                                            {
                                                key: "https",
                                                label: <span title={(localRepoStore.checkResult?.hasGit == false) ? "未安装Git工具" : ""}>HTTPS</span>,
                                                onClick: () => setCloneUrl(props.repoInfo?.http_url_to_repo ?? ""),
                                                disabled: localRepoStore.checkResult?.hasGit == false,
                                            },
                                        ],
                                    }} trigger={["click"]} >
                                        <Space>
                                            <a style={{ color: "orange", fontWeight: 500 }}>
                                                克隆到本地
                                                <DownOutlined />
                                            </a>
                                        </Space>
                                    </Dropdown>
                                )}
                                <div>默认分支:{props.repoInfo.default_branch}</div>
                            </Space>
                        }>
                        <pre style={{ whiteSpace: "pre-wrap", wordWrap: "break-word" }}>
                            {(props.repoInfo.description ?? "") == "" && <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="暂无项目描述" />}
                            {props.repoInfo.description}
                        </pre>
                    </Card>
                    <div style={{ padding: "10px 10px" }}>
                        <Tabs type="card" activeKey={activeKey} onChange={key => setActiveKey(key)}
                            items={[
                                {
                                    key: "issue",
                                    label: "工单列表",
                                    children: (
                                        <>
                                            {activeKey == "issue" && <GitCodeIssueList userToken={userStore.userInfo.extraToken} ownerName={props.repoInfo.namespace.path} repoName={props.repoInfo.path} />}
                                        </>
                                    ),
                                },
                                {
                                    key: "branch",
                                    label: "分支列表",
                                    children: (
                                        <>
                                            {activeKey == "branch" && <GitCodeBranchList userToken={userStore.userInfo.extraToken} ownerName={props.repoInfo.namespace.path} repoName={props.repoInfo.path} />}
                                        </>
                                    ),
                                },
                                {
                                    key: "tag",
                                    label: "标签列表",
                                    children: (
                                        <>
                                            {activeKey == "tag" && <GitCodeTagList userToken={userStore.userInfo.extraToken} ownerName={props.repoInfo.namespace.path} repoName={props.repoInfo.path} />}
                                        </>
                                    ),
                                },
                            ]} />
                    </div>
                </>
            )}
            {cloneUrl != "" && (
                <AddRepoModal name={props.repoInfo?.name ?? ""} enName={props.repoInfo?.path ?? ""} remoteUrl={cloneUrl} onCancel={() => setCloneUrl("")}
                    onOk={() => {
                        localRepoStore.loadRepoList().then(() => {
                            findLocalRepo();
                            setCloneUrl("");
                        });
                    }} />
            )}
        </div>
    );
};

const GitCodePanel = () => {
    const userStore = useStores("userStore");

    const [orgList, setOrgList] = useState<GitCodeOrg[]>([]);
    const [curOrgPath, setCurOrgPath] = useState(get_username(userStore.userInfo.userName));
    const [repoList, setRepoList] = useState<GitCodeRepo[]>([]);
    const [curRepoId, setCurRepoId] = useState("");
    const [keyword, setKeyword] = useState("");

    const loadOrgList = async () => {
        const userName = get_username(userStore.userInfo.userName);
        const res = await list_org(userStore.userInfo.extraToken);
        setOrgList([
            {
                id: 0,
                login: userName,
                path: userName,
                name: "个人项目",
                url: `https://gitcode.com/${userName}`,
                description: "",
            },
            ...res]);
    };

    const loadRepoList = async () => {
        const res = await list_org_repo(userStore.userInfo.extraToken, curOrgPath);
        setRepoList(res);
        if (res.length > 0 && res.map(item => item.id.toString()).includes(curRepoId) == false) {
            setCurRepoId(res[0].id.toString());
        }
    };

    useEffect(() => {
        loadOrgList();
    }, []);

    useEffect(() => {
        loadRepoList();
    }, [curOrgPath]);

    return (
        <Layout>
            <Layout.Sider style={{ borderRight: "1px solid #e4e4e8" }} theme="light">
                <Space style={{ padding: "10px 0px 10px 4px", backgroundColor: "#eee", width: "100%" }}>
                    <Select value={curOrgPath} onChange={value => {
                        setCurOrgPath(value);
                    }} style={{ width: "160px" }}>
                        {orgList.map((org, orgIndex) => (
                            <Select.Option key={org.id} value={org.path}>{`${orgIndex > 0 ? "组织 " : ""}${org.name}`}</Select.Option>
                        ))}
                    </Select>
                </Space>
                <Space style={{ margin: "4px 0px" }}>
                    <Input placeholder="搜索项目" style={{ width: "170px" }} allowClear value={keyword}
                        onChange={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            setKeyword(e.target.value.trim());
                        }} />
                    <FilterFilled />
                </Space>
                <Menu items={repoList.filter(repo => repo.name.includes(keyword)).map(repo => ({
                    key: repo.id.toFixed(0),
                    label: (
                        <Space style={{ fontSize: "14px" }}>
                            {repo.private && <ProjectOutlined />}
                            {repo.public && <GlobalOutlined />}
                            <div>{repo.name}</div>
                        </Space>
                    ),
                }))} style={{ border: "none", height: "calc(100vh - 320px)", overflowY: "scroll" }} selectedKeys={curRepoId == "" ? [] : [curRepoId]}
                    onSelect={info => {
                        setCurRepoId(info.key);
                    }} />
            </Layout.Sider>
            <Layout.Content style={{ height: "calc(100vh - 230px)", overflowY: "scroll", backgroundColor: "white" }}>
                <GitRepoRepoPanel repoInfo={repoList.find(item => item.id.toFixed(0) == curRepoId)} curOrgPath={curOrgPath} />
            </Layout.Content>
        </Layout>
    )
};

export default observer(GitCodePanel);