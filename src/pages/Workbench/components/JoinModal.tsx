//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from 'react';
import { joinOrgOrProject } from '@/components/LeftMenu/join';
import { Form, Input, Modal } from 'antd';
import { observer } from 'mobx-react';
import { useHistory } from 'react-router-dom';
import { useStores } from '@/hooks';
import { useTranslation } from "react-i18next";


interface JoinModalProps {
    onClose: () => void;
}

const JoinModal = (props: JoinModalProps) => {
    const history = useHistory();

    const { t } = useTranslation();

    const userStore = useStores('userStore');
    const projectStore = useStores('projectStore');
    const orgStore = useStores('orgStore');
    const appStore = useStores('appStore');

    const [linkText, setLinkText] = useState('');

    const runJoin = async () => {
        await joinOrgOrProject(linkText, userStore, projectStore, orgStore, appStore, history);
        props.onClose();
    };

    return (
        <Modal open title={t("workbench.top.joinAll")} mask={false}
            width={700}
            okText={t("text.join")} okButtonProps={{ disabled: linkText == "" }}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onClose();
            }}
            onOk={e => {
                e.stopPropagation();
                e.preventDefault();
                runJoin();
            }}>
            <Form labelCol={{ span: 4 }} style={{ paddingRight: "20px" }}>
                <Form.Item label={t("text.inviteCode")}>
                    <Input
                        placeholder={t("header.inputInviteCode")}
                        allowClear
                        onChange={(e) => setLinkText(e.target.value.trim())}
                    />
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default observer(JoinModal);