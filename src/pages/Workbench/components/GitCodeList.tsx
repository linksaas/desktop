//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import type { GitCodeIssue } from "@/api/gitcode/issue";
import { list_issue } from "@/api/gitcode/issue";
import type { ColumnsType } from 'antd/lib/table';
import moment from "moment";
import { List, Space, Table, Tag } from "antd";
import UserPhoto from "@/components/Portrait/UserPhoto";
import { ExportOutlined } from "@ant-design/icons";
import type { GitCodeBranch } from "@/api/gitcode/branch";
import { list_branch } from "@/api/gitcode/branch";
import type { GitCodeTag } from "@/api/gitcode/tag";
import { list_tag } from "@/api/gitcode/tag";

export interface GitCodeListProps {
    userToken: string;
    ownerName: string;
    repoName: string;
}

export const GitCodeIssueList = (props: GitCodeListProps) => {
    const [issueList, setIssueList] = useState([] as GitCodeIssue[]);

    const loadIssueList = async () => {
        const res = await list_issue(props.userToken, props.ownerName, props.repoName);
        setIssueList(res);
    };

    const columns: ColumnsType<GitCodeIssue> = [
        {
            title: "标题",
            width: 200,
            render: (_, row: GitCodeIssue) => (
                <a href={row.html_url} target="_blank" rel="noreferrer">{row.title}&nbsp;<ExportOutlined /></a>
            ),
        },
        {
            title: "提交人",
            width: 150,
            render: (_, row: GitCodeIssue) => (
                <a href={row.user.html_url} target="_blank" rel="noreferrer">
                    <Space>
                        <UserPhoto logoUri={row.user.avatar_url} style={{ width: "16px", borderRadius: "10px" }} />
                        <div>{row.user.name}&nbsp;<ExportOutlined /></div>
                    </Space>
                </a>
            ),
        },
        {
            title: "指派人",
            width: 100,
            render: (_, row: GitCodeIssue) => (
                <>
                    {row.assignee != undefined && row.assignee.id != undefined && (
                        <a href={row.assignee.html_url} target="_blank" rel="noreferrer">
                            <Space>
                                <UserPhoto logoUri={row.assignee.avatar_url} style={{ width: "16px", borderRadius: "10px" }} />
                                <div>{row.assignee.name}&nbsp;<ExportOutlined /></div>
                            </Space>
                        </a>
                    )}
                </>
            ),
        },
        {
            title: "创建时间",
            width: 120,
            render: (_, row: GitCodeIssue) => moment(row.created_at).format("YYYY-MM-DD HH:mm"),
        },
        {
            title: "更新时间",
            width: 120,
            render: (_, row: GitCodeIssue) => moment(row.updated_at).format("YYYY-MM-DD HH:mm"),
        },
        {
            title: "关闭时间",
            width: 120,
            render: (_, row: GitCodeIssue) => (
                <>
                    {row.finished_at != "" && moment(row.finished_at).format("YYYY-MM-DD HH:mm")}
                </>
            ),
        },
    ];

    useEffect(() => {
        loadIssueList();
    }, [props.ownerName, props.repoName]);

    return (
        <Table rowKey="id" dataSource={issueList} columns={columns} pagination={false} bordered={false} />
    );
};

export const GitCodeBranchList = (props: GitCodeListProps) => {
    const [branchList, setBranchList] = useState([] as GitCodeBranch[]);

    const loadBranchList = async () => {
        const res = await list_branch(props.userToken, props.ownerName, props.repoName);
        setBranchList(res);
    };

    useEffect(() => {
        loadBranchList();
    }, [props.ownerName, props.repoName]);

    return (
        <List rowKey="name" dataSource={branchList} grid={{ gutter: 16 }} renderItem={item => (
            <List.Item>
                <Tag>{item.name}{item.protected ? "(保护分支)" : ""}</Tag>
            </List.Item>
        )} />
    );
};

export const GitCodeTagList = (props: GitCodeListProps) => {
    const [tagList, setTagList] = useState([] as GitCodeTag[]);

    const loadTagList = async () => {
        const res = await list_tag(props.userToken, props.ownerName, props.repoName);
        setTagList(res);
    };

    useEffect(() => {
        loadTagList();
    }, [props.ownerName, props.repoName]);

    return (
        <List rowKey="name" dataSource={tagList} grid={{ gutter: 16 }} renderItem={item => (
            <List.Item>
                <Tag>{item.name}</Tag>
            </List.Item>
        )} />
    );
};