//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from "react";
import { observer } from 'mobx-react';
import { useStores } from "@/hooks";
import { useHistory } from "react-router-dom";
import { APP_ORG_MANAGER_PATH, APP_PROJECT_MANAGER_PATH, DATA_VIEW_PATH, GROW_CENTER_PATH, PUB_RES_PATH, WORKBENCH_PATH } from "@/utils/constant";
import { Space } from "antd";
import { open as shell_open } from '@tauri-apps/api/shell';
import s from "./WelcomeInfo.module.less";
import { useTranslation } from "react-i18next";

const WelcomeInfo = () => {
    const history = useHistory();
    const { t } = useTranslation();


    const appStore = useStores("appStore");
    const userStore = useStores("userStore");

    const getJoinToName = () => {
        const tmpList = [] as string[];
        if (appStore.vendorCfg?.ability.enable_project && userStore.userInfo.featureInfo.enable_project) {
            tmpList.push(t("text.project"));
        }
        if (appStore.vendorCfg?.ability.enable_org && userStore.userInfo.featureInfo.enable_org) {
            tmpList.push(t("text.org"));
        }
        return tmpList.join("/");
    };

    return (
        <div className={s.welcome_wrap}>
            <h1>{t("workbench.welcome.welcomUse")}<b>{appStore.vendorCfg?.layout.app_name ?? ""}</b>!</h1>
            {appStore.vendorCfg?.layout.vendor_intro == "" && (
                <p>
                    <b>{appStore.vendorCfg?.layout.app_name ?? ""}</b>
                    {t("workbench.welcome.intro1", { addon: appStore.vendorCfg?.ability.enable_project == true ? t("workbench.welcome.andOrg") : "" })}
                </p>
            )}
            {appStore.vendorCfg?.layout.vendor_intro != "" && (
                <div dangerouslySetInnerHTML={{ __html: appStore.vendorCfg?.layout.vendor_intro ?? "" }} />

            )}
            <p>
                {t("workbench.welcome.intro2", { "name": appStore.vendorCfg?.layout.app_name ?? "" })}
                <a onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    history.push(`${WORKBENCH_PATH}?tab=localRepo`);
                }} style={{ textDecoration: "underline" }}>{t("workbench.welcome.localRepo")}</a>
                {appStore.vendorCfg?.work_bench.enable_minapp && (
                    <>
                        ，{t("workbench.welcome.intro3")}<a onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            history.push(`${WORKBENCH_PATH}?tab=userApp`);
                        }} style={{ textDecoration: "underline" }}>{t("workbench.welcome.minApp")}</a>{t("workbench.welcome.intro4")}{userStore.sessionId == "" && (appStore.vendorCfg?.account.inner_account || appStore.vendorCfg?.account.external_account) && (
                            <>(<a onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                userStore.showUserLogin = true;
                            }} style={{ textDecoration: "underline" }}>{"workbench.welcome.login"}</a>{t("workbench.welcome.intro5")})</>
                        )}
                    </>
                )}
                。
                {userStore.sessionId != "" && appStore.vendorCfg?.ability.enable_dataview && userStore.userInfo.featureInfo.enable_data_view && (
                    <>
                        {t("workbench.welcome.intro6")}<a
                            onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                history.push(DATA_VIEW_PATH);
                            }} style={{ textDecoration: "underline" }}>{t("workbench.welcome.dataview")}</a>，{t("workbench.welcome.intro7")}
                    </>
                )}
                {userStore.sessionId != "" && appStore.vendorCfg?.ability.enable_grow_center && userStore.userInfo.featureInfo.enable_grow_center && (
                    <>
                        {t("workbench.welcome.intro6")}<a onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            history.push(GROW_CENTER_PATH);
                        }} style={{ textDecoration: "underline" }}>{t("workbench.welcome.growCenter")}</a>，{t("workbench.welcome.intro8")}
                    </>
                )}

            </p>
            {appStore.vendorCfg?.ability.enable_project && (
                <>
                    <p>
                        {t("workbench.welcome.intro9")}
                        <a
                            onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                shell_open("https://marketplace.visualstudio.com/items?itemName=linksaas.local-api");
                            }} style={{ textDecoration: "underline" }}>{t("workbench.welcome.vscodePlugin")}</a>，{t("workbench.welcome.intro10")}
                    </p>
                </>
            )}
            <br />
            <p>
                {t("workbench.welcome.intro11")}<a onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    appStore.showFeedBack = true;
                }} style={{ textDecoration: "underline" }}>{t("workbench.welcome.feedBack")}</a>，{t("workbench.welcome.intro12")}
            </p>
            <h1>{t("workbench.welcome.funcEntry")}</h1>
            <h2>{t("workbench.welcome.personnel")}</h2>
            <Space>
                <a onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    history.push(`${WORKBENCH_PATH}?tab=localRepo`);
                }} style={{ textDecoration: "underline" }}>{t("workbench.main.localRepo")}</a>
                {appStore.vendorCfg?.work_bench.enable_minapp == true && (
                    <a onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        history.push(`${WORKBENCH_PATH}?tab=userApp`);
                    }} style={{ textDecoration: "underline" }}>{t("workbench.main.minapp")}</a>
                )}
            </Space>
            {userStore.sessionId != "" && (
                <>
                    {((appStore.vendorCfg?.ability.enable_project && userStore.userInfo.featureInfo.enable_project) ||
                        (appStore.vendorCfg?.ability.enable_org && userStore.userInfo.featureInfo.enable_org)) && (
                            <>
                                <h2>{t("workbench.welcome.devTeam")}</h2>
                                <div>
                                    <a onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        appStore.showJoinModal = true;
                                    }} style={{ textDecoration: "underline" }}>{t("text.join")} {getJoinToName()}</a>
                                </div>
                                {(appStore.vendorCfg?.ability.enable_project && userStore.userInfo.featureInfo.enable_project) && (
                                    <div>
                                        <Space>
                                            <a onClick={e => {
                                                e.stopPropagation();
                                                e.preventDefault();
                                                appStore.showCreateOrJoinProject = true;
                                            }} style={{ textDecoration: "underline" }}>{t("workbench.welcome.createProject")}</a>
                                            <a onClick={e => {
                                                e.stopPropagation();
                                                e.preventDefault();
                                                history.push(APP_PROJECT_MANAGER_PATH);
                                            }} style={{ textDecoration: "underline" }}>{t("workbench.welcome.manageProject")}</a>
                                        </Space>
                                    </div>
                                )}
                                {(appStore.vendorCfg?.ability.enable_org && userStore.userInfo.featureInfo.enable_org) && (
                                    <Space>
                                        <a onClick={e => {
                                            e.stopPropagation();
                                            e.preventDefault();
                                            appStore.showCreateOrJoinOrg = true;
                                        }} style={{ textDecoration: "underline" }}>{t("workbench.welcome.createOrg")}</a>
                                        <a onClick={e => {
                                            e.stopPropagation();
                                            e.preventDefault();
                                            history.push(APP_ORG_MANAGER_PATH);
                                        }} style={{ textDecoration: "underline" }}>{t("workbench.welcome.manageOrg")}</a>
                                    </Space>
                                )}
                            </>
                        )}
                </>
            )}
            {appStore.vendorCfg?.ability.enable_pubres && (
                <>
                    <h2>{t("workbench.welcome.pubRes")}</h2>
                    <Space>
                        <a onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            history.push(`${PUB_RES_PATH}?tab=ideaStore`);
                        }} style={{ textDecoration: "underline" }}>{t("workbench.welcome.knowledgePoint")}</a>
                        <a onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            history.push(`${PUB_RES_PATH}?tab=swStore`);
                        }} style={{ textDecoration: "underline" }}>{t("workbench.welcome.commonSoftWare")}</a>
                    </Space>
                </>
            )}
            {userStore.sessionId != "" && appStore.vendorCfg?.ability.enable_grow_center && userStore.userInfo.featureInfo.enable_grow_center && (
                <>
                    <h2>{t("text.other")}</h2>
                    <Space>
                        <a onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            history.push(GROW_CENTER_PATH);
                        }} style={{ textDecoration: "underline" }}>{t("workbench.top.growcenter")}</a>
                    </Space>
                </>
            )}
        </div>
    );
};

export default observer(WelcomeInfo);