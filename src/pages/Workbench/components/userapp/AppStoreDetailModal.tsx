//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import type { AppInfo } from "@/api/appstore";
import { agree_app, cancel_agree_app, get_app, install_app } from "@/api/appstore";
import { Button, Card, Descriptions, Dropdown, Modal, Space } from "antd";
import { observer } from 'mobx-react';
import { useStores } from "@/hooks";
import { request } from "@/utils/request";
import { CloseOutlined, DownloadOutlined, HeartTwoTone } from "@ant-design/icons";
import AppPermPanel from "@/pages/Admin/AppAdmin/components/AppPermPanel";
import { ReadOnlyEditor } from "@/components/Editor";
import { list_app as list_user_app, add_app as add_user_app, remove_app as remove_user_app } from "@/api/user_app";
import { open as open_shell } from '@tauri-apps/api/shell';
import { get_global_server_addr } from "@/api/client_cfg";
import { useTranslation } from "react-i18next";

const AppStoreDetailModal = () => {
    const { t } = useTranslation();

    const appStore = useStores('appStore');
    const userStore = useStores('userStore');
    const pubResStore = useStores('pubResStore');

    const [myAppIdList, setMyAppIdList] = useState<string[]>([]);

    const [appInfo, setAppInfo] = useState<AppInfo | null>(null);

    const loadMyAppIdList = async () => {
        const res = await request(list_user_app({
            session_id: userStore.sessionId,
        }));
        setMyAppIdList(res.app_id_list);
    };

    const loadAppInfo = async () => {
        const addr = await get_global_server_addr();
        const res = await request(get_app(addr, {
            app_id: pubResStore.showAppId,
            session_id: userStore.sessionId,
        }));
        setAppInfo(res.app_info);
    };

    const installUserApp = async () => {
        if (appInfo == null) {
            return;
        }
        const tmpList = myAppIdList.slice();
        if (tmpList.includes(appInfo.app_id) == false) {
            tmpList.push(appInfo.app_id);
            setMyAppIdList(tmpList);
            await request(add_user_app({
                session_id: userStore.sessionId,
                app_id: appInfo.app_id,
            }));
            const addr = await get_global_server_addr();
            await install_app(addr, { app_id: appInfo.app_id });
            setAppInfo({ ...appInfo, install_count: appInfo.install_count + 1 });
            pubResStore.incAppDataVersion();
        }
        appStore.openMinAppParam = {
            minAppId: pubResStore.showAppId,
            extraInfo: '""',
            extraInfoName: '""',
        };
    };

    const removeUserApp = async () => {
        if (appInfo == null) {
            return;
        }
        const tmpList = myAppIdList.filter(item => item != appInfo.app_id);
        setMyAppIdList(tmpList);
        await request(remove_user_app({
            session_id: userStore.sessionId,
            app_id: appInfo.app_id,
        }))
    };

    const agreeApp = async (appId: string, newAgree: boolean) => {
        if (appInfo == null) {
            return;
        }
        const addr = await get_global_server_addr();
        if (newAgree) {
            await request(agree_app(addr, {
                session_id: userStore.sessionId,
                app_id: appId,
            }));
        } else {
            await request(cancel_agree_app(addr, {
                session_id: userStore.sessionId,
                app_id: appId,
            }));
        }
        let newAgreeCount = appInfo.agree_count;
        if (newAgree) {
            newAgreeCount = appInfo.agree_count + 1;
        } else {
            if (appInfo.agree_count > 0) {
                newAgreeCount = appInfo.agree_count - 1;
            }
        }

        setAppInfo({ ...appInfo, agree_count: newAgreeCount, my_agree: newAgree });
        pubResStore.incAppDataVersion();
    };

    useEffect(() => {
        if (pubResStore.showAppId != "") {
            loadAppInfo();
            loadMyAppIdList();
        }
    }, [pubResStore.showAppId]);


    return (
        <Modal open width={1000} footer={null} closable={false} mask={false}>
            <Card title={
                <h2 style={{ fontSize: "16px", fontWeight: 700 }}>{appInfo?.base_info.app_name ?? ""}</h2>
            } bordered={false}
                bodyStyle={{ height: "calc(100vh - 280px)", overflowY: "scroll" }}
                extra={
                    <Space style={{ fontSize: "18px" }} size="middle">
                        {appInfo != null && (
                            <>
                                <div><DownloadOutlined />&nbsp;{appInfo.install_count}</div>
                                <div onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    if (userStore.sessionId != "") {
                                        agreeApp(appInfo.app_id, !appInfo.my_agree);
                                    }
                                }} style={{ margin: "0px 20px" }}>
                                    <a style={{ cursor: userStore.sessionId == "" ? "default" : "pointer" }}>
                                        <HeartTwoTone twoToneColor={appInfo.my_agree ? ["red", "red"] : ["#e4e4e8", "#e4e4e8"]} />
                                    </a>
                                    &nbsp;{appInfo.agree_count}
                                </div>
                                {myAppIdList.includes(appInfo.app_id) == true && (
                                    <Dropdown.Button type="primary" menu={{
                                        items: [
                                            {
                                                key: "remove",
                                                label: <div style={{ padding: "10px 10px", color: "red" }}>{t("workbench.minapp.uninstall")}</div>,
                                                onClick: () => removeUserApp(),
                                            }
                                        ]
                                    }} onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        appStore.openMinAppParam = {
                                            minAppId: pubResStore.showAppId,
                                            extraInfo: '""',
                                            extraInfoName: '""',
                                        };
                                    }}>{t("workbench.minapp.run")}</Dropdown.Button>
                                )}
                                {myAppIdList.includes(appInfo.app_id) == false && (
                                    <Button type="primary" onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        installUserApp();
                                    }}>{t("workbench.minapp.install")}</Button>
                                )}
                            </>
                        )}
                        <Button type="text" icon={<CloseOutlined style={{ fontSize: "20px" }} />} title={t("text.close")}
                            onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                pubResStore.showAppId = "";
                            }} />
                    </Space>
                }>
                <h2 style={{ fontSize: "18px", fontWeight: 600, marginBottom: "10px" }}>{t("workbench.minapp.appInfo")}</h2>
                {appInfo != null && (
                    <Descriptions bordered labelStyle={{ width: "150px" }} column={2}>
                        <Descriptions.Item label={t("workbench.minapp.majorCate")}>{appInfo.major_cate.cate_name}</Descriptions.Item>
                        <Descriptions.Item label={t("workbench.minapp.minorCate")}>{appInfo.minor_cate.cate_name}</Descriptions.Item>
                        <Descriptions.Item span={3} label={t("workbench.minapp.appDesc")}>
                            <ReadOnlyEditor content={appInfo.base_info.app_desc} />
                        </Descriptions.Item>
                        <Descriptions.Item span={3} label={t("workbench.minapp.appPerm")}>
                            <AppPermPanel disable={true} showTitle={false} onChange={() => { }} perm={appInfo.app_perm} />
                        </Descriptions.Item>
                        {appInfo.base_info.src_url !== "" && (
                            <Descriptions.Item label={t("workbench.minapp.sourceCode")}>
                                <a onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    open_shell(appInfo?.base_info.src_url ?? "");
                                }}>{appInfo.base_info.src_url}</a>
                            </Descriptions.Item>
                        )}
                    </Descriptions>
                )}
            </Card>
        </Modal>
    );
};

export default observer(AppStoreDetailModal);