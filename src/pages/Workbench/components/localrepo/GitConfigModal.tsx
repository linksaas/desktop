//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { list_config, set_config } from "@/api/local_repo";
import { Form, Input, message, Modal } from "antd";
import { useTranslation } from "react-i18next";

export interface GitConfigModalProps {
    onCancel: () => void;
    onOk: () => void;
}

const GitConfigModal = (props: GitConfigModalProps) => {
    const { t } = useTranslation();

    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [hasChange, setHasChange] = useState(false);


    const updateConfig = async () => {
        await set_config("user.name",username);
        await set_config("user.email",email);
        props.onOk();
        message.info(t("text.updateSuccess"));
    };

    useEffect(() => {
        list_config().then(items => {
            for (const item of items) {
                if (item.name == "user.name") {
                    setUsername(item.value);
                } else if (item.name == "user.email") {
                    setEmail(item.value);
                }
            }
        });
    }, []);

    return (
        <Modal open title={t("workbench.localRepo.configGitUser")} mask={false}
            okText={t("text.update")} okButtonProps={{ disabled: !(username != "" && email.includes("@") && hasChange) }}
            onCancel={e=>{
                e.stopPropagation();
                e.preventDefault();
                props.onCancel();
            }}
            onOk={e=>{
                e.stopPropagation();
                e.preventDefault();
                updateConfig();
            }}>
            <Form labelCol={{ span: 4 }}>
                <Form.Item label="user.name">
                    <Input value={username} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setHasChange(true);
                        setUsername(e.target.value.trim());
                    }} />
                </Form.Item>
                <Form.Item label="user.email" help={
                    <>
                        {email != "" && email.includes("@") == false && (
                            <span style={{ color: "red" }}>请输入正确的邮件地址</span>
                        )}
                    </>
                }>
                    <Input value={email} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setHasChange(true);
                        setEmail(e.target.value.trim());
                    }} />
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default GitConfigModal;