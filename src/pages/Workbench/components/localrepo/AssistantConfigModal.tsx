//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { set_assistant_mode, get_assistant_mode } from "@/api/local_repo";
import { Form, message, Modal, Select } from "antd";
import { useTranslation } from "react-i18next";

export interface AssistantConfigModalProps {
    onClose: () => void;
}

const AssistantConfigModal = (props: AssistantConfigModalProps) => {
    const { t } = useTranslation();

    const [mode, setMode] = useState<"simple" | "expert">("simple");
    const [hasChange, setHasChange] = useState(false);

    const save = async () => {
        await set_assistant_mode(mode);
        message.info(t("text.updateSuccess"));
        props.onClose();
    };

    useEffect(() => {
        get_assistant_mode().then(res => setMode(res));
    }, []);

    return (
        <Modal open title={t("workbench.localRepo.configGitAssistantMode")} mask={false}
            okText={t("text.update")} okButtonProps={{ disabled: !hasChange }}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onClose();
            }}
            onOk={e => {
                e.stopPropagation();
                e.preventDefault();
                save();
            }}>
            <Form>
                <Form.Item label={t("text.mode")}>
                    <Select value={mode} onChange={value => {
                        setMode(value);
                        setHasChange(true);
                    }}>
                        <Select.Option value="simple">{t("text.simpleMode")}</Select.Option>
                        <Select.Option value="expert">{t("text.expertMode")}</Select.Option>
                    </Select>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default AssistantConfigModal;