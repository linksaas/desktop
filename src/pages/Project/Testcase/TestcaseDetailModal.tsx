//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { Modal, Tabs } from "antd";
import DetailPanel from "./components/DetailPanel";
import ResultPanel from "./components/ResultPanel";
import CommentTab from "@/components/CommentEntry/CommentTab";
import { COMMENT_TARGET_TEST_CASE } from "@/api/project_comment";
import CommentInModal from "@/components/CommentEntry/CommentInModal";
import type { ProjectInfo } from "@/api/project";
import { type MemberInfo, list_member } from "@/api/project_member";
import { request } from "@/utils/request";
import { get_session } from "@/api/user";

export interface TestcaseDetailModalProps {
    projectInfo: ProjectInfo;
    myUserId: string;
    testCaseId: string;
    testCaseTab: "detail" | "result" | "comment";
    testCaseLinkSpritId: string;

    onClose: () => void;
    onChangeTab: (newTab: "detail" | "result" | "comment") => void;
}

const TestcaseDetailModal = (props: TestcaseDetailModalProps) => {

    const [commentDataVersion, setCommentDataVersion] = useState(0);
    const [memberList, setMemberList] = useState<MemberInfo[]>([]);

    const loadMemberList = async () => {
        const sessionId = await get_session();
        const res = await request(list_member(sessionId, props.projectInfo.project_id, false, []));
        setMemberList(res.member_list);
    };

    useEffect(() => {
        if (props.testCaseTab == "comment") {
            setTimeout(() => {
                setCommentDataVersion(oldValue => oldValue + 1);
            }, 500);
        }
    }, [props.testCaseTab]);

    useEffect(() => {
        loadMemberList();
    }, [props.projectInfo.project_id]);

    return (
        <Modal open title="测试用例" footer={null} mask={false}
            width="800px"
            bodyStyle={{ height: "calc(100vh - 300px)", padding: "0px 10px", overflowY: "hidden" }}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onClose();

            }}>
            <Tabs activeKey={props.testCaseTab}
                onChange={key => props.onChangeTab((key as "detail" | "result" | "comment"))}
                type="card" tabPosition="left" size="large"
                items={[
                    {
                        key: "detail",
                        label: "测试用例详情",
                        children: (
                            <div style={{ height: "calc(100vh - 320px)", overflowY: "scroll" }}>
                                {props.testCaseTab == "detail" && (
                                    <DetailPanel projectInfo={props.projectInfo}
                                        testCaseId={props.testCaseId}
                                        testCaseLinkSpritId={props.testCaseLinkSpritId}
                                        memberList={memberList}
                                        onRemove={() => {
                                            props.onClose();
                                        }} />
                                )}
                            </div>
                        ),
                    },
                    {
                        key: "result",
                        label: "测试结果",
                        children: (
                            <div style={{ height: "calc(100vh - 320px)", overflowY: "scroll" }}>
                                {props.testCaseTab == "result" && (
                                    <ResultPanel projectInfo={props.projectInfo} myUserId={props.myUserId}
                                        myAdmin={props.projectInfo.user_project_perm.can_admin} testCaseId={props.testCaseId} testCaseLinkSpritId={props.testCaseLinkSpritId} />
                                )}
                            </div>
                        ),
                    },
                    {
                        key: "comment",
                        label: <CommentTab projectId={props.projectInfo.project_id} targetType={COMMENT_TARGET_TEST_CASE} targetId={props.testCaseId} dataVersion={commentDataVersion} />,
                        children: (
                            <div style={{ height: "calc(100vh - 320px)", overflowY: "scroll", paddingRight: "10px" }}>
                                <CommentInModal projectId={props.projectInfo.project_id} targetType={COMMENT_TARGET_TEST_CASE} targetId={props.testCaseId}
                                    myUserId={props.myUserId} myAdmin={props.projectInfo.user_project_perm.can_admin} enableAdd={!props.projectInfo.closed}/>
                            </div>
                        ),
                    },
                ]} />
        </Modal>
    );
};

export default TestcaseDetailModal;