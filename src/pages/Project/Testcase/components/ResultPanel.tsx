//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { Button, Card, Checkbox, Form, List, Modal, Popover, Space, message } from "antd";
import { MoreOutlined, PlusOutlined } from "@ant-design/icons";
import { ReadOnlyEditor, change_file_owner, useCommonEditor } from "@/components/Editor";
import { FILE_OWNER_TYPE_PROJECT, FILE_OWNER_TYPE_TEST_RESULT } from "@/api/fs";
import type { TestResultInfo } from "@/api/project_testcase";
import { add_test_result, list_test_result, remove_test_result, update_test_result } from "@/api/project_testcase";
import { request } from "@/utils/request";
import UserPhoto from "@/components/Portrait/UserPhoto";
import moment from "moment";
import type { ProjectInfo } from "@/api/project";
import { get_session } from "@/api/user";

const PAGE_SIZE = 10;

interface ResultCardProps {
    projectInfo: ProjectInfo;
    myUserId: string;
    myAdmin: boolean;
    result: TestResultInfo;
    onChange: () => void;
}

const ResultCard = (props: ResultCardProps) => {
    const [inEdit, setInEdit] = useState(false);
    const [testOk, setTestOk] = useState(props.result.test_ok);
    const [showRemoveModal, setShowRemoveModal] = useState(false);

    const { editor, editorRef } = useCommonEditor({
        content: props.result.content,
        fsId: props.projectInfo.test_case_fs_id,
        ownerType: FILE_OWNER_TYPE_TEST_RESULT,
        ownerId: props.result.test_result_id,
        historyInToolbar: false,
        clipboardInToolbar: false,
        commonInToolbar: true,
        pubResInToolbar: false,
        placeholder: "请输入测试结果...",
    });

    const removeResult = async () => {
        const sessionId = await get_session();
        await request(remove_test_result({
            session_id: sessionId,
            project_id: props.projectInfo.project_id,
            test_result_id: props.result.test_result_id,
        }));
        setShowRemoveModal(false);
        props.onChange();
        message.info("移至回收站成功");
    };

    const updateResult = async () => {
        const sessionId = await get_session();
        if (editorRef.current == null) {
            return;
        }
        const content = editorRef.current.getContent();
        await request(update_test_result({
            session_id: sessionId,
            project_id: props.projectInfo.project_id,
            test_result_id: props.result.test_result_id,
            test_ok: testOk,
            content: JSON.stringify(content),
        }));
        setInEdit(false);
        props.onChange();
        message.info("更新成功");
    }

    return (
        <Card title={
            <Space>
                <UserPhoto logoUri={props.result.create_logo_uri} style={{ width: "16px", borderRadius: "10px" }} />
                <span>{props.result.create_display_name}({moment(props.result.create_time).format("YYYY-MM-DD HH:mm")})</span>
            </Space>
        } style={{ width: "100%" }} headStyle={{ padding: "0px" }} bordered={false}
            extra={
                <Space>
                    {(props.myAdmin || props.myUserId == props.result.create_user_id) && (
                        <>
                            {inEdit == false && (
                                <>
                                    <Button type="link" onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        setTestOk(props.result.test_ok);
                                        const t = setInterval(() => {
                                            if (editorRef.current != null) {
                                                editorRef.current.setContent(props.result.content);
                                                clearInterval(t);
                                            }
                                        }, 100);
                                        setInEdit(true);
                                    }}>编辑</Button>
                                    <Popover trigger="click" placement="bottom" content={
                                        <div style={{ padding: "10px" }}>
                                            <Button type="link" danger onClick={e => {
                                                e.stopPropagation();
                                                e.preventDefault();
                                                setShowRemoveModal(true);
                                            }}>删除</Button>
                                        </div>
                                    }>
                                        <MoreOutlined />
                                    </Popover>
                                </>
                            )}
                            {inEdit == true && (
                                <>
                                    <Button onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        setTestOk(props.result.test_ok);
                                        const t = setInterval(() => {
                                            if (editorRef.current != null) {
                                                editorRef.current.setContent(props.result.content);
                                                clearInterval(t);
                                            }
                                        }, 100);
                                        setInEdit(false);
                                    }}>取消</Button>
                                    <Button type="primary" onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        updateResult();
                                    }}>保存</Button>
                                </>
                            )}
                        </>
                    )}
                </Space>
            }>
            <Form disabled={!inEdit}>
                <Form.Item label="测试正常">
                    <Checkbox checked={testOk} onChange={e => {
                        e.stopPropagation();
                        setTestOk(e.target.checked);
                    }} />
                </Form.Item>
            </Form>
            {inEdit == false && (
                <ReadOnlyEditor content={props.result.content} />
            )}
            {inEdit == true && (
                <div className="_chatContext">
                    {editor}
                </div>
            )}
            {showRemoveModal == true && (
                <Modal open title="删除测试结果" mask={false}
                    okText="删除" okButtonProps={{ danger: true }}
                    onCancel={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setShowRemoveModal(false);
                    }}
                    onOk={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        removeResult();
                    }}>
                    是否删除测试结果?
                </Modal>
            )}
        </Card>
    );
};

export interface ResultPanleProps {
    projectInfo: ProjectInfo;
    myUserId: string;
    myAdmin: boolean;
    testCaseId: string;
    testCaseLinkSpritId: string;
}

const ResultPanle = (props: ResultPanleProps) => {

    const [inEdit, setInEdit] = useState(false);
    const [testOk, setTestOk] = useState(false);

    const [resultList, setResultList] = useState([] as TestResultInfo[]);
    const [totalCount, setTotalCount] = useState(0);
    const [curPage, setCurPage] = useState(0);

    const { editor, editorRef } = useCommonEditor({
        content: "",
        fsId: props.projectInfo.test_case_fs_id,
        ownerType: FILE_OWNER_TYPE_PROJECT,
        ownerId: props.projectInfo.project_id,
        historyInToolbar: false,
        clipboardInToolbar: false,
        commonInToolbar: true,
        pubResInToolbar: false,
        placeholder: "请输入测试结果...",
    });

    const loadResultList = async () => {
        const sessionId = await get_session();
        const res = await request(list_test_result({
            session_id: sessionId,
            project_id: props.projectInfo.project_id,
            case_id: props.testCaseId,
            filter_by_sprit_id: props.testCaseLinkSpritId != "",
            sprit_id: props.testCaseLinkSpritId,
            offset: PAGE_SIZE * curPage,
            limit: PAGE_SIZE,
        }));
        setTotalCount(res.count);
        setResultList(res.result_list);
    };

    const addTestResult = async () => {
        const sessionId = await get_session();
        if (editorRef.current == null) {
            return;
        }
        const content = editorRef.current.getContent();

        const res = await request(add_test_result({
            session_id: sessionId,
            project_id: props.projectInfo.project_id,
            case_id: props.testCaseId,
            sprit_id: props.testCaseLinkSpritId,
            test_ok: testOk,
            content: JSON.stringify(content),
        }));

        await change_file_owner(content, sessionId, FILE_OWNER_TYPE_TEST_RESULT, res.test_result_id);
        editorRef.current.setContent("");
        setTestOk(false);
        setInEdit(false);
        if (curPage != 0) {
            setCurPage(0);
        } else {
            loadResultList();
        }
        message.info("增加成功");
    };



    useEffect(() => {
        loadResultList();
    }, [curPage]);

    return (
        <Card title={props.testCaseLinkSpritId == "" ? "包含全部测试结果" : "只包含当前工作计划相关测试结果"}
            headStyle={{ padding: "0px 10px" }} bordered={false}
            bodyStyle={{ height: "calc(100vh - 370px)", overflowY: "scroll" }}
            extra={
                <Space>
                    {inEdit == false && (
                        <Button type="primary" icon={<PlusOutlined />} onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            editorRef.current?.setContent("");
                            setTestOk(false);
                            setInEdit(true);
                        }}>新增测试结果</Button>
                    )}
                    {inEdit == true && (
                        <>
                            <Button onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                editorRef.current?.setContent("");
                                setTestOk(false);
                                setInEdit(false);
                            }}>取消</Button>
                            <Button type="primary" onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                addTestResult();
                            }}>保存</Button>
                        </>
                    )}
                </Space>
            }>
            {inEdit == true && (
                <Form>
                    <Form.Item label="测试正常">
                        <Checkbox checked={testOk} onChange={e => {
                            e.stopPropagation();
                            setTestOk(e.target.checked);
                        }} />
                    </Form.Item>
                    <Form.Item label="测试详情">
                        <div className="_chatContext">
                            {editor}
                        </div>
                    </Form.Item>
                </Form>
            )}
            <List rowKey="test_result_id" dataSource={resultList}
                pagination={{ current: curPage + 1, total: totalCount, pageSize: PAGE_SIZE, onChange: page => setCurPage(page - 1), hideOnSinglePage: true }}
                renderItem={item => (
                    <List.Item>
                        <ResultCard
                            projectInfo={props.projectInfo}
                            myUserId={props.myUserId}
                            myAdmin={props.myAdmin}
                            result={item} onChange={() => loadResultList()} />
                    </List.Item>
                )} />
        </Card>
    );
};

export default ResultPanle;