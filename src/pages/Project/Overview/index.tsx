//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from "react";
import { observer } from 'mobx-react';
import CardWrap from "@/components/CardWrap";
import { Button, Card, Space } from "antd";
import { useStores } from "@/hooks";
import { ReadOnlyEditor } from "@/components/Editor";
import ProjectFsList from "./components/ProjectFsList";
import { PROJECT_SETTING_TAB } from "@/utils/constant";
import HotkeyHelpInfo from "./components/HotkeyHelpInfo";

const ProjectOverview = () => {
    const projectStore = useStores('projectStore');

    return (
        <CardWrap title={
            <Space>
                项目信息
            </Space>} halfContent>
            <div style={{ padding: "10px 10px" }}>
                <Card title="项目简介"
                    headStyle={{ backgroundColor: "#eee", fontSize: "16px", fontWeight: 600 }} style={{ marginBottom: "10px" }}
                    extra={
                        <>
                            {projectStore.isAdmin && !projectStore.isClosed && (
                                <Button type="link" onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    projectStore.showProjectSetting = PROJECT_SETTING_TAB.PROJECT_SETTING_DESC;
                                }}>编辑</Button>
                            )}
                        </>
                    }>
                    <ReadOnlyEditor content={projectStore.curProject?.basic_info.project_desc ?? ""} />
                </Card>
                
                <Card title="快捷键" headStyle={{ backgroundColor: "#eee", fontSize: "16px", fontWeight: 600 }} style={{ marginBottom: "10px" }}
                    bodyStyle={{ padding: "0px 0px" }}>
                    <HotkeyHelpInfo />
                </Card>
                <ProjectFsList />
            </div>
        </CardWrap>
    );
};

export default observer(ProjectOverview);