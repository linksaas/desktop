//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from "react";
import CardWrap from "@/components/CardWrap";
import { Space } from "antd";
import GitRepoList from "./compoents/GitRepoList";
import ServerList from "./compoents/ServerList";


const ProjectServer = () => {
    
    return (
        <CardWrap title={
            <Space>
                服务列表
            </Space>
        } halfContent>
            <GitRepoList />
            <ServerList />
        </CardWrap>
    );
};

export default ProjectServer;