//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { observer } from 'mobx-react';
import { get_conn_server_addr } from '@/api/main';
import { writeText } from '@tauri-apps/api/clipboard';
import { useStores } from "@/hooks";
import { request } from "@/utils/request";
import { gen_join_token, SERVER_TYPE_OPEN_DRAGON_FLY, SERVER_TYPE_OPEN_SEA_OTTER } from "@/api/project_server";
import { open as shell_open } from '@tauri-apps/api/shell';
import { Descriptions, message } from "antd";

export const InstallDragonFlyInfo = observer(() => {
    const userStore = useStores('userStore');
    const projectStore = useStores('projectStore');

    const [joinToken, setJoinToken] = useState("");
    const [serverAddr, setServerAddr] = useState("");

    const loadJoinToken = async () => {
        const res = await request(gen_join_token({
            session_id: userStore.sessionId,
            project_id: projectStore.curProjectId,
            server_type: SERVER_TYPE_OPEN_DRAGON_FLY,
        }));
        setJoinToken(res.token);
        return res.token;
    };

    const initServerAddr = async () => {
        const addr = await get_conn_server_addr();
        setServerAddr(addr.replaceAll("http://", "").replaceAll("/", ""));
    }

    useEffect(() => {
        loadJoinToken();
        initServerAddr();
    }, []);

    return (
        <div style={{ padding: "0px 20px" }}>
            <h3 style={{ fontSize: "20px", fontWeight: 700 }}>操作说明</h3>
            <ol style={{ listStyleType: "decimal" }}>
                <li>从<a onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    shell_open("https://gitcode.com/opendragonfly/df_server/releases");
                }}>这里</a>下载最新的服务端程序</li>
                <li>以root用户在Linux服务器上执行./df_server service install
                    &nbsp;<a onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        writeText(`./df_server service install`);
                        message.info("复制成功");
                    }}>复制命令</a>
                </li>
                <li>以root用户在Linux服务器上执行./df_server service start
                    &nbsp;<a onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        writeText(`./df_server service start`);
                        message.info("复制成功");
                    }}>复制命令</a>
                </li>
                <li>以root用户在Linux服务器上执行./df_server config addRemote --serverAddr {serverAddr} {joinToken} [NAME] [ADDR:PORT]
                    &nbsp;<a onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        loadJoinToken().then(token => {
                            writeText(`./df_server config addRemote --serverAddr ${serverAddr} ${token} [NAME] [ADDR:PORT]`);
                            message.info("复制成功");
                        });
                    }}>复制命令</a>
                </li>
            </ol>
            <h4 style={{ fontSize: "16px", fontWeight: 700 }}>参数说明</h4>
            <Descriptions column={1} bordered={true} labelStyle={{ width: "100px" }}>
                <Descriptions.Item label="NAME">在OpenLinkSaas服务器列表里面显示的名称</Descriptions.Item>
                <Descriptions.Item label="ADDR">链路追踪服务器的外网IP</Descriptions.Item>
                <Descriptions.Item label="PORT">链路追踪服务器的服务端口,默认是7000</Descriptions.Item>
            </Descriptions>
        </div>
    )
});

export const InstallSeaOtterInfo = observer(() => {
    const userStore = useStores('userStore');
    const projectStore = useStores('projectStore');

    const [joinToken, setJoinToken] = useState("");
    const [serverAddr, setServerAddr] = useState("");

    const loadJoinToken = async () => {
        const res = await request(gen_join_token({
            session_id: userStore.sessionId,
            project_id: projectStore.curProjectId,
            server_type: SERVER_TYPE_OPEN_SEA_OTTER,
        }));
        setJoinToken(res.token);
        return res.token;
    };

    const initServerAddr = async () => {
        const addr = await get_conn_server_addr();
        setServerAddr(addr.replaceAll("http://", "").replaceAll("/", ""));
    }

    useEffect(() => {
        loadJoinToken();
        initServerAddr();
    }, []);

    return (
        <div style={{ padding: "0px 20px" }}>
            <h3 style={{ fontSize: "20px", fontWeight: 700 }}>操作说明</h3>
            <ol style={{ listStyleType: "decimal" }}>
                <li>从<a onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    shell_open("https://gitcode.com/openseaotter/so_server/releases");
                }}>这里</a>下载最新的服务端程序</li>
                <li>以root用户在Linux服务器上执行./so_server config init --domain [DOMAIN] --tlsCertFile [CERT_FILE] --tlsKeyFile [KEY_FILE]
                    &nbsp;<a onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        writeText("./so_server config init --domain [DOMAIN] --tlsCertFile [CERT_FILE] --tlsKeyFile [KEY_FILE]");
                        message.info("复制成功");
                    }}>复制命令</a>
                </li>
                <li>以root用户在Linux服务器上执行./so_server service install
                    &nbsp;<a onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        writeText("./so_server service install");
                        message.info("复制成功");
                    }}>复制命令</a>
                </li>
                <li>以root用户在Linux服务器上执行./so_server service start
                    &nbsp;<a onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        writeText("./so_server service start");
                        message.info("复制成功");
                    }}>复制命令</a>
                </li>
                <li>以root用户在Linux服务器上执行./so_server config addRemote --serverAddr {serverAddr} {joinToken} [NAME] [ADDR:PORT]
                    &nbsp;<a onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        loadJoinToken().then(token => {
                            writeText(`./so_server config addRemote --serverAddr ${serverAddr} ${token} [NAME] [ADDR:PORT]`);
                            message.info("复制成功");
                        });
                    }}>复制命令</a>
                </li>
            </ol>
            <h4 style={{ fontSize: "16px", fontWeight: 700 }}>参数说明</h4>
            <Descriptions column={1} bordered={true} labelStyle={{ width: "150px" }}>
                <Descriptions.Item label="DOMAIN">镜像仓库的域名</Descriptions.Item>
                <Descriptions.Item label="CERT_FILE(可选参数)">ssh证书cert文件</Descriptions.Item>
                <Descriptions.Item label="KEY_FILE(可选参数)">ssh证书key文件</Descriptions.Item>
                <Descriptions.Item label="NAME">在OpenLinkSaas服务器列表里面显示的名称</Descriptions.Item>
                <Descriptions.Item label="ADDR">镜像仓库服务器的外网IP</Descriptions.Item>
                <Descriptions.Item label="PORT">镜像仓库服务器的服务端口,默认是6004</Descriptions.Item>
            </Descriptions>
        </div>
    )
});