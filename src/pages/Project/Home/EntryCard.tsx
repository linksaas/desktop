//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from "react";
import { Button, Card, Popover, Space, Tag } from "antd";
import { observer } from 'mobx-react';
import type { EntryInfo } from "@/api/project_entry";
import { ENTRY_TYPE_SPRIT, ENTRY_TYPE_DOC, ENTRY_TYPE_PAGES, ENTRY_TYPE_API_COLL, API_COLL_GRPC, API_COLL_OPENAPI, API_COLL_CUSTOM, ENTRY_TYPE_DRAW } from "@/api/project_entry";
import s from "./Card.module.less";
import { useStores } from "@/hooks";
import { EditOutlined, InfoCircleOutlined, MoreOutlined } from "@ant-design/icons";
import EntryPopover from "./components/EntryPopover";
import { getEntryTypeStr } from "./components/common";
import RemoveEntryModal from "./components/RemoveEntryModal";
import spritIcon from '@/assets/allIcon/icon-sprit.png';
import htmlIcon from '@/assets/allIcon/icon-html.png';
import apiCollIcon from '@/assets/allIcon/icon-apicoll.png';
import docIcon from '@/assets/channel/doc@2x.png';
import drawIcon from '@/assets/allIcon/icon-draw.png';
import { openApiCollPage } from "@/utils/apicoll";
import { openDocPage } from "@/utils/doc";
import { openWorkplanPage } from "@/utils/workplan";
import { openDrawPage } from "@/utils/draw";
import { useTranslation } from "react-i18next";

export interface EntryCardPorps {
    entryInfo: EntryInfo;
}

const EntryCard = (props: EntryCardPorps) => {
    const { t } = useTranslation();

    const projectStore = useStores('projectStore');
    const projectModalStore = useStores('projectModalStore');
    const entryStore = useStores('entryStore');

    const [showRemoveModal, setShowRemoveModal] = useState(false);

    const openEntry = async () => {
        entryStore.reset();
        if (props.entryInfo.entry_type == ENTRY_TYPE_DOC) {
            await openDocPage(projectStore.curProjectId, props.entryInfo.entry_id);
        } else if (props.entryInfo.entry_type == ENTRY_TYPE_DRAW) {
            await openDrawPage(projectStore.curProjectId, props.entryInfo.entry_id);
        } else if (props.entryInfo.entry_type == ENTRY_TYPE_SPRIT) {
            await openWorkplanPage(projectStore.curProjectId, props.entryInfo.entry_id);
        } else if (props.entryInfo.entry_type == ENTRY_TYPE_PAGES) {
            projectModalStore.projectId = projectStore.curProjectId;
            projectModalStore.pagesEntryId = props.entryInfo.entry_id;
            projectModalStore.pagesFileId = props.entryInfo.extra_info.ExtraPagesInfo?.file_id ?? "";
        } else if (props.entryInfo.entry_type == ENTRY_TYPE_API_COLL) {
            await openApiCollPage(projectStore.curProjectId, projectStore.curProject?.api_coll_fs_id ?? "", props.entryInfo.entry_id, props.entryInfo.entry_title, props.entryInfo.extra_info.ExtraApiCollInfo?.api_coll_type ?? 0,
                props.entryInfo.extra_info.ExtraApiCollInfo?.default_addr ?? "", props.entryInfo.can_update, projectStore.isAdmin, false);
        }
    };

    const getBgColor = () => {
        if (props.entryInfo.entry_type == ENTRY_TYPE_DOC) {
            return "lightyellow";
        } else if (props.entryInfo.entry_type == ENTRY_TYPE_DRAW) {
            return "gainsboro";
        } else if (props.entryInfo.entry_type == ENTRY_TYPE_SPRIT) {
            return "seashell";
        } else if (props.entryInfo.entry_type == ENTRY_TYPE_PAGES) {
            return "bisque";
        } else if (props.entryInfo.entry_type == ENTRY_TYPE_API_COLL) {
            return "cornsilk";
        }
        return "white";
    };

    const getBgImage = () => {
        if (props.entryInfo.entry_type == ENTRY_TYPE_DOC) {
            return docIcon;
        } else if (props.entryInfo.entry_type == ENTRY_TYPE_DRAW) {
            return drawIcon;
        } else if (props.entryInfo.entry_type == ENTRY_TYPE_SPRIT) {
            return spritIcon;
        } else if (props.entryInfo.entry_type == ENTRY_TYPE_PAGES) {
            return htmlIcon;
        } else if (props.entryInfo.entry_type == ENTRY_TYPE_API_COLL) {
            return apiCollIcon;
        }
        return "";
    };

    const calcTitle = (): string => {
        const retTitle = props.entryInfo.entry_title;
        if (props.entryInfo.entry_type == ENTRY_TYPE_API_COLL) {
            if (props.entryInfo.extra_info.ExtraApiCollInfo?.api_coll_type == API_COLL_GRPC) {
                return retTitle + "(GRPC)";
            } else if (props.entryInfo.extra_info.ExtraApiCollInfo?.api_coll_type == API_COLL_OPENAPI) {
                return retTitle + "(OPENAPI/SWAGGER)";
            } else if (props.entryInfo.extra_info.ExtraApiCollInfo?.api_coll_type == API_COLL_CUSTOM) {
                return retTitle + `(${t("project.entry.apitype.custom")})`;
            }
        }
        return retTitle;
    };

    return (
        <Card title={
            <Space size="small">
                <a onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    if (props.entryInfo.my_watch) {
                        entryStore.unwatchEntry(props.entryInfo.entry_id);
                    } else {
                        entryStore.watchEntry(props.entryInfo.entry_id);
                    }
                }}>
                    <span className={props.entryInfo.my_watch ? s.isCollect : s.noCollect} />
                </a>
                <span style={{ cursor: "pointer", fontWeight: 600 }} onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    openEntry();
                }}>{getEntryTypeStr(props.entryInfo.entry_type, t)}</span>
            </Space>
        }
            className={s.card} style={{ backgroundColor: getBgColor(), backgroundImage: `url(${getBgImage()})`, backgroundRepeat: "no-repeat", backgroundPosition: "95% 95%", boxShadow: "10px 10px 5px 0px #888" }}
            headStyle={{ borderBottom: "none" }} bodyStyle={{ padding: "0px 10px" }} extra={
                <Space>
                    <Popover placement="top" trigger="hover" content={<EntryPopover entryInfo={props.entryInfo} />}>
                        <InfoCircleOutlined />
                    </Popover>
                    {props.entryInfo.can_update && (
                        <Button style={{ minWidth: 0, padding: "0px 0px" }} type="text" icon={<EditOutlined />}
                            onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                entryStore.editEntryId = props.entryInfo.entry_id;
                            }} />
                    )}
                    <Popover trigger="click" placement="bottom" content={
                        <Space direction="vertical">
                            {props.entryInfo.can_update && (
                                <Button type="link" onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    entryStore.editEntryId = props.entryInfo.entry_id;
                                }}>{t("project.entry.editBasicInfo")}</Button>
                            )}
                            {props.entryInfo.my_watch && (
                                <Button type="link" onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    entryStore.unwatchEntry(props.entryInfo.entry_id);
                                }}>{t("text.unwatch")}</Button>
                            )}
                            {props.entryInfo.my_watch == false && (
                                <Button type="link" onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    entryStore.watchEntry(props.entryInfo.entry_id);
                                }}>{t("text.watch")}</Button>
                            )}
                            <Button type="link"
                                disabled={!props.entryInfo.can_remove} danger
                                onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    setShowRemoveModal(true);
                                }}>{t("text.moveRecycle")}</Button>
                        </Space>
                    }>
                        <MoreOutlined />
                    </Popover>
                </Space>
            }>
            <a onClick={e => {
                e.stopPropagation();
                e.preventDefault();
                openEntry();
            }} style={{ width: "180px", display: "block", height: "90px" }}>
                <h1 className={s.title} title={calcTitle()}>
                    {calcTitle()}
                </h1>
                <div className={s.tagList}>
                    {props.entryInfo.tag_list.map(tag => (
                        <Tag key={tag.tag_id} style={{ backgroundColor: tag.bg_color }}>{tag.tag_name}</Tag>
                    ))}
                </div>
            </a>
            {showRemoveModal == true && (
                <RemoveEntryModal entryInfo={props.entryInfo} onRemove={() => {
                    setShowRemoveModal(false);
                }} onCancel={() => setShowRemoveModal(false)} />
            )}
        </Card>

    );
};

export default observer(EntryCard);