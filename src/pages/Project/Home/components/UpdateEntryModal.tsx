//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { Button, Checkbox, DatePicker, Form, Input, Modal, Progress, Radio, Select, Tag, message } from "antd";
import React, { useEffect, useState } from "react";
import type { EntryInfo } from "@/api/project_entry";
import { get as get_entry, update_title, update_perm, update_tag, update_extra_info, ENTRY_TYPE_SPRIT, ISSUE_LIST_ALL, ISSUE_LIST_LIST, ISSUE_LIST_KANBAN, ENTRY_TYPE_PAGES, ENTRY_TYPE_API_COLL, API_COLL_GRPC, API_COLL_OPENAPI, API_COLL_CUSTOM } from "@/api/project_entry";
import { request } from "@/utils/request";
import UserPhoto from "@/components/Portrait/UserPhoto";
import s from "./UpdateEntryModal.module.less";
import moment, { type Moment } from "moment";
import { open as open_dialog } from '@tauri-apps/api/dialog';
import { uniqId } from "@/utils/utils";
import { pack_min_app } from "@/api/min_app";
import { FILE_OWNER_TYPE_PAGES, set_file_owner, write_file } from "@/api/fs";
import type { FsProgressEvent } from '@/api/fs';
import { listen } from '@tauri-apps/api/event';
import { FolderOpenOutlined } from "@ant-design/icons";
import type { GrpcExtraInfo, OpenApiExtraInfo } from "@/api/api_collection";
import { get_rpc, update_rpc, get_open_api, update_open_api } from "@/api/api_collection";
import type { CustomExtraInfo } from "@/api/http_custom";
import { get_custom, update_custom } from "@/api/http_custom";
import type { ProjectInfo, TagInfo } from "@/api/project";
import { get_session } from "@/api/user";
import type { MemberInfo } from "@/api/project_member";
import { type VendorConfig, get_vendor_config } from "@/api/client_cfg";
import { useTranslation } from "react-i18next";

export interface UpdateEntryModalProps {
    projectInfo: ProjectInfo;
    entryId: string;
    memberList: MemberInfo[];
    tagList: TagInfo[];
    onClose: () => void;
}

const UpdateEntryModal = (props: UpdateEntryModalProps) => {
    const { t } = useTranslation();

    const [entryInfo, setEntryInfo] = useState<EntryInfo | null>(null);
    const [tagIdList, setTagIdList] = useState<string[]>([]);
    const [titleChanged, setTitleChanged] = useState(false);
    const [permChanged, setPermChanged] = useState(false);
    const [tagChanged, setTagChanged] = useState(false);
    const [extraChanged, setExtraChanged] = useState(false);

    const [localPagesPath, setLocalPagesPath] = useState("");
    const [uploadPagesTraceId, setUploadPagesTraceId] = useState("");
    const [packFileName, setPackFileName] = useState("");
    const [uploadRatio, setUploadRatio] = useState(0);

    const [grpcExtraInfo, setGrpcExtraInfo] = useState<GrpcExtraInfo | null>(null);
    const [openApiExtraInfo, setOpenApiExtraInfo] = useState<OpenApiExtraInfo | null>(null);
    const [customExtraInfo, setCustomExtraInfo] = useState<CustomExtraInfo | null>(null);

    const [vendorCfg, setVendorCfg] = useState<VendorConfig | null>(null);

    const loadVendorCfg = async () => {
        const res = await get_vendor_config();
        setVendorCfg(res);
    };

    const loadEntryInfo = async () => {
        const sessionId = await get_session();
        const res = await request(get_entry({
            session_id: sessionId,
            project_id: props.projectInfo.project_id,
            entry_id: props.entryId,
        }));
        setEntryInfo(res.entry);
        setTagIdList(res.entry.tag_list.map(tag => tag.tag_id));
        if (res.entry.entry_type == ENTRY_TYPE_API_COLL) {
            if (res.entry.extra_info.ExtraApiCollInfo?.api_coll_type == API_COLL_GRPC) {
                const res2 = await request(get_rpc({
                    session_id: sessionId,
                    project_id: props.projectInfo.project_id,
                    api_coll_id: props.entryId,
                }));
                setGrpcExtraInfo(res2.extra_info);
            } else if (res.entry.extra_info.ExtraApiCollInfo?.api_coll_type == API_COLL_OPENAPI) {
                const res2 = await request(get_open_api({
                    session_id: sessionId,
                    project_id: props.projectInfo.project_id,
                    api_coll_id: props.entryId,
                }));
                setOpenApiExtraInfo(res2.extra_info);
            } else if (res.entry.extra_info.ExtraApiCollInfo?.api_coll_type == API_COLL_CUSTOM) {
                const res2 = await request(get_custom({
                    session_id: sessionId,
                    project_id: props.projectInfo.project_id,
                    api_coll_id: props.entryId,
                }));
                setCustomExtraInfo(res2.extra_info);
            }
        }
    };

    const checkDayValid = (day: Moment): boolean => {
        if (entryInfo == null) {
            return false;
        }
        const startTime = entryInfo.extra_info.ExtraSpritInfo?.start_time ?? 0;
        const endTime = entryInfo.extra_info.ExtraSpritInfo?.end_time ?? 0;
        const dayTime = day.valueOf();
        return dayTime >= startTime && dayTime <= endTime;
    };

    const choicePagesPath = async () => {
        const selected = await open_dialog({
            title: "打开本地应用目录",
            directory: true,
        });
        if (selected == null || Array.isArray(selected)) {
            return;
        }
        setLocalPagesPath(selected);
        setExtraChanged(true);
    };

    const uploadPages = async () => {
        const sessionId = await get_session();
        const traceId = uniqId();
        setUploadPagesTraceId(traceId);
        try {
            const path = await pack_min_app(localPagesPath, traceId);
            const res = await request(write_file(sessionId, props.projectInfo.pages_fs_id, path, traceId));
            return res.file_id;
        } finally {
            setUploadRatio(0);
            setUploadPagesTraceId("");
            setPackFileName("");
        }
    };

    const updateEntry = async () => {
        const sessionId = await get_session();
        if (entryInfo == null) {
            return;
        }
        if (titleChanged) {
            await request(update_title({
                session_id: sessionId,
                project_id: props.projectInfo.project_id,
                entry_id: entryInfo.entry_id,
                title: entryInfo.entry_title,
            }));
        }
        if (entryInfo == null) {
            return;
        }
        if (permChanged) {
            await request(update_perm({
                session_id: sessionId,
                project_id: props.projectInfo.project_id,
                entry_id: entryInfo.entry_id,
                entry_perm: {
                    update_for_all: entryInfo.entry_perm.update_for_all,
                    extra_update_user_id_list: entryInfo.entry_perm.update_for_all ? [] : entryInfo.entry_perm.extra_update_user_id_list,
                },
            }));
        }
        if (entryInfo == null) {
            return;
        }
        if (tagChanged) {
            await request(update_tag({
                session_id: sessionId,
                project_id: props.projectInfo.project_id,
                entry_id: entryInfo.entry_id,
                tag_id_list: tagIdList,
            }));
        }
        if (entryInfo == null) {
            return;
        }
        if (extraChanged) {
            if (entryInfo.entry_type == ENTRY_TYPE_PAGES) {
                const pagesFileId = await uploadPages();
                await request(set_file_owner({
                    session_id: sessionId,
                    fs_id: props.projectInfo.pages_fs_id,
                    file_id: pagesFileId,
                    owner_type: FILE_OWNER_TYPE_PAGES,
                    owner_id: entryInfo.entry_id,
                }));
                await request(update_extra_info({
                    session_id: sessionId,
                    project_id: props.projectInfo.project_id,
                    entry_id: entryInfo.entry_id,
                    extra_info: {
                        ExtraPagesInfo: {
                            file_id: pagesFileId,
                        },
                    },
                }));
            } else {
                await request(update_extra_info({
                    session_id: sessionId,
                    project_id: props.projectInfo.project_id,
                    entry_id: entryInfo.entry_id,
                    extra_info: entryInfo.extra_info,
                }));
            }

            if (entryInfo.entry_type == ENTRY_TYPE_API_COLL) {
                if (entryInfo.extra_info.ExtraApiCollInfo?.api_coll_type == API_COLL_GRPC) {
                    if (grpcExtraInfo != null) {
                        await request(update_rpc({
                            session_id: sessionId,
                            project_id: props.projectInfo.project_id,
                            api_coll_id: grpcExtraInfo.api_coll_id,
                            proto_file_id: grpcExtraInfo.proto_file_id,
                            secure: grpcExtraInfo.secure,
                        }));
                    }
                } else if (entryInfo.extra_info.ExtraApiCollInfo?.api_coll_type == API_COLL_OPENAPI) {
                    if (openApiExtraInfo != null) {
                        await request(update_open_api({
                            session_id: sessionId,
                            project_id: props.projectInfo.project_id,
                            api_coll_id: openApiExtraInfo.api_coll_id,
                            proto_file_id: openApiExtraInfo.proto_file_id,
                            net_protocol: openApiExtraInfo.net_protocol,
                        }));
                    }
                } else if (entryInfo.extra_info.ExtraApiCollInfo?.api_coll_type == API_COLL_CUSTOM) {
                    if (customExtraInfo != null) {
                        await request(update_custom({
                            session_id: sessionId,
                            project_id: props.projectInfo.project_id,
                            api_coll_id: customExtraInfo.api_coll_id,
                            net_protocol: customExtraInfo.net_protocol,
                        }));
                    }
                }
            }
        }
        message.info(t("text.updateSuccess"));
        props.onClose();
    };

    useEffect(() => {
        loadEntryInfo();
    }, [props.entryId, props.projectInfo.project_id]);

    useEffect(() => {
        if (uploadPagesTraceId == "") {
            return;
        }
        const unListenFn = listen<FsProgressEvent>("uploadFile_" + uploadPagesTraceId, ev => {
            if (ev.payload.total_step == 0) {
                setUploadRatio(100);
            } else {
                setUploadRatio(Math.round(ev.payload.cur_step * 100 / ev.payload.total_step));
            }
        });

        const unListenFn2 = listen<string>(uploadPagesTraceId, ev => {
            setPackFileName(ev.payload);
        });
        return () => {
            unListenFn.then((unListen) => unListen());
            unListenFn2.then((unListen) => unListen());
        };
    }, [uploadPagesTraceId]);

    useEffect(() => {
        loadVendorCfg();
    }, []);

    return (
        <Modal open title={t("project.entry.update")} mask={false}
            okText={t("text.update")}
            okButtonProps={{ disabled: entryInfo == null || entryInfo.can_update == false || entryInfo.entry_title == "" || (!titleChanged && !permChanged && !tagChanged && !extraChanged) || packFileName != "" || uploadRatio > 0 }}
            cancelButtonProps={{ disabled: packFileName != "" || uploadRatio > 0 }}
            closable={!(packFileName != "" || uploadRatio > 0)}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onClose();
            }}
            onOk={e => {
                e.stopPropagation();
                e.preventDefault();
                updateEntry();
            }} bodyStyle={{ maxHeight: "calc(100vh - 300px)", overflowY: "scroll" }}>
            {entryInfo != null && (
                <Form labelCol={{ span: 7 }} disabled={packFileName != "" || uploadRatio > 0}>
                    <Form.Item label={t("text.title")}>
                        <Input value={entryInfo.entry_title} onChange={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            setEntryInfo({ ...entryInfo, entry_title: e.target.value.trim() });
                            setTitleChanged(true);
                        }} />
                    </Form.Item>
                    <Form.Item label={t("project.entry.updateForAll")}>
                        <Checkbox checked={entryInfo.entry_perm.update_for_all} onChange={e => {
                            e.stopPropagation();
                            setEntryInfo({ ...entryInfo, entry_perm: { ...entryInfo.entry_perm, update_for_all: e.target.checked } });
                            setPermChanged(true);
                        }} />
                    </Form.Item>
                    {entryInfo.entry_perm.update_for_all == false && (
                        <Form.Item label={t("project.entry.updateForMember")} help={t("project.entry.updateForMemberTip")}>
                            <Checkbox.Group value={entryInfo.entry_perm.extra_update_user_id_list}
                                options={props.memberList.filter(member => member.can_admin == false).map(member => (
                                    {
                                        label: (<div>
                                            <UserPhoto logoUri={member.logo_uri} style={{ width: "16px", borderRadius: "10px", marginRight: "10px" }} />
                                            {member.display_name}
                                        </div>),
                                        value: member.member_user_id,
                                    }
                                ))} onChange={value => {
                                    setEntryInfo({ ...entryInfo, entry_perm: { ...entryInfo.entry_perm, extra_update_user_id_list: value as string[] } });
                                    setPermChanged(true);
                                }} />
                        </Form.Item>
                    )}
                    <Form.Item label={t("project.entry.tag")}>
                        <Checkbox.Group value={tagIdList} options={props.tagList.filter(tag => tag.use_in_entry).map(tag => ({
                            label: <div style={{ backgroundColor: tag.bg_color, padding: "0px 4px", marginBottom: "10px" }}>{tag.tag_name}</div>,
                            value: tag.tag_id,
                        }))} onChange={value => {
                            setTagIdList(value as string[]);
                            setTagChanged(true);
                        }} />
                    </Form.Item>
                    {entryInfo.entry_type == ENTRY_TYPE_SPRIT && (
                        <>
                            <Form.Item label={t("text.timeRange")}>
                                <DatePicker.RangePicker popupClassName={s.date_picker}
                                    allowClear={false}
                                    value={[moment(entryInfo.extra_info.ExtraSpritInfo?.start_time ?? 0), moment(entryInfo.extra_info.ExtraSpritInfo?.end_time ?? 0)]}
                                    onChange={value => {
                                        if (value == null) {
                                            return;
                                        }
                                        if ((value[0]?.valueOf() ?? 0) >= (value[1]?.valueOf() ?? 0)) {
                                            return;
                                        }
                                        setEntryInfo({
                                            ...entryInfo, extra_info: {
                                                ExtraSpritInfo: {
                                                    ...entryInfo.extra_info.ExtraSpritInfo!,
                                                    start_time: value[0]?.startOf("day").valueOf() ?? 0,
                                                    end_time: value[1]?.endOf("day").valueOf() ?? 0,
                                                }
                                            }
                                        });
                                        setExtraChanged(true);
                                    }} />
                            </Form.Item>
                            <Form.Item label={t("project.entry.nonWorkDay")}>
                                {(entryInfo.extra_info.ExtraSpritInfo?.non_work_day_list ?? []).map(dayTime => (
                                    <Tag key={dayTime} style={{ lineHeight: "26px", marginTop: "2px" }}
                                        closable onClose={e => {
                                            e.stopPropagation();
                                            e.preventDefault();
                                            const tmpList = (entryInfo.extra_info.ExtraSpritInfo?.non_work_day_list ?? []).filter(item => item != dayTime);
                                            setEntryInfo({
                                                ...entryInfo, extra_info: {
                                                    ExtraSpritInfo: {
                                                        ...entryInfo.extra_info.ExtraSpritInfo!,
                                                        non_work_day_list: tmpList,
                                                    }
                                                }
                                            });
                                            setExtraChanged(true);
                                        }}>
                                        {moment(dayTime).format("YYYY-MM-DD")}
                                    </Tag>
                                ))}
                                <DatePicker popupClassName={s.date_picker} value={null}
                                    disabled={(entryInfo.extra_info.ExtraSpritInfo?.start_time ?? 0) == 0 || (entryInfo.extra_info.ExtraSpritInfo?.end_time ?? 0) == 0}
                                    disabledDate={(day) => checkDayValid(day) == false}
                                    onChange={value => {
                                        if (value !== null) {
                                            if (checkDayValid(value) == false) {
                                                return;
                                            }
                                            const dayTime = value.startOf("day").valueOf();
                                            if ((entryInfo.extra_info.ExtraSpritInfo?.non_work_day_list ?? []).includes(dayTime) == false) {
                                                const tmpList = (entryInfo.extra_info.ExtraSpritInfo?.non_work_day_list ?? []).slice();
                                                tmpList.push(dayTime);
                                                tmpList.sort();
                                                setEntryInfo({
                                                    ...entryInfo, extra_info: {
                                                        ExtraSpritInfo: {
                                                            ...entryInfo.extra_info.ExtraSpritInfo!,
                                                            non_work_day_list: tmpList,
                                                        }
                                                    }
                                                });
                                                setExtraChanged(true);
                                            }
                                        }
                                    }} />
                            </Form.Item>
                            <Form.Item label={t("project.workplan.listType")}>
                                <Radio.Group value={entryInfo.extra_info.ExtraSpritInfo?.issue_list_type ?? ISSUE_LIST_ALL} onChange={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    setEntryInfo({
                                        ...entryInfo, extra_info: {
                                            ExtraSpritInfo: {
                                                ...entryInfo.extra_info.ExtraSpritInfo!,
                                                issue_list_type: e.target.value,
                                            }
                                        }
                                    });
                                    setExtraChanged(true);
                                }}>
                                    <Radio value={ISSUE_LIST_ALL}>{t("project.workplan.listAndKanban")}</Radio>
                                    <Radio value={ISSUE_LIST_LIST}>{t("project.workplan.list")}</Radio>
                                    <Radio value={ISSUE_LIST_KANBAN}>{t("project.workplan.kanban")}</Radio>
                                </Radio.Group>
                            </Form.Item>
                            <Form.Item label={t("project.workplan.hideGantt")}>
                                <Checkbox checked={entryInfo.extra_info.ExtraSpritInfo?.hide_gantt_panel ?? false} onChange={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    setEntryInfo({
                                        ...entryInfo, extra_info: {
                                            ExtraSpritInfo: {
                                                ...entryInfo.extra_info.ExtraSpritInfo!,
                                                hide_gantt_panel: e.target.checked,
                                            }
                                        }
                                    });
                                    setExtraChanged(true);
                                }} />
                            </Form.Item>
                            <Form.Item label={t("project.workplan.hideBurndown")} >
                                <Checkbox checked={entryInfo.extra_info.ExtraSpritInfo?.hide_burndown_panel ?? false} onChange={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    setEntryInfo({
                                        ...entryInfo, extra_info: {
                                            ExtraSpritInfo: {
                                                ...entryInfo.extra_info.ExtraSpritInfo!,
                                                hide_burndown_panel: e.target.checked,
                                            }
                                        }
                                    });
                                    setExtraChanged(true);
                                }} />
                            </Form.Item>
                            <Form.Item label={t("project.workplan.hideStatistics")} >
                                <Checkbox checked={entryInfo.extra_info.ExtraSpritInfo?.hide_stat_panel ?? false} onChange={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    setEntryInfo({
                                        ...entryInfo, extra_info: {
                                            ExtraSpritInfo: {
                                                ...entryInfo.extra_info.ExtraSpritInfo!,
                                                hide_stat_panel: e.target.checked,
                                            }
                                        }
                                    });
                                    setExtraChanged(true);
                                }} />
                            </Form.Item>
                            {vendorCfg != null && vendorCfg.project.show_testcase_list_entry && props.projectInfo.setting.show_test_case_list_entry && (
                                <Form.Item label={t("project.workplan.hideTestplan")} >
                                    <Checkbox checked={entryInfo.extra_info.ExtraSpritInfo?.hide_test_plan_panel ?? false} onChange={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        setEntryInfo({
                                            ...entryInfo, extra_info: {
                                                ExtraSpritInfo: {
                                                    ...entryInfo.extra_info.ExtraSpritInfo!,
                                                    hide_test_plan_panel: e.target.checked,
                                                }
                                            }
                                        });
                                        setExtraChanged(true);
                                    }} />
                                </Form.Item>
                            )}
                            <Form.Item label={t("project.workplan.hideSummary")} >
                                <Checkbox checked={entryInfo.extra_info.ExtraSpritInfo?.hide_summary_panel ?? false} onChange={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    setEntryInfo({
                                        ...entryInfo, extra_info: {
                                            ExtraSpritInfo: {
                                                ...entryInfo.extra_info.ExtraSpritInfo!,
                                                hide_summary_panel: e.target.checked,
                                            }
                                        }
                                    });
                                    setExtraChanged(true);
                                }} />
                            </Form.Item>
                        </>
                    )}
                    {entryInfo.entry_type == ENTRY_TYPE_PAGES && (
                        <>
                            <Form.Item label={t("project.pages.dir")} help={t("project.pages.dirTip")}>
                                <Input value={localPagesPath} onChange={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    setLocalPagesPath(e.target.value);
                                    setExtraChanged(true);
                                }}
                                    addonAfter={<Button type="link" style={{ height: 20 }} icon={<FolderOpenOutlined />} onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        choicePagesPath();
                                    }} />} />
                            </Form.Item>
                            {uploadRatio == 0 && packFileName != "" && (
                                <Form.Item label={t("project.pages.packageProgress")}>
                                    {packFileName}
                                </Form.Item>
                            )}
                            {uploadRatio > 0 && (
                                <Form.Item label={t("project.pages.uploadProgress")}>
                                    <Progress percent={uploadRatio} />
                                </Form.Item>
                            )}
                        </>
                    )}
                    {entryInfo.entry_type == ENTRY_TYPE_API_COLL && (
                        <>
                            <Form.Item label={t("project.entry.apiType")}>
                                {entryInfo.extra_info.ExtraApiCollInfo?.api_coll_type == API_COLL_GRPC && t("project.entry.apiType.grpc")}
                                {entryInfo.extra_info.ExtraApiCollInfo?.api_coll_type == API_COLL_OPENAPI && t("project.entry.apitype.openapi")}
                                {entryInfo.extra_info.ExtraApiCollInfo?.api_coll_type == API_COLL_CUSTOM && t("project.entry.apitype.custom")}
                            </Form.Item>
                            {entryInfo.extra_info.ExtraApiCollInfo !== undefined && (
                                <>
                                    <Form.Item label={t("text.serverAddr")} help={
                                        <>
                                            {entryInfo.extra_info.ExtraApiCollInfo?.api_coll_type == API_COLL_GRPC && entryInfo.extra_info.ExtraApiCollInfo.default_addr !== "" && entryInfo.extra_info.ExtraApiCollInfo.default_addr.split(":").length != 2 && (
                                                <span style={{ color: "red" }}>{t("project.apiCol.serverAddrTip")}</span>
                                            )}
                                        </>
                                    }>
                                        <Input value={entryInfo.extra_info.ExtraApiCollInfo.default_addr} onChange={e => {
                                            e.stopPropagation();
                                            e.preventDefault();
                                            setEntryInfo({
                                                ...entryInfo, extra_info: {
                                                    ExtraApiCollInfo: {
                                                        ...entryInfo.extra_info.ExtraApiCollInfo!,
                                                        default_addr: e.target.value.trim(),
                                                    }
                                                }
                                            });
                                            setExtraChanged(true);
                                        }} />
                                    </Form.Item>
                                    {entryInfo.extra_info.ExtraApiCollInfo.api_coll_type == API_COLL_GRPC && grpcExtraInfo != null && (
                                        <Form.Item label={t("project.apiCol.useTls")}>
                                            <Checkbox checked={grpcExtraInfo.secure} onChange={e => {
                                                e.stopPropagation();
                                                setGrpcExtraInfo({
                                                    ...grpcExtraInfo,
                                                    secure: e.target.checked,
                                                });
                                                setExtraChanged(true);
                                            }} />
                                        </Form.Item>
                                    )}
                                    {entryInfo.extra_info.ExtraApiCollInfo.api_coll_type == API_COLL_OPENAPI && openApiExtraInfo != null && (
                                        <Form.Item label={t("text.netProtocol")}>
                                            <Select value={openApiExtraInfo.net_protocol} onChange={value => {
                                                setOpenApiExtraInfo({
                                                    ...openApiExtraInfo,
                                                    net_protocol: value,
                                                });
                                                setExtraChanged(true);
                                            }}>
                                                <Select.Option value="http">http</Select.Option>
                                                <Select.Option value="https">https</Select.Option>
                                            </Select>
                                        </Form.Item>
                                    )}
                                    {entryInfo.extra_info.ExtraApiCollInfo.api_coll_type == API_COLL_CUSTOM && customExtraInfo != null && (
                                        <Form.Item label={t("text.netProtocol")}>
                                            <Select value={customExtraInfo.net_protocol} onChange={value => {
                                                setCustomExtraInfo({
                                                    ...customExtraInfo,
                                                    net_protocol: value,
                                                });
                                                setExtraChanged(true);
                                            }}>
                                                <Select.Option value="http">http</Select.Option>
                                                <Select.Option value="https">https</Select.Option>
                                            </Select>
                                        </Form.Item>
                                    )}
                                </>
                            )}
                        </>
                    )}
                </Form>
            )}
        </Modal>
    );
};

export default UpdateEntryModal;
