//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { Button, Card, Checkbox, DatePicker, Form, Input, Modal, Progress, Radio, Select, Tag, message } from "antd";
import React, { useEffect, useState } from "react";
import { observer } from 'mobx-react';
import { create_doc } from "@/api/project_doc";
import { create as create_sprit } from "@/api/project_sprit";
import {
    ENTRY_TYPE_API_COLL, ENTRY_TYPE_DOC, ENTRY_TYPE_PAGES,
    ENTRY_TYPE_SPRIT, ISSUE_LIST_ALL, ISSUE_LIST_KANBAN, ISSUE_LIST_LIST,
    API_COLL_GRPC, API_COLL_OPENAPI, API_COLL_CUSTOM,
    create as create_entry,
    ENTRY_TYPE_DRAW,
} from "@/api/project_entry";
import { useStores } from "@/hooks";
import type { EntryPerm, ExtraSpritInfo, CreateRequest } from "@/api/project_entry";
import UserPhoto from "@/components/Portrait/UserPhoto";
import moment, { type Moment } from "moment";
import s from "./UpdateEntryModal.module.less";
import { request } from "@/utils/request";
import { DeleteOutlined, FolderOpenOutlined } from "@ant-design/icons";
import { open as open_dialog } from '@tauri-apps/api/dialog';
import { pack_min_app } from "@/api/min_app";
import { uniqId } from "@/utils/utils";
import { FILE_OWNER_TYPE_PAGES, set_file_owner, write_file, make_tmp_dir, FILE_OWNER_TYPE_API_COLLECTION } from "@/api/fs";
import { listen } from '@tauri-apps/api/event';
import type { FsProgressEvent } from '@/api/fs';
import { nanoid } from 'nanoid';
import { create_rpc, create_open_api } from "@/api/api_collection";
import { create_custom } from "@/api/http_custom";
import { Command } from "@tauri-apps/api/shell";
import { platform } from "@tauri-apps/api/os";
import { openDocPage } from "@/utils/doc";
import { openWorkplanPage } from "@/utils/workplan";
import { create_draw } from "@/api/project_draw";
import { openDrawPage } from "@/utils/draw";
import { useTranslation } from "react-i18next";

interface PathWrap {
    id: string;
    path: string;
}

const CreateEntryModal = () => {
    const { t } = useTranslation();

    const userStore = useStores('userStore');
    const projectStore = useStores('projectStore');
    const entryStore = useStores('entryStore');
    const memberStore = useStores('memberStore');
    const appStore = useStores('appStore');

    const [title, setTitle] = useState("");
    const [entryPerm, setEntryPerm] = useState<EntryPerm>({
        update_for_all: true,
        extra_update_user_id_list: [],
    });
    const [tagIdList, setTagIdList] = useState<string[]>([]);
    const [spritExtraInfo, setSpritExtraInfo] = useState<ExtraSpritInfo>({
        start_time: moment().startOf("day").valueOf(),
        end_time: moment().add(7, "days").endOf("day").valueOf(),
        non_work_day_list: [],
        issue_list_type: ISSUE_LIST_ALL,
        hide_gantt_panel: false,
        hide_burndown_panel: false,
        hide_stat_panel: false,
        hide_summary_panel: false,
        hide_test_plan_panel: false,
    });

    const [localPagesPath, setLocalPagesPath] = useState("");
    const [uploadPagesTraceId, setUploadPagesTraceId] = useState("");
    const [packFileName, setPackFileName] = useState("");
    const [uploadRatio, setUploadRatio] = useState(0);

    //接口集合相关字段
    const [apiCollType, setApiCollType] = useState(API_COLL_GRPC);
    const [defaultAddr, setDefaultAddr] = useState("");

    const [grpcRootPath, setGrpcRootPath] = useState("");
    const [grpcImportPathList, setGrpcImportPathList] = useState<PathWrap[]>([]);
    const [grpcSecure, setGrpcSecure] = useState(false);

    const [openApiPath, setOpenApiPath] = useState("");
    const [openApiProtocol, setOpenApiProtocol] = useState("http");

    const [customProtocol, setCustomProtocol] = useState("https");

    const checkDayValid = (day: Moment): boolean => {
        const startTime = spritExtraInfo.start_time;
        const endTime = spritExtraInfo.end_time;
        const dayTime = day.valueOf();
        return dayTime >= startTime && dayTime <= endTime;
    };

    const choicePagesPath = async () => {
        const selected = await open_dialog({
            title: "打开本地应用目录",
            directory: true,
        });
        if (selected == null || Array.isArray(selected)) {
            return;
        }
        setLocalPagesPath(selected);
    };

    const choiceRootPath = async () => {
        const selected = await open_dialog({
            title: "选择grpc接口定义路径",
            directory: true,
        });
        if ((selected == null) || (Array.isArray(selected))) {
            return;
        }
        setGrpcRootPath(selected);
    };

    const choiceImportPath = async (id: string) => {
        const selected = await open_dialog({
            title: "选择导入路径",
            directory: true,
        });
        if ((selected == null) || (Array.isArray(selected))) {
            return;
        }
        const tmpList = grpcImportPathList.slice();
        const index = tmpList.findIndex(item => item.id == id);
        if (index != -1) {
            tmpList[index].path = selected;
            setGrpcImportPathList(tmpList);
        }
    };

    const choiceOpenApiPath = async () => {
        const selected = await open_dialog({
            title: "选择openapi定义文件",
            filters: [
                {
                    name: "json文件",
                    extensions: ["json"],
                },
                {
                    name: "yaml文件",
                    extensions: ["yml", "yaml"]
                }
            ],
        });
        if ((selected == null) || (Array.isArray(selected))) {
            return;
        }
        setOpenApiPath(selected);
    };

    const uploadPages = async () => {
        const traceId = uniqId();
        setUploadPagesTraceId(traceId);
        try {
            const path = await pack_min_app(localPagesPath, traceId);
            const res = await request(write_file(userStore.sessionId, projectStore.curProject?.pages_fs_id ?? "", path, traceId));
            return res.file_id;
        } finally {
            setUploadRatio(0);
            setUploadPagesTraceId("");
            setPackFileName("");
        }
    };

    const checkValid = () => {
        if (title == "" || packFileName != "" || uploadRatio > 0) {
            return false;
        }
        if (entryStore.createEntryType == ENTRY_TYPE_PAGES) {
            if (localPagesPath == "") {
                return false;
            }
        } else if (entryStore.createEntryType == ENTRY_TYPE_API_COLL) {
            console.log(defaultAddr, grpcRootPath);
            if (defaultAddr == "") {
                return false;
            }
            if (apiCollType == API_COLL_GRPC) {
                if (grpcRootPath == "") {
                    return false;
                }
                if (defaultAddr.includes(":") == false) {
                    return false;
                }
            } else if (apiCollType == API_COLL_OPENAPI) {
                if (openApiPath == "") {
                    return false;
                }
            }
        }
        return true;
    };

    const createEntry = async () => {
        const platName = await platform();

        if (title == "" || entryStore.createEntryType == null) {
            return;
        }
        let entryId = "";
        let pagesFileId = "";
        let apiCollFileId = "";
        if (entryStore.createEntryType == ENTRY_TYPE_PAGES) {
            pagesFileId = await uploadPages();
            entryId = nanoid(32);
        } else if (entryStore.createEntryType == ENTRY_TYPE_SPRIT) {
            const res = await request(create_sprit(userStore.sessionId, projectStore.curProjectId));
            entryId = res.sprit_id;
        } else if (entryStore.createEntryType == ENTRY_TYPE_DOC) {
            const res = await request(create_doc({
                session_id: userStore.sessionId,
                project_id: projectStore.curProjectId,
                base_info: {
                    content: JSON.stringify({ type: "doc" }),
                },
            }));
            entryId = res.doc_id;
        } else if (entryStore.createEntryType == ENTRY_TYPE_DRAW) {
            const res = await request(create_draw({
                session_id: userStore.sessionId,
                project_id: projectStore.curProjectId,
            }));
            entryId = res.draw_id;
        } else if (entryStore.createEntryType == ENTRY_TYPE_API_COLL) {
            if (apiCollType == API_COLL_GRPC) {
                const tmpDir = await make_tmp_dir();
                let tmpFile = `${tmpDir}/grpc.data`;
                if (platName.includes("win32")) {
                    tmpFile = `${tmpDir}\\grpc.data`;
                }
                const args: string[] = ["parse", "--rootPath", grpcRootPath, "--outPath", tmpFile];
                for (const importPath of grpcImportPathList) {
                    args.push("--importPath");
                    args.push(importPath.path);
                }
                const cmd = Command.sidecar("bin/linksaas_grpcutil", args);
                const output = await cmd.execute();
                const result = JSON.parse(output.stdout);
                if (result.error != undefined) {
                    message.error(`无法编译grpc协议文件。${result.error}`)
                    return;
                }
                //上传文件
                const upRes = await request(write_file(userStore.sessionId, projectStore.curProject?.api_coll_fs_id ?? "", tmpFile, ""));
                apiCollFileId = upRes.file_id;
                const res = await request(create_rpc({
                    session_id: userStore.sessionId,
                    project_id: projectStore.curProjectId,
                    default_addr: defaultAddr,
                    proto_file_id: upRes.file_id,
                    secure: grpcSecure,
                }));
                entryId = res.api_coll_id;
            } else if (apiCollType == API_COLL_OPENAPI) {
                //上传文件
                const upRes = await request(write_file(userStore.sessionId, projectStore.curProject?.api_coll_fs_id ?? "", openApiPath, ""));
                apiCollFileId = upRes.file_id;
                //创建api
                const res = await request(create_open_api({
                    session_id: userStore.sessionId,
                    project_id: projectStore.curProjectId,
                    default_addr: defaultAddr,
                    proto_file_id: upRes.file_id,
                    net_protocol: openApiProtocol,
                }));
                entryId = res.api_coll_id;
            } else if (apiCollType == API_COLL_CUSTOM) {
                const res = await request(create_custom({
                    session_id: userStore.sessionId,
                    project_id: projectStore.curProjectId,
                    default_addr: defaultAddr,
                    net_protocol: customProtocol,
                }));
                entryId = res.api_coll_id;
            }
        }
        if (entryId == "" || entryStore.createEntryType == null) {
            return;
        }
        const createReq: CreateRequest = {
            session_id: userStore.sessionId,
            project_id: projectStore.curProjectId,
            entry_id: entryId,
            entry_type: entryStore.createEntryType,
            entry_title: title,
            tag_id_list: tagIdList,
            entry_perm: {
                update_for_all: entryPerm.update_for_all,
                extra_update_user_id_list: entryPerm.update_for_all ? [] : entryPerm.extra_update_user_id_list,
            },
        };
        if (entryStore.createEntryType == ENTRY_TYPE_SPRIT) {
            createReq.extra_info = {
                ExtraSpritInfo: spritExtraInfo,
            };
        } else if (entryStore.createEntryType == ENTRY_TYPE_PAGES) {
            createReq.extra_info = {
                ExtraPagesInfo: {
                    file_id: pagesFileId,
                },
            }
        } else if (entryStore.createEntryType == ENTRY_TYPE_API_COLL) {
            createReq.extra_info = {
                ExtraApiCollInfo: {
                    api_coll_type: apiCollType,
                    default_addr: defaultAddr,
                },
            };
        }
        await request(create_entry(createReq));

        if (entryStore.createEntryType == ENTRY_TYPE_SPRIT) {
            await openWorkplanPage(projectStore.curProjectId, entryId);
        } else if (entryStore.createEntryType == ENTRY_TYPE_DOC) {
            await openDocPage(projectStore.curProjectId, entryId, true);
        } else if (entryStore.createEntryType == ENTRY_TYPE_DRAW) {
            await openDrawPage(projectStore.curProjectId, entryId, true);
        } else if (entryStore.createEntryType == ENTRY_TYPE_PAGES) {
            await request(set_file_owner({
                session_id: userStore.sessionId,
                fs_id: projectStore.curProject?.pages_fs_id ?? "",
                file_id: pagesFileId,
                owner_type: FILE_OWNER_TYPE_PAGES,
                owner_id: entryId,
            }));
        } else if (entryStore.createEntryType == ENTRY_TYPE_API_COLL) {
            if (apiCollFileId != "") {
                await request(set_file_owner({
                    session_id: userStore.sessionId,
                    fs_id: projectStore.curProject?.api_coll_fs_id ?? "",
                    file_id: apiCollFileId,
                    owner_type: FILE_OWNER_TYPE_API_COLLECTION,
                    owner_id: entryId,
                }));
            }
        }
        message.info(t("text.createSuccess"));
        entryStore.createEntryType = null;
    };

    useEffect(() => {
        if (uploadPagesTraceId == "") {
            return;
        }
        const unListenFn = listen<FsProgressEvent>("uploadFile_" + uploadPagesTraceId, ev => {
            if (ev.payload.total_step == 0) {
                setUploadRatio(100);
            } else {
                setUploadRatio(Math.round(ev.payload.cur_step * 100 / ev.payload.total_step));
            }
        });

        const unListenFn2 = listen<string>(uploadPagesTraceId, ev => {
            setPackFileName(ev.payload);
        });

        return () => {
            unListenFn.then((unListen) => unListen());
            unListenFn2.then((unListen) => unListen());
        };
    }, [uploadPagesTraceId]);

    return (
        <Modal open title={t("project.entry.create")} mask={false}
            bodyStyle={{ maxHeight: "calc(100vh - 300px)", overflowY: "auto" }}
            okText={t("text.create")} okButtonProps={{ disabled: checkValid() == false }}
            cancelButtonProps={{ disabled: packFileName != "" || uploadRatio > 0 }}
            closable={!(packFileName != "" || uploadRatio > 0)}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                entryStore.createEntryType = null;
            }}
            onOk={e => {
                e.stopPropagation();
                e.preventDefault();
                createEntry();
            }}>
            <Form labelCol={{ span: 7 }} disabled={packFileName != "" || uploadRatio > 0}>
                <Form.Item label={t("text.title")}>
                    <Input value={title} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setTitle(e.target.value.trim());
                    }} />
                </Form.Item>
                {entryStore.createEntryType != projectStore.projectHome.contentEntryType && (
                    <Form.Item label={t("text.cate")}>
                        <Radio.Group value={entryStore.createEntryType} onChange={e => {
                            e.stopPropagation();
                            entryStore.createEntryType = e.target.value;
                        }}>
                            {entryStore.createEntryType == ENTRY_TYPE_SPRIT && projectStore.isAdmin && (
                                <Radio value={ENTRY_TYPE_SPRIT}>{t("project.entry.type.workPlan")}</Radio>
                            )}
                            <Radio value={ENTRY_TYPE_DOC}>{t("project.entry.type.doc")}</Radio>
                            <Radio value={ENTRY_TYPE_DRAW}>{t("project.entry.type.draw")}</Radio>
                            <Radio value={ENTRY_TYPE_PAGES}>{t("project.entry.type.pages")}</Radio>
                            <Radio value={ENTRY_TYPE_API_COLL}>{t("project.entry.type.apiColl")}</Radio>
                        </Radio.Group>
                    </Form.Item>
                )}

                <Form.Item label={t("project.entry.updateForAll")}>
                    <Checkbox checked={entryPerm.update_for_all} onChange={e => {
                        e.stopPropagation();
                        setEntryPerm({ ...entryPerm, update_for_all: e.target.checked });
                    }} />
                </Form.Item>
                {entryPerm.update_for_all == false && (
                    <Form.Item label={t("project.entry.updateForMember")} help={t("project.entry.updateForMemberTip")}>
                        <Checkbox.Group value={entryPerm.extra_update_user_id_list}
                            options={memberStore.memberList.filter(member => member.member.can_admin == false).map(member => (
                                {
                                    label: (<div>
                                        <UserPhoto logoUri={member.member.logo_uri} style={{ width: "16px", borderRadius: "10px", marginRight: "10px" }} />
                                        {member.member.display_name}
                                    </div>),
                                    value: member.member.member_user_id,
                                }
                            ))} onChange={value => {
                                setEntryPerm({ ...entryPerm, extra_update_user_id_list: value as string[] });
                            }} />
                    </Form.Item>
                )}
                <Form.Item label={t("project.entry.tag")}>
                    <Checkbox.Group value={tagIdList} options={(projectStore.curProject?.tag_list ?? []).filter(tag => tag.use_in_entry).map(tag => ({
                        label: <div style={{ backgroundColor: tag.bg_color, padding: "0px 4px", marginBottom: "10px" }}>{tag.tag_name}</div>,
                        value: tag.tag_id,
                    }))} onChange={value => {
                        setTagIdList(value as string[]);
                    }} />
                </Form.Item>
                {entryStore.createEntryType == ENTRY_TYPE_SPRIT && (
                    <>
                        <Form.Item label={t("text.timeRange")}>
                            <DatePicker.RangePicker popupClassName={s.date_picker}
                                allowClear={false}
                                value={[moment(spritExtraInfo.start_time), moment(spritExtraInfo.end_time)]}
                                onChange={value => {
                                    if (value == null) {
                                        return;
                                    }
                                    if ((value[0]?.valueOf() ?? 0) >= (value[1]?.valueOf() ?? 0)) {
                                        return;
                                    }
                                    setSpritExtraInfo({
                                        ...spritExtraInfo,
                                        start_time: value[0]?.startOf("day").valueOf() ?? 0,
                                        end_time: value[1]?.endOf("day").valueOf() ?? 0,
                                    });
                                }} />
                        </Form.Item>
                        <Form.Item label={t("project.entry.nonWorkDay")}>
                            {(spritExtraInfo.non_work_day_list).map(dayTime => (
                                <Tag key={dayTime} style={{ lineHeight: "26px", marginTop: "2px" }}
                                    closable onClose={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        const tmpList = (spritExtraInfo.non_work_day_list).filter(item => item != dayTime);
                                        setSpritExtraInfo({
                                            ...spritExtraInfo,
                                            non_work_day_list: tmpList,
                                        });
                                    }}>
                                    {moment(dayTime).format("YYYY-MM-DD")}
                                </Tag>
                            ))}
                            <DatePicker popupClassName={s.date_picker} value={null}
                                disabled={spritExtraInfo.start_time == 0 || spritExtraInfo.end_time == 0}
                                disabledDate={(day) => checkDayValid(day) == false}
                                onChange={value => {
                                    if (value !== null) {
                                        if (checkDayValid(value) == false) {
                                            return;
                                        }
                                        const dayTime = value.startOf("day").valueOf();
                                        if (spritExtraInfo.non_work_day_list.includes(dayTime) == false) {
                                            const tmpList = spritExtraInfo.non_work_day_list.slice();
                                            tmpList.push(dayTime);
                                            tmpList.sort();
                                            setSpritExtraInfo({
                                                ...spritExtraInfo,
                                                non_work_day_list: tmpList,
                                            });
                                        }
                                    }
                                }} />
                        </Form.Item>
                        <Form.Item label={t("project.workplan.listType")}>
                            <Radio.Group value={spritExtraInfo.issue_list_type} onChange={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setSpritExtraInfo({
                                    ...spritExtraInfo,
                                    issue_list_type: e.target.value,
                                });
                            }}>
                                <Radio value={ISSUE_LIST_ALL}>{t("project.workplan.listAndKanban")}</Radio>
                                <Radio value={ISSUE_LIST_LIST}>{t("project.workplan.list")}</Radio>
                                <Radio value={ISSUE_LIST_KANBAN}>{t("project.workplan.kanban")}</Radio>
                            </Radio.Group>
                        </Form.Item>

                        <Form.Item label={t("project.workplan.hideGantt")}>
                            <Checkbox checked={spritExtraInfo.hide_gantt_panel} onChange={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setSpritExtraInfo({
                                    ...spritExtraInfo,
                                    hide_gantt_panel: e.target.checked,
                                });
                            }} />
                        </Form.Item>
                        <Form.Item label={t("project.workplan.hideBurndown")}>
                            <Checkbox checked={spritExtraInfo.hide_burndown_panel} onChange={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setSpritExtraInfo({
                                    ...spritExtraInfo,
                                    hide_burndown_panel: e.target.checked,
                                });
                            }} />
                        </Form.Item>
                        <Form.Item label={t("project.workplan.hideStatistics")}>
                            <Checkbox checked={spritExtraInfo.hide_stat_panel} onChange={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setSpritExtraInfo({
                                    ...spritExtraInfo,
                                    hide_stat_panel: e.target.checked,
                                });
                            }} />
                        </Form.Item>
                        {appStore.vendorCfg?.project.show_testcase_list_entry && projectStore.curProject?.setting.show_test_case_list_entry && (
                            <Form.Item label={t("project.workplan.hideTestplan")}>
                                <Checkbox checked={spritExtraInfo.hide_test_plan_panel} onChange={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    setSpritExtraInfo({
                                        ...spritExtraInfo,
                                        hide_test_plan_panel: e.target.checked,
                                    });
                                }} />
                            </Form.Item>
                        )}
                        <Form.Item label={t("project.workplan.hideSummary")}>
                            <Checkbox checked={spritExtraInfo.hide_summary_panel} onChange={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setSpritExtraInfo({
                                    ...spritExtraInfo,
                                    hide_summary_panel: e.target.checked,
                                });
                            }} />
                        </Form.Item>
                    </>
                )}
                {entryStore.createEntryType == ENTRY_TYPE_PAGES && (
                    <>
                        <Form.Item label={t("project.pages.dir")} help={t("project.pages.dirTip")}>
                            <Input value={localPagesPath} onChange={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setLocalPagesPath(e.target.value);
                            }}
                                addonAfter={<Button type="link" style={{ height: 20 }} icon={<FolderOpenOutlined />} onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    choicePagesPath();
                                }} />} />
                        </Form.Item>
                        {uploadRatio == 0 && packFileName != "" && (
                            <Form.Item label={t("project.pages.packageProgress")}>
                                {packFileName}
                            </Form.Item>
                        )}
                        {uploadRatio > 0 && (
                            <Form.Item label={t("project.pages.uploadProgress")}>
                                <Progress percent={uploadRatio} />
                            </Form.Item>
                        )}
                    </>
                )}
                {entryStore.createEntryType == ENTRY_TYPE_API_COLL && (
                    <>
                        <Form.Item label={t("project.entry.apiType")}>
                            <Select value={apiCollType} onChange={value => setApiCollType(value)}>
                                <Select.Option value={API_COLL_GRPC}>{t("project.entry.apiType.grpc")}</Select.Option>
                                <Select.Option value={API_COLL_OPENAPI}>{t("project.entry.apitype.openapi")}</Select.Option>
                                <Select.Option value={API_COLL_CUSTOM}>{t("project.entry.apitype.custom")}</Select.Option>
                            </Select>
                        </Form.Item>
                        <Form.Item label={t("text.serverAddr")} help={
                            <>
                                {apiCollType == API_COLL_GRPC && defaultAddr !== "" && defaultAddr.split(":").length != 2 && (
                                    <span style={{ color: "red" }}>{t("project.apiCol.serverAddrTip")}</span>
                                )}
                            </>
                        }>
                            <Input value={defaultAddr} onChange={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setDefaultAddr(e.target.value.trim());
                            }} />
                        </Form.Item>
                        {apiCollType == API_COLL_GRPC && (
                            <>
                                <Form.Item label={t("project.apiCol.grpcPath")}>
                                    <Input addonAfter={<Button type="text" style={{ height: "20px", minWidth: 0, padding: "0px 0px" }} icon={<FolderOpenOutlined />} onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        choiceRootPath();
                                    }} />} value={grpcRootPath} onChange={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        setGrpcRootPath(e.target.value);
                                    }} />
                                </Form.Item>
                                <Form.Item label={t("project.apiCol.useTls")}>
                                    <Checkbox checked={grpcSecure} onChange={e => {
                                        e.stopPropagation();
                                        setGrpcSecure(e.target.checked);
                                    }} />
                                </Form.Item>
                            </>
                        )}
                        {apiCollType == API_COLL_OPENAPI && (
                            <>
                                <Form.Item label={t("project.apiCol.openapiPath")}>
                                    <Input addonAfter={<Button type="text" style={{ height: "20px", minWidth: 0, padding: "0px 0px" }} icon={<FolderOpenOutlined />} onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        choiceOpenApiPath();
                                    }} />} value={openApiPath} onChange={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        setOpenApiPath(e.target.value);
                                    }} />
                                </Form.Item>
                                <Form.Item label={t("text.netProtocol")}>
                                    <Select value={openApiProtocol} onChange={value => setOpenApiProtocol(value)}>
                                        <Select.Option value="http">http</Select.Option>
                                        <Select.Option value="https">https</Select.Option>
                                    </Select>
                                </Form.Item>
                            </>
                        )}
                        {apiCollType == API_COLL_CUSTOM && (
                            <Form.Item label={t("text.netProtocol")}>
                                <Select value={customProtocol} onChange={value => setCustomProtocol(value)}>
                                    <Select.Option value="http">http</Select.Option>
                                    <Select.Option value="https">https</Select.Option>
                                </Select>
                            </Form.Item>
                        )}
                    </>
                )}
            </Form>
            {entryStore.createEntryType == ENTRY_TYPE_API_COLL && apiCollType == API_COLL_GRPC && (
                <div>
                    <Card title={t("project.apiCol.importPath")} style={{ border: "none" }} headStyle={{ padding: "0px 0px 0px 20px" }} bodyStyle={{ padding: "10px 0px 0px 0px" }}
                        extra={
                            <Button type="link" onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                const tmpList = grpcImportPathList.slice();
                                tmpList.push({
                                    id: uniqId(),
                                    path: "",
                                });
                                setGrpcImportPathList(tmpList);
                            }}>{t("project.apiCol.addImportPath")}</Button>
                        }>
                        <Form labelCol={{ span: 5 }}>
                            {grpcImportPathList.map((item, index) => (
                                <Form.Item key={item.id} label={`${t("text.path")} ${index + 1}`}>
                                    <div style={{ display: "flex" }}>
                                        <Input addonAfter={<Button type="text" style={{ height: "20px", minWidth: 0, padding: "0px 0px" }} icon={<FolderOpenOutlined />} onClick={e => {
                                            e.stopPropagation();
                                            e.preventDefault();
                                            choiceImportPath(item.id);
                                        }} />} onChange={e => {
                                            e.stopPropagation();
                                            e.preventDefault();
                                            const tmpList = grpcImportPathList.slice();
                                            const itemIndex = tmpList.findIndex(tmpItem => tmpItem.id == item.id);
                                            if (itemIndex != -1) {
                                                tmpList[itemIndex].path = e.target.value.trim();
                                                setGrpcImportPathList(tmpList);
                                            }
                                        }} value={item.path} />
                                        <Button type="text" danger style={{ minWidth: 0, padding: "0px 0px", marginLeft: "10px" }} icon={<DeleteOutlined />} onClick={e => {
                                            e.stopPropagation();
                                            e.preventDefault();
                                            const tmpList = grpcImportPathList.filter(tmpItem => tmpItem.id != item.id);
                                            setGrpcImportPathList(tmpList);
                                        }} />
                                    </div>
                                </Form.Item>
                            ))}
                        </Form>
                    </Card>
                </div>
            )}
        </Modal>
    );
};

export default observer(CreateEntryModal);