//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { ENTRY_TYPE_SPRIT, type ENTRY_TYPE, ENTRY_TYPE_DOC, ENTRY_TYPE_PAGES, ENTRY_TYPE_API_COLL, ENTRY_TYPE_DRAW } from "@/api/project_entry";
import { TFunction } from "i18next";

export const getEntryTypeStr = (entryType: ENTRY_TYPE, t?: TFunction<"translation", undefined>): string => {
    if (entryType == ENTRY_TYPE_SPRIT) {
        return t == undefined ? "工作计划" : t("project.entry.type.workPlan");
    } else if (entryType == ENTRY_TYPE_DOC) {
        return t == undefined ? "文档" : t("project.entry.type.doc");
    } else if (entryType == ENTRY_TYPE_PAGES) {
        return t == undefined ? "静态网页" : t("project.entry.type.pages");
    } else if (entryType == ENTRY_TYPE_DRAW) {
        return t == undefined ? "绘图白板" : t("project.entry.type.draw");
    } else if (entryType == ENTRY_TYPE_API_COLL) {
        return t == undefined ? "接口集合" : t("project.entry.type.apiColl");
    }
    return "";
};