//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { observer } from 'mobx-react';
import type { PanelProps } from "./common";
import { Button, Card, Checkbox, Form, message, Space } from "antd";
import { useStores } from "@/hooks";
import { request } from "@/utils/request";
import { update_setting } from "@/api/project";

const LayoutSettingPanel = (props: PanelProps) => {
    const userStore = useStores('userStore');
    const projectStore = useStores('projectStore');
    const appStore = useStores('appStore');

    const [showRequireMent, setShowRequireMent] = useState(projectStore.curProject?.setting.show_require_ment_list_entry ?? false);
    const [showTask, setShowTask] = useState(projectStore.curProject?.setting.show_task_list_entry ?? false);
    const [showBug, setShowBug] = useState(projectStore.curProject?.setting.show_bug_list_entry ?? false);
    const [showTestCase, setShowTestCase] = useState(projectStore.curProject?.setting.show_test_case_list_entry ?? false);

    const [hasChange, setHasChange] = useState(false);



    const resetConfig = () => {
        setShowRequireMent(projectStore.curProject?.setting.show_require_ment_list_entry ?? false);
        setShowTask(projectStore.curProject?.setting.show_task_list_entry ?? false);
        setShowBug(projectStore.curProject?.setting.show_bug_list_entry ?? false);
        setShowTestCase(projectStore.curProject?.setting.show_test_case_list_entry ?? false);
    };

    const updateConfig = async () => {
        if (projectStore.curProject == undefined) {
            return;
        }
        await request(update_setting({
            session_id: userStore.sessionId,
            project_id: projectStore.curProjectId,
            setting: {
                ...projectStore.curProject.setting,
                show_require_ment_list_entry: showRequireMent,
                show_task_list_entry: showTask,
                show_bug_list_entry: showBug,
                show_test_case_list_entry: showTestCase,
            },
        }));
        message.info("保存成功");
        await projectStore.updateProject(projectStore.curProjectId);
        setHasChange(false);
    };

    useEffect(() => {
        props.onChange(hasChange);
    }, [hasChange]);

    return (
        <Card bordered={false} title={props.title} bodyStyle={{ height: "calc(100vh - 400px)", overflowY: "scroll" }}
            extra={
                <Space>
                    <Button disabled={!hasChange} onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        resetConfig();
                    }}>取消</Button>
                    <Button type="primary" disabled={!hasChange} onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        updateConfig();
                    }}>保存</Button>
                </Space>
            }>
            <Form labelCol={{ span: 9 }} disabled={projectStore.isClosed || !projectStore.isAdmin}>
                {appStore.vendorCfg?.project.show_requirement_list_entry && (
                    <Form.Item label="显示项目需求列表入口">
                        <Checkbox checked={showRequireMent} onChange={e => {
                            e.stopPropagation();
                            setShowRequireMent(e.target.checked);
                            setHasChange(true);
                        }} />
                    </Form.Item>
                )}

                {appStore.vendorCfg?.project.show_task_list_entry && (
                    <Form.Item label="显示项目任务列表入口">
                        <Checkbox checked={showTask} onChange={e => {
                            e.stopPropagation();
                            setShowTask(e.target.checked);
                            setHasChange(true);
                        }} />
                    </Form.Item>
                )}

                {appStore.vendorCfg?.project.show_bug_list_entry && (
                    <Form.Item label="显示项目缺陷列表入口">
                        <Checkbox checked={showBug} onChange={e => {
                            e.stopPropagation();
                            setShowBug(e.target.checked);
                            setHasChange(true);
                        }} />
                    </Form.Item>
                )}

                {appStore.vendorCfg?.project.show_testcase_list_entry && (
                    <Form.Item label="显示测试用例列表入口">
                        <Checkbox checked={showTestCase} onChange={e => {
                            e.stopPropagation();
                            setShowTestCase(e.target.checked);
                            setHasChange(true);
                        }} />
                    </Form.Item>
                )}
            </Form>
        </Card>
    );
};

export default observer(LayoutSettingPanel);