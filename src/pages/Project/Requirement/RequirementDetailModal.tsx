//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { Modal, Tabs } from "antd";
import { COMMENT_TARGET_REQUIRE_MENT } from "@/api/project_comment";
import CommentTab from "@/components/CommentEntry/CommentTab";
import CommentInModal from "@/components/CommentEntry/CommentInModal";
import DetailPanel from "./components/DetailPanel";
import LinkIssuePanel from "./components/LinkIssuePanel";
import FourQPanel from "./components/FourQPanel";
import KanoPanel from "./components/KanoPanel";
import EventListPanel from "./components/EventListPanel";
import type { ProjectInfo, TagInfo } from '@/api/project';

export interface RequirementDetailModalProps {
    projectInfo: ProjectInfo;
    tagList: TagInfo[];
    myUserId: string;
    requirementId: string;
    requirementTab: "detail" | "issue" | "fourq" | "kano" | "event" | "comment";

    onClose: () => void;
    onChange: (newTab: "detail" | "issue" | "fourq" | "kano" | "event" | "comment") => void;
    onClickTask: (projectId: string, issueId: string) => void;
}

const RequirementDetailModal = (props: RequirementDetailModalProps) => {
    const [commentDataVersion, setCommentDataVersion] = useState(0);

    useEffect(() => {
        if (props.requirementTab == "comment") {
            setTimeout(() => {
                setCommentDataVersion(oldValue => oldValue + 1);
            }, 500);
        }
    }, [props.requirementTab]);

    return (
        <Modal open title="项目需求" footer={null}
            width="800px" mask={false}
            bodyStyle={{ height: "calc(100vh - 300px)", padding: "0px 10px", overflowY: "hidden" }}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onClose();
            }}>
            <Tabs activeKey={props.requirementTab}
                onChange={key => props.onChange((key as "detail" | "issue" | "fourq" | "kano" | "event" | "comment"))}
                type="card" tabPosition="left" size="large"
                items={[
                    {
                        key: "detail",
                        label: "需求详情",
                        children: (
                            <div style={{ height: "calc(100vh - 320px)", overflowY: "scroll" }}>
                                {props.requirementTab == "detail" && (
                                    <DetailPanel projectInfo={props.projectInfo} tagList={props.tagList} requirementId={props.requirementId} myUserId={props.myUserId}
                                        onClose={() => props.onClose()} />
                                )}
                            </div>
                        ),
                    },
                    {
                        key: "issue",
                        label: "相关任务",
                        children: (
                            <div style={{ height: "calc(100vh - 320px)", overflowY: "scroll" }}>
                                {props.requirementTab == "issue" && (
                                    <LinkIssuePanel projectInfo={props.projectInfo} myUserId={props.myUserId} requirementId={props.requirementId} inModal
                                        onClickTask={(projectId, issueId) => props.onClickTask(projectId, issueId)} />
                                )}
                            </div>
                        ),
                    },
                    {
                        key: "fourq",
                        label: "四象限分析",
                        children: (
                            <div style={{ height: "calc(100vh - 320px)", overflowY: "scroll" }}>
                                {props.requirementTab == "fourq" && (
                                    <FourQPanel projectInfo={props.projectInfo} requirementId={props.requirementId} />
                                )}
                            </div>
                        ),
                    },
                    {
                        key: "kano",
                        label: "KANO分析",
                        children: (
                            <div style={{ height: "calc(100vh - 320px)", overflowY: "scroll" }}>
                                {props.requirementTab == "kano" && (
                                    <KanoPanel projectInfo={props.projectInfo} requirementId={props.requirementId} />
                                )}
                            </div>
                        ),
                    },
                    {
                        key: "event",
                        label: "操作历史",
                        children: (
                            <div style={{ height: "calc(100vh - 320px)", overflowY: "scroll" }}>
                                {props.requirementTab == "event" && (
                                    <EventListPanel projecttId={props.projectInfo.project_id} requirementId={props.requirementId} />
                                )}
                            </div>
                        ),
                    },
                    {
                        key: "comment",
                        label: <CommentTab projectId={props.projectInfo.project_id} targetType={COMMENT_TARGET_REQUIRE_MENT} targetId={props.requirementId} dataVersion={commentDataVersion} />,
                        children: (
                            <div style={{ height: "calc(100vh - 320px)", overflowY: "scroll", paddingRight: "10px" }}>
                                <CommentInModal projectId={props.projectInfo.project_id} targetType={COMMENT_TARGET_REQUIRE_MENT} targetId={props.requirementId}
                                    myUserId={props.myUserId} myAdmin={props.projectInfo.user_project_perm.can_admin} enableAdd={!props.projectInfo.closed}/>
                            </div>
                        ),
                    },
                ]} />
        </Modal>
    );
};

export default RequirementDetailModal;