//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import type { PluginEvent } from '@/api/events';
import { EVENT_REF_TYPE_REQUIRE_MENT, EVENT_TYPE_REQUIRE_MENT } from '@/api/events';
import { request } from "@/utils/request";
import { list_event_by_ref } from '@/api/events';
import { Space, Timeline } from "antd";
import EventCom from "@/components/EventCom";
import moment from "moment";
import UserPhoto from "@/components/Portrait/UserPhoto";
import { get_session } from "@/api/user";

export interface EventListPanelProps {
    projecttId: string;
    requirementId: string;
}

const EventListPanel = (props:EventListPanelProps) => {

    const [timeLine, setTimeLine] = useState<PluginEvent[]>();

    const loadEvent = async () => {
        const sessionId = await get_session();
        const res = await request(list_event_by_ref({
            session_id: sessionId,
            project_id: props.projecttId,
            event_type: EVENT_TYPE_REQUIRE_MENT,
            ref_type: EVENT_REF_TYPE_REQUIRE_MENT,
            ref_id: props.requirementId,
        }));
        setTimeLine(res.event_list);
    }

    useEffect(() => {
        loadEvent();
    }, [props.requirementId]);

    return (
        <Timeline reverse={true} style={{ paddingTop: "10px" }}>
            {timeLine?.map((item) => (
                <Timeline.Item color="gray" key={item.event_id}>
                    <Space>
                        <UserPhoto logoUri={item.cur_logo_uri} style={{ width: "16px", borderRadius: "10px" }} />
                        <span>
                            {item.cur_user_display_name}({moment(item.event_time).format("YYYY-MM-DD HH:mm:ss")})
                        </span>
                    </Space>
                    <EventCom key={item.event_id} item={item} skipProjectName={true} skipLink={true} showMoreLink={false} />
                </Timeline.Item>
            ))}
        </Timeline>
    );
};

export default EventListPanel;