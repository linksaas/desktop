//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { Input, Modal } from "antd";
import React, { useState } from "react";
import { create as create_issue, ISSUE_TYPE_TASK, TASK_PRIORITY_LOW } from '@/api/project_issue';
import { request } from "@/utils/request";
import { link_issue } from '@/api/project_requirement';
import { report_error } from "@/api/client_cfg";
import { get_session } from "@/api/user";
import { ProjectInfo } from "@/api/project";

interface BatchCreateTaskProps {
    projectInfo: ProjectInfo;
    requirementId: string;
    onCancel: () => void;
    onOk: () => void;
}

const BatchCreateTask: React.FC<BatchCreateTaskProps> = (props) => {
    const [titles, setTitles] = useState("");
    const [disabled, setDisabled] = useState(true);

    const batchCreate = async () => {
        const sessionId = await get_session();
        const titleList = titles.split("\n");
        for (let title of titleList) {
            title = title.trim();
            if (title.length == 0) {
                continue;
            }
            try {
                const res = await request(create_issue({
                    session_id: sessionId,
                    project_id: props.projectInfo.project_id,
                    issue_type: ISSUE_TYPE_TASK,
                    basic_info: {
                        title: title,
                        content: JSON.stringify({ type: "doc" }),
                        tag_id_list: [],
                    },
                    extra_info: {
                        ExtraTaskInfo: {
                            priority: TASK_PRIORITY_LOW,
                        },
                        ExtraBugInfo: undefined,
                    }
                }));
                await request(link_issue({
                    session_id: sessionId,
                    project_id: props.projectInfo.project_id,
                    requirement_id: props.requirementId,
                    issue_id: res.issue_id,
                }));
            } catch (e) {
                console.log(e);
                report_error({
                    err_data: `${e}`,
                });
            }
        }
        props.onOk();
    };

    return (
        <Modal
            title="批量创建任务"
            open mask={false}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onCancel();
            }}
            okButtonProps={{ disabled: disabled }}
            okText="创建"
            onOk={e => {
                e.stopPropagation();
                e.preventDefault();
                batchCreate();
            }}>
            <p>下面每一行将作为一个任务标题</p>
            <Input.TextArea rows={10} onChange={e => {
                e.stopPropagation();
                e.preventDefault();
                setTitles(e.target.value);
                if (e.target.value.trim().length == 0) {
                    setDisabled(true);
                } else {
                    setDisabled(false);
                }
            }} />
        </Modal>
    );
};

export default BatchCreateTask;