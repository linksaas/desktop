//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { Button, Form, Input, Modal, Popover, Space, message } from "antd";
import { ReadOnlyEditor, is_empty_doc, useCommonEditor } from "@/components/Editor";
import { FILE_OWNER_TYPE_BULLETIN } from "@/api/fs";
import { request } from "@/utils/request";
import { get as get_bulletin, update as update_bulletin, remove as remove_bulletin } from "@/api/project_bulletin";
import { MoreOutlined } from "@ant-design/icons";
import moment from "moment";
import type { ProjectInfo } from "@/api/project";
import { get_session } from "@/api/user";
import { useTranslation } from "react-i18next";

export interface ViewBulletinModalProps {
    projectInfo: ProjectInfo;
    bulletinId: string;
    onClose: () => void;
}

const ViewBulletinModal = (props: ViewBulletinModalProps) => {
    const { t } = useTranslation();

    const [inEdit, setInEdit] = useState(false);
    const [title, setTitle] = useState("");
    const [content, setContent] = useState("");
    const [createDisplayName, setCreateDisplayName] = useState("");
    const [createTime, setCreateTime] = useState(0);

    const { editor, editorRef } = useCommonEditor({
        content: "",
        fsId: props.projectInfo.bulletin_fs_id,
        ownerType: FILE_OWNER_TYPE_BULLETIN,
        ownerId: props.bulletinId,
        historyInToolbar: false,
        clipboardInToolbar: false,
        commonInToolbar: true,
        pubResInToolbar: false,
    });

    const loadBulletin = async () => {
        const sessionId = await get_session();
        const res = await request(get_bulletin({
            session_id: sessionId,
            project_id: props.projectInfo.project_id,
            bulletin_id: props.bulletinId,
        }));
        setTitle(res.key_info.title);
        editorRef.current?.setContent(res.content);
        setContent(res.content);
        setCreateDisplayName(res.key_info.create_display_name);
        setCreateTime(res.key_info.create_time);
    };

    const updateBulletin = async () => {
        const sessionId = await get_session();
        if (title.trim() == "") {
            message.error(t("project.bulletin.emptyTitle"));
            return;
        }
        const newContent = editorRef.current?.getContent() ?? {
            type: 'doc',
        };
        if (is_empty_doc(newContent)) {
            message.error(t("project.bulletin.emptyContent"));
            return;
        }
        await request(update_bulletin({
            session_id: sessionId,
            project_id: props.projectInfo.project_id,
            bulletin_id: props.bulletinId,
            title: title.trim(),
            content: JSON.stringify(newContent),
        }));
        setInEdit(false);
        message.info(t("text.updateSuccess"));
        await loadBulletin();
    };

    const removeBulletin = async () => {
        const sessionId = await get_session();
        await request(remove_bulletin({
            session_id: sessionId,
            project_id: props.projectInfo.project_id,
            bulletin_id: props.bulletinId,
        }));
        message.info(t("project.moveRecycleSuccess"));
        props.onClose();
    };

    useEffect(() => {
        loadBulletin();
    }, []);

    return (
        <Modal open title={inEdit ? t("project.bulletin.update") : t("project.bulletin.view")} footer={null} mask={false}
            width="calc(100vw - 400px)"
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onClose();
            }}>
            {props.projectInfo.user_project_perm.can_admin && !props.projectInfo.closed && (
                <div style={{ display: "flex", borderBottom: "1px solid #e4e4e8", paddingBottom: "4px", marginBottom: "10px" }}>
                    <div style={{ flex: 1 }}>{t("project.bulletin.updateTip")}</div>
                    <Space>
                        {inEdit == false && (
                            <>
                                <Button type="primary" onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    setInEdit(true);
                                    const t = setInterval(() => {
                                        if (editorRef.current != null) {
                                            clearInterval(t);
                                            editorRef.current.setContent(content);
                                        }
                                    }, 100)
                                }}>{t("text.edit")}</Button>
                                <Popover trigger="click" placement="bottom" content={
                                    <div>
                                        <Button type="link" danger onClick={e => {
                                            e.stopPropagation();
                                            e.preventDefault();
                                            removeBulletin();
                                        }}>{t("text.moveRecycle")}</Button>
                                    </div>
                                }>
                                    <MoreOutlined />
                                </Popover>
                            </>
                        )}
                        {inEdit == true && (
                            <>
                                <Button onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    loadBulletin().then(() => {
                                        setInEdit(false);
                                    });
                                }}>{t("text.cancel")}</Button>
                                <Button type="primary" onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    updateBulletin();
                                }}>{t("text.update")}</Button>
                            </>
                        )}
                    </Space>
                </div>
            )}
            <Form labelCol={{ span: 2 }}>
                <Form.Item label={t("text.title")}>
                    {inEdit == true && (
                        <Input value={title} onChange={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            setTitle(e.target.value);
                        }} />
                    )}
                    {inEdit == false && (
                        <div>{title}</div>
                    )}
                </Form.Item>
                {inEdit == false && (
                    <Form.Item label={t("project.bulletin.publisher")}>{createDisplayName}&nbsp;
                        {createTime != 0 && moment(createTime).format("(YYYY-MM-DD HH:mm)")}</Form.Item>
                )}
                <Form.Item label={t("text.content")}>
                    {inEdit == true && (
                        <div className="_editChatContext">{editor}</div>
                    )}
                    {inEdit == false && content != "" && (
                        <div className="_editChatContext">
                            <ReadOnlyEditor content={content} />
                        </div>
                    )}
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default ViewBulletinModal;

