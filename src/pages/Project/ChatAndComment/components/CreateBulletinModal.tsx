//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from "react";
import { Form, Input, Modal, message } from "antd";
import { create } from "@/api/project_bulletin";
import { request } from "@/utils/request";
import { change_file_owner, is_empty_doc, useCommonEditor } from "@/components/Editor";
import { FILE_OWNER_TYPE_BULLETIN, FILE_OWNER_TYPE_PROJECT } from "@/api/fs";
import type { ProjectInfo } from "@/api/project";
import { get_session } from "@/api/user";
import { useTranslation } from "react-i18next";

export interface CreateBulletinModalProps {
    projectInfo: ProjectInfo;
    onClose: () => void;
}

const CreateBulletinModal = (props: CreateBulletinModalProps) => {
    const { t } = useTranslation();

    const [title, setTitle] = useState("");

    const { editor, editorRef } = useCommonEditor({
        content: "",
        fsId: props.projectInfo.bulletin_fs_id ?? "",
        ownerType: FILE_OWNER_TYPE_PROJECT,
        ownerId: props.projectInfo.project_id,
        historyInToolbar: false,
        clipboardInToolbar: false,
        commonInToolbar: true,
        pubResInToolbar: false,
    });

    const createBulletin = async () => {
        const sessionId = await get_session();
        if (title.trim() == "") {
            message.error(t("project.bulletin.emptyTitle"));
            return;
        }
        const content = editorRef.current?.getContent() ?? {
            type: 'doc',
        };
        if (is_empty_doc(content)) {
            message.error(t("project.bulletin.emptyContent"));
            return;
        }
        const createRes = await request(create({
            session_id: sessionId,
            project_id: props.projectInfo.project_id,
            title: title.trim(),
            content: JSON.stringify(content),
        }));
        //变更文件Owner
        await change_file_owner(content, sessionId, FILE_OWNER_TYPE_BULLETIN, createRes.bulletin_id);
        props.onClose();
        message.info(t("text.createSuccess"));
    };

    return (
        <Modal title={t("project.bulletin.create")} open okText={t("text.create")} okButtonProps={{ disabled: title.trim() == "" }}
            width="calc(100vw - 400px)" mask={false}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onClose();
            }}
            onOk={e => {
                e.stopPropagation();
                e.preventDefault();
                createBulletin();
            }}>
            <Form labelCol={{ span: 2 }}>
                <Form.Item label={t("text.title")}>
                    <Input value={title} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setTitle(e.target.value);
                    }} />
                </Form.Item>
                <Form.Item label={t("text.content")}>
                    <div className="_editChatContext">{editor}</div>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default CreateBulletinModal;