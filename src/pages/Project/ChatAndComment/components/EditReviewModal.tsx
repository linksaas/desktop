//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from "react";
import { Form, Input, Modal, Select, Space, message } from "antd";
import type { SimpleReviewInfo } from "@/api/project_review";
import { create as create_review, update_basic, REVIEW_TYPE_BUG, REVIEW_TYPE_CODE, REVIEW_TYPE_FLOW, REVIEW_TYPE_MONTH, REVIEW_TYPE_PRODUCT, REVIEW_TYPE_QUARTER, REVIEW_TYPE_TEST } from "@/api/project_review";
import { request } from "@/utils/request";
import type { ProjectInfo } from "@/api/project";
import { get_session } from "@/api/user";
import type { MemberInfo } from "@/api/project_member";
import UserPhoto from "@/components/Portrait/UserPhoto";
import { useTranslation } from "react-i18next";


export interface EditReviewModalProps {
    reviewInfo?: SimpleReviewInfo;
    projectInfo: ProjectInfo;
    memberList: MemberInfo[];
    onClose: () => void;
}

const EditReviewModal = (props: EditReviewModalProps) => {
    const { t } = useTranslation();

    const [title, setTitle] = useState(props.reviewInfo?.title ?? "");
    const [reviewType, setReviewType] = useState(props.reviewInfo?.review_type ?? REVIEW_TYPE_CODE);
    const [memberUserIdList, setMemberUserIdList] = useState<string[]>((props.reviewInfo?.member_list ?? []).map(item => item.member_user_id));
    const [desc, SetDesc] = useState(props.reviewInfo?.desc ?? "");

    const createReview = async () => {
        const sessionId = await get_session();
        await request(create_review({
            session_id: sessionId,
            project_id: props.projectInfo.project_id,
            title: title,
            review_type: reviewType,
            desc: desc,
            member_user_id_list: memberUserIdList,
        }));
        message.info(t("text.createSuccess"));
        props.onClose();
    };

    const updateReview = async () => {
        if (props.reviewInfo == undefined) {
            return;
        }
        const sessionId = await get_session();
        await request(update_basic({
            session_id: sessionId,
            project_id: props.projectInfo.project_id,
            review_id: props.reviewInfo.review_id,
            title: title,
            review_type: reviewType,
            desc: desc,
            member_user_id_list: memberUserIdList,
        }));
        message.info(t("text.updateSuccess"));
        props.onClose();
    };

    return (
        <Modal open title={props.reviewInfo == undefined ? t("project.review.create") : t("project.review.update")} mask={false}
            okText={props.reviewInfo == undefined ? t("text.create") : t("text.update")} okButtonProps={{ disabled: title == "" }}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onClose();
            }}
            onOk={e => {
                e.stopPropagation();
                e.preventDefault();
                if (props.reviewInfo == undefined) {
                    createReview();
                } else {
                    updateReview();
                }
            }}>
            <Form labelCol={{ span: 5 }}>
                <Form.Item label={t("text.title")}>
                    <Input value={title} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setTitle(e.target.value.trim());
                    }} />
                </Form.Item>
                <Form.Item label={t("text.cate")}>
                    <Select value={reviewType} onChange={value => setReviewType(value)}>
                        <Select.Option value={REVIEW_TYPE_CODE}>{t("project.review.cate.code")}</Select.Option>
                        <Select.Option value={REVIEW_TYPE_FLOW}>{t("project.review.cate.flow")}</Select.Option>
                        <Select.Option value={REVIEW_TYPE_PRODUCT}>{t("project.review.cate.product")}</Select.Option>
                        <Select.Option value={REVIEW_TYPE_BUG}>{t("project.review.cate.bug")}</Select.Option>
                        <Select.Option value={REVIEW_TYPE_TEST}>{t("project.review.cate.test")}</Select.Option>
                        <Select.Option value={REVIEW_TYPE_MONTH}>{t("project.review.cate.month")}</Select.Option>
                        <Select.Option value={REVIEW_TYPE_QUARTER}>{t("project.review.cate.quarter")}</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item label={t("project.review.releateMember")}>
                    <Select value={memberUserIdList} mode="multiple" onChange={values => setMemberUserIdList(values)}>
                        {props.memberList.map(memberItem => (
                            <Select.Option key={memberItem.member_user_id} value={memberItem.member_user_id}>
                                <Space>
                                    <UserPhoto logoUri={memberItem.logo_uri} style={{ width: "20px", borderRadius: "10px" }} />
                                    {memberItem.display_name}
                                </Space>
                            </Select.Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item label={t("text.desc")}>
                    <Input.TextArea value={desc} autoSize={{ minRows: 3, maxRows: 3 }} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        SetDesc(e.target.value);
                    }} />
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default EditReviewModal;