//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { observer } from 'mobx-react';
import { List, Popover, Space, Tag } from "antd";
import { useStores } from "@/hooks";
import type { ThreadInfo } from "@/api/project_code";
import { list_thread } from "@/api/project_code";
import { request } from "@/utils/request";
import moment from "moment";
import ReactMarkdown from 'react-markdown';
import UserPhoto from "@/components/Portrait/UserPhoto";

const PAGE_SIZE = 20;

const CodeCommentThreadList = () => {
    const userStore = useStores("userStore");
    const projectStore = useStores("projectStore");

    const [threadList, setThreadList] = useState<ThreadInfo[]>([]);
    const [totalCount, setTotalCount] = useState(0);
    const [curPage, setCurPage] = useState(0);

    const loadThreadList = async () => {
        const res = await request(list_thread({
            session_id: userStore.sessionId,
            project_id: projectStore.curProjectId,
            offset: curPage * PAGE_SIZE,
            limit: PAGE_SIZE,
        }));
        setTotalCount(res.total_count);
        setThreadList(res.thread_list);
    };

    useEffect(() => {
        if (projectStore.curProjectId != "") {
            loadThreadList();
        }
    }, [projectStore.curProjectId, curPage]);

    return (
        <List rowKey="thread_id" dataSource={threadList}
            pagination={{ total: totalCount, current: curPage + 1, pageSize: PAGE_SIZE, onChange: page => setCurPage(page - 1), showSizeChanger: false, hideOnSinglePage: true }}
            renderItem={thread => (
                <List.Item style={{ paddingLeft: "10px" }}
                    extra={
                        <div style={{ width: "100px" }}>
                            <div>
                                <a onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    projectStore.setCodeCommentInfo(thread.thread_id, thread.last_comment.comment_id);
                                }}>{thread.comment_count}条评论</a>
                            </div>
                            <div>
                                <Popover placement="bottom" trigger={["click", "hover"]} content={
                                    <Space style={{ width: "200px", flexWrap: "wrap" }}>
                                        {thread.comment_user_list.map(commentUser => (
                                            <Tag style={{ padding: "4px 8px" }}>
                                                <UserPhoto logoUri={commentUser.comment_logo_uri} width="20px" height="20px" style={{
                                                    borderRadius: '10px',
                                                    marginRight: "4px"
                                                }} />
                                                {commentUser.comment_display_name}
                                            </Tag>
                                        ))}
                                    </Space>
                                }>
                                    {thread.comment_user_list.length}人参与
                                </Popover>
                            </div>
                        </div>
                    }>
                    <div style={{ display: "flex", width: "360px" }}>
                        <div style={{ width: "100px" }}>
                            <div>{moment(thread.last_comment.update_time).format("YYYY-MM-DD HH:mm")}</div>
                            <div>
                                <UserPhoto logoUri={thread.last_comment.user_logo_uri} width="20px" height="20px" style={{
                                    borderRadius: '10px',
                                    marginRight: "10px"
                                }} />
                                {thread.last_comment.user_display_name}
                            </div>
                        </div>
                        <div style={{ flex: 1 }}>
                            <ReactMarkdown linkTarget="_blank">{thread.last_comment.content}</ReactMarkdown>
                        </div>
                    </div>
                </List.Item>
            )} />
    );
};

export default observer(CodeCommentThreadList);