//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { observer, useLocalObservable } from 'mobx-react';
import {
    ASSGIN_USER_ALL, ISSUE_STATE_CHECK, ISSUE_STATE_PROCESS, ISSUE_TYPE_BUG, ISSUE_TYPE_TASK, SORT_KEY_UPDATE_TIME, SORT_TYPE_DSC,
    list as list_issue
} from "@/api/project_issue";
import { useStores } from "@/hooks";
import { request } from "@/utils/request";
import { LocalIssueStore, LocalRequirementStore, LocalTestcaseStore } from "@/stores/local";
import type { ColumnsType } from 'antd/lib/table';
import type { ISSUE_TYPE, IssueInfo } from "@/api/project_issue";
import { Button, Card, Empty, Space, Table, Tooltip } from "antd";
import { CloseOutlined, LinkOutlined } from "@ant-design/icons";
import { LinkBugInfo, LinkEntryInfo, LinkRequirementInfo, LinkTaskInfo, LinkTestCaseInfo } from "@/stores/linkAux";
import { useHistory } from "react-router-dom";
import { issueState } from "@/utils/constant";
import { getStateColor } from "@/pages/Issue/components/utils";
import { ENTRY_TYPE_API_COLL, ENTRY_TYPE_DOC, ENTRY_TYPE_PAGES, ENTRY_TYPE_SPRIT, list as list_entry } from "@/api/project_entry";
import type { EntryInfo } from "@/api/project_entry";
import PagesModal from "../Home/components/PagesModal";
import { REQ_SORT_UPDATE_TIME, list_requirement } from "@/api/project_requirement";
import type { RequirementInfo } from "@/api/project_requirement";
import { list_project_event, type PluginEvent } from "@/api/events";
import moment from "moment";
import style from "./MyWork.module.less";
import EventCom from "@/components/EventCom";
import { EVENT_ICON_LIST } from "../Record/common";
import { timeToDateString } from "@/utils/utils";
import { listen } from "@tauri-apps/api/event";
import type * as NoticeType from '@/api/notice_type';
import { unwatch, WATCH_TARGET_BUG, WATCH_TARGET_ENTRY, WATCH_TARGET_REQUIRE_MENT, WATCH_TARGET_TASK, WATCH_TARGET_TEST_CASE } from "@/api/project_watch";
import { CaseInfo, type FolderOrCaseInfo, list_case_flat } from "@/api/project_testcase";

const PAGE_SIZE = 10;

const TodoIssueList = observer(() => {
    const history = useHistory();

    const userStore = useStores("userStore");
    const projectStore = useStores("projectStore");
    const linkAuxStore = useStores('linkAuxStore');

    const issueStore = useLocalObservable(() => new LocalIssueStore(projectStore.curProjectId, ""));

    const [totalCount, setTotalCount] = useState(0);
    const [curPage, setCurPage] = useState(0);

    const loadIssueList = async () => {
        const res = await request(list_issue({
            session_id: userStore.sessionId,
            project_id: projectStore.curProjectId,
            list_param: {
                filter_by_issue_type: false,
                issue_type: 0,
                filter_by_state: true,
                state_list: [ISSUE_STATE_PROCESS, ISSUE_STATE_CHECK],
                filter_by_create_user_id: false,
                create_user_id_list: [],
                filter_by_assgin_user_id: true,
                assgin_user_id_list: [userStore.userInfo.userId],
                assgin_user_type: ASSGIN_USER_ALL,
                filter_by_sprit_id: false,
                sprit_id_list: [],
                filter_by_create_time: false,
                from_create_time: 0,
                to_create_time: 0,
                filter_by_update_time: false,
                from_update_time: 0,
                to_update_time: 0,
                filter_by_title_keyword: false,
                title_keyword: "",
                filter_by_tag_id_list: false,
                tag_id_list: [],
                filter_by_watch: false,
                ///任务相关
                filter_by_task_priority: false,
                task_priority_list: [],
                ///缺陷相关
                filter_by_software_version: false,
                software_version_list: [],
                filter_by_bug_priority: false,
                bug_priority_list: [],
                filter_by_bug_level: false,
                bug_level_list: [],
            },
            sort_type: SORT_TYPE_DSC,
            sort_key: SORT_KEY_UPDATE_TIME,
            offset: curPage * PAGE_SIZE,
            limit: PAGE_SIZE,
        }));
        setTotalCount(res.total_count);
        issueStore.itemList = res.info_list;
    };

    const columns: ColumnsType<IssueInfo> = [
        {
            title: "标题",
            ellipsis: true,
            render: (_, record: IssueInfo) => {
                return (
                    <span
                        style={{ cursor: 'pointer', display: "inline-block", paddingTop: "5px", paddingBottom: "5px" }}
                        onClick={(e) => {
                            e.stopPropagation();
                            e.preventDefault();
                            if (record.issue_type == ISSUE_TYPE_TASK) {
                                linkAuxStore.goToLink(new LinkTaskInfo("", record.project_id, record.issue_id), history);
                            } else if (record.issue_type == ISSUE_TYPE_BUG) {
                                linkAuxStore.goToLink(new LinkBugInfo("", record.project_id, record.issue_id), history);
                            }
                        }}
                    >
                        <a><LinkOutlined />&nbsp;{record.basic_info.title}</a>
                    </span>
                );
            },
        },
        {
            title: "类型",
            width: 60,
            render: (_, row: IssueInfo) => row.issue_type == ISSUE_TYPE_TASK ? "任务" : "缺陷",
        },
        {
            title: '状态',
            dataIndex: 'state',
            width: 100,
            align: 'center',
            render: (val: number, row: IssueInfo) => {
                const v = issueState[val];
                let tips = "";
                if (row.user_issue_perm.next_state_list.length == 0) {
                    if ([ISSUE_STATE_PROCESS, ISSUE_STATE_CHECK].includes(row.state) && (
                        (userStore.userInfo.userId == row.exec_user_id) || (userStore.userInfo.userId == row.check_user_id)
                    )) {
                        if (row.sub_issue_status.done_count < row.sub_issue_status.total_count) {
                            tips = "子任务未完成";
                        } else {
                            tips = "请等待同事更新状态";
                        }
                    }
                }
                return (
                    <div
                        tabIndex={0}
                        style={{
                            background: `rgb(${getStateColor(val)} / 20%)`,
                            width: '60px',
                            borderRadius: '50px',
                            textAlign: 'center',
                            color: `rgb(${getStateColor(val)})`,
                            margin: '0 auto',
                        }}
                    >
                        <Tooltip title={tips}>{v.label}</Tooltip>
                    </div>
                );
            },
        },
    ];

    useEffect(() => {
        if (projectStore.curProjectId != "") {
            loadIssueList();
        }
    }, [curPage]);

    useEffect(() => {
        return () => {
            issueStore.unlisten();
        };
    }, []);

    return (
        <Table rowKey="issue_id" dataSource={issueStore.itemList} style={{ minHeight: "200px" }}
            columns={columns}
            pagination={{
                current: curPage + 1,
                pageSize: PAGE_SIZE,
                total: totalCount,
                onChange: page => setCurPage(page - 1),
                hideOnSinglePage: true,
            }} />
    );
});

interface WatchIssueListProps {
    issueType: ISSUE_TYPE,
}

const WatchIssueList = observer((props: WatchIssueListProps) => {
    const history = useHistory();

    const userStore = useStores('userStore');
    const projectStore = useStores('projectStore');
    const linkAuxStore = useStores('linkAuxStore');

    const issueStore = useLocalObservable(() => new LocalIssueStore(projectStore.curProjectId, ""));

    const [totalCount, setTotalCount] = useState(0);
    const [curPage, setCurPage] = useState(0);

    const loadIssueList = async () => {
        const res = await request(list_issue({
            session_id: userStore.sessionId,
            project_id: projectStore.curProjectId,
            list_param: {
                filter_by_issue_type: true,
                issue_type: props.issueType,
                filter_by_state: false,
                state_list: [],
                filter_by_create_user_id: false,
                create_user_id_list: [],
                filter_by_assgin_user_id: false,
                assgin_user_id_list: [],
                assgin_user_type: ASSGIN_USER_ALL,
                filter_by_sprit_id: false,
                sprit_id_list: [],
                filter_by_create_time: false,
                from_create_time: 0,
                to_create_time: 0,
                filter_by_update_time: false,
                from_update_time: 0,
                to_update_time: 0,
                filter_by_title_keyword: false,
                title_keyword: "",
                filter_by_tag_id_list: false,
                tag_id_list: [],
                filter_by_watch: true,

                ///任务相关
                filter_by_task_priority: false,
                task_priority_list: [],
                ///缺陷相关
                filter_by_software_version: false,
                software_version_list: [],
                filter_by_bug_priority: false,
                bug_priority_list: [],
                filter_by_bug_level: false,
                bug_level_list: [],
            },
            sort_type: SORT_TYPE_DSC,
            sort_key: SORT_KEY_UPDATE_TIME,
            offset: curPage * PAGE_SIZE,
            limit: PAGE_SIZE,
        }));
        setTotalCount(res.total_count);
        issueStore.itemList = res.info_list;
    };

    const columns: ColumnsType<IssueInfo> = [
        {
            title: "标题",
            ellipsis: true,
            render: (_, record: IssueInfo) => {
                return (
                    <span
                        style={{ cursor: 'pointer', display: "inline-block", paddingTop: "5px", paddingBottom: "5px" }}
                        onClick={(e) => {
                            e.stopPropagation();
                            e.preventDefault();
                            if (record.issue_type == ISSUE_TYPE_TASK) {
                                linkAuxStore.goToLink(new LinkTaskInfo("", record.project_id, record.issue_id), history);
                            } else if (record.issue_type == ISSUE_TYPE_BUG) {
                                linkAuxStore.goToLink(new LinkBugInfo("", record.project_id, record.issue_id), history);
                            }
                        }}
                    >
                        <a><LinkOutlined />&nbsp;{record.basic_info.title}</a>
                    </span>
                );
            },
        },
        {
            title: "类型",
            width: 60,
            render: (_, row: IssueInfo) => row.issue_type == ISSUE_TYPE_TASK ? "任务" : "缺陷",
        },
        {
            title: '状态',
            dataIndex: 'state',
            width: 100,
            align: 'center',
            render: (val: number, row: IssueInfo) => {
                const v = issueState[val];
                let tips = "";
                if (row.user_issue_perm.next_state_list.length == 0) {
                    if ([ISSUE_STATE_PROCESS, ISSUE_STATE_CHECK].includes(row.state) && (
                        (userStore.userInfo.userId == row.exec_user_id) || (userStore.userInfo.userId == row.check_user_id)
                    )) {
                        if (row.sub_issue_status.done_count < row.sub_issue_status.total_count) {
                            tips = "子任务未完成";
                        } else {
                            tips = "请等待同事更新状态";
                        }
                    }
                }
                return (
                    <div
                        tabIndex={0}
                        style={{
                            background: `rgb(${getStateColor(val)} / 20%)`,
                            width: '60px',
                            borderRadius: '50px',
                            textAlign: 'center',
                            color: `rgb(${getStateColor(val)})`,
                            margin: '0 auto',
                        }}
                    >
                        <Tooltip title={tips}>{v.label}</Tooltip>
                    </div>
                );
            },
        },
        {
            title: "操作",
            width: 80,
            render: (_, row: IssueInfo) => (
                <Button type="link" style={{ minWidth: 0, padding: "0px 0px" }}
                    onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        request(unwatch({
                            session_id: userStore.sessionId,
                            project_id: projectStore.curProjectId,
                            target_type: row.issue_type == ISSUE_TYPE_TASK ? WATCH_TARGET_TASK : WATCH_TARGET_BUG,
                            target_id: row.issue_id,
                        }));
                    }}>取消关注</Button>
            ),
        }
    ];

    useEffect(() => {
        if (projectStore.curProjectId != "") {
            loadIssueList();
        }
    }, [curPage]);

    useEffect(() => {
        return () => {
            issueStore.unlisten();
        };
    }, []);

    useEffect(() => {
        const unListenFn = listen<NoticeType.AllNotice>("notice", ev => {
            if (ev.payload.ClientNotice?.WatchChangeNotice != undefined && [WATCH_TARGET_TASK, WATCH_TARGET_BUG].includes(ev.payload.ClientNotice.WatchChangeNotice.targetType)) {
                loadIssueList();
            }
        });

        return () => {
            unListenFn.then((unListen) => unListen());
        };
    }, []);

    return (
        <Table rowKey="issue_id" dataSource={issueStore.itemList} style={{ minHeight: "200px" }}
            columns={columns}
            pagination={{
                current: curPage + 1,
                pageSize: PAGE_SIZE,
                total: totalCount,
                onChange: page => setCurPage(page - 1),
                hideOnSinglePage: true,
            }} />
    );
});

const WatchEntryList = () => {
    const history = useHistory();

    const userStore = useStores('userStore');
    const projectStore = useStores('projectStore');
    const linkAuxStore = useStores('linkAuxStore');

    const [entryList, setEntryList] = useState([] as EntryInfo[]);
    const [totalCount, setTotalCount] = useState(0);
    const [curPage, setCurPage] = useState(0);

    const [showPagesEntryInfo, setShowPagesEntryInfo] = useState<EntryInfo | null>(null);

    const loadEntryList = async () => {
        if (projectStore.curProjectId == "") {
            setTotalCount(0);
            setEntryList([]);
            return;
        }
        const res = await request(list_entry({
            session_id: userStore.sessionId,
            project_id: projectStore.curProjectId,
            list_param: {
                filter_by_watch: true,
                filter_by_tag_id: false,
                tag_id_list: [],
                filter_by_keyword: false,
                keyword: "",
                filter_by_entry_type: false,
                entry_type_list: [],
            },
            offset: curPage * PAGE_SIZE,
            limit: PAGE_SIZE,
        }));
        setTotalCount(res.total_count);
        setEntryList(res.entry_list);
    };

    const columns: ColumnsType<EntryInfo> = [
        {
            title: "名称",
            width: 150,
            render: (_, row: EntryInfo) => (<a onClick={e => {
                e.stopPropagation();
                e.preventDefault();
                if (row.entry_type == ENTRY_TYPE_PAGES) {
                    setShowPagesEntryInfo(row);
                } else {
                    linkAuxStore.goToLink(new LinkEntryInfo("", projectStore.curProjectId, row.entry_id), history);
                }
            }}>{row.entry_title}</a>),
        },
        {
            title: "类型",
            width: 100,
            render: (_, row: EntryInfo) => {
                if (row.entry_type == ENTRY_TYPE_SPRIT) {
                    return "工作计划"
                } else if (row.entry_type == ENTRY_TYPE_DOC) {
                    return "文档";
                } else if (row.entry_type == ENTRY_TYPE_PAGES) {
                    return "静态网页";
                } else if (row.entry_type == ENTRY_TYPE_API_COLL) {
                    return "接口集合"
                }
                return "";
            }
        },
        {
            title: "操作",
            width: 80,
            render: (_, row: EntryInfo) => (
                <Button type="link" style={{ minWidth: 0, padding: "0px 0px" }}
                    onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        request(unwatch({
                            session_id: userStore.sessionId,
                            project_id: projectStore.curProjectId,
                            target_type: WATCH_TARGET_ENTRY,
                            target_id: row.entry_id,
                        }));
                    }}>取消关注</Button>
            )
        },
    ];

    useEffect(() => {
        if (projectStore.curProjectId != "") {
            loadEntryList();
        }
    }, [curPage]);

    useEffect(() => {
        const unListenFn = listen<NoticeType.AllNotice>("notice", ev => {
            if (ev.payload.ClientNotice?.WatchChangeNotice != undefined && ev.payload.ClientNotice.WatchChangeNotice.targetType == WATCH_TARGET_ENTRY) {
                loadEntryList();
            }
        });

        return () => {
            unListenFn.then((unListen) => unListen());
        };
    }, []);

    return (
        <div>
            <Table rowKey="entry_id" dataSource={entryList}
                columns={columns}
                pagination={{
                    current: curPage + 1,
                    pageSize: PAGE_SIZE,
                    total: totalCount,
                    onChange: page => setCurPage(page - 1),
                    hideOnSinglePage: true,
                }} style={{ minHeight: "200px" }} />
            {showPagesEntryInfo != null && (
                <PagesModal projectInfo={projectStore.curProject!} fileId={showPagesEntryInfo.extra_info.ExtraPagesInfo?.file_id ?? ""}
                    entryId={showPagesEntryInfo.entry_id} entryTitle={showPagesEntryInfo.entry_title}
                    onClose={() => setShowPagesEntryInfo(null)} />
            )}
        </div>
    );
};

const WatchRequirementList = observer(() => {
    const history = useHistory();

    const userStore = useStores('userStore');
    const projectStore = useStores('projectStore');
    const linkAuxStore = useStores('linkAuxStore');

    const requirementStore = useLocalObservable(() => new LocalRequirementStore(projectStore.curProjectId));

    const [totalCount, setTotalCount] = useState(0);
    const [curPage, setCurPage] = useState(0);

    const loadReqList = async () => {
        const res = await request(list_requirement({
            session_id: userStore.sessionId,
            project_id: projectStore.curProjectId,
            filter_by_keyword: false,
            keyword: "",
            filter_by_has_link_issue: false,
            has_link_issue: false,
            filter_by_closed: false,
            closed: false,
            filter_by_tag_id_list: false,
            tag_id_list: [],
            filter_by_watch: true,
            offset: curPage * PAGE_SIZE,
            limit: PAGE_SIZE,
            sort_type: REQ_SORT_UPDATE_TIME,
        }));
        setTotalCount(res.total_count);
        requirementStore.itemList = res.requirement_list;
    };

    const columns: ColumnsType<RequirementInfo> = [
        {
            title: "名称",
            width: 150,
            render: (_, row: RequirementInfo) => (
                <a onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    linkAuxStore.goToLink(new LinkRequirementInfo("", projectStore.curProjectId, row.requirement_id), history);
                }}>{row.base_info.title}</a>
            ),
        },
        {
            title: "状态",
            width: 80,
            render: (_, row: RequirementInfo) => row.closed ? "关闭" : "打开",
        },
        {
            title: "任务数量",
            width: 80,
            dataIndex: "issue_link_count",
        },
        {
            title: "操作",
            width: 80,
            render: (_, row: RequirementInfo) => (
                <Button type="link" style={{ minWidth: 0, padding: "0px 0px" }}
                    onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        request(unwatch({
                            session_id: userStore.sessionId,
                            project_id: projectStore.curProjectId,
                            target_type: WATCH_TARGET_REQUIRE_MENT,
                            target_id: row.requirement_id,
                        }));
                    }}>取消关注</Button>
            ),
        }
    ];

    useEffect(() => {
        if (projectStore.curProjectId != "") {
            loadReqList();
        }
    }, [curPage]);

    useEffect(() => {
        return () => {
            requirementStore.unlisten();
        };
    }, []);

    useEffect(() => {
        const unListenFn = listen<NoticeType.AllNotice>("notice", ev => {
            if (ev.payload.ClientNotice?.WatchChangeNotice != undefined && ev.payload.ClientNotice.WatchChangeNotice.targetType == WATCH_TARGET_REQUIRE_MENT) {
                loadReqList();
            }
        });

        return () => {
            unListenFn.then((unListen) => unListen());
        };
    }, []);

    return (
        <Table rowKey="requirement_id" dataSource={requirementStore.itemList} columns={columns}
            pagination={{
                current: curPage + 1,
                pageSize: PAGE_SIZE,
                total: totalCount,
                onChange: page => setCurPage(page - 1),
                hideOnSinglePage: true,
            }} style={{ minHeight: "200px" }} />
    );
});

const WatchTestCaseList = observer(() => {
    const history = useHistory();

    const userStore = useStores('userStore');
    const projectStore = useStores('projectStore');
    const linkAuxStore = useStores('linkAuxStore');

    const testCaseStore = useLocalObservable(() => new LocalTestcaseStore(projectStore.curProjectId, ""));
    const [totalCount, setTotalCount] = useState(0);
    const [curPage, setCurPage] = useState(0);

    const loadTestCase = async () => {
        const res = await request(list_case_flat({
            session_id: userStore.sessionId,
            project_id: projectStore.curProjectId,
            list_param: {
                filter_by_title: false,
                title: "",
                my_watch: true,
            },
            offset: PAGE_SIZE * curPage,
            limit: PAGE_SIZE,
        }));
        setTotalCount(res.count);
        testCaseStore.itemList = res.case_list.map(item => ({
            id: item.case_id,
            dataType: "case",
            dataValue: item,
        }));
    };

    const columns: ColumnsType<FolderOrCaseInfo> = [
        {
            title: "名称",
            render: (_, row: FolderOrCaseInfo) => (
                <a onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    linkAuxStore.goToLink(new LinkTestCaseInfo("", projectStore.curProjectId, row.id), history);
                }}>{(row.dataValue as CaseInfo).title}</a>
            ),
        },
        {
            title: "测试结果",
            width: 100,
            render: (_, row: FolderOrCaseInfo) => (row.dataValue as CaseInfo).result_count,
        },
        {
            title: "操作",
            width: 80,
            render: (_, row: FolderOrCaseInfo) => (
                <Button type="link" style={{ minWidth: 0, padding: "0px 0px" }}
                    onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        request(unwatch({
                            session_id: userStore.sessionId,
                            project_id: projectStore.curProjectId,
                            target_type: WATCH_TARGET_TEST_CASE,
                            target_id: row.id,
                        }));
                    }}>取消关注</Button>
            ),
        },
    ];

    useEffect(() => {
        return () => {
            testCaseStore.unlisten();
        };
    }, []);

    useEffect(() => {
        if (projectStore.curProjectId != "") {
            loadTestCase();
        }
    }, [curPage]);

    useEffect(() => {
        const unListenFn = listen<NoticeType.AllNotice>("notice", ev => {
            if (ev.payload.ClientNotice?.WatchChangeNotice != undefined && ev.payload.ClientNotice.WatchChangeNotice.targetType == WATCH_TARGET_TEST_CASE) {
                loadTestCase();
            }
        });

        return () => {
            unListenFn.then((unListen) => unListen());
        };
    }, []);

    return (
        <Table rowKey="id" dataSource={testCaseStore.itemList} columns={columns}
            pagination={{
                current: curPage + 1,
                pageSize: PAGE_SIZE,
                total: totalCount,
                onChange: page => setCurPage(page - 1),
                hideOnSinglePage: true,
            }} style={{ minHeight: "200px" }} />
    );
});

const MyEventList = observer(() => {
    const userStore = useStores("userStore");
    const projectStore = useStores('projectStore');
    const memberStore = useStores("memberStore");

    const [eventList, setEventList] = useState<PluginEvent[]>([]);

    const loadEventList = async () => {
        const now = moment().valueOf();
        const res = await request(list_project_event({
            session_id: userStore.sessionId,
            project_id: projectStore.curProjectId,
            filter_by_member_user_id: true,
            member_user_id: userStore.userInfo.userId,
            from_time: now - 3 * 24 * 3600 * 1000,
            to_time: now,
            offset: 0,
            limit: 20,
        }));

        setEventList(res.event_list);
    };

    useEffect(() => {
        if (projectStore.curProjectId != "") {
            loadEventList();
        }
    }, [projectStore.curProjectId, memberStore.myLastEventId]);

    return (
        <>
            {eventList.length == 0 && <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />}
            <div className={style.record}>
                {eventList.reverse().map(event => (
                    <div key={event.event_id} className={style.recordItem}>
                        <EventCom item={event} skipProjectName={true} skipLink={false} showMoreLink={true}>
                            <img
                                className={style.icon}
                                src={
                                    event.event_type > 99 ? EVENT_ICON_LIST[event.event_type]?.icon : EVENT_ICON_LIST[0]?.icon
                                }
                                alt=""
                            />
                            <span className={style.time}>{timeToDateString(event.event_time)}</span>
                        </EventCom>
                    </div>
                ))}
            </div>

        </>
    )
});

const MyWorkInfo = () => {
    const projectStore = useStores("projectStore");
    const appStore = useStores("appStore");

    return (
        <Card title="我的工作" headStyle={{ fontSize: "16px", fontWeight: 700 }}
            bodyStyle={{ height: "calc(100vh - 150px)", overflowY: "scroll", padding: "0px 0px" }} bordered={false}
            extra={
                <Space>
                    <Button type="text" style={{ minWidth: 0, padding: "0px 0px", marginLeft: "10px" }}
                        icon={<CloseOutlined style={{ fontSize: "18px" }} />} title="关闭面板"
                        onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            projectStore.setShowChatAndComment(false, "bulletin");
                        }} />
                </Space>
            }>
            <h1 style={{ width: "100%", backgroundColor: "#eee", fontSize: "20px", fontWeight: 700, padding: "4px 4px" }}>待办任务/缺陷</h1>
            <TodoIssueList />
            {appStore.vendorCfg?.project.show_task_list_entry && projectStore.curProject?.setting.show_task_list_entry && (
                <>
                    <h1 style={{ width: "100%", backgroundColor: "#eee", fontSize: "20px", fontWeight: 700, padding: "4px 4px" }}>关注任务</h1>
                    <WatchIssueList issueType={ISSUE_TYPE_TASK} />
                </>
            )}
            {appStore.vendorCfg?.project.show_bug_list_entry && projectStore.curProject?.setting.show_bug_list_entry && (
                <>
                    <h1 style={{ width: "100%", backgroundColor: "#eee", fontSize: "20px", fontWeight: 700, padding: "4px 4px" }}>关注缺陷</h1>
                    <WatchIssueList issueType={ISSUE_TYPE_BUG} />
                </>
            )}

            <h1 style={{ width: "100%", backgroundColor: "#eee", fontSize: "20px", fontWeight: 700, padding: "4px 4px" }}>关注内容</h1>
            <WatchEntryList />
            {appStore.vendorCfg?.project.show_requirement_list_entry && projectStore.curProject?.setting.show_require_ment_list_entry && (
                <>
                    <h1 style={{ width: "100%", backgroundColor: "#eee", fontSize: "20px", fontWeight: 700, padding: "4px 4px" }}>关注需求</h1>
                    <WatchRequirementList />
                </>
            )}
            {appStore.vendorCfg?.project.show_testcase_list_entry && projectStore.curProject?.setting.show_test_case_list_entry && (
                <>
                    <h1 style={{ width: "100%", backgroundColor: "#eee", fontSize: "20px", fontWeight: 700, padding: "4px 4px" }}>关注测试用例</h1>
                    <WatchTestCaseList />
                </>
            )}
            <h1 style={{ width: "100%", backgroundColor: "#eee", fontSize: "20px", fontWeight: 700, padding: "4px 4px" }}>工作记录</h1>
            <MyEventList />
        </Card>
    );
};

export default observer(MyWorkInfo);