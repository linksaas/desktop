//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only


import React from "react";
import { Badge, Card, Form, Input, Popover, Select, Space, Tabs } from "antd";
import { observer } from 'mobx-react';
import UnreadCommentList from "./UnreadCommentList";
import { useStores } from "@/hooks";
import Button from "@/components/Button";
import BulletinList from "./BulletinList";
import { CloseOutlined, DoubleLeftOutlined, InfoCircleOutlined } from "@ant-design/icons";
import { open as shell_open } from '@tauri-apps/api/shell';
import CodeEditor from '@uiw/react-textarea-code-editor';
import CodeCommentThreadList from "./CodeCommentThreadList";
import ReviewList from "./ReviewList";
import { REVIEW_TYPE_BUG, REVIEW_TYPE_CODE, REVIEW_TYPE_FLOW, REVIEW_TYPE_MONTH, REVIEW_TYPE_PRODUCT, REVIEW_TYPE_QUARTER, REVIEW_TYPE_TEST } from "@/api/project_review";
import ReviewDetail from "./ReviewDetail";
import { useTranslation } from "react-i18next";

const ChatAndCommentPanel = () => {
    const { t } = useTranslation();

    const projectStore = useStores('projectStore');
    const projectModalStore = useStores('projectModalStore');

    return (
        <>
            <Tabs style={{ width: "100%" }} type="card"
                tabBarStyle={{ height: "45px", marginBottom: "0px" }}
                activeKey={projectStore.showChatAndCommentTab}
                onChange={key => projectStore.setShowChatAndComment(true, (key as ("comment" | "bulletin" | "member" | "codecomment" | "mywork" | "review")))}
                items={[
                    {
                        key: "bulletin",
                        label: (
                            <div>
                                <Badge count={(projectStore.curProject?.project_status.bulletin_count ?? 0)} offset={[0, -6]} style={{ padding: '0 3px', height: '16px', lineHeight: '16px' }}>
                                    {t("project.communicate.bulletin")}
                                </Badge>
                            </div>
                        ),
                        children: (
                            <div style={{ height: "calc(100vh - 146px)", overflowY: "hidden" }}>
                                {projectStore.showChatAndCommentTab == "bulletin" && (<BulletinList />)}
                            </div>
                        )
                    },
                    {
                        key: "review",
                        label: t("project.communicate.review"),
                        children: (
                            <Card title={<Space style={{ fontSize: "18px", fontWeight: 700 }} >
                                {projectStore.curReview != null && (
                                    <Button type="link" icon={<DoubleLeftOutlined />} style={{ minWidth: "0px", padding: "0px 0px" }} title="返回列表"
                                        onClick={e => {
                                            e.stopPropagation();
                                            e.preventDefault();
                                            projectStore.curReview = null;
                                        }} />
                                )}
                                {projectStore.curReview?.title ?? t("project.review.title")}
                            </Space>} bordered={false} bodyStyle={{ height: "calc(100vh - 186px)", overflowY: "scroll" }}
                                extra={
                                    <>
                                        {projectStore.curReview == null && (
                                            <Form layout="inline">
                                                <Form.Item>
                                                    <Select value={projectStore.filterReviewByType} onChange={value => projectStore.filterReviewByType = value}
                                                        style={{ width: "100px" }}>
                                                        <Select.Option value={null}>{t("text.all")}</Select.Option>
                                                        <Select.Option value={REVIEW_TYPE_CODE}>{t("project.review.cate.code")}</Select.Option>
                                                        <Select.Option value={REVIEW_TYPE_FLOW}>{t("project.review.cate.flow")}</Select.Option>
                                                        <Select.Option value={REVIEW_TYPE_PRODUCT}>{t("project.review.cate.product")}</Select.Option>
                                                        <Select.Option value={REVIEW_TYPE_BUG}>{t("project.review.cate.bug")}</Select.Option>
                                                        <Select.Option value={REVIEW_TYPE_TEST}>{t("project.review.cate.test")}</Select.Option>
                                                        <Select.Option value={REVIEW_TYPE_MONTH}>{t("project.review.cate.month")}</Select.Option>
                                                        <Select.Option value={REVIEW_TYPE_QUARTER}>{t("project.review.cate.quarter")}</Select.Option>
                                                    </Select>
                                                </Form.Item>
                                                <Form.Item>
                                                    <Input style={{ width: "100px" }} placeholder={t("project.review.filterTitle")} value={projectStore.filterReviewByTitleKeyword} onChange={e => {
                                                        e.stopPropagation();
                                                        e.preventDefault();
                                                        projectStore.filterReviewByTitleKeyword = e.target.value.trim();
                                                    }} allowClear />
                                                </Form.Item>
                                            </Form>
                                        )}
                                    </>
                                }>
                                {projectStore.showChatAndCommentTab == "review" && projectStore.curReview == null && (<ReviewList />)}
                                {projectStore.showChatAndCommentTab == "review" && projectStore.curReview != null && (<ReviewDetail />)}
                            </Card>
                        )
                    },
                    {
                        key: "comment",
                        label: (
                            <div>
                                <Badge count={(projectStore.curProject?.project_status.unread_comment_count ?? 0)} offset={[0, -6]} style={{ padding: '0 3px', height: '16px', lineHeight: '16px' }}>
                                    {t("project.communicate.unreadComment")}
                                </Badge>
                            </div>),
                        children: (
                            <div style={{ height: "calc(100vh - 146px)", overflowY: "scroll" }}>
                                {projectStore.showChatAndCommentTab == "comment" && (<UnreadCommentList />)}
                            </div>
                        ),
                    },
                    {
                        key: "codecomment",
                        label: t("project.communicate.codeComment"),
                        children: (
                            <div style={{ height: "calc(100vh - 146px)", overflowY: "scroll" }}>
                                {projectStore.showChatAndCommentTab == "codecomment" && (<CodeCommentThreadList />)}
                            </div>
                        )
                    },
                ]} tabBarExtraContent={
                    <div style={{ marginRight: "10px" }}>
                        {projectStore.showChatAndCommentTab == "bulletin" && projectStore.isAdmin && !projectStore.isClosed && (
                            <Button onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                projectModalStore.projectId = projectStore.curProjectId;
                                projectModalStore.createBulletin = true;
                            }}>{t("project.bulletin.publish")}</Button>
                        )}
                        {projectStore.showChatAndCommentTab == "codecomment" && (
                            <Popover placement="bottom" trigger="hover" content={
                                <>
                                    <div>安装<a onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        shell_open("https://marketplace.visualstudio.com/items?itemName=linksaas.local-api");
                                    }}>vscode插件</a>后,可进行代码评论。</div>
                                    <p>需要在项目根目录下创建.linksaas.yml，并写入以下内容：</p>
                                    <CodeEditor
                                        value={`project_id: ${projectStore.curProjectId}`}
                                        language="yaml"
                                        readOnly
                                        style={{
                                            fontSize: 14,
                                            backgroundColor: '#f5f5f5',
                                        }}
                                    />
                                </>
                            }>
                                <InfoCircleOutlined style={{ fontSize: "16px" }} />
                            </Popover>
                        )}
                        {projectStore.showChatAndCommentTab == "review" && projectStore.isAdmin && !projectStore.isClosed && projectStore.curReview == null && (
                            <Button onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                projectModalStore.projectId = projectStore.curProjectId;
                                projectModalStore.createReview = true;
                            }}>{t("text.create")}</Button>
                        )}
                        <Button type="text" style={{ minWidth: 0, padding: "0px 0px", marginLeft: "10px" }}
                            icon={<CloseOutlined style={{ fontSize: "18px" }} />} title="关闭面板"
                            onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                projectStore.setShowChatAndComment(false, "bulletin");
                            }} />
                    </div>
                } />
        </>
    );
};

export default observer(ChatAndCommentPanel);