//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { observer } from 'mobx-react';
import { useHistory } from "react-router-dom";
import { useStores } from "@/hooks";
import type { RoleInfo } from "@/api/project_member";
import { request } from "@/utils/request";
import { list_role, set_member_role, remove_member, leave as leave_project } from '@/api/project_member';
import { Button, Card, Modal, Select, Space, Table, message } from "antd";
import type { WebMemberInfo } from "@/stores/member";
import type { ColumnsType } from 'antd/es/table';
import { change_owner } from "@/api/project";
import { CloseOutlined, MinusCircleOutlined, UserAddOutlined } from "@ant-design/icons";
import UserPhoto from "@/components/Portrait/UserPhoto";
import { useTranslation } from "react-i18next";

const MemberList = () => {
    const history = useHistory();

    const { t } = useTranslation();

    const userStore = useStores('userStore');
    const memberStore = useStores('memberStore');
    const projectStore = useStores('projectStore');

    const [roleList, setRoleList] = useState<RoleInfo[]>([]);
    const [removeMemberUserId, setRemoveMemberUserId] = useState("");
    const [ownerMemberUserId, setOwnerMemberUserId] = useState("");

    const loadRoleList = async () => {
        if (projectStore.curProjectId == "") {
            return;
        }
        const res = await request(list_role(userStore.sessionId, projectStore.curProjectId));
        if (res) {
            setRoleList(res.role_list);
        }
    };

    const updateMemberRole = async (memberUserId: string, roleId: string) => {
        await set_member_role(userStore.sessionId, projectStore.curProjectId, roleId, memberUserId);
        memberStore.updateMemberRole(memberUserId, roleId);
        message.info(t("project.member.changeRoleSuccess"));
    };

    const changeOwner = async () => {
        await request(change_owner(userStore.sessionId, projectStore.curProjectId, ownerMemberUserId));

        await projectStore.updateProject(projectStore.curProjectId);
        await memberStore.updateMemberInfo(projectStore.curProjectId, userStore.userInfo.userId);
        setOwnerMemberUserId("");
        message.info(t("project.member.changeOwnerSuccess"));
    };

    const removeMember = async () => {
        await request(remove_member(userStore.sessionId, projectStore.curProjectId, removeMemberUserId));
        await memberStore.loadMemberList(projectStore.curProjectId);
        setRemoveMemberUserId("");
        message.info(t("project.member.removeSuccess"));
    };

    const leaveProject = async () => {
        await request(leave_project(userStore.sessionId, projectStore.curProjectId));
        setRemoveMemberUserId("");
        projectStore.removeProject(projectStore.curProjectId, history);
        message.info(t("project.member.quitSuccess"));
    };

    const columns: ColumnsType<WebMemberInfo> = [
        {
            title: t("project.member.nickName"),
            render: (_, row: WebMemberInfo) => (
                <a onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                    memberStore.showDetailMemberId = row.member.member_user_id;
                }}>
                    <UserPhoto logoUri={row.member.logo_uri} style={{ width: "16px", borderRadius: "10px" }} />
                    &nbsp;&nbsp;{row.member.display_name}
                </a>
            ),
        },
        {
            title: t("project.member.role"),
            width: 120,
            render: (_, row: WebMemberInfo) => (
                <div>
                    {row.member.is_project_owner == true && t("project.member.role.super")}
                    {row.member.is_project_owner == false && (
                        <Select value={row.member.role_id}
                            style={{ width: 100 }}
                            disabled={!projectStore.isAdmin} onChange={value => {
                                if (value == "") {
                                    setOwnerMemberUserId(row.member.member_user_id);
                                } else {
                                    updateMemberRole(row.member.member_user_id, value);
                                }
                            }}>
                            {projectStore.curProject?.owner_user_id == userStore.userInfo.userId && (
                                <Select.Option value="">{t("project.member.role.super")}</Select.Option>
                            )}
                            {roleList.map(item => (
                                <Select.Option value={item.role_id} key={item.role_id}>
                                    {item.basic_info.admin ? t("project.member.role.Admin") : t("project.member.role.Normal")}
                                </Select.Option>
                            ))}
                        </Select>
                    )}
                </div>
            )
        },
        {
            title: t("text.operation"),
            width: 90,
            render: (_, row: WebMemberInfo) => (
                <div>
                    {userStore.userInfo.userId == row.member.member_user_id && userStore.userInfo.userId != (projectStore.curProject?.owner_user_id ?? "") && (
                        <Button type="link"
                            style={{ minWidth: 0, padding: "0px 0px" }} danger
                            onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setRemoveMemberUserId(row.member.member_user_id);
                            }}>{t("project.member.quit")}</Button>
                    )}
                    {userStore.userInfo.userId != row.member.member_user_id && (
                        <Button
                            type="link"
                            style={{ minWidth: 0, padding: "0px 0px" }}
                            disabled={projectStore.isClosed || !projectStore.isAdmin}
                            danger
                            onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                setRemoveMemberUserId(row.member.member_user_id);
                            }}
                        >
                            <MinusCircleOutlined />
                            {t("text.remove")}
                        </Button>
                    )}
                </div>
            ),
        }
    ];

    useEffect(() => {
        loadRoleList();
    }, [projectStore.curProjectId]);

    return (
        <Card title={t("project.member.memberList")} headStyle={{ fontSize: "16px", fontWeight: 700 }}
            bodyStyle={{ height: "calc(100vh - 150px)", overflowY: "scroll", padding: "0px 0px" }} bordered={false}
            extra={
                <Space>
                    {projectStore.isAdmin && projectStore.isClosed == false && (
                        <Button type="primary" icon={<UserAddOutlined />} style={{ borderRadius: "6px" }}
                            onClick={e => {
                                e.stopPropagation();
                                e.preventDefault();
                                memberStore.showInviteMember = true;
                            }}>{t("text.invite")}</Button>
                    )}
                    <Button type="text" style={{ minWidth: 0, padding: "0px 0px", marginLeft: "10px" }}
                        icon={<CloseOutlined style={{ fontSize: "18px" }} />} title={t("text.close")}
                        onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            projectStore.setShowChatAndComment(false, "bulletin");
                        }} />
                </Space>
            }>
            <Table rowKey="member_user_id" dataSource={memberStore.memberList} pagination={false} columns={columns} />
            {ownerMemberUserId != "" && (
                <Modal
                    title={t("project.member.moveSuperAdmin")}
                    open mask={false}
                    onCancel={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setOwnerMemberUserId("");
                    }}
                    onOk={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        changeOwner();
                    }}>
                    {t("project.member.moveSuperAdminMsg", { name: memberStore.getMember(ownerMemberUserId)?.member.display_name ?? "" })}
                </Modal>
            )}
            {removeMemberUserId != "" && (
                <Modal title={userStore.userInfo.userId == removeMemberUserId ? t("project.member.quit") : t("project.member.remove")} open mask={false}
                    okButtonProps={{ danger: true }}
                    okText={userStore.userInfo.userId == removeMemberUserId ? t("text.quit") : t("text.remove")}
                    onCancel={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setRemoveMemberUserId("");
                    }}
                    onOk={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        if (userStore.userInfo.userId == removeMemberUserId) {
                            leaveProject();
                        } else {
                            removeMember();
                        }
                    }}>
                    <div
                        style={{
                            fontSize: '14px',
                            lineHeight: '20px',
                            marginBottom: '20px',
                            color: ' #2C2D2E',
                        }}
                    >
                        {userStore.userInfo.userId == removeMemberUserId && t("project.member.quitMsg")}
                        {userStore.userInfo.userId != removeMemberUserId &&
                            <span>
                                {t("project.member.removeMsg", { name: memberStore.getMember(removeMemberUserId)?.member.display_name ?? "" })}
                            </span>
                        }
                    </div>
                </Modal>
            )}
        </Card>
    );
};

export default observer(MemberList);