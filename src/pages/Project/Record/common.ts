//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import iconApp from '@/assets/allIcon/logo.png';
import taskIcon from '@/assets/allIcon/icon-task.png';
import bugIcon from '@/assets/allIcon/icon-bug.png';
import spritIcon from '@/assets/allIcon/icon-sprit.png';
import docIcon from '@/assets/channel/doc@2x.png';
import reqIcon from '@/assets/allIcon/icon-req.png';
import ideaIcon from '@/assets/allIcon/icon-idea.png';
import apiCollIcon from '@/assets/allIcon/icon-apicoll.png';
import testCaseIcon from '@/assets/allIcon/icon-testcase.png';

import * as API from '@/api/events';

export const EVENT_ICON_LIST = {
  [0]: {
    title: 'linksaas',
    icon: iconApp,
  },
  [API.EVENT_TYPE_PROJECT]: {
    title: 'linksaas',
    icon: iconApp,
  },
  [API.EVENT_TYPE_TASK]: {
    title: 'task',
    icon: taskIcon,
  },
  [API.EVENT_TYPE_BUG]: {
    title: 'bug',
    icon: bugIcon,
  },
  [API.EVENT_TYPE_SPRIT]: {
    title: 'sprit',
    icon: spritIcon,
  },
  [API.EVENT_TYPE_DOC]: {
    title: 'doc',
    icon: docIcon,
  },
  [API.EVENT_TYPE_ENTRY]: {
    title: 'doc',
    icon: docIcon,
  },
  [API.EVENT_TYPE_DISK]: {
    title: 'disk',
    icon: iconApp,
  },
  [API.EVENT_TYPE_REQUIRE_MENT]: {
    title: 'requirement',
    icon: reqIcon,
  },
  [API.EVENT_TYPE_CODE]: {
    title: 'linksaas',
    icon: iconApp,
  },
  [API.EVENT_TYPE_IDEA]: {
    title: 'idea',
    icon: ideaIcon,
  },
  [API.EVENT_TYPE_API_COLLECTION]: {
    title: 'ApiCollection',
    icon: apiCollIcon,
  },
  [API.EVENT_TYPE_CUSTOM_EVENT]: {
    title: 'CustomEvent',
    icon: iconApp,
  },

  [API.EVENT_TYPE_TEST_CASE]: {
    title: "TestCase",
    icon: testCaseIcon,
  }
};
