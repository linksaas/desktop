//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { makeAutoObservable, runInAction } from 'mobx';
import type { ServerInfo } from "@/api/project_server";
import { get_server, gen_access_token } from "@/api/project_server";
import { get_my_perm, type MemberPerm } from "@/api/seaotter/member";
import { get_session } from '@/api/user';
import { request } from '@/utils/request';
import type { MemberInfo as ProjectMemberInfo } from "@/api/project_member";
import { list_member as list_project_member } from "@/api/project_member";
import type { ImageGroupInfo } from "@/api/seaotter/image_group";
import type { DomainInfo } from "@/api/seaotter/config";
import { get_domain } from "@/api/seaotter/config";


export class AppStore {
    constructor() {
        makeAutoObservable(this);
    }

    private _adminUser = false;
    private _projectId = "";
    private _remoteServer: ServerInfo | null = null;
    private _myPerm: MemberPerm | null = null;
    private _projectMemberList: ProjectMemberInfo[] = [];
    private _domainInfo: DomainInfo | null = null;

    get adminUser() {
        return this._adminUser;
    }

    get projectId() {
        return this._projectId;
    }

    get remoteServer() {
        return this._remoteServer;
    }

    get myPerm() {
        return this._myPerm;
    }

    get projectMemberList() {
        return this._projectMemberList;
    }

    get domainInfo() {
        return this._domainInfo;
    }

    async loadMyPerm() {
        if (this._remoteServer == null) {
            return;
        }
        const sessionId = await get_session();
        const accessRes = await request(gen_access_token({
            session_id: sessionId,
            project_id: this._projectId,
            server_id: this._remoteServer.server_id,
        }));
        const permRes = await request(get_my_perm(this._remoteServer.basic_info.addr_list[0], {
            access_token: accessRes.token,
            remote_id: this._remoteServer.server_id,
        }))
        runInAction(() => {
            this._myPerm = permRes.perm;
        });
    }

    async loadProjectMemberList() {
        const sessionId = await get_session();
        const res = await request(list_project_member(sessionId, this._projectId, false, []));
        runInAction(() => {
            this._projectMemberList = res.member_list;
        });
    }

    async loadDomainInfo() {
        if (this._remoteServer == null) {
            return;
        }
        const sessionId = await get_session();
        const accessRes = await request(gen_access_token({
            session_id: sessionId,
            project_id: this._projectId,
            server_id: this._remoteServer.server_id,
        }));

        const res = await request(get_domain(this._remoteServer.basic_info.addr_list[0], {
            access_token: accessRes.token,
            remote_id: this._remoteServer.server_id,
        }));
        runInAction(() => {
            this._domainInfo = res.info;
        });
    }

    async init(projectId: string, remoteId: string, adminUser: boolean) {
        const sessionId = await get_session();
        const res = await request(get_server({
            session_id: sessionId,
            project_id: projectId,
            server_id: remoteId,
        }));
        runInAction(() => {
            this._adminUser = adminUser;
            this._projectId = projectId;
            this._remoteServer = res.server_info;
        });
        await this.loadMyPerm();
        await this.loadProjectMemberList();
        await this.loadDomainInfo();
    }

    private _curImageGroup: ImageGroupInfo | null = null
    private _groupDataVersion = 0;
    private _curImage = ""

    get curImageGroup() {
        return this._curImageGroup
    }

    set curImageGroup(val: ImageGroupInfo | null) {
        runInAction(() => {
            this._curImageGroup = val;
            this._curImage = "";
        });
    }

    get curImage() {
        return this._curImage;
    }

    set curImage(val: string) {
        runInAction(() => {
            this._curImage = val;
        });
    }

    get groupDataVersion() {
        return this._groupDataVersion;
    }

    incRroupDataVersion() {
        runInAction(() => {
            this._groupDataVersion += 1;
        });
    }
}