//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from "react";
import { AppStore } from "./app";

const stores = React.createContext(new AppStore());

export const useSeaOtterStores = () => React.useContext(stores);

export default stores;