//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from "react";
import { ConfigProvider, message } from 'antd';
import zhCN from 'antd/lib/locale/zh_CN';
import { createRoot } from 'react-dom/client';
import 'moment/dist/locale/zh-cn';
import '@/styles/global.less';
import { BrowserRouter } from "react-router-dom";
import { report_error } from "@/api/client_cfg";
import { Provider } from 'mobx-react';
import stores from './store/index';
import SeaOtterApp from "./SeaOtterApp";


const App = () => {
    return (
        <Provider {...stores}>
            <ConfigProvider locale={zhCN}>
                <BrowserRouter>
                    <SeaOtterApp />
                </BrowserRouter>
            </ConfigProvider>
        </Provider>
    );
}

const root = createRoot(document.getElementById('root')!);
root.render(<App />);

window.addEventListener('unhandledrejection', function (event) {
    // 防止默认处理（例如将错误输出到控制台）
    event.preventDefault();
    if (`${event.reason}`.includes("error trying to connect")) {
        return;
    }
    message.error(event?.reason);
    // console.log(event);
    try {
        report_error({
            err_data: `${event?.reason}`,
        });
    } catch (e) {
        console.log(e);
    }
});