//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from "react";
import { observer } from 'mobx-react';
import { Empty, Layout } from "antd";
import ImageGroupList from "./components/ImageGroupList";
import ImageInfoList from "./components/ImageInfoList";
import { useSeaOtterStores } from "./store";
import ImageTagList from "./components/ImageTagList";

const ImageList = observer(() => {
    const appStore = useSeaOtterStores();

    return (
        <>
            {appStore.curImageGroup == null && appStore.curImage == "" && (
                <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="未选中镜像分组" />
            )}
            {appStore.curImageGroup != null && appStore.curImage == "" && (
                <ImageInfoList />
            )}
            {appStore.curImageGroup != null && appStore.curImage != "" && (
                <ImageTagList />
            )}
        </>
    );
});

const ImagePanel = () => {
    return (
        <Layout>
            <Layout.Sider theme="light" style={{borderRight:"1px solid #e4e4e8"}}>
                <ImageGroupList />
            </Layout.Sider>
            <Layout.Content style={{backgroundColor:"white"}}>
                <ImageList />
            </Layout.Content>
        </Layout>
    );
};


export default ImagePanel;