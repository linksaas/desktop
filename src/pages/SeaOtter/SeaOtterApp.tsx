//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { observer } from 'mobx-react';
import { useSeaOtterStores } from "./store";
import { useLocation } from "react-router-dom";
import { appWindow } from "@tauri-apps/api/window";
import { Button, Space, Tabs } from "antd";
import MemberPanel from "./MemberPanel";
import AuthSecretPanel from "./AuthSecretPanel";
import ImagePanel from "./ImagePanel";
import EditWatchModal from "./components/EditWatchModal";
import WatchPanel from "./WatchPanel";

const SeaOtterApp = () => {
    const appStore = useSeaOtterStores();

    const [activeKey, setActiveKey] = useState("image");
    const [watchVersion, setWatchVersion] = useState(0);
    const [showCreateWatchModal, setShowCreateWatchModal] = useState(false);

    const location = useLocation();
    const urlParams = new URLSearchParams(location.search);
    const adminStr = urlParams.get("admin") ?? "false";
    const projectId = urlParams.get("projectId") ?? "";
    const remoteId = urlParams.get("remoteId") ?? "";

    useEffect(() => {
        appStore.init(projectId, remoteId, adminStr.toLowerCase().startsWith("t"))
    }, [adminStr, projectId, remoteId]);

    useEffect(() => {
        appWindow.setAlwaysOnTop(false);
    }, []);

    return (
        <>
            {appStore.remoteServer != null && appStore.myPerm != null && (
                <Tabs activeKey={activeKey} onChange={key => setActiveKey(key)}
                    type="card" tabBarStyle={{ height: "40px" }} style={{ padding: "0px 0px" }}
                    tabBarExtraContent={
                        <Space style={{ marginRight: "20px" }}>
                            {activeKey == "watch" && appStore.myPerm.manage_watch && (
                                <Button type="primary" onClick={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    setShowCreateWatchModal(true);
                                }}>创建配置</Button>
                            )}
                        </Space>
                    }>
                    <Tabs.TabPane tab={<span style={{ fontSize: "16px" }}>镜像数据</span>} key="image">
                        {activeKey == "image" && (
                            <ImagePanel />
                        )}
                    </Tabs.TabPane>
                    {appStore.adminUser && (
                        <Tabs.TabPane tab={<span style={{ fontSize: "16px" }}>成员管理</span>} key="member">
                            {activeKey == "member" && (
                                <div style={{ height: "calc(100vh - 50px)", overflowY: "scroll" }}>
                                    <MemberPanel />
                                </div>
                            )}
                        </Tabs.TabPane>
                    )}
                    {appStore.myPerm.manage_auth_secret && (
                        <Tabs.TabPane tab={<span style={{ fontSize: "16px" }}>密钥管理</span>} key="secret">
                            {activeKey == "secret" && (
                                <div style={{ height: "calc(100vh - 50px)", overflowY: "scroll" }}>
                                    <AuthSecretPanel />
                                </div>
                            )}
                        </Tabs.TabPane>
                    )}
                    {appStore.myPerm.manage_watch && (
                        <Tabs.TabPane tab={<span style={{ fontSize: "16px" }}>变更通知配置</span>} key="watch">
                            {activeKey == "watch" && (
                                <div style={{ height: "calc(100vh - 50px)", overflowY: "scroll" }}>
                                    <WatchPanel dataVersion={watchVersion} />
                                </div>
                            )}
                        </Tabs.TabPane>
                    )}
                </Tabs>
            )}

            {showCreateWatchModal == true && (
                <EditWatchModal onCancel={() => setShowCreateWatchModal(false)} onOk={() => {
                    setShowCreateWatchModal(false);
                    setWatchVersion(watchVersion + 1);
                }} />
            )}
        </>
    )
};


export default observer(SeaOtterApp);