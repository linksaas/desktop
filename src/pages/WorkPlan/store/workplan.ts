//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { makeAutoObservable, runInAction } from 'mobx';
import { type EntryInfo, get as get_entry } from "@/api/project_entry";
import { request } from '@/utils/request';
import { get_session, get_user_id } from '@/api/user';
import { type ProjectInfo, TAG_SCOPRE_ALL, type TagInfo, get_project, list_tag } from "@/api/project";
import { type MemberInfo, list_member } from "@/api/project_member";
import type { IssueInfo } from '@/api/project_issue';
import { type GetCfgResponse, type VendorConfig, get_cfg, get_vendor_config } from "@/api/client_cfg";

export class WorkPlanStore {
    constructor() {
        makeAutoObservable(this);
    }

    private _projectId = "";
    private _myUserId = "";
    private _workPlanId = "";
    private _entry: EntryInfo | null = null;
    private _clientCfg: GetCfgResponse | null = null;
    private _vendorCfg: VendorConfig| null = null;

    private _project: ProjectInfo | null = null;
    private _memberList: MemberInfo[] = [];
    private _tagList: TagInfo[] = [];

    get projectId() {
        return this._projectId;
    }

    set projectId(val: string) {
        runInAction(() => {
            this._projectId = val;
        });
    }

    get myUserId() {
        return this._myUserId;
    }

    get myUser() {
        return this._memberList.find(item => item.member_user_id == this._myUserId);
    }

    get workPlanId() {
        return this._workPlanId;
    }

    set workPlanId(val: string) {
        runInAction(() => {
            this._workPlanId = val;
        });
    }

    get entry() {
        return this._entry;
    }

    get project() {
        return this._project;
    }

    get clientCfg() {
        return this._clientCfg;
    }

    get vendorCfg() {
        return this._vendorCfg;
    }

    get memberList() {
        return this._memberList;
    }

    get tagList() {
        return this._tagList;
    }

    async loadProjectAndUser() {
        const cfgRes = await get_cfg();
        const venderRes = await get_vendor_config();

        const sessionId = await get_session();
        const prjRes = await request(get_project(sessionId, this._projectId));
        const userId = await get_user_id();
        const memberRes = await request(list_member(sessionId, this._projectId, false, []));
        const tagRes = await request(list_tag({
            session_id: sessionId,
            project_id: this._projectId,
            tag_scope_type: TAG_SCOPRE_ALL,
        }));
        runInAction(() => {
            this._clientCfg = cfgRes;
            this._vendorCfg = venderRes;
            this._project = prjRes.info;
            this._myUserId = userId;
            this._memberList = memberRes.member_list;
            this._tagList = tagRes.tag_info_list;
        });
    }

    async loadWorkPlan() {
        const sessionId = await get_session();
        const entryRes = await request(get_entry({
            session_id: sessionId,
            project_id: this._projectId,
            entry_id: this._workPlanId,
        }));
        runInAction(() => {
            this._entry = entryRes.entry;
        });
    }

    getUser(userId: string) {
        return this._memberList.find(item => item.member_user_id == userId);
    }

    //浮层配置
    private _detailIssue: IssueInfo | null = null;

    get detailIssue() {
        return this._detailIssue;
    }

    set detailIssue(val: IssueInfo | null) {
        runInAction(() => {
            this._detailIssue = val;
        });
    }
}