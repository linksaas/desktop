//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from "react";
import { WorkPlanStore } from "./workplan";

const stores = React.createContext(new WorkPlanStore());

export const useWorkPlanStores = () => React.useContext(stores);

export default stores;