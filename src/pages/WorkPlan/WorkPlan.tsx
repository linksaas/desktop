//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import s from "./index.module.less";
import SpritDetail from './SpritDetail';
import { observer } from 'mobx-react';
import { useWorkPlanStores } from './store';
import IssueDetailModal from '../Issue/IssueDetailModal';
import { appWindow } from '@tauri-apps/api/window';
import moment from 'moment';
import { listen } from '@tauri-apps/api/event';
import type * as NoticeType from '@/api/notice_type';


const WorkPlan = () => {
    const store = useWorkPlanStores();

    const urlParams = new URLSearchParams(location.search);
    const projectId = urlParams.get("projectId") ?? "";
    const workPlanId = urlParams.get("workPlanId") ?? "";

    const [issueTab, setIssueTab] = useState<"detail" | "subtask" | "mydep" | "depme" | "event" | "comment" | "vote">("detail");

    useEffect(() => {
        store.projectId = projectId;
        store.workPlanId = workPlanId;
        store.loadProjectAndUser();
        store.loadWorkPlan();
        appWindow.setAlwaysOnTop(true);
        setTimeout(() => {
            appWindow.setAlwaysOnTop(false);
        }, 500);
    }, []);

    useEffect(() => {
        appWindow.setTitle(`工作计划:${store.entry?.entry_title ?? ""}(${moment(store.entry?.extra_info.ExtraSpritInfo?.start_time ?? 0).format("YYYY-MM-DD")}至${moment(store.entry?.extra_info.ExtraSpritInfo?.end_time ?? 0).format("YYYY-MM-DD")})`);
    }, [store.entry]);

    useEffect(() => {
        const unListenFn = listen<NoticeType.AllNotice>("notice", ev => {
          const notice = ev.payload;
          if (notice.EntryNotice?.UpdateEntryNotice != undefined) {
            if (notice.EntryNotice.UpdateEntryNotice.entry_id == store.workPlanId) {
              store.loadWorkPlan()
            }
          } else if (notice.ProjectNotice?.UpdateProjectNotice != undefined && notice.ProjectNotice.UpdateProjectNotice.project_id == store.projectId) {
            store.loadProjectAndUser();
          } else if (notice.ProjectNotice?.UpdateMemberNotice != undefined && notice.ProjectNotice.UpdateMemberNotice.project_id == store.projectId && notice.ProjectNotice.UpdateMemberNotice.member_user_id == store.myUserId) {
            store.loadProjectAndUser();
          }
        });
        return () => {
          unListenFn.then((unListen) => unListen());
        };
      }, []);

    return (
        <div className={s.work_plan_wrap}>
            {store.project != null && store.entry != null && (
                <SpritDetail />
            )}
            {store.project != null && store.detailIssue != null && (
                <IssueDetailModal projectInfo={store.project}
                    tagList={store.tagList}
                    myUserId={store.myUserId} myDisplayName={store.myUser?.display_name ?? ""} myLogoUri={store.myUser?.logo_uri ?? ""}
                    issueType={store.detailIssue.issue_type}
                    issueId={store.detailIssue.issue_id}
                    issueTab={issueTab}
                    onClose={() => store.detailIssue = null}
                    onChange={newTab => setIssueTab(newTab)}
                    onClickIssue={() => {
                        //do nothing
                    }} />
            )}
        </div>
    );
};

export default observer(WorkPlan);