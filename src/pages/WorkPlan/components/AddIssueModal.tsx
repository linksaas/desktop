//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from "react";
import { observer } from 'mobx-react';
import { DatePicker, Divider, Form, Input, Modal, Select, Space, message } from "antd";
import type { BUG_LEVEL, BUG_PRIORITY, ISSUE_TYPE, TASK_PRIORITY } from "@/api/project_issue";
import {
    ISSUE_TYPE_TASK, ISSUE_TYPE_BUG, create as create_issue, TASK_PRIORITY_LOW, BUG_LEVEL_MINOR, BUG_PRIORITY_LOW,
    assign_exec_user, assign_check_user, link_sprit,
    TASK_PRIORITY_MIDDLE,
    TASK_PRIORITY_HIGH,
    BUG_PRIORITY_NORMAL,
    BUG_PRIORITY_HIGH,
    BUG_PRIORITY_URGENT,
    BUG_PRIORITY_IMMEDIATE,
    BUG_LEVEL_MAJOR,
    BUG_LEVEL_CRITICAL,
    BUG_LEVEL_BLOCKER,
    set_start_time,
    set_end_time,
    set_estimate_minutes,
    set_remain_minutes
} from "@/api/project_issue";
import UserPhoto from "@/components/Portrait/UserPhoto";
import { request } from "@/utils/request";
import { useWorkPlanStores } from "../store";
import { get_session } from "@/api/user";
import type { Moment } from "moment";
import moment from "moment";

export interface AddIssueModalProps {
    issueType?: ISSUE_TYPE;
    onClose: () => void;
}

const AddIssueModal = (props: AddIssueModalProps) => {
    const store = useWorkPlanStores();


    const [issueType, setIssueType] = useState<ISSUE_TYPE>(props.issueType ?? ISSUE_TYPE_TASK);
    const [title, setTitle] = useState("");
    const [execUserId, setExecUserId] = useState("");
    const [checkUserId, setCheckUserId] = useState("");
    const [taskPriority, setTaskPriority] = useState<TASK_PRIORITY>(TASK_PRIORITY_LOW);
    const [bugPriority, setBugPriority] = useState<BUG_PRIORITY>(BUG_PRIORITY_LOW);
    const [bugLevel, setBugLevel] = useState<BUG_LEVEL>(BUG_LEVEL_MINOR);

    const [startTime, setStartTime] = useState<Moment | null>(null);
    const [endTime, setEndTime] = useState<Moment | null>(null);
    const [estimateMinute, setEstimateMinute] = useState<number | null>(null);

    const addIssue = async () => {
        const sessionId = await get_session();
        const createRes = await request(create_issue({
            session_id: sessionId,
            project_id: store.projectId,
            issue_type: issueType,
            basic_info: {
                title: title,
                content: JSON.stringify({ type: "doc" }),
                tag_id_list: [],
            },
            extra_info: {
                ExtraTaskInfo: issueType == ISSUE_TYPE_TASK ? {
                    priority: taskPriority,
                } : undefined,
                ExtraBugInfo: issueType == ISSUE_TYPE_TASK ? undefined : {
                    software_version: "",
                    level: bugLevel,
                    priority: bugPriority,
                },
            }
        }));
        if (execUserId != "") {
            await request(assign_exec_user(sessionId, store.projectId, createRes.issue_id, execUserId));
        }
        if (checkUserId != "") {
            await request(assign_check_user(sessionId, store.projectId, createRes.issue_id, checkUserId));
        }
        await request(link_sprit(sessionId, store.projectId, createRes.issue_id, store.workPlanId));
        if (startTime != null) {
            await request(set_start_time(sessionId, store.projectId, createRes.issue_id, startTime.startOf("day").valueOf()));
        }
        if (endTime != null) {
            await request(set_end_time(sessionId, store.projectId, createRes.issue_id, endTime.endOf("day").valueOf()));
        }
        if (estimateMinute != null) {
            await request(set_estimate_minutes(sessionId, store.projectId, createRes.issue_id, estimateMinute));
            await request(set_remain_minutes({
                session_id: sessionId,
                project_id: store.projectId,
                issue_id: createRes.issue_id,
                remain_minutes: estimateMinute,
                has_spend_minutes: false,
                spend_minutes: 0,
            }))
        }
        props.onClose();
        message.info(`增加${issueType == ISSUE_TYPE_TASK ? "任务" : "缺陷"}成功`);
    };

    const checkValid = (): boolean => {
        if (title == "") {
            return false;
        }
        if (startTime != null && endTime != null && endTime.endOf("day").valueOf() < startTime.startOf("day").valueOf()) {
            return false;
        }
        return true;
    };

    return (
        <Modal open title="增加任务/缺陷" mask={false}
            bodyStyle={{ maxHeight: "calc(100vh - 300px)", overflowY: "scroll" }}
            okText="增加" okButtonProps={{ disabled: !checkValid() }}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                props.onClose();
            }}
            onOk={e => {
                e.stopPropagation();
                e.preventDefault();
                addIssue();
            }}>
            <Form labelCol={{ span: 5 }}>
                <Form.Item label="类型">
                    <Select value={issueType} onChange={value => setIssueType(value)} disabled={props.issueType != null}>
                        <Select.Option value={ISSUE_TYPE_TASK}>任务</Select.Option>
                        <Select.Option value={ISSUE_TYPE_BUG}>缺陷</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item label="标题">
                    <Input value={title} onChange={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setTitle(e.target.value.trim());
                    }} />
                </Form.Item>
                <Form.Item label="执行人">
                    <Select value={execUserId} onChange={value => setExecUserId(value)}>
                        <Select.Option value="">未指定</Select.Option>
                        {store.memberList.filter(item => item.member_user_id != checkUserId).map(item => (
                            <Select.Option key={item.member_user_id} value={item.member_user_id}>
                                <Space>
                                    <UserPhoto logoUri={item.logo_uri} style={{ width: "16px", borderRadius: "10px" }} />
                                    {item.display_name}
                                </Space>
                            </Select.Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item label="检查人">
                    <Select value={checkUserId} onChange={value => setCheckUserId(value)}>
                        <Select.Option value="">未指定</Select.Option>
                        {store.memberList.filter(item => item.member_user_id != execUserId).map(item => (
                            <Select.Option key={item.member_user_id} value={item.member_user_id}>
                                <Space>
                                    <UserPhoto logoUri={item.logo_uri} style={{ width: "16px", borderRadius: "10px" }} />
                                    {item.display_name}
                                </Space>
                            </Select.Option>
                        ))}
                    </Select>
                </Form.Item>
                {issueType == ISSUE_TYPE_TASK && (
                    <Form.Item label="优先级">
                        <Select value={taskPriority} onChange={value => setTaskPriority(value)}>
                            <Select.Option value={TASK_PRIORITY_LOW}>低优先级</Select.Option>
                            <Select.Option value={TASK_PRIORITY_MIDDLE}>正常处理</Select.Option>
                            <Select.Option value={TASK_PRIORITY_HIGH}>高度重视</Select.Option>
                        </Select>
                    </Form.Item>
                )}
                {issueType == ISSUE_TYPE_BUG && (
                    <>
                        <Form.Item label="缺陷级别">
                            <Select value={bugLevel} onChange={value => setBugLevel(value)}>
                                <Select.Option value={BUG_LEVEL_MINOR}>提示</Select.Option>
                                <Select.Option value={BUG_LEVEL_MAJOR}>一般</Select.Option>
                                <Select.Option value={BUG_LEVEL_CRITICAL}>严重</Select.Option>
                                <Select.Option value={BUG_LEVEL_BLOCKER}>致命</Select.Option>
                            </Select>
                        </Form.Item>
                        <Form.Item label="优先级">
                            <Select value={bugPriority} onChange={value => setBugPriority(value)}>
                                <Select.Option value={BUG_PRIORITY_LOW}>低优先级</Select.Option>
                                <Select.Option value={BUG_PRIORITY_NORMAL}>正常处理</Select.Option>
                                <Select.Option value={BUG_PRIORITY_HIGH}>高度重视</Select.Option>
                                <Select.Option value={BUG_PRIORITY_URGENT}>急需解决</Select.Option>
                                <Select.Option value={BUG_PRIORITY_IMMEDIATE}>马上解决</Select.Option>
                            </Select>
                        </Form.Item>

                    </>
                )}
                <Divider orientation="left">进度预估(可选)</Divider>
                <Form.Item label="预估开始时间">
                    <DatePicker popupStyle={{ zIndex: 10000 }} allowClear onChange={value => setStartTime(value)}
                        value={startTime} defaultPickerValue={moment(store.entry?.extra_info.ExtraSpritInfo?.start_time ?? 0)}
                        disabledDate={value => {
                            if (value.valueOf() < (store.entry?.extra_info.ExtraSpritInfo?.start_time ?? 0)) {
                                return true;
                            } else if (value.valueOf() > (store.entry?.extra_info.ExtraSpritInfo?.end_time ?? 0)) {
                                return true;
                            }
                            return false;
                        }} />
                </Form.Item>
                <Form.Item label="预估结束时间" help={
                    <>
                        {startTime != null && endTime != null && endTime.endOf("day").valueOf() < startTime.startOf("day").valueOf() && (
                            <span style={{ color: "red" }}>结束时间不能小于开始时间</span>
                        )}
                    </>
                }>
                    <DatePicker popupStyle={{ zIndex: 10000 }} allowClear onChange={value => setEndTime(value)}
                        value={endTime} defaultPickerValue={moment(store.entry?.extra_info.ExtraSpritInfo?.start_time ?? 0)}
                        disabledDate={value => {
                            if (value.valueOf() < (store.entry?.extra_info.ExtraSpritInfo?.start_time ?? 0)) {
                                return true;
                            } else if (value.valueOf() > (store.entry?.extra_info.ExtraSpritInfo?.end_time ?? 0)) {
                                return true;
                            }
                            return false;
                        }} />
                </Form.Item>
                <Form.Item label="预估工时">
                    <Select value={estimateMinute} allowClear onChange={value => setEstimateMinute(value)}>
                        {[0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12, 12.5, 13, 13.5, 14, 14.5, 15, 15.5, 16].map(hour => (
                            <Select key={hour} value={hour * 60}>{hour}小时</Select>
                        ))}
                    </Select>
                </Form.Item>
            </Form>
        </Modal>
    );
};
export default observer(AddIssueModal);
