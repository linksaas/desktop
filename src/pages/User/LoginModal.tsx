//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { Button, Form, Input, Modal, Space, Tabs } from "antd";
import React, { useEffect, useState } from "react";
import { observer } from 'mobx-react';
import { useStores } from "@/hooks";
import { get_conn_server_addr } from "@/api/main";
import { USER_TYPE_INTERNAL } from "@/api/user";
import iconAtomgit from '@/assets/allIcon/icon-atomgit.png';
import iconGitee from '@/assets/allIcon/icon-gitee.png';
import iconGitCode from '@/assets/allIcon/icon-gitcode.png';
import { ExportOutlined } from "@ant-design/icons";
import { WebviewWindow } from '@tauri-apps/api/window';
import { sleep } from "@/utils/time";
import { useTranslation } from "react-i18next";


const LoginModal = () => {
    const { t } = useTranslation();

    const appStore = useStores('appStore');
    const userStore = useStores('userStore');
    const localRepoStore = useStores("localRepoStore");

    const [activeKey, setActiveKey] = useState((appStore.vendorCfg?.account.external_account == true) ? "extern" : "password");

    const [userName, setUserName] = useState("");
    const [password, setPassword] = useState("");
    const [connAddr, setConnAddr] = useState("");

    const openAtomLoginPage = async () => {
        const label = "atomGitLogin";
        const win = await WebviewWindow.getByLabel(label);
        if (win != null) {
            await win.close();
        }
        await sleep(200);
        new WebviewWindow(label, {
            url: `https://atomgit.com/login/oauth/authorize?client_id=${appStore.clientCfg?.atom_git_client_id ?? ""}&state=state_test`,
            title: t("user.authorize.atomGit"),
            alwaysOnTop: true,
            width: 1200,
            height: 900,
        });
    }

    const openGiteeLoginPage = async () => {
        const label = "giteeLogin";
        const win = await WebviewWindow.getByLabel(label);
        if (win != null) {
            await win.close();
        }
        await sleep(200);
        new WebviewWindow(label, {
            url: `https://gitee.com/oauth/authorize?client_id=${appStore.clientCfg?.gitee_client_id ?? ""}&redirect_uri=${encodeURIComponent("https://www.linksaas.pro/callback/gitee")}&response_type=code`,
            title: t("user.authorize.gitee"),
            alwaysOnTop: true,
            width: 1200,
            height: 760,
        });
    };

    const openGitCodeLoginPage = async () => {
        const label = "gitCodeLogin";
        const win = await WebviewWindow.getByLabel(label);
        if (win != null) {
            await win.close();
        }
        await sleep(200);
        new WebviewWindow(label, {
            url: `https://gitcode.com/oauth/authorize?client_id=${appStore.clientCfg?.git_code_client_id ?? ""}&redirect_uri=${encodeURIComponent("https://www.linksaas.pro/callback/gitcode")}&response_type=code&scope=${encodeURIComponent("all_user,all_key,all_groups,all_projects,all_pr,all_issue,all_note,all_hook,all_repository")}&state=linksaas`,
            title: t("user.authorize.gitCode"),
            alwaysOnTop: true,
            width: 800,
            height: 700,
        });
    };

    useEffect(() => {
        get_conn_server_addr().then(addr => {
            const tmpAddr = addr.replace("http://", "");
            setConnAddr(tmpAddr);
            const tmpUserName = localStorage.getItem(`${tmpAddr}:username`) ?? "";
            setUserName(tmpUserName);
        })
    }, []);


    return (
        <Modal title={
            <div>
                <span style={{ fontSize: "16px", fontWeight: 600 }}>{t("user.login")}</span>
            </div>
        } open footer={null} mask={false}
            bodyStyle={{ padding: "0px 10px" }}
            onCancel={e => {
                e.stopPropagation();
                e.preventDefault();
                userStore.showUserLogin = false;
            }}>
            <Tabs tabPosition="top" type="card"
                activeKey={activeKey} onChange={value => setActiveKey(value)}>
                {appStore.vendorCfg?.account.external_account && (
                    <Tabs.TabPane tab={t("text.externalAccount")} key="extern" style={{ padding: "20px 10px" }}>
                        {appStore.clientCfg?.atom_git_client_id != "" && appStore.vendorCfg?.account.external_atomgit && (
                            <Space style={{ marginBottom: "20px" }}>
                                <div style={{ width: "150px" }}>
                                    <img src={iconAtomgit} style={{ width: "20px", marginRight: "10px" }} />
                                    AtomGit
                                </div>
                                <div style={{ width: "200px", fontWeight: 800, fontSize: "16px" }}>
                                    <a onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        openAtomLoginPage();
                                    }}>{t("user.authorizedLogin")}&nbsp;<ExportOutlined /></a>
                                </div>
                                <div><a href="https://passport.atomgit.com/login" target="_blank" rel="noreferrer">{t("user.registerAccount")}&nbsp;<ExportOutlined /></a></div>
                            </Space>
                        )}
                        {appStore.clientCfg?.git_code_client_id != "" && appStore.vendorCfg?.account.external_gitcode && (
                            <Space style={{ marginBottom: "20px" }}>
                                <div style={{ width: "150px" }}>
                                    <img src={iconGitCode} style={{ width: "20px", marginRight: "10px" }} />
                                    GitCode
                                </div>
                                <div style={{ width: "200px", fontWeight: 800, fontSize: "16px" }}>
                                    <a onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        openGitCodeLoginPage();
                                    }}>{t("user.authorizedLogin")}&nbsp;<ExportOutlined /></a>
                                </div>
                                <div><a href="https://gitcode.com/" target="_blank" rel="noreferrer">{t("user.registerAccount")}&nbsp;<ExportOutlined /></a></div>
                            </Space>
                        )}
                        {appStore.clientCfg?.gitee_client_id != "" && appStore.vendorCfg?.account.external_gitee && (
                            <Space style={{ marginBottom: "20px" }}>
                                <div style={{ width: "150px" }}>
                                    <img src={iconGitee} style={{ width: "20px", marginRight: "10px" }} />
                                    Gitee
                                </div>
                                <div style={{ width: "200px", fontWeight: 800, fontSize: "16px" }}>
                                    <a onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        openGiteeLoginPage();
                                    }}>{t("user.authorizedLogin")}&nbsp;<ExportOutlined /></a>
                                </div>
                                <div><a href="https://gitee.com/signup" target="_blank" rel="noreferrer">{t("user.registerAccount")}&nbsp;<ExportOutlined /></a></div>
                            </Space>
                        )}
                    </Tabs.TabPane>
                )}
                {appStore.vendorCfg?.account.inner_account && (
                    <Tabs.TabPane tab={t("text.internalAccount")} key="password">
                        <Form labelCol={{ span: 3 }}>
                            <Form.Item label={t("text.username")}>
                                <Input value={userName} onChange={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    setUserName(e.target.value.trim());
                                }} />
                            </Form.Item>
                            <Form.Item label={t("text.password")}>
                                <Input.Password value={password} onChange={e => {
                                    e.stopPropagation();
                                    e.preventDefault();
                                    setPassword(e.target.value.trim());
                                }} onKeyDown={e => {
                                    if (e.key == "Enter") {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        if (userName == "" || password == "") {
                                            return;
                                        }
                                        userStore.callLogin(userName, password, USER_TYPE_INTERNAL).then(() => {
                                            localStorage.setItem(`${connAddr}:username`, userName);
                                            userStore.showUserLogin = false;
                                            if (appStore.vendorCfg?.ability.enable_work_bench) {
                                                localRepoStore.init();
                                            }
                                        });
                                    }
                                }} />
                            </Form.Item>
                            <div style={{ display: "flex", justifyContent: "flex-end", borderTop: "1px solid #e4e4e8", paddingTop: "10px", marginTop: "10px", marginBottom: "10px" }}>
                                <Space size="large">
                                    <Button onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        userStore.showUserLogin = false;
                                    }}>{t("text.cancel")}</Button>
                                    <Button type="primary" disabled={userName == "" || password == ""} onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        userStore.callLogin(userName, password, USER_TYPE_INTERNAL).then(() => {
                                            localStorage.setItem(`${connAddr}:username`, userName);
                                            userStore.showUserLogin = false;
                                            if (appStore.vendorCfg?.ability.enable_work_bench) {
                                                localRepoStore.init();
                                            }
                                        });
                                    }}>{t("user.login")}</Button>
                                </Space>
                            </div>
                        </Form>
                    </Tabs.TabPane>
                )}
            </Tabs>
        </Modal>
    );
};

export default observer(LoginModal);