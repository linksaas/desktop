//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { Form, DatePicker, Select, Tabs } from "antd";
import { get_head_info, HeadInfo, list_repo_branch } from "@/api/local_repo";
import moment, { type Moment } from "moment";
import { useLocation } from "react-router-dom";
import CommitAnalyse from "./components/CommitAnalyse";
import CountAnalyse from "./components/CountAnalyse";


const GitAnalyse = () => {
    const location = useLocation();
    const urlParams = new URLSearchParams(location.search);
    const repoPath = urlParams.get("path") ?? "";

    const [branchNameList, setBranchNameList] = useState<string[]>([]);
    const [curBranchName, setCurBranchName] = useState("");

    const [fromTime, setFromTime] = useState<Moment>(moment().subtract(7, "days").startOf("day"));
    const [toTime, setToTime] = useState<Moment>(moment().endOf("day"));

    const [activeKey, setActiveKey] = useState<"commit" | "count">("commit");
    const [headInfo, setHeadInfo] = useState<HeadInfo | null>(null);

    const loadRepoInfo = async () => {
        const tmpBranchList = await list_repo_branch(repoPath, false);
        setBranchNameList(tmpBranchList.map(item => item.name));
        const tmpHeadInfo = await get_head_info(repoPath);
        if (tmpHeadInfo.detached == false) {
            setCurBranchName(tmpHeadInfo.branch_name);
        } else if (tmpBranchList.length > 0) {
            setCurBranchName(tmpBranchList[0].name);
        }
        setHeadInfo(tmpHeadInfo);
    };

    useEffect(() => {
        loadRepoInfo();
    }, []);

    return (
        <Tabs type="card" tabBarStyle={{ marginLeft: "10px" }}
            activeKey={activeKey} onChange={value => setActiveKey(value as "commit" | "count")}
            tabBarExtraContent={
                <>
                    {activeKey == "commit" && (
                        <Form layout="inline">
                            <Form.Item label="分支列表">
                                <Select style={{ width: "150px" }} value={curBranchName} onChange={value => setCurBranchName(value)}>
                                    {branchNameList.map(name => (
                                        <Select.Option key={name} value={name}>{name}</Select.Option>
                                    ))}
                                </Select>
                            </Form.Item>
                            <Form.Item label="时间区间">
                                <DatePicker.RangePicker value={[fromTime, toTime]}
                                    popupStyle={{ zIndex: 10000 }}
                                    onChange={values => {
                                        if (values?.length == 2) {
                                            setFromTime(values[0]!.startOf("day"));
                                            setToTime(values[1]!.endOf("day"));
                                        }
                                    }} />
                            </Form.Item>
                        </Form>
                    )}
                    {activeKey == "count" && headInfo != null && (
                        <div style={{ marginRight: "20px" }}>
                            统计当前工作目录代码({repoPath})&nbsp;&nbsp;当前分支({headInfo.detached ? headInfo.tag_name : headInfo.branch_name})
                        </div>
                    )}
                </>
            }>
            <Tabs.TabPane tab={<span style={{ fontSize: "20px", fontWeight: 700 }}>变更分析</span>} key="commit">
                {activeKey == "commit" && (
                    <CommitAnalyse repoPath={repoPath} curBranchName={curBranchName} fromTime={fromTime} toTime={toTime} />
                )}
            </Tabs.TabPane>
            <Tabs.TabPane tab={<span style={{ fontSize: "20px", fontWeight: 700 }}>代码统计</span>} key="count">
                {activeKey == "count" && (
                    <CountAnalyse repoPath={repoPath} />
                )}
            </Tabs.TabPane>
        </Tabs>
    );
};

export default GitAnalyse;