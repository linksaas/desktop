//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import moment, { type Moment } from "moment";
import type { LocalRepoAnalyseInfo } from "@/api/local_repo";
import { analyse,} from "@/api/local_repo";
import { Descriptions, Spin } from "antd";
import { useSize } from "ahooks";
import { BarChart, Bar, XAxis, YAxis, Tooltip, Legend } from 'recharts';

export interface CommitAnalyseProps {
    repoPath: string;
    curBranchName: string;
    fromTime: Moment;
    toTime: Moment;
}

const CommitAnalyse = (props:CommitAnalyseProps) => {
    const winSize = useSize(window.document.body);

    const [analyseInfo, setAnalyseInfo] = useState<LocalRepoAnalyseInfo | null>(null);

    const calcAnalyseInfo = async () => {
        if (props.curBranchName == "") {
            return;
        }
        setAnalyseInfo(null);
        const res = await analyse(props.repoPath, props.curBranchName, props.fromTime.valueOf(), props.toTime.valueOf());
        setAnalyseInfo(res);
    };


    useEffect(() => {
        calcAnalyseInfo();
    }, [props.fromTime, props.toTime, props.curBranchName]);

    return (
<div style={{ height: "calc(100vh - 60px)", overflowY: "scroll", padding: "10px 10px", overflowX: "hidden" }}>
                {analyseInfo == null && (
                    <Spin tip="统计中..." style={{ paddingLeft: "270px" }} />
                )}
                {analyseInfo != null && (
                    <>
                        <Descriptions title={`总体统计(提交${analyseInfo.global_stat.commit_count}次)`} column={2} bordered={true}>
                            <Descriptions.Item label="累计新增">{analyseInfo.global_stat.total_add_count}行</Descriptions.Item>
                            <Descriptions.Item label="累计删除">{analyseInfo.global_stat.total_del_count}行</Descriptions.Item>
                            <Descriptions.Item label="有效新增">{analyseInfo.effect_add_count}行</Descriptions.Item>
                            <Descriptions.Item label="有效删除">{analyseInfo.effect_del_count}行</Descriptions.Item>
                            {analyseInfo.global_stat.min_commit.commit_id != "" && (
                                <>
                                    <Descriptions.Item label="最小提交" span={2}>{analyseInfo.global_stat.min_commit.summary}</Descriptions.Item>
                                    <Descriptions.Item label="最小提交新增">{analyseInfo.global_stat.min_commit.add_count}</Descriptions.Item>
                                    <Descriptions.Item label="最小提交删除">{analyseInfo.global_stat.min_commit.del_count}</Descriptions.Item>
                                </>
                            )}
                            {analyseInfo.global_stat.max_commit.commit_id != "" && (
                                <>
                                    <Descriptions.Item label="最大提交" span={2}>{analyseInfo.global_stat.max_commit.summary}</Descriptions.Item>
                                    <Descriptions.Item label="最大提交新增">{analyseInfo.global_stat.max_commit.add_count}</Descriptions.Item>
                                    <Descriptions.Item label="最大提交删除">{analyseInfo.global_stat.max_commit.del_count}</Descriptions.Item>
                                </>
                            )}
                            {analyseInfo.last_time > 0 && (
                                <Descriptions.Item label="最后提交时间" span={2}>{moment(analyseInfo.last_time * 1000).format("YYYY-MM-DD HH:mm:ss")}</Descriptions.Item>
                            )}
                        </Descriptions>
                        {analyseInfo.commiter_stat_list.map(item => (
                            <div key={item.commiter}>
                                <Descriptions title={`${item.commiter} 相关统计(提交${item.stat.commit_count}次)`} column={2} bordered={true}
                                    style={{ marginTop: "10px" }}>
                                    <Descriptions.Item label="累计新增">{item.stat.total_add_count}行</Descriptions.Item>
                                    <Descriptions.Item label="累计删除">{item.stat.total_del_count}行</Descriptions.Item>
                                    {item.stat.min_commit.commit_id != "" && (
                                        <>
                                            <Descriptions.Item label="最小提交" span={2}>{item.stat.min_commit.summary}</Descriptions.Item>
                                            <Descriptions.Item label="最小提交新增">{item.stat.min_commit.add_count}</Descriptions.Item>
                                            <Descriptions.Item label="最小提交删除">{item.stat.min_commit.del_count}</Descriptions.Item>
                                        </>
                                    )}
                                    {item.stat.max_commit.commit_id != "" && (
                                        <>
                                            <Descriptions.Item label="最大提交" span={2}>{item.stat.max_commit.summary}</Descriptions.Item>
                                            <Descriptions.Item label="最大提交新增">{item.stat.max_commit.add_count}</Descriptions.Item>
                                            <Descriptions.Item label="最大提交删除">{item.stat.max_commit.del_count}</Descriptions.Item>
                                        </>
                                    )}
                                </Descriptions>
                                {winSize !== undefined && (
                                    <BarChart height={300} width={winSize.width - 40} data={item.day_stat_list}>
                                        <XAxis dataKey="day_str" />
                                        <YAxis />
                                        <Tooltip />
                                        <Legend />
                                        <Bar dataKey="add_count" fill="green" />
                                        <Bar dataKey="del_count" fill="red" />
                                        <Bar dataKey="commit_count" fill="black" />
                                    </BarChart>
                                )}
                            </div>
                        ))}
                    </>
                )}
            </div>
    );
};

export default CommitAnalyse;