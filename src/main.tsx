//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import { createRoot } from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import { observer, Provider } from 'mobx-react';
import '@/styles/global.less';
import { renderRoutes } from 'react-router-config';
import routes from './routes';
import stores from '@/stores';
import zhCN from 'antd/lib/locale/zh_CN';
import enUs from 'antd/lib/locale/en_US';

import { message, ConfigProvider, Spin, Alert, Space, Button } from 'antd';
import { ErrorBoundary } from '@/components/ErrorBoundary';
import 'moment/dist/locale/zh-cn';
import 'remirror/styles/all.css';
import '@/components/Editor/editor.less';
import { list_server, report_error } from './api/client_cfg';
import { conn_grpc_server, get_conn_server_addr } from './api/main';
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import { MainResources } from './locales/MainResources';

const InnerApp = observer(() => {
  const [hasConn, setHasConn] = useState(false);

  const getDefaultServer = async () => {
    const res = await list_server(false);
    const connServAddr = await get_conn_server_addr();
    for (const server of res.server_list) {
      if (!server.addr.includes(":")) {
        server.addr += ":5000";
      }
      if (server.addr.replace("http://", "") == connServAddr.replace("http://", "").replace("/", "")) {
        return server.addr;
      }
    }
    for (const item of res.server_list) {
      if (item.default_server) {
        return item.addr;
      }
    };
    return "";
  };

  const connDefaultServer = async () => {
    const addr = await getDefaultServer();
    if (addr == "") {
      message.error("服务器地址为空");
      return false;
    }
    const res = await conn_grpc_server(addr);
    if (res == false) {
      message.error("连接服务器失败");
      return false;
    }
    return true;
  };

  const init = async () => {
    let lang = localStorage.getItem("lang") ?? navigator.language;
    if (lang.startsWith("zh")) {
      lang = "zh";
    } else {
      lang = "en";
    }

    i18n.use(LanguageDetector).use(initReactI18next).init({
      resources: MainResources,
      fallbackLng: "en",
      lng: lang,
    });

    stores.appStore.lang = lang;
    await stores.appStore.loadVendorCfg();

    const success = await connDefaultServer();
    if (success) {
      stores.appStore.loadClientCfg();
      if (stores.appStore.vendorCfg?.ability.enable_work_bench) {
        stores.localRepoStore.init();
      }
    }
    if (success && stores.userStore.sessionId !== '') {
      stores.projectStore.initLoadProjectList();
      stores.orgStore.initLoadOrgList().then(() => {
        stores.orgStore.setCurOrgId("");
      });
      if (stores.appStore.vendorCfg?.ability.enable_user_mail) {
        stores.userStore.loadGitNoticeList();
        stores.userStore.loadUserMailStatus();
      }
    }
    setHasConn(true);
  };

  useEffect(() => {
    init();

    const unListenList = stores.noticeStore.initListen();

    return () => {
      unListenList.then(unListens => {
        for (const unListen of unListens) {
          unListen();
        }
      })
    };
  }, []);

  useEffect(() => {
    if (hasConn && stores.appStore.clientCfg == undefined) {
      let count = 1;
      const t = setInterval(() => {
        count += 1;
        if (count > 3) {
          clearInterval(t);
        } else {
          stores.appStore.loadClientCfg().then(() => {
            clearInterval(t);
          });
        }
      }, 3000);
    }
  }, [hasConn, stores.appStore.clientCfg]);

  return (
    <ConfigProvider locale={stores.appStore.lang == "zh" ? zhCN : enUs}>
      {hasConn && stores.appStore.clientCfg != undefined && (
        <ErrorBoundary>
          {renderRoutes(routes)}
        </ErrorBoundary>
      )}
      {hasConn && stores.appStore.clientCfg == undefined && (
        <div style={{ width: "100vw", height: "100vh", backgroundColor: "white", border: "1px solid #e4e4e8", paddingTop: "40px" }} data-tauri-drag-region>
          <Spin tip="从网络加载配置..." size="large" style={{ marginTop: "100px" }}>
            <div />
          </Spin>
          <Alert
            message="正在从网络加载配置,如果无法加载配置请检查网络状态。"
            style={{ marginTop: "140px" }}
            description={
              <Space>
                <Button type="default" onClick={e => {
                  e.stopPropagation();
                  e.preventDefault();
                  stores.appStore.loadClientCfg().catch(() => message.error("无法加载配置,请检查网络状态。"));
                }}>重新加载配置</Button>
                <Button type="primary" onClick={e => {
                  e.stopPropagation();
                  e.preventDefault();
                  stores.appStore.useDefaultClientCfg();
                }}>使用默认配置</Button>
              </Space>
            }
            type="info"
          />
        </div>
      )}
    </ConfigProvider>
  );
});

const App = () => {
  return (
    <Provider stores={stores}>
      <BrowserRouter>
        <InnerApp />
      </BrowserRouter>
    </Provider >
  );
};

const root = createRoot(document.getElementById('root')!);
root.render(<App />);

window.addEventListener('unhandledrejection', function (event) {
  // 防止默认处理（例如将错误输出到控制台）
  event.preventDefault();
  if (`${event.reason}`.includes("error trying to connect")) {
    return;
  }
  message.error(event?.reason);
  // console.log(event);
  try {
    report_error({
      err_data: `${event?.reason}`,
    });
  } catch (e) {
    console.log(e);
  }
});
