//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import type { PluginEvent } from '@/api/events';
import * as API from '@/api/event_type';
import { useEffect, useState } from 'react';
import React from 'react';
import { Modal } from 'antd';
import type { LinkInfo, LinkImageInfo, LinkExterneInfo } from '@/stores/linkAux';
import { LINK_TARGET_TYPE, LinkEventlInfo } from '@/stores/linkAux';
import CodeEditor from '@uiw/react-textarea-code-editor';
import AsyncImage from '../AsyncImage';
import { platform } from '@tauri-apps/api/os';
import type * as NoticeType from '@/api/notice_type';
import { WebviewWindow } from '@tauri-apps/api/window';

type EventComProps = {
  item: PluginEvent;
  skipProjectName: boolean;
  skipLink: boolean;
  showMoreLink: boolean;
  showSource?: boolean;
  className?: string;
  children?: React.ReactNode;
  onLinkClick?: () => void;
};

const EventCom: React.FC<EventComProps> = ({
  item,
  skipProjectName,
  skipLink,
  showMoreLink,
  showSource,
  className,
  children,
  onLinkClick,
}) => {
  const contentList = API.get_simple_content(item, skipProjectName);

  const [showSourceModal, setShowSourceModal] = useState(false);
  const [isOsWindows, setIsOsWindows] = useState<boolean | null>(null);

  const getTypeLink = (v: LinkInfo, index: number) => {
    if (isOsWindows == null) {
      return "";
    }
    if (skipLink) {
      return <span key={index}>{v.linkContent + ' '}</span>;
    }
    switch (v.linkTargeType) {
      case LINK_TARGET_TYPE.LINK_TARGET_IMAGE: // 查看图片
        const imgLink = v as LinkImageInfo;
        let imgUrl = imgLink.imgUrl || '';
        let thumbImgUrl = imgLink.thumbImgUrl || '';
        if (isOsWindows) {
          imgUrl = imgUrl.replace(/^fs:\/\/localhost\//, 'https://fs.localhost/') || '';
          thumbImgUrl = thumbImgUrl.replace(/^fs:\/\/localhost\//, 'https://fs.localhost/') || '';
        }
        return (
          <div key={index}>
            <AsyncImage
              preview={{
                src: imgUrl,
                mask: false,
                wrapStyle: {
                  margin: "60px 60px 60px 60px",
                },
              }}
              src={thumbImgUrl}
              useRawImg={false}
            />
          </div>
        );
      case LINK_TARGET_TYPE.LINK_TARGET_EXTERNE: // 外部链接
        const extLink = v as LinkExterneInfo;
        return (
          <a key={index} href={extLink.destUrl} target="_blank" rel="noreferrer">
            {v.linkContent + ' '}
          </a>
        );
      default:
        return (
          <a
            key={index}
            onClick={(e) => {
              e.preventDefault();
              e.stopPropagation();
              const notice: NoticeType.AllNotice = {
                ClientNotice: {
                  OpenLinkInfoNotice: {
                    link_info: v,
                  },
                },
              };
              const mainWindow = WebviewWindow.getByLabel("main");
              mainWindow?.emit("notice", notice);
            }}
          >
            {v.linkContent + ' '}
          </a>
        );
    }
  };

  useEffect(() => {
    platform().then((platName: string) => {
      if (platName.includes("win32")) {
        setIsOsWindows(true);
      } else {
        setIsOsWindows(false);
      }
    });
  }, []);

  return (
    <div className={className}>
      {children}
      {isOsWindows != null && contentList.map((v, index) => {
        if (v.linkTargeType != LINK_TARGET_TYPE.LINK_TARGET_NONE) {
          return getTypeLink(v, index);
        }
        return <span key={index}>{v.linkContent + ' '}</span>;
      })}
      {showMoreLink && (
        <a
          onClick={(e) => {
            e.preventDefault();
            e.stopPropagation();
            const notice: NoticeType.AllNotice = {
              ClientNotice: {
                OpenLinkInfoNotice: {
                  link_info: new LinkEventlInfo('', item.project_id, item.event_id, item.user_id, item.event_time),
                },
              },
            };
            const mainWindow = WebviewWindow.getByLabel("main");
            mainWindow?.emit("notice", notice);
            onLinkClick?.();
          }}
        >
          查看更多&nbsp;
        </a>
      )}
      {showSource == true && (
        <a onClick={e => {
          e.preventDefault();
          e.stopPropagation();
          setShowSourceModal(true);
        }}>查看源信息&nbsp;</a>
      )}
      {showSourceModal == true && (
        <Modal title="事件源信息"
          open mask={false}
          footer={null}
          onCancel={e => {
            e.stopPropagation();
            e.preventDefault();
            setShowSourceModal(false);
          }}>
          <div style={{ maxHeight: "calc(100vh - 300px)", overflowY: "scroll", paddingRight: "10px" }}>
            <CodeEditor
              value={JSON.stringify(item, null, 2)}
              language="json"
              disabled
              style={{
                fontSize: 14,
                backgroundColor: '#f5f5f5',
              }}
            />
          </div>
        </Modal>
      )}
    </div>
  );
};

export default EventCom;
