//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from "react";
import cls from './index.module.less';
import { observer } from 'mobx-react';
import { useStores } from "@/hooks";

import ProjectItem from "./ProjectItem";
import { PlusOutlined, ProjectOutlined, UnorderedListOutlined } from "@ant-design/icons";
import { useHistory } from "react-router-dom";
import { APP_PROJECT_MANAGER_PATH } from "@/utils/constant";
import { Button, Space } from "antd";
import { useTranslation } from "react-i18next";


const ProjectList = () => {
    const history = useHistory();

    const { t } = useTranslation();

    const appStore = useStores('appStore');
    const projectStore = useStores('projectStore');
    const orgStore = useStores('orgStore');

    return (
        <div className={cls.project_menu}>
            <div className={cls.menu_main_title}>
                <div style={{ width: "140px", cursor: "default" }}>
                    <ProjectOutlined style={{ width: "20px" }} />{t("leftMenu.project")}
                </div>
                <Space style={{ width: "60px" }}>
                    <Button type="text" style={{ minWidth: 0, padding: "0px 0px", width: "20px" }} icon={<PlusOutlined style={{ color: projectStore.projectList.length == 0 ? "orange" : "white" }} />}
                        title="创建/加入项目"
                        onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            appStore.showCreateOrJoinProject = true;
                        }} />
                    <Button type="text" style={{ minWidth: 0, padding: "0px 0px", width: "20px" }} icon={<UnorderedListOutlined style={{ color: "white" }} />}
                        title="项目管理界面"
                        onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            if (appStore.inEdit) {
                                appStore.showCheckLeave(() => {
                                    projectStore.setCurProjectId("");
                                    orgStore.setCurOrgId("");
                                    appStore.curExtraMenu = null;
                                    history.push(APP_PROJECT_MANAGER_PATH);
                                });
                            } else {
                                projectStore.setCurProjectId("");
                                orgStore.setCurOrgId("");
                                appStore.curExtraMenu = null;
                                history.push(APP_PROJECT_MANAGER_PATH);
                            }
                        }} />
                </Space>
            </div>
            <div style={{ maxHeight: "calc(50vh - 150px)", overflowY: "scroll" }}>
                {projectStore.projectList.map(item => (
                    <div key={item.project_id} className={cls.project_child_menu}>
                        <ProjectItem item={item} />
                    </div>
                ))}
            </div>
        </div>
    )
};

export default observer(ProjectList);