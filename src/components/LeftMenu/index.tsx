//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import workbench_icon from '@/assets/allIcon/workbench_icon.png';
import { useStores } from '@/hooks';
import { Layout } from 'antd';
import { observer } from 'mobx-react';
import React, { useEffect, useState } from 'react';
import cls from './index.module.less';
const { Sider } = Layout;
import ProjectList from './ProjectList';
import { ExportOutlined, GlobalOutlined, LinkOutlined, PieChartOutlined, RiseOutlined } from '@ant-design/icons';
import { useHistory, useLocation } from 'react-router-dom';
import { APP_EXTERN_PAGE_PATH, DATA_VIEW_PATH, GROW_CENTER_PATH, PUB_RES_PATH, WORKBENCH_PATH } from '@/utils/constant';
import OrgList from './OrgList';
import { getVersion } from '@tauri-apps/api/app';
import { open as shell_open } from '@tauri-apps/api/shell';
import AsyncImage from '../AsyncImage';
import { useTranslation } from "react-i18next";


const LeftMenu: React.FC = () => {
  const location = useLocation();
  const history = useHistory();

  const { t } = useTranslation();

  const userStore = useStores('userStore');
  const appStore = useStores('appStore');
  const projectStore = useStores('projectStore');
  const orgStore = useStores('orgStore');

  const [version, setVersion] = useState("");

  useEffect(() => {
    getVersion().then(res => setVersion(res));
  }, []);

  return (
    <Sider className={cls.sider}>
      <div style={{ height: "10px" }} />
      <div>
        {appStore.vendorCfg?.ability.enable_work_bench == true && (
          <div className={`${cls.workbench_menu} ${location.pathname.startsWith(WORKBENCH_PATH) ? cls.active_menu : ""}`}
            style={{ marginLeft: "10px", marginRight: "10px", paddingBottom: "4px", paddingLeft: "10px" }}
            onClick={e => {
              e.stopPropagation();
              e.preventDefault();
              if (appStore.inEdit) {
                appStore.showCheckLeave(() => {
                  history.push(WORKBENCH_PATH);
                  projectStore.setCurProjectId("");
                  orgStore.setCurOrgId("");
                  appStore.curExtraMenu = null;
                });
              } else {
                history.push(WORKBENCH_PATH);
                projectStore.setCurProjectId("");
                orgStore.setCurOrgId("");
                appStore.curExtraMenu = null;
              }
            }}>
            <img src={workbench_icon} alt="" className={cls.workbench_icon} />
            {t("leftMenu.workbench")}
          </div>
        )}
        {userStore.sessionId != "" && appStore.vendorCfg?.ability.enable_dataview && userStore.userInfo.featureInfo.enable_data_view && (
          <div className={`${cls.workbench_menu} ${location.pathname.startsWith(DATA_VIEW_PATH) ? cls.active_menu : ""}`}
            style={{ marginLeft: "10px", marginRight: "10px", paddingBottom: "4px", paddingLeft: "10px" }}
            onClick={e => {
              e.stopPropagation();
              e.preventDefault();
              if (appStore.inEdit) {
                appStore.showCheckLeave(() => {
                  history.push(DATA_VIEW_PATH);
                  projectStore.setCurProjectId("");
                  orgStore.setCurOrgId("");
                  appStore.curExtraMenu = null;
                });
              } else {
                history.push(DATA_VIEW_PATH);
                projectStore.setCurProjectId("");
                orgStore.setCurOrgId("");
                appStore.curExtraMenu = null;
              }
            }}>
            <PieChartOutlined />&nbsp;{t("leftMenu.dataview")}
          </div>
        )}
        {userStore.sessionId != "" && appStore.vendorCfg?.ability.enable_project && userStore.userInfo.featureInfo.enable_project && (
          <>
            <div style={{ borderBottom: "2px dotted #333", margin: "5px 24px", paddingTop: "5px" }} />
            <ProjectList />
          </>
        )}

        {userStore.sessionId != "" && appStore.vendorCfg?.ability.enable_org && userStore.userInfo.featureInfo.enable_org && (
          <>
            <div style={{ borderTop: "2px dotted #333", margin: "5px 24px", paddingTop: "5px" }} />
            <OrgList />
          </>
        )}
        {(appStore.vendorCfg?.ability.enable_pubres || (appStore.vendorCfg?.ability.enable_grow_center && userStore.userInfo.featureInfo.enable_grow_center)) && (
          <div style={{ borderTop: "2px dotted #333", margin: "5px 24px" }} />
        )}
        {appStore.vendorCfg?.ability.enable_pubres && (
          <div className={`${cls.workbench_menu} ${location.pathname.startsWith(PUB_RES_PATH) ? cls.active_menu : ""}`}
            style={{ marginLeft: "10px", marginRight: "10px", paddingBottom: "4px", paddingLeft: "10px" }}
            onClick={e => {
              e.stopPropagation();
              e.preventDefault();
              if (appStore.inEdit) {
                appStore.showCheckLeave(() => {
                  history.push(PUB_RES_PATH);
                  projectStore.setCurProjectId("");
                  orgStore.setCurOrgId("");
                  appStore.curExtraMenu = null;
                });
              } else {
                history.push(PUB_RES_PATH);
                projectStore.setCurProjectId("");
                orgStore.setCurOrgId("");
                appStore.curExtraMenu = null;
              }
            }}>
            <GlobalOutlined />&nbsp;{t("leftMenu.pubres")}
          </div>
        )}

        {appStore.vendorCfg?.ability.enable_grow_center && (userStore.userInfo.featureInfo.enable_grow_center || appStore.vendorCfg.grow_center.force_show) && (
          <div className={`${cls.workbench_menu} ${location.pathname.startsWith(GROW_CENTER_PATH) ? cls.active_menu : ""}`}
            style={{ marginLeft: "10px", marginRight: "10px", paddingBottom: "4px", paddingLeft: "10px" }}
            onClick={e => {
              e.stopPropagation();
              e.preventDefault();
              if (appStore.inEdit) {
                appStore.showCheckLeave(() => {
                  history.push(GROW_CENTER_PATH);
                  projectStore.setCurProjectId("");
                  orgStore.setCurOrgId("");
                  appStore.curExtraMenu = null;
                });
              } else {
                history.push(GROW_CENTER_PATH);
                projectStore.setCurProjectId("");
                orgStore.setCurOrgId("");
                appStore.curExtraMenu = null;
              }
            }}>
            <RiseOutlined />&nbsp;{t("leftMenu.growcenter")}
          </div>
        )}

        {(((appStore.clientCfg?.item_list.filter(item => item.main_menu) ?? []).length > 0 && appStore.vendorCfg?.layout.enable_server_menu) ||
          (appStore.vendorCfg?.layout.menu_list ?? []).length > 0) && (
            <div style={{ borderTop: "2px dotted #333", margin: "5px 24px" }} />
          )}
        {appStore.vendorCfg?.layout.enable_server_menu && appStore.clientCfg?.item_list.filter(item => item.main_menu).map(item => (
          <div className={`${cls.workbench_menu} ${appStore.curExtraMenu?.menu_id == item.menu_id ? cls.active_menu : ""}`}
            key={item.menu_id}
            style={{ marginLeft: "10px", marginRight: "10px", paddingBottom: "4px", paddingLeft: "10px" }}
            onClick={e => {
              e.stopPropagation();
              e.preventDefault();
              if (item.open_in_browser) {
                shell_open(item.url);
              } else {
                projectStore.setCurProjectId("");
                orgStore.setCurOrgId("");
                appStore.curExtraMenu = item;
                history.push(APP_EXTERN_PAGE_PATH);
              }
            }}
          >
            {item.open_in_browser && <ExportOutlined />}
            {!item.open_in_browser && <LinkOutlined />}
            &nbsp;<span title={item.name}>{item.name}</span>
          </div>
        ))}
      </div>

      {appStore.vendorCfg?.layout.menu_list.map(item => (
        <div className={`${cls.workbench_menu} ${appStore.curExtraMenu?.menu_id == item.id ? cls.active_menu : ""}`}
          key={item.id}
          style={{ marginLeft: "10px", marginRight: "10px", paddingBottom: "4px", paddingLeft: "10px" }}
          onClick={e => {
            e.stopPropagation();
            e.preventDefault();
            projectStore.setCurProjectId("");
            orgStore.setCurOrgId("");
            appStore.curExtraMenu = {
              name: item.name,
              url: item.url,
              menu_id: item.id,
              weight: 0,
              main_menu: true,
              open_in_browser: false,
            };
            history.push(APP_EXTERN_PAGE_PATH);
          }}
        >
          <LinkOutlined />
          &nbsp;<span title={item.name}>{item.name}</span>
        </div>
      ))}

      {appStore.vendorCfg?.layout.vendor_logo_url != "" && (
        <a style={{ cursor: appStore.vendorCfg?.layout.vendor_link_url == "" ? undefined : "pointer" }} onClick={e => {
          e.stopPropagation();
          e.preventDefault();
          if (appStore.vendorCfg?.layout.vendor_link_url != "") {
            shell_open(appStore.vendorCfg?.layout.vendor_link_url ?? "");
          }
        }}>
          <AsyncImage src={appStore.vendorCfg?.layout.vendor_logo_url ?? ""} useRawImg
            height={appStore.vendorCfg?.layout.vendor_logo_height ?? 40}
            style={{ position: "absolute", bottom: "2px", left: "10px" }} />
        </a>
      )}

      {appStore.vendorCfg?.layout.show_manual_and_version && (
        <>
          <div style={{ position: "absolute", bottom: "2px", left: "10px", cursor: "pointer" }} onClick={e => {
            e.stopPropagation();
            e.preventDefault();
            shell_open("https://openlinksaas.atomgit.net/docs/#/");
          }}>
            使用手册</div>
          <div style={{ position: "absolute", bottom: "2px", right: "10px", cursor: "pointer" }} onClick={e => {
            e.stopPropagation();
            e.preventDefault();
            shell_open("https://atomgit.com/openlinksaas-org/desktop/tags?tab=release");
          }}>
            软件版本:{version}
          </div>
        </>
      )}

    </Sider>
  );
};
export default observer(LeftMenu);
