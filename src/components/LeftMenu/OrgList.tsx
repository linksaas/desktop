//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from "react";
import cls from './index.module.less';
import { observer } from 'mobx-react';
import { useHistory } from "react-router-dom";
import { APP_ORG_MANAGER_PATH, APP_ORG_PATH } from "@/utils/constant";
import { PlusOutlined, TeamOutlined, UnorderedListOutlined } from "@ant-design/icons";
import { useStores } from "@/hooks";
import { Button, Space } from "antd";
import { useTranslation } from "react-i18next";

const OrgList = () => {
    const history = useHistory();

    const { t } = useTranslation();

    const appStore = useStores('appStore');
    const projectStore = useStores('projectStore');
    const orgStore = useStores('orgStore');

    return (
        <div className={cls.project_menu}>
            <div className={cls.menu_main_title}>
                <div style={{ width: "140px", cursor: "default" }} >
                    <TeamOutlined style={{ width: "20px" }} />{t("leftMenu.org")}
                </div>
                <Space style={{ width: "60px" }}>
                    <Button type="text" style={{ minWidth: 0, padding: "0px 0px", width: "20px" }} icon={<PlusOutlined style={{ color: orgStore.orgList.length == 0 ? "orange" : "white" }} />}
                        title="创建/加入团队"
                        onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            appStore.showCreateOrJoinOrg = true;
                        }} />
                    <Button type="text" style={{ minWidth: 0, padding: "0px 0px", width: "20px" }} icon={<UnorderedListOutlined style={{ color: "white" }} />}
                        title="团队管理界面"
                        onClick={e => {
                            e.stopPropagation();
                            e.preventDefault();
                            if (appStore.inEdit) {
                                appStore.showCheckLeave(() => {
                                    projectStore.setCurProjectId("");
                                    orgStore.setCurOrgId("");
                                    appStore.curExtraMenu = null;
                                    history.push(APP_ORG_MANAGER_PATH);
                                });
                            } else {
                                projectStore.setCurProjectId("");
                                orgStore.setCurOrgId("");
                                appStore.curExtraMenu = null;
                                history.push(APP_ORG_MANAGER_PATH);
                            }
                        }} />
                </Space>
            </div>

            <div style={{ maxHeight: "calc(50vh - 250px)", overflowY: "scroll" }}>
                {orgStore.orgList.map(item => (
                    <div key={item.org_id} className={cls.project_child_menu}>
                        <div className={cls.project_child_wrap}>
                            <div className={`${cls.project_child_title} ${item.org_id == orgStore.curOrgId ? cls.active_menu : ""}`}>
                                <span className={cls.name} title={item.basic_info.org_name}
                                    onClick={e => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        if (appStore.inEdit) {
                                            appStore.showCheckLeave(() => {
                                                orgStore.setCurOrgId(item.org_id).then(() => {
                                                    projectStore.setCurProjectId("");
                                                    appStore.curExtraMenu = null;
                                                    history.push(APP_ORG_PATH);
                                                });
                                            });
                                        } else {
                                            orgStore.setCurOrgId(item.org_id).then(() => {
                                                projectStore.setCurProjectId("");
                                                appStore.curExtraMenu = null;
                                                history.push(APP_ORG_PATH);
                                            });
                                        }
                                    }}>
                                    {item.basic_info.org_name}
                                </span>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default observer(OrgList);