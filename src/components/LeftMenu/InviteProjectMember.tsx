//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { Button, Form, Input, message, Modal, Select, Space, Table, Tabs } from 'antd';
import type { FC } from 'react';
import { useEffect, useState } from 'react';
import React from 'react';
import { writeText } from '@tauri-apps/api/clipboard';
import { gen_invite } from '@/api/project_member';
import { request } from '@/utils/request';
import { observer } from 'mobx-react';
import { useStores } from '@/hooks';
import type { InviteInfo } from "@/api/project_member";
import { list_invite, remove_invite } from "@/api/project_member";
import type { ColumnsType } from 'antd/es/table';
import UserPhoto from "@/components/Portrait/UserPhoto";
import moment from 'moment';
import { useTranslation } from "react-i18next";

const { TextArea } = Input;

const PAGE_SIZE = 10;

const InviteList = observer(() => {
  const { t } = useTranslation();

  const userStore = useStores('userStore');
  const projectStore = useStores('projectStore');

  const [inviteList, setInviteList] = useState<InviteInfo[]>([]);
  const [totalCount, setTotalCount] = useState(0);
  const [curPage, setCurPage] = useState(0);

  const loadInviteList = async () => {
    const res = await request(list_invite({
      session_id: userStore.sessionId,
      project_id: projectStore.curProjectId,
      offset: PAGE_SIZE * curPage,
      limit: PAGE_SIZE,
    }));
    setTotalCount(res.total_count);
    setInviteList(res.invite_info_list);
  };

  const removeInvite = async (inviteCode: string) => {
    await request(remove_invite({
      session_id: userStore.sessionId,
      project_id: projectStore.curProjectId,
      invite_code: inviteCode,
    }));
    if (curPage != 0) {
      setCurPage(0);
    } else {
      await loadInviteList();
    }
    message.info(t("text.removeSuccess"));
  };

  const columns: ColumnsType<InviteInfo> = [
    {
      title: t("project.member.inviteHistory.genUser"),
      width: 150,
      render: (_, row: InviteInfo) => (
        <Space style={{ overflow: "hidden" }} title={row.create_display_name}>
          <UserPhoto logoUri={row.create_logo_uri} style={{ width: "24px", height: "24px", borderRadius: "24px" }} />
          <span>{row.create_display_name}</span>
        </Space>
      ),
    },
    {
      title: t("project.member.inviteHistory.startTime"),
      width: 120,
      render: (_, row: InviteInfo) => moment(row.create_time).format("YYYY-MM-DD HH点"),
    },
    {
      title: t("project.member.inviteHistory.expireTime"),
      width: 120,
      render: (_, row: InviteInfo) => moment(row.expire_time).format("YYYY-MM-DD HH点"),
    },
    {
      title: t("project.member.inviteHistory.code"),
      width: 100,
      render: (_, row: InviteInfo) => <div style={{ textWrap: "wrap", width: "100px" }}>{row.invite_code}</div>
    },
    {
      title: t("text.operation"),
      render: (_, row: InviteInfo) => (
        <Space>
          <Button type="link"
            style={{ minWidth: 0, padding: "0px 0px" }}
            onClick={e => {
              e.stopPropagation();
              e.preventDefault();
              writeText(row.invite_code);
              message.info(t("text.copySuccess"));
            }}>{t("text.copy")}</Button>
          <Button type="link" danger onClick={e => {
            e.stopPropagation();
            e.preventDefault();
            removeInvite(row.invite_code);
          }}>{t("project.member.inviteHistory.remove")}</Button>
        </Space>
      ),
    }
  ];

  useEffect(() => {
    loadInviteList();
  }, [projectStore.curProjectId, curPage]);

  return (
    <Table rowKey="invite_code" dataSource={inviteList} columns={columns} pagination={{
      current: curPage + 1,
      total: totalCount,
      pageSize: PAGE_SIZE,
      onChange: page => setCurPage(page - 1),
      hideOnSinglePage: true,
      showSizeChanger: false
    }} scroll={{ y: "calc(100vh - 400px)" }} />
  );
});


type InviteProjectMemberProps = {
  visible: boolean;
  onChange: (boo: boolean) => void;
};

const InviteProjectMember: FC<InviteProjectMemberProps> = (props) => {
  const { t } = useTranslation();

  const { visible, onChange } = props;

  const userStore = useStores("userStore");
  const projectStore = useStores("projectStore");

  const [linkText, setLinkText] = useState('');
  const [ttl, setTtl] = useState(1);

  const [activeKey, setActiveKey] = useState('invite');

  const getTtlStr = () => {
    if (ttl < 24) {
      return `${ttl}${t("text.hour")}`;
    } else {
      return `${(ttl / 24).toFixed(0)}${t("text.day")}`;
    }
  };

  const genInvite = async () => {
    const res = await request(gen_invite(userStore.sessionId, projectStore.curProjectId, ttl));
    if (res) {
      setLinkText(`${userStore.userInfo.displayName} 邀请您加入 ${projectStore.curProject?.basic_info.project_name ?? ""} 项目，您的邀请码 ${res.invite_code} (有效期${getTtlStr()}),请在软件内输入邀请码加入项目。`);
    }
  };

  const copyAndClose = async () => {
    await writeText(linkText);
    onChange(false);
    message.success(t("text.copySuccess"));
  };


  const getOkText = (): string => {
    if (activeKey == "invite") {
      return linkText == "" ? t("project.member.invite.genCode") : t("project.member.invite.copyAndClose");
    }
    return "";
  }

  return (
    <Modal
      open={visible}
      title={t("project.member.invite.title")}
      width={700} mask={false}
      footer={activeKey == "history" ? null : undefined}
      okText={getOkText()}
      bodyStyle={{ padding: "0px 10px" }}
      onCancel={e => {
        e.stopPropagation();
        e.preventDefault();
        onChange(false);
      }}
      onOk={e => {
        e.stopPropagation();
        e.preventDefault();
        if (activeKey == "invite") {
          if (linkText == "") {
            genInvite();
          } else {
            copyAndClose();
          }
        }
      }}
    >
      <Tabs activeKey={activeKey} onChange={key => { setActiveKey(key); console.log(key) }}
        type="card">
        <Tabs.TabPane tab={t("text.invite")} key='invite'>
          {linkText == "" && (
            <Form>
              <Form.Item label={t("text.periodOfValid")}>
                <Select value={ttl} onChange={value => setTtl(value)}>
                  <Select.Option value={1}>1{t("text.hour")}</Select.Option>
                  <Select.Option value={3}>3{t("text.hour")}</Select.Option>
                  <Select.Option value={24}>1{t("text.day")}</Select.Option>
                  <Select.Option value={24 * 3}>3{t("text.day")}</Select.Option>
                  <Select.Option value={24 * 7}>1{t("text.week")}</Select.Option>
                  <Select.Option value={24 * 14}>2{t("text.week")}</Select.Option>
                  <Select.Option value={24 * 30}>1{t("text.month")}</Select.Option>
                </Select>
              </Form.Item>
            </Form>
          )}
          {linkText != "" && (
            <>
              <div
                style={{
                  textAlign: 'left',
                  fontSize: '14px',
                  lineHeight: '20px',
                  color: ' #2C2D2E',
                }}
              >
                {t("project.member.invite.tip")}
              </div>

              <div style={{ margin: '10px 0' }}>
                <TextArea placeholder="请输入" value={linkText} autoSize={{ minRows: 2, maxRows: 5 }} readOnly />
              </div>
            </>
          )}
        </Tabs.TabPane>
        <Tabs.TabPane tab={t("project.member.invite.history")} key='history'>
          {activeKey == "history" && <InviteList />}
        </Tabs.TabPane>
      </Tabs>
    </Modal>
  );
};

export default observer(InviteProjectMember);
