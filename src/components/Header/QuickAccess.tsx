//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { MenuOutlined } from "@ant-design/icons";
import { Dropdown, message, Modal } from "antd";
import React, { useEffect, useState } from "react";
import { observer } from 'mobx-react';
import type { MenuProps } from 'antd';
import { useStores } from "@/hooks";
import type { MenuInfo } from 'rc-menu/lib/interface';
import { useHistory } from "react-router-dom";
import { ISSUE_TAB_LIST_TYPE, LinkIdeaPageInfo } from "@/stores/linkAux";
import type { ItemType } from "antd/lib/menu/hooks/useItems";
import { APP_PROJECT_HOME_PATH } from "@/utils/constant";
import { ENTRY_TYPE_API_COLL, ENTRY_TYPE_DATAVIEW, ENTRY_TYPE_DOC, ENTRY_TYPE_DRAW, ENTRY_TYPE_KNOWLEDGE, ENTRY_TYPE_PAGES, ENTRY_TYPE_SPRIT } from "@/api/project_entry";
import { useHotkeys } from 'react-hotkeys-hook';
import HotkeyHelpInfo from "@/pages/Project/Overview/components/HotkeyHelpInfo";
import { PROJECT_HOME_CONTENT_LIST } from "@/api/project";
import { type FeatureInfo, update_feature, USER_TYPE_INTERNAL } from "@/api/user";
import { get_port, get_token } from '@/api/local_api';
import { WebviewWindow, appWindow } from '@tauri-apps/api/window';
import { request } from "@/utils/request";
import { writeText } from '@tauri-apps/api/clipboard';
import { useTranslation } from "react-i18next";
import i18n from 'i18next';


const MENU_KEY_USER_LOGIN = "user.login";
const MENU_KEY_USER_LOGOUT = "user.logout";
const MENU_KEY_USER_SHOW_ACCOUNT = "user.showAccount"
const MENU_KEY_USER_CHANGE_PASSWORD = "user.changePasswd";
const MENU_KEY_USER_CHANGE_LOGO = "user.changeLogo";
const MENU_KEY_USER_CHANGE_RESUME = "user.changeResume";

const MENU_KEY_USER_SWITCH_ORG = "user.switchOrg";
const MENU_KEY_USER_SWITCH_PROJECT = "user.switchProject";
const MENU_KEY_USER_SWITCH_DATA_VIEW = "user.switchDataView";
const MENU_KEY_USER_SWITCH_GROW_CENTER = "user.switchGrowCenter";


const MENU_KEY_ADMIN_LOGIN = "admin.login";
const MENU_KEY_EXIST_APP = "app.exit";

const MENU_KEY_TEST_LOCALAPI = "localapi.test";

const MENU_KEY_JOIN_PROJECT_OR_ORG = "join.projectOrOrg";


const MENU_KEY_SHOW_INVITE_MEMBER = "invite.member.show";
const MENU_KEY_MEMBER_PREFIX = "member:";
const MENU_KEY_SHOW_TOOL_BAR_IDEA = "toolbar.idea.show"

const MENU_KEY_SHOW_TOOL_BAR_REQUIRE_MENT = "toolbar.requirement.show";
const MENU_KEY_CREATE_REQUIRE_MENT = "create.requirement";

const MENU_KEY_SHOW_TOOL_BAR_TASK_MY = "toolbar.task.my.show";
const MENU_KEY_SHOW_TOOL_BAR_TASK_ALL = "toolbar.task.all.show";
const MENU_KEY_CREATE_TASK = "create.task";

const MENU_KEY_SHOW_TOOL_BAR_BUG_MY = "toolbar.bug.my.show";
const MENU_KEY_SHOW_TOOL_BAR_BUG_ALL = "toolbar.bug.all.show";
const MENU_KEY_CREATE_BUG = "create.bug";

const MENU_KEY_SHOW_TOOL_BAR_TEST_CASE = "toolbar.testcase.show";
const MENU_KEY_CREATE_TEST_CASE = "create.testcase";

const MENU_KEY_SHOW_TOOL_BAR_EVENTS = "toolbar.events.show";

const MENU_KEY_SHOW_TOOL_BAR_RECYCLE = "toolbar.recycle.show";
const MENU_KEY_SHOW_TOOL_BAR_OVERVIEW = "toolbar.overview.show";

const MENU_KEY_SHOW_TOOL_BAR_CHAT_AND_COMMENT = "toolbar.chatAndComment.show"


const MENU_KEY_ENTRY_CREATE_SPRIT = "project.entry.sprit.create";
const MENU_KEY_ENTRY_CREATE_DOC = "project.entry.doc.create";
const MENU_KEY_ENTRY_CREATE_DRAW = "project.entry.draw.create";
const MENU_KEY_ENTRY_CREATE_PAGES = "project.entry.pages.create";
const MENU_KEY_ENTRY_CREATE_API_COLL = "project.entry.apicoll.create";

const MENU_KEY_HOME_PREFIX = "project.home."
const MENU_KEY_HOME_KNOWLEDGE = MENU_KEY_HOME_PREFIX + "knowledge";
const MENU_KEY_HOME_WORK_PLAN = MENU_KEY_HOME_PREFIX + "workplan";
const MENU_KEY_HOME_DATA_VIEW = MENU_KEY_HOME_PREFIX + "dataview";


const MENU_KEY_HOME_MYWORK = MENU_KEY_HOME_PREFIX + "mywork";

const MENU_KEY_SHOW_HELP = "help.show"

const MENU_KEY_ORG_SHOW_INVITE_MEMBER = "org.invite.member.show";
const MENU_KEY_ORG_SHOW_SETTING = "org.setting.show";
const MENU_KEY_ORG_SHOW_EVAL_TARGET = "org.evaltarget.show";



const QuickAccess = () => {
    const { t } = useTranslation();

    const memberStore = useStores('memberStore');
    const linkAuxStore = useStores('linkAuxStore');
    const projectStore = useStores('projectStore');
    const orgStore = useStores('orgStore');
    const appStore = useStores('appStore');
    const entryStore = useStores('entryStore');
    const userStore = useStores('userStore');
    const projectModalStore = useStores('projectModalStore');

    const history = useHistory();

    const [items, setItems] = useState<MenuProps['items']>([]);
    const [_, setCreateFlag] = useState(false);
    const [showHelp, setShowHelp] = useState(false);
    const [showLoginAccount, setshowLoginAccount] = useState(false);

    const calcItems = () => {
        i18n.changeLanguage(appStore.lang);

        const tmpItems: MenuProps['items'] = [];
        if (projectStore.curProjectId != "") {
            tmpItems.push({
                key: "home",
                label: t("header.quickAccess.projectHome"),
                children: [
                    {
                        key: MENU_KEY_HOME_WORK_PLAN,
                        label: t("header.quickAccess.workPlan"),
                    },
                    {
                        key: MENU_KEY_HOME_KNOWLEDGE,
                        label: t("header.quickAccess.knowledge"),
                    },
                    {
                        key: MENU_KEY_HOME_DATA_VIEW,
                        label: t("header.quickAccess.dataview"),
                    },
                ],
            });
            const memberItem: ItemType = {
                key: "member",
                label: t("header.quickAccess.projectMember"),
                children: [
                    {
                        key: MENU_KEY_SHOW_INVITE_MEMBER,
                        label: t("header.quickAccess.inviteMember"),
                        disabled: projectStore.curProject?.closed || projectStore.isAdmin == false,
                    },
                    {
                        key: "members",
                        label: t("header.quickAccess.memberList"),
                        children: memberStore.memberList.map(item => ({
                            key: `${MENU_KEY_MEMBER_PREFIX}${item.member.member_user_id}`,
                            label: item.member.display_name,
                        })),
                    }
                ]
            };
            tmpItems.push(memberItem);
            tmpItems.push({
                key: "contentEntry",
                label: t("header.quickAccess.contentEntry"),
                children: [
                    {
                        key: MENU_KEY_ENTRY_CREATE_SPRIT,
                        label: t("header.quickAccess.createWorkPlan"),
                        disabled: projectStore.isAdmin == false || [ENTRY_TYPE_SPRIT].includes(projectStore.projectHome.contentEntryType) == false,
                    },
                    {
                        key: MENU_KEY_ENTRY_CREATE_DOC,
                        label: t("header.quickAccess.createDoc"),
                        disabled: [ENTRY_TYPE_DOC, ENTRY_TYPE_KNOWLEDGE].includes(projectStore.projectHome.contentEntryType) == false,
                    },
                    {
                        key: MENU_KEY_ENTRY_CREATE_DRAW,
                        label: t("header.quickAccess.createDraw"),
                        disabled: [ENTRY_TYPE_DRAW, ENTRY_TYPE_KNOWLEDGE].includes(projectStore.projectHome.contentEntryType) == false,
                    },
                    {
                        key: MENU_KEY_ENTRY_CREATE_PAGES,
                        label: t("header.quickAccess.createPages"),
                        disabled: [ENTRY_TYPE_PAGES, ENTRY_TYPE_KNOWLEDGE].includes(projectStore.projectHome.contentEntryType) == false,
                    },
                    {
                        key: MENU_KEY_ENTRY_CREATE_API_COLL,
                        label: t("header.quickAccess.createApiColl"),
                        disabled: [ENTRY_TYPE_API_COLL, ENTRY_TYPE_KNOWLEDGE].includes(projectStore.projectHome.contentEntryType) == false,
                    },
                ],
            });
            tmpItems.push({
                key: MENU_KEY_SHOW_TOOL_BAR_CHAT_AND_COMMENT,
                label: t("header.quickAccess.projectCommunication"),
            });

            tmpItems.push({
                key: MENU_KEY_HOME_MYWORK,
                label: t("header.quickAccess.myWork"),
            });

            tmpItems.push({
                key: MENU_KEY_SHOW_TOOL_BAR_IDEA,
                label: t("header.quickAccess.knowledgePoint"),
            });

            tmpItems.push({
                key: "requirement",
                label: t("header.quickAccess.requirement"),
                children: [
                    {
                        key: MENU_KEY_SHOW_TOOL_BAR_REQUIRE_MENT,
                        label: t("header.quickAccess.listRequirement"),
                    },
                    {
                        key: MENU_KEY_CREATE_REQUIRE_MENT,
                        label: t("header.quickAccess.createRequirement"),
                    },
                ],
            });
            tmpItems.push({
                key: "task",
                label: t("header.quickAccess.task"),
                children: [
                    {
                        key: MENU_KEY_SHOW_TOOL_BAR_TASK_MY,
                        label: t("header.quickAccess.listMyTask"),
                    },
                    {
                        key: MENU_KEY_SHOW_TOOL_BAR_TASK_ALL,
                        label: t("header.quickAccess.listAllTask"),
                    },
                    {
                        key: MENU_KEY_CREATE_TASK,
                        label: t("header.quickAccess.createTask"),
                    }
                ],
            });
            tmpItems.push({
                key: "bug",
                label: t("header.quickAccess.bug"),
                children: [
                    {
                        key: MENU_KEY_SHOW_TOOL_BAR_BUG_MY,
                        label: t("header.quickAccess.listMyBug"),
                    },
                    {
                        key: MENU_KEY_SHOW_TOOL_BAR_BUG_ALL,
                        label: t("header.quickAccess.listAllBug"),
                    },
                    {
                        key: MENU_KEY_CREATE_BUG,
                        label: t("header.quickAccess.createBug"),
                    }
                ],
            });
            tmpItems.push({
                key: "testcase",
                label: t("header.quickAccess.testcase"),
                children: [
                    {
                        key: MENU_KEY_SHOW_TOOL_BAR_TEST_CASE,
                        label: t("header.quickAccess.listTestcase"),
                    },
                    {
                        key: MENU_KEY_CREATE_TEST_CASE,
                        label: t("header.quickAccess.createTestcase"),
                    },
                ],
            });
            tmpItems.push({
                key: MENU_KEY_SHOW_TOOL_BAR_EVENTS,
                label: t("header.quickAccess.listWorkEvent"),
            });
            tmpItems.push({
                key: MENU_KEY_SHOW_TOOL_BAR_RECYCLE,
                label: t("header.quickAccess.openRecycle"),
            });
            tmpItems.push({
                key: MENU_KEY_SHOW_TOOL_BAR_OVERVIEW,
                label: t("header.quickAccess.projectInfo"),
            });
            tmpItems.push({
                key: MENU_KEY_SHOW_HELP,
                label: t("header.quickAccess.shortcutHelp"),
            });
        }
        //团队菜单
        if (orgStore.curOrgId != "") {
            const orgItem: ItemType = {
                key: "org",
                label: t("header.quickAccess.org"),
                children: [],
            }
            if (userStore.userInfo.userId == orgStore.curOrg?.owner_user_id) {
                orgItem.children.push({
                    key: MENU_KEY_ORG_SHOW_INVITE_MEMBER,
                    label: t("header.quickAccess.inviteMember"),
                });
                orgItem.children.push({
                    key: MENU_KEY_ORG_SHOW_SETTING,
                    label: t("header.quickAccess.org.setting"),
                })
            }
            if ((userStore.userInfo.userId == orgStore.curOrg?.owner_user_id || orgStore.selfMember?.member_perm_info.manage_evaluate_target) &&
                orgStore.curOrg?.setting.enable_evaluate == true) {
                orgItem.children.push({
                    key: MENU_KEY_ORG_SHOW_EVAL_TARGET,
                    label: t("header.quickAccess.org.evalTarget"),
                });
            }
            tmpItems.push(orgItem);
        }

        //全局菜单
        if (appStore.vendorCfg?.account.inner_account == true || appStore.vendorCfg?.account.external_account == true) {
            const userItem: ItemType = {
                key: "user",
                label: t("header.quickAccess.account"),
                children: [],
            };
            if (userStore.sessionId == "") {
                userItem.children.push({
                    key: MENU_KEY_USER_LOGIN,
                    label: t("header.quickAccess.account.login"),
                });
            } else {
                userItem.children.push({
                    key: MENU_KEY_USER_SHOW_ACCOUNT,
                    label: t("header.quickAccess.account.view"),
                });
                if (userStore.userInfo.userType == USER_TYPE_INTERNAL && userStore.userInfo.testAccount == false) {
                    userItem.children.push({
                        key: MENU_KEY_USER_CHANGE_PASSWORD,
                        label: t("header.quickAccess.account.changePassword"),
                    });

                    userItem.children.push({
                        key: MENU_KEY_USER_CHANGE_LOGO,
                        label: t("header.quickAccess.account.changeProfilePhoto"),
                    });
                }
                if ((userStore.userInfo.userType == USER_TYPE_INTERNAL && !userStore.userInfo.testAccount) || appStore.vendorCfg.work_bench.enable_user_resume) {
                    userItem.children.push({
                        key: MENU_KEY_USER_CHANGE_RESUME,
                        label: t("header.quickAccess.account.changeResume"),
                    });
                }
                if (appStore.vendorCfg.ability.enable_dataview) {
                    userItem.children.push({
                        key: MENU_KEY_USER_SWITCH_DATA_VIEW,
                        label: userStore.userInfo.featureInfo.enable_data_view ? t("header.quickAccess.account.disableDataview") : t("header.quickAccess.account.enableDataview"),
                    });
                }
                if (appStore.vendorCfg.ability.enable_project) {
                    userItem.children.push({
                        key: MENU_KEY_USER_SWITCH_PROJECT,
                        label: userStore.userInfo.featureInfo.enable_project ? t("header.quickAccess.account.disableProject") : t("header.quickAccess.account.enableProject"),
                    });
                }
                if (appStore.vendorCfg.ability.enable_org) {
                    userItem.children.push({
                        key: MENU_KEY_USER_SWITCH_ORG,
                        label: userStore.userInfo.featureInfo.enable_org ? t("header.quickAccess.account.disableOrg") : t("header.quickAccess.account.enableOrg"),
                    });
                }
                if (appStore.vendorCfg.ability.enable_grow_center) {
                    userItem.children.push({
                        key: MENU_KEY_USER_SWITCH_GROW_CENTER,
                        label: userStore.userInfo.featureInfo.enable_grow_center ? t("header.quickAccess.account.disableGrowCenter") : t("header.quickAccess.account.enableGrowCenter"),
                    });
                }


                userItem.children.push({
                    key: MENU_KEY_USER_LOGOUT,
                    label: <span style={{ color: "red" }}>{t("header.quickAccess.account.logout")}</span>,
                });
            }
            tmpItems.push(userItem);
        }
        if (userStore.sessionId != "") {
            tmpItems.push({
                key: MENU_KEY_JOIN_PROJECT_OR_ORG,
                label: t("header.quickAccess.join"),
            });
        }
        if (appStore.vendorCfg?.layout.show_local_api_in_quick_access) {
            tmpItems.push({
                key: MENU_KEY_TEST_LOCALAPI,
                label: t("header.quickAccess.debugLocalApi"),
            });
        }
        if (appStore.vendorCfg?.layout.show_admin_in_quick_access && appStore.clientCfg?.enable_admin == true && userStore.sessionId == "") {
            tmpItems.push({
                key: MENU_KEY_ADMIN_LOGIN,
                label: t("header.quickAccess.loginAdmin"),
            });
        }
        tmpItems.push({
            key: MENU_KEY_EXIST_APP,
            label: <span style={{ color: "red" }}>{t("header.quickAccess.exist")}</span>,
        });

        setItems(tmpItems);
    };

    const openLocalApi = async () => {
        const port = await get_port();
        const token = await get_token();

        const label = "localapi";
        const view = WebviewWindow.getByLabel(label);
        if (view != null) {
            await view.close();
        }
        const pos = await appWindow.innerPosition();

        new WebviewWindow(label, {
            url: `local_api.html ? port = ${port} & token=${token}`,
            width: 800,
            minWidth: 800,
            height: 600,
            minHeight: 600,
            center: true,
            title: "本地接口调试",
            resizable: true,
            x: pos.x + Math.floor(Math.random() * 200),
            y: pos.y + Math.floor(Math.random() * 200),
        });
    };

    const gotoHomePage = async (key: string) => {
        if (key != MENU_KEY_HOME_MYWORK) {
            projectStore.projectHome.contentEntryType = PROJECT_HOME_CONTENT_LIST;
        }

        let homeType = PROJECT_HOME_CONTENT_LIST;
        projectStore.projectHome.contentEntryType = ENTRY_TYPE_SPRIT;
        if (key == MENU_KEY_HOME_WORK_PLAN) {
            projectStore.projectHome.contentEntryType = ENTRY_TYPE_SPRIT;
        } else if (key == MENU_KEY_HOME_KNOWLEDGE) {
            projectStore.projectHome.contentEntryType = ENTRY_TYPE_KNOWLEDGE;
        } else if (key == MENU_KEY_HOME_DATA_VIEW) {
            projectStore.projectHome.contentEntryType = ENTRY_TYPE_DATAVIEW;
        }

        if (appStore.inEdit) {
            appStore.showCheckLeave(() => {
                entryStore.reset();
                projectStore.projectHome.homeType = homeType;
                if (key == MENU_KEY_HOME_MYWORK) {
                    if (projectStore.showChatAndComment && projectStore.showChatAndCommentTab == "mywork") {
                        projectStore.setShowChatAndComment(false, "mywork");
                    } else {
                        projectStore.setShowChatAndComment(true, "mywork");
                    }
                } else {
                    history.push(APP_PROJECT_HOME_PATH);
                }
            });
            return;
        }
        entryStore.reset();
        projectStore.projectHome.homeType = homeType;
        if (key == MENU_KEY_HOME_MYWORK) {
            if (projectStore.showChatAndComment && projectStore.showChatAndCommentTab == "mywork") {
                projectStore.setShowChatAndComment(false, "mywork");
            } else {
                projectStore.setShowChatAndComment(true, "mywork");
            }
        } else {
            history.push(APP_PROJECT_HOME_PATH);
        }
    }

    const processMenuKey = async (key: string) => {
        switch (key) {
            case MENU_KEY_USER_LOGIN:
                userStore.showUserLogin = true;
                break;
            case MENU_KEY_USER_LOGOUT:
                if (appStore.inEdit) {
                    message.info("请先保存修改内容");
                    return;
                }
                userStore.showLogout = true;
                userStore.accountsModal = false;
                break;
            case MENU_KEY_USER_SHOW_ACCOUNT:
                setshowLoginAccount(true);
                break;
            case MENU_KEY_USER_CHANGE_PASSWORD:
                userStore.showChangePasswd = true;
                userStore.accountsModal = false;
                break;
            case MENU_KEY_USER_CHANGE_LOGO:
                if (userStore.userInfo.testAccount) {
                    return;
                }
                if (userStore.userInfo.userType != USER_TYPE_INTERNAL) {
                    return;
                }
                userStore.showChangeLogo = true;
                userStore.accountsModal = false;
                break;
            case MENU_KEY_USER_CHANGE_RESUME:
                userStore.showChangeResume = true;
                break;
            case MENU_KEY_ADMIN_LOGIN:
                userStore.showAdminLogin = true;
                break;
            case MENU_KEY_EXIST_APP:
                appStore.showExit = true;
                break;
            case MENU_KEY_TEST_LOCALAPI:
                openLocalApi();
                break;
            case MENU_KEY_JOIN_PROJECT_OR_ORG:
                appStore.showJoinModal = true;
                break;
            case MENU_KEY_USER_SWITCH_ORG:
                {
                    const feature: FeatureInfo = {
                        enable_project: userStore.userInfo.featureInfo.enable_project,
                        enable_org: !userStore.userInfo.featureInfo.enable_org,
                        enable_data_view: userStore.userInfo.featureInfo.enable_data_view,
                        enable_grow_center: userStore.userInfo.featureInfo.enable_grow_center,
                    };
                    request(update_feature({
                        session_id: userStore.sessionId,
                        feature: feature,
                    })).then(() => userStore.updateFeature(feature));
                }
                break;
            case MENU_KEY_USER_SWITCH_PROJECT:
                {
                    const feature: FeatureInfo = {
                        enable_project: !userStore.userInfo.featureInfo.enable_project,
                        enable_org: userStore.userInfo.featureInfo.enable_org,
                        enable_data_view: userStore.userInfo.featureInfo.enable_data_view,
                        enable_grow_center: userStore.userInfo.featureInfo.enable_grow_center,
                    };
                    request(update_feature({
                        session_id: userStore.sessionId,
                        feature: feature,
                    })).then(() => userStore.updateFeature(feature));
                }
                break;
            case MENU_KEY_USER_SWITCH_DATA_VIEW:
                {
                    {
                        const feature: FeatureInfo = {
                            enable_project: userStore.userInfo.featureInfo.enable_project,
                            enable_org: userStore.userInfo.featureInfo.enable_org,
                            enable_data_view: !userStore.userInfo.featureInfo.enable_data_view,
                            enable_grow_center: userStore.userInfo.featureInfo.enable_grow_center,
                        };
                        request(update_feature({
                            session_id: userStore.sessionId,
                            feature: feature,
                        })).then(() => userStore.updateFeature(feature));
                    }
                }
                break;
            case MENU_KEY_USER_SWITCH_GROW_CENTER:
                {
                    {
                        const feature: FeatureInfo = {
                            enable_project: userStore.userInfo.featureInfo.enable_project,
                            enable_org: userStore.userInfo.featureInfo.enable_org,
                            enable_data_view: userStore.userInfo.featureInfo.enable_data_view,
                            enable_grow_center: !userStore.userInfo.featureInfo.enable_grow_center,
                        };
                        request(update_feature({
                            session_id: userStore.sessionId,
                            feature: feature,
                        })).then(() => userStore.updateFeature(feature));
                    }
                }
            case MENU_KEY_SHOW_INVITE_MEMBER:
                if (projectStore.curProjectId != "") {
                    projectStore.setShowChatAndComment(true, "member");
                    memberStore.showInviteMember = true;
                    linkAuxStore.pickupToolbar(history);
                }
                break;
            case MENU_KEY_SHOW_TOOL_BAR_IDEA:
                if (projectStore.curProjectId != "") {
                    linkAuxStore.goToLink(new LinkIdeaPageInfo("", projectStore.curProjectId, "", []), history);
                }
                break;
            case MENU_KEY_SHOW_TOOL_BAR_REQUIRE_MENT:
                if (projectStore.curProjectId != "") {
                    linkAuxStore.goToRequirementList(history);
                }
                break;
            case MENU_KEY_CREATE_REQUIRE_MENT:
                if (projectStore.curProjectId != "") {
                    projectModalStore.projectId = projectStore.curProjectId;
                    projectModalStore.createRequirement = true;
                }
                break;
            case MENU_KEY_SHOW_TOOL_BAR_TASK_MY:
                if (projectStore.curProjectId != "") {
                    linkAuxStore.goToTaskList({
                        stateList: [],
                        execUserIdList: [],
                        checkUserIdList: [],
                        tabType: ISSUE_TAB_LIST_TYPE.ISSUE_TAB_LIST_ASSGIN_ME,
                    }, history);
                }
                break;
            case MENU_KEY_SHOW_TOOL_BAR_TASK_ALL:
                if (projectStore.curProjectId != "") {
                    linkAuxStore.goToTaskList({
                        stateList: [],
                        execUserIdList: [],
                        checkUserIdList: [],
                        tabType: ISSUE_TAB_LIST_TYPE.ISSUE_TAB_LIST_ALL,
                    }, history);
                }
                break;
            case MENU_KEY_CREATE_TASK:
                if (projectStore.curProjectId != "") {
                    linkAuxStore.goToCreateTask(projectStore.curProjectId);
                }
                break;
            case MENU_KEY_SHOW_TOOL_BAR_BUG_MY:
                if (projectStore.curProjectId != "") {
                    linkAuxStore.goToBugList({
                        stateList: [],
                        execUserIdList: [],
                        checkUserIdList: [],
                        tabType: ISSUE_TAB_LIST_TYPE.ISSUE_TAB_LIST_ASSGIN_ME,
                    }, history);
                }
                break;
            case MENU_KEY_SHOW_TOOL_BAR_BUG_ALL:
                if (projectStore.curProjectId != "") {
                    linkAuxStore.goToBugList({
                        stateList: [],
                        execUserIdList: [],
                        checkUserIdList: [],
                        tabType: ISSUE_TAB_LIST_TYPE.ISSUE_TAB_LIST_ALL,
                    }, history);
                }
                break;
            case MENU_KEY_CREATE_BUG:
                if (projectStore.curProjectId != "") {
                    linkAuxStore.goToCreateBug(projectStore.curProjectId);
                }
                break;
            case MENU_KEY_SHOW_TOOL_BAR_TEST_CASE:
                if (projectStore.curProjectId != "") {
                    linkAuxStore.goToTestCaseList(history);
                }
                break;
            case MENU_KEY_CREATE_TEST_CASE:
                if (projectStore.curProjectId != "") {
                    projectModalStore.projectId = projectStore.curProjectId;
                    projectModalStore.setCreateTestCase(true, "", false);
                }
                break;
            case MENU_KEY_SHOW_TOOL_BAR_EVENTS:
                if (projectStore.curProjectId != "") {
                    linkAuxStore.goToEventList(history);
                }
                break;
            case MENU_KEY_ENTRY_CREATE_SPRIT:
                if (projectStore.curProjectId != "" && projectStore.isAdmin) {
                    entryStore.createEntryType = ENTRY_TYPE_SPRIT;
                }
                break;
            case MENU_KEY_ENTRY_CREATE_DOC:
                if (projectStore.curProjectId != "") {
                    entryStore.createEntryType = ENTRY_TYPE_DOC;
                }
                break;
            case MENU_KEY_ENTRY_CREATE_DRAW:
                if (projectStore.curProjectId != "") {
                    entryStore.createEntryType = ENTRY_TYPE_DRAW;
                }
                break;
            case MENU_KEY_ENTRY_CREATE_PAGES:
                if (projectStore.curProjectId != "") {
                    entryStore.createEntryType = ENTRY_TYPE_PAGES;
                }
                break;
            case MENU_KEY_ENTRY_CREATE_API_COLL:
                if (projectStore.curProjectId != "") {
                    entryStore.createEntryType = ENTRY_TYPE_API_COLL;
                }
                break;
            case MENU_KEY_SHOW_TOOL_BAR_OVERVIEW:
                if (projectStore.curProjectId != "") {
                    linkAuxStore.gotoOverview(history);
                }
                break;
            case MENU_KEY_SHOW_TOOL_BAR_RECYCLE:
                if (projectStore.curProjectId != "") {
                    linkAuxStore.gotoRecycle(history);
                }
                break;
            case MENU_KEY_SHOW_TOOL_BAR_CHAT_AND_COMMENT:
                if (projectStore.curProjectId != "") {
                    if (!projectStore.showChatAndComment) {
                        linkAuxStore.pickupToolbar(history);
                    }
                    projectStore.setShowChatAndComment(!projectStore.showChatAndComment, "bulletin");
                }
                break;
            case MENU_KEY_SHOW_HELP:
                if (projectStore.curProjectId != "") {
                    setShowHelp(oldValue => !oldValue);
                }
                break;
            case MENU_KEY_ORG_SHOW_INVITE_MEMBER:
                orgStore.showInviteMember = true;
                break;
            case MENU_KEY_ORG_SHOW_SETTING:
                orgStore.showUpdateOrgModal = true;
                break;
            case MENU_KEY_ORG_SHOW_EVAL_TARGET:
                orgStore.showEvalTargetModal = true;
                break;
            default:
                if (projectStore.curProjectId != "" && key.startsWith(MENU_KEY_HOME_PREFIX)) {
                    gotoHomePage(key);
                }
        }
        if (projectStore.curProjectId != "" && key.startsWith(MENU_KEY_MEMBER_PREFIX)) {
            const memberUserId = key.substring(MENU_KEY_MEMBER_PREFIX.length);
            memberStore.showDetailMemberId = memberUserId;
            projectStore.setShowChatAndComment(true, "member");
            linkAuxStore.pickupToolbar(history);
        }
    }

    const onMenuClick = async (info: MenuInfo) => {
        processMenuKey(info.key);
    }

    useHotkeys("alt+1", () => processMenuKey(MENU_KEY_HOME_WORK_PLAN));
    useHotkeys("alt+2", () => processMenuKey(MENU_KEY_HOME_KNOWLEDGE));
    useHotkeys("alt+3", () => processMenuKey(MENU_KEY_HOME_DATA_VIEW));
    useHotkeys("alt+m", () => processMenuKey(MENU_KEY_HOME_MYWORK));
    useHotkeys("alt+c", () => processMenuKey(MENU_KEY_SHOW_TOOL_BAR_CHAT_AND_COMMENT));
    useHotkeys("alt+i", () => processMenuKey(MENU_KEY_SHOW_TOOL_BAR_IDEA));
    useHotkeys("alt+q", () => processMenuKey(MENU_KEY_SHOW_TOOL_BAR_REQUIRE_MENT));
    useHotkeys("alt+t", () => processMenuKey(MENU_KEY_SHOW_TOOL_BAR_TASK_ALL));
    useHotkeys("alt+b", () => processMenuKey(MENU_KEY_SHOW_TOOL_BAR_BUG_ALL));
    useHotkeys("alt+e", () => processMenuKey(MENU_KEY_SHOW_TOOL_BAR_EVENTS));
    useHotkeys("alt+h", () => processMenuKey(MENU_KEY_SHOW_HELP));

    useHotkeys("ctrl+n", () => {
        setCreateFlag(true);
        setTimeout(() => {
            setCreateFlag(oldValue => {
                if (oldValue) {
                    if (projectStore.isAdmin) {
                        processMenuKey(MENU_KEY_ENTRY_CREATE_SPRIT);
                    } else {
                        processMenuKey(MENU_KEY_ENTRY_CREATE_DOC);
                    }
                }
                return false;
            });
        }, 1000);
    });

    useHotkeys("w", () => {
        setCreateFlag(oldValue => {
            if (oldValue) {
                processMenuKey(MENU_KEY_ENTRY_CREATE_SPRIT);
            }
            return false;
        });
    });

    useHotkeys("d", () => {
        setCreateFlag(oldValue => {
            if (oldValue) {
                processMenuKey(MENU_KEY_ENTRY_CREATE_DOC);
            }
            return false;
        });
    });

    useHotkeys("r", () => {
        setCreateFlag(oldValue => {
            if (oldValue) {
                processMenuKey(MENU_KEY_ENTRY_CREATE_DRAW);
            }
            return false;
        });
    });

    useHotkeys("p", () => {
        setCreateFlag(oldValue => {
            if (oldValue) {
                processMenuKey(MENU_KEY_ENTRY_CREATE_PAGES);
            }
            return false;
        });
    });

    useHotkeys("q", () => {
        setCreateFlag(oldValue => {
            if (oldValue) {
                processMenuKey(MENU_KEY_CREATE_REQUIRE_MENT);
            }
            return false;
        });
    });

    useHotkeys("t", () => {
        setCreateFlag(oldValue => {
            if (oldValue) {
                processMenuKey(MENU_KEY_CREATE_TASK);
            }
            return false;
        });
    });

    useHotkeys("b", () => {
        setCreateFlag(oldValue => {
            if (oldValue) {
                processMenuKey(MENU_KEY_CREATE_BUG);
            }
            return false;
        });
    });

    useHotkeys("a", () => {
        setCreateFlag(oldValue => {
            if (oldValue) {
                processMenuKey(MENU_KEY_ENTRY_CREATE_API_COLL);
            }
            return false;
        });
    });

    useEffect(() => {
        if (appStore.clientCfg !== undefined && appStore.vendorCfg !== undefined) {
            calcItems();
        }
    }, [
        appStore.lang,
        projectStore.curProject?.setting,
        projectStore.curProjectId,
        memberStore.memberList,
        orgStore.curOrgId,
        appStore.clientCfg,
        appStore.vendorCfg,
        userStore.sessionId,
        userStore.userInfo.featureInfo.enable_org,
        userStore.userInfo.featureInfo.enable_project,
        userStore.userInfo.featureInfo.enable_data_view,
        userStore.userInfo.featureInfo.enable_grow_center,
        orgStore.curOrg?.setting.enable_evaluate,
        projectStore.projectHome.contentEntryType]);

    return (
        <>
            {appStore.vendorCfg?.layout.show_quick_access && (
                <Dropdown overlayStyle={{ minWidth: "100px" }} menu={{ items, subMenuCloseDelay: 0.05, onClick: (info: MenuInfo) => onMenuClick(info) }} trigger={["click"]} >
                    <a onClick={(e) => e.preventDefault()} style={{ margin: "0px 10px", color: "orange", fontSize: "18px" }} title="快捷菜单">
                        <MenuOutlined />
                    </a>
                </Dropdown >
            )}
            {showHelp == true && (
                <Modal open title="快捷键帮助" footer={null} mask={false}
                    bodyStyle={{ height: "calc(100vh - 300px)", overflowY: "scroll", padding: "0px 0px" }}
                    onCancel={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setShowHelp(false);
                    }}>
                    <HotkeyHelpInfo />
                </Modal>
            )}
            {showLoginAccount == true && (
                <Modal title="登录账号" open footer={null}
                    mask={false}
                    onCancel={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        setshowLoginAccount(false);
                    }}>
                    登录账号:&nbsp;{userStore.userInfo.userName}&nbsp;&nbsp;<a onClick={e => {
                        e.stopPropagation();
                        e.preventDefault();
                        writeText(userStore.userInfo.userName);
                        message.info("复制成功");
                    }}>复制</a>
                </Modal>
            )}
        </>
    );
};

export default observer(QuickAccess);