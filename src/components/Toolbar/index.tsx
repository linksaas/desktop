//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { Badge, Divider, Popover } from 'antd';

import style from './index.module.less';
import { useStores } from '@/hooks';
import { observer } from 'mobx-react';
import { APP_PROJECT_HOME_PATH } from '@/utils/constant';
import { ISSUE_TYPE_BUG, ISSUE_TYPE_TASK } from '@/api/project_issue';
import { PROJECT_HOME_CONTENT_LIST } from '@/api/project';
import { ENTRY_TYPE_KNOWLEDGE, ENTRY_TYPE_SPRIT } from '@/api/project_entry';
import { useTranslation } from "react-i18next";


const Item: React.FC<{ id: string; pathname: string; title: string; showHelp: boolean, badge?: number, style?: React.CSSProperties }> = observer((props) => {
  const history = useHistory();

  const [hover, setHover] = useState(false);

  const current = props.pathname.includes(props.id);
  const gotoPage = (id: string) => {
    if (props.pathname.startsWith(APP_PROJECT_HOME_PATH)) {
      history.push(APP_PROJECT_HOME_PATH + "/" + id);
    }
  };

  return (
    <Popover overlayInnerStyle={{ backgroundColor: "orange" }}
      content={props.title}
      placement="left"
      open={hover || props.showHelp}
    >
      <div
        data-menu-id={props.id}
        className={current ? style.menuCurrent : style.menu}
        onClick={() => gotoPage(props.id)}
        onMouseEnter={e => {
          e.stopPropagation();
          e.preventDefault();
          setHover(true);
        }}
        onMouseLeave={e => {
          e.stopPropagation();
          e.preventDefault();
          setHover(false);
        }}
        style={props.style}
      >
        <Badge
          count={props.badge ?? 0}
          offset={[15, -18]}
          style={{ padding: ' 0   3px', height: '16px', lineHeight: '16px' }}
        />
      </div>
    </Popover>
  );
});

const Toolbar: React.FC = observer(() => {
  const location = useLocation();
  const pathname = location.pathname;

  const { t } = useTranslation();

  const projectStore = useStores('projectStore');
  const ideaStore = useStores("ideaStore");
  const projectModalStore = useStores('projectModalStore');
  const appStore = useStores('appStore');

  return (
    <div className={style.toolbar}>
      <Item
        id="idea"
        pathname={pathname}
        title={t("project.tool.knowledgePointRepo")}
        showHelp={ideaStore.showCreateIdea == true}
        style={(projectStore.projectHome.homeType == PROJECT_HOME_CONTENT_LIST && projectStore.projectHome.contentEntryType == ENTRY_TYPE_KNOWLEDGE) ? { backgroundColor: "#faf49f", borderRadius: "10px" } : undefined}
      />
      {((appStore.vendorCfg?.project.show_requirement_list_entry && projectStore.curProject?.setting.show_require_ment_list_entry) ||
        (appStore.vendorCfg?.project.show_task_list_entry && projectStore.curProject?.setting.show_task_list_entry) ||
        (appStore.vendorCfg?.project.show_bug_list_entry && projectStore.curProject?.setting.show_bug_list_entry) ||
        (appStore.vendorCfg?.project.show_testcase_list_entry && projectStore.curProject?.setting.show_test_case_list_entry)) && <Divider />}

      {(appStore.vendorCfg?.project.show_requirement_list_entry && projectStore.curProject?.setting.show_require_ment_list_entry) && (
        <Item
          id="req"
          pathname={pathname}
          title={t("project.tool.requireMentList")}
          showHelp={projectModalStore.createRequirement == true}
          style={(projectStore.projectHome.homeType == PROJECT_HOME_CONTENT_LIST && projectStore.projectHome.contentEntryType == ENTRY_TYPE_SPRIT) ? { backgroundColor: "#faf49f", borderRadius: "10px" } : undefined}
        />
      )}

      {(appStore.vendorCfg?.project.show_task_list_entry && projectStore.curProject?.setting.show_task_list_entry) && (
        <Item
          id="task"
          pathname={pathname}
          title={t("project.tool.taskList")}
          badge={projectStore.curProject?.project_status.undone_task_count || 0}
          showHelp={projectModalStore.createIssue == true && projectModalStore.createIssueType == ISSUE_TYPE_TASK}
          style={(projectStore.projectHome.homeType == PROJECT_HOME_CONTENT_LIST && projectStore.projectHome.contentEntryType == ENTRY_TYPE_SPRIT) ? { backgroundColor: "#faf49f", borderRadius: "10px" } : undefined}
        />
      )}

      {(appStore.vendorCfg?.project.show_bug_list_entry && projectStore.curProject?.setting.show_bug_list_entry) && (
        <Item
          id="bug"
          pathname={pathname}
          title={t("project.tool.bugList")}
          badge={projectStore.curProject?.project_status.undone_bug_count || 0}
          showHelp={projectModalStore.createIssue == true && projectModalStore.createIssueType == ISSUE_TYPE_BUG}
          style={(projectStore.projectHome.homeType == PROJECT_HOME_CONTENT_LIST && projectStore.projectHome.contentEntryType == ENTRY_TYPE_SPRIT) ? { backgroundColor: "#faf49f", borderRadius: "10px" } : undefined}
        />
      )}

      {(appStore.vendorCfg?.project.show_testcase_list_entry && projectStore.curProject?.setting.show_test_case_list_entry) && (
        <Item
          id="testcase"
          pathname={pathname}
          title={t("project.tool.testCaseList")}
          showHelp={false}
          style={(projectStore.projectHome.homeType == PROJECT_HOME_CONTENT_LIST && projectStore.projectHome.contentEntryType == ENTRY_TYPE_SPRIT) ? { backgroundColor: "#faf49f", borderRadius: "10px" } : undefined}
        />
      )}

      <Divider />

      <Item
        id="record"
        pathname={pathname}
        title={t("project.tool.eventList")}
        badge={projectStore.curProject?.project_status.new_event_count || 0}
        showHelp={false}
      />

      <Item
        id="webhook"
        pathname={pathname}
        title={t("project.tool.thirdpartEventList")}
        showHelp={false}
      />

      <Divider />
      <Item id="recycle" pathname={pathname} title={t("project.tool.recycle")} showHelp={false} />
      <Divider />
      <Item id="server" pathname={pathname} title={t("project.tool.serverList")} showHelp={false} />
      <Item id="overview" pathname={pathname} title={t("project.tool.projectInfo")} showHelp={false} />
    </div>
  );
});
export default Toolbar;
