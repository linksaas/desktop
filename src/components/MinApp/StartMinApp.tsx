//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { observer } from 'mobx-react';
import { useStores } from "@/hooks";
import { list_app as list_user_app, add_app as add_user_app } from "@/api/user_app";
import DownloadProgressModal from "@/components/MinApp/DownloadProgressModal";
import { get_app, type AppInfo } from "@/api/appstore";
import { check_unpark, get_min_app_path, start as start_app } from '@/api/min_app';
import { GLOBAL_APPSTORE_FS_ID, get_cache_file } from "@/api/fs";
import { request } from "@/utils/request";
import { get_global_server_addr } from "@/api/client_cfg";
import { uniqId } from "@/utils/utils";

interface DownloadInfo {
    fsId: string;
    fileId: string;
}

const StartMinApp = () => {
    const appStore = useStores('appStore');
    const userStore = useStores('userStore');

    const [appInfo, setAppInfo] = useState<AppInfo | null>(null);
    const [showDownload, setShowDownload] = useState<DownloadInfo | null>(null);

    const openUserApp = async (fsId: string, fileId: string) => {
        if (appInfo == null || appStore.openMinAppParam == null) {
            return;
        }

        const path = await get_min_app_path(fsId, fileId);
        await start_app({
            user_id: userStore.userInfo.userId,
            user_display_name: userStore.userInfo.displayName,
            label: `minApp:${appInfo.app_id}:${uniqId()}`,
            title: `${appInfo.base_info.app_name}(微应用)`,
            path: path,
            extra_info_name: appStore.openMinAppParam.extraInfoName,
            extra_info: appStore.openMinAppParam.extraInfo,
        }, appInfo?.app_perm);

        //清空openMinAppParam
        appStore.openMinAppParam = null;
    };

    const preOpenUserApp = async () => {
        //检查文件是否已经下载
        const res = await get_cache_file(GLOBAL_APPSTORE_FS_ID, appInfo?.file_id ?? "", "content.zip");
        if (res.exist_in_local == false) {
            setShowDownload({
                fsId: GLOBAL_APPSTORE_FS_ID,
                fileId: appInfo?.file_id ?? "",
            });
            return;
        }
        //检查是否已经解压zip包
        const ok = await check_unpark(GLOBAL_APPSTORE_FS_ID, appInfo?.file_id ?? "");
        if (!ok) {
            setShowDownload({
                fsId: GLOBAL_APPSTORE_FS_ID,
                fileId: appInfo?.file_id ?? "",
            });
            return;
        }
        //打开微应用
        await openUserApp(GLOBAL_APPSTORE_FS_ID, appInfo?.file_id ?? "");
    }

    const startMinApp = async () => {
        if (appInfo == null) {
            return;
        }

        //安装应用
        const appList = await request(list_user_app({
            session_id: userStore.sessionId,
        }));
        if (appStore.openMinAppParam == null) {
            return;
        }
        if (!appList.app_id_list.includes(appStore.openMinAppParam.minAppId)) {
            await request(add_user_app({
                session_id: userStore.sessionId,
                app_id: appStore.openMinAppParam.minAppId,
            }));
        }
        //打开应用
        await preOpenUserApp();
    };

    const loadAppInfo = async () => {
        if (appStore.openMinAppParam == null) {
            return;
        }
        const addr = await get_global_server_addr();
        const res = await request(get_app(addr, {
            app_id: appStore.openMinAppParam.minAppId,
            session_id: userStore.sessionId,
        }));
        setAppInfo(res.app_info);
    };

    useEffect(() => {
        startMinApp();
    }, [appInfo]);

    useEffect(() => {
        if (appStore.openMinAppParam == null) {
            setAppInfo(null);
            setShowDownload(null);
        } else {
            loadAppInfo();
        }
    }, [appStore.openMinAppParam]);

    return (
        <div>
            {showDownload != null && (
                <DownloadProgressModal fsId={showDownload.fsId} fileId={showDownload.fileId}
                    onCancel={() => setShowDownload(null)}
                    onOk={() => {
                        setShowDownload(null);
                        openUserApp(showDownload.fsId, showDownload.fileId);
                    }} />
            )}
        </div>
    );
};

export default observer(StartMinApp);