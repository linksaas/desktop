//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from "react";
import { check_un_read, type COMMENT_TARGET_TYPE } from "@/api/project_comment";
import { request } from "@/utils/request";
import { Badge } from "antd";
import { get_session } from "@/api/user";

export interface CommentTabProps {
    projectId: string;
    targetType: COMMENT_TARGET_TYPE;
    targetId: string;
    dataVersion: number;
}

const CommentTab = (props: CommentTabProps) => {
    const [hasUnRead, setHasUnRead] = useState(false);

    const checkHasUnread = async () => {
        const sessionId = await get_session();
        const res = await request(check_un_read({
            session_id: sessionId,
            project_id: props.projectId,
            target_type: props.targetType,
            target_id: props.targetId,
        }));
        setHasUnRead(res.has_un_read);
    };

    useEffect(() => {
        checkHasUnread();
    }, [props.targetId, props.dataVersion]);

    return (
        <Badge count={hasUnRead ? 1 : 0} dot={true}>
            评论&nbsp;&nbsp;
        </Badge>
    );
}

export default CommentTab;