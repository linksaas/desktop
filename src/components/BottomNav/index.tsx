//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import { Badge, Button, Segmented, Space } from 'antd';
import s from './index.module.less';
import {
  PROJECT_SETTING_TAB
} from '@/utils/constant';
import { useStores } from '@/hooks';
import { observer } from 'mobx-react';
import AlarmHeader from './AlarmHeader';
import ProjectTipList from './ProjectTipList';
import { MessageOutlined, SettingFilled, SolutionOutlined, StopOutlined, TeamOutlined, UserAddOutlined } from '@ant-design/icons';
import { useTranslation } from "react-i18next";


const RightFloat = observer(() => {
  const { t } = useTranslation();

  const projectStore = useStores('projectStore');
  const memberStore = useStores('memberStore');

  return (
    <div className={s.right_float}>
      <AlarmHeader />
      {projectStore.isAdmin && (
        <Space>
          {projectStore.isClosed == false && (
            <Button type={memberStore.memberList.length == 1 ? "primary" : "default"} icon={<UserAddOutlined />} style={{ borderRadius: "6px", marginRight: "10px", height: "24px", padding: "0px 10px" }}
              onClick={e => {
                e.stopPropagation();
                e.preventDefault();
                projectStore.setShowChatAndComment(true, "member");
                memberStore.showInviteMember = true;
              }}>{t("text.invite")}</Button>
          )}
          <Button type="link" className={s.setting_btn} onClick={e => {
            e.stopPropagation();
            e.preventDefault();
            projectStore.showProjectSetting = PROJECT_SETTING_TAB.PROJECT_SETTING_ALARM;
          }}><SettingFilled style={{ marginTop: "14px" }} /></Button>
        </Space>
      )}
    </div>
  );
});

const BottomNav = () => {
  const { t } = useTranslation();

  const projectStore = useStores('projectStore');

  const [segValue, setSegValue] = useState("");

  const calcSegValue = () => {
    if (projectStore.showChatAndComment == false) {
      return "";
    }
    if (projectStore.showChatAndCommentTab == "member") {
      return "member";
    } else if (projectStore.showChatAndCommentTab == "mywork") {
      return "mywork";
    }
    return "comment";
  };

  useEffect(() => {
    const newSegValue = calcSegValue();
    setSegValue(newSegValue);
  }, [projectStore.showChatAndComment, projectStore.showChatAndCommentTab]);

  return (
    <div className={s.nav}>
      <div className={s.left}>
        <Segmented options={[
          {
            label: "",
            value: "",
            icon: <StopOutlined />,
          },
          {
            label: t("project.bottom.member"),
            value: "member",
            icon: <TeamOutlined />,
          },
          {
            label: <Badge count={(projectStore.curProject?.project_status.bulletin_count ?? 0) + (projectStore.curProject?.project_status.unread_comment_count ?? 0)}
              offset={[-4, 4]} size='small' overflowCount={99}>
              <div style={{ paddingRight: "16px" }}>{t("project.bottom.communication")}</div>
            </Badge>,
            value: "comment",
            icon: <MessageOutlined />,
          },
          {
            label: t("project.bottom.myWork"),
            value: "mywork",
            icon: <SolutionOutlined />,
          }
        ]} size='large' style={{ marginRight: "10px", borderRadius: "10px" }} value={segValue}
          onChange={value => {
            if (value == "member") {
              projectStore.setShowChatAndComment(true, "member");
            } else if (value == "comment") {
              projectStore.setShowChatAndComment(true, "bulletin");
            } else if (value == "mywork") {
              projectStore.setShowChatAndComment(true, "mywork");
            } else {
              projectStore.setShowChatAndComment(false, "member");
            }
          }} />
        <ProjectTipList />
      </div>
      <div className={s.right}>
        <RightFloat />
      </div>
    </div>
  );
};

export default observer(BottomNav);
