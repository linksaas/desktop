//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useCallback, useRef } from 'react';
import { Remirror, useRemirror, EditorComponent } from '@remirror/react';
import { ThemeProvider } from '@remirror/react';
import Toolbar from './components/Toolbar';
import FloatToolBar from './components/FloatToolbar';
import { AllStyledComponent } from '@remirror/styles/emotion';
import { TableComponents } from '@remirror/extension-react-tables';
import {
  historyItem,
  clipboardItem,
  headItem,
  listItem,
  newCommItem,
  pubresWidgetItem,
} from './components/ToolbarItem';

import type { EditorRef } from './common';
import { ImperativeHandle } from './common';
import type { RemirrorContentType } from '@remirror/core';

import type { FILE_OWNER_TYPE } from '@/api/fs';
import { getExtensions } from './extensions';
import type { InvalidContentHandler, RemirrorJSON } from 'remirror';
import type { TocInfo } from './extensions/index';
import TableCellMenu from './components/TableCellMenu';
import type { EventsOptions } from '@remirror/extension-events';


export interface UseCommonEditorAttrs {
  content: RemirrorContentType;
  fsId: string;
  ownerType: FILE_OWNER_TYPE;
  ownerId: string;
  historyInToolbar: boolean;
  clipboardInToolbar: boolean;
  commonInToolbar: boolean;
  pubResInToolbar: boolean;
  tocCallback?: (tocList: TocInfo[]) => void;
  eventsOption?: EventsOptions;
  placeholder?: string;
  hideToolbar?: boolean;
}

export const useCommonEditor = (attrs: UseCommonEditorAttrs) => {

  let newContent = attrs.content;
  if (typeof newContent == 'string') {
    try {
      newContent = JSON.parse(newContent) as RemirrorJSON;
      if (newContent.type == undefined) {
        newContent.type = "doc";
      }
    } catch (err) { }
  }
  const onError: InvalidContentHandler = useCallback(({ json, invalidContent, transformers }) => {
    return transformers.remove(json, invalidContent);
  }, []);

  const { manager, state } = useRemirror({
    extensions: getExtensions({
      fsId: attrs.fsId,
      thumbWidth: 200,
      thumbHeight: 150,
      ownerType: attrs.ownerType,
      ownerId: attrs.ownerId,
      tocCallback: attrs.tocCallback,
      eventsOption: attrs.eventsOption,
    }),
    content: newContent,
    stringHandler: 'html',
    onError: onError,
  });

  const editorRef = useRef<EditorRef | null>(null);
  const toolbarItems = [];
  if (attrs.historyInToolbar) {
    toolbarItems.push(historyItem);
  }
  if (attrs.clipboardInToolbar) {
    toolbarItems.push(clipboardItem);
  }
  toolbarItems.push(
    ...[
      headItem,
      listItem,
    ],
  );
  if (attrs.commonInToolbar) {
    toolbarItems.push(newCommItem({
      fsId: attrs.fsId,
      thumbWidth: 200,
      thumbHeight: 150,
      ownerType: attrs.ownerType,
      ownerId: attrs.ownerId,
    }));
  }
  if (attrs.pubResInToolbar) {
    toolbarItems.push(pubresWidgetItem());
  }

  const editor = (
    <ThemeProvider>
      <AllStyledComponent>
        <Remirror manager={manager} initialContent={state} placeholder={attrs?.placeholder ?? "请输入......"}>
          {(attrs.hideToolbar == undefined || attrs.hideToolbar == false) && (
            <Toolbar items={toolbarItems} />
          )}
          <FloatToolBar />
          <ImperativeHandle ref={editorRef} />
          <EditorComponent />
          <TableComponents tableCellMenuProps={{ Component: TableCellMenu }} />
        </Remirror>
      </AllStyledComponent>
    </ThemeProvider>
  );
  return {
    editor,
    editorRef,
  };
};
