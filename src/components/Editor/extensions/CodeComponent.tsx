//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React, { useEffect, useState } from 'react';
import type { NodeViewComponentProps } from '@remirror/react';
import { useCommands } from '@remirror/react';
import CodeEditor from '@uiw/react-textarea-code-editor';
import { Select } from 'antd';
import EditorWrap from '../components/EditorWrap';
import style from './common.module.less';
import { ErrorBoundary } from '@/components/ErrorBoundary';
import { LANG_LIST } from '@/utils/constant';

export type EditCodeProps = NodeViewComponentProps & {
  lang: string;
  code: string;
};

let _lastLang = "json";

export function getLastLang(): string {
  return _lastLang;
}

export function setLastLang(val: string) {
  _lastLang = val;
}

export const EditCode: React.FC<EditCodeProps> = (props) => {
  const [lang, setLang] = useState(props.lang);
  const [code, setCode] = useState(props.code);

  const { deleteCode } = useCommands();

  const removeNode = () => {
    deleteCode((props.getPosition as () => number)());
  };

  useEffect(() => {
    props.updateAttributes({
      lang: lang,
      code: code,
    });
  }, [lang, code]);

  return (
    <ErrorBoundary>
      <EditorWrap onChange={() => removeNode()}>
        <div className={style.selectHd}>
          <Select
            value={lang}
            showSearch
            onChange={(value) => {
              setLang(value);
              setLastLang(value);
            }}
          >
            {LANG_LIST.map((item) => (
              <Select.Option key={item} value={item}>
                {item}
              </Select.Option>
            ))}
          </Select>
        </div>

        <CodeEditor
          value={code}
          language={lang}
          minHeight={200}
          placeholder="请输入代码"
          onChange={(e) => {
            e.stopPropagation();
            e.preventDefault();
            setCode(e.target.value);
          }}
          style={{
            fontSize: 14,
            backgroundColor: '#f5f5f5',
          }}
        />
      </EditorWrap>
    </ErrorBoundary>
  );
};

export type ViewCodeProps = NodeViewComponentProps & {
  lang: string;
  code: string;
};

export const ViewCode: React.FC<ViewCodeProps> = (props) => {
  return (
    <ErrorBoundary>
      <EditorWrap>
        <CodeEditor
          value={props.code}
          language={props.lang}
          disabled
          style={{
            fontSize: 14,
            backgroundColor: '#f5f5f5',
          }}
        />
      </EditorWrap>
    </ErrorBoundary>
  );
};
