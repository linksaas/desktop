//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from 'react';
import type { NodeViewComponentProps } from '@remirror/react';
import * as widgets from '../widgets';
import { useCommands } from '@remirror/react';

export type WidgetProps = NodeViewComponentProps & {
  widgetType: widgets.WIDGET_TYPE;
  widgetData: unknown;
};

export const Widget: React.FC<WidgetProps> = (props) => {
  const { deleteWidget } = useCommands();

  const widgetProps: widgets.WidgetProps = {
    editMode: props.view.editable,
    initData: props.widgetData,
    removeSelf: () => {
      deleteWidget((props.getPosition as () => number)());
    },
    writeData: (data: unknown) => {
      props.updateAttributes({
        widgetType: props.widgetType,
        widgetData: data,
      });
    },
  };
  switch (props.widgetType) {
    default: {
      return <widgets.UnkwownWidget {...widgetProps} />
    }
  }
};
