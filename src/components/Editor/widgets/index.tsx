//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

export type { WidgetProps } from './common';
export { UnkwownWidget } from "./UnkwownWidget";


export type WIDGET_TYPE = string;
export const WIDGET_TYPE_UNKWOWN: WIDGET_TYPE = "unkwown";


export const WidgetTypeList: WIDGET_TYPE[] = [
    WIDGET_TYPE_UNKWOWN,
];


export const WidgetInitDataMap: Map<WIDGET_TYPE, unknown> = new Map();

WidgetInitDataMap.set(WIDGET_TYPE_UNKWOWN, {});