//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from 'react';
import { type WidgetProps } from './common';
import EditorWrap from '../components/EditorWrap';
import { ErrorBoundary } from '@/components/ErrorBoundary';

const EditUnkwown: React.FC<WidgetProps> = (props) => {
    return (
        <ErrorBoundary>
            <EditorWrap onChange={() => props.removeSelf()}>
                <div style={{ height: "60px", marginLeft: "40px", paddingTop: "20px" }}>未知插件</div>
            </EditorWrap>
        </ErrorBoundary>
    );
};

const ViewUnkwown: React.FC<WidgetProps> = (props) => {
    return (
        <span>未知插件</span>
    );
}

export const UnkwownWidget: React.FC<WidgetProps> = (props) => {
    if (props.editMode) {
        return <EditUnkwown {...props} />;
    } else {
        return <ViewUnkwown {...props} />;
    }
};