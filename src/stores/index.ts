//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import MemberStore from './member';
import AppStore from './app';
import ProjectStore from './project';
import UserStore from './user';
import NoticeStore from './notice';
import LinkAuxStore from './linkAux';
import IdeaStore from './idea';
import PubResStore from './pubRes';
import EntryStore from './entry';
import OrgStore from "./org";
import LocalRepoStore from './localrepo';
import { ProjectModalStore } from './project_modal';

export class RootStore {
  userStore: UserStore;
  projectStore: ProjectStore;
  appStore: AppStore;
  memberStore: MemberStore;
  noticeStore: NoticeStore;
  linkAuxStore: LinkAuxStore;
  ideaStore: IdeaStore;
  pubResStore: PubResStore;
  entryStore: EntryStore;
  orgStore: OrgStore;
  localRepoStore: LocalRepoStore;
  projectModalStore: ProjectModalStore;

  constructor() {
    this.userStore = new UserStore(this);
    this.projectStore = new ProjectStore(this);
    this.appStore = new AppStore(this);
    this.memberStore = new MemberStore(this);
    this.noticeStore = new NoticeStore(this);
    this.linkAuxStore = new LinkAuxStore(this);
    this.ideaStore = new IdeaStore(this);
    this.pubResStore = new PubResStore();
    this.entryStore = new EntryStore(this);
    this.orgStore = new OrgStore(this);
    this.localRepoStore = new LocalRepoStore();
    this.projectModalStore = new ProjectModalStore();
  }
}

const rootStore = new RootStore();
const _store = {
  userStore: rootStore.userStore,
  projectStore: rootStore.projectStore,
  appStore: rootStore.appStore,
  memberStore: rootStore.memberStore,
  noticeStore: rootStore.noticeStore,
  linkAuxStore: rootStore.linkAuxStore,
  ideaStore: rootStore.ideaStore,
  pubResStore: rootStore.pubResStore,
  entryStore: rootStore.entryStore,
  orgStore: rootStore.orgStore,
  localRepoStore: rootStore.localRepoStore,
  projectModalStore: rootStore.projectModalStore,
};

export type StoreType = typeof _store;

export default _store;
