//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import type { RootStore } from './index';
import { makeAutoObservable, runInAction } from 'mobx';
import type { ProjectInfo, TagInfo } from '@/api/project';
import { list as listProject, get_project as getProject, list_tag, TAG_SCOPRE_ALL, set_weight, PROJECT_HOME_CONTENT_LIST } from '@/api/project';
import { request } from '@/utils/request';
import type { PROJECT_SETTING_TAB } from '@/utils/constant';
import { APP_PROJECT_HOME_PATH } from '@/utils/constant';
import { get_member_state as get_my_issue_state } from '@/api/project_issue';
import type { History } from 'history';
import { get_alarm_state } from "@/api/project_alarm";
import { list_key as list_bulletin_key } from "@/api/project_bulletin";
import { get_un_read_state } from "@/api/project_comment";
import { ProjectHomeStore } from './project_home';
import { ENTRY_TYPE_SPRIT } from '@/api/project_entry';
import type { REVIEW_TYPE, SimpleReviewInfo } from '@/api/project_review';

export class WebProjectStatus {
  constructor(projectId: string, projectStore: ProjectStore) {
    this.projectId = projectId;
    this.projectStore = projectStore;
    makeAutoObservable(this);
  }
  private projectId: string;
  private projectStore: ProjectStore;

  undone_task_count: number = 0;
  undone_bug_count: number = 0;
  new_event_count: number = 0; //启动软件以来的新事件数
  alarm_hit_count: number = 0;
  alarm_alert_count: number = 0;
  bulletin_count: number = 0;
  unread_comment_count: number = 0;

  get total_count(): number {
    const projectInfo = this.projectStore.getProject(this.projectId);
    let result = (
      this.new_event_count +
      this.alarm_hit_count +
      this.alarm_alert_count +
      this.bulletin_count +
      this.unread_comment_count
    );
    if (projectInfo?.setting.show_task_list_entry) {
      result += this.undone_task_count;
    }
    if (projectInfo?.setting.show_bug_list_entry) {
      result += this.undone_bug_count;
    }
    return result;
  }
}

export type WebProjectInfo = ProjectInfo & {
  project_status: WebProjectStatus;
  tag_list: TagInfo[];
};

export default class ProjectStore {
  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
    makeAutoObservable(this);
  }
  rootStore: RootStore;
  //当前项目
  private _curProjectId: string = '';
  //全部项目
  private _projectList: WebProjectInfo[] = [];

  public projectHome = new ProjectHomeStore();

  async setCurProjectId(val: string) {
    const oldProjectId = this._curProjectId;
    if (val == oldProjectId) {
      return;
    }
    runInAction(() => {
      this._curProjectId = val;
    });
    if (val !== '' && val != oldProjectId) {
      this.rootStore.entryStore.reset();
      this.rootStore.memberStore.showDetailMemberId = "";
      await this.rootStore.memberStore.loadMemberList(val);
      this.rootStore.ideaStore.searchKeywords = [];
      this.rootStore.ideaStore.curIdeaGroupId = "";
      this.rootStore.ideaStore.curIdeaId = "";

      this.setCodeCommentInfo("", "");
      this.showProjectSetting = null;
      this.setShowChatAndComment(false, "bulletin");
      this.projectHome.contentEntryType = ENTRY_TYPE_SPRIT;
      this.projectHome.homeType = PROJECT_HOME_CONTENT_LIST;

      this._curReview = null;
      this._curReviewPage = 0;
      this._filterReviewByTitleKeyword = "";
      this._filterReviewByType = null;
    }
  }

  get curProjectId(): string {
    return this._curProjectId;
  }

  get projectList(): WebProjectInfo[] {
    return this._projectList;
  }

  get curProject(): WebProjectInfo | undefined {
    return this._projectList.find(item => item.project_id == this._curProjectId);
  }

  getProject(projectId: string): WebProjectInfo | undefined {
    return this._projectList.find(item => item.project_id == projectId);
  }

  reset() {
    runInAction(() => {
      this._curProjectId = "";
      this._projectList = [];
    });
  }

  async initLoadProjectList() {
    const prjListRes = await request(listProject(this.rootStore.userStore.sessionId, false, false));
    const prjList: WebProjectInfo[] = prjListRes.info_list.map((info) => {
      return makeAutoObservable({
        ...info,
        project_status: new WebProjectStatus(info.project_id, this),
        tag_list: [],
      });
    });

    runInAction(() => {
      this._projectList = prjList;
    });
    //更新项目状态
    const projectIdList = this._projectList.map((item) => item.project_id);
    projectIdList.forEach(async (projectId: string) => {
      const status = await this.clacProjectStatus(projectId);
      const tagList = await this.listTag(projectId);
      runInAction(() => {
        const index = this._projectList.findIndex((item) => item.project_id == projectId);
        if (index != -1) {
          this._projectList[index].project_status = status;
          this._projectList[index].tag_list = tagList;
        }
      });
    });
  }

  private async clacProjectStatus(projectId: string): Promise<WebProjectStatus> {
    const status = new WebProjectStatus(projectId, this);
    const prjInfo = this.getProject(projectId);
    if (prjInfo !== undefined) {
      status.new_event_count = prjInfo.project_status.new_event_count;
    }
    const issueStateRes = await request(
      get_my_issue_state(this.rootStore.userStore.sessionId, projectId),
    );
    if (issueStateRes) {
      status.undone_bug_count =
        issueStateRes.member_state.bug_un_check_count +
        issueStateRes.member_state.bug_un_exec_count;
      status.undone_task_count =
        issueStateRes.member_state.task_un_check_count +
        issueStateRes.member_state.task_un_exec_count;
    }

    const alarmRes = await request(get_alarm_state({
      session_id: this.rootStore.userStore.sessionId,
      project_id: projectId,
    }));
    status.alarm_hit_count = alarmRes.hit_count;
    status.alarm_alert_count = alarmRes.alert_count;

    const bulletinRes = await request(list_bulletin_key({
      session_id: this.rootStore.userStore.sessionId,
      project_id: projectId,
      list_un_read: true,
      offset: 0,
      limit: 1,
    }));
    status.bulletin_count = bulletinRes.total_count;

    const unReadRes = await request(get_un_read_state({
      session_id: this.rootStore.userStore.sessionId,
      project_id: projectId,
    }));
    status.unread_comment_count = unReadRes.un_read_count;

    return status;
  }

  private async listTag(projectId: string): Promise<TagInfo[]> {
    const res = await request(list_tag({
      session_id: this.rootStore.userStore.sessionId,
      project_id: projectId,
      tag_scope_type: TAG_SCOPRE_ALL,
    }));
    return res.tag_info_list;
  }

  async updateTagList(projectId: string) {
    const tagList = await this.listTag(projectId);
    runInAction(() => {
      const index = this._projectList.findIndex((item) => item.project_id == projectId);
      if (index != -1) {
        this._projectList[index].tag_list = tagList;
      }
    });
  }

  async updateAlarmStatus(projectId: string) {
    const res = await request(get_alarm_state({
      session_id: this.rootStore.userStore.sessionId,
      project_id: projectId,
    }));
    runInAction(() => {
      const index = this._projectList.findIndex((item) => item.project_id == projectId);
      if (index != -1) {
        this._projectList[index].project_status.alarm_hit_count = res.hit_count;
        this._projectList[index].project_status.alarm_alert_count = res.alert_count;
      }
    });
  }

  async updateBulletinStatus(projectId: string) {
    const res = await request(list_bulletin_key({
      session_id: this.rootStore.userStore.sessionId,
      project_id: projectId,
      list_un_read: true,
      offset: 0,
      limit: 1,
    }));
    runInAction(() => {
      const index = this._projectList.findIndex((item) => item.project_id == projectId);
      if (index != -1) {
        this._projectList[index].project_status.bulletin_count = res.total_count;
      }
    });
  }

  async updateUnReadCommentStatus(projectId: string) {
    const res = await request(get_un_read_state({
      session_id: this.rootStore.userStore.sessionId,
      project_id: projectId,
    }));
    runInAction(() => {
      const index = this._projectList.findIndex((item) => item.project_id == projectId);
      if (index != -1) {
        this._projectList[index].project_status.unread_comment_count = res.un_read_count;
      }
    });
  }

  async updateProjectIssueCount(projectId: string) {
    const issueStateRes = await request(
      get_my_issue_state(this.rootStore.userStore.sessionId, projectId),
    );
    if (issueStateRes) {
      const undoneBugCount =
        issueStateRes.member_state.bug_un_check_count +
        issueStateRes.member_state.bug_un_exec_count;
      const undoneTaskCount =
        issueStateRes.member_state.task_un_check_count +
        issueStateRes.member_state.task_un_exec_count;
      runInAction(() => {
        const index = this._projectList.findIndex((item) => item.project_id == projectId);
        if (index != -1) {
          this._projectList[index].project_status.undone_bug_count = undoneBugCount;
          this._projectList[index].project_status.undone_task_count = undoneTaskCount;
        }
      });
    }
  }

  async updateWeight(projectId: string, weight: number) {
    await request(set_weight({
      session_id: this.rootStore.userStore.sessionId,
      project_id: projectId,
      weight: weight,
    }));
    let tmpList = this._projectList.slice();
    const index = tmpList.findIndex(item => item.project_id == projectId);
    if (index == -1) {
      return;
    }

    runInAction(() => {
      tmpList[index].my_weight = weight;
      tmpList = tmpList.sort((a, b) => b.my_weight - a.my_weight);
      this._projectList = tmpList;
    });
  }

  addNewEventCount(projectId: string) {
    let oldCount = 0;
    const prj = this.getProject(projectId);
    if (prj == undefined) {
      return;
    }
    oldCount = prj.project_status.new_event_count;
    runInAction(() => {
      const index = this._projectList.findIndex((item) => item.project_id == projectId);
      const newCount = oldCount + 1;
      if (index != -1) {
        this._projectList[index].project_status.new_event_count = newCount;
      }
    });
  }

  clearNewEventCount(projectId: string) {
    runInAction(() => {
      const index = this._projectList.findIndex((item) => item.project_id == projectId);
      if (index != -1) {
        this._projectList[index].project_status.new_event_count = 0;
      }
    });
  }

  async updateProject(projectId: string) {
    const res = await request(getProject(this.rootStore.userStore.sessionId, projectId));
    if (res) {
      const status = await this.clacProjectStatus(projectId);
      const tagList = await this.listTag(projectId);

      runInAction(() => {
        const tmpList = this._projectList.slice();
        const index = tmpList.findIndex((item) => item.project_id == projectId);

        if (index == -1) {
          tmpList.unshift(
            makeAutoObservable({
              ...res.info, project_status: status, tag_list: tagList,
            }));
        } else {
          tmpList[index] = makeAutoObservable({
            ...res.info, project_status: status, tag_list: tagList,
          });
        }
        this._projectList = tmpList;
      });
    }
  }


  removeProject(projecId: string, history?: History) {
    const tmpList = this._projectList.filter((item) => item.project_id != projecId);
    let newProjectId = "";
    runInAction(() => {
      this._projectList = tmpList;
      if (this._curProjectId == projecId) {
        if (tmpList.length > 0) {
          newProjectId = tmpList[0].project_id;
        }
      }
    });
    if (history == undefined) {
      return;
    }
    if (newProjectId == "") {
      history.push('/app/workbench');
    } else {
      this.setCurProjectId(newProjectId);
      this.projectHome.homeType = PROJECT_HOME_CONTENT_LIST;
      history.push(APP_PROJECT_HOME_PATH);
    }
  }

  get isAdmin(): boolean {
    const curProject = this.curProject;
    if (curProject !== undefined) {
      return curProject.user_project_perm.can_admin;
    }

    return false;
  }

  get isClosed(): boolean {
    const curProject = this.curProject;
    if (curProject == undefined) {
      return false;
    }
    return curProject.closed;
  }

  //显示代码评论
  private _codeCommentThreadId = "";
  private _codeCommentId = "";

  setCodeCommentInfo(threadId: string, commentId: string) {
    runInAction(() => {
      this._codeCommentThreadId = threadId;
      this._codeCommentId = commentId;
    });
  }

  get codeCommentThreadId(): string {
    return this._codeCommentThreadId;
  }

  get codeCommentId(): string {
    return this._codeCommentId;
  }

  //显示项目设置
  private _showProjectSetting: PROJECT_SETTING_TAB | null = null;

  get showProjectSetting(): PROJECT_SETTING_TAB | null {
    return this._showProjectSetting;
  }

  set showProjectSetting(val: PROJECT_SETTING_TAB | null) {
    runInAction(() => {
      this._showProjectSetting = val;
    });
  }


  //显示沟通和评论
  private _showChatAndComment: boolean = false;
  private _showChatAndCommentTab: "comment" | "bulletin" | "member" | "codecomment" | "mywork" | "review" = "bulletin";

  get showChatAndComment(): boolean {
    return this._showChatAndComment;
  }

  get showChatAndCommentTab(): "comment" | "bulletin" | "member" | "codecomment" | "mywork" | "review" {
    return this._showChatAndCommentTab;
  }

  setShowChatAndComment(val: boolean, tab: "comment" | "bulletin" | "member" | "codecomment" | "mywork" | "review") {
    runInAction(() => {
      this._showChatAndComment = val;
      this._showChatAndCommentTab = tab;
    });
  }

  //项目复盘相关
  private _curReview: SimpleReviewInfo | null = null;
  private _curReviewPage = 0;
  private _filterReviewByType: REVIEW_TYPE | null = null;
  private _filterReviewByTitleKeyword = "";

  get curReview() {
    return this._curReview;
  }

  set curReview(val: SimpleReviewInfo | null) {
    runInAction(() => {
      this._curReview = val;
    });
  }

  get curReviewPage() {
    return this._curReviewPage;
  }

  set curReviewPage(val: number) {
    runInAction(() => {
      this._curReviewPage = val;
    });
  }

  get filterReviewByType() {
    return this._filterReviewByType;
  }

  set filterReviewByType(val: REVIEW_TYPE | null) {
    runInAction(() => {
      this._filterReviewByType = val;
    });
  }

  get filterReviewByTitleKeyword() {
    return this._filterReviewByTitleKeyword;
  }

  set filterReviewByTitleKeyword(val: string) {
    runInAction(() => {
      this._filterReviewByTitleKeyword = val;
    });
  }
}
