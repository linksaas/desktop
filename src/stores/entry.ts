//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import type { RootStore } from './index';
import { makeAutoObservable, runInAction } from 'mobx';
import type { EntryInfo, ENTRY_TYPE } from "@/api/project_entry";
import { get as get_entry } from "@/api/project_entry";
import { request } from '@/utils/request';
import { WATCH_TARGET_ENTRY, unwatch, watch } from '@/api/project_watch';

export default class EntryStore {
    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
        makeAutoObservable(this);
    }
    rootStore: RootStore;

    private _editEntryId = "";
    private _entryList: EntryInfo[] = [];
    private _createEntryType: ENTRY_TYPE | null = null;

    reset() {
        runInAction(() => {
            this._editEntryId = "";
            this._createEntryType = null;
        });
    }

    get editEntryId(): string {
        return this._editEntryId;
    }

    set editEntryId(val: string) {
        runInAction(() => {
            this._editEntryId = val;
        });
    }

    get entryList(): EntryInfo[] {
        return this._entryList;
    }

    set entryList(val: EntryInfo[]) {
        runInAction(() => {
            this._entryList = val;
        });
    }

    get createEntryType(): ENTRY_TYPE | null {
        return this._createEntryType;
    }

    set createEntryType(val: ENTRY_TYPE | null) {
        runInAction(() => {
            this._createEntryType = val;
        });
    }

    async onUpdateEntry(entryId: string) {
        const res = await request(get_entry({
            session_id: this.rootStore.userStore.sessionId,
            project_id: this.rootStore.projectStore.curProjectId,
            entry_id: entryId,
        }));
        runInAction(() => {
            const tmpList = this._entryList.slice();
            const index = tmpList.findIndex(item => item.entry_id == entryId);
            if (index != -1) {
                tmpList[index] = res.entry;
                this._entryList = tmpList;
            }
        });
    }

    async onRemoveEntry(entryId: string) {
        runInAction(() => {
            const tmpList = this._entryList.filter(item => item.entry_id != entryId);
            this._entryList = tmpList;
        });
    }

    async unwatchEntry(entryId: string) {
        await request(unwatch({
            session_id: this.rootStore.userStore.sessionId,
            project_id: this.rootStore.projectStore.curProjectId,
            target_type: WATCH_TARGET_ENTRY,
            target_id: entryId,
        }));
        await this.onUpdateEntry(entryId);
    }

    async watchEntry(entryId: string) {
        await request(watch({
            session_id: this.rootStore.userStore.sessionId,
            project_id: this.rootStore.projectStore.curProjectId,
            target_type: WATCH_TARGET_ENTRY,
            target_id: entryId,
        }));
        await this.onUpdateEntry(entryId);
    }
}