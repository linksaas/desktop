//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { makeAutoObservable, runInAction } from 'mobx';
import type * as NoticeType from '@/api/notice_type';
import { listen } from '@tauri-apps/api/event';
import type { UnlistenFn } from '@tauri-apps/api/event';
import type { RootStore } from '.';
import type { ShortNoteEvent } from '@/utils/short_note';
import { showShortNote } from '@/utils/short_note';
import { SHORT_NOTE_TASK, SHORT_NOTE_BUG, SHORT_NOTE_MODE_DETAIL, SHORT_NOTE_MODE_SHOW } from '@/api/short_note';
import { LinkBugInfo, LinkEntryInfo, LinkTaskInfo } from './linkAux';
import { isString } from 'lodash';
import type { History } from 'history';
import { createBrowserHistory } from 'history';
import { appWindow } from '@tauri-apps/api/window';
import { request } from '@/utils/request';
import { get as get_issue } from '@/api/project_issue';
import { message } from 'antd';
import { get_session, USER_TYPE_ATOM_GIT, USER_TYPE_GIT_CODE, USER_TYPE_GITEE } from '@/api/user';
import { report_error } from "@/api/client_cfg";
import { WATCH_TARGET_ENTRY } from '@/api/project_watch';

class NoticeStore {
  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
    makeAutoObservable(this);
  }
  private rootStore: RootStore;


  private history: History = createBrowserHistory();

  setHistory(history: History) {
    runInAction(() => {
      this.history = history;
    });
  }

  //成功登录后接收通知
  async initListen(): Promise<UnlistenFn[]> {
    const unlistenFn = await listen<NoticeType.AllNotice>('notice', (ev) => {
      try {
        const notice = ev.payload
        console.log("notice", notice);
        if (notice.UserNotice !== undefined) {
          this.processUserNotice(notice.UserNotice);
        } else if (notice.ProjectNotice !== undefined) {
          this.processProjectNotice(notice.ProjectNotice);
        } else if (notice.IssueNotice !== undefined) {
          this.processIssueNotice(notice.IssueNotice);
        } else if (notice.EntryNotice !== undefined) {
          this.processEntryNotice(notice.EntryNotice);
        } else if (notice.ClientNotice !== undefined) {
          this.processClientNotice(notice.ClientNotice);
        } else if (notice.CommentNotice !== undefined) {
          this.processCommentNotice(notice.CommentNotice);
        } else if (notice.OrgNotice !== undefined) {
          this.processOrgNotice(notice.OrgNotice);
        }
      } catch (e) {
        console.log(e);
        report_error({
          err_data: `${e}`,
        });
      }
    });

    const unlistenShortNoteFn = await listen<ShortNoteEvent | string>("shortNote", (ev) => {
      try {
        if (isString(ev.payload)) {
          this.processShortNoteEvent(JSON.parse(ev.payload));
        } else {
          this.processShortNoteEvent(ev.payload);
        }
      } catch (e) {
        console.log(e);
      }

    });
    return [unlistenFn, unlistenShortNoteFn];
  }


  private processCommentNotice(notice: NoticeType.comment.AllNotice) {
    if (notice.AddCommentNotice !== undefined) {
      this.rootStore.projectStore.updateUnReadCommentStatus(notice.AddCommentNotice.project_id);
    } else if (notice.UpdateCommentNotice !== undefined) {
    } else if (notice.RemoveCommentNotice !== undefined) {
    } else if (notice.RemoveUnReadNotice !== undefined) {
      this.rootStore.projectStore.updateUnReadCommentStatus(notice.RemoveUnReadNotice.project_id);
    }
  }

  private async processOrgNotice(notice: NoticeType.org.AllNotice) {
    if (notice.JoinOrgNotice !== undefined) {
      this.rootStore.orgStore.onJoin(notice.JoinOrgNotice.org_id, notice.JoinOrgNotice.member_user_id);
    } else if (notice.LeaveOrgNotice !== undefined) {
      this.rootStore.orgStore.onLeave(notice.LeaveOrgNotice.org_id, notice.LeaveOrgNotice.member_user_id, this.history);
    } else if (notice.UpdateOrgNotice !== undefined) {
      this.rootStore.orgStore.initLoadOrgList();
    } else if (notice.CreateDepartMentNotice !== undefined) {
      this.rootStore.orgStore.onUpdateDepartMent(notice.CreateDepartMentNotice.org_id, notice.CreateDepartMentNotice.depart_ment_id);
    } else if (notice.RemoveDepartMentNotice !== undefined) {
      this.rootStore.orgStore.onRemoveDepartMent(notice.RemoveDepartMentNotice.org_id, notice.RemoveDepartMentNotice.depart_ment_id);
    } else if (notice.UpdateDepartMentNotice !== undefined) {
      this.rootStore.orgStore.onUpdateDepartMent(notice.UpdateDepartMentNotice.org_id, notice.UpdateDepartMentNotice.depart_ment_id);
    } else if (notice.UpdateMemberNotice !== undefined) {
      this.rootStore.orgStore.onUpdateMember(notice.UpdateMemberNotice.org_id, notice.UpdateMemberNotice.member_user_id);
    }
  }


  private async processClientNotice(notice: NoticeType.client.AllNotice) {
    if (this.rootStore.appStore.inEdit) { //编辑状态下忽略通知
      return;
    }
    if (notice.WrongSessionNotice !== undefined) {
      if (this.rootStore.userStore.adminSessionId != "") {
        runInAction(() => {
          this.rootStore.userStore.adminSessionId = "";
        });
        this.history.push("/");
      } else {
        const sessInRust = await get_session();
        if (sessInRust == "") {
          this.rootStore.userStore.logout();
        }
        message.warn("会话失效");
      }
    } else if (notice.StartMinAppNotice !== undefined) {
      await appWindow.show();
      await appWindow.unminimize();
      await appWindow.setAlwaysOnTop(true);
      setTimeout(() => {
        appWindow.setAlwaysOnTop(false);
      }, 200);
      this.rootStore.appStore.openMinAppParam = {
        minAppId: notice.StartMinAppNotice.min_app_id,
        extraInfo: '""',
        extraInfoName: '""',
      };
    } else if (notice.OpenLinkInfoNotice !== undefined) {
      this.rootStore.linkAuxStore.goToLink(notice.OpenLinkInfoNotice.link_info, this.history);
    } else if (notice.OpenEntryNotice != undefined) {
      this.rootStore.linkAuxStore.goToLink(new LinkEntryInfo("", notice.OpenEntryNotice.project_id, notice.OpenEntryNotice.entry_id), this.history);
    } else if (notice.NewExtraTokenNotice !== undefined) {
      this.rootStore.userStore.updateExtraToken(notice.NewExtraTokenNotice.extra_token);
    } else if (notice.AtomGitLoginNotice !== undefined) {
      await this.rootStore.userStore.callLogin("", notice.AtomGitLoginNotice.code, USER_TYPE_ATOM_GIT);
      this.rootStore.userStore.showUserLogin = false;
      if (this.rootStore.appStore.vendorCfg?.ability.enable_work_bench) {
        await this.rootStore.localRepoStore.init();
      }
    } else if (notice.GitCodeLoginNotice !== undefined) {
      await this.rootStore.userStore.callLogin("", notice.GitCodeLoginNotice.code, USER_TYPE_GIT_CODE);
      this.rootStore.userStore.showUserLogin = false;
      if (this.rootStore.appStore.vendorCfg?.ability.enable_work_bench) {
        await this.rootStore.localRepoStore.init();
      }
    } else if (notice.GiteeLoginNotice !== undefined) {
      await this.rootStore.userStore.callLogin("", notice.GiteeLoginNotice.code, USER_TYPE_GITEE);
      this.rootStore.userStore.showUserLogin = false;
      if (this.rootStore.appStore.vendorCfg?.ability.enable_work_bench) {
        await this.rootStore.localRepoStore.init();
      }
    } else if (notice.LocalRepoChangeNotice !== undefined) {
      await this.rootStore.localRepoStore.onUpdateRepo(notice.LocalRepoChangeNotice.repoId);
    } else if (notice.WatchChangeNotice !== undefined) {
      if (notice.WatchChangeNotice.targetType == WATCH_TARGET_ENTRY) {
        await this.rootStore.entryStore.onUpdateEntry(notice.WatchChangeNotice.targetId);
      }
    } else if (notice.RemoveMailNotice !== undefined || notice.UpdateMailStateNotice != undefined) {
      await this.rootStore.userStore.loadUserMailStatus();
    }
  }

  private async processUserNotice(notice: NoticeType.user.AllNotice) {
    if (notice.UserMailNotice !== undefined) {
      await this.rootStore.userStore.loadUserMailStatus();
    }
  }

  private async processProjectNotice(notice: NoticeType.project.AllNotice) {
    if (notice.UpdateProjectNotice !== undefined) {
      await this.rootStore.projectStore.updateProject(notice.UpdateProjectNotice.project_id);
    } else if (notice.RemoveProjectNotice !== undefined) {
      this.rootStore.projectStore.removeProject(notice.RemoveProjectNotice.project_id, this.history);
    } else if (notice.AddMemberNotice !== undefined) {
      if (notice.AddMemberNotice.project_id == this.rootStore.projectStore.curProjectId) {
        //更新成员信息
        await this.rootStore.memberStore.updateMemberInfo(notice.AddMemberNotice.project_id, notice.AddMemberNotice.member_user_id);
        const member = this.rootStore.memberStore.getMember(notice.AddMemberNotice.member_user_id);
        if (member != undefined) {
          await this.rootStore.memberStore.updateLastEvent(notice.AddMemberNotice.project_id, notice.AddMemberNotice.member_user_id, member.member.last_event_id);
        }
        await this.rootStore.memberStore.updateIssueState(notice.AddMemberNotice.project_id, notice.AddMemberNotice.member_user_id);
      } else {
        const projectId = notice.AddMemberNotice.project_id;
        const index = this.rootStore.projectStore.projectList.findIndex(item => item.project_id == projectId);
        if (index == -1) {
          await this.rootStore.projectStore.updateProject(projectId);
        }
      }
    } else if (notice.UpdateMemberNotice !== undefined) {
      if (notice.UpdateMemberNotice.project_id == this.rootStore.projectStore.curProjectId) {
        await this.rootStore.memberStore.updateMemberInfo(notice.UpdateMemberNotice.project_id, notice.UpdateMemberNotice.member_user_id);
      }
    } else if (notice.RemoveMemberNotice !== undefined) {
      if (notice.RemoveMemberNotice.member_user_id == this.rootStore.userStore.userInfo.userId) {
        this.rootStore.projectStore.removeProject(notice.RemoveMemberNotice.project_id, this.history);
      } else if (notice.RemoveMemberNotice.project_id == this.rootStore.projectStore.curProjectId) {
        this.rootStore.memberStore.loadMemberList(notice.RemoveMemberNotice.project_id);
      }
    } else if (notice.NewEventNotice !== undefined) {
      if (notice.NewEventNotice.project_id == this.rootStore.projectStore.curProjectId) {
        await this.rootStore.memberStore.updateLastEvent(notice.NewEventNotice.project_id, notice.NewEventNotice.member_user_id, notice.NewEventNotice.event_id);
      }
      const projectId = notice.NewEventNotice.project_id;
      setTimeout(() => {
        this.rootStore.projectStore.addNewEventCount(projectId);
      }, 500);
    } else if (notice.SetMemberRoleNotice !== undefined) {
      if (notice.SetMemberRoleNotice.project_id == this.rootStore.projectStore.curProjectId) {
        this.rootStore.memberStore.updateMemberRole(notice.SetMemberRoleNotice.member_user_id, notice.SetMemberRoleNotice.role_id);
      }
    } else if (notice.UpdateShortNoteNotice !== undefined) {
      if (notice.UpdateShortNoteNotice.project_id == this.rootStore.projectStore.curProjectId) {
        this.rootStore.memberStore.updateShortNote(notice.UpdateShortNoteNotice.project_id, notice.UpdateShortNoteNotice.member_user_id);
      }
    } else if (notice.UpdateAlarmStatNotice !== undefined) {
      this.rootStore.projectStore.updateAlarmStatus(notice.UpdateAlarmStatNotice.project_id);
    } else if (notice.CreateBulletinNotice !== undefined) {
      this.rootStore.projectStore.updateBulletinStatus(notice.CreateBulletinNotice.project_id);
    } else if (notice.UpdateBulletinNotice !== undefined) {
      this.rootStore.projectStore.updateBulletinStatus(notice.UpdateBulletinNotice.project_id);
    } else if (notice.RemoveBulletinNotice !== undefined) {
      this.rootStore.projectStore.updateBulletinStatus(notice.RemoveBulletinNotice.project_id);
    } else if (notice.AddTagNotice !== undefined) {
      this.rootStore.projectStore.updateTagList(notice.AddTagNotice.project_id);
    } else if (notice.UpdateTagNotice !== undefined) {
      this.rootStore.projectStore.updateTagList(notice.UpdateTagNotice.project_id);
    } else if (notice.RemoveTagNotice !== undefined) {
      this.rootStore.projectStore.updateTagList(notice.RemoveTagNotice.project_id);
    }
  }

  //只处理项目状态计数
  private async processIssueNotice(notice: NoticeType.issue.AllNotice) {
    if (notice.NewIssueNotice !== undefined) {
      await this.rootStore.projectStore.updateProjectIssueCount(notice.NewIssueNotice.project_id);
    } else if (notice.UpdateIssueNotice !== undefined) {
      await this.rootStore.projectStore.updateProjectIssueCount(notice.UpdateIssueNotice.project_id);
    } else if (notice.RemoveIssueNotice !== undefined) {
      await this.rootStore.projectStore.updateProjectIssueCount(notice.RemoveIssueNotice.project_id);
    }
  }

  private async processEntryNotice(notice: NoticeType.entry.AllNotice) {
    if (notice.UpdateEntryNotice !== undefined && notice.UpdateEntryNotice.project_id == this.rootStore.projectStore.curProjectId) {
      await this.rootStore.entryStore.onUpdateEntry(notice.UpdateEntryNotice.entry_id);
    } else if (notice.RemoveEntryNotice !== undefined && notice.RemoveEntryNotice.project_id == this.rootStore.projectStore.curProjectId) {
      await this.rootStore.entryStore.onRemoveEntry(notice.RemoveEntryNotice.entry_id);
    }
  }

  private async processShortNoteEvent(ev: ShortNoteEvent) {
    if (ev.shortNoteType == SHORT_NOTE_TASK) {
      if (ev.shortNoteModeType == SHORT_NOTE_MODE_DETAIL) {
        this.rootStore.linkAuxStore.goToLink(new LinkTaskInfo("", ev.projectId, ev.targetId), this.history);
      } else if (ev.shortNoteModeType == SHORT_NOTE_MODE_SHOW) {
        const res = await request(get_issue(this.rootStore.userStore.sessionId, ev.projectId, ev.targetId));
        if (res) {
          await showShortNote(this.rootStore.userStore.sessionId, {
            shortNoteType: ev.shortNoteType,
            data: res.info,
          }, this.rootStore.projectStore.getProject(ev.projectId)?.basic_info.project_name ?? "");
        }
      }
    } else if (ev.shortNoteType == SHORT_NOTE_BUG) {
      if (ev.shortNoteModeType == SHORT_NOTE_MODE_DETAIL) {
        this.rootStore.linkAuxStore.goToLink(new LinkBugInfo("", ev.projectId, ev.targetId), this.history);
      } else if (ev.shortNoteModeType == SHORT_NOTE_MODE_SHOW) {
        const res = await request(get_issue(this.rootStore.userStore.sessionId, ev.projectId, ev.targetId));
        if (res) {
          await showShortNote(this.rootStore.userStore.sessionId, {
            shortNoteType: ev.shortNoteType,
            data: res.info,
          }, this.rootStore.projectStore.getProject(ev.projectId)?.basic_info.project_name ?? "");
        }
      }
    }

    if (ev.shortNoteModeType != SHORT_NOTE_MODE_SHOW) {
      await appWindow.show();
      await appWindow.unminimize();
      await appWindow.setAlwaysOnTop(true);
      setTimeout(() => {
        appWindow.setAlwaysOnTop(false);
      }, 200);
    }
  }

}



export default NoticeStore;
