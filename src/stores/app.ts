//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { makeAutoObservable, runInAction } from 'mobx';
import * as clientCfgApi from '@/api/client_cfg';
import type { RootStore } from '.';

export interface OpenMinAppParam {
  minAppId: string;
  extraInfoName: string;
  extraInfo: string;
}

class AppStore {
  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
    makeAutoObservable(this);

  }
  rootStore: RootStore;

  private _lang = "zh";
  private _clientCfg: clientCfgApi.GetCfgResponse | undefined = undefined;
  private _vendorCfg: clientCfgApi.VendorConfig | undefined = undefined;

  get lang() {
    return this._lang;
  }

  set lang(val: string) {
    runInAction(() => {
      this._lang = val;
    });
  }

  get clientCfg(): clientCfgApi.GetCfgResponse | undefined {
    return this._clientCfg;
  }

  get vendorCfg() {
    return this._vendorCfg;
  }

  private _curExtraMenu: clientCfgApi.ExtraMenuItem | null = null;

  get curExtraMenu() {
    return this._curExtraMenu;
  }

  set curExtraMenu(val: clientCfgApi.ExtraMenuItem | null) {
    runInAction(() => {
      this._curExtraMenu = val;
    });
  }

  async loadClientCfg() {
    const res = await clientCfgApi.get_cfg();
    runInAction(() => {
      this._clientCfg = res;
    });
  }

  useDefaultClientCfg() {
    runInAction(() => {
      this._clientCfg = {
        item_list: [],
        enable_admin: true,
        atom_git_client_id: "",
        gitee_client_id: "",
        git_code_client_id: "",
        server_time: 0,
        client_time: 0,
        enable_user_mail: false,
        max_mail_count_per_user: 0,
        max_mail_account_per_user: 0,
        user_mail_domain: "",
        serv_http_addr: "",
      };
    });
  }

  async loadVendorCfg() {
    const res = await clientCfgApi.get_vendor_config();
    runInAction(() => {
      this._vendorCfg = res;
    });
  }

  private _showCreateOrJoinProject: boolean = false;

  get showCreateOrJoinProject(): boolean {
    return this._showCreateOrJoinProject;
  }

  set showCreateOrJoinProject(val: boolean) {
    runInAction(() => {
      this._showCreateOrJoinProject = val;
    });
  }

  private _showCreateOrJoinOrg: boolean = false;

  get showCreateOrJoinOrg(): boolean {
    return this._showCreateOrJoinOrg;
  }

  set showCreateOrJoinOrg(val: boolean) {
    runInAction(() => {
      this._showCreateOrJoinOrg = val;
    });
  }

  //显示全局服务器设置
  private _showGlobalServerModal = false;

  get showGlobalServerModal() {
    return this._showGlobalServerModal;
  }

  set showGlobalServerModal(val: boolean) {
    runInAction(() => {
      this._showGlobalServerModal = val;
    });
  }

  //离开编辑状态提示
  private _inEdit = false;
  private _checkLeave = false;
  private _onLeave: (() => void) | null = null;

  get inEdit(): boolean {
    return this._inEdit;
  }

  set inEdit(val: boolean) {
    runInAction(() => {
      this._inEdit = val;
    });
  }

  get checkLeave(): boolean {
    return this._checkLeave;
  }
  get onLeave(): (() => void) | null {
    return this._onLeave;
  }

  showCheckLeave(fn: () => void) {
    runInAction(() => {
      this._checkLeave = true;
      this._onLeave = fn;
    });
  }

  clearCheckLeave() {
    runInAction(() => {
      this._checkLeave = false;
      this._onLeave = null;
    });
  }

  // 打开微应用
  private _openMinAppParam: OpenMinAppParam | null = null;

  get openMinAppParam() {
    return this._openMinAppParam;
  }

  set openMinAppParam(val: OpenMinAppParam | null) {
    runInAction(() => {
      this._openMinAppParam = val;
    });
  }

  // 显示退出
  private _showExit = false;

  get showExit() {
    return this._showExit;
  }

  set showExit(val: boolean) {
    runInAction(() => {
      this._showExit = val;
    });
  }

  //显示反馈
  private _showFeedBack = false;

  get showFeedBack() {
    return this._showFeedBack;
  }

  set showFeedBack(val: boolean) {
    runInAction(() => {
      this._showFeedBack = val;
    });
  }

  // 显示加入项目/团队
  private _showJoinModal = false;

  get showJoinModal() {
    return this._showJoinModal;
  }

  set showJoinModal(val: boolean) {
    runInAction(() => {
      this._showJoinModal = val;
    });
  }

  //布局配置
  private _simpleLayout: boolean | null = null;

  get simpleLayout(): boolean {
    if (this._simpleLayout == null) {
      return !(this._vendorCfg?.layout.enable_normal_layout ?? false);
    }
    return this._simpleLayout;
  }

  set simpleLayout(val: boolean) {
    runInAction(() => {
      this._simpleLayout = val;
    });
  }

}

export default AppStore;
