//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import type { PROJECT_HOME_TYPE } from '@/api/project';
import { PROJECT_HOME_CONTENT_LIST } from '@/api/project';
import { ENTRY_TYPE_SPRIT, type ENTRY_TYPE } from '@/api/project_entry';
import { TrigerInfo } from '@/api/project_triger';
import { makeAutoObservable, runInAction } from 'mobx';

export class ProjectHomeStore {
    constructor() {
        makeAutoObservable(this);
    }
    private _homeType = PROJECT_HOME_CONTENT_LIST;

    get homeType() {
        return this._homeType;
    }

    set homeType(val: PROJECT_HOME_TYPE) {
        if (val == this._homeType) {
            return;
        }
        runInAction(() => {
            if (val == PROJECT_HOME_CONTENT_LIST) {
                this._contentCurPage = 0;
                this._contentTotalCount = 0;
                this._contentKeyword = "";
                this._contentTagIdList = [] as string[];
                this._contentFilterByWatch = false;
                this._contentEntryType = PROJECT_HOME_CONTENT_LIST;
            }
            this._homeType = val;
        });
    }

    //内容面板相关
    private _contentCurPage = 0;
    private _contentTotalCount = 0;
    private _contentKeyword = "";
    private _contentTagIdList = [] as string[];
    private _contentEntryType = ENTRY_TYPE_SPRIT;
    private _contentFilterByWatch = false;

    get contentCurPage() {
        return this._contentCurPage;
    }

    set contentCurPage(val: number) {
        runInAction(() => {
            this._contentCurPage = val;
        });
    }

    get contentTotalCount() {
        return this._contentTotalCount;
    }

    set contentTotalCount(val: number) {
        runInAction(() => {
            this._contentTotalCount = val;
        });
    }

    get contentKeyword() {
        return this._contentKeyword;
    }

    set contentKeyword(val: string) {
        runInAction(() => {
            this._contentKeyword = val;
        });
    }

    get contentTagIdList() {
        return this._contentTagIdList;
    }

    set contentTagIdList(val: string[]) {
        runInAction(() => {
            this._contentTagIdList = val;
        });
    }

    get contentEntryType() {
        return this._contentEntryType;
    }

    set contentEntryType(val: ENTRY_TYPE) {
        runInAction(() => {
            this._contentCurPage = 0;
            this._contentTotalCount = 0;
            this._contentKeyword = "";
            this._contentTagIdList = [];
            this._contentFilterByWatch = false;
            this._contentEntryType = val;
        });
    }

    get contentFilterByWatch() {
        return this._contentFilterByWatch;
    }

    set contentFilterByWatch(val: boolean) {
        runInAction(() => {
            this._contentFilterByWatch = val;
        });
    }

    //研发流程相关属性
    private _curTriger: TrigerInfo | null = null;

    get curTriger() {
        return this._curTriger;
    }

    set curTriger(val: TrigerInfo | null) {
        runInAction(() => {
            this._curTriger = val;
        });
    }
}