//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import React from 'react';
import { renderRoutes } from 'react-router-config';
import type { IRouteConfig } from '@/routes';
import style from './style.module.less';
import { observer } from 'mobx-react';
import { useStores } from '@/hooks';
import CodeCommentThreadModal from '@/pages/Project/Code/CodeCommentThreadModal';
import ProjectSettingModal from '@/pages/Project/Setting/ProjectSettingModal';
import CreateIdeaModal from '@/pages/Idea/components/CreateIdeaModal';
import UpdateEntryModal from '@/pages/Project/Home/components/UpdateEntryModal';
import ChatAndCommentPanel from '@/pages/Project/ChatAndComment';
import CreateEntryModal from '@/pages/Project/Home/components/CreateEntryModal';
import { ErrorBoundary } from '@/components/ErrorBoundary';
import MemberList from '@/pages/Project/ChatAndComment/MemberList';
import MemberDetail from '@/pages/Project/ChatAndComment/MemberDetail';
import MyWorkInfo from '@/pages/Project/ChatAndComment/MyWorkInfo';

const ProjectLayout: React.FC<{ route: IRouteConfig }> = ({ route }) => {

    const projectStore = useStores("projectStore");
    const ideaStore = useStores("ideaStore");
    const entryStore = useStores("entryStore");
    const memberStore = useStores('memberStore');

    return (
        <ErrorBoundary>
            <div className={style.projectLayout}>
                {projectStore.showChatAndComment && (
                    <div style={{ width: "400px", borderLeft: "1px solid #e4e4e8" }}>
                        <div style={{ width: "390px", backgroundColor: "white", margin: "5px 5px", height: "calc(100vh - 96px)", borderRadius: "10px" }}>
                            {projectStore.showChatAndCommentTab != "member" && projectStore.showChatAndCommentTab != "mywork" && <ChatAndCommentPanel />}
                            {projectStore.showChatAndCommentTab == "member" && memberStore.showDetailMemberId == "" && <MemberList />}
                            {projectStore.showChatAndCommentTab == "member" && memberStore.showDetailMemberId != "" && <MemberDetail />}
                            {projectStore.showChatAndCommentTab == "mywork" && <MyWorkInfo />}
                        </div>
                    </div>
                )}

                <div style={{ flex: 1, marginRight: "60px" }}>
                    {renderRoutes(route.routes)}
                </div>

                {projectStore.codeCommentThreadId != "" && (
                    <CodeCommentThreadModal threadId={projectStore.codeCommentThreadId} commentId={projectStore.codeCommentId} />
                )}
                {projectStore.curProjectId != "" && projectStore.showProjectSetting != null && (
                    <ProjectSettingModal />
                )}
                {projectStore.curProjectId != "" && ideaStore.showCreateIdea == true && (
                    <CreateIdeaModal />
                )}
                {projectStore.curProjectId != "" && entryStore.editEntryId != "" && (
                    <UpdateEntryModal projectInfo={projectStore.curProject!} entryId={entryStore.editEntryId}
                        memberList={memberStore.memberList.map(item => item.member)} tagList={projectStore.curProject?.tag_list ?? []}
                        onClose={() => entryStore.editEntryId = ""} />
                )}
                {entryStore.createEntryType != null && (
                    <CreateEntryModal />
                )}

            </div>
        </ErrorBoundary>
    );
};

export default observer(ProjectLayout);
