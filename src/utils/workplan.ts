//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { appWindow, WebviewWindow } from "@tauri-apps/api/window";

//打开工作计划页面
export async function openWorkplanPage(projectId: string, workPlanId: string) {
    const label = `workplan:${workPlanId.replaceAll("-", "")}`;
    const pos = await appWindow.innerPosition();
    new WebviewWindow(label, {
        title: "工作计划",
        url: `prj_workplan.html?projectId=${projectId}&workPlanId=${workPlanId}`,
        x: pos.x + Math.floor(Math.random() * 200),
        y: pos.y + Math.floor(Math.random() * 200),
        fileDropEnabled: false,
        width: 1280,
        height: 640,
    });
}