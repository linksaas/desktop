//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { appWindow, WebviewWindow } from "@tauri-apps/api/window";
import { sleep } from "./time";

export async function openRoadmapView(roadmapId: string, roadmapName: string, sessionId: string, adminUser: boolean, canUpdate: boolean, nodeId: string = "", showNote: boolean = false) {
    const label = `roadmap:${roadmapId.replaceAll("-", "")}`;
    const view = WebviewWindow.getByLabel(label);
    if (view != null) {
        await view.setAlwaysOnTop(true);
        await sleep(500);
        await view.setAlwaysOnTop(false);
        return;
    }
    const pos = await appWindow.innerPosition();

    new WebviewWindow(label, {
        url: `roadmap.html?roadmapId=${encodeURIComponent(roadmapId)}&sessionId=${encodeURIComponent(sessionId)}&adminUser=${adminUser}&canUpdate=${canUpdate}&nodeId=${encodeURIComponent(nodeId)}&showNote=${showNote}`,
        width: 1000,
        minWidth: 1000,
        height: 700,
        minHeight: 700,
        title: `成长路线(${roadmapName})`,
        resizable: true,
        x: pos.x + Math.floor(Math.random() * 200),
        y: pos.y + Math.floor(Math.random() * 200),
        fileDropEnabled: false,
    });
}