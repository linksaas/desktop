//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { appWindow, WebviewWindow } from "@tauri-apps/api/window";

//打开文档页面
export async function openDocPage(projectId: string, docId: string, inEdit: boolean = false) {
    const label = `doc:${docId.replaceAll("-", "")}`;
    const pos = await appWindow.innerPosition();
    new WebviewWindow(label, {
        title: "项目文档",
        url: `prj_doc.html?projectId=${projectId}&docId=${docId}&inEdit=${inEdit}`,
        x: pos.x + Math.floor(Math.random() * 200),
        y: pos.y + Math.floor(Math.random() * 200),
        width: 1280,
        height: 640,
    });
}