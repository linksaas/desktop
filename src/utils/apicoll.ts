//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { API_COLL_CUSTOM, API_COLL_GRPC, API_COLL_OPENAPI, type API_COLL_TYPE } from "@/api/project_entry";
import { appWindow, WebviewWindow } from "@tauri-apps/api/window";

//打开接口集合页面
export async function openApiCollPage(projectId: string, fsId: string, apiCollId: string, name: string, apiCollType: API_COLL_TYPE, defaultAddr: string, canEdit: boolean, canAdmin: boolean, showComment: boolean = false) {
    const label = `apiColl:${apiCollId.replaceAll("-", "")}`;
    const pos = await appWindow.innerPosition();
    if (apiCollType == API_COLL_GRPC) {
        new WebviewWindow(label, {
            title: `${name}(GRPC)`,
            url: `api_grpc.html?projectId=${projectId}&apiCollId=${apiCollId}&fsId=${fsId}&remoteAddr=${defaultAddr}&edit=${canEdit}&admin=${canAdmin}&showComment=${showComment}`,
            x: pos.x + Math.floor(Math.random() * 200),
            y: pos.y + Math.floor(Math.random() * 200),
            width: 1280,
            height: 640,
        });
    } else if (apiCollType == API_COLL_OPENAPI) {
        new WebviewWindow(label, {
            title: `${name}(OPENAPI/SWAGGER)`,
            url: `api_swagger.html?projectId=${projectId}&apiCollId=${apiCollId}&fsId=${fsId}&remoteAddr=${defaultAddr}&edit=${canEdit}&admin=${canAdmin}&showComment=${showComment}`,
            x: pos.x + Math.floor(Math.random() * 200),
            y: pos.y + Math.floor(Math.random() * 200),
            width: 1280,
            height: 640,
        });
    } else if (apiCollType == API_COLL_CUSTOM) {
        new WebviewWindow(label, {
            title: `${name}(自定义接口)`,
            url: `api_custom.html?projectId=${projectId}&apiCollId=${apiCollId}&remoteAddr=${defaultAddr}&edit=${canEdit}&admin=${canAdmin}&showComment=${showComment}`,
            x: pos.x + Math.floor(Math.random() * 200),
            y: pos.y + Math.floor(Math.random() * 200),
            width: 1280,
            height: 640,
        });
    }
}