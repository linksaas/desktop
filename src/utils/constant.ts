//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import * as issueApi from '@/api/project_issue';

export const WORKBENCH_PATH = '/app/workbench';
export const DATA_VIEW_PATH = '/app/dataview';
export const PUB_RES_PATH = '/app/pubres';
export const GROW_CENTER_PATH = '/app/growcenter';

export const RESET_TEXT = 'resetPassword';

export const APP_PROJECT_MANAGER_PATH = '/app/project_mgr';
export const APP_PROJECT_PATH = '/app/project';
export const APP_PROJECT_HOME_PATH = '/app/project/home';

export const APP_ORG_MANAGER_PATH = '/app/org_mgr';
export const APP_ORG_PATH = '/app/org';

export const APP_EXTERN_PAGE_PATH = "/app/extern";

export const ADMIN_PATH = "/admin";

export const ADMIN_PATH_USER_LIST_SUFFIX = '/admin/user/list';
export const ADMIN_PATH_USER_DETAIL_SUFFIX = '/admin/user/detail';
export const ADMIN_PATH_USER_CREATE_SUFFIX = '/admin/user/create';
export const ADMIN_PATH_USER_FEEDBACK_SUFFIX = '/admin/user/feedback';


export const ADMIN_PATH_PROJECT_LIST_SUFFIX = '/admin/project/list';
export const ADMIN_PATH_PROJECT_DETAIL_SUFFIX = '/admin/project/detail';

export const ADMIN_PATH_ORG_LIST_SUFFIX = '/admin/org/list';
export const ADMIN_PATH_ORG_DETAIL_SUFFIX = '/admin/org/detail';

export const ADMIN_PATH_APPSTORE_CATE_SUFFIX = '/admin/appstore/cate';
export const ADMIN_PATH_APPSTORE_APP_SUFFIX = '/admin/appstore/app';

export const ADMIN_PATH_IDEA_STORE_CATE_SUFFIX = "/admin/ideastore/cate";
export const ADMIN_PATH_IDEA_STORE_SUFFIX = "/admin/ideastore/store";
export const ADMIN_PATH_IDEA_SUFFIX = "/admin/ideastore/idea";

export const ADMIN_PATH_WIDGET_SUFFIX = "/admin/widgetstore/widget"

export const ADMIN_PATH_SOFTWARE_CATE_SUFFIX = '/admin/swstore/cate';
export const ADMIN_PATH_SOFTWARE_SUFFIX = '/admin/swstore/software';


export const ADMIN_PATH_CLIENT_MENU_SUFFIX = '/admin/client/menu';
export const ADMIN_PATH_SECURITY_KEYWORD_SUFFIX = '/admin/security/keyword';
export const ADMIN_PATH_SECURITY_ADMIN_USER_SUFFIX = '/admin/security/admin_user';

export const ADMIN_PATH_GROW_CENTER_ROADMAP_SUFFIX = '/admin/growcenter/roadmap';
export const ADMIN_PATH_GROW_CENTER_TAG_SUFFIX = '/admin/growcenter/tag';
export const ADMIN_PATH_GROW_CENTER_WHITE_USER_SUFFIX = '/admin/growcenter/white_user';


export const statusText = {
  [issueApi.ISSUE_STATE_PLAN]: '规划中阶段', //规划中
  [issueApi.ISSUE_STATE_PROCESS]: '处理阶段', //处理
  [issueApi.ISSUE_STATE_CHECK]: '验收阶段', //验收
  [issueApi.ISSUE_STATE_CLOSE]: '关闭阶段', //关闭
};

export enum ISSUE_STATE_COLOR_ENUM {
  规划中颜色 = '72 201 118', //规划中
  处理颜色 = '83 165 255', //处理
  验收颜色 = '247 136 91', //验收
  关闭颜色 = '196 196 196', //关闭
}

export const issueState = {
  [issueApi.ISSUE_STATE_PLAN]: {
    label: '规划中',
    value: issueApi.ISSUE_STATE_PLAN,
  },
  [issueApi.ISSUE_STATE_PROCESS]: {
    label: '处理',
    value: issueApi.ISSUE_STATE_PROCESS,
  },
  [issueApi.ISSUE_STATE_CHECK]: {
    label: '验收',
    value: issueApi.ISSUE_STATE_CHECK,
  },
  [issueApi.ISSUE_STATE_CLOSE]: {
    label: '关闭',
    value: issueApi.ISSUE_STATE_CLOSE,
  },
  [issueApi.ISSUE_STATE_PROCESS_OR_CHECK]: {
    label: '处理或验收',
    value: issueApi.ISSUE_STATE_PROCESS_OR_CHECK,
  }
};

export const taskPriority = {
  [issueApi.TASK_PRIORITY_LOW]: {
    label: '低',
    value: issueApi.TASK_PRIORITY_LOW,
    color: '#38CB80',
  },
  [issueApi.TASK_PRIORITY_MIDDLE]: {
    label: '中',
    value: issueApi.TASK_PRIORITY_MIDDLE,
    color: '#FF8963',
  },
  [issueApi.TASK_PRIORITY_HIGH]: {
    label: '高',
    value: issueApi.TASK_PRIORITY_HIGH,
    color: '#FF4C30',
  },
};

export const bugLevel = {
  [issueApi.BUG_LEVEL_MINOR]: {
    label: '提示',
    value: issueApi.BUG_LEVEL_MINOR,
    color: '#06C776',
  },
  [issueApi.BUG_LEVEL_MAJOR]: {
    label: '一般',
    value: issueApi.BUG_LEVEL_MAJOR,
    color: '#F99B00',
  },
  [issueApi.BUG_LEVEL_CRITICAL]: {
    label: '严重',
    value: issueApi.BUG_LEVEL_CRITICAL,
    color: '#FB6D17',
  },
  [issueApi.BUG_LEVEL_BLOCKER]: {
    label: '致命',
    value: issueApi.BUG_LEVEL_BLOCKER,
    color: '#DF0627',
  },
};

export const bugPriority = {
  [issueApi.BUG_PRIORITY_LOW]: {
    label: '低优先级',
    value: issueApi.BUG_PRIORITY_LOW,
    color: '#1D85F8',
  },
  [issueApi.BUG_PRIORITY_NORMAL]: {
    label: '正常处理',
    value: issueApi.BUG_PRIORITY_NORMAL,
    color: '#06C776',
  },
  [issueApi.BUG_PRIORITY_HIGH]: {
    label: '高度重视',
    value: issueApi.BUG_PRIORITY_HIGH,
    color: '#F99B00',
  },
  [issueApi.BUG_PRIORITY_URGENT]: {
    label: '急需解决',
    value: issueApi.BUG_PRIORITY_URGENT,
    color: '#FB6D17',
  },
  [issueApi.BUG_PRIORITY_IMMEDIATE]: {
    label: '马上解决',
    value: issueApi.BUG_PRIORITY_IMMEDIATE,
    color: '#DF0627',
  },
};

export enum FILTER_DOC_ENUM {
  ALL = 'all',
  CONCERN = 'concern',
  NOT_CONCERN = 'not_concern',
}

export const filterDocItemList = [
  {
    label: '不限',
    value: FILTER_DOC_ENUM.ALL,
  },
  {
    label: '已关注的文档',
    value: FILTER_DOC_ENUM.CONCERN,
  },
  {
    label: '未关注的文档',
    value: FILTER_DOC_ENUM.NOT_CONCERN,
  },
];

export enum PROJECT_SETTING_TAB {
  PROJECT_SETTING_ALARM,
  PROJECT_SETTING_LAYOUT,
  PROJECT_SETTING_TIPLIST,
  PROJECT_SETTING_TAGLIST,
  PROJECT_SETTING_EVENT,
  PROJECT_SETTING_DESC,
}


export const LANG_LIST = [
  'abap',
  'abnf',
  'actionscript',
  'ada',
  'agda',
  'al',
  'antlr4',
  'apacheconf',
  'apex',
  'apl',
  'applescript',
  'aql',
  'arduino',
  'arff',
  'armasm',
  'arturo',
  'asciidoc',
  'asm6502',
  'asmatmel',
  'aspnet',
  'autohotkey',
  'autoit',
  'avisynth',
  'avro-idl',
  'awk',
  'bash',
  'basic',
  'batch',
  'bbcode',
  'bicep',
  'birb',
  'bison',
  'bnf',
  'brainfuck',
  'brightscript',
  'bro',
  'bsl',
  'c',
  'cfscript',
  'chaiscript',
  'cil',
  'clike',
  'clojure',
  'cmake',
  'cobol',
  'coffeescript',
  'concurnas',
  'cooklang',
  'coq',
  'cpp',
  'crystal',
  'csharp',
  'cshtml',
  'csp',
  'css-extras',
  'css',
  'csv',
  'cue',
  'cypher',
  'd',
  'dart',
  'dataweave',
  'dax',
  'dhall',
  'diff',
  'django',
  'dns-zone-file',
  'docker',
  'dot',
  'ebnf',
  'editorconfig',
  'eiffel',
  'ejs',
  'elixir',
  'elm',
  'erb',
  'erlang',
  'etlua',
  'excel-formula',
  'factor',
  'false',
  'firestore-security-rules',
  'flow',
  'fortran',
  'fsharp',
  'ftl',
  'gap',
  'gcode',
  'gdscript',
  'gedcom',
  'gettext',
  'gherkin',
  'git',
  'glsl',
  'gml',
  'gn',
  'go-module',
  'go',
  'graphql',
  'groovy',
  'haml',
  'handlebars',
  'haskell',
  'haxe',
  'hcl',
  'hlsl',
  'hoon',
  'hpkp',
  'hsts',
  'http',
  'ichigojam',
  'icon',
  'icu-message-format',
  'idris',
  'iecst',
  'ignore',
  'inform7',
  'ini',
  'io',
  'j',
  'java',
  'javadoc',
  'javadoclike',
  'javascript',
  'javastacktrace',
  'jexl',
  'jolie',
  'jq',
  'js-extras',
  'js-templates',
  'jsdoc',
  'json',
  'json5',
  'jsonp',
  'jsstacktrace',
  'jsx',
  'julia',
  'keepalived',
  'keyman',
  'kotlin',
  'kumir',
  'kusto',
  'latex',
  'latte',
  'less',
  'lilypond',
  'linker-script',
  'liquid',
  'lisp',
  'livescript',
  'llvm',
  'log',
  'lolcode',
  'lua',
  'magma',
  'makefile',
  'markdown',
  'markup-templating',
  'markup',
  'mata',
  'matlab',
  'maxscript',
  'mel',
  'mermaid',
  'mizar',
  'mongodb',
  'monkey',
  'moonscript',
  'n1ql',
  'n4js',
  'nand2tetris-hdl',
  'naniscript',
  'nasm',
  'neon',
  'nevod',
  'nginx',
  'nim',
  'nix',
  'nsis',
  'objectivec',
  'ocaml',
  'odin',
  'opencl',
  'openqasm',
  'oz',
  'parigp',
  'parser',
  'pascal',
  'pascaligo',
  'pcaxis',
  'peoplecode',
  'perl',
  'php-extras',
  'php',
  'phpdoc',
  'plant-uml',
  'plsql',
  'powerquery',
  'powershell',
  'processing',
  'prolog',
  'promql',
  'properties',
  'protobuf',
  'psl',
  'pug',
  'puppet',
  'pure',
  'purebasic',
  'purescript',
  'python',
  'q',
  'qml',
  'qore',
  'qsharp',
  'r',
  'racket',
  'reason',
  'regex',
  'rego',
  'renpy',
  'rescript',
  'rest',
  'rip',
  'roboconf',
  'robotframework',
  'ruby',
  'rust',
  'sas',
  'sass',
  'scala',
  'scheme',
  'scss',
  'shell-session',
  'smali',
  'smalltalk',
  'smarty',
  'sml',
  'solidity',
  'solution-file',
  'soy',
  'sparql',
  'splunk-spl',
  'sqf',
  'sql',
  'squirrel',
  'stan',
  'stata',
  'stylus',
  'supercollider',
  'swift',
  'systemd',
  't4-cs',
  't4-templating',
  't4-vb',
  'tap',
  'tcl',
  'textile',
  'toml',
  'tremor',
  'tsx',
  'tt2',
  'turtle',
  'twig',
  'typescript',
  'typoscript',
  'unrealscript',
  'uorazor',
  'uri',
  'v',
  'vala',
  'vbnet',
  'velocity',
  'verilog',
  'vhdl',
  'vim',
  'visual-basic',
  'warpscript',
  'wasm',
  'web-idl',
  'wiki',
  'wolfram',
  'wren',
  'xeora',
  'xml-doc',
  'xojo',
  'xquery',
  'yaml',
  'yang',
  'zig',
];