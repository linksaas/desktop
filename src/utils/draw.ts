//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

import { appWindow, WebviewWindow } from "@tauri-apps/api/window";

//打开绘图白板
export async function openDrawPage(projectId: string, drawId: string, inEdit: boolean = false) {
    const label = `draw:${drawId.replaceAll("-", "")}`;
    const pos = await appWindow.innerPosition();
    new WebviewWindow(label, {
        title: "绘图白板",
        url: `prj_draw.html?projectId=${projectId}&drawId=${drawId}&inEdit=${inEdit}`,
        x: pos.x + Math.floor(Math.random() * 200),
        y: pos.y + Math.floor(Math.random() * 200),
        width: 1280,
        height: 640,
    });
}