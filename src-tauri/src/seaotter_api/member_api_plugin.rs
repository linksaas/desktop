//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

use so_proto_gen_rust::member_api::member_api_client::MemberApiClient;
use so_proto_gen_rust::member_api::*;
use tauri::{
    plugin::{Plugin, Result as PluginResult},
    AppHandle, Invoke, PageLoadPayload, Runtime, Window,
};


#[tauri::command]
async fn list(
    addr: String,
    request: ListRequest,
) -> Result<ListResponse, String> {
    let chan = crate::conn_extern_server(addr).await;
    if chan.is_err() {
        return Err(chan.err().unwrap());
    }
    let mut client = MemberApiClient::new(chan.unwrap());
    match client.list(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn remove(
    addr: String,
    request: RemoveRequest,
) -> Result<RemoveResponse, String> {
    let chan = crate::conn_extern_server(addr).await;
    if chan.is_err() {
        return Err(chan.err().unwrap());
    }
    let mut client = MemberApiClient::new(chan.unwrap());
    match client.remove(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn update_perm(
    addr: String,
    request: UpdatePermRequest,
) -> Result<UpdatePermResponse, String> {
    let chan = crate::conn_extern_server(addr).await;
    if chan.is_err() {
        return Err(chan.err().unwrap());
    }
    let mut client = MemberApiClient::new(chan.unwrap());
    match client.update_perm(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn get_my_perm(
    addr: String,
    request: GetMyPermRequest,
) -> Result<GetMyPermResponse, String> {
    let chan = crate::conn_extern_server(addr).await;
    if chan.is_err() {
        return Err(chan.err().unwrap());
    }
    let mut client = MemberApiClient::new(chan.unwrap());
    match client.get_my_perm(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}



pub struct MemberApiPlugin<R: Runtime> {
    invoke_handler: Box<dyn Fn(Invoke<R>) + Send + Sync + 'static>,
}

impl<R: Runtime> MemberApiPlugin<R> {
    pub fn new() -> Self {
        Self {
            invoke_handler: Box::new(tauri::generate_handler![
                list,
                remove,
                update_perm,
                get_my_perm,
            ]),
        }
    }
}

impl<R: Runtime> Plugin<R> for MemberApiPlugin<R> {
    fn name(&self) -> &'static str {
        "seaotter_member_api"
    }
    fn initialization_script(&self) -> Option<String> {
        None
    }

    fn initialize(&mut self, _app: &AppHandle<R>, _config: serde_json::Value) -> PluginResult<()> {
        Ok(())
    }

    fn created(&mut self, _window: Window<R>) {}

    fn on_page_load(&mut self, _window: Window<R>, _payload: PageLoadPayload) {}

    fn extend_api(&mut self, message: Invoke<R>) {
        (self.invoke_handler)(message)
    }
}