//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

use df_proto_gen_rust::trace_api::trace_api_client::TraceApiClient;
use df_proto_gen_rust::trace_api::*;
use tauri::{
    plugin::{Plugin, Result as PluginResult},
    AppHandle, Invoke, PageLoadPayload, Runtime, Window,
};

#[tauri::command]
async fn list_service(
    addr: String,
    request: ListServiceRequest,
) -> Result<ListServiceResponse, String> {
    let chan = crate::conn_extern_server(addr).await;
    if chan.is_err() {
        return Err(chan.err().unwrap());
    }
    let mut client = TraceApiClient::new(chan.unwrap());
    match client.list_service(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn list_root_span_name(
    addr: String,
    request: ListRootSpanNameRequest,
) -> Result<ListRootSpanNameResponse, String> {
    let chan = crate::conn_extern_server(addr).await;
    if chan.is_err() {
        return Err(chan.err().unwrap());
    }
    let mut client = TraceApiClient::new(chan.unwrap());
    match client.list_root_span_name(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn list_trace(
    addr: String,
    request: ListTraceRequest,
) -> Result<ListTraceResponse, String> {
    let chan = crate::conn_extern_server(addr).await;
    if chan.is_err() {
        return Err(chan.err().unwrap());
    }
    let mut client = TraceApiClient::new(chan.unwrap());
    match client.list_trace(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn get_trace(
    addr: String,
    request: GetTraceRequest,
) -> Result<GetTraceResponse, String> {
    let chan = crate::conn_extern_server(addr).await;
    if chan.is_err() {
        return Err(chan.err().unwrap());
    }
    let mut client = TraceApiClient::new(chan.unwrap());
    match client.get_trace(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn list_span(
    addr: String,
    request: ListSpanRequest,
) -> Result<ListSpanResponse, String> {
    let chan = crate::conn_extern_server(addr).await;
    if chan.is_err() {
        return Err(chan.err().unwrap());
    }
    let mut client = TraceApiClient::new(chan.unwrap());
    match client.list_span(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

pub struct TraceApiPlugin<R: Runtime> {
    invoke_handler: Box<dyn Fn(Invoke<R>) + Send + Sync + 'static>,
}

impl<R: Runtime> TraceApiPlugin<R> {
    pub fn new() -> Self {
        Self {
            invoke_handler: Box::new(tauri::generate_handler![
                list_service,
                list_root_span_name,
                list_trace,
                get_trace,
                list_span,
            ]),
        }
    }
}

impl<R: Runtime> Plugin<R> for TraceApiPlugin<R> {
    fn name(&self) -> &'static str {
        "dragonfly_trace_api"
    }
    fn initialization_script(&self) -> Option<String> {
        None
    }

    fn initialize(&mut self, _app: &AppHandle<R>, _config: serde_json::Value) -> PluginResult<()> {
        Ok(())
    }

    fn created(&mut self, _window: Window<R>) {}

    fn on_page_load(&mut self, _window: Window<R>, _payload: PageLoadPayload) {}

    fn extend_api(&mut self, message: Invoke<R>) {
        (self.invoke_handler)(message)
    }
}