//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

pub mod user_admin_api_plugin;
pub mod user_api_plugin;
pub mod user_app_api_plugin;
pub mod user_dataview_api_plugin;
pub mod user_mail_api_plugin;
pub mod user_memo_api_plugin;
pub mod user_resume_admin_api_plugin;
pub mod user_resume_api_plugin;
pub mod user_server_plugin;
pub mod user_todo_api_plugin;
