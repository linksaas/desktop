//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

use crate::notice_decode::new_wrong_session_notice;
use proto_gen_rust::user_app_api::user_app_api_client::UserAppApiClient;
use proto_gen_rust::user_app_api::*;
use tauri::{
    plugin::{Plugin, Result as PluginResult},
    AppHandle, Invoke, PageLoadPayload, Runtime, Window,
};

#[tauri::command]
async fn add_app<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: AddAppRequest,
) -> Result<AddAppResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = UserAppApiClient::new(chan.unwrap());
    match client.add_app(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == add_app_response::Code::WrongSession as i32 {
                if let Err(err) = window.emit("notice", new_wrong_session_notice("add_app".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn list_app<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: ListAppRequest,
) -> Result<ListAppResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = UserAppApiClient::new(chan.unwrap());
    match client.list_app(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == list_app_response::Code::WrongSession as i32 {
                if let Err(err) = window.emit("notice", new_wrong_session_notice("list_app".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn set_top<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: SetTopRequest,
) -> Result<SetTopResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = UserAppApiClient::new(chan.unwrap());
    match client.set_top(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == set_top_response::Code::WrongSession as i32 {
                if let Err(err) = window.emit("notice", new_wrong_session_notice("set_top".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn remove_app<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: RemoveAppRequest,
) -> Result<RemoveAppResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = UserAppApiClient::new(chan.unwrap());
    match client.remove_app(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == remove_app_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("remove_app".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

pub struct UserAppApiPlugin<R: Runtime> {
    invoke_handler: Box<dyn Fn(Invoke<R>) + Send + Sync + 'static>,
}

impl<R: Runtime> UserAppApiPlugin<R> {
    pub fn new() -> Self {
        Self {
            invoke_handler: Box::new(tauri::generate_handler![
                add_app, list_app, set_top, remove_app
            ]),
        }
    }
}

impl<R: Runtime> Plugin<R> for UserAppApiPlugin<R> {
    fn name(&self) -> &'static str {
        "user_app_api"
    }
    fn initialization_script(&self) -> Option<String> {
        None
    }

    fn initialize(&mut self, _app: &AppHandle<R>, _config: serde_json::Value) -> PluginResult<()> {
        Ok(())
    }

    fn created(&mut self, _window: Window<R>) {}

    fn on_page_load(&mut self, _window: Window<R>, _payload: PageLoadPayload) {}

    fn extend_api(&mut self, message: Invoke<R>) {
        (self.invoke_handler)(message)
    }
}
