//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

use proto_gen_rust::client_cfg_api::client_cfg_api_client::ClientCfgApiClient;
use proto_gen_rust::client_cfg_api::*;
use tauri::async_runtime::Mutex;
use tauri::Manager;
use tauri::{
    plugin::{Plugin, Result as PluginResult},
    AppHandle, Invoke, PageLoadPayload, Runtime, Window,
};
use tokio::fs;
use tokio::io::AsyncReadExt;
use tokio::io::AsyncWriteExt;
use tokio::sync::RwLock;

#[derive(Default)]
pub struct FileLock(pub RwLock<i32>);

#[derive(Default)]
pub struct GlobalServerAddr(pub Mutex<String>);

#[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
pub struct ServerInfo {
    pub name: String,
    pub system: bool,
    pub addr: String,
    pub default_server: bool,
}

#[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
pub struct ListServerResult {
    pub server_list: Vec<ServerInfo>,
}

#[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
pub struct VendorAbility {
    pub enable_work_bench: bool,
    pub enable_dataview: bool,
    pub enable_project: bool,
    pub enable_org: bool,
    pub enable_pubres: bool,
    pub enable_user_mail: bool,
    pub enable_grow_center: bool,
}

#[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
pub struct VendorDataView {
    pub enable_atomgit: bool,
    pub enable_gitcode: bool,
    pub enable_gitee: bool,
    pub enable_gitlab: bool,
    pub enable_tencloud: bool,
    pub enable_alicloud: bool,
    pub enable_hwcloud: bool,
}

#[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
pub struct VendorWorkBench {
    pub enable_minapp: bool,
    pub enable_user_memo: bool,
    pub enable_server_list: bool,
    pub enable_user_resume: bool,
    pub enable_user_todo: bool,
}

#[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
pub struct VendorProject {
    pub show_requirement_list_entry: bool,
    pub show_task_list_entry: bool,
    pub show_bug_list_entry: bool,
    pub show_testcase_list_entry: bool,
}

#[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
pub struct VendorOrg {
    pub enable_forum: bool,
}

#[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
pub struct VendorGrowCenter {
    pub force_show: bool,   //强制显示成长中心入口
    pub filter_tag: String, //显示路线图列表时按tag过滤
}

#[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
pub struct VendorAccount {
    pub inner_account: bool,
    pub external_account: bool,
    pub external_atomgit: bool,
    pub external_gitcode: bool,
    pub external_gitee: bool,
}

#[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
pub struct VendorMenuItem {
    pub id: String,
    pub name: String,
    pub url: String,
}

#[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
pub struct VendorLayout {
    pub app_name: String,
    pub hide_sys_bar: bool,
    pub show_feedback_modal: bool,
    pub feedback_url: String,
    pub show_server_switch: bool,
    pub show_quick_access: bool,
    pub show_admin_in_quick_access: bool,
    pub show_local_api_in_quick_access: bool,
    pub enable_normal_layout: bool,
    pub show_layout_switch: bool,
    pub enable_server_menu: bool,
    pub menu_list: Vec<VendorMenuItem>,
    pub welcome_page_url: String,
    pub show_manual_and_version: bool,
    pub vendor_logo_url: String,
    pub vendor_logo_height: i16,
    pub vendor_link_url: String,
    pub vendor_intro: String,
}

#[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
pub struct VendorConfig {
    pub vendor_name: String,
    pub default_server_name: String,
    pub default_server_addr: String,
    pub global_server_addr: String,
    pub ability: VendorAbility,
    pub work_bench: VendorWorkBench,
    pub dataview: VendorDataView,
    pub project: VendorProject,
    pub org: VendorOrg,
    pub grow_center: VendorGrowCenter,
    pub account: VendorAccount,
    pub layout: VendorLayout,
}

#[tauri::command]
async fn get_cfg<R: Runtime>(
    app_handle: AppHandle<R>,
    _window: Window<R>,
    request: GetCfgRequest,
) -> Result<GetCfgResponse, String> {
    let chan = super::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ClientCfgApiClient::new(chan.unwrap());
    match client.get_cfg(request).await {
        Ok(response) => {
            let response = response.into_inner();
            return Ok(response);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn report_error<R: Runtime>(
    app_handle: AppHandle<R>,
    _window: Window<R>,
    request: ReportErrorRequest,
) -> Result<ReportErrorResponse, String> {
    let chan = super::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ClientCfgApiClient::new(chan.unwrap());
    match client.report_error(request).await {
        Ok(response) => {
            let response = response.into_inner();
            return Ok(response);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn list_server<R: Runtime>(app_handle: AppHandle<R>, skip_system: bool) -> ListServerResult {
    let _l = app_handle.state::<FileLock>().inner().0.read().await;

    let mut result = ListServerResult {
        server_list: Vec::new(),
    };
    if !skip_system {
        let cfg = crate::vendor_config::get_vendor_config();
        result.server_list.push(ServerInfo {
            name: cfg.default_server_name,
            system: true,
            addr: cfg.default_server_addr,
            default_server: false,
        });
    }
    //读取配置文件
    if let Some(cfg_path) = get_server_cfg().await {
        if let Ok(mut f) = fs::File::open(cfg_path).await {
            let mut data: Vec<u8> = Vec::new();
            if let Ok(_) = f.read_to_end(&mut data).await {
                if let Ok(json_str) = String::from_utf8(data) {
                    let server_list: Result<Vec<ServerInfo>, serde_json::Error> =
                        serde_json::from_str(&json_str);
                    if server_list.is_ok() {
                        for info in server_list.unwrap() {
                            result.server_list.push(info);
                        }
                    }
                }
            }
        }
    }
    if !skip_system {
        let mut has_default = false;
        for info in &(result.server_list) {
            if info.default_server {
                has_default = true;
            }
        }
        if !has_default {
            result.server_list[0].default_server = true;
        }
    }
    return result;
}

#[tauri::command]
async fn save_server_list<R: Runtime>(app_handle: AppHandle<R>, server_list: Vec<ServerInfo>) {
    let _l = app_handle.state::<FileLock>().inner().0.write().await;

    let mut new_server_list: Vec<ServerInfo> = Vec::new();

    for info in &server_list {
        if info.system {
            continue;
        }
        new_server_list.push(ServerInfo {
            name: info.name.clone(),
            system: false,
            addr: info.addr.clone(),
            default_server: info.default_server,
        });
    }

    if let Ok(json_str) = serde_json::to_string(&new_server_list) {
        if let Some(cfg_path) = get_server_cfg().await {
            if let Ok(mut f) = fs::File::create(cfg_path).await {
                if let Err(err) = f.write_all(json_str.as_bytes()).await {
                    println!("{}", err);
                }
            }
        }
    }
}

async fn get_server_cfg() -> Option<String> {
    if let Some(base_dir) = crate::get_base_dir() {
        return Some(format!("{}/server.json", base_dir));
    }
    None
}

#[tauri::command]
pub async fn get_global_server_addr<R: Runtime>(app_handle: AppHandle<R>) -> String {
    let cur_value = app_handle.state::<GlobalServerAddr>().inner();
    let result = cur_value.0.lock().await;
    let result = result.clone();
    if &result == "" {
        let cfg = crate::vendor_config::get_vendor_config();
        return cfg.global_server_addr;
    }
    return result;
}

#[tauri::command]
pub async fn set_global_server_addr<R: Runtime>(
    app_handle: AppHandle<R>,
    addr: String,
) -> Result<(), String> {
    let _l = app_handle.state::<FileLock>().inner().0.write().await;

    let user_dir = crate::get_base_dir();
    if user_dir.is_none() {
        return Err("miss user dir".into());
    }
    let mut file_path = std::path::PathBuf::from(user_dir.unwrap());
    if !file_path.exists() {
        let result = fs::create_dir_all(&file_path).await;
        if result.is_err() {
            return Err(result.err().unwrap().to_string());
        }
    }
    file_path.push("global_server.json");
    let f = fs::File::create(file_path).await;
    if f.is_err() {
        return Err(f.err().unwrap().to_string());
    }
    let mut f = f.unwrap();
    let result = f.write_all(addr.as_bytes()).await;
    if result.is_err() {
        return Err(result.err().unwrap().to_string());
    }
    let cur_value = app_handle.state::<GlobalServerAddr>().inner();
    *cur_value.0.lock().await = addr;
    Ok(())
}

async fn load_global_server_addr<R: Runtime>(app_handle: AppHandle<R>) -> String {
    let _l = app_handle.state::<FileLock>().inner().0.read().await;

    let cfg = crate::vendor_config::get_vendor_config();

    let user_dir = crate::get_base_dir();
    if user_dir.is_none() {
        return cfg.default_server_addr;
    }
    let mut file_path = std::path::PathBuf::from(user_dir.unwrap());
    file_path.push("global_server.json");
    if !file_path.exists() {
        return cfg.default_server_addr;
    }
    let f = fs::File::open(file_path).await;
    if f.is_err() {
        return cfg.default_server_addr;
    }
    let mut f = f.unwrap();
    let mut data = Vec::new();
    let result = f.read_to_end(&mut data).await;
    if result.is_err() {
        return cfg.default_server_addr;
    }
    let result = String::from_utf8(data);
    if result.is_err() {
        return cfg.default_server_addr;
    }
    return result.unwrap();
}

#[tauri::command]
async fn get_vendor_config() -> Result<VendorConfig, String> {
    let cfg = crate::vendor_config::get_vendor_config();
    return Ok(cfg);
}

pub struct ClientCfgApiPlugin<R: Runtime> {
    invoke_handler: Box<dyn Fn(Invoke<R>) + Send + Sync + 'static>,
}

impl<R: Runtime> ClientCfgApiPlugin<R> {
    pub fn new() -> Self {
        Self {
            invoke_handler: Box::new(tauri::generate_handler![
                get_cfg,
                report_error,
                list_server,
                save_server_list,
                get_global_server_addr,
                set_global_server_addr,
                get_vendor_config,
            ]),
        }
    }
}

impl<R: Runtime> Plugin<R> for ClientCfgApiPlugin<R> {
    fn name(&self) -> &'static str {
        "client_cfg_api"
    }
    fn initialization_script(&self) -> Option<String> {
        None
    }

    fn initialize(&mut self, app: &AppHandle<R>, _config: serde_json::Value) -> PluginResult<()> {
        app.manage(GlobalServerAddr(Default::default()));
        app.manage(FileLock(Default::default()));
        tauri::async_runtime::block_on(async {
            let addr = load_global_server_addr(app.clone()).await;
            let global_addr = app.state::<GlobalServerAddr>().inner();
            *global_addr.0.lock().await = addr;
        });
        Ok(())
    }

    fn created(&mut self, _window: Window<R>) {}

    fn on_page_load(&mut self, _window: Window<R>, _payload: PageLoadPayload) {}

    fn extend_api(&mut self, message: Invoke<R>) {
        (self.invoke_handler)(message)
    }
}
