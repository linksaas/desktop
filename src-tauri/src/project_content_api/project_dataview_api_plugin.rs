//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

use crate::notice_decode::new_wrong_session_notice;
use proto_gen_rust::project_dataview_api::project_data_view_api_client::ProjectDataViewApiClient;
use proto_gen_rust::project_dataview_api::*;
use tauri::{
    plugin::{Plugin, Result as PluginResult},
    AppHandle, Invoke, PageLoadPayload, Runtime, Window,
};

#[tauri::command]
async fn create_view<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: CreateViewRequest,
) -> Result<CreateViewResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDataViewApiClient::new(chan.unwrap());
    match client.create_view(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == create_view_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("create_view".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn list_view<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: ListViewRequest,
) -> Result<ListViewResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDataViewApiClient::new(chan.unwrap());
    match client.list_view(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == list_view_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("list_view".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn get_view<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: GetViewRequest,
) -> Result<GetViewResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDataViewApiClient::new(chan.unwrap());
    match client.get_view(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == get_view_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("get_view".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn update_view<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: UpdateViewRequest,
) -> Result<UpdateViewResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDataViewApiClient::new(chan.unwrap());
    match client.update_view(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == update_view_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("update_view".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn remove_view<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: RemoveViewRequest,
) -> Result<RemoveViewResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDataViewApiClient::new(chan.unwrap());
    match client.remove_view(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == remove_view_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("remove_view".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn update_view_perm<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: UpdateViewPermRequest,
) -> Result<UpdateViewPermResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDataViewApiClient::new(chan.unwrap());
    match client.update_view_perm(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == update_view_perm_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("update_view_perm".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn create_node<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: CreateNodeRequest,
) -> Result<CreateNodeResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDataViewApiClient::new(chan.unwrap());
    match client.create_node(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == create_node_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("create_node".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn update_node_position<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: UpdateNodePositionRequest,
) -> Result<UpdateNodePositionResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDataViewApiClient::new(chan.unwrap());
    match client.update_node_position(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == update_node_position_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("update_node_position".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn update_node_size<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: UpdateNodeSizeRequest,
) -> Result<UpdateNodeSizeResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDataViewApiClient::new(chan.unwrap());
    match client.update_node_size(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == update_node_size_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("update_node_size".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn update_node_bg_color<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: UpdateNodeBgColorRequest,
) -> Result<UpdateNodeBgColorResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDataViewApiClient::new(chan.unwrap());
    match client.update_node_bg_color(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == update_node_bg_color_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("update_node_bg_color".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn remove_node<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: RemoveNodeRequest,
) -> Result<RemoveNodeResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDataViewApiClient::new(chan.unwrap());
    match client.remove_node(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == remove_node_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("remove_node".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn list_node<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: ListNodeRequest,
) -> Result<ListNodeResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDataViewApiClient::new(chan.unwrap());
    match client.list_node(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == list_node_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("list_node".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn get_node<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: GetNodeRequest,
) -> Result<GetNodeResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDataViewApiClient::new(chan.unwrap());
    match client.get_node(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == get_node_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("get_node".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn update_node_data<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: UpdateNodeDataRequest,
) -> Result<UpdateNodeDataResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDataViewApiClient::new(chan.unwrap());
    match client.update_node_data(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == update_node_data_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("update_node_data".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn convert_to_ref_node<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: ConvertToRefNodeRequest,
) -> Result<ConvertToRefNodeResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDataViewApiClient::new(chan.unwrap());
    match client.convert_to_ref_node(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == convert_to_ref_node_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("convert_to_ref_node".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn create_edge<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: CreateEdgeRequest,
) -> Result<CreateEdgeResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDataViewApiClient::new(chan.unwrap());
    match client.create_edge(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == create_edge_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("create_edge".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn update_edge<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: UpdateEdgeRequest,
) -> Result<UpdateEdgeResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDataViewApiClient::new(chan.unwrap());
    match client.update_edge(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == update_edge_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("update_edge".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn remove_edge<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: RemoveEdgeRequest,
) -> Result<RemoveEdgeResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDataViewApiClient::new(chan.unwrap());
    match client.remove_edge(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == remove_edge_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("remove_edge".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn list_edge<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: ListEdgeRequest,
) -> Result<ListEdgeResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDataViewApiClient::new(chan.unwrap());
    match client.list_edge(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == list_edge_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("list_edge".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn get_edge<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: GetEdgeRequest,
) -> Result<GetEdgeResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDataViewApiClient::new(chan.unwrap());
    match client.get_edge(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == get_edge_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("get_edge".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}


pub struct ProjectDataviewApiPlugin<R: Runtime> {
    invoke_handler: Box<dyn Fn(Invoke<R>) + Send + Sync + 'static>,
}

impl<R: Runtime> ProjectDataviewApiPlugin<R> {
    pub fn new() -> Self {
        Self {
            invoke_handler: Box::new(tauri::generate_handler![
                create_view,
                list_view,
                get_view,
                update_view,
                remove_view,
                update_view_perm,
                create_node,
                update_node_position,
                update_node_size,
                update_node_bg_color,
                remove_node,
                list_node,
                get_node,
                update_node_data,
                convert_to_ref_node,
                create_edge,
                update_edge,
                remove_edge,
                list_edge,
                get_edge,
            ]),
        }
    }
}

impl<R: Runtime> Plugin<R> for ProjectDataviewApiPlugin<R> {
    fn name(&self) -> &'static str {
        "project_dataview_api"
    }
    fn initialization_script(&self) -> Option<String> {
        None
    }

    fn initialize(&mut self, _app: &AppHandle<R>, _config: serde_json::Value) -> PluginResult<()> {
        Ok(())
    }

    fn created(&mut self, _window: Window<R>) {}

    fn on_page_load(&mut self, _window: Window<R>, _payload: PageLoadPayload) {}

    fn extend_api(&mut self, message: Invoke<R>) {
        (self.invoke_handler)(message)
    }
}
