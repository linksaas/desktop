//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

use crate::notice_decode::new_wrong_session_notice;
use proto_gen_rust::dataview_chat_api::data_view_chat_api_client::DataViewChatApiClient;
use proto_gen_rust::dataview_chat_api::*;
use tauri::{
    plugin::{Plugin, Result as PluginResult},
    AppHandle, Invoke, PageLoadPayload, Runtime, Window,
};

#[tauri::command]
async fn send_msg<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: SendMsgRequest,
) -> Result<SendMsgResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = DataViewChatApiClient::new(chan.unwrap());
    match client.send_msg(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == send_msg_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("send_msg".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn list_msg<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: ListMsgRequest,
) -> Result<ListMsgResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = DataViewChatApiClient::new(chan.unwrap());
    match client.list_msg(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == list_msg_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("list_msg".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn update_msg<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: UpdateMsgRequest,
) -> Result<UpdateMsgResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = DataViewChatApiClient::new(chan.unwrap());
    match client.update_msg(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == update_msg_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("update_msg".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn get_msg<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: GetMsgRequest,
) -> Result<GetMsgResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = DataViewChatApiClient::new(chan.unwrap());
    match client.get_msg(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == get_msg_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("get_msg".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

pub struct DataviewChatApiPlugin<R: Runtime> {
    invoke_handler: Box<dyn Fn(Invoke<R>) + Send + Sync + 'static>,
}

impl<R: Runtime> DataviewChatApiPlugin<R> {
    pub fn new() -> Self {
        Self {
            invoke_handler: Box::new(tauri::generate_handler![
                send_msg,
                list_msg,
                update_msg,
                get_msg,
            ]),
        }
    }
}

impl<R: Runtime> Plugin<R> for DataviewChatApiPlugin<R> {
    fn name(&self) -> &'static str {
        "dataview_chat_api"
    }
    fn initialization_script(&self) -> Option<String> {
        None
    }

    fn initialize(&mut self, _app: &AppHandle<R>, _config: serde_json::Value) -> PluginResult<()> {
        Ok(())
    }

    fn created(&mut self, _window: Window<R>) {}

    fn on_page_load(&mut self, _window: Window<R>, _payload: PageLoadPayload) {}

    fn extend_api(&mut self, message: Invoke<R>) {
        (self.invoke_handler)(message)
    }
}