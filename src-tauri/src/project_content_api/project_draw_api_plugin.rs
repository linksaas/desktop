//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

use crate::notice_decode::new_wrong_session_notice;
use proto_gen_rust::project_draw_api::project_draw_api_client::ProjectDrawApiClient;
use proto_gen_rust::project_draw_api::*;
use tauri::{
    plugin::{Plugin, Result as PluginResult},
    AppHandle, Invoke, PageLoadPayload, Runtime, Window,
};

#[tauri::command]
async fn create_draw<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: CreateDrawRequest,
) -> Result<CreateDrawResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDrawApiClient::new(chan.unwrap());
    match client.create_draw(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == create_draw_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("create_draw".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn start_update_draw<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: StartUpdateDrawRequest,
) -> Result<StartUpdateDrawResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDrawApiClient::new(chan.unwrap());
    match client.start_update_draw(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == start_update_draw_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("start_update_draw".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn keep_update_draw<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: KeepUpdateDrawRequest,
) -> Result<KeepUpdateDrawResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDrawApiClient::new(chan.unwrap());
    match client.keep_update_draw(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == keep_update_draw_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("keep_update_draw".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn end_update_draw<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: EndUpdateDrawRequest,
) -> Result<EndUpdateDrawResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDrawApiClient::new(chan.unwrap());
    match client.end_update_draw(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == end_update_draw_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("end_update_draw".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn update_draw<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: UpdateDrawRequest,
) -> Result<UpdateDrawResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDrawApiClient::new(chan.unwrap());
    match client.update_draw(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == update_draw_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("update_draw".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn get_draw<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: GetDrawRequest,
) -> Result<GetDrawResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectDrawApiClient::new(chan.unwrap());
    match client.get_draw(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == get_draw_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("get_draw".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}


pub struct ProjectDrawApiPlugin<R: Runtime> {
    invoke_handler: Box<dyn Fn(Invoke<R>) + Send + Sync + 'static>,
}

impl<R: Runtime> ProjectDrawApiPlugin<R> {
    pub fn new() -> Self {
        Self {
            invoke_handler: Box::new(tauri::generate_handler![
                create_draw,
                start_update_draw,
                keep_update_draw,
                end_update_draw,
                update_draw,
                get_draw,
            ]),
        }
    }
}

impl<R: Runtime> Plugin<R> for ProjectDrawApiPlugin<R> {
    fn name(&self) -> &'static str {
        "project_draw_api"
    }
    fn initialization_script(&self) -> Option<String> {
        None
    }

    fn initialize(&mut self, _app: &AppHandle<R>, _config: serde_json::Value) -> PluginResult<()> {
        Ok(())
    }

    fn created(&mut self, _window: Window<R>) {}

    fn on_page_load(&mut self, _window: Window<R>, _payload: PageLoadPayload) {}

    fn extend_api(&mut self, message: Invoke<R>) {
        (self.invoke_handler)(message)
    }
}
