//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

pub mod user {
    use prost::Message;
    use proto_gen_rust::google::protobuf::Any;
    use proto_gen_rust::notices_user;
    use proto_gen_rust::TypeUrl;

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq, Debug)]
    pub enum Notice {
        UserMailNotice(notices_user::UserMailNotice),
    }

    pub fn decode_notice(data: &Any) -> Option<Notice> {
        if data.type_url == notices_user::UserMailNotice::type_url() {
            if let Ok(notice) = notices_user::UserMailNotice::decode(data.value.as_slice()) {
                return Some(Notice::UserMailNotice(notice));
            }
        }
        None
    }
}

pub mod project {
    use prost::Message;
    use proto_gen_rust::google::protobuf::Any;
    use proto_gen_rust::notices_project;
    use proto_gen_rust::TypeUrl;

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq, Debug)]
    pub enum Notice {
        UpdateProjectNotice(notices_project::UpdateProjectNotice),
        RemoveProjectNotice(notices_project::RemoveProjectNotice),
        AddMemberNotice(notices_project::AddMemberNotice),
        UpdateMemberNotice(notices_project::UpdateMemberNotice),
        RemoveMemberNotice(notices_project::RemoveMemberNotice),
        NewEventNotice(notices_project::NewEventNotice),
        SetMemberRoleNotice(notices_project::SetMemberRoleNotice),
        UpdateShortNoteNotice(notices_project::UpdateShortNoteNotice),
        UpdateAlarmStatNotice(notices_project::UpdateAlarmStatNotice),
        CreateBulletinNotice(notices_project::CreateBulletinNotice),
        UpdateBulletinNotice(notices_project::UpdateBulletinNotice),
        RemoveBulletinNotice(notices_project::RemoveBulletinNotice),
        AddTagNotice(notices_project::AddTagNotice),
        UpdateTagNotice(notices_project::UpdateTagNotice),
        RemoveTagNotice(notices_project::RemoveTagNotice),
        UpdateSpritNotice(notices_project::UpdateSpritNotice),
        CreateReviewNotice(notices_project::CreateReviewNotice),
        UpdateReviewNotice(notices_project::UpdateReviewNotice),
        RemoveReviewNotice(notices_project::RemoveReviewNotice),
    }

    pub fn decode_notice(data: &Any) -> Option<Notice> {
        if data.type_url == notices_project::UpdateProjectNotice::type_url() {
            if let Ok(notice) = notices_project::UpdateProjectNotice::decode(data.value.as_slice())
            {
                return Some(Notice::UpdateProjectNotice(notice));
            }
        } else if data.type_url == notices_project::RemoveProjectNotice::type_url() {
            if let Ok(notice) = notices_project::RemoveProjectNotice::decode(data.value.as_slice())
            {
                return Some(Notice::RemoveProjectNotice(notice));
            }
        } else if data.type_url == notices_project::AddMemberNotice::type_url() {
            if let Ok(notice) = notices_project::AddMemberNotice::decode(data.value.as_slice()) {
                return Some(Notice::AddMemberNotice(notice));
            }
        } else if data.type_url == notices_project::UpdateMemberNotice::type_url() {
            if let Ok(notice) = notices_project::UpdateMemberNotice::decode(data.value.as_slice()) {
                return Some(Notice::UpdateMemberNotice(notice));
            }
        } else if data.type_url == notices_project::RemoveMemberNotice::type_url() {
            if let Ok(notice) = notices_project::RemoveMemberNotice::decode(data.value.as_slice()) {
                return Some(Notice::RemoveMemberNotice(notice));
            }
        } else if data.type_url == notices_project::NewEventNotice::type_url() {
            if let Ok(notice) = notices_project::NewEventNotice::decode(data.value.as_slice()) {
                return Some(Notice::NewEventNotice(notice));
            }
        } else if data.type_url == notices_project::SetMemberRoleNotice::type_url() {
            if let Ok(notice) = notices_project::SetMemberRoleNotice::decode(data.value.as_slice())
            {
                return Some(Notice::SetMemberRoleNotice(notice));
            }
        } else if data.type_url == notices_project::UpdateShortNoteNotice::type_url() {
            if let Ok(notice) =
                notices_project::UpdateShortNoteNotice::decode(data.value.as_slice())
            {
                return Some(Notice::UpdateShortNoteNotice(notice));
            }
        } else if data.type_url == notices_project::UpdateAlarmStatNotice::type_url() {
            if let Ok(notice) =
                notices_project::UpdateAlarmStatNotice::decode(data.value.as_slice())
            {
                return Some(Notice::UpdateAlarmStatNotice(notice));
            }
        } else if data.type_url == notices_project::CreateBulletinNotice::type_url() {
            if let Ok(notice) = notices_project::CreateBulletinNotice::decode(data.value.as_slice())
            {
                return Some(Notice::CreateBulletinNotice(notice));
            }
        } else if data.type_url == notices_project::UpdateBulletinNotice::type_url() {
            if let Ok(notice) = notices_project::UpdateBulletinNotice::decode(data.value.as_slice())
            {
                return Some(Notice::UpdateBulletinNotice(notice));
            }
        } else if data.type_url == notices_project::RemoveBulletinNotice::type_url() {
            if let Ok(notice) = notices_project::RemoveBulletinNotice::decode(data.value.as_slice())
            {
                return Some(Notice::RemoveBulletinNotice(notice));
            }
        } else if data.type_url == notices_project::AddTagNotice::type_url() {
            if let Ok(notice) = notices_project::AddTagNotice::decode(data.value.as_slice()) {
                return Some(Notice::AddTagNotice(notice));
            }
        } else if data.type_url == notices_project::UpdateTagNotice::type_url() {
            if let Ok(notice) = notices_project::UpdateTagNotice::decode(data.value.as_slice()) {
                return Some(Notice::UpdateTagNotice(notice));
            }
        } else if data.type_url == notices_project::RemoveTagNotice::type_url() {
            if let Ok(notice) = notices_project::RemoveTagNotice::decode(data.value.as_slice()) {
                return Some(Notice::RemoveTagNotice(notice));
            }
        } else if data.type_url == notices_project::UpdateSpritNotice::type_url() {
            if let Ok(notice) = notices_project::UpdateSpritNotice::decode(data.value.as_slice()) {
                return Some(Notice::UpdateSpritNotice(notice));
            }
        } else if data.type_url == notices_project::CreateReviewNotice::type_url() {
            if let Ok(notice) = notices_project::CreateReviewNotice::decode(data.value.as_slice()) {
                return Some(Notice::CreateReviewNotice(notice));
            }
        } else if data.type_url == notices_project::UpdateReviewNotice::type_url() {
            if let Ok(notice) = notices_project::UpdateReviewNotice::decode(data.value.as_slice()) {
                return Some(Notice::UpdateReviewNotice(notice));
            }
        } else if data.type_url == notices_project::RemoveReviewNotice::type_url() {
            if let Ok(notice) = notices_project::RemoveReviewNotice::decode(data.value.as_slice()) {
                return Some(Notice::RemoveReviewNotice(notice));
            }
        }
        None
    }
}

pub mod issue {
    use prost::Message;
    use proto_gen_rust::google::protobuf::Any;
    use proto_gen_rust::notices_issue;
    use proto_gen_rust::TypeUrl;

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq, Debug)]
    pub enum Notice {
        NewIssueNotice(notices_issue::NewIssueNotice),
        RemoveIssueNotice(notices_issue::RemoveIssueNotice),
        UpdateIssueNotice(notices_issue::UpdateIssueNotice),
        SetSpritNotice(notices_issue::SetSpritNotice),
        UpdateIssueDepNotice(notices_issue::UpdateIssueDepNotice),
        UpdateSubIssueNotice(notices_issue::UpdateSubIssueNotice),
    }

    pub fn decode_notice(data: &Any) -> Option<Notice> {
        if data.type_url == notices_issue::NewIssueNotice::type_url() {
            if let Ok(notice) = notices_issue::NewIssueNotice::decode(data.value.as_slice()) {
                return Some(Notice::NewIssueNotice(notice));
            }
        } else if data.type_url == notices_issue::RemoveIssueNotice::type_url() {
            if let Ok(notice) = notices_issue::RemoveIssueNotice::decode(data.value.as_slice()) {
                return Some(Notice::RemoveIssueNotice(notice));
            }
        } else if data.type_url == notices_issue::UpdateIssueNotice::type_url() {
            if let Ok(notice) = notices_issue::UpdateIssueNotice::decode(data.value.as_slice()) {
                return Some(Notice::UpdateIssueNotice(notice));
            }
        } else if data.type_url == notices_issue::SetSpritNotice::type_url() {
            if let Ok(notice) = notices_issue::SetSpritNotice::decode(data.value.as_slice()) {
                return Some(Notice::SetSpritNotice(notice));
            }
        } else if data.type_url == notices_issue::UpdateIssueDepNotice::type_url() {
            if let Ok(notice) = notices_issue::UpdateIssueDepNotice::decode(data.value.as_slice()) {
                return Some(Notice::UpdateIssueDepNotice(notice));
            }
        } else if data.type_url == notices_issue::UpdateSubIssueNotice::type_url() {
            if let Ok(notice) = notices_issue::UpdateSubIssueNotice::decode(data.value.as_slice()) {
                return Some(Notice::UpdateSubIssueNotice(notice));
            }
        }
        None
    }
}

pub mod idea {
    use prost::Message;
    use proto_gen_rust::google::protobuf::Any;
    use proto_gen_rust::notices_idea;
    use proto_gen_rust::TypeUrl;

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq, Debug)]
    pub enum Notice {
        CreateGroupNotice(notices_idea::CreateGroupNotice),
        UpdateGroupNotice(notices_idea::UpdateGroupNotice),
        RemoveGroupNotice(notices_idea::RemoveGroupNotice),
        CreateIdeaNotice(notices_idea::CreateIdeaNotice),
        UpdateIdeaNotice(notices_idea::UpdateIdeaNotice),
        RemoveIdeaNotice(notices_idea::RemoveIdeaNotice),
        MoveIdeaNotice(notices_idea::MoveIdeaNotice),
        ClearGroupNotice(notices_idea::ClearGroupNotice),
    }

    pub fn decode_notice(data: &Any) -> Option<Notice> {
        if data.type_url == notices_idea::CreateGroupNotice::type_url() {
            if let Ok(notice) = notices_idea::CreateGroupNotice::decode(data.value.as_slice()) {
                return Some(Notice::CreateGroupNotice(notice));
            }
        } else if data.type_url == notices_idea::UpdateGroupNotice::type_url() {
            if let Ok(notice) = notices_idea::UpdateGroupNotice::decode(data.value.as_slice()) {
                return Some(Notice::UpdateGroupNotice(notice));
            }
        } else if data.type_url == notices_idea::RemoveGroupNotice::type_url() {
            if let Ok(notice) = notices_idea::RemoveGroupNotice::decode(data.value.as_slice()) {
                return Some(Notice::RemoveGroupNotice(notice));
            }
        } else if data.type_url == notices_idea::CreateIdeaNotice::type_url() {
            if let Ok(notice) = notices_idea::CreateIdeaNotice::decode(data.value.as_slice()) {
                return Some(Notice::CreateIdeaNotice(notice));
            }
        } else if data.type_url == notices_idea::UpdateIdeaNotice::type_url() {
            if let Ok(notice) = notices_idea::UpdateIdeaNotice::decode(data.value.as_slice()) {
                return Some(Notice::UpdateIdeaNotice(notice));
            }
        } else if data.type_url == notices_idea::RemoveIdeaNotice::type_url() {
            if let Ok(notice) = notices_idea::RemoveIdeaNotice::decode(data.value.as_slice()) {
                return Some(Notice::RemoveIdeaNotice(notice));
            }
        } else if data.type_url == notices_idea::MoveIdeaNotice::type_url() {
            if let Ok(notice) = notices_idea::MoveIdeaNotice::decode(data.value.as_slice()) {
                return Some(Notice::MoveIdeaNotice(notice));
            }
        } else if data.type_url == notices_idea::ClearGroupNotice::type_url() {
            if let Ok(notice) = notices_idea::ClearGroupNotice::decode(data.value.as_slice()) {
                return Some(Notice::ClearGroupNotice(notice));
            }
        }
        None
    }
}

pub mod comment {
    use prost::Message;
    use proto_gen_rust::google::protobuf::Any;
    use proto_gen_rust::notices_comment;
    use proto_gen_rust::TypeUrl;

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq, Debug)]
    pub enum Notice {
        AddCommentNotice(notices_comment::AddCommentNotice),
        UpdateCommentNotice(notices_comment::UpdateCommentNotice),
        RemoveCommentNotice(notices_comment::RemoveCommentNotice),
        RemoveUnReadNotice(notices_comment::RemoveUnReadNotice),
    }

    pub fn decode_notice(data: &Any) -> Option<Notice> {
        if data.type_url == notices_comment::AddCommentNotice::type_url() {
            if let Ok(notice) = notices_comment::AddCommentNotice::decode(data.value.as_slice()) {
                return Some(Notice::AddCommentNotice(notice));
            }
        } else if data.type_url == notices_comment::UpdateCommentNotice::type_url() {
            if let Ok(notice) = notices_comment::UpdateCommentNotice::decode(data.value.as_slice())
            {
                return Some(Notice::UpdateCommentNotice(notice));
            }
        } else if data.type_url == notices_comment::RemoveCommentNotice::type_url() {
            if let Ok(notice) = notices_comment::RemoveCommentNotice::decode(data.value.as_slice())
            {
                return Some(Notice::RemoveCommentNotice(notice));
            }
        } else if data.type_url == notices_comment::RemoveUnReadNotice::type_url() {
            if let Ok(notice) = notices_comment::RemoveUnReadNotice::decode(data.value.as_slice()) {
                return Some(Notice::RemoveUnReadNotice(notice));
            }
        }
        None
    }
}

pub mod entry {
    use prost::Message;
    use proto_gen_rust::google::protobuf::Any;
    use proto_gen_rust::notices_entry;
    use proto_gen_rust::TypeUrl;

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq, Debug)]
    pub enum Notice {
        NewEntryNotice(notices_entry::NewEntryNotice),
        UpdateEntryNotice(notices_entry::UpdateEntryNotice),
        RemoveEntryNotice(notices_entry::RemoveEntryNotice),
    }

    pub fn decode_notice(data: &Any) -> Option<Notice> {
        if data.type_url == notices_entry::NewEntryNotice::type_url() {
            if let Ok(notice) = notices_entry::NewEntryNotice::decode(data.value.as_slice()) {
                return Some(Notice::NewEntryNotice(notice));
            }
        } else if data.type_url == notices_entry::UpdateEntryNotice::type_url() {
            if let Ok(notice) = notices_entry::UpdateEntryNotice::decode(data.value.as_slice()) {
                return Some(Notice::UpdateEntryNotice(notice));
            }
        } else if data.type_url == notices_entry::RemoveEntryNotice::type_url() {
            if let Ok(notice) = notices_entry::RemoveEntryNotice::decode(data.value.as_slice()) {
                return Some(Notice::RemoveEntryNotice(notice));
            }
        }
        None
    }
}

pub mod requirement {
    use prost::Message;
    use proto_gen_rust::google::protobuf::Any;
    use proto_gen_rust::notices_requirement;
    use proto_gen_rust::TypeUrl;

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq, Debug)]
    pub enum Notice {
        NewRequirementNotice(notices_requirement::NewRequirementNotice),
        UpdateRequirementNotice(notices_requirement::UpdateRequirementNotice),
        RemoveRequirementNotice(notices_requirement::RemoveRequirementNotice),
        LinkIssueNotice(notices_requirement::LinkIssueNotice),
        UnlinkIssueNotice(notices_requirement::UnlinkIssueNotice),
    }

    pub fn decode_notice(data: &Any) -> Option<Notice> {
        if data.type_url == notices_requirement::NewRequirementNotice::type_url() {
            if let Ok(notice) =
                notices_requirement::NewRequirementNotice::decode(data.value.as_slice())
            {
                return Some(Notice::NewRequirementNotice(notice));
            }
        } else if data.type_url == notices_requirement::UpdateRequirementNotice::type_url() {
            if let Ok(notice) =
                notices_requirement::UpdateRequirementNotice::decode(data.value.as_slice())
            {
                return Some(Notice::UpdateRequirementNotice(notice));
            }
        } else if data.type_url == notices_requirement::RemoveRequirementNotice::type_url() {
            if let Ok(notice) =
                notices_requirement::RemoveRequirementNotice::decode(data.value.as_slice())
            {
                return Some(Notice::RemoveRequirementNotice(notice));
            }
        } else if data.type_url == notices_requirement::LinkIssueNotice::type_url() {
            if let Ok(notice) = notices_requirement::LinkIssueNotice::decode(data.value.as_slice())
            {
                return Some(Notice::LinkIssueNotice(notice));
            }
        } else if data.type_url == notices_requirement::UnlinkIssueNotice::type_url() {
            if let Ok(notice) =
                notices_requirement::UnlinkIssueNotice::decode(data.value.as_slice())
            {
                return Some(Notice::UnlinkIssueNotice(notice));
            }
        }
        None
    }
}

pub mod testcase {
    use prost::Message;
    use proto_gen_rust::google::protobuf::Any;
    use proto_gen_rust::notices_testcase;
    use proto_gen_rust::TypeUrl;

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq, Debug)]
    pub enum Notice {
        NewFolderNotice(notices_testcase::NewFolderNotice),
        UpdateFolderNotice(notices_testcase::UpdateFolderNotice),
        RemoveFolderNotice(notices_testcase::RemoveFolderNotice),
        NewCaseNotice(notices_testcase::NewCaseNotice),
        UpdateCaseNotice(notices_testcase::UpdateCaseNotice),
        RemoveCaseNotice(notices_testcase::RemoveCaseNotice),
        LinkSpritNotice(notices_testcase::LinkSpritNotice),
        UnlinkSpritNotice(notices_testcase::UnlinkSpritNotice),
    }

    pub fn decode_notice(data: &Any) -> Option<Notice> {
        if data.type_url == notices_testcase::NewFolderNotice::type_url() {
            if let Ok(notice) = notices_testcase::NewFolderNotice::decode(data.value.as_slice()) {
                return Some(Notice::NewFolderNotice(notice));
            }
        } else if data.type_url == notices_testcase::UpdateFolderNotice::type_url() {
            if let Ok(notice) = notices_testcase::UpdateFolderNotice::decode(data.value.as_slice())
            {
                return Some(Notice::UpdateFolderNotice(notice));
            }
        } else if data.type_url == notices_testcase::RemoveFolderNotice::type_url() {
            if let Ok(notice) = notices_testcase::RemoveFolderNotice::decode(data.value.as_slice())
            {
                return Some(Notice::RemoveFolderNotice(notice));
            }
        } else if data.type_url == notices_testcase::NewCaseNotice::type_url() {
            if let Ok(notice) = notices_testcase::NewCaseNotice::decode(data.value.as_slice()) {
                return Some(Notice::NewCaseNotice(notice));
            }
        } else if data.type_url == notices_testcase::UpdateCaseNotice::type_url() {
            if let Ok(notice) = notices_testcase::UpdateCaseNotice::decode(data.value.as_slice()) {
                return Some(Notice::UpdateCaseNotice(notice));
            }
        } else if data.type_url == notices_testcase::RemoveCaseNotice::type_url() {
            if let Ok(notice) = notices_testcase::RemoveCaseNotice::decode(data.value.as_slice()) {
                return Some(Notice::RemoveCaseNotice(notice));
            }
        } else if data.type_url == notices_testcase::LinkSpritNotice::type_url() {
            if let Ok(notice) = notices_testcase::LinkSpritNotice::decode(data.value.as_slice()) {
                return Some(Notice::LinkSpritNotice(notice));
            }
        } else if data.type_url == notices_testcase::UnlinkSpritNotice::type_url() {
            if let Ok(notice) = notices_testcase::UnlinkSpritNotice::decode(data.value.as_slice()) {
                return Some(Notice::UnlinkSpritNotice(notice));
            }
        }
        None
    }
}

pub mod org {
    use prost::Message;
    use proto_gen_rust::google::protobuf::Any;
    use proto_gen_rust::notices_org;
    use proto_gen_rust::TypeUrl;

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq, Debug)]
    pub enum Notice {
        JoinOrgNotice(notices_org::JoinOrgNotice),
        LeaveOrgNotice(notices_org::LeaveOrgNotice),
        UpdateOrgNotice(notices_org::UpdateOrgNotice),
        CreateDepartMentNotice(notices_org::CreateDepartMentNotice),
        RemoveDepartMentNotice(notices_org::RemoveDepartMentNotice),
        UpdateDepartMentNotice(notices_org::UpdateDepartMentNotice),
        UpdateMemberNotice(notices_org::UpdateMemberNotice),
    }

    pub fn decode_notice(data: &Any) -> Option<Notice> {
        if data.type_url == notices_org::JoinOrgNotice::type_url() {
            if let Ok(notice) = notices_org::JoinOrgNotice::decode(data.value.as_slice()) {
                return Some(Notice::JoinOrgNotice(notice));
            }
        } else if data.type_url == notices_org::LeaveOrgNotice::type_url() {
            if let Ok(notice) = notices_org::LeaveOrgNotice::decode(data.value.as_slice()) {
                return Some(Notice::LeaveOrgNotice(notice));
            }
        } else if data.type_url == notices_org::UpdateOrgNotice::type_url() {
            if let Ok(notice) = notices_org::UpdateOrgNotice::decode(data.value.as_slice()) {
                return Some(Notice::UpdateOrgNotice(notice));
            }
        } else if data.type_url == notices_org::CreateDepartMentNotice::type_url() {
            if let Ok(notice) = notices_org::CreateDepartMentNotice::decode(data.value.as_slice()) {
                return Some(Notice::CreateDepartMentNotice(notice));
            }
        } else if data.type_url == notices_org::RemoveDepartMentNotice::type_url() {
            if let Ok(notice) = notices_org::RemoveDepartMentNotice::decode(data.value.as_slice()) {
                return Some(Notice::RemoveDepartMentNotice(notice));
            }
        } else if data.type_url == notices_org::UpdateDepartMentNotice::type_url() {
            if let Ok(notice) = notices_org::UpdateDepartMentNotice::decode(data.value.as_slice()) {
                return Some(Notice::UpdateDepartMentNotice(notice));
            }
        } else if data.type_url == notices_org::UpdateMemberNotice::type_url() {
            if let Ok(notice) = notices_org::UpdateMemberNotice::decode(data.value.as_slice()) {
                return Some(Notice::UpdateMemberNotice(notice));
            }
        }
        None
    }
}

pub mod triger {
    use prost::Message;
    use proto_gen_rust::google::protobuf::Any;
    use proto_gen_rust::notices_triger;
    use proto_gen_rust::TypeUrl;

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq, Debug)]
    pub enum Notice {
        CreateTrigerNotice(notices_triger::CreateTrigerNotice),
        UpdateTrigerNotice(notices_triger::UpdateTrigerNotice),
        RemoveTrigerNotice(notices_triger::RemoveTrigerNotice),
    }
    pub fn decode_notice(data: &Any) -> Option<Notice> {
        if data.type_url == notices_triger::CreateTrigerNotice::type_url() {
            if let Ok(notice) = notices_triger::CreateTrigerNotice::decode(data.value.as_slice()) {
                return Some(Notice::CreateTrigerNotice(notice));
            }
        } else if data.type_url == notices_triger::UpdateTrigerNotice::type_url() {
            if let Ok(notice) = notices_triger::UpdateTrigerNotice::decode(data.value.as_slice()) {
                return Some(Notice::UpdateTrigerNotice(notice));
            }
        } else if data.type_url == notices_triger::RemoveTrigerNotice::type_url() {
            if let Ok(notice) = notices_triger::RemoveTrigerNotice::decode(data.value.as_slice()) {
                return Some(Notice::RemoveTrigerNotice(notice));
            }
        }
        None
    }
}

pub mod dataview {
    use prost::Message;
    use proto_gen_rust::google::protobuf::Any;
    use proto_gen_rust::notices_dataview;
    use proto_gen_rust::TypeUrl;

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq, Debug)]
    pub enum Notice {
        CreateViewNotice(notices_dataview::CreateViewNotice),
        UpdateViewNotice(notices_dataview::UpdateViewNotice),
        RemoveViewNotice(notices_dataview::RemoveViewNotice),
        CreateNodeNotice(notices_dataview::CreateNodeNotice),
        UpdateNodeNotice(notices_dataview::UpdateNodeNotice),
        RemoveNodeNotice(notices_dataview::RemoveNodeNotice),
        CreateEdgeNotice(notices_dataview::CreateEdgeNotice),
        UpdateEdgeNotice(notices_dataview::UpdateEdgeNotice),
        RemoveEdgeNotice(notices_dataview::RemoveEdgeNotice),
        NewChatMsgNotice(notices_dataview::NewChatMsgNotice),
        UpdateChatMsgNotice(notices_dataview::UpdateChatMsgNotice),
    }

    pub fn decode_notice(data: &Any) -> Option<Notice> {
        if data.type_url == notices_dataview::CreateViewNotice::type_url() {
            if let Ok(notice) = notices_dataview::CreateViewNotice::decode(data.value.as_slice()) {
                return Some(Notice::CreateViewNotice(notice));
            }
        } else if data.type_url == notices_dataview::UpdateViewNotice::type_url() {
            if let Ok(notice) = notices_dataview::UpdateViewNotice::decode(data.value.as_slice()) {
                return Some(Notice::UpdateViewNotice(notice));
            }
        } else if data.type_url == notices_dataview::RemoveViewNotice::type_url() {
            if let Ok(notice) = notices_dataview::RemoveViewNotice::decode(data.value.as_slice()) {
                return Some(Notice::RemoveViewNotice(notice));
            }
        } else if data.type_url == notices_dataview::CreateNodeNotice::type_url() {
            if let Ok(notice) = notices_dataview::CreateNodeNotice::decode(data.value.as_slice()) {
                return Some(Notice::CreateNodeNotice(notice));
            }
        } else if data.type_url == notices_dataview::UpdateNodeNotice::type_url() {
            if let Ok(notice) = notices_dataview::UpdateNodeNotice::decode(data.value.as_slice()) {
                return Some(Notice::UpdateNodeNotice(notice));
            }
        } else if data.type_url == notices_dataview::RemoveNodeNotice::type_url() {
            if let Ok(notice) = notices_dataview::RemoveNodeNotice::decode(data.value.as_slice()) {
                return Some(Notice::RemoveNodeNotice(notice));
            }
        } else if data.type_url == notices_dataview::CreateEdgeNotice::type_url() {
            if let Ok(notice) = notices_dataview::CreateEdgeNotice::decode(data.value.as_slice()) {
                return Some(Notice::CreateEdgeNotice(notice));
            }
        } else if data.type_url == notices_dataview::UpdateEdgeNotice::type_url() {
            if let Ok(notice) = notices_dataview::UpdateEdgeNotice::decode(data.value.as_slice()) {
                return Some(Notice::UpdateEdgeNotice(notice));
            }
        } else if data.type_url == notices_dataview::RemoveEdgeNotice::type_url() {
            if let Ok(notice) = notices_dataview::RemoveEdgeNotice::decode(data.value.as_slice()) {
                return Some(Notice::RemoveEdgeNotice(notice));
            }
        } else if data.type_url == notices_dataview::NewChatMsgNotice::type_url() {
            if let Ok(notice) = notices_dataview::NewChatMsgNotice::decode(data.value.as_slice()) {
                return Some(Notice::NewChatMsgNotice(notice));
            }
        } else if data.type_url == notices_dataview::UpdateChatMsgNotice::type_url() {
            if let Ok(notice) = notices_dataview::UpdateChatMsgNotice::decode(data.value.as_slice())
            {
                return Some(Notice::UpdateChatMsgNotice(notice));
            }
        }
        None
    }
}

pub mod client {
    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq, Debug)]
    #[serde(rename_all = "snake_case")]
    pub struct WrongSessionNotice {
        pub name: String, //用于区分发生错误的地方
    }

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq, Debug)]
    #[serde(rename_all = "snake_case")]
    pub struct StartMinAppNotice {
        pub min_app_id: String,
    }

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq, Debug)]
    #[serde(rename_all = "snake_case")]
    pub struct OpenEntryNotice {
        pub project_id: String,
        pub entry_id: String,
    }

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq, Debug)]
    #[serde(rename_all = "snake_case")]
    pub struct NewExtraTokenNotice {
        pub extra_token: String,
    }

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq, Debug)]
    #[serde(rename_all = "snake_case")]
    pub struct CheckUpdateNotice {}

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq, Debug)]
    pub enum Notice {
        WrongSessionNotice(WrongSessionNotice),
        StartMinAppNotice(StartMinAppNotice),
        OpenEntryNotice(OpenEntryNotice),
        NewExtraTokenNotice(NewExtraTokenNotice),
        CheckUpdateNotice(CheckUpdateNotice),
    }
}

#[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq, Debug)]
pub enum NoticeMessage {
    UserNotice(user::Notice),
    ProjectNotice(project::Notice),
    IssueNotice(issue::Notice),
    IdeaNotice(idea::Notice),
    CommentNotice(comment::Notice),
    EntryNotice(entry::Notice),
    RequirementNotice(requirement::Notice),
    TestcaseNotice(testcase::Notice),
    OrgNotice(org::Notice),
    DataviewNotice(dataview::Notice),
    TrigerNotice(triger::Notice),
    ClientNotice(client::Notice),
}

use proto_gen_rust::google::protobuf::Any;

pub fn decode_notice(data: &Any) -> Option<NoticeMessage> {
    if let Some(ret) = user::decode_notice(data) {
        return Some(NoticeMessage::UserNotice(ret));
    }
    if let Some(ret) = project::decode_notice(data) {
        return Some(NoticeMessage::ProjectNotice(ret));
    }
    if let Some(ret) = issue::decode_notice(data) {
        return Some(NoticeMessage::IssueNotice(ret));
    }
    if let Some(ret) = idea::decode_notice(data) {
        return Some(NoticeMessage::IdeaNotice(ret));
    }
    if let Some(ret) = comment::decode_notice(data) {
        return Some(NoticeMessage::CommentNotice(ret));
    }
    if let Some(ret) = entry::decode_notice(data) {
        return Some(NoticeMessage::EntryNotice(ret));
    }
    if let Some(ret) = requirement::decode_notice(data) {
        return Some(NoticeMessage::RequirementNotice(ret));
    }
    if let Some(ret) = testcase::decode_notice(data) {
        return Some(NoticeMessage::TestcaseNotice(ret));
    }
    if let Some(ret) = org::decode_notice(data) {
        return Some(NoticeMessage::OrgNotice(ret));
    }
    if let Some(ret) = triger::decode_notice(data) {
        return Some(NoticeMessage::TrigerNotice(ret));
    }
    if let Some(ret) = dataview::decode_notice(data) {
        return Some(NoticeMessage::DataviewNotice(ret));
    }
    None
}

pub fn new_wrong_session_notice(name: String) -> NoticeMessage {
    return NoticeMessage::ClientNotice(client::Notice::WrongSessionNotice(
        client::WrongSessionNotice { name: name },
    ));
}

pub fn new_open_entry_notice(project_id: String, entry_id: String) -> NoticeMessage {
    return NoticeMessage::ClientNotice(client::Notice::OpenEntryNotice(client::OpenEntryNotice {
        project_id: project_id,
        entry_id: entry_id,
    }));
}

pub fn new_extra_token_notice(extra_token: String) -> NoticeMessage {
    return NoticeMessage::ClientNotice(client::Notice::NewExtraTokenNotice(
        client::NewExtraTokenNotice {
            extra_token: extra_token,
        },
    ));
}

pub fn new_check_update_notice() -> NoticeMessage {
    return NoticeMessage::ClientNotice(client::Notice::CheckUpdateNotice(
        client::CheckUpdateNotice {},
    ));
}
