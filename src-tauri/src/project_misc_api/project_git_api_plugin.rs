//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

use crate::notice_decode::new_wrong_session_notice;
use proto_gen_rust::project_git_api::project_git_api_client::ProjectGitApiClient;
use proto_gen_rust::project_git_api::*;
use tauri::{
    plugin::{Plugin, Result as PluginResult},
    AppHandle, Invoke, PageLoadPayload, Runtime, Window,
};

#[tauri::command]
async fn add_git_repo<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: AddGitRepoRequest,
) -> Result<AddGitRepoResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectGitApiClient::new(chan.unwrap());
    match client.add_git_repo(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == add_git_repo_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("add_git_repo".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn update_git_repo<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: UpdateGitRepoRequest,
) -> Result<UpdateGitRepoResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectGitApiClient::new(chan.unwrap());
    match client.update_git_repo(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == update_git_repo_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("update_git_repo".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn remove_git_repo<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: RemoveGitRepoRequest,
) -> Result<RemoveGitRepoResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectGitApiClient::new(chan.unwrap());
    match client.remove_git_repo(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == remove_git_repo_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("remove_git_repo".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn list_git_repo<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: ListGitRepoRequest,
) -> Result<ListGitRepoResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectGitApiClient::new(chan.unwrap());
    match client.list_git_repo(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == list_git_repo_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("list_git_repo".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn get_git_repo<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: GetGitRepoRequest,
) -> Result<GetGitRepoResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectGitApiClient::new(chan.unwrap());
    match client.get_git_repo(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == get_git_repo_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("get_git_repo".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

pub struct ProjectGitApiPlugin<R: Runtime> {
    invoke_handler: Box<dyn Fn(Invoke<R>) + Send + Sync + 'static>,
}

impl<R: Runtime> ProjectGitApiPlugin<R> {
    pub fn new() -> Self {
        Self {
            invoke_handler: Box::new(tauri::generate_handler![
                add_git_repo,
                update_git_repo,
                remove_git_repo,
                list_git_repo,
                get_git_repo,
            ]),
        }
    }
}

impl<R: Runtime> Plugin<R> for ProjectGitApiPlugin<R> {
    fn name(&self) -> &'static str {
        "project_git_api"
    }
    fn initialization_script(&self) -> Option<String> {
        None
    }

    fn initialize(&mut self, _app: &AppHandle<R>, _config: serde_json::Value) -> PluginResult<()> {
        Ok(())
    }

    fn created(&mut self, _window: Window<R>) {}

    fn on_page_load(&mut self, _window: Window<R>, _payload: PageLoadPayload) {}

    fn extend_api(&mut self, message: Invoke<R>) {
        (self.invoke_handler)(message)
    }
}