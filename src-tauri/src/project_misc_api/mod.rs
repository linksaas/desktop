//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

pub mod project_bulletin_api_plugin;
pub mod project_code_api_plugin;
pub mod project_comment_api_plugin;
pub mod project_git_api_plugin;
pub mod project_idea_api_plugin;
pub mod project_recycle_api_plugin;
pub mod project_review_api_plugin;
pub mod project_server_api_plugin;
pub mod project_tool_api_plugin;
pub mod project_watch_api_plugin;
pub mod short_note_api_plugin;
