//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

use crate::notice_decode::new_wrong_session_notice;
use proto_gen_rust::project_server_api::project_server_api_client::ProjectServerApiClient;
use proto_gen_rust::project_server_api::*;
use tauri::{
    plugin::{Plugin, Result as PluginResult},
    AppHandle, Invoke, PageLoadPayload, Runtime, Window,
};

#[tauri::command]
async fn add_server<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: AddServerRequest,
) -> Result<AddServerResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectServerApiClient::new(chan.unwrap());
    match client.add_server(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == add_server_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("add_server".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn update_server<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: UpdateServerRequest,
) -> Result<UpdateServerResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectServerApiClient::new(chan.unwrap());
    match client.update_server(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == update_server_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("update_server".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn list_server<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: ListServerRequest,
) -> Result<ListServerResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectServerApiClient::new(chan.unwrap());
    match client.list_server(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == list_server_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("list_server".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn get_server<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: GetServerRequest,
) -> Result<GetServerResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectServerApiClient::new(chan.unwrap());
    match client.get_server(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == get_server_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("get_server".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn remove_server<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: RemoveServerRequest,
) -> Result<RemoveServerResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectServerApiClient::new(chan.unwrap());
    match client.remove_server(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == remove_server_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("remove_server".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn gen_join_token<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: GenJoinTokenRequest,
) -> Result<GenJoinTokenResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectServerApiClient::new(chan.unwrap());
    match client.gen_join_token(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == gen_join_token_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("gen_join_token".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn gen_access_token<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: GenAccessTokenRequest,
) -> Result<GenAccessTokenResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = ProjectServerApiClient::new(chan.unwrap());
    match client.gen_access_token(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == gen_access_token_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("gen_access_token".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

pub struct ProjectServerApiPlugin<R: Runtime> {
    invoke_handler: Box<dyn Fn(Invoke<R>) + Send + Sync + 'static>,
}

impl<R: Runtime> ProjectServerApiPlugin<R> {
    pub fn new() -> Self {
        Self {
            invoke_handler: Box::new(tauri::generate_handler![
                add_server,
                update_server,
                list_server,
                get_server,
                remove_server,
                gen_join_token,
                gen_access_token,
            ]),
        }
    }
}

impl<R: Runtime> Plugin<R> for ProjectServerApiPlugin<R> {
    fn name(&self) -> &'static str {
        "project_server_api"
    }
    fn initialization_script(&self) -> Option<String> {
        None
    }

    fn initialize(&mut self, _app: &AppHandle<R>, _config: serde_json::Value) -> PluginResult<()> {
        Ok(())
    }

    fn created(&mut self, _window: Window<R>) {}

    fn on_page_load(&mut self, _window: Window<R>, _payload: PageLoadPayload) {}

    fn extend_api(&mut self, message: Invoke<R>) {
        (self.invoke_handler)(message)
    }
}
