//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

pub mod project {
    use prost::Message;
    use proto_gen_rust::events_project;
    use proto_gen_rust::google::protobuf::Any;
    use proto_gen_rust::TypeUrl;

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
    pub enum Event {
        CreateProjectEvent(events_project::CreateProjectEvent),
        UpdateProjectEvent(events_project::UpdateProjectEvent),
        OpenProjectEvent(events_project::OpenProjectEvent),
        CloseProjectEvent(events_project::CloseProjectEvent),
        RemoveProjectEvent(events_project::RemoveProjectEvent),
        GenInviteEvent(events_project::GenInviteEvent),
        JoinProjectEvent(events_project::JoinProjectEvent),
        LeaveProjectEvent(events_project::LeaveProjectEvent),
        CreateRoleEvent(events_project::CreateRoleEvent),
        UpdateRoleEvent(events_project::UpdateRoleEvent),
        RemoveRoleEvent(events_project::RemoveRoleEvent),
        UpdateProjectMemberEvent(events_project::UpdateProjectMemberEvent),
        RemoveProjectMemberEvent(events_project::RemoveProjectMemberEvent),
        SetProjectMemberRoleEvent(events_project::SetProjectMemberRoleEvent),
        ChangeOwnerEvent(events_project::ChangeOwnerEvent),
        SetAlarmConfigEvent(events_project::SetAlarmConfigEvent),
        CustomEvent(events_project::CustomEvent),
        WatchEvent(events_project::WatchEvent),
        UnwatchEvent(events_project::UnwatchEvent),
        RecoverFromRecycleEvent(events_project::RecoverFromRecycleEvent),
        RemoveFromRecycleEvent(events_project::RemoveFromRecycleEvent),
        ClearFromRecycleEvent(events_project::ClearFromRecycleEvent),
    }

    pub fn decode_event(data: &Any) -> Option<Event> {
        if data.type_url == events_project::CreateProjectEvent::type_url() {
            if let Ok(ev) = events_project::CreateProjectEvent::decode(data.value.as_slice()) {
                return Some(Event::CreateProjectEvent(ev));
            }
        } else if data.type_url == events_project::UpdateProjectEvent::type_url() {
            if let Ok(ev) = events_project::UpdateProjectEvent::decode(data.value.as_slice()) {
                return Some(Event::UpdateProjectEvent(ev));
            }
        } else if data.type_url == events_project::OpenProjectEvent::type_url() {
            if let Ok(ev) = events_project::OpenProjectEvent::decode(data.value.as_slice()) {
                return Some(Event::OpenProjectEvent(ev));
            }
        } else if data.type_url == events_project::CloseProjectEvent::type_url() {
            if let Ok(ev) = events_project::CloseProjectEvent::decode(data.value.as_slice()) {
                return Some(Event::CloseProjectEvent(ev));
            }
        } else if data.type_url == events_project::RemoveProjectEvent::type_url() {
            if let Ok(ev) = events_project::RemoveProjectEvent::decode(data.value.as_slice()) {
                return Some(Event::RemoveProjectEvent(ev));
            }
        } else if data.type_url == events_project::GenInviteEvent::type_url() {
            if let Ok(ev) = events_project::GenInviteEvent::decode(data.value.as_slice()) {
                return Some(Event::GenInviteEvent(ev));
            }
        } else if data.type_url == events_project::JoinProjectEvent::type_url() {
            if let Ok(ev) = events_project::JoinProjectEvent::decode(data.value.as_slice()) {
                return Some(Event::JoinProjectEvent(ev));
            }
        } else if data.type_url == events_project::LeaveProjectEvent::type_url() {
            if let Ok(ev) = events_project::LeaveProjectEvent::decode(data.value.as_slice()) {
                return Some(Event::LeaveProjectEvent(ev));
            }
        } else if data.type_url == events_project::CreateRoleEvent::type_url() {
            if let Ok(ev) = events_project::CreateRoleEvent::decode(data.value.as_slice()) {
                return Some(Event::CreateRoleEvent(ev));
            }
        } else if data.type_url == events_project::UpdateRoleEvent::type_url() {
            if let Ok(ev) = events_project::UpdateRoleEvent::decode(data.value.as_slice()) {
                return Some(Event::UpdateRoleEvent(ev));
            }
        } else if data.type_url == events_project::RemoveRoleEvent::type_url() {
            if let Ok(ev) = events_project::RemoveRoleEvent::decode(data.value.as_slice()) {
                return Some(Event::RemoveRoleEvent(ev));
            }
        } else if data.type_url == events_project::UpdateProjectMemberEvent::type_url() {
            if let Ok(ev) = events_project::UpdateProjectMemberEvent::decode(data.value.as_slice())
            {
                return Some(Event::UpdateProjectMemberEvent(ev));
            }
        } else if data.type_url == events_project::RemoveProjectMemberEvent::type_url() {
            if let Ok(ev) = events_project::RemoveProjectMemberEvent::decode(data.value.as_slice())
            {
                return Some(Event::RemoveProjectMemberEvent(ev));
            }
        } else if data.type_url == events_project::SetProjectMemberRoleEvent::type_url() {
            if let Ok(ev) = events_project::SetProjectMemberRoleEvent::decode(data.value.as_slice())
            {
                return Some(Event::SetProjectMemberRoleEvent(ev));
            }
        } else if data.type_url == events_project::ChangeOwnerEvent::type_url() {
            if let Ok(ev) = events_project::ChangeOwnerEvent::decode(data.value.as_slice()) {
                return Some(Event::ChangeOwnerEvent(ev));
            }
        } else if data.type_url == events_project::SetAlarmConfigEvent::type_url() {
            if let Ok(ev) = events_project::SetAlarmConfigEvent::decode(data.value.as_slice()) {
                return Some(Event::SetAlarmConfigEvent(ev));
            }
        } else if data.type_url == events_project::CustomEvent::type_url() {
            if let Ok(ev) = events_project::CustomEvent::decode(data.value.as_slice()) {
                return Some(Event::CustomEvent(ev));
            }
        } else if data.type_url == events_project::WatchEvent::type_url() {
            if let Ok(ev) = events_project::WatchEvent::decode(data.value.as_slice()) {
                return Some(Event::WatchEvent(ev));
            }
        } else if data.type_url == events_project::UnwatchEvent::type_url() {
            if let Ok(ev) = events_project::UnwatchEvent::decode(data.value.as_slice()) {
                return Some(Event::UnwatchEvent(ev));
            }
        } else if data.type_url == events_project::RecoverFromRecycleEvent::type_url() {
            if let Ok(ev) = events_project::RecoverFromRecycleEvent::decode(data.value.as_slice()) {
                return Some(Event::RecoverFromRecycleEvent(ev));
            }
        } else if data.type_url == events_project::RemoveFromRecycleEvent::type_url() {
            if let Ok(ev) = events_project::RemoveFromRecycleEvent::decode(data.value.as_slice()) {
                return Some(Event::RemoveFromRecycleEvent(ev));
            }
        } else if data.type_url == events_project::ClearFromRecycleEvent::type_url() {
            if let Ok(ev) = events_project::ClearFromRecycleEvent::decode(data.value.as_slice()) {
                return Some(Event::ClearFromRecycleEvent(ev));
            }
        }
        None
    }
}

pub mod issue {
    use prost::Message;
    use proto_gen_rust::events_issue;
    use proto_gen_rust::google::protobuf::Any;
    use proto_gen_rust::TypeUrl;

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
    pub enum Event {
        CreateEvent(events_issue::CreateEvent),
        UpdateEvent(events_issue::UpdateEvent),
        RemoveEvent(events_issue::RemoveEvent),
        AssignExecUserEvent(events_issue::AssignExecUserEvent),
        AssignCheckUserEvent(events_issue::AssignCheckUserEvent),
        ChangeStateEvent(events_issue::ChangeStateEvent),
        UpdateProcessStageEvent(events_issue::UpdateProcessStageEvent),
        LinkSpritEvent(events_issue::LinkSpritEvent),
        CancelLinkSpritEvent(events_issue::CancelLinkSpritEvent),
        SetStartTimeEvent(events_issue::SetStartTimeEvent),
        SetEndTimeEvent(events_issue::SetEndTimeEvent),
        SetEstimateMinutesEvent(events_issue::SetEstimateMinutesEvent),
        SetRemainMinutesEvent(events_issue::SetRemainMinutesEvent),
        CancelStartTimeEvent(events_issue::CancelStartTimeEvent),
        CancelEndTimeEvent(events_issue::CancelEndTimeEvent),
        CancelEstimateMinutesEvent(events_issue::CancelEstimateMinutesEvent),
        CancelRemainMinutesEvent(events_issue::CancelRemainMinutesEvent),

        CreateSubIssueEvent(events_issue::CreateSubIssueEvent),
        UpdateSubIssueEvent(events_issue::UpdateSubIssueEvent),
        UpdateSubIssueStateEvent(events_issue::UpdateSubIssueStateEvent),
        RemoveSubIssueEvent(events_issue::RemoveSubIssueEvent),
        AddDependenceEvent(events_issue::AddDependenceEvent),
        RemoveDependenceEvent(events_issue::RemoveDependenceEvent),

        SetDeadLineTimeEvent(events_issue::SetDeadLineTimeEvent),
        CancelDeadLineTimeEvent(events_issue::CancelDeadLineTimeEvent),

        UpdateTagEvent(events_issue::UpdateTagEvent),
    }

    pub fn decode_event(data: &Any) -> Option<Event> {
        if data.type_url == events_issue::CreateEvent::type_url() {
            if let Ok(ev) = events_issue::CreateEvent::decode(data.value.as_slice()) {
                return Some(Event::CreateEvent(ev));
            }
        } else if data.type_url == events_issue::UpdateEvent::type_url() {
            if let Ok(ev) = events_issue::UpdateEvent::decode(data.value.as_slice()) {
                return Some(Event::UpdateEvent(ev));
            }
        } else if data.type_url == events_issue::RemoveEvent::type_url() {
            if let Ok(ev) = events_issue::RemoveEvent::decode(data.value.as_slice()) {
                return Some(Event::RemoveEvent(ev));
            }
        } else if data.type_url == events_issue::AssignExecUserEvent::type_url() {
            if let Ok(ev) = events_issue::AssignExecUserEvent::decode(data.value.as_slice()) {
                return Some(Event::AssignExecUserEvent(ev));
            }
        } else if data.type_url == events_issue::AssignCheckUserEvent::type_url() {
            if let Ok(ev) = events_issue::AssignCheckUserEvent::decode(data.value.as_slice()) {
                return Some(Event::AssignCheckUserEvent(ev));
            }
        } else if data.type_url == events_issue::ChangeStateEvent::type_url() {
            if let Ok(ev) = events_issue::ChangeStateEvent::decode(data.value.as_slice()) {
                return Some(Event::ChangeStateEvent(ev));
            }
        } else if data.type_url == events_issue::UpdateProcessStageEvent::type_url() {
            if let Ok(ev) = events_issue::UpdateProcessStageEvent::decode(data.value.as_slice()) {
                return Some(Event::UpdateProcessStageEvent(ev));
            }
        } else if data.type_url == events_issue::LinkSpritEvent::type_url() {
            if let Ok(ev) = events_issue::LinkSpritEvent::decode(data.value.as_slice()) {
                return Some(Event::LinkSpritEvent(ev));
            }
        } else if data.type_url == events_issue::CancelLinkSpritEvent::type_url() {
            if let Ok(ev) = events_issue::CancelLinkSpritEvent::decode(data.value.as_slice()) {
                return Some(Event::CancelLinkSpritEvent(ev));
            }
        } else if data.type_url == events_issue::SetStartTimeEvent::type_url() {
            if let Ok(ev) = events_issue::SetStartTimeEvent::decode(data.value.as_slice()) {
                return Some(Event::SetStartTimeEvent(ev));
            }
        } else if data.type_url == events_issue::SetEndTimeEvent::type_url() {
            if let Ok(ev) = events_issue::SetEndTimeEvent::decode(data.value.as_slice()) {
                return Some(Event::SetEndTimeEvent(ev));
            }
        } else if data.type_url == events_issue::SetEstimateMinutesEvent::type_url() {
            if let Ok(ev) = events_issue::SetEstimateMinutesEvent::decode(data.value.as_slice()) {
                return Some(Event::SetEstimateMinutesEvent(ev));
            }
        } else if data.type_url == events_issue::SetRemainMinutesEvent::type_url() {
            if let Ok(ev) = events_issue::SetRemainMinutesEvent::decode(data.value.as_slice()) {
                return Some(Event::SetRemainMinutesEvent(ev));
            }
        } else if data.type_url == events_issue::CancelStartTimeEvent::type_url() {
            if let Ok(ev) = events_issue::CancelStartTimeEvent::decode(data.value.as_slice()) {
                return Some(Event::CancelStartTimeEvent(ev));
            }
        } else if data.type_url == events_issue::CancelEndTimeEvent::type_url() {
            if let Ok(ev) = events_issue::CancelEndTimeEvent::decode(data.value.as_slice()) {
                return Some(Event::CancelEndTimeEvent(ev));
            }
        } else if data.type_url == events_issue::CancelEstimateMinutesEvent::type_url() {
            if let Ok(ev) = events_issue::CancelEstimateMinutesEvent::decode(data.value.as_slice())
            {
                return Some(Event::CancelEstimateMinutesEvent(ev));
            }
        } else if data.type_url == events_issue::CancelRemainMinutesEvent::type_url() {
            if let Ok(ev) = events_issue::CancelRemainMinutesEvent::decode(data.value.as_slice()) {
                return Some(Event::CancelRemainMinutesEvent(ev));
            }
        } else if data.type_url == events_issue::CreateSubIssueEvent::type_url() {
            if let Ok(ev) = events_issue::CreateSubIssueEvent::decode(data.value.as_slice()) {
                return Some(Event::CreateSubIssueEvent(ev));
            }
        } else if data.type_url == events_issue::UpdateSubIssueEvent::type_url() {
            if let Ok(ev) = events_issue::UpdateSubIssueEvent::decode(data.value.as_slice()) {
                return Some(Event::UpdateSubIssueEvent(ev));
            }
        } else if data.type_url == events_issue::UpdateSubIssueStateEvent::type_url() {
            if let Ok(ev) = events_issue::UpdateSubIssueStateEvent::decode(data.value.as_slice()) {
                return Some(Event::UpdateSubIssueStateEvent(ev));
            }
        } else if data.type_url == events_issue::RemoveSubIssueEvent::type_url() {
            if let Ok(ev) = events_issue::RemoveSubIssueEvent::decode(data.value.as_slice()) {
                return Some(Event::RemoveSubIssueEvent(ev));
            }
        } else if data.type_url == events_issue::AddDependenceEvent::type_url() {
            if let Ok(ev) = events_issue::AddDependenceEvent::decode(data.value.as_slice()) {
                return Some(Event::AddDependenceEvent(ev));
            }
        } else if data.type_url == events_issue::RemoveDependenceEvent::type_url() {
            if let Ok(ev) = events_issue::RemoveDependenceEvent::decode(data.value.as_slice()) {
                return Some(Event::RemoveDependenceEvent(ev));
            }
        } else if data.type_url == events_issue::SetDeadLineTimeEvent::type_url() {
            if let Ok(ev) = events_issue::SetDeadLineTimeEvent::decode(data.value.as_slice()) {
                return Some(Event::SetDeadLineTimeEvent(ev));
            }
        } else if data.type_url == events_issue::CancelDeadLineTimeEvent::type_url() {
            if let Ok(ev) = events_issue::CancelDeadLineTimeEvent::decode(data.value.as_slice()) {
                return Some(Event::CancelDeadLineTimeEvent(ev));
            }
        } else if data.type_url == events_issue::UpdateTagEvent::type_url() {
            if let Ok(ev) = events_issue::UpdateTagEvent::decode(data.value.as_slice()) {
                return Some(Event::UpdateTagEvent(ev));
            }
        }
        None
    }
}

pub mod requirement {
    use prost::Message;
    use proto_gen_rust::events_requirement;
    use proto_gen_rust::google::protobuf::Any;
    use proto_gen_rust::TypeUrl;

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
    pub enum Event {
        CreateRequirementEvent(events_requirement::CreateRequirementEvent),
        UpdateRequirementEvent(events_requirement::UpdateRequirementEvent),
        RemoveRequirementEvent(events_requirement::RemoveRequirementEvent),
        LinkIssueEvent(events_requirement::LinkIssueEvent),
        UnlinkIssueEvent(events_requirement::UnlinkIssueEvent),
        CloseRequirementEvent(events_requirement::CloseRequirementEvent),
        OpenRequirementEvent(events_requirement::OpenRequirementEvent),
        SetKanoInfoEvent(events_requirement::SetKanoInfoEvent),
        SetFourQInfoEvent(events_requirement::SetFourQInfoEvent),
        UpdateTagEvent(events_requirement::UpdateTagEvent),
    }

    pub fn decode_event(data: &Any) -> Option<Event> {
        if data.type_url == events_requirement::CreateRequirementEvent::type_url() {
            if let Ok(ev) =
                events_requirement::CreateRequirementEvent::decode(data.value.as_slice())
            {
                return Some(Event::CreateRequirementEvent(ev));
            }
        } else if data.type_url == events_requirement::UpdateRequirementEvent::type_url() {
            if let Ok(ev) =
                events_requirement::UpdateRequirementEvent::decode(data.value.as_slice())
            {
                return Some(Event::UpdateRequirementEvent(ev));
            }
        } else if data.type_url == events_requirement::RemoveRequirementEvent::type_url() {
            if let Ok(ev) =
                events_requirement::RemoveRequirementEvent::decode(data.value.as_slice())
            {
                return Some(Event::RemoveRequirementEvent(ev));
            }
        } else if data.type_url == events_requirement::RemoveRequirementEvent::type_url() {
            if let Ok(ev) =
                events_requirement::RemoveRequirementEvent::decode(data.value.as_slice())
            {
                return Some(Event::RemoveRequirementEvent(ev));
            }
        } else if data.type_url == events_requirement::LinkIssueEvent::type_url() {
            if let Ok(ev) = events_requirement::LinkIssueEvent::decode(data.value.as_slice()) {
                return Some(Event::LinkIssueEvent(ev));
            }
        } else if data.type_url == events_requirement::UnlinkIssueEvent::type_url() {
            if let Ok(ev) = events_requirement::UnlinkIssueEvent::decode(data.value.as_slice()) {
                return Some(Event::UnlinkIssueEvent(ev));
            }
        } else if data.type_url == events_requirement::CloseRequirementEvent::type_url() {
            if let Ok(ev) = events_requirement::CloseRequirementEvent::decode(data.value.as_slice())
            {
                return Some(Event::CloseRequirementEvent(ev));
            }
        } else if data.type_url == events_requirement::OpenRequirementEvent::type_url() {
            if let Ok(ev) = events_requirement::OpenRequirementEvent::decode(data.value.as_slice())
            {
                return Some(Event::OpenRequirementEvent(ev));
            }
        } else if data.type_url == events_requirement::SetKanoInfoEvent::type_url() {
            if let Ok(ev) = events_requirement::SetKanoInfoEvent::decode(data.value.as_slice()) {
                return Some(Event::SetKanoInfoEvent(ev));
            }
        } else if data.type_url == events_requirement::SetFourQInfoEvent::type_url() {
            if let Ok(ev) = events_requirement::SetFourQInfoEvent::decode(data.value.as_slice()) {
                return Some(Event::SetFourQInfoEvent(ev));
            }
        } else if data.type_url == events_requirement::UpdateTagEvent::type_url() {
            if let Ok(ev) = events_requirement::UpdateTagEvent::decode(data.value.as_slice()) {
                return Some(Event::UpdateTagEvent(ev));
            }
        }
        None
    }
}

pub mod code {
    use prost::Message;
    use proto_gen_rust::events_code;
    use proto_gen_rust::google::protobuf::Any;
    use proto_gen_rust::TypeUrl;

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
    pub enum Event {
        AddCommentEvent(events_code::AddCommentEvent),
        UpdateCommentEvent(events_code::UpdateCommentEvent),
        RemoveCommentEvent(events_code::RemoveCommentEvent),
    }

    pub fn decode_event(data: &Any) -> Option<Event> {
        if data.type_url == events_code::AddCommentEvent::type_url() {
            if let Ok(ev) = events_code::AddCommentEvent::decode(data.value.as_slice()) {
                return Some(Event::AddCommentEvent(ev));
            }
        } else if data.type_url == events_code::UpdateCommentEvent::type_url() {
            if let Ok(ev) = events_code::UpdateCommentEvent::decode(data.value.as_slice()) {
                return Some(Event::UpdateCommentEvent(ev));
            }
        } else if data.type_url == events_code::RemoveCommentEvent::type_url() {
            if let Ok(ev) = events_code::RemoveCommentEvent::decode(data.value.as_slice()) {
                return Some(Event::RemoveCommentEvent(ev));
            }
        }
        None
    }
}

pub mod idea {
    use prost::Message;
    use proto_gen_rust::events_idea;
    use proto_gen_rust::google::protobuf::Any;
    use proto_gen_rust::TypeUrl;

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
    pub enum Event {
        ClearGroupEvent(events_idea::ClearGroupEvent),
        CreateIdeaEvent(events_idea::CreateIdeaEvent),
        UpdateIdeaContentEvent(events_idea::UpdateIdeaContentEvent),
        UpdateIdeaKeywordEvent(events_idea::UpdateIdeaKeywordEvent),
        RemoveIdeaEvent(events_idea::RemoveIdeaEvent),
        SetAppraiseEvent(events_idea::SetAppraiseEvent),
        CancelAppraiseEvent(events_idea::CancelAppraiseEvent),
        ImportIdeaEvent(events_idea::ImportIdeaEvent),
    }

    pub fn decode_event(data: &Any) -> Option<Event> {
        if data.type_url == events_idea::CreateIdeaEvent::type_url() {
            if let Ok(ev) = events_idea::CreateIdeaEvent::decode(data.value.as_slice()) {
                return Some(Event::CreateIdeaEvent(ev));
            }
        } else if data.type_url == events_idea::UpdateIdeaContentEvent::type_url() {
            if let Ok(ev) = events_idea::UpdateIdeaContentEvent::decode(data.value.as_slice()) {
                return Some(Event::UpdateIdeaContentEvent(ev));
            }
        } else if data.type_url == events_idea::UpdateIdeaKeywordEvent::type_url() {
            if let Ok(ev) = events_idea::UpdateIdeaKeywordEvent::decode(data.value.as_slice()) {
                return Some(Event::UpdateIdeaKeywordEvent(ev));
            }
        } else if data.type_url == events_idea::RemoveIdeaEvent::type_url() {
            if let Ok(ev) = events_idea::RemoveIdeaEvent::decode(data.value.as_slice()) {
                return Some(Event::RemoveIdeaEvent(ev));
            }
        } else if data.type_url == events_idea::SetAppraiseEvent::type_url() {
            if let Ok(ev) = events_idea::SetAppraiseEvent::decode(data.value.as_slice()) {
                return Some(Event::SetAppraiseEvent(ev));
            }
        } else if data.type_url == events_idea::CancelAppraiseEvent::type_url() {
            if let Ok(ev) = events_idea::CancelAppraiseEvent::decode(data.value.as_slice()) {
                return Some(Event::CancelAppraiseEvent(ev));
            }
        } else if data.type_url == events_idea::ImportIdeaEvent::type_url() {
            if let Ok(ev) = events_idea::ImportIdeaEvent::decode(data.value.as_slice()) {
                return Some(Event::ImportIdeaEvent(ev));
            }
        } else if data.type_url == events_idea::ClearGroupEvent::type_url() {
            if let Ok(ev) = events_idea::ClearGroupEvent::decode(data.value.as_slice()) {
                return Some(Event::ClearGroupEvent(ev));
            }
        }
        None
    }
}

pub mod entry {
    use prost::Message;
    use proto_gen_rust::events_entry;
    use proto_gen_rust::google::protobuf::Any;
    use proto_gen_rust::TypeUrl;

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
    pub enum Event {
        CreateEvent(events_entry::CreateEvent),
        RemoveEvent(events_entry::RemoveEvent),
    }

    pub fn decode_event(data: &Any) -> Option<Event> {
        if data.type_url == events_entry::CreateEvent::type_url() {
            if let Ok(ev) = events_entry::CreateEvent::decode(data.value.as_slice()) {
                return Some(Event::CreateEvent(ev));
            }
        } else if data.type_url == events_entry::RemoveEvent::type_url() {
            if let Ok(ev) = events_entry::RemoveEvent::decode(data.value.as_slice()) {
                return Some(Event::RemoveEvent(ev));
            }
        }
        None
    }
}

pub mod testcase {
    use prost::Message;
    use proto_gen_rust::events_testcase;
    use proto_gen_rust::google::protobuf::Any;
    use proto_gen_rust::TypeUrl;

    #[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
    pub enum Event {
        CreateCaseEvent(events_testcase::CreateCaseEvent),
        UpdateCaseEvent(events_testcase::UpdateCaseEvent),
        RemoveCaseEvent(events_testcase::RemoveCaseEvent),
        LinkSpritEvent(events_testcase::LinkSpritEvent),
        UnlinkSpritEvent(events_testcase::UnlinkSpritEvent),
    }

    pub fn decode_event(data: &Any) -> Option<Event> {
        if data.type_url == events_testcase::CreateCaseEvent::type_url() {
            if let Ok(ev) = events_testcase::CreateCaseEvent::decode(data.value.as_slice()) {
                return Some(Event::CreateCaseEvent(ev));
            }
        } else if data.type_url == events_testcase::UpdateCaseEvent::type_url() {
            if let Ok(ev) = events_testcase::UpdateCaseEvent::decode(data.value.as_slice()) {
                return Some(Event::UpdateCaseEvent(ev));
            }
        } else if data.type_url == events_testcase::RemoveCaseEvent::type_url() {
            if let Ok(ev) = events_testcase::RemoveCaseEvent::decode(data.value.as_slice()) {
                return Some(Event::RemoveCaseEvent(ev));
            }
        } else if data.type_url == events_testcase::LinkSpritEvent::type_url() {
            if let Ok(ev) = events_testcase::LinkSpritEvent::decode(data.value.as_slice()) {
                return Some(Event::LinkSpritEvent(ev));
            }
        } else if data.type_url == events_testcase::UnlinkSpritEvent::type_url() {
            if let Ok(ev) = events_testcase::UnlinkSpritEvent::decode(data.value.as_slice()) {
                return Some(Event::UnlinkSpritEvent(ev));
            }
        }
        None
    }
}

#[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
pub enum EventMessage {
    ProjectEvent(project::Event),
    IssueEvent(issue::Event),
    RequirementEvent(requirement::Event),
    CodeEvent(code::Event),
    IdeaEvent(idea::Event),
    EntryEvent(entry::Event),
    TestCaseEvent(testcase::Event),
    NoopEvent(),
}

use proto_gen_rust::google::protobuf::Any;

pub fn decode_event(data: &Any) -> Option<EventMessage> {
    if let Some(ret) = project::decode_event(data) {
        return Some(EventMessage::ProjectEvent(ret));
    }
    if let Some(ret) = issue::decode_event(data) {
        return Some(EventMessage::IssueEvent(ret));
    }
    if let Some(ret) = requirement::decode_event(data) {
        return Some(EventMessage::RequirementEvent(ret));
    }
    if let Some(ret) = code::decode_event(data) {
        return Some(EventMessage::CodeEvent(ret));
    }
    if let Some(ret) = idea::decode_event(data) {
        return Some(EventMessage::IdeaEvent(ret));
    }
    if let Some(ret) = entry::decode_event(data) {
        return Some(EventMessage::EntryEvent(ret));
    }
    if let Some(ret) = testcase::decode_event(data) {
        return Some(EventMessage::TestCaseEvent(ret));
    }
    Some(EventMessage::NoopEvent())
}
