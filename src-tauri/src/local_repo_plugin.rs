//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

use crate::user_api::user_api_plugin::get_user_id;
use base64ct::LineEnding;
use ssh_key::{
    private::{KeypairData, RsaKeypair},
    PrivateKey,
};
use tauri::{
    plugin::{Plugin, Result as PluginResult},
    AppHandle, Invoke, Manager, PageLoadPayload, Runtime, Window,
};
use tokio::fs;
use tokio::io::AsyncReadExt;
use tokio::io::AsyncWriteExt;
use tokio::sync::RwLock;

#[derive(Default)]
pub struct FileLock(pub RwLock<i32>);

#[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
pub struct LocalRepoInfo {
    pub id: String,
    pub name: String,
    pub path: String,
    pub color: Option<String>,
}

#[derive(serde::Serialize, serde::Deserialize, Clone, PartialEq)]
pub struct SshKeyPairInfo {
    pub pub_key: String,
    pub priv_key: String,
}

async fn load_repo_data<R: Runtime>(
    app_handle: AppHandle<R>,
) -> Result<Vec<LocalRepoInfo>, String> {
    let _l = app_handle.state::<FileLock>().inner().0.read().await;

    let user_dir = crate::get_user_dir();
    if user_dir.is_none() {
        return Err("miss user dir".into());
    }
    let mut file_path = std::path::PathBuf::from(user_dir.unwrap());
    file_path.push("all");
    file_path.push("local_repo.json");
    if !file_path.exists() {
        return Ok(Vec::new());
    }
    let f = fs::File::open(file_path).await;
    if f.is_err() {
        return Err(f.err().unwrap().to_string());
    }
    let mut f = f.unwrap();
    let mut data = Vec::new();
    let result = f.read_to_end(&mut data).await;
    if result.is_err() {
        return Err(result.err().unwrap().to_string());
    }
    let json_str = String::from_utf8(data);
    if json_str.is_err() {
        return Err(json_str.err().unwrap().to_string());
    }
    let json_str = json_str.unwrap();
    let repo_list = serde_json::from_str(&json_str);
    if repo_list.is_err() {
        return Err(repo_list.err().unwrap().to_string());
    }
    return Ok(repo_list.unwrap());
}

async fn save_repo_data<R: Runtime>(
    app_handle: AppHandle<R>,
    repo_list: &Vec<LocalRepoInfo>,
) -> Result<(), String> {
    let _l = app_handle.state::<FileLock>().inner().0.write().await;

    let user_dir = crate::get_user_dir();
    if user_dir.is_none() {
        return Err("miss user dir".into());
    }
    let mut file_path = std::path::PathBuf::from(user_dir.unwrap());
    file_path.push("all");
    if !file_path.exists() {
        let result = fs::create_dir_all(&file_path).await;
        if result.is_err() {
            return Err(result.err().unwrap().to_string());
        }
    }
    file_path.push("local_repo.json");

    let json_str = serde_json::to_string(&repo_list);
    if json_str.is_err() {
        return Err(json_str.err().unwrap().to_string());
    }
    let json_str = json_str.unwrap();

    let f = fs::File::create(file_path).await;
    if f.is_err() {
        return Err(f.err().unwrap().to_string());
    }
    let mut f = f.unwrap();
    let result = f.write_all(json_str.as_bytes()).await;
    if result.is_err() {
        return Err(result.err().unwrap().to_string());
    }

    return Ok(());
}

#[tauri::command]
async fn add_repo<R: Runtime>(
    app_handle: AppHandle<R>,
    id: String,
    name: String,
    path: String,
    color: String,
) -> Result<(), String> {
    let tmp_list = load_repo_data(app_handle.clone()).await;
    if tmp_list.is_err() {
        return Err(tmp_list.err().unwrap());
    }
    let tmp_list = tmp_list.unwrap();
    for repo in &tmp_list {
        if repo.id == id {
            return Ok(());
        }
    }
    let mut repo_list = vec![LocalRepoInfo {
        id: id,
        name: name,
        path: path,
        color: Some(color),
    }];
    repo_list.extend(tmp_list);
    return save_repo_data(app_handle, &repo_list).await;
}

#[tauri::command]
async fn set_top<R: Runtime>(app_handle: AppHandle<R>, id: String) -> Result<(), String> {
    let tmp_list = load_repo_data(app_handle.clone()).await;
    if tmp_list.is_err() {
        return Err(tmp_list.err().unwrap());
    }
    let tmp_list = tmp_list.unwrap();
    let mut repo_list = Vec::new();
    for repo in &tmp_list {
        if repo.id == id {
            repo_list.push(repo.clone());
        }
    }
    for repo in &tmp_list {
        if repo.id != id {
            repo_list.push(repo.clone());
        }
    }
    return save_repo_data(app_handle, &repo_list).await;
}

#[tauri::command]
async fn update_repo<R: Runtime>(
    app_handle: AppHandle<R>,
    id: String,
    name: String,
    path: String,
    color: String,
) -> Result<(), String> {
    let repo_list = load_repo_data(app_handle.clone()).await;
    if repo_list.is_err() {
        return Err(repo_list.err().unwrap());
    }
    let repo_list = repo_list.unwrap();
    let mut new_repo_list = Vec::new();
    for repo in &repo_list {
        if repo.id == id {
            new_repo_list.push(LocalRepoInfo {
                id: id.clone(),
                name: name.clone(),
                path: path.clone(),
                color: Some(color.clone()),
            });
        } else {
            new_repo_list.push(repo.clone());
        }
    }

    return save_repo_data(app_handle, &new_repo_list).await;
}

//删除本地仓库
#[tauri::command]
async fn remove_repo<R: Runtime>(app_handle: AppHandle<R>, id: String) -> Result<(), String> {
    let repo_list = load_repo_data(app_handle.clone()).await;
    if repo_list.is_err() {
        return Err(repo_list.err().unwrap());
    }
    let repo_list = repo_list.unwrap();
    let mut new_repo_list = Vec::new();
    for repo in &repo_list {
        if repo.id != id {
            new_repo_list.push(repo.clone());
        }
    }
    return save_repo_data(app_handle, &new_repo_list).await;
}

//列出本地仓库
#[tauri::command]
async fn list_repo<R: Runtime>(app_handle: AppHandle<R>) -> Result<Vec<LocalRepoInfo>, String> {
    return load_repo_data(app_handle).await;
}

#[tauri::command]
async fn get_assistant_mode<R: Runtime>(app_handle: AppHandle<R>) -> Result<String, String> {
    let _l = app_handle.state::<FileLock>().inner().0.read().await;

    let user_dir = crate::get_user_dir();
    if user_dir.is_none() {
        return Err("miss user dir".into());
    }

    let mut file_path = std::path::PathBuf::from(user_dir.unwrap());

    let user_id = get_user_id(app_handle.clone()).await;
    if user_id == "" {
        file_path.push("all");
    } else {
        file_path.push(user_id);
    }
    file_path.push("assistant_mode");

    if !file_path.exists() {
        return Ok("simple".into());
    }
    let f = fs::File::open(file_path).await;
    if f.is_err() {
        return Err(f.err().unwrap().to_string());
    }
    let mut f = f.unwrap();
    let mut data = Vec::new();
    let result = f.read_to_end(&mut data).await;
    if result.is_err() {
        return Err(result.err().unwrap().to_string());
    }
    let mode = String::from_utf8(data);
    if mode.is_err() {
        return Err(mode.err().unwrap().to_string());
    }
    return Ok(mode.unwrap());
}

#[tauri::command]
async fn set_assistant_mode<R: Runtime>(
    app_handle: AppHandle<R>,
    mode: String,
) -> Result<(), String> {
    let _l = app_handle.state::<FileLock>().inner().0.write().await;

    let user_dir = crate::get_user_dir();
    if user_dir.is_none() {
        return Err("miss user dir".into());
    }

    let mut file_path = std::path::PathBuf::from(user_dir.unwrap());

    let user_id = get_user_id(app_handle.clone()).await;
    if user_id == "" {
        file_path.push("all");
    } else {
        file_path.push(user_id);
    }
    if !file_path.exists() {
        let result = fs::create_dir_all(&file_path).await;
        if result.is_err() {
            return Err(result.err().unwrap().to_string());
        }
    }
    file_path.push("assistant_mode");

    let f = fs::File::create(file_path).await;
    if f.is_err() {
        return Err(f.err().unwrap().to_string());
    }
    let mut f = f.unwrap();
    let result = f.write_all(mode.as_bytes()).await;
    if result.is_err() {
        return Err(result.err().unwrap().to_string());
    }

    return Ok(());
}

#[tauri::command]
async fn gen_ssh_key() -> Result<SshKeyPairInfo, String> {
    let mut rng = rand::thread_rng();
    let key_pair = RsaKeypair::random(&mut rng, 2048);
    if key_pair.is_err() {
        return Err(key_pair.err().unwrap().to_string());
    }
    let priv_key = PrivateKey::new(KeypairData::Rsa(key_pair.unwrap()), "linksaas");
    if priv_key.is_err() {
        return Err(priv_key.err().unwrap().to_string());
    }
    let priv_key = priv_key.unwrap();
    let priv_key_str = priv_key.to_openssh(LineEnding::LF);
    if priv_key_str.is_err() {
        return Err(priv_key_str.err().unwrap().to_string());
    }
    let pub_key = priv_key.public_key();
    let pub_key_str = pub_key.to_openssh();
    if pub_key_str.is_err() {
        return Err(pub_key_str.err().unwrap().to_string());
    }

    return Ok(SshKeyPairInfo {
        priv_key: priv_key_str.unwrap().to_string(),
        pub_key: pub_key_str.unwrap(),
    });
}

pub struct LocalRepoPlugin<R: Runtime> {
    invoke_handler: Box<dyn Fn(Invoke<R>) + Send + Sync + 'static>,
}

impl<R: Runtime> LocalRepoPlugin<R> {
    pub fn new() -> Self {
        Self {
            invoke_handler: Box::new(tauri::generate_handler![
                add_repo,
                set_top,
                update_repo,
                remove_repo,
                list_repo,
                gen_ssh_key,
                get_assistant_mode,
                set_assistant_mode,
            ]),
        }
    }
}

impl<R: Runtime> Plugin<R> for LocalRepoPlugin<R> {
    fn name(&self) -> &'static str {
        "local_repo"
    }
    fn initialization_script(&self) -> Option<String> {
        None
    }

    fn initialize(&mut self, app: &AppHandle<R>, _config: serde_json::Value) -> PluginResult<()> {
        app.manage(FileLock(Default::default()));
        Ok(())
    }

    fn created(&mut self, _window: Window<R>) {}

    fn on_page_load(&mut self, _window: Window<R>, _payload: PageLoadPayload) {}

    fn extend_api(&mut self, message: Invoke<R>) {
        (self.invoke_handler)(message)
    }
}
