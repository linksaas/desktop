//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

use crate::user_api::user_api_plugin::{get_session_inner, get_user_id_inner};
use local_api_rust::models::{BugInfo, BugInfoLevel, BugInfoPriority, BugInfoState, IssueInfo as ModelIssueInfo, IssueInfoIssueType, SubTaskInfo, TaskInfo, TaskInfoPriority};
use proto_gen_rust::project_issue_api::issue_info::ExtraInfo;
use proto_gen_rust::project_issue_api::project_issue_api_client::ProjectIssueApiClient;
use proto_gen_rust::project_issue_api::*;
use tauri::AppHandle;

pub async fn list_my_issue(
    app: &AppHandle,
    project_id: &String,
    issue_type: i32,
    state: &String,
) -> Result<ListResponse, String> {
    let chan = crate::get_grpc_chan(app).await;
    if chan.is_none() {
        return Err("grpc连接出错".into());
    }
    let mut state_list = Vec::new();
    if state == "closed" {
        state_list.push(IssueState::Close as i32);
    } else if state == "unclose" {
        state_list.push(IssueState::Plan as i32);
        state_list.push(IssueState::Process as i32);
        state_list.push(IssueState::Check as i32);
    }
    let mut client = ProjectIssueApiClient::new(chan.unwrap());
    let res = client
        .list(ListRequest {
            session_id: get_session_inner(app).await,
            project_id: project_id.clone(),
            list_param: Some(ListParam {
                filter_by_issue_type: true,
                issue_type: issue_type,
                filter_by_state: state_list.len() > 0,
                state_list: state_list,
                filter_by_create_user_id: false,
                create_user_id_list: Vec::new(),
                filter_by_assgin_user_id: true,
                assgin_user_id_list: vec![get_user_id_inner(app).await],
                assgin_user_type: AssginUserType::AssginUserAll as i32,
                filter_by_sprit_id: false,
                sprit_id_list: Vec::new(),
                filter_by_create_time: false,
                from_create_time: 0,
                to_create_time: 0,
                filter_by_update_time: false,
                from_update_time: 0,
                to_update_time: 0,
                filter_by_title_keyword: false,
                title_keyword: "".into(),
                filter_by_task_priority: false,
                task_priority_list: Vec::new(),
                filter_by_software_version: false,
                software_version_list: Vec::new(),
                filter_by_bug_priority: false,
                bug_priority_list: Vec::new(),
                filter_by_bug_level: false,
                bug_level_list: Vec::new(),
                filter_by_tag_id_list: false,
                tag_id_list: Vec::new(),
                filter_by_watch: false,
            }),
            sort_type: SortType::Dsc as i32,
            sort_key: SortKey::UpdateTime as i32,
            offset: 0,
            limit: 999,
        })
        .await;
    if res.is_err() {
        return Err("调用接口出错".into());
    } else {
        return Ok(res.unwrap().into_inner());
    }
}

pub async fn list_issue(
    app: &AppHandle,
    project_id: &String,
    issue_type: i32,
    offset: u32,
    limit: u32,
) -> Result<ListResponse, String> {
    let chan = crate::get_grpc_chan(app).await;
    if chan.is_none() {
        return Err("grpc连接出错".into());
    }
    let mut client = ProjectIssueApiClient::new(chan.unwrap());
    let res = client
        .list(ListRequest {
            session_id: get_session_inner(app).await,
            project_id: project_id.clone(),
            list_param: Some(ListParam {
                filter_by_issue_type: true,
                issue_type: issue_type,
                filter_by_state: false,
                state_list: Vec::new(),
                filter_by_create_user_id: false,
                create_user_id_list: Vec::new(),
                filter_by_assgin_user_id: false,
                assgin_user_id_list: Vec::new(),
                assgin_user_type: AssginUserType::AssginUserAll as i32,
                filter_by_sprit_id: false,
                sprit_id_list: Vec::new(),
                filter_by_create_time: false,
                from_create_time: 0,
                to_create_time: 0,
                filter_by_update_time: false,
                from_update_time: 0,
                to_update_time: 0,
                filter_by_title_keyword: false,
                title_keyword: "".into(),
                filter_by_task_priority: false,
                task_priority_list: Vec::new(),
                filter_by_software_version: false,
                software_version_list: Vec::new(),
                filter_by_bug_priority: false,
                bug_priority_list: Vec::new(),
                filter_by_bug_level: false,
                bug_level_list: Vec::new(),
                filter_by_tag_id_list: false,
                tag_id_list: Vec::new(),
                filter_by_watch: false,
            }),
            sort_type: SortType::Dsc as i32,
            sort_key: SortKey::UpdateTime as i32,
            offset: offset,
            limit: limit,
        })
        .await;
    if res.is_err() {
        return Err("调用接口出错".into());
    } else {
        return Ok(res.unwrap().into_inner());
    }
}

pub async fn list_sub_task(
    app: &AppHandle,
    project_id: &String,
    task_id: &String,
) -> Result<ListSubIssueResponse, String> {
    let chan = crate::get_grpc_chan(app).await;
    if chan.is_none() {
        return Err("grpc连接出错".into());
    }
    let mut client = ProjectIssueApiClient::new(chan.unwrap());
    let res = client
        .list_sub_issue(ListSubIssueRequest {
            session_id: get_session_inner(app).await,
            project_id: project_id.clone(),
            issue_id: task_id.clone(),
        })
        .await;
    if res.is_err() {
        return Err("调用接口出错".into());
    } else {
        return Ok(res.unwrap().into_inner());
    }
}

pub async fn list_my_depend(
    app: &AppHandle,
    project_id: &String,
    task_id: &String,
) -> Result<ListMyDependResponse, String> {
    let chan = crate::get_grpc_chan(app).await;
    if chan.is_none() {
        return Err("grpc连接出错".into());
    }
    let mut client = ProjectIssueApiClient::new(chan.unwrap());
    let res = client
        .list_my_depend(ListMyDependRequest {
            session_id: get_session_inner(app).await,
            project_id: project_id.clone(),
            issue_id: task_id.clone(),
        })
        .await;
    if res.is_err() {
        return Err("调用接口出错".into());
    } else {
        return Ok(res.unwrap().into_inner());
    }
}

pub async fn list_depend_me(
    app: &AppHandle,
    project_id: &String,
    task_id: &String,
) -> Result<ListDependMeResponse, String> {
    let chan = crate::get_grpc_chan(app).await;
    if chan.is_none() {
        return Err("grpc连接出错".into());
    }
    let mut client = ProjectIssueApiClient::new(chan.unwrap());
    let res = client
        .list_depend_me(ListDependMeRequest {
            session_id: get_session_inner(app).await,
            project_id: project_id.clone(),
            issue_id: task_id.clone(),
        })
        .await;
    if res.is_err() {
        return Err("调用接口出错".into());
    } else {
        return Ok(res.unwrap().into_inner());
    }
}

pub fn convert_to_issue_list(issue_list: Vec<IssueInfo>) -> Vec<ModelIssueInfo> {
    let mut ret_list = Vec::new();
    issue_list.iter().for_each(|issue| {
        let basic_info = issue.basic_info.clone().unwrap_or_else(|| {
            return BasicIssueInfo {
                title: "".into(),
                content: "".into(),
                tag_id_list: Vec::new(),
            };
        });
        let mut issue_type = IssueInfoIssueType::Task;
        if issue.issue_type == IssueType::Bug as i32 {
            issue_type = IssueInfoIssueType::Bug;
        } else if issue.issue_type == IssueType::Task as i32 {
            issue_type = IssueInfoIssueType::Task;
        }
        let mut state  = BugInfoState::Plan;
        if issue.state == IssueState::Plan as i32 {
            state = BugInfoState::Plan;
        } else if issue.state == IssueState::Process as i32 {
            state = BugInfoState::Process;
        } else if issue.state == IssueState::Check as i32 {
            state = BugInfoState::Check;
        } else if issue.state == IssueState::Close as i32 {
            state = BugInfoState::Close;
        }
        ret_list.push(ModelIssueInfo {
            issue_id: Some(issue.issue_id.clone()),
            issue_type: Some(issue_type),
            title: Some(basic_info.title.clone()),
            state: Some(state),
            create_user_id: Some(issue.create_user_id.clone()),
            create_display_name: Some(issue.create_display_name.clone()),
            exec_user_id: Some(issue.exec_user_id.clone()),
            exec_display_name: Some(issue.exec_display_name.clone()),
            check_user_id: Some(issue.check_user_id.clone()),
            check_display_name: Some(issue.check_display_name.clone()),
            exec_award_point: Some(0),
            check_award_point: Some(0),
            create_time: Some(issue.create_time),
            update_time: Some(issue.update_time),
        });
    });
    return ret_list;
}

pub fn convert_to_sub_task_list(sub_task_list: Vec<SubIssueInfo>) -> Vec<SubTaskInfo> {
    let mut ret_list = Vec::new();
    sub_task_list.iter().for_each(|sub_task| {
        let basic_info = sub_task
            .basic_info
            .clone()
            .unwrap_or_else(|| return BasicSubIssueInfo { title: "".into() });
        ret_list.push(SubTaskInfo {
            sub_task_id: Some(sub_task.sub_issue_id.clone()),
            task_id: Some(sub_task.issue_id.clone()),
            title: Some(basic_info.title.clone()),
            create_user_id: Some(sub_task.create_user_id.clone()),
            create_display_name: Some(sub_task.create_display_name.clone()),
            done: Some(sub_task.done),
            create_time: Some(sub_task.create_time),
            update_time: Some(sub_task.update_time),
        });
    });
    return ret_list;
}

//只转换类型是bug的类型
pub fn convert_to_bug_list(issue_list: Vec<IssueInfo>) -> Vec<BugInfo> {
    let mut ret_list = Vec::new();
    issue_list.iter().for_each(|issue| {
        if issue.issue_type == IssueType::Bug as i32 {
            let basic_info = issue.basic_info.clone().unwrap_or_else(|| {
                return BasicIssueInfo {
                    title: "".into(),
                    content: "".into(),
                    tag_id_list: Vec::new(),
                };
            });
            let mut state = BugInfoState::Plan;
            if issue.state == IssueState::Plan as i32 {
                state = BugInfoState::Plan;
            } else if issue.state == IssueState::Process as i32 {
                state = BugInfoState::Process;
            } else if issue.state == IssueState::Check as i32 {
                state = BugInfoState::Check;
            } else if issue.state == IssueState::Close as i32 {
                state = BugInfoState::Close;
            }
            let mut software_version = String::from("");
            let mut level = BugInfoLevel::Minor;
            let mut priority = BugInfoPriority::Low;
            if let Some(extra_info) = &issue.extra_info {
                if let ExtraInfo::ExtraBugInfo(info) = extra_info {
                    software_version = info.software_version.clone();
                    if info.level == BugLevel::Minor as i32 {
                        level = BugInfoLevel::Minor;
                    } else if info.level == BugLevel::Major as i32 {
                        level = BugInfoLevel::Major;
                    } else if info.level == BugLevel::Critical as i32 {
                        level = BugInfoLevel::Critical;
                    } else if info.level == BugLevel::Blocker as i32 {
                        level = BugInfoLevel::Blocker;
                    }
                    if info.priority == BugPriority::Low as i32 {
                        priority = BugInfoPriority::Low;
                    } else if info.priority == BugPriority::Normal as i32 {
                        priority = BugInfoPriority::Normal;
                    } else if info.priority == BugPriority::High as i32 {
                        priority = BugInfoPriority::High;
                    } else if info.priority == BugPriority::Urgent as i32 {
                        priority = BugInfoPriority::Urgent;
                    } else if info.priority == BugPriority::Immediate as i32 {
                        priority = BugInfoPriority::Immediate;
                    }
                }
            }
            ret_list.push(BugInfo {
                bug_id: Some(issue.issue_id.clone()),
                title: Some(basic_info.title.clone()),
                state: Some(state),
                create_user_id: Some(issue.create_user_id.clone()),
                create_display_name: Some(issue.create_display_name.clone()),
                exec_user_id: Some(issue.exec_user_id.clone()),
                exec_display_name: Some(issue.exec_display_name.clone()),
                check_user_id: Some(issue.check_user_id.clone()),
                check_display_name: Some(issue.check_display_name.clone()),
                exec_award_point: Some(0),
                check_award_point: Some(0),
                create_time: Some(issue.create_time),
                update_time: Some(issue.update_time),
                software_version: Some(software_version),
                level: Some(level),
                priority: Some(priority),
            });
        }
    });
    return ret_list;
}

//只转换类型是task的类型
pub fn convert_to_task_list(issue_list: Vec<IssueInfo>) -> Vec<TaskInfo> {
    let mut ret_list = Vec::new();
    issue_list.iter().for_each(|issue| {
        if issue.issue_type == IssueType::Task as i32 {
            let basic_info = issue.basic_info.clone().unwrap_or_else(|| {
                return BasicIssueInfo {
                    title: "".into(),
                    content: "".into(),
                    tag_id_list: Vec::new(),
                };
            });
            let mut state = BugInfoState::Plan;
            if issue.state == IssueState::Plan as i32 {
                state = BugInfoState::Plan;
            } else if issue.state == IssueState::Process as i32 {
                state = BugInfoState::Process;
            } else if issue.state == IssueState::Check as i32 {
                state = BugInfoState::Check;
            } else if issue.state == IssueState::Close as i32 {
                state = BugInfoState::Close;
            }
            let mut priority = TaskInfoPriority::Low;
            if let Some(extra_info) = &issue.extra_info {
                if let ExtraInfo::ExtraTaskInfo(info) = extra_info {
                    if info.priority == TaskPriority::Low as i32 {
                        priority = TaskInfoPriority::Low;
                    } else if info.priority == TaskPriority::Middle as i32 {
                        priority = TaskInfoPriority::Middle;
                    } else if info.priority == TaskPriority::High as i32 {
                        priority = TaskInfoPriority::High;
                    }
                }
            }
            ret_list.push(TaskInfo {
                task_id: Some(issue.issue_id.clone()),
                title: Some(basic_info.title.clone()),
                state: Some(state),
                create_user_id: Some(issue.create_user_id.clone()),
                create_display_name: Some(issue.create_display_name.clone()),
                exec_user_id: Some(issue.exec_user_id.clone()),
                exec_display_name: Some(issue.exec_display_name.clone()),
                check_user_id: Some(issue.check_user_id.clone()),
                check_display_name: Some(issue.check_display_name.clone()),
                exec_award_point: Some(0),
                check_award_point: Some(0),
                create_time: Some(issue.create_time),
                update_time: Some(issue.update_time),
                priority: Some(priority),
            });
        }
    });
    return ret_list;
}
