//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

use crate::user_api::user_api_plugin::get_session_inner;
use local_api_rust::models::{self, EventInfoEventType, EventInfoRefType};
use proto_gen_rust::events_api::events_api_client::EventsApiClient;
use proto_gen_rust::events_api::*;
use serde_json::json;
use tauri::AppHandle;

pub async fn add_custom_event(
    app: &AppHandle,
    project_id: &String,
    ev_type: &String,
    ev_content: &String,
) -> Result<(), String> {
    let chan = crate::get_grpc_chan(app).await;
    if chan.is_none() {
        return Err("grpc连接出错".into());
    }
    let mut client = EventsApiClient::new(chan.unwrap());
    let res = client
        .add_custom_event(AddCustomEventRequest {
            session_id: get_session_inner(app).await,
            project_id: project_id.clone(),
            event_type: ev_type.clone(),
            event_content: ev_content.clone(),
        })
        .await;
    if res.is_err() {
        return Err("调用接口出错".into());
    } else {
        let res = res.unwrap().into_inner();
        if res.code != 0 {
            return Err(res.err_msg);
        }
        return Ok(());
    }
}

pub async fn list_project_event(
    app: &AppHandle,
    project_id: &String,
    member_user_id: &String,
    from_time: i64,
    to_time: i64,
    offset: u32,
    limit: u32,
) -> Result<ListProjectEventResponse, String> {
    let chan = crate::get_grpc_chan(app).await;
    if chan.is_none() {
        return Err("grpc连接出错".into());
    }
    let mut client = EventsApiClient::new(chan.unwrap());
    let res = client
        .list_project_event(ListProjectEventRequest {
            session_id: get_session_inner(app).await,
            project_id: project_id.clone(),
            filter_by_member_user_id: member_user_id.eq("") == false,
            member_user_id: member_user_id.clone(),
            from_time: from_time,
            to_time: to_time,
            offset: offset,
            limit: limit,
        })
        .await;
    if res.is_err() {
        return Err("调用接口出错".into());
    } else {
        return Ok(res.unwrap().into_inner());
    }
}

pub async fn list_event_by_ref(
    app: &AppHandle,
    project_id: &String,
    event_type: EventType,
    ref_type: EventRefType,
    ref_id: &String,
) -> Result<ListEventByRefResponse, String> {
    let chan = crate::get_grpc_chan(app).await;
    if chan.is_none() {
        return Err("grpc连接出错".into());
    }
    let mut client = EventsApiClient::new(chan.unwrap());
    let res = client
        .list_event_by_ref(ListEventByRefRequest {
            session_id: get_session_inner(app).await,
            project_id: project_id.clone(),
            event_type: event_type as i32,
            ref_type: ref_type as i32,
            ref_id: ref_id.clone(),
        })
        .await;
    if res.is_err() {
        return Err("调用接口出错".into());
    } else {
        return Ok(res.unwrap().into_inner());
    }
}

pub fn convert_event_list(event_list: Vec<Event>) -> Vec<models::EventInfo> {
    let mut ret_list = Vec::new();
    event_list.iter().for_each(|item| {
        let mut event_type = EventInfoEventType::User;
        if item.event_type == EventType::User as i32 {
            event_type = EventInfoEventType::User;
        } else if item.event_type == EventType::Project as i32 {
            event_type = EventInfoEventType::Project;
        } else if item.event_type == EventType::Task as i32 {
            event_type = EventInfoEventType::Task;
        } else if item.event_type == EventType::Bug as i32 {
            event_type = EventInfoEventType::Bug;
        } else if item.event_type == EventType::Sprit as i32 {
            event_type = EventInfoEventType::Sprit;
        } else if item.event_type == EventType::Doc as i32 {
            event_type = EventInfoEventType::Doc;
        } else if item.event_type == EventType::Disk as i32 {
            event_type = EventInfoEventType::Disk;
        } else if item.event_type == EventType::RequireMent as i32 {
            event_type = EventInfoEventType::Requirement;
        } else if item.event_type == EventType::Code as i32 {
            event_type = EventInfoEventType::Code;
        } else if item.event_type == EventType::Idea as i32 {
            event_type = EventInfoEventType::Idea;
        } else if item.event_type == EventType::ApiCollection as i32 {
            event_type = EventInfoEventType::ApiColl;
        } else if item.event_type == EventType::Entry as i32 {
            event_type = EventInfoEventType::Entry;
        } else if item.event_type == EventType::TestCase as i32 {
            event_type = EventInfoEventType::Testcase;
        } else if item.event_type == EventType::Custom as i32 {
            event_type = EventInfoEventType::Custom;
        }

        let mut ref_type = EventInfoRefType::None;
        if item.ref_type == EventRefType::None as i32 {
            ref_type = EventInfoRefType::None;
        } else if item.ref_type == EventRefType::User as i32 {
            ref_type = EventInfoRefType::User;
        } else if item.ref_type == EventRefType::Project as i32 {
            ref_type = EventInfoRefType::Project;
        } else if item.ref_type == EventRefType::Sprit as i32 {
            ref_type = EventInfoRefType::Sprit;
        } else if item.ref_type == EventRefType::Task as i32 {
            ref_type = EventInfoRefType::Task;
        } else if item.ref_type == EventRefType::Bug as i32 {
            ref_type = EventInfoRefType::Bug;
        } else if item.ref_type == EventRefType::Doc as i32 {
            ref_type = EventInfoRefType::Doc;
        } else if item.ref_type == EventRefType::RequireMent as i32 {
            ref_type = EventInfoRefType::Requirement;
        } else if item.ref_type == EventRefType::CodeCommentThread as i32 {
            ref_type = EventInfoRefType::CodeCommentThread;
        } else if item.ref_type == EventRefType::Idea as i32 {
            ref_type = EventInfoRefType::Idea;
        } else if item.ref_type == EventRefType::ApiCollection as i32 {
            ref_type = EventInfoRefType::ApiColl;
        }

        let mut event_data = json!({});
        if let Some(data) = &item.event_data {
            let ev = crate::events_decode::decode_event(data);
            if let Some(ev) = ev {
                if let Ok(value) = serde_json::to_value(ev) {
                    event_data = value;
                }
            }
        }

        ret_list.push(models::EventInfo {
            event_id: Some(item.event_id.clone()),
            user_id: Some(item.user_id.clone()),
            user_display_name: Some(item.user_display_name.clone()),
            event_type: Some(event_type),
            event_time: Some(item.event_time),
            ref_type: Some(ref_type),
            ref_id: Some(item.ref_id.clone()),
            event_data: Some(event_data),
        });
    });
    return ret_list;
}
