//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

use crate::user_api::user_api_plugin::get_session_inner;
use local_api_rust::models::ProjectProjectIdEntryEntryTypeGetEntryTypeParameter;
use proto_gen_rust::project_entry_api::project_entry_api_client::ProjectEntryApiClient;
use proto_gen_rust::project_entry_api::*;
use tauri::AppHandle;

pub async fn list_entry(
    app: &AppHandle,
    project_id: &String,
    entry_type: ProjectProjectIdEntryEntryTypeGetEntryTypeParameter,
) -> Result<ListResponse, String> {
    let chan = crate::get_grpc_chan(app).await;
    if chan.is_none() {
        return Err("grpc连接出错".into());
    }

    let mut entry_type_value = 0 as i32;
    if entry_type == ProjectProjectIdEntryEntryTypeGetEntryTypeParameter::WorkPlan {
        entry_type_value = EntryType::Sprit as i32;
    }else if entry_type == ProjectProjectIdEntryEntryTypeGetEntryTypeParameter::Doc {
        entry_type_value = EntryType::Doc as i32;
    }else if entry_type == ProjectProjectIdEntryEntryTypeGetEntryTypeParameter::Pages {
        entry_type_value = EntryType::Pages as i32;
    }else if entry_type == ProjectProjectIdEntryEntryTypeGetEntryTypeParameter::ApiColl {
        entry_type_value = EntryType::ApiColl as i32;
    }else if entry_type == ProjectProjectIdEntryEntryTypeGetEntryTypeParameter::Draw {
        entry_type_value = EntryType::Draw as i32;
    }
    let mut client = ProjectEntryApiClient::new(chan.unwrap());
    let res = client
        .list(ListRequest {
            session_id: get_session_inner(app).await,
            project_id: project_id.clone(),
            list_param: Some(ListParam {
                filter_by_watch: false,
                filter_by_tag_id: false,
                tag_id_list: Vec::new(),
                filter_by_keyword: false,
                keyword: "".into(),
                filter_by_entry_type: true,
                entry_type_list: vec![entry_type_value],
            }),
            offset: 0,
            limit: 20,
        })
        .await;
    if res.is_err() {
        return Err("调用接口出错".into());
    } else {
        return Ok(res.unwrap().into_inner());
    }
}
