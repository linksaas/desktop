//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

use reqwest::blocking::ClientBuilder;
use std::io::Read;
use tauri::async_runtime::Mutex;
use tauri::{
    plugin::{Plugin, Result as PluginResult},
    AppHandle, Invoke, Manager, PageLoadPayload, Runtime, Window,
};

mod entry_api;
mod event_api;
mod issue_api;
mod notice;
mod project_api;
mod project_code_api;
mod server;

#[derive(Default)]
pub struct ServPort(Mutex<Option<i16>>);

#[derive(Default)]
pub struct ServToken(Mutex<Option<String>>);

#[tauri::command]
pub fn remove_info_file() {
    if let Some(home_dir) = dirs::home_dir() {
        let file_path = format!("{}/.linksaas/local_api", home_dir.to_str().unwrap());
        if let Err(err) = std::fs::remove_file(file_path) {
            println!("{}", err);
        }
    }
}

#[tauri::command]
async fn get_port<R: Runtime>(app_handle: AppHandle<R>) -> i16 {
    let port = app_handle.state::<ServPort>().inner();
    if let Some(value) = port.0.lock().await.clone() {
        return value;
    }
    return 0;
}

#[tauri::command]
async fn get_token<R: Runtime>(app_handle: AppHandle<R>) -> String {
    let token = app_handle.state::<ServToken>().inner();
    if let Some(value) = token.0.lock().await.clone() {
        return value;
    }
    return String::from("");
}

pub fn is_instance_run() -> bool {
    let home_dir = dirs::home_dir();
    if home_dir.is_none() {
        return false;
    }
    let home_dir = home_dir.unwrap();
    let file_path = format!("{}/.linksaas/local_api", home_dir.to_str().unwrap());
    let file = std::fs::OpenOptions::new().read(true).open(file_path);
    if file.is_err() {
        return false;
    }
    let mut file = file.unwrap();
    let mut data: Vec<u8> = Vec::new();
    if let Ok(_) = file.read_to_end(&mut data) {
        if let Ok(line) = String::from_utf8(data) {
            let parts: Vec<String> = line.split(" ").map(|x| String::from(x)).collect();
            let mut addr = String::from("");
            let mut token = String::from("");
            if parts.len() >= 1 {
                addr.clone_from(&parts[0]);
            }
            if parts.len() >= 2 {
                token.clone_from(&parts[1]);
            }
            let builder = ClientBuilder::new();
            let client = builder.build();
            if client.is_err() {
                return false;
            }
            let client = client.unwrap();
            let hello_res = client
                .get(format!("http://{}/hello?accessToken={}", &addr, &token))
                .send();
            if hello_res.is_err() {
                return false;
            }
            let hello_res = hello_res.unwrap();
            let hello_res_str = hello_res.text_with_charset("utf8");
            if hello_res_str.is_err() {
                return false;
            }
            let hello_res_str = hello_res_str.unwrap();
            if hello_res_str.eq("hello linksaas") == false {
                return false;
            }
            //调用show
            let show_res = client
                .get(format!("http://{}/show?accessToken={}", &addr, &token))
                .send();
            if show_res.is_err() {
                println!("{:?}", show_res.err().unwrap());
            }
            return true;
        }
    }
    return false;
}

pub struct LocalApiPlugin {
    invoke_handler: Box<dyn Fn(Invoke) + Send + Sync + 'static>,
}

impl LocalApiPlugin {
    pub fn new() -> Self {
        Self {
            invoke_handler: Box::new(tauri::generate_handler![
                remove_info_file,
                get_port,
                get_token
            ]),
        }
    }
}

impl Plugin<tauri::Wry> for LocalApiPlugin {
    fn name(&self) -> &'static str {
        "local_api"
    }
    fn initialization_script(&self) -> Option<String> {
        None
    }

    fn initialize(&mut self, app: &AppHandle, _config: serde_json::Value) -> PluginResult<()> {
        let handle = app.clone();
        app.manage(ServPort(Default::default()));
        app.manage(ServToken(Default::default()));
        tauri::async_runtime::spawn(async move {
            server::run(handle).await;
        });
        Ok(())
    }

    fn created(&mut self, _window: Window) {}

    fn on_page_load(&mut self, _window: Window, _payload: PageLoadPayload) {}

    fn extend_api(&mut self, message: Invoke) {
        (self.invoke_handler)(message)
    }
}
