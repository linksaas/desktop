//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

use crate::notice_decode::new_wrong_session_notice;
use proto_gen_rust::roadmap_content_api::roadmap_content_api_client::RoadmapContentApiClient;
use proto_gen_rust::roadmap_content_api::*;
use tauri::{
    plugin::{Plugin, Result as PluginResult},
    AppHandle, Invoke, PageLoadPayload, Runtime, Window,
};

#[tauri::command]
async fn add_node<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: AddNodeRequest,
) -> Result<AddNodeResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = RoadmapContentApiClient::new(chan.unwrap());
    match client.add_node(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == add_node_response::Code::WrongSession as i32 {
                if let Err(err) = window.emit("notice", new_wrong_session_notice("add_node".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn list_node<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: ListNodeRequest,
) -> Result<ListNodeResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = RoadmapContentApiClient::new(chan.unwrap());
    match client.list_node(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == list_node_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("list_node".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn get_node<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: GetNodeRequest,
) -> Result<GetNodeResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = RoadmapContentApiClient::new(chan.unwrap());
    match client.get_node(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == get_node_response::Code::WrongSession as i32 {
                if let Err(err) = window.emit("notice", new_wrong_session_notice("get_node".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn update_node_size<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: UpdateNodeSizeRequest,
) -> Result<UpdateNodeSizeResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = RoadmapContentApiClient::new(chan.unwrap());
    match client.update_node_size(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == update_node_size_response::Code::WrongSession as i32 {
                if let Err(err) = window.emit(
                    "notice",
                    new_wrong_session_notice("update_node_size".into()),
                ) {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn update_node_position<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: UpdateNodePositionRequest,
) -> Result<UpdateNodePositionResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = RoadmapContentApiClient::new(chan.unwrap());
    match client.update_node_position(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == update_node_position_response::Code::WrongSession as i32 {
                if let Err(err) = window.emit(
                    "notice",
                    new_wrong_session_notice("update_node_position".into()),
                ) {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn update_node_data<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: UpdateNodeDataRequest,
) -> Result<UpdateNodeDataResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = RoadmapContentApiClient::new(chan.unwrap());
    match client.update_node_data(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == update_node_data_response::Code::WrongSession as i32 {
                if let Err(err) = window.emit(
                    "notice",
                    new_wrong_session_notice("update_node_data".into()),
                ) {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn remove_node<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: RemoveNodeRequest,
) -> Result<RemoveNodeResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = RoadmapContentApiClient::new(chan.unwrap());
    match client.remove_node(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == remove_node_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("remove_node".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn add_edge<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: AddEdgeRequest,
) -> Result<AddEdgeResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = RoadmapContentApiClient::new(chan.unwrap());
    match client.add_edge(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == add_edge_response::Code::WrongSession as i32 {
                if let Err(err) = window.emit("notice", new_wrong_session_notice("add_edge".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn list_edge<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: ListEdgeRequest,
) -> Result<ListEdgeResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = RoadmapContentApiClient::new(chan.unwrap());
    match client.list_edge(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == list_edge_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("list_edge".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn get_edge<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: GetEdgeRequest,
) -> Result<GetEdgeResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = RoadmapContentApiClient::new(chan.unwrap());
    match client.get_edge(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == get_edge_response::Code::WrongSession as i32 {
                if let Err(err) = window.emit("notice", new_wrong_session_notice("get_edge".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn update_edge<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: UpdateEdgeRequest,
) -> Result<UpdateEdgeResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = RoadmapContentApiClient::new(chan.unwrap());
    match client.update_edge(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == update_edge_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("update_edge".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn remove_edge<R: Runtime>(
    app_handle: AppHandle<R>,
    window: Window<R>,
    request: RemoveEdgeRequest,
) -> Result<RemoveEdgeResponse, String> {
    let chan = crate::get_grpc_chan(&app_handle).await;
    if (&chan).is_none() {
        return Err("no grpc conn".into());
    }
    let mut client = RoadmapContentApiClient::new(chan.unwrap());
    match client.remove_edge(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            if inner_resp.code == remove_edge_response::Code::WrongSession as i32 {
                if let Err(err) =
                    window.emit("notice", new_wrong_session_notice("remove_edge".into()))
                {
                    println!("{:?}", err);
                }
            }
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

pub struct RoadmapContentApiPlugin<R: Runtime> {
    invoke_handler: Box<dyn Fn(Invoke<R>) + Send + Sync + 'static>,
}

impl<R: Runtime> RoadmapContentApiPlugin<R> {
    pub fn new() -> Self {
        Self {
            invoke_handler: Box::new(tauri::generate_handler![
                add_node,
                list_node,
                get_node,
                update_node_size,
                update_node_position,
                update_node_data,
                remove_node,
                add_edge,
                list_edge,
                get_edge,
                update_edge,
                remove_edge,
            ]),
        }
    }
}

impl<R: Runtime> Plugin<R> for RoadmapContentApiPlugin<R> {
    fn name(&self) -> &'static str {
        "roadmap_content_api"
    }
    fn initialization_script(&self) -> Option<String> {
        None
    }

    fn initialize(&mut self, _app: &AppHandle<R>, _config: serde_json::Value) -> PluginResult<()> {
        Ok(())
    }

    fn created(&mut self, _window: Window<R>) {}

    fn on_page_load(&mut self, _window: Window<R>, _payload: PageLoadPayload) {}

    fn extend_api(&mut self, message: Invoke<R>) {
        (self.invoke_handler)(message)
    }
}
