//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

use proto_gen_rust::appstore_api::appstore_api_client::AppstoreApiClient;
use proto_gen_rust::appstore_api::*;
use tauri::{
    plugin::{Plugin, Result as PluginResult},
    AppHandle, Invoke, PageLoadPayload, Runtime, Window,
};

#[tauri::command]
async fn list_major_cate(
    addr: String,
    request: ListMajorCateRequest,
) -> Result<ListMajorCateResponse, String> {
    let chan = crate::conn_extern_server(addr).await;
    if chan.is_err() {
        return Err(chan.err().unwrap());
    }
    let mut client = AppstoreApiClient::new(chan.unwrap());
    match client.list_major_cate(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn list_minor_cate(
    addr: String,
    request: ListMinorCateRequest,
) -> Result<ListMinorCateResponse, String> {
    let chan = crate::conn_extern_server(addr).await;
    if chan.is_err() {
        return Err(chan.err().unwrap());
    }
    let mut client = AppstoreApiClient::new(chan.unwrap());
    match client.list_minor_cate(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn list_sub_minor_cate(
    addr: String,
    request: ListSubMinorCateRequest,
) -> Result<ListSubMinorCateResponse, String> {
    let chan = crate::conn_extern_server(addr).await;
    if chan.is_err() {
        return Err(chan.err().unwrap());
    }
    let mut client = AppstoreApiClient::new(chan.unwrap());
    match client.list_sub_minor_cate(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn list_app(
    addr: String,
    request: ListAppRequest,
) -> Result<ListAppResponse, String> {
    let chan = crate::conn_extern_server(addr).await;
    if chan.is_err() {
        return Err(chan.err().unwrap());
    }
    let mut client = AppstoreApiClient::new(chan.unwrap());
    match client.list_app(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
pub async fn list_app_by_id(
    addr: String,
    request: ListAppByIdRequest,
) -> Result<ListAppByIdResponse, String> {
    let chan = crate::conn_extern_server(addr).await;
    if chan.is_err() {
        return Err(chan.err().unwrap());
    }
    let mut client = AppstoreApiClient::new(chan.unwrap());
    match client.list_app_by_id(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
pub async fn list_app_by_type(
    addr: String,
    request: ListAppByTypeRequest,
) -> Result<ListAppByTypeResponse, String> {
    let chan = crate::conn_extern_server(addr).await;
    if chan.is_err() {
        return Err(chan.err().unwrap());
    }
    let mut client = AppstoreApiClient::new(chan.unwrap());
    match client.list_app_by_type(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn get_app(
    addr: String,
    request: GetAppRequest,
) -> Result<GetAppResponse, String> {
    let chan = crate::conn_extern_server(addr).await;
    if chan.is_err() {
        return Err(chan.err().unwrap());
    }
    let mut client = AppstoreApiClient::new(chan.unwrap());
    match client.get_app(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn install_app(
    addr: String,
    request: InstallAppRequest,
) -> Result<InstallAppResponse, String> {
    let chan = crate::conn_extern_server(addr).await;
    if chan.is_err() {
        return Err(chan.err().unwrap());
    }
    let mut client = AppstoreApiClient::new(chan.unwrap());
    match client.install_app(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn query_perm(
    addr: String,
    request: QueryPermRequest,
) -> Result<QueryPermResponse, String> {
    let chan = crate::conn_extern_server(addr).await;
    if chan.is_err() {
        return Err(chan.err().unwrap());
    }
    let mut client = AppstoreApiClient::new(chan.unwrap());
    match client.query_perm(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn get_cate_path(
    addr: String,
    request: GetCatePathRequest,
) -> Result<GetCatePathResponse, String> {
    let chan = crate::conn_extern_server(addr).await;
    if chan.is_err() {
        return Err(chan.err().unwrap());
    }
    let mut client = AppstoreApiClient::new(chan.unwrap());
    match client.get_cate_path(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn agree_app(
    addr: String,
    request: AgreeAppRequest,
) -> Result<AgreeAppResponse, String> {
    let chan = crate::conn_extern_server(addr).await;
    if chan.is_err() {
        return Err(chan.err().unwrap());
    }
    let mut client = AppstoreApiClient::new(chan.unwrap());
    match client.agree_app(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

#[tauri::command]
async fn cancel_agree_app(
    addr: String,
    request: CancelAgreeAppRequest,
) -> Result<CancelAgreeAppResponse, String> {
    let chan = crate::conn_extern_server(addr).await;
    if chan.is_err() {
        return Err(chan.err().unwrap());
    }
    let mut client = AppstoreApiClient::new(chan.unwrap());
    match client.cancel_agree_app(request).await {
        Ok(response) => {
            let inner_resp = response.into_inner();
            return Ok(inner_resp);
        }
        Err(status) => Err(status.message().into()),
    }
}

pub struct AppstoreApiPlugin<R: Runtime> {
    invoke_handler: Box<dyn Fn(Invoke<R>) + Send + Sync + 'static>,
}

impl<R: Runtime> AppstoreApiPlugin<R> {
    pub fn new() -> Self {
        Self {
            invoke_handler: Box::new(tauri::generate_handler![
                list_major_cate,
                list_minor_cate,
                list_sub_minor_cate,
                list_app,
                list_app_by_id,
                list_app_by_type,
                get_app,
                install_app,
                query_perm,
                get_cate_path,
                agree_app,
                cancel_agree_app,
            ]),
        }
    }
}

impl<R: Runtime> Plugin<R> for AppstoreApiPlugin<R> {
    fn name(&self) -> &'static str {
        "appstore_api"
    }
    fn initialization_script(&self) -> Option<String> {
        None
    }

    fn initialize(&mut self, _app: &AppHandle<R>, _config: serde_json::Value) -> PluginResult<()> {
        Ok(())
    }

    fn created(&mut self, _window: Window<R>) {}

    fn on_page_load(&mut self, _window: Window<R>, _payload: PageLoadPayload) {}

    fn extend_api(&mut self, message: Invoke<R>) {
        (self.invoke_handler)(message)
    }
}
