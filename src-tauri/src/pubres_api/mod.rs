//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

pub mod appstore_admin_api_plugin;
pub mod appstore_api_plugin;
pub mod idea_store_api_plugin;
pub mod idea_store_admin_api_plugin;
pub mod widget_store_api_plugin;
pub mod widget_store_admin_api_plugin;
pub mod sw_store_api_plugin;
pub mod sw_store_admin_api_plugin;