//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

use crate::client_cfg_api_plugin::{
    VendorAbility, VendorAccount, VendorConfig, VendorDataView, VendorGrowCenter, VendorLayout, VendorOrg, VendorProject, VendorWorkBench
};

pub fn get_vendor_config() -> VendorConfig {
    return VendorConfig {
        vendor_name: "开放原子开源基金会".into(),

        default_server_name: "官方".into(),
        default_server_addr: "serv.linksaas.pro:5000".into(),
        global_server_addr: "serv.linksaas.pro:5000".into(),

        ability: VendorAbility {
            enable_work_bench: true,
            enable_dataview: false,
            enable_project: false,
            enable_org: false,
            enable_pubres: false,
            enable_user_mail: true,
            enable_grow_center: false,
        },
        account: VendorAccount {
            inner_account: false,
            external_account: true,
            external_atomgit: true,
            external_gitcode: false,
            external_gitee: false,
        },
        work_bench: VendorWorkBench {
            enable_minapp: true,
            enable_user_memo: false,
            enable_server_list: false,
            enable_user_resume: false,
            enable_user_todo: true,
        },
        dataview: VendorDataView {
            enable_atomgit: true,
            enable_gitcode: true,
            enable_gitee: true,
            enable_gitlab: true,
            enable_tencloud: true,
            enable_alicloud: true,
            enable_hwcloud: true,
        },
        project: VendorProject {
            show_requirement_list_entry: true,
            show_task_list_entry: true,
            show_bug_list_entry: true,
            show_testcase_list_entry: true,
        },
        org: VendorOrg {
            enable_forum: true,
        },
        grow_center: VendorGrowCenter {
            force_show: false,
            filter_tag: "".into(),
        },
        layout: VendorLayout {
            app_name: "AtomGit客户端".into(),
            hide_sys_bar: true,
            show_feedback_modal: false,
            feedback_url: "https://atomgit.com/openlinksaas-org/desktop/issues".into(),
            show_server_switch: false,
            show_quick_access: false,
            show_admin_in_quick_access: false,
            show_local_api_in_quick_access: false,
            enable_normal_layout: false,
            show_layout_switch: false,
            enable_server_menu: false,
            menu_list: vec![],
            welcome_page_url: "".into(),
            show_manual_and_version: true,
            vendor_logo_url: "".into(),
            vendor_logo_height: 80,
            vendor_link_url: "".into(),
            vendor_intro: r#"
<p>
    <b>AtomGit</b>是开放原子开源基金会旗下的具有自主核心技术的开源代码托管平台,为开源软件、开源硬件、开源芯片等各类开源项目提供基于 Git 协议的源码托管服务,推动创新资源共建共享。
</p>"#.into(),
        },
    };
}
