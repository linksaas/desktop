# 凌鲨简介
凌鲨是专用于软件研发人员/团队专用的效能提升工具，从如下三个方面提升研发效能：
* 为软件研发人员提供了丰富的研发工具
* 为软件研发团队提供项目管理和协作工具
* 通过兴趣组探讨研发知识和技巧

凌鲨使用tauri作为客户端框架，目前支持windows/macos/linux等桌面系统，暂不支持移动端。

凌鲨功能非常多，如果您只需要项目管理相关的功能，可以看下我的另外一个开源项目 [eteam](https://gitcode.com/eteam/desktop)

# 软件架构
![arch](screenshot/arch.png)

# 项目项目

## 微应用

| 项目                                                                        | 说明                      |
| --------------------------------------------------------------------------- | ------------------------- |
| [ssh-proxy-proto](https://atomgit.com/openlinksaas-org/ssh-proxy-proto)     | ssh代理协议(swagger)      |
| [mongo-proxy-proto](https://atomgit.com/openlinksaas-org/mongo-proxy-proto) | mongo代理协议(swagger)    |
| [netutil-proto](https://atomgit.com/openlinksaas-org/netutil-proto)         | 网络调试代理协议(swagger) |
| [sql-proxy-proto](https://atomgit.com/openlinksaas-org/sql-proxy-proto)     | sql代理协议(swagger)      |
| [redis-proxy-proto](https://atomgit.com/openlinksaas/redis-proxy-proto)     | redis代理协议(swagger)    |
| [proxy](https://atomgit.com/openlinksaas-org/proxy)                         | 各种网络协议代理实现      |
| [minapp](https://gitcode.com/minapp)                                        | 官方微应用集合            |

## 其他

| 项目                                                  | 说明                  |
| ----------------------------------------------------- | --------------------- |
| [gitspy](https://atomgit.com/openlinksaas-org/gitspy) | 本地代码仓库工具      |
| [gitwrap](https://atomgit.com/openlinksaas/gitwrap)   | 对git命令行工具的封装 |

# 软件功能
* 个人
  * 本地仓库管理
  * 研发工具
    * 数据库工具
    * ssh终端
    * 其他小工具
* 项目
  * 文档/静态网页/工作计划/绘图白板功能
  * 项目管理
  * API联调
  * 风险预警
  * 外部服务器管理
* 团队
  * 组织架构管理
  * 日报/周报
  * 个人目标
  * 团队互评
* 公共部分
  * 公共知识库
  * 成长路线
  * 常用软件下载
* 成长中心

![img](screenshot/1.png)

# 开发模式

```bash
cp yarn/<os>.lock yarn.lock
yarn install
cd src-tauri
cargo update
cd ..
yarn tauri dev
```


# 构建项目

```bash
cp yarn/<os>.lock yarn.lock
yarn install
cd src-tauri
cargo update
cd ..
yarn tauri build
```

# 使用手册

- [OpenLinkSaas使用手册-简介](https://blog.csdn.net/weixin_37582237/article/details/144734421)
- [OpenLinkSaas使用手册-Git工具](https://blog.csdn.net/weixin_37582237/article/details/144736233)
- [OpenLinkSaas使用手册-研发辅助小工具](https://blog.csdn.net/weixin_37582237/article/details/144759422)
- [OpenLinkSaas使用手册-待办事项和通知中心](https://blog.csdn.net/weixin_37582237/article/details/144876955)
- [OpenLinkSaas使用手册-数据视图](https://blog.csdn.net/weixin_37582237/article/details/144760550)
- [OpenLinkSaas使用手册-项目功能简介](https://blog.csdn.net/weixin_37582237/article/details/144820284)
- [OpenLinkSaas使用手册-项目沟通](https://blog.csdn.net/weixin_37582237/article/details/144822358)
- [OpenLinkSaas使用手册-项目接口联调](https://blog.csdn.net/weixin_37582237/article/details/144844269)
- [OpenLinkSaas使用文档-知识点和项目文档](https://blog.csdn.net/weixin_37582237/article/details/144841491)
- [OpenLinkSaas使用手册-项目视图](https://blog.csdn.net/weixin_37582237/article/details/144875598)
- [OpenLinkSaas使用手册-项目外部资源管理](https://blog.csdn.net/weixin_37582237/article/details/144852140)
- [OpenLinkSaas使用手册-团队功能简介](https://blog.csdn.net/weixin_37582237/article/details/145283629)
- [OpenLinkSaas使用手册-成长中心](https://blog.csdn.net/weixin_37582237/article/details/144899590)